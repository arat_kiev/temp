/**
 * Our base Gruntfile. Use this to init your project
 *
 * @author David Faber
 * @company Peritus Webdesign
 */
module.exports = function (grunt) {
    'use strict';

    grunt.initConfig({});

    // Load internal tasks
    grunt.loadTasks('tools/Grunt');
};
