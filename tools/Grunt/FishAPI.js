/**
 * Module-loader
 *
 * @author David Faber
 * @company Peritus Webdesign
 */
module.exports = function (grunt) {
    'use strict';

    // Loading our modules
    grunt.loadTasks('tools/Grunt/Checkstyle');
    grunt.loadTasks('tools/Grunt/Jenkins');
};

