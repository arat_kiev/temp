/**
 * Our checkstyle-scripts for php
 *
 * @author David Faber
 * @company Peritus Webdesign
 */
module.exports = function (grunt) {
    'use strict';

    // Build the exec-config
    grunt.config('exec.checkstylePhpcsFull', 'php Vendor/squizlabs/php_codesniffer/scripts/phpcs');
    grunt.config(
        'exec.checkstylePhpcsDiff',
        'git diff --name-only develop | grep .php$'
        + ' | while read -r file; do php Vendor/squizlabs/php_codesniffer/scripts/phpcs "$file"; done'
    );
    grunt.config(
        'exec.checkstylePhpcsCommit',
        'git diff --name-only @~..@ | grep .php$'
        + ' | while read -r file; do php Vendor/squizlabs/php_codesniffer/scripts/phpcs "$file"; done'
    );

    // Register the Full check task
    grunt.registerTask('checkstyle:php:full', ['exec:checkstylePhpcsFull']);
    grunt.registerTask('checkstyle:php:diff', ['exec:checkstylePhpcsDiff']);
    grunt.registerTask('checkstyle:php:commit', ['exec:checkstylePhpcsCommit']);

    // Load 3rd-party tasks
    grunt.loadNpmTasks('grunt-exec');
};
