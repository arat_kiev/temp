/**
 * Our checkstyle-scripts for php
 *
 * @author David Faber
 * @company Peritus Webdesign
 */
module.exports = function (grunt) {
    'use strict';

    // Build the exec-config
    grunt.config('exec.jenkinsComposerInstall', 'composer install --no-scripts');

    // Our jenkins tasks
    grunt.registerTask('jenkins:pr', [
        'exec:jenkinsComposerInstall',
        'checkstyle:php:diff',
    ]);

    grunt.registerTask('jenkins:branch', [
        'exec:jenkinsComposerInstall',
        'checkstyle:php:commit',
    ]);

    // Load 3rd-party tasks
    grunt.loadNpmTasks('grunt-exec');
};
