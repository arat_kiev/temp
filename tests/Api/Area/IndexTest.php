<?php

namespace Application\Tests\Api\Area;

use Application\Tests\Api\ApiTestCase;

class IndexTest extends ApiTestCase
{
    public function testPositiveDemo()
    {
        $params = [
            'search'    => 'demo',
        ];
        $paramsQuery = http_build_query($params);

        $response = $this->get('/v1/areas?' . $paramsQuery, $this->apiHeader);

        $response->assertStatus(200);
        $response->assertJson(['id' => 1]);
    }
}
