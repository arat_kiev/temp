<?php

namespace Application\Tests\Api\Area;

use App\Models\Area\Area;
use Application\Tests\Api\ApiTestCase;
use App\Models\User;
use Hamcrest\AssertionError;

class ShowTest extends ApiTestCase
{
    public function testPositive()
    {
        $user = User::find(18);
        $userArea = Area::isMember($user)->first();

        if (!$userArea) {
            throw new AssertionError('User hasn\'t any area');
        }
        $area = $this->get('/v1/areas/' . $userArea->id, $this->apiHeader);

        $area->assertStatus(200);
    }

    public function testNegative()
    {
        $area = $this->get('/v1/areas/not_found', $this->apiHeader);

        $area->assertStatus(404);
        $area->assertJson([
            'error' => 'Not found',
        ]);
    }
}
