<?php

namespace Application\Tests\Api\Fish;

use Application\Tests\Api\ApiTestCase;

class ShowTest extends ApiTestCase
{
    /** @test */
    public function testPositive()
    {
        $this->get('/v1/fishes/1', $this->apiHeader)
            ->assertStatus(200)
            ->assertJson([
                'id' => 1,
                'slug' => 'bachforelle',
            ]);
    }
}
