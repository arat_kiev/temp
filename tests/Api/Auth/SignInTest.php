<?php

namespace Application\Tests\Api\Auth;

use Application\Tests\Api\ApiTestCase;

class SignInTest extends ApiTestCase
{
    /** @test */
    public function testPositive()
    {
        $params = [
            'email'     => 'user1@example.com',
            'password'  => 'user1pass',
        ];

        $this->post('/v1/auth/signin', $params, $this->apiHeader)
            ->assertStatus(200);
    }
}
