<?php

namespace Application\Tests\Api\Area;

use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use App\Models\Product\ProductPrice;
use App\Models\Product\ProductSale;
use App\Models\Product\Timeslot;
use App\Models\Product\TimeslotDate;
use Application\Tests\Api\ApiTestCase;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TimeslotTest extends ApiTestCase
{
    use DatabaseTransactions;

    private function createProductPrice()
    {
        $product = new Product([
            'name' => 'Test Product',
        ]);
        $product->category()->associate(ProductCategory::first());
        $product->save();

        $price = new ProductPrice([
            'visible_from' => Carbon::now()->startOfDay(),
            'valid_from' => Carbon::now()->startOfDay(),
            'valid_till' => Carbon::now()->endOfYear(),
            'value' => '10',
            'vat' => 20,
        ]);

        return tap($price, function ($price) use ($product) {
            $product->prices()->save($price);
        });
    }

    private function createTimeslotsForPrice(ProductPrice $price)
    {
        return $price->timeslots()->createMany([
            [
                'from' => '08:00',
                'till' => '12:00',
                'default_pool' => 5,
            ],
            [
                'from' => '13:00',
                'till' => '17:00',
                'default_pool' => 6,
            ],
        ]);
    }

    public function testResponseWithNoTimeslots()
    {
        $price = $this->createProductPrice();

        $response = $this->get('/v1/products/' . $price->product->id, $this->apiHeader);

        $response
            ->assertStatus(200)
            ->assertJson([
                'active_price' => [
                    'id' => $price->id,
                    'bookable' => false,
                ],
            ]);
    }

    public function testResponseWithTimeslots()
    {
        /** @var ProductPrice $price */
        $price = $this->createProductPrice();
        $this->createTimeslotsForPrice($price);

        $response = $this->get('/v1/products/' . $price->product->id, $this->apiHeader);

        $response
            ->assertStatus(200)
            ->assertJson([
                'active_price' => [
                    'id' => $price->id,
                    'bookable' => true,
                ],
            ]);
    }

    public function testResponseHasTimeslotElements()
    {
        /** @var ProductPrice $price */
        $price = $this->createProductPrice();
        $timeslots = $this->createTimeslotsForPrice($price);

        $response = $this->get('/v1/products/' . $price->product->id, $this->apiHeader);

        $response
            ->assertStatus(200)
            ->assertJson([
                'active_price' => [
                    'id' => $price->id,
                    'bookable' => true,
                    'timeslots' => [
                        'data' => $timeslots->transform(function ($timeslot) {
                            return [
                                'id' => $timeslot->id,
                                'from' => $timeslot->from->format('H:i'),
                                'till' => $timeslot->till->format('H:i'),
                            ];
                        })->toArray(),
                    ],
                ],
            ]);
    }

    public function testResponseHasTimeslotDates()
    {
        /** @var ProductPrice $price */
        $price = $this->createProductPrice();
        $timeslots = $this->createTimeslotsForPrice($price);
        $today = Carbon::now();

        $timeslots->each(function (Timeslot $timeslot) use ($today) {
            foreach (range(0, 6) as $day) {
                $timeslot->dates()->create([
                    'date' => $today->copy()->addDays($day),
                    'pool' => 5,
                ]);
            }
        });

        $response = $this->get('/v1/products/' . $price->product->id, $this->apiHeader);

        $response
            ->assertStatus(200)
            ->assertJson([
                'active_price' => [
                    'id' => $price->id,
                    'bookable' => true,
                    'timeslot_dates' => [],
                ],
            ]);
    }

    public function testResponseHasNumberAvailable()
    {
        /** @var ProductPrice $price */
        $price = $this->createProductPrice();
        $timeslots = $this->createTimeslotsForPrice($price);

        /** @var TimeslotDate $timeslotDate */
        $timeslotDate = $timeslots->first()->dates()->create([
            'date' => Carbon::now(),
            'pool' => 5,
        ]);

        /** @var ProductSale $productSale */
        $productSale = new ProductSale();
        $productSale->product()->associate($price->product);
        $productSale->price()->associate($price);
        $productSale->save();
        $productSale->timeslotDates()->attach($timeslotDate);

        $response = $this->get('/v1/products/' . $price->product->id, $this->apiHeader);

        $response
            ->assertStatus(200)
            ->assertJson([
                'active_price' => [
                    'timeslot_dates' => [
                        $timeslotDate->date->format('Y-m-d') => [
                            [
                                'id' => $timeslotDate->id,
                                'timeslot_id' => $timeslotDate->timeslot->id,
                                'available' => $timeslotDate->pool - 1,
                            ],
                        ]
                    ],
                ],
            ]);
    }
}
