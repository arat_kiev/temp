<?php

namespace Application\Tests\Api\Payment;

use Application\Tests\Api\ApiTestCase;
use App\Models\Payment;
use App\Models\User;
use JWTAuth;

class PaymentTest extends ApiTestCase
{
    /** @test */
    public function testNegativeAuthenticationError()
    {
        $this->post('/v1/payment/init', [], $this->apiHeader)
            ->assertStatus(401)
            ->assertJson([
                'error' => 'Token required',
            ]);
    }

    /** @test */
    public function testPositivePaymentUrlForCreditCard()
    {
        $user = User::find(1);
        $token = JWTAuth::fromUser($user);

        $this->post('/v1/payment/init', [
            'method' => 'CC',
            'amount' => '50',
            'redirect_target' => 'location.href',
            'success_url' => 'https://www.example.com',
            'failure_url' => 'https://www.example.com'
        ], array_merge(
            $this->apiHeader,
            ['Authorization' => 'Bearer '.$token]
        ))->assertStatus(200);
    }

    /** @test */
    public function testPositiveFromPaymentProvider()
    {
        $user = User::find(1);
        $token = JWTAuth::fromUser($user);

        $this->post('/v1/payment/init', [
            'method' => 'CC',
            'amount' => '50',
            'redirect_target' => 'top.location.href',
            'success_url' => 'https://www.success.com',
            'failure_url' => 'https://www.failure.com'
        ], array_merge(
            $this->apiHeader,
            ['Authorization' => 'Bearer '.$token]
        ))->assertStatus(200);

        $payment = Payment::orderBy('id', 'desc')->first();

        $this->post('/v1/payment/response', [
            'PROCESSING.RESULT' => 'ACK',
            'IDENTIFICATION.TRANSACTIONID' => $payment->uuid,
            'CRITERION.SECRET' => $payment->secure_hash,
            'PRESENTATION.AMOUNT' => number_format($payment->amount / 100.0, 2, '.', ''),
            'PRESENTATION.CURRENCY' => 'EUR',
        ])->assertStatus(200)
            ->assertSee('https://www.success.com');
    }

    /** @test */
    public function testNegativeFromPaymentProvider()
    {
        $user = User::find(1);
        $token = JWTAuth::fromUser($user);

        $this->post('/v1/payment/init', [
            'method' => 'CC',
            'amount' => '50',
            'redirect_target' => 'location.href',
            'success_url' => 'https://www.success.com',
            'failure_url' => 'https://www.failure.com'
        ], array_merge(
            $this->apiHeader,
            ['Authorization' => 'Bearer '.$token]
        ))->assertStatus(200);

        $payment = Payment::orderBy('id', 'desc')->first();

        $this->post('/v1/payment/response', [
            'PROCESSING.RESULT' => 'NOK',
            'IDENTIFICATION.TRANSACTIONID' => $payment->uuid,
            'CRITERION.SECRET' => $payment->secure_hash,
            'PRESENTATION.AMOUNT' => number_format($payment->amount / 100.0, 2, '.', ''),
            'PRESENTATION.CURRENCY' => 'EUR',
        ])->assertStatus(200)
            ->assertSee('https://www.failure.com');
    }
}
