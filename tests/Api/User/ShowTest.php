<?php

namespace Application\Tests\Api\User;

use Application\Tests\Api\ApiTestCase;
use App\Models\User;
use Hamcrest\AssertionError;
use JWTAuth;

class ShowTest extends ApiTestCase
{
    /** @test */
    public function testPositive()
    {
        $this->get('/v1/users/1', $this->apiHeader)
            ->assertStatus(401)
            ->assertJson([
                'error' => 'Token required',
            ]);
    }

    /** @test */
    public function testPositiveWithAuthentication()
    {
        $user = User::find(1);
        $token = JWTAuth::fromUser($user);

        $this->get('/v1/users/self', array_merge($this->apiHeader, ['Authorization' => 'Bearer '.$token]))
            ->assertStatus(200)
            ->assertJson([
                'id' => 1,
                'name' => 'User Test',
            ]);
    }

    /** @test */
    public function testPositiveSuperAdminPermission()
    {
        $user = User::select('users.*')
            ->join('user_roles', 'users.id', '=', 'user_roles.user_id')
            ->join('roles', 'roles.id', '=', 'user_roles.role_id')
            ->where('roles.name', '=', 'superadmin')
            ->first();

        if (!$user) {
            throw new AssertionError('There is no superadmin in a system yet');
        }

        $token = JWTAuth::fromUser($user);

        $this->get('/v1/users/13', array_merge($this->apiHeader, ['Authorization' => 'Bearer '.$token]))
            ->assertStatus(200)
            ->assertJson([
                'id' => 13,
                'name' => 'eyeball',
            ]);
    }
}
