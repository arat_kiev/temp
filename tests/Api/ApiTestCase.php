<?php

namespace Application\Tests\Api;

use Application\Tests\TestCase;

class ApiTestCase extends TestCase
{
    protected $baseUrl;

    protected $apiKey = '4ORfBS1w2kICYNNCvpUsb92jAMN5EKrMapEpmI9y';
    protected $apiHeader = [
        'X-Api-Key'     => '4ORfBS1w2kICYNNCvpUsb92jAMN5EKrMapEpmI9y',
        'Content-Type'  => 'application/x-www-form-urlencoded',
        'Accept'        => 'application/json',
        'X-Locale'      => 'de_DE',
    ];

    public function setUp()
    {
        parent::setUp();

        config(['app.url' => 'https://' . env('API_SUBDOMAIN', 'api') . '.' . env('DOMAIN', 'hejfish.com')]);
    }
}
