<?php

namespace Application\Tests;

use Illuminate\Foundation\Testing\TestCase as IlluminateTestCase;
use Illuminate\Contracts\Console\Kernel;

class TestCase extends IlluminateTestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        config(['app.url' => 'https://' . env('DOMAIN', 'www.hejfish.com')]);
    }


    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }
}
