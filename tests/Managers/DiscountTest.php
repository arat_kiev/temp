<?php

namespace Application\Tests\Managers;

use App\Managers\DiscountManager;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use App\Models\Product\ProductPrice;
use Application\Tests\TestCase;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DiscountTest extends TestCase
{
    use DatabaseTransactions;

    /** @var DiscountManager */
    private $dm;

    public function setUp()
    {
        parent::setUp();

        $this->dm = app(DiscountManager::class);
    }

    public function testPriceHasNoDiscount()
    {
        $product = new Product([
            'name' => 'Test Product',
        ]);
        $product->category()->associate(ProductCategory::first());
        $product->save();

        $price = new ProductPrice([
            'visible_from' => Carbon::now()->startOfDay(),
            'valid_from' => Carbon::now()->startOfDay(),
            'valid_till' => Carbon::now()->endOfYear(),
            'value' => '10',
            'vat' => 20,
        ]);
        $product->prices()->save($price);

        $price->timeDiscounts()->createMany([
            [
                'count' => 2,
                'discount' => 100,
                'inclusive' => false,
            ],
            [
                'count' => 5,
                'discount' => 150,
                'inclusive' => true,
            ],
            [
                'count' => 10,
                'discount' => 200,
                'inclusive' => false,
            ],
            [
                'count' => 12,
                'discount' => 300,
                'inclusive' => true,
            ],
        ]);

        $expectedValues = [1000, 1900, 2800, 3700, 4250, 5100, 5950, 6800, 7650, 8450, 9250, 8400, 9100, 9800];
        foreach ($expectedValues as $index => $value) {
            $this->assertEquals($value, $this->dm->discountedPrice($price, $index + 1));
        }
    }
}