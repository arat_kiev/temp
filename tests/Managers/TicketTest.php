<?php

namespace Application\Tests\Managers;

use Application\Tests\TestCase;
use App\Managers\TicketManager;
use App\Models\Ticket\TicketPrice;
use App\Models\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class TicketTest extends TestCase
{
    use WithoutMiddleware;

    /** @var TicketManager */
    private $tm;

    public function setUp()
    {
        parent::setUp();

        $this->tm = app('App\Managers\TicketManager');
    }

    /** @test */
    public function testNegativeNotEnoughBalance()
    {
        $user = new User();
        $user->balance = 1200;

        $price = new TicketPrice();
        $price->value = 1800;

        $this->assertFalse($this->tm->hasEnoughBalance($price, $user));
    }

    /** @test */
    public function testPositiveEnoughBalance()
    {
        $user = new User();
        $user->balance = 1800;

        $price = new TicketPrice();
        $price->value = 1800;

        $this->assertTrue($this->tm->hasEnoughBalance($price, $user));

        $user->balance = 1900;

        $this->assertTrue($this->tm->hasEnoughBalance($price, $user));
    }

    /** @test */
    public function testPositiveMissingBalance()
    {
        $user = new User();
        $user->balance = 1200;

        $price = new TicketPrice();
        $price->value = 1800;

        $this->assertEquals(600, $this->tm->missingBalance($price, $user));
    }

    /** @test */
    public function testNegativeNotValidLicense()
    {
        $user = User::find(1);
        $price = TicketPrice::find(1);

        $this->assertFalse($this->tm->hasValidLicenses($price, $user));
    }
}
