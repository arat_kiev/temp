<?php

Route::group(['namespace' => 'Http\Controllers'], function () {
    Route::get('apidoc/view', 'ApiDocController@getView')->name('apidoc.view');
    Route::get('apidoc/postman', 'ApiDocController@getPostmanTests')->name('apidoc.postman');

    /* Preview Tickets/Products Sales/Invoices (html, img, pdf) */
    Route::get('preview/demo/{ticket}/{format}', 'PreviewController@ticketDemo')->name('preview.demo');
    Route::get('preview/ticket/{ticket}/{format}', 'PreviewController@ticket')->name('preview.ticket');
    Route::get('preview/product/{product_sale}/{format}', 'PreviewController@product')->name('preview.product');
    Route::get('preview/invoice/{invoice}/{format}', 'PreviewController@invoice')->name('preview.invoice');

    /* Static files */
    Route::get('files/download/{name}', 'FileController@download')->where('name', '(.*)')->name('fileload');
    Route::get('files/{name}', 'FileController@stream')->where('name', '(.*)')->name('filestream');
});