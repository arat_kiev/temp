<?php

Route::group(['namespace' => 'Http\Controllers', 'as' => 'dev::'], function () {
    Route::get('/', 'DevController@index')->name('home');
    Route::get('/info', 'DevController@info')->name('info');
    Route::get('/length/{weight}', 'DevController@calculateLenght')->name('length');
    Route::get('/weight/{length}', 'DevController@calculateWeight')->name('weight');
    Route::get('/corp/{length}/{weight}', 'DevController@calculateCorp')->name('corp');
    Route::get('/qrcode', 'DevController@qrcode')->name('qrcode');
    Route::get('/logo', 'DevController@logo')->name('logo');
    Route::get('/related/{userId}', 'DevController@related')->name('related');
});
