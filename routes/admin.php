<?php

Route::group(['namespace' => 'Admin\Controllers', 'as' => 'admin::'], function () {

    Route::group(['namespace' => 'Auth'], function () {
        // Admin login
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login')->name('login');
        Route::get('logout', 'LoginController@logout')->name('logout');
        // Admin password reset
        Route::get('reset', 'ResetPasswordController@getPasswordReset')->name('reset');
        Route::post('reset', 'ResetPasswordController@postPasswordReset')->name('reset');
    });

    Route::group(['middleware' => ['admin'], 'as' => 'docs:'], function () {
        Route::get('manager/legal/{docId}/', 'ManagerDocumentController@getShow')
            ->name('manager.legal.show');
        Route::post('manager/legal/{docId}', 'ManagerDocumentController@postUpdate')
            ->name('manager.legal.update');
    });

    Route::group(['middleware' => ['auth', 'admin', 'manager.docs']], function () {
        Route::get('/', 'HomeController@redirectAdminDashboard')->name('home');

        // Dashboard
        Route::get('dashboard', 'DashboardController@getIndex')->name('dashboard');
        Route::get('dashboard/area_tickets', 'DashboardController@getTicketsPerArea')
            ->name('dashboard.tickets.area');
        Route::get('dashboard/month_tickets', 'DashboardController@getTicketsPerMonth')
            ->name('dashboard.tickets.month');
        Route::get('dashboard/fishtype_catches', 'DashboardController@getCatchesPerFishType')
            ->name('dashboard.catches.fishtype');

        // Manager
        Route::get('manager', 'ManagerController@getIndex')->name('manager.index');
        Route::post('manager/update', 'ManagerController@postUpdate')->name('manager.update');
        Route::get('managers', 'AdminController@getAllManagers')->name('managers.index');
        Route::post('managers/{managerId}', 'AdminController@setManager')->name('managers.set');

        // Rentals
        Route::get('rental', 'RentalController@getIndex')->name('rental.index');
        Route::get('rental/reservations/{timeslotDateId}', 'RentalController@getReservations')->name('rental.reservations.index');
        Route::post('rental/reservations', 'RentalController@postManualReservation')->name('rental.reservations.create');
        Route::post('rental/reservations/delete', 'RentalController@removeManualReservation')->name('rental.reservations.delete');

        Route::get('rentals', 'AdminController@getAllRentals')->name('rentals.index');
        Route::post('rentals/{rentalId}', 'AdminController@setRental')->name('rentals.set');

        // Resellers
        Route::get('resellers', 'ResellerController@getIndex')->name('resellers.index');
        Route::any('resellers/data', 'ResellerController@anyData')->name('resellers.data');
        Route::get('resellers/{resellerId}', 'ResellerController@getEdit')->name('resellers.edit');
        Route::post('resellers/{resellerId}', 'ResellerController@postUpdate')->name('resellers.update');
        Route::get('resellers/ticket_prices/{resellerId?}', 'ResellerController@ticketPricesList')
            ->name('resellers.ticket_prices.list');
        Route::post('resellers/ticket_prices/{resellerId?}', 'ResellerController@updateTicketPrices')
            ->name('resellers.ticket_prices.update');

        Route::get('resellers/{resellerId}/products', 'ResellerController@getProducts')->name('resellers.products.index');
        Route::post('resellers/{resellerId}/products', 'ResellerController@postProducts')->name('resellers.products.store');

        // Inspectors
        Route::get('inspector', 'InspectorController@getIndex')->name('inspector.index');
        Route::any('inspector/data', 'InspectorController@anyData')->name('inspector.data');
        Route::get('inspector/{userId}/areas/{type?}', 'InspectorController@getInspectorAreas')
            ->name('inspector.areas.index');
        Route::post('inspector/{userId}/areas', 'InspectorController@postInspectorAreas')
            ->name('inspector.areas.store');
        Route::post('inspector/{userId}/attach', 'InspectorController@attachToManager')
            ->name('inspector.manager.attach');
        Route::delete('inspector/{userId}/detach', 'InspectorController@detachFromManager')
            ->name('inspector.manager.detach');

        Route::get('inspector/{inspectorId}/inspections', 'TicketInspectionController@getIndex')
            ->name('inspection.index');
        Route::any('inspector/{inspectorId}/inspections/data', 'TicketInspectionController@anyData')
            ->name('inspection.data');
        Route::get('inspector/{inspectorId}/export/{format}', 'TicketInspectionController@getExport')
            ->name('inspection.export');

        Route::get('inspectors', 'AdminController@getAllInspectors')->name('inspectors.index');
        Route::post('inspectors/{inspectorId}', 'AdminController@setInspector')->name('inspectors.set');

        // Blocked users
        Route::get('blacklist', 'BlacklistController@getIndex')->name('blacklist.index');
        Route::any('blacklist/data', 'BlacklistController@anyData')->name('blacklist.data');
        Route::get('blacklist/users', 'BlacklistController@getUserBy')->name('blacklist.users');
        Route::get('blacklist/country', 'BlacklistController@getCountry')->name('blacklist.country');
        Route::delete('blacklist/{recordId}/users', 'BlacklistController@removeUserFromBlackList')->name('blacklist.users.delete');
        Route::post('blacklist/users', 'BlacklistController@addToBlackList')->name('blacklist.users.add');

        // Users
        Route::get('users/all', 'UserController@all')->name('users.all');
        Route::get('users/search', 'UserController@search')->name('users.search');

        // Organization
        Route::get('organization', 'OrganizationController@getIndex')->name('organization.index');
        Route::post('organization/update', 'OrganizationController@postUpdate')->name('organization.update');
        Route::get('organizations', 'AdminController@getAllOrganizations')->name('organizations.index');
        Route::post('organizations/{organizationId}', 'AdminController@setOrganization')->name('organizations.set');

        // Organization members
        Route::get('members', 'MemberController@getIndex')->name('members.index');
        Route::get('members/create', 'MemberController@getCreate')->name('members.create');
        Route::post('members/store', 'MemberController@postStore')->name('members.store');
        Route::any('members/data', 'MemberController@anyData')->name('members.data');
        Route::post('members/import', 'MemberController@parseImport')->name('csv-uploads.import');
        Route::post('members/import/store', 'MemberController@processImport')->name('csv-uploads.store');
        Route::get('members/{memberId}', 'MemberController@getEdit')->name('members.edit');
        Route::post('members/{memberId}', 'MemberController@postUpdate')->name('members.update');
        Route::get('members/{memberId}/delete', 'MemberController@getDelete')->name('members.delete');

        // Organization news
        Route::get('news', 'NewsController@getIndex')->name('news.index');
        Route::any('news/data', 'NewsController@anyData')->name('news.data');
        Route::get('news/create', 'NewsController@getCreate')->name('news.create');
        Route::post('news/store', 'NewsController@postStore')->name('news.store');
        Route::get('news/{newsId}', 'NewsController@getEdit')->name('news.edit');
        Route::post('news/{newsId}', 'NewsController@postUpdate')->name('news.update');

        // News gallery
        Route::post('news/{newsId}/gallery/upload', 'NewsController@postUpload')->name('news.gallery.upload');
        Route::get('news/{newsId}/gallery/{pictureId}', 'NewsController@getPrimary')->name('news.gallery.primary');
        Route::get('news/{newsId}/gallery/{pictureId}/delete', 'NewsController@getDeletePicture')->name('news.gallery.delete');
        Route::get('news/{newsId}/delete', 'NewsController@getDelete')->name('news.delete');

        // Area
        Route::get('areas', 'AreaController@getIndex')->name('areas.index');
        Route::get('areas/all', 'AreaController@allManagerAreas')->name('areas.all');
        Route::any('areas/data', 'AreaController@anyData')->name('areas.data');
        Route::get('affiliated', 'AreaController@getAffiliated')->name('areas.affiliated');
        Route::get('areas/create', 'AreaController@getCreate')->name('areas.create');
        Route::post('areas/store', 'AreaController@postStore')->name('areas.store');
        Route::get('areas/{areaId}', 'AreaController@getEdit')->name('areas.edit');
        Route::post('areas/{areaId}', 'AreaController@postUpdate')->name('areas.update');

        // Ticket types
        Route::get('areas/{areaId}/tickettypes', 'TicketTypeController@getIndex')->name('areas.tickettypes.index');
        Route::get('areas/{areaId}/tickettypes/create', 'TicketTypeController@getCreate')
            ->name('areas.tickettypes.create');
        Route::post('areas/{areaId}/tickettypes/store', 'TicketTypeController@postStore')
            ->name('areas.tickettypes.store');
        Route::get('areas/{areaId}/tickettypes/{tickeTypeId}', 'TicketTypeController@getEdit')
            ->name('areas.tickettypes.edit');
        Route::post('areas/{areaId}/tickettypes/{tickeTypeId}', 'TicketTypeController@postUpdate')
            ->name('areas.tickettypes.update');
        Route::post('areas/{areaId}/tickettypes/{tickeTypeId?}/partial', 'TicketTypeController@partialUpdate')
            ->name('areas.tickettypes.partial.update');
        Route::post('areas/{areaId}/tickettypes/{tickeTypeId?}/additionalAreas', 'TicketTypeController@additionalAreasUpdate')
            ->name('areas.tickettypes.additional-areas.update');
        Route::post('areas/{areaId}/tickettypes/{ticketTypeId?}/getAdditionalAreas', 'TicketTypeController@getAdditionalAreas')
            ->name('areas.tickettypes.additional-areas.get');

        // Ticket prices
        Route::get('areas/{areaId}/tickettypes/{tickeTypeId}/ticketprices', 'TicketPriceController@getIndex')
            ->name('ticketprices.index');
        Route::get('areas/{areaId}/tickettypes/{tickeTypeId}/ticketprices/create', 'TicketPriceController@getCreate')
            ->name('ticketprices.create');
        Route::post('areas/{areaId}/tickettypes/{tickeTypeId}/ticketprices/store', 'TicketPriceController@postStore')
            ->name('ticketprices.store');
        Route::get('areas/{areaId}/tickettypes/{tickeTypeId}/ticketprices/{ticketPriceId}', 'TicketPriceController@getEdit')
            ->name('ticketprices.edit');
        Route::post('areas/{areaId}/tickettypes/{tickeTypeId}/ticketprices/{ticketPriceId}', 'TicketPriceController@postUpdate')
            ->name('ticketprices.update');
        Route::get('areas/{areaId}/tickettypes/{tickeTypeId}/ticketprices/{ticketPriceId}/organizations', 'TicketPriceController@getOrganizations')
            ->name('ticketprices.organizations.index');
        Route::post('areas/{areaId}/tickettypes/{tickeTypeId}/ticketprices/{ticketPriceId}/organizations', 'TicketPriceController@postOrganizations')
            ->name('ticketprices.organizations.store');

        // Area's Gallery
        Route::get('areas/{areaId}/gallery', 'GalleryController@getList')->name('areas.gallery.list');
        Route::post('areas/{areaId}/gallery/upload', 'GalleryController@postUpload')->name('areas.gallery.upload');
        Route::get('areas/{areaId}/gallery/{pictureId}', 'GalleryController@getEdit')->name('areas.gallery.edit');
        Route::post('areas/{areaId}/gallery/{pictureId}', 'GalleryController@postSave')->name('areas.gallery.save');
        Route::get('areas/{areaId}/gallery/{pictureId}/delete', 'GalleryController@getDelete')
            ->name('areas.gallery.delete');

        // Products
        Route::get('products', 'ProductController@getIndex')->name('products.index');
        Route::get('products/data', 'ProductController@anyData')->name('products.data');
        Route::get('products/all', 'ProductController@allManagerProducts')->name('products.all');
        Route::get('products/create', 'ProductController@getCreate')->name('products.create');
        Route::get('products/{productId}', 'ProductController@getEdit')->name('products.edit');
        Route::get('products/{productId}/data', 'ProductController@show')->name('products.show');
        Route::post('products/', 'ProductController@postCreate')->name('products.store');
        Route::post('products/{productId}', 'ProductController@postUpdate')->name('products.update');

        Route::get('products/{productId}/areas', 'ProductController@getAreas')->name('products.areas.index');
        Route::post('products/{productId}/areas', 'ProductController@postAreas')->name('products.areas.store');

        Route::post('products/{productId}/attachment', 'ProductController@uploadAttachment')
            ->name('products.attachment.upload');
        Route::delete('products/{productId}/attachment', 'ProductController@deleteAttachment')
            ->name('products.attachment.delete');

        Route::post('products/{productId}/info', 'ProductController@updateHiddenInfo')
            ->name('products.info.update');

        // Products' Gallery
        Route::get('products/{productId}/gallery', 'ProductGalleryController@getList')
            ->name('products.gallery.list');
        Route::post('products/{productId}/gallery/upload', 'ProductGalleryController@postUpload')
            ->name('products.gallery.upload');
        Route::get('products/{productId}/gallery/{pictureId}', 'ProductGalleryController@getEdit')
            ->name('products.gallery.edit');
        Route::post('products/{productId}/gallery/{pictureId}', 'ProductGalleryController@postSave')
            ->name('products.gallery.save');
        Route::get('products/{productId}/gallery/{pictureId}/delete', 'ProductGalleryController@getDelete')
            ->name('products.gallery.delete');

        // Product prices
        Route::get('products/{productId}/prices', 'ProductPriceController@getIndex')
            ->name('products.prices.index');
        Route::get('products/{productId}/prices/data', 'ProductPriceController@anyData')
            ->name('products.prices.data');
        Route::get('products/{productId}/prices/create', 'ProductPriceController@getCreate')
            ->name('products.prices.create');
        Route::get('products/{productId}/prices/{priceId}', 'ProductPriceController@getEdit')
            ->name('products.prices.edit');
        Route::post('products/{productId}/prices', 'ProductPriceController@postCreate')
            ->name('products.prices.store');
        Route::post('products/{productId}/prices/{priceId}', 'ProductPriceController@postUpdate')
            ->name('products.prices.update');
        Route::delete('products/{productId}/prices/{priceId}', 'ProductPriceController@delete')
            ->name('products.prices.delete');
        Route::get('products/{productId}/prices/{priceId}/dates', 'ProductPriceController@getTimeslotDates')
            ->name('products.prices.dates.list');
        Route::post('products/{productId}/prices/{priceId}/dates', 'ProductPriceController@postTimeslotDates')
            ->name('products.prices.dates.update');

        // Product Sales
        Route::get('product_sales', 'ProductSaleController@getIndex')->name('sales.product.index');
        Route::get('product_sales/export/{format}', 'ProductSaleController@getExport')->name('sales.product.export');
        Route::any('product_sales/data', 'ProductSaleController@anyData')->name('sales.product.data');
        Route::post('product_sales/{saleId}/delete', 'ProductSaleController@postStornoProductSale')
            ->name('sales.product.storno');
        Route::post('product_sales/{saleId}/status', 'ProductSaleController@postUpdateStatus')
            ->name('sales.product.status');
        // Canceled product sales (storno product sales)
        Route::get('product_sales/storno', 'ProductSaleController@getStornoIndex')
            ->name('sales.product.storno.index');
        Route::any('product_sales/storno/data', 'ProductSaleController@anyStornoData')
            ->name('sales.product.storno.data');

        // Ticket Sales
        Route::get('sales', 'SalesController@getIndex')->name('sales.index');
        Route::get('sales/export/{format}', 'SalesController@getExport')->name('sales.export');
        Route::any('sales/data', 'SalesController@anyData')->name('sales.data');
        Route::get('tickets/{ticketId}', 'SalesController@ticketDetails')->name('tickets.details');
        Route::get('tickets/{ticketId}/checkins', 'SalesController@getCheckins')->name('tickets.checkins');
        Route::patch('tickets/{ticketId}/checkins', 'SalesController@updateCheckins')->name('tickets.checkins.update');
        Route::delete('tickets/{ticketId}/checkins/{checkinId}', 'SalesController@deleteCheckin')->name('tickets.checkins.delete');
        Route::post('sales/{ticketId}/delete', 'SalesController@softDelete')->name('sales.ticket.storno');
        // Canceled Tickets (storno ticket)
        Route::get('sales/storno', 'SalesController@getStornoIndex')->name('sales.storno.index');
        Route::any('sales/storno/data', 'SalesController@anyStornoData')->name('sales.storno.data');

        // Invoices
        Route::get('invoices/', 'InvoiceController@getIndex')->name('invoices.index');
        Route::any('invoices/data', 'InvoiceController@anyData')->name('invoices.data');
        Route::get('invoices/create', 'InvoiceController@getCreate')->name('invoices.create');
        Route::get('invoices/{invoiceId}', 'InvoiceController@getShow')->name('invoices.show');
        Route::get('invoices/{invoiceId}/data', 'InvoiceController@invoiceData')->name('invoices.show.data');
        Route::post('invoices/', 'InvoiceController@postCreate')->name('invoices.store');
        Route::post('invoices/{invoiceId}/delete', 'InvoiceController@postStornoInvoice')->name('invoices.storno');

        // Hauls
        Route::get('hauls', 'HaulsController@getIndex')->name('hauls.index');
        Route::any('hauls/data', 'HaulsController@anyData')->name('hauls.data');
        Route::get('hauls/create', 'HaulsController@getCreate')->name('hauls.create');
        Route::get('hauls/{haulId}', 'HaulsController@getEdit')->name('hauls.edit');
        Route::get('hauls/get/{haulId}', 'HaulsController@getHauls')->name('hauls.get-hauls');
        Route::get('hauls/export/{format}', 'HaulsController@getExport')->name('hauls.export');
        Route::post('hauls', 'HaulsController@postStore')->name('hauls.store');
        Route::post('hauls/multi', 'HaulsController@postMultiStore')->name('hauls.multi.create');
        Route::post('hauls/empty', 'HaulsController@postEmptyHaul')->name('hauls.empty.create');
        Route::put('hauls/{haulId}', 'HaulsController@postUpdate')->name('hauls.update');
        Route::delete('hauls/{haulId}', 'HaulsController@delete')->name('hauls.delete');

        Route::get('license/{licenseId}/accept', 'LicenseController@getAccept')->name('license.accept');
        
        // Fishes
        Route::get('fishes/area/{areaId}', 'FishController@areaFishes')->name('fishes.area');

        //Tickets
        Route::get('tickets/additionalAreas/{ticketId}', 'TicketController@getAdditionalAreas')->name('tickets.getAdditionalAreas');

        // Inspector
        Route::group(['prefix' => 'inspect', 'namespace' => 'Inspect', 'as' => 'inspect:'], function () {
            // Tickets
            Route::get('tickets', 'TicketController@getIndex')->name('tickets.index');
            Route::any('tickets/data', 'TicketController@anyData')->name('tickets.data');
            Route::get('tickets/{ticketId}', 'TicketController@getEdit')->name('tickets.inspect.edit');
            Route::any('tickets/{ticketId}/data', 'TicketController@anyInspectionData')->name('tickets.inspect.data');
            Route::post('tickets/{ticketId}', 'TicketController@postUpdate')->name('tickets.inspect.update');

            // Hauls
            Route::get('hauls', 'HaulController@getIndex')->name('hauls.index');
            Route::any('hauls/data', 'HaulController@anyData')->name('hauls.data');
            Route::get('hauls/export/{format}', 'HaulController@getExport')->name('hauls.export');

            // Areas
            Route::get('areas', 'AreaController@getIndex')->name('areas.index');
            Route::any('areas/data', 'AreaController@anyData')->name('areas.data');
        });

        // Superadmin
        Route::group(['prefix' => 'super', 'namespace' => 'Super', 'as' => 'super:'], function () {
            // Managers
            Route::get('managers', 'ManagerController@getIndex')->name('managers.index');
            Route::get('managers/additional', 'ManagerController@getAdditionalManagersList')->name('managers.additional');
            Route::any('managers/data', 'ManagerController@anyData')->name('managers.data');
            Route::get('managers/all', 'ManagerController@getAllManagers')->name('managers.all');
            Route::get('managers/create', 'ManagerController@getCreate')->name('managers.create');
            Route::post('managers/store', 'ManagerController@postStore')->name('managers.store');
            Route::get('managers/{managerId}', 'ManagerController@getEdit')->name('managers.edit');
            Route::post('managers/{managerId}', 'ManagerController@postUpdate')->name('managers.update');
            Route::get('managers/{managerId}/admins', 'ManagerController@getAdmins')->name('managers.admins.index');
            Route::get('managers/{managerId}/last_invoice', 'ManagerController@getLastInvoice')->name('managers.sale.last_invoice');
            Route::post('managers/{managerId}/admins', 'ManagerController@postAdmins')->name('managers.admins.store');
            Route::post('managers/{managerId}/rksv', 'ManagerController@createRKSV')->name('managers.rksv.create');
            Route::get('managers/{managerId}/rksv/activate', 'ManagerController@activateRKSV')->name('managers.rksv.activate');
            Route::get('managers/{managerId}/rksv/deactivate', 'ManagerController@deactivateRKSV')->name('managers.rksv.deactivate');
            Route::get('managers/{managerID}/data', 'ManagerController@getManagerData')->name('managers.rksv.data');
            Route::delete('managers/{managerId}', 'ManagerController@delete')->name('managers.delete');
            Route::post('managers/{managerId}/publish', 'ManagerController@publish')->name('managers.publish');
            // TODO: Refactor method to user controller
            Route::get('users/{userId}/data', 'ManagerController@getUserData')->name('managers.rksv.user');

            Route::get('managers/{managerId}/commissions/tickets', 'CommissionController@treeTicketsCommissions')
                ->name('managers.commissions.tickets.tree');
            Route::get('managers/{managerId}/commissions/products', 'CommissionController@treeProductsCommissions')
                ->name('managers.commissions.products.tree');
            Route::post('managers/{managerId}/commissions/tickets', 'CommissionController@updateTicketsCommissions')
                ->name('managers.commissions.tickets.update');
            Route::post('managers/{managerId}/commissions/products', 'CommissionController@updateProductsCommissions')
                ->name('managers.commissions.products.update');

            // Resellers
            Route::get('resellers', 'ResellerController@getIndex')->name('resellers.index');
            Route::any('resellers/data', 'ResellerController@anyData')->name('resellers.data');
            Route::get('resellers/create', 'ResellerController@getCreate')->name('resellers.create');
            Route::post('resellers/store', 'ResellerController@postStore')->name('resellers.store');
            Route::get('resellers/{resellerId}', 'ResellerController@getEdit')->name('resellers.edit');
            Route::post('resellers/{resellerId}', 'ResellerController@postUpdate')->name('resellers.update');
            Route::get('resellers/{resellerId}/admins', 'ResellerController@getAdmins')
                ->name('resellers.admins.index');
            Route::get('resellers/{resellerId}/managers', 'ResellerController@getManagers')
                ->name('resellers.managers.index');
            Route::post('resellers/{resellerId}/admins', 'ResellerController@postAdmins')
                ->name('resellers.admins.store');
            Route::post('resellers/{resellerId}/managers', 'ResellerController@postManagers')
                ->name('resellers.managers.store');

            Route::get('resellers/{resellerId}/products', 'ResellerController@getProducts')
                ->name('resellers.products.index');
            Route::post('resellers/{resellerId}/products', 'ResellerController@postProducts')
                ->name('resellers.products.store');

            // Rentals
            Route::get('rentals', 'RentalController@getIndex')->name('rentals.index');
            Route::any('rentals/data', 'RentalController@anyData')->name('rentals.data');
            Route::get('rentals/create', 'RentalController@getCreate')->name('rentals.create');
            Route::post('rentals/store', 'RentalController@postStore')->name('rentals.store');
            Route::get('rentals/{resellerId}', 'RentalController@getEdit')->name('rentals.edit');
            Route::post('rentals/{resellerId}', 'RentalController@postUpdate')->name('rentals.update');
            Route::get('rentals/{resellerId}/admins', 'RentalController@getAdmins')
                ->name('rentals.admins.index');
            Route::post('rentals/{resellerId}/admins', 'RentalController@postAdmins')
                ->name('rentals.admins.store');

            // Users
            Route::get('users', 'UserController@getIndex')->name('users.index');
            Route::any('users/data', 'UserController@anyData')->name('users.data');
            Route::get('users/all', 'UserController@getAllUsers')->name('users.all');
            Route::get('users/{userId}', 'UserController@getEdit')->name('users.edit');
            Route::post('users/{userId}', 'UserController@postUpdate')->name('users.update');
            Route::delete('users/{userId}', 'UserController@destroy')->name('users.destroy');
            Route::get('users/{userId}/roles', 'UserController@getRoles')->name('users.roles.index');
            Route::post('users/{userId}/roles', 'UserController@postRoles')->name('users.roles.store');
            Route::post('users/{userId}/reset', 'UserController@postPasswordReset')->name('users.password.reset');
            Route::get('roles/all', 'UserController@getAllRoles')->name('users.roles.all');
            // User's Stats
            Route::get('stats', 'UserController@stats')->name('users.stats');
            Route::get('stats/users', 'UserController@statsUsers')->name('users.stats.users');
            Route::get('stats/tickets', 'UserController@statsTickets')->name('users.stats.tickets');
            // User's export
            Route::post('export', 'UserController@createExport')->name('users.export.create');
            Route::get('users/export/{filename?}', 'UserController@getExport')->name('users.export.download');
            //User's ticket history
            Route::get('users/{userId}/tickets', 'UserController@getUserTicketHistory')->name('users.tickets.history');

            // Areas
            Route::get('areas', 'AreaController@getIndex')->name('areas.index');
            Route::any('areas/data', 'AreaController@anyData')->name('areas.data');
            Route::get('areas/all', 'AreaController@allAreas')->name('areas.all');
            Route::get('affiliated', 'AreaController@getAffiliated')->name('areas.affiliated');
            Route::get('areas/create', 'AreaController@getCreate')->name('areas.create');
            Route::post('areas/store', 'AreaController@postStore')->name('areas.store');
            Route::get('areas/{areaId}', 'AreaController@getEdit')->name('areas.edit');
            Route::get('areas/{areaId}/managers', 'AreaController@getAdditionalManagers')->name('get.areas.additional.managers');
            Route::post('areas/{areaId}/managers', 'AreaController@createAdditionalManagers')->name('create.areas.additional.managers');
            Route::post('areas/{areaId}/managers/{managerId}', 'AreaController@editAdditionalManagers')->name('edit.areas.additional.managers');
            Route::delete('areas/{areaId}/managers/{managerId}', 'AreaController@deleteAdditionalManagers')->name('delete.areas.additional.managers');
            Route::post('areas/{areaId}', 'AreaController@postUpdate')->name('areas.update');
            Route::delete('areas/{areaId}', 'AreaController@delete')->name('areas.delete');
            Route::post('areas/{areaId}/publish', 'AreaController@publish')->name('areas.publish');

            // Clients
            Route::get('areas/{areaId}/clients', 'AreaController@getClients')->name('areas.clients.index');
            Route::post('areas/{areaId}/clients', 'AreaController@postClients')->name('areas.clients.store');
            Route::get('clients/all', 'AreaController@getAllClients')->name('areas.clients.all');

            // Licenses
            Route::get('licenses', 'LicenseController@getIndex')->name('licenses.index');
            Route::any('licenses/data', 'LicenseController@anyData')->name('licenses.data');
            Route::get('licenses/{licenseId}', 'LicenseController@getEdit')->name('licenses.edit');
            Route::get('licenses/{licenseId}/delete', 'LicenseController@getDelete')->name('licenses.delete');
            Route::post('licenses/{licenseId}', 'LicenseController@postUpdate')->name('licenses.update');
            Route::get('licenses/types/{licenseTypeId?}', 'LicenseController@getLicenseTypeAttributes')
                ->name('license.type.attributes');
            Route::post('licenses', 'LicenseController@create')->name('licenses.create');

            // License types
            Route::get('licensetypes', 'LicenseTypeController@getIndex')->name('licensetypes.index');
            Route::any('licensetypes/data', 'LicenseTypeController@anyData')->name('licensetypes.data');
            Route::get('licensetypes/create', 'LicenseTypeController@getCreate')->name('licensetypes.create');
            Route::post('licensetypes/store', 'LicenseTypeController@postStore')->name('licensetypes.store');
            Route::get('licensetypes/{typeId}', 'LicenseTypeController@getEdit')->name('licensetypes.edit');
            Route::post('licensetypes/{typeId}', 'LicenseTypeController@postUpdate')->name('licensetypes.update');
            Route::get('licensetypes/{typeId}/states', 'LicenseTypeController@getStates')
                ->name('licensetypes.states.index');
            Route::post('licensetypes/{typeId}/states', 'LicenseTypeController@postStates')
                ->name('licensetypes.states.store');
            
            // Stocks
            Route::get('stocks', 'StockController@getIndex')->name('stocks.index');
            Route::get('stocks/data', 'StockController@anyData')->name('stocks.data');
            Route::get('stocks/create', 'StockController@getCreate')->name('stocks.create');
            Route::get('stocks/{stockId}', 'StockController@getEdit')->name('stocks.edit');
            Route::get('stocks/{stockId}/data', 'StockController@show')->name('stocks.show');
            Route::post('stocks/', 'StockController@postCreate')->name('stocks.store');
            Route::post('stocks/{stockId}', 'StockController@postUpdate')->name('stocks.update');
            Route::delete('stocks/{stockId}', 'StockController@delete')->name('stocks.delete');

            // Products in Stocks
            Route::get('product_stocks', 'ProductStockController@getIndex')->name('product_stocks.index');
            Route::get('product_stocks/data', 'ProductStockController@anyData')->name('product_stocks.data');
            Route::get('product_stocks/create', 'ProductStockController@getCreate')->name('product_stocks.create');
            Route::get('product_stocks/{stockId}', 'ProductStockController@getEdit')->name('product_stocks.edit');
            Route::get('product_stocks/{stockId}/notifiable', 'ProductStockController@getNotifiables')
                ->name('product_stocks.notifiable.index');
            Route::post('product_stocks/{stockId}/notifiable', 'ProductStockController@postNotifiables')
                ->name('product_stocks.notifiable.store');
            Route::post('product_stocks/', 'ProductStockController@postCreate')->name('product_stocks.store');
            Route::put('product_stocks/{stockId}', 'ProductStockController@postUpdate')->name('product_stocks.update');
            Route::delete('product_stocks/{stockId}', 'ProductStockController@delete')->name('product_stocks.delete');

            // Promotions
            Route::get('promotions', 'PromotionController@getIndex')->name('promotions.index');
            Route::get('promotions/data', 'PromotionController@anyData')->name('promotions.data');
            Route::get('promotions/create', 'PromotionController@getCreate')->name('promotions.create');
            Route::get('promotions/{promoId}/data', 'PromotionController@show')->name('promotions.show');
            Route::get('promotions/{promoId}', 'PromotionController@getEdit')->name('promotions.edit');
            Route::post('promotions/', 'PromotionController@postCreate')->name('promotions.store');
            Route::post('promotions/{promoId}', 'PromotionController@postUpdate')->name('promotions.update');
            Route::delete('promotions/{promoId}', 'PromotionController@delete')->name('promotions.delete');

            Route::get('promotions/{promoId}/products', 'PromotionController@getProducts')
                ->name('promotions.products.index');
            Route::post('promotions/{promoId}/products', 'PromotionController@postProducts')
                ->name('promotions.products.store');
            Route::get('promotions/{promoId}/tickettypes', 'PromotionController@getTicketTypes')
                ->name('promotions.tickettypes.index');
            Route::post('promotions/{promoId}/tickettypes', 'PromotionController@postTicketTypes')
                ->name('promotions.tickettypes.store');
            
            // Coupons
            Route::get('coupons', 'CouponController@getIndex')->name('coupons.index');
            Route::get('coupons/data', 'CouponController@anyData')->name('coupons.data');
            Route::get('coupons/{couponId}/data', 'CouponController@show')->name('coupons.show');
            Route::put('coupons/{couponId}/code', 'CouponController@updateCode')->name('coupons.update.code');
            Route::delete('coupons/{couponId}', 'CouponController@delete')->name('coupons.delete');

            // Fees
            Route::get('fees', 'FeeController@getIndex')->name('fees.index');
            Route::get('fees/all', 'FeeController@getAllFees')->name('fees.all');
            Route::get('fees/data', 'FeeController@anyData')->name('fees.data');
            Route::get('fees/create', 'FeeController@getCreate')->name('fees.create');
            Route::get('fees/{feeId}', 'FeeController@getEdit')->name('fees.edit');
            Route::get('fees/{feeId}/data', 'FeeController@show')->name('fees.show');
            Route::post('fees/', 'FeeController@postCreate')->name('fees.store');
            Route::post('fees/{feeId}', 'FeeController@postUpdate')->name('fees.update');
            Route::delete('fees/{feeId}', 'FeeCategoryController@delete')->name('fees.delete');

            // Fee categories
            Route::get('fee_categories', 'FeeCategoryController@getIndex')
                ->name('fee_categories.index');
            Route::get('fee_categories/data', 'FeeCategoryController@anyData')
                ->name('fee_categories.data');
            Route::get('fee_categories/create', 'FeeCategoryController@getCreate')
                ->name('fee_categories.create');
            Route::get('fee_categories/{categoryId}', 'FeeCategoryController@getEdit')
                ->name('fee_categories.edit');
            Route::get('fee_categories/{categoryId}/data', 'FeeCategoryController@show')
                ->name('fee_categories.show');
            Route::post('fee_categories/', 'FeeCategoryController@postCreate')
                ->name('fee_categories.store');
            Route::post('fee_categories/{categoryId}', 'FeeCategoryController@postUpdate')
                ->name('fee_categories.update');
            Route::delete('fee_categories/{categoryId}', 'FeeCategoryController@delete')
                ->name('fee_categories.delete');

            // Products
            Route::get('products', 'ProductController@getIndex')->name('products.index');
            Route::get('products/data', 'ProductController@anyData')->name('products.data');
            Route::get('products/all', 'ProductController@allProducts')->name('products.all');
            Route::get('products/create', 'ProductController@getCreate')->name('products.create');
            Route::get('products/{productId}', 'ProductController@getEdit')->name('products.edit');
            Route::get('products/{productId}/data', 'ProductController@show')->name('products.show');
            Route::post('products/', 'ProductController@postCreate')->name('products.store');
            Route::post('products/{productId}', 'ProductController@postUpdate')->name('products.update');

            Route::get('products/{productId}/areas', 'ProductController@getAreas')
                ->name('products.areas.index');
            Route::post('products/{productId}/areas', 'ProductController@postAreas')
                ->name('products.areas.store');
            Route::get('products/{productId}/managers', 'ProductController@getManagers')
                ->name('products.managers.index');
            Route::post('products/{productId}/managers', 'ProductController@postManagers')
                ->name('products.managers.store');
            Route::get('products/{productId}/rentals', 'ProductController@getRentals')
                ->name('products.rentals.index');
            Route::post('products/{productId}/rentals', 'ProductController@postRentals')
                ->name('products.rentals.store');

            Route::post('products/{productId}/attachment', 'ProductController@uploadAttachment')
                ->name('products.attachment.upload');
            Route::delete('products/{productId}/attachment', 'ProductController@deleteAttachment')
                ->name('products.attachment.delete');

            // Products' Gallery
            Route::get('products/{productId}/gallery', 'ProductGalleryController@getList')
                ->name('products.gallery.list');
            Route::post('products/{productId}/gallery/upload', 'ProductGalleryController@postUpload')
                ->name('products.gallery.upload');
            Route::get('products/{productId}/gallery/{pictureId}', 'ProductGalleryController@getEdit')
                ->name('products.gallery.edit');
            Route::post('products/{productId}/gallery/{pictureId}', 'ProductGalleryController@postSave')
                ->name('products.gallery.save');
            Route::get('products/{productId}/gallery/{pictureId}/delete', 'ProductGalleryController@getDelete')
                ->name('products.gallery.delete');

            // Product prices
            Route::get('product_prices', 'ProductPriceController@getIndex')->name('product_prices.index');
            Route::get('product_prices/data', 'ProductPriceController@anyData')->name('product_prices.data');
            Route::get('product_prices/create', 'ProductPriceController@getCreate')->name('product_prices.create');
            Route::get('product_prices/{priceId}', 'ProductPriceController@getEdit')->name('product_prices.edit');
            Route::post('product_prices/', 'ProductPriceController@postCreate')->name('product_prices.store');
            Route::post('product_prices/{priceId}', 'ProductPriceController@postUpdate')->name('product_prices.update');
            Route::delete('product_prices/{priceId}', 'ProductPriceController@delete')->name('product_prices.delete');

            Route::get('product_prices/{priceId}/fees', 'ProductPriceController@getFees')
                ->name('product_prices.fees.index');
            Route::post('product_prices/{priceId}/fees', 'ProductPriceController@postFees')
                ->name('product_prices.fees.store');

            // Product categories
            Route::get('product_categories', 'ProductCategoryController@getIndex')
                ->name('product_categories.index');
            Route::get('product_categories/data', 'ProductCategoryController@anyData')
                ->name('product_categories.data');
            Route::get('product_categories/create', 'ProductCategoryController@getCreate')
                ->name('product_categories.create');
            Route::get('product_categories/{categoryId}', 'ProductCategoryController@getEdit')
                ->name('product_categories.edit');
            Route::get('product_categories/{categoryId}/data', 'ProductCategoryController@show')
                ->name('product_categories.show');
            Route::post('product_categories/', 'ProductCategoryController@postCreate')
                ->name('product_categories.store');
            Route::post('product_categories/{categoryId}', 'ProductCategoryController@postUpdate')
                ->name('product_categories.update');
            Route::delete('product_categories/{categoryId}', 'ProductCategoryController@delete')
                ->name('product_categories.delete');

            // Product Sales
            Route::get('product_sales', 'ProductSaleController@getIndex')->name('sales.product.index');
            Route::get('product_sales/export/{format}', 'ProductSaleController@getExport')->name('sales.product.export');
            Route::any('product_sales/data', 'ProductSaleController@anyData')->name('sales.product.data');
            Route::post('product_sales/{saleId}/delete', 'ProductSaleController@postStornoProductSale')
                ->name('sales.product.storno');
            Route::post('product_sales/{saleId}/status', 'ProductSaleController@postUpdateStatus')
                ->name('sales.product.status');
            // Canceled product sales (storno product sales)
            Route::get('product_sales/storno', 'ProductSaleController@getStornoIndex')
                ->name('sales.product.storno.index');
            Route::any('product_sales/storno/data', 'ProductSaleController@anyStornoData')
                ->name('sales.product.storno.data');

            // Tickets
            Route::get('tickets', 'TicketController@getIndex')->name('tickets.index');
            Route::any('tickets/data', 'TicketController@anyData')->name('tickets.data');
            Route::get('tickets/{ticketId}', 'TicketController@getEdit')->name('tickets.edit');
            Route::post('tickets/{ticketId}', 'TicketController@postUpdate')->name('tickets.update');
            Route::post('tickets/{ticketId}/delete', 'TicketController@softDelete')->name('tickets.delete');
            Route::get('tickets/export/{format}', 'TicketController@getExport')->name('tickets.export');

            // Invoices
            Route::get('invoices/', 'InvoiceController@getIndex')->name('invoices.index');
            Route::any('invoices/data', 'InvoiceController@anyData')->name('invoices.data');
            Route::get('invoices/create', 'InvoiceController@getCreate')->name('invoices.create');
            Route::get('invoices/{invoiceId}', 'InvoiceController@getShow')->name('invoices.show');
            Route::get('invoices/{invoiceId}/data', 'InvoiceController@invoiceData')->name('invoices.show.data');
            Route::get('invoices/{invoiceId}/export/{format}', 'InvoiceController@getExport')->name('invoices.export');
            Route::post('invoices/', 'InvoiceController@postCreate')->name('invoices.store');
            Route::post('invoices/{invoiceId}/delete', 'InvoiceController@postStornoInvoice')->name('invoices.storno');

            // Ticket types
            Route::get('tickettypes/all', 'TicketTypeController@allTicketTypes')->name('tickettypes.all');
            Route::delete('tickettypes/{ticketTypeId}', 'TicketTypeController@delete')->name('tickettypes.delete');
            
            // Ticket prices
            Route::delete('ticketprices/{ticketPriceId}', 'TicketPriceController@delete')->name('ticketprices.delete');

            // Hauls
            Route::get('hauls', 'HaulsController@getIndex')->name('hauls.index');
            Route::any('hauls/data', 'HaulsController@anyData')->name('hauls.data');
            Route::get('hauls/create', 'HaulsController@getCreate')->name('hauls.create');
            Route::get('hauls/{haulId}', 'HaulsController@getEdit')->name('hauls.edit');
            Route::get('hauls/export/{format}', 'HaulsController@getExport')->name('hauls.export');
            Route::post('hauls', 'HaulsController@postStore')->name('hauls.store');
            Route::put('hauls/{haulId}', 'HaulsController@postUpdate')->name('hauls.update');
            Route::delete('hauls/{haulId}', 'HaulsController@delete')->name('hauls.delete');

            // Api Docs
            Route::get('apidoc', 'ApiDocController@getIndex')->name('apidoc.index');
            Route::post('apidoc', 'ApiDocController@refresh')->name('apidoc.refresh');

            // Reports
            Route::group(['prefix' => 'reports', 'namespace' => 'Reports', 'as' => 'reports.'], function () {
                // Hauls
                Route::get('hauls', 'HaulController@getIndex')->name('hauls.index');
                Route::any('hauls/data', 'HaulController@anyData')->name('hauls.data');
                Route::get('hauls/{haulId}', 'HaulController@getEdit')->name('hauls.edit');
                Route::post('hauls/{haulId}', 'HaulController@postUpdate')->name('hauls.update');

                // Comments
                Route::get('comments', 'CommentController@getIndex')->name('comments.index');
                Route::any('comments/data', 'CommentController@anyData')->name('comments.data');
                Route::get('comments/{commentId}', 'CommentController@getEdit')->name('comments.edit');
                Route::post('comments/{commentId}', 'CommentController@postUpdate')->name('comments.update');
            });

            // Draft
            Route::get('draft', 'DraftController@index')->name('draft.index');
            Route::any('draft/data/{type?}', 'DraftController@anyData')->name('draft.data');
            Route::get('draft/{type}/{typeId}', 'DraftController@getEdit')->name('draft.edit');
            Route::post('draft/{type}/{typeId}', 'DraftController@postUpdate')->name('draft.update');
            Route::delete('draft/{type}/{typeId}', 'DraftController@delete')->name('draft.delete');


            // Content
            Route::group(['prefix' => 'content', 'namespace' => 'Content', 'as' => 'content.'], function () {
                // Fishes
                Route::get('fishes', 'FishController@getIndex')->name('fishes.index');
                Route::any('fishes/data', 'FishController@anyData')->name('fishes.data');
                Route::get('fishes/create', 'FishController@getCreate')->name('fishes.create');
                Route::get('fishes/{fishId}/data', 'FishController@show')->name('fishes.show');
                Route::get('fishes/{fishId}', 'FishController@getEdit')->name('fishes.edit');
                Route::post('fishes', 'FishController@postCreate')->name('fishes.store');
                Route::post('fishes/{fishId}', 'FishController@postUpdate')->name('fishes.update');
                Route::delete('fishes/{fishId}', 'FishController@delete')->name('fishes.delete');
                Route::put('fishes/{fishId}', 'FishController@delete')->name('fishes.delete');
                Route::post('fishes/{fishId}/publish', 'FishController@publish')->name('fishes.publish');

                // Fishing Methods
                Route::group(['prefix' => 'fishing-methods'], function () {
                    Route::get('/', 'FishingMethodController@getIndex')->name('fishing-methods.index');
                    Route::any('data', 'FishingMethodController@anyData')->name('fishing-methods.data');
                    Route::get('get-form', 'FishingMethodController@getDataForm')->name('fishing-methods.get-form');
                    Route::get('create', 'FishingMethodController@getCreate')->name('fishing-methods.create');
                    Route::post('create', 'FishingMethodController@create')->name('fishing-methods.store');
                    Route::get('{fishingMethodId}', 'FishingMethodController@getEdit')->name('fishing-methods.edit');
                    Route::post('{fishingMethodId}', 'FishingMethodController@update')->name('fishing-methods.update');
                    Route::delete('{fishingMethodId}', 'FishingMethodController@delete')->name('fishing-methods.delete');
                    Route::post('{fishingMethodId}', 'FishingMethodController@restore')->name('fishing-methods.restore');
                });

                // Fishes' Gallery
                Route::get('fishes/{fishId}/gallery', 'FishGalleryController@getList')
                    ->name('fishes.gallery.list');
                Route::post('fishes/{fishId}/gallery/upload', 'FishGalleryController@postUpload')
                    ->name('fishes.gallery.upload');
                Route::get('fishes/{fishId}/gallery/{pictureId}', 'FishGalleryController@getEdit')
                    ->name('fishes.gallery.edit');
                Route::post('fishes/{fishId}/gallery/{pictureId}', 'FishGalleryController@postSave')
                    ->name('fishes.gallery.save');
                Route::get('fishes/{fishId}/gallery/{pictureId}/delete', 'FishGalleryController@getDelete')
                    ->name('fishes.gallery.delete');

                // Techniques
                Route::get('techniques', 'TechniqueController@getIndex')->name('techniques.index');
                Route::any('techniques/data', 'TechniqueController@anyData')->name('techniques.data');
                Route::get('techniques/create', 'TechniqueController@getCreate')->name('techniques.create');
                Route::get('techniques/{techniqueId}/data', 'TechniqueController@show')->name('techniques.show');
                Route::get('techniques/{techniqueId}', 'TechniqueController@getEdit')->name('techniques.edit');
                Route::post('techniques', 'TechniqueController@postCreate')->name('techniques.store');
                Route::post('techniques/{techniqueId}', 'TechniqueController@postUpdate')->name('techniques.update');
                Route::delete('techniques/{techniqueId}', 'TechniqueController@delete')->name('techniques.delete');
                Route::post('techniques/{techniqueId}/publish', 'TechniqueController@publish')->name('techniques.publish');

                // Techniques' Gallery
                Route::get('techniques/{techId}/gallery', 'TechniqueGalleryController@getList')
                    ->name('techniques.gallery.list');
                Route::post('techniques/{techId}/gallery/upload', 'TechniqueGalleryController@postUpload')
                    ->name('techniques.gallery.upload');
                Route::get('techniques/{techId}/gallery/{pictureId}', 'TechniqueGalleryController@getEdit')
                    ->name('techniques.gallery.edit');
                Route::post('techniques/{techId}/gallery/{pictureId}', 'TechniqueGalleryController@postSave')
                    ->name('techniques.gallery.save');
                Route::get('techniques/{techId}/gallery/{pictureId}/delete', 'TechniqueGalleryController@getDelete')
                    ->name('techniques.gallery.delete');
                Route::get('techniques/{techId}/gallery/{pictureId}/detach', 'TechniqueGalleryController@getDetach')
                    ->name('techniques.gallery.detach');

                // Countries
                Route::get('countries', 'CountryController@getIndex')->name('countries.index');
                Route::any('countries/data', 'CountryController@anyData')->name('countries.data');
                Route::get('countries/create', 'CountryController@getCreate')->name('countries.create');
                Route::get('countries/{countryId}/data', 'CountryController@show')->name('countries.show');
                Route::get('countries/{countryId}', 'CountryController@getEdit')->name('countries.edit');
                Route::post('countries', 'CountryController@postCreate')->name('countries.store');
                Route::post('countries/{countryId}', 'CountryController@postUpdate')->name('countries.update');
                Route::delete('countries/{countryId}', 'CountryController@delete')->name('countries.delete');

                // States
                Route::get('states', 'StateController@getIndex')->name('states.index');
                Route::any('states/data', 'StateController@anyData')->name('states.data');
                Route::get('states/all', 'StateController@getAllStates')->name('states.all');
                Route::get('states/create', 'StateController@getCreate')->name('states.create');
                Route::get('states/{stateId}/data', 'StateController@show')->name('states.show');
                Route::get('states/{stateId}', 'StateController@getEdit')->name('states.edit');
                Route::post('states', 'StateController@postCreate')->name('states.store');
                Route::post('states/{stateId}', 'StateController@postUpdate')->name('states.update');
                Route::delete('states/{stateId}', 'StateController@delete')->name('states.delete');

                // State fishes
                Route::get('states/{stateId}/fishes', 'StateFishController@getIndex')
                    ->name('states.fishes.index');
                Route::any('states/{stateId}/fishes/data', 'StateFishController@anyData')
                    ->name('states.fishes.data');
                Route::get('states/{stateId}/fishes/create', 'StateFishController@getCreate')
                    ->name('states.fishes.create');
                Route::get('states/{stateId}/fishes/{fishId}/data', 'StateFishController@show')
                    ->name('states.fishes.show');
                Route::get('states/{stateId}/fishes/{fishId}', 'StateFishController@getEdit')
                    ->name('states.fishes.edit');
                Route::post('states/{stateId}/fishes', 'StateFishController@postCreate')
                    ->name('states.fishes.store');
                Route::post('states/{stateId}/fishes/{fishId}', 'StateFishController@postUpdate')
                    ->name('states.fishes.update');
                Route::delete('states/{stateId}/fishes/{fishId}', 'StateFishController@delete')
                    ->name('states.fishes.delete');

                // Regions
                Route::get('regions', 'RegionController@getIndex')->name('regions.index');
                Route::any('regions/data', 'RegionController@anyData')->name('regions.data');
                Route::get('regions/create', 'RegionController@getCreate')->name('regions.create');
                Route::get('regions/{regionId}/data', 'RegionController@show')->name('regions.show');
                Route::get('regions/{regionId}', 'RegionController@getEdit')->name('regions.edit');
                Route::post('regions', 'RegionController@postCreate')->name('regions.store');
                Route::post('regions/{regionId}', 'RegionController@postUpdate')->name('regions.update');
                Route::delete('regions/{regionId}', 'RegionController@delete')->name('regions.delete');

                // Cities
                Route::get('cities', 'CityController@getIndex')->name('cities.index');
                Route::any('cities/data', 'CityController@anyData')->name('cities.data');
                Route::get('cities/create', 'CityController@getCreate')->name('cities.create');
                Route::get('cities/{cityId}/data', 'CityController@show')->name('cities.show');
                Route::get('cities/{cityId}', 'CityController@getEdit')->name('cities.edit');
                Route::post('cities', 'CityController@postCreate')->name('cities.store');
                Route::post('cities/{cityId}', 'CityController@postUpdate')->name('cities.update');
                Route::delete('cities/{cityId}', 'CityController@delete')->name('cities.delete');

                // Parent areas
                Route::get('parentareas', 'ParentAreasController@getIndex')->name('parentareas.index');
                Route::any('parentareas/data', 'ParentAreasController@anyData')->name('parentareas.data');
                Route::get('parentareas/create', 'ParentAreasController@getCreate')->name('parentareas.create');
                Route::get('parentareas/{areaId}/data', 'ParentAreasController@show')->name('parentareas.show');
                Route::get('parentareas/{areaId}', 'ParentAreasController@getEdit')->name('parentareas.edit');
                Route::post('parentareas', 'ParentAreasController@postCreate')->name('parentareas.store');
                Route::post('parentareas/{areaId}', 'ParentAreasController@postUpdate')->name('parentareas.update');
                Route::delete('parentareas/{areaId}', 'ParentAreasController@delete')->name('parentareas.delete');

                // Point of interests
                Route::get('poi', 'PoiController@getIndex')->name('poi.index');
                Route::any('poi/data', 'PoiController@anyData')->name('poi.data');
                Route::get('poi/create', 'PoiController@getCreate')->name('poi.create');
                Route::get('poi/{poiId}', 'PoiController@getEdit')->name('poi.edit');
                Route::post('poi', 'PoiController@postCreate')->name('poi.store');
                Route::post('poi/{poiId}', 'PoiController@postUpdate')->name('poi.update');
                Route::delete('poi/{poiId}', 'PoiController@delete')->name('poi.delete');
                Route::post('poi/{poiId}/publish', 'PoiController@publish')->name('poi.publish');

                // Landingpages
                Route::get('lp', 'LandingPageController@getIndex')->name('lp.index');
                Route::any('lp/data', 'LandingPageController@anyData')->name('lp.data');
                Route::get('lp/create', 'LandingPageController@getCreate')->name('lp.create');
                Route::get('lp/areas', 'LandingPageController@getAllAreas')->name('lp.areas.all');
                Route::get('lp/{lpId}/areas', 'LandingPageController@getAreas')->name('lp.areas.index');
                Route::get('lp/{lpId}/data', 'LandingPageController@show')->name('lp.show');
                Route::get('lp/{lpId}', 'LandingPageController@getEdit')->name('lp.edit');
                Route::post('lp', 'LandingPageController@postCreate')->name('lp.store');
                Route::post('lp/{lpId}', 'LandingPageController@postUpdate')->name('lp.update');
                Route::delete('lp/{lpId}', 'LandingPageController@delete')->name('lp.delete');

                // Legal documents
                Route::get('legaldocs', 'LegalDocumentController@getIndex')->name('legaldocs.index');
                Route::any('legaldocs/data', 'LegalDocumentController@anyData')->name('legaldocs.data');
                Route::get('legaldocs/create', 'LegalDocumentController@getCreate')->name('legaldocs.create');
                Route::get('legaldocs/{docId}', 'LegalDocumentController@getEdit')->name('legaldocs.edit');
                Route::post('legaldocs', 'LegalDocumentController@postCreate')->name('legaldocs.store');
                Route::post('legaldocs/{docId}', 'LegalDocumentController@postUpdate')->name('legaldocs.update');
                Route::delete('legaldocs/{docId}', 'LegalDocumentController@delete')->name('legaldocs.delete');

                Route::get('legaldocs/{docId}/managers', 'DocumentManagerController@getIndex')
                    ->name('legaldocs.managers.index');
                Route::any('legaldocs/{docId}/managers/data', 'DocumentManagerController@anyData')
                    ->name('legaldocs.managers.data');
                Route::post('legaldocs/{docId}/managers/add', 'DocumentManagerController@postAdd')
                    ->name('legaldocs.managers.add');
                Route::delete('legaldocs/{docId}/managers/{managerId}', 'DocumentManagerController@delete')
                    ->name('legaldocs.managers.delete');
            });
        });
    });
});
