<?php

Route::group(['as' => 'pos::'], function () {
    Route::group(['namespace' => 'Admin\Controllers\Auth'], function () {
        // PoS login
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login')->name('login');
        Route::get('logout', 'LoginController@logout')->name('logout');
        // PoS password reset
        Route::get('reset', 'ResetPasswordController@getPasswordReset')->name('reset');
        Route::post('reset', 'ResetPasswordController@postPasswordReset')->name('reset');
    });

    Route::group(['namespace' => 'PoS\Controllers', 'middleware' => ['auth', 'admin']], function () {
        Route::get('/', '\App\Admin\Controllers\HomeController@redirectPosTickets')->name('home');

        Route::get('/tickets', 'TicketController@getIndex')->name('tickets.index');
        Route::post('/tickets', 'TicketController@postNewTicket')->name('tickets.new');
        Route::get('/tickets/{ticketId}/pdf', 'TicketController@getTicketPdf')->name('tickets.pdf');
        Route::get('/tickets/areas', 'TicketController@getAreas')->name('tickets.areas');
        Route::get('/tickets/types/{areaId}', 'TicketController@getTicketTypes')->name('tickets.types');
        Route::get('/tickets/prices/{ticketTypeId}', 'TicketController@getTicketPrices')->name('tickets.prices');
        Route::get('/tickets/dates/{ticketPriceId}', 'TicketController@getValidDates')->name('tickets.dates');
        Route::get('/tickets/last', 'TicketController@getLastUserData')->name('tickets.last');
        Route::get('/tickets/licenses/{ticketTypeId}', 'TicketController@getLicenseTypes')->name('tickets.licenses');
        Route::get('/tickets/fisher/{fisherId}', 'TicketController@getUserByFisherId')->name('tickets.fisher');

        Route::get('products', 'ProductController@getIndex')->name('products.index');
        Route::get('products/data', 'ProductController@anyData')->name('products.data');
        Route::get('products/{productId}', 'ProductController@getShow')->name('products.show');
        Route::post('products/buy', 'ProductController@postNewSale')->name('products.buy');

        Route::get('/sales', 'SalesController@getIndex')->name('sales.index');
        Route::any('/sales/data', 'SalesController@anyData')->name('sales.data');
        Route::get('/sales/export/{format}/ticket', 'SalesController@getTicketExport')
            ->name('sales.export.ticket');
        Route::get('/sales/export/{format}/product', 'SalesController@getProductSaleExport')
            ->name('sales.export.product');
        Route::post('/sales/{ticketId}/delete/ticket', 'SalesController@softTicketDelete')
            ->name('sales.ticket.storno');
        Route::post('/sales/{saleId}/delete/product', 'SalesController@softProductSaleDelete')
            ->name('sales.sale.storno');
        // Canceled Tickets (storno ticket)
        Route::get('/sales/storno', 'SalesController@getStornoIndex')->name('sales.storno.index');
        Route::any('/sales/storno/data', 'SalesController@anyStornoData')->name('sales.storno.data');
    });
});
