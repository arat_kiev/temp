<?php

/* Ping - Pong */
Route::get('v1/heartbeat', 'Api1\Controllers\HeartbeatController@ping')->name('v1.heartbeat');

/* PDF views */
Route::group([
    'as' => 'pdf.',
    'prefix' => 'pdf',
    'namespace' => 'Http\Controllers',
    'middleware' => [
        'api.key',
        'local'
    ]], function () {

    Route::get('ticket/{ticket}', 'PdfController@ticket')->name('ticket');
    Route::get('product/{product}', 'PdfController@product')->name('product');
    Route::get('invoice/{invoice}', 'PdfController@invoice')->name('invoice');
});

/* Activate user account (verify email address) */
Route::get('v1/auth/activate/{activation_code}', 'Api1\Controllers\AuthController@activate')->name('v1.auth.activate');
Route::get('v1/auth/password/new/{token}', 'Api1\Controllers\AuthController@getChangePassword')->name('v1.auth.password_new');
Route::post('v1/auth/password/new/{token}', 'Api1\Controllers\AuthController@postChangePassword')->name('v1.auth.password_save');
Route::post('v1/auth/email/update', 'Api1\Controllers\AuthController@postChangeEmail')->name('v1.auth.email.update');

Route::post('v1/payment/response', 'Api1\Controllers\PaymentController@response')->name('v1.payment.response');

/* Stats for managers - external API */
Route::get('v1/areas/{area_id}/stats/catch', 'Api1\Controllers\StatsController@getHauls')->name('v1.areas.stats.catch');
Route::get('v1/areas/{area_id}/stats/sales', 'Api1\Controllers\StatsController@getSales')->name('v1.areas.stats.sales');

/* API Version 1 */
Route::group([
    'as' => 'v1.',
    'prefix' => 'v1',
    'namespace' => 'Api1\Controllers',
    'middleware' => [
        'api.key',
        'cache.response',
    ]], function () {

    /* Image Upload */
    Route::post('image/upload', 'ImageController@upload')->name('imageupload');
    Route::post('image/upload/string', 'ImageController@uploadBase64')->name('imageupload.string');

    /* Authentication */
    Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
        Route::get('refresh', 'AuthController@refresh')->name('refresh');
        Route::post('signin', 'AuthController@signin')->name('signin');
        Route::post('signup', 'AuthController@signup')->name('signup');
        Route::post('password/change', 'AuthController@change')->name('password_change');
        Route::get('password/check', 'AuthController@oldPasswordCheck')->name('old_password_check');
        Route::post('password/reset', 'AuthController@reset')->name('password_reset');
        Route::post('{provider}', 'AuthController@socialLogin')->name('social_login');
    });

    // Statistics
    Route::group(['prefix' => 'statistics', 'namespace' => 'Statistics', 'as' => 'statistics.'], function () {
        Route::get('catches', 'CatchController@index')->middleware('jwt.auth');
    });

    // Comments
    Route::post('comments/{commentId}/report', 'CommentController@report')->name('comments.report')->middleware('jwt.auth');
    Route::get('comments/{entity}/{id}', 'CommentController@index')->name('comments.index');
    Route::post('comments/{entity}/{id}', 'CommentController@create')->name('comments.store')->middleware('jwt.auth');

    Route::post('payment/init', 'PaymentController@init')->name('payment.init')->middleware('jwt.auth');

    /* API Endpoints */
    $withoutForms = ['except' => ['create', 'edit']];
    $viewOnly = ['only' => ['index', 'show']];

    Route::resource('area-rating-categories', 'AreaRatingCategoryController', ['only' => ['index']]);

    Route::get('areas/map', 'AreaController@map')->name('areas.map');
    Route::get('areas/simple', 'AreaController@simpleIndex')->name('areas.simple');
    Route::resource('areas', 'AreaController', $withoutForms);
    Route::post('areas/{areaId}/rate', 'AreaRatingController@store')->name('areas.rate');
    Route::resource('parentareas', 'ParentAreasController', $withoutForms);
    Route::resource('areas.pictures', 'Area\PictureController', $withoutForms);
    Route::resource('areatypes', 'AreaTypeController', $withoutForms);
    Route::resource('lp', 'LandingPageController', $withoutForms);

    Route::resource('tickets', 'TicketController', $withoutForms);

    Route::group(['prefix' => 'ticketprices/{ticketprices}'], function () {
        Route::get('/dates', 'TicketPriceController@getDates')->name('ticketprices.dates');
    });

    Route::resource('ticketprices', 'TicketPriceController', $withoutForms);
    Route::resource('tickettypes', 'TicketTypeController', $withoutForms);

    Route::resource('products', 'ProductController', $viewOnly);
    Route::resource('product_categories', 'ProductCategoryController', $viewOnly);
    Route::resource('product_stocks', 'ProductStockController', $viewOnly);
    Route::resource('stocks', 'StockController', $viewOnly);

    Route::group(['prefix' => 'users/{users}/tickets/{tickets}'], function () {
        Route::get('checkins', 'Profile\TicketController@getCheckins')->name('users.checkins.index');
        Route::post('checkins', 'Profile\TicketController@postCheckins')->name('users.checkins.create');
        Route::patch('checkins/{checkinId}', 'Profile\TicketController@updateCheckins')->name('users.checkins.update');

    });

    Route::resource('users', 'UserController', $withoutForms);
    Route::resource('users.licenses', 'Profile\LicenseController', $withoutForms);
    Route::get('users/{userId}/check_tickets_intersection', 'Profile\TicketController@checkDateIntersection')->name('users.check_tickets_intersection');
    Route::post('users/{userId}/tickets/claim', 'Profile\TicketController@attachTickets')->name('users.claim-tickets');
    Route::resource('users.tickets', 'Profile\TicketController', $withoutForms);
    Route::get('users/{userId}/tickets/{ticketId}/pdf', 'Profile\TicketController@pdf')
        ->name('users.tickets.pdf');
    Route::get('users/{userId}/product_sales/{productSaleId}/pdf', 'Profile\ProductSaleController@pdf')
        ->name('users.product_sales.pdf');
    Route::resource('users.product_sales', 'Profile\ProductSaleController', $withoutForms);
    Route::resource('users.hauls', 'Profile\HaulController', $withoutForms);
    Route::get('users/{userId}/hauls/{haulId}/check', 'Profile\HaulController@check');
    Route::resource('users.favorites', 'Profile\FavoriteController', $withoutForms);
    Route::resource('users.fishes', 'Profile\FishController', $withoutForms);
    Route::resource('users.techniques', 'Profile\TechniqueController', $withoutForms);
    Route::resource('users.memberAreas', 'Profile\MemberController', $withoutForms);
    Route::resource('users.organizations', 'Profile\OrganizationController', $viewOnly);
    Route::resource('users.follows', 'Profile\UserFollowsController', ['only' => ['index', 'show', 'store', 'destroy']]);
    Route::resource('users.followed', 'Profile\UserFollowsController', ['only' => ['index', 'show']]);

    Route::group(['prefix' => 'users/{user_id}/profile', 'namespace' => 'Profile', 'where' => ['user_id' => '[\d]+']], function () {
        Route::get('/', 'UserController@profile')->name('users.profile');
        Route::get('hauls', 'UserController@hauls')->name('users.profile.hauls');
        Route::get('favorites', 'UserController@areas')->name('users.profile.favorites');
        Route::get('followers', 'UserController@followers')->name('users.profile.followers');
    });

    Route::group(['prefix' => 'users/picture', 'where' => ['pictureType' => 'avatar|hero']], function () {
        Route::post('{picture_id}/{pictureType}', 'UserController@addHeroAvatarPicture')
            ->where('picture_id', '[\d]+')
            ->name('users.picture.store');
        Route::delete('{pictureType}', 'UserController@rmHeroAvatarPicture')->name('users.picture.remove');
    });

    Route::group(['prefix' => 'basket', 'middleware' => ['jwt.auth']], function () {
        Route::get('/', 'BasketController@list')->name('basket.list');
        Route::post('/', 'BasketController@add')->name('basket.add');
        Route::delete('/', 'BasketController@clear')->name('basket.clear');
        Route::patch('/{id}', 'BasketController@update')->name('basket.update');
        Route::post('/checkout', 'BasketController@checkout')->name('basket.checkout');
    });

    Route::resource('roles', 'RoleController', $withoutForms);
    Route::resource('permissions', 'PermissionController', $withoutForms);

    Route::resource('licenses', 'LicenseController', $withoutForms);
    Route::resource('licensetypes', 'LicenseTypeController', $withoutForms);

    Route::resource('hauls', 'HaulController', $withoutForms);
    Route::get('hauls/{id}/check', 'HaulController@check');
    Route::post('hauls/{id}/vote/{type}', 'HaulController@vote')->name('hauls.vote');
    Route::post('hauls/{id}/report', 'HaulController@report')->name('hauls.report');

    Route::resource('countries', 'CountryController', $withoutForms);
    Route::resource('states', 'StateController', $withoutForms);
    Route::resource('regions', 'RegionController', $withoutForms);
    Route::resource('cities', 'CityController', $withoutForms);

    Route::resource('fishes', 'FishController', $withoutForms);
    Route::resource('fish-categories', 'FishCategoryController', $withoutForms);
    Route::resource('techniques', 'TechniqueController', $withoutForms);
    Route::resource('fishingmethods', 'FishingMethodController', $withoutForms);

    Route::resource('rentals', 'RentalController', $withoutForms);
    Route::resource('managers', 'ManagerController', $withoutForms);
    Route::resource('organizations', 'OrganizationController', $withoutForms);
    Route::resource('organizations.news', 'NewsController', $viewOnly);

    Route::resource('resellers', 'ResellerController', $withoutForms);
    Route::resource('resellers.areas', 'Reseller\AreaController', ['only' => ['index']]);
    Route::resource('resellers.products', 'Reseller\ProductController', ['only' => ['index']]);
    Route::resource('resellers.ticketTypes', 'Reseller\TicketTypeController', ['only' => ['index']]);
    Route::resource('resellers.ticketPrices', 'Reseller\TicketPriceController', ['only' => ['index']]);
    Route::resource('resellers.ticketDates', 'Reseller\TicketDateController', ['only' => ['index']]);
    Route::get('resellers/{resellers}/tickets/{tickets}/pdf', 'Reseller\TicketController@pdf')
        ->name('resellers.tickets.pdf');
    Route::get('resellers/{resellers}/invoices/{invoices}/pdf', 'Reseller\InvoiceController@pdf')
        ->name('resellers.invoices.pdf');
    Route::resource('resellers.tickets', 'Reseller\TicketController', ['only' => ['index', 'show', 'store', 'destroy']]);
    Route::resource('resellers.productsales', 'Reseller\ProductSaleController', ['only' => ['index', 'show', 'store', 'destroy']]);
    Route::get('resellers/{resellers}/productsales/{products}/pdf', 'Reseller\ProductSaleController@pdf')
        ->name('resellers.productsales.pdf');
    Route::resource('resellers.invoices', 'Reseller\InvoiceController', $viewOnly);

    Route::get('resellers/{resellers}/fishers/{fisher_id}', 'Reseller\TicketController@getUserByFisherId')->name('reseller.fisher');

    Route::resource('galleries', 'GalleryController', $withoutForms);
    Route::resource('pictures', 'PictureController', $withoutForms);

    Route::resource('testimonials', 'TestimonialController', $withoutForms);

    Route::resource('quotaunits', 'QuotaUnitController', $withoutForms);
    Route::resource('checkinunits', 'CheckinUnitController', $withoutForms);
    Route::resource('ticketcategories', 'TicketCategoryController', $withoutForms);

    Route::resource('poi', 'PointsOfInterestController', $withoutForms);

    Route::resource('promotions', 'PromoController', $withoutForms);
    Route::put('coupons', 'CouponController@useCoupon')->name('coupons.use');
    Route::post('coupons/transfer', 'CouponController@storeTransfer')->name('coupons.transfer.store');
    Route::put('coupons/transfer', 'CouponController@useTransfer')->name('coupons.transfer.use');
    Route::resource('coupons', 'CouponController', $withoutForms);

    Route::get('notifications', 'NotificationController@index')->name('index.notifs');
    Route::get('notifications/archive', 'NotificationController@indexArchive')->name('index.archive.notifs');

    Route::get('sitemaps/{type}/{subType?}', 'SitemapController@show')->name('sitemaps.show');

    /**
     * Inspections
     */
    Route::group(['prefix' => 'tickets/{ticketId}'], function () {
        Route::post('inspect', 'Inspect\TicketController@newInspection')
            ->name('inspection.createInspection');
        Route::patch('inspections/{inspectionId}', 'Inspect\TicketController@updateInspection')
            ->name('inspection.updateInspection');
        Route::get('inspections', 'Inspect\TicketController@getAllInspections')
            ->name('inspection.allInspections');
        Route::get('inspections/{inspectionId}', 'Inspect\TicketController@getInspection')
            ->name('inspection.getInspection');
        Route::get('inspections/{inspectionId}/hauls', 'HaulController@getHauls')
            ->name('inspection.getHauls');
        Route::post('inspections/{inspectionId}/hauls', 'HaulController@storeHauls')
            ->name('inspection.storeHauls');
    });

    Route::group(['prefix' => 'inspectors/{inspectorId}'], function () {
        Route::get('inspections', 'Inspect\InspectionController@getInspections');
        Route::get('areas', 'Inspect\AreaController@getAreas');
        Route::get('observations', 'Inspect\ObservationController@getObservations');
        Route::get('areas', 'Inspect\AreaController@getInspectionAreas');
        Route::get('tickets', 'Inspect\TicketController@getInspectionAreasTickets');

        Route::group(['prefix' => 'tours'], function () {
            Route::get('/', 'Inspect\TourController@getTours');
            Route::post('/', 'Inspect\TourController@newTour');
            Route::group(['prefix' => '{tourId}'], function () {
                Route::patch('/', 'Inspect\TourController@endTour');
                Route::post('observations', 'Inspect\ObservationController@newObservation');
                Route::patch('observations/{observationId}', 'Inspect\ObservationController@updateObservation');
            });
        });
    });
    /**
     * Inspections end
     */
});
