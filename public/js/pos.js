$(document).ready(function () {
    //--- AJAX Spinner
    $(document).ajaxStart(function () {
        $.LoadingOverlay('show', {
            image: '',
            fontawesome: 'fa fa-spinner fa-pulse',
            custom: '<h2 class="noselect">Daten werden geladen ...</h2>'
        });
    });

    $(document).ajaxStop(function () {
        window.setTimeout(function () {
            $.LoadingOverlay('hide');
        }, 500);
    });
    //--- End AJAX Spinner

    //--- Global Variables
    var dateSelect = $('#date-select');
    var validDates = [];
    var selDate;

    //--- Scroll to helper
    function scrollTo(el, offset)
    {
        $('html, body').animate({
            scrollTop: $(el).offset().top - offset
        }, 1000);
    }

    //--- Enhanced country select
    $('#country_id').chosen({
        width: '100%'
    });

    //--- Button Callbacks
    $('#btn-cal-today').click(function (evt) {
        evt.preventDefault();
        selDate = moment();
        dateSelect.datepicker('setDate', selDate.toDate());
        setQuotaInfo(selDate.format('DD.MM.YYYY'));
        setupStep5();
    });

    $('#btn-cal-tomorrow').click(function (evt) {
        evt.preventDefault();
        selDate = moment().add('1', 'day');
        dateSelect.datepicker('setDate', selDate.toDate());
        setQuotaInfo(selDate.format('DD.MM.YYYY'));
        setupStep5();
    });

    $('#btn-load-fisher').click(function (evt) {
        evt.preventDefault();

        var fisherId = $('#fisher_id').val();
        $.get('/tickets/fisher/' + fisherId).done(function (res) {
            var birthday = moment(res.birthday);

            $('input#first_name').val(res.first_name);
            $('input#last_name').val(res.last_name);
            $('input#birth_day').val(birthday.date());
            $('input#birth_month').val(birthday.month()+1);
            $('input#birth_year').val(birthday.year());
            $('input#email').val(res.email);
            $('input#phone').val(res.phone);
            $('input#street').val(res.street);
            $('input#post_code').val(res.post_code);
            $('input#city').val(res.city);
            $('input#authorization_id').val(res.authorization_id);
            $('input#issuing_authority').val(res.issuing_authority);
            $('select#country_id option[value="' + res.country_id + '"]').prop('selected', true).trigger("chosen:updated");

            if (res.licenses) {
                res.licenses.forEach(function (license) {
                    $('input[name="licenses[]"][value="' + license + '"]').prop('checked', true);
                });
            }
        });
    });

    $('#btn-load-user').click(function (evt) {
        evt.preventDefault();

        $.get('/tickets/last').done(function (res) {
            var birthday = moment(res.birthday);

            $('input#first_name').val(res.first_name);
            $('input#last_name').val(res.last_name);
            $('input#birth_day').val(birthday.date());
            $('input#birth_month').val(birthday.month()+1);
            $('input#birth_year').val(birthday.year());
            $('input#email').val(res.email);
            $('input#phone').val(res.phone);
            $('input#street').val(res.street);
            $('input#post_code').val(res.post_code);
            $('input#city').val(res.city);
            $('input#authorization_id').val(res.authorization_id);
            $('input#issuing_authority').val(res.issuing_authority);
            $('select#country_id option[value="' + res.country_id + '"]').prop('selected', true).trigger("chosen:updated");

            if (res.licenses) {
                res.licenses.forEach(function (license) {
                    $('input[name="licenses[]"][value="' + license + '"]').prop('checked', true);
                });
            }
        });
    });
    //--- End Button Callbacks

    function getQuotaObject(dates, date)
    {
        var year = date.year();
        var month = date.month() + 1;
        var day = date.date();

        if (dates.hasOwnProperty(year)) {
            if (dates[year].hasOwnProperty(month)) {
                if (dates[year][month].hasOwnProperty(day)) {
                    return dates[year][month][day];
                }
            }
        }

        return {
            "type": 'Keine',
            "max": 'Keine',
            "left": 'Keine'
        };
    }

    function setGeneralQuota()
    {
        var quotaToday = getQuotaObject(validDates, moment());
        var quotaTomorrow = getQuotaObject(validDates, moment().add(1, 'days'));

        $('#btn-cal-today').prop('disabled', quotaToday.left === 'Keine' || quotaToday.left === 0);
        $('#btn-cal-tomorrow').prop('disabled', quotaTomorrow.left === 'Keine' || quotaTomorrow.left === 0);

        $('#quota-count-today').text((quotaToday.left === -1 ? 'Unbegrenzt' : quotaToday.left) + ' verfügbar');
        $('#quota-count-tomorrow').text((quotaTomorrow.left === -1 ? 'Unbegrenzt' : quotaTomorrow.left) + ' verfügbar');
    }

    function setQuotaInfo(date)
    {
        if (date) {
            selDate = moment(date, 'DD-MM-YYYY');

            quota = getQuotaObject(validDates, selDate);
            var quotaText = quota.max + (quota.max === 1 ? ' Karte ' : ' Karten ') + quota.type;

            $('#quota-count-selected').text((quota.left === -1 ? 'Unbegrenzt' : quota.left) + ' verfügbar');
            $('#quota-selected-date').text(selDate.format('DD.MM.YYYY'));
            $('#quota-type').text(quota.type === null ? 'Unbegrenzt' : quotaText);
        } else {
            $('#quota-count-selected,#quota-selected-date,#quota-type').text('---');
        }
    }

    $.get('/tickets/areas').done(function (res) {
        var areaList = $('#area-list');
        areaList.empty();

        res.forEach(function (area) {
            areaList.append(templateArea(area));
        });

        setupStep1();

        $('input[name="area-select"]').change(function (evt) {
            $(this).closest('ul').find('label').addClass('inactive').removeClass('active');
            $(this).parent().toggleClass('active inactive');

            var areaId = $(this).val();

            $.get('/tickets/types/' + areaId).done(function (res) {
                var ticketList = $('#ticket-list');
                var hasTickets = false;
                ticketList.empty();

                res.forEach(function (ticketGroup) {
                    if (ticketGroup.data.length > 0) {
                        ticketList.append(templateTicketGroup(ticketGroup));
                        hasTickets = true;
                    }
                });

                if (!hasTickets) {
                    ticketList.append(templateNoTickets());
                }

                setupStep2();

                $('input[name="ticket-select"]').change(function (evt) {
                    $(this).closest('ul').find('label').addClass('inactive').removeClass('active');
                    $(this).parent().toggleClass('active inactive');

                    if ($(this).data('category') === 1) {
                        $('#input-ticket-time').removeClass('hidden');
                    } else {
                        $('#input-ticket-time').addClass('hidden');
                    }

                    var typeId = $(this).val();

                    // Get available license types for checkboxes
                    $.get('/tickets/licenses/' + typeId).done(function (res) {
                        var licenseList = $('#license-list');
                        licenseList.empty();

                        res.forEach(function (licenseType) {
                            licenseList.append(templateLicenseType(licenseType));
                        });
                    });

                    $.get('/tickets/prices/' + typeId).done(function (res) {
                        var priceList = $('#price-list');
                        priceList.empty();
                        
                        res.forEach(function (price) {
                            priceList.append(templatePrice(price));
                        });

                        setupStep3();

                        $('input[name="price-select"]').change(function (evt) {
                            $(this).closest('ul').find('label').addClass('inactive').removeClass('active');
                            $(this).parent().toggleClass('active inactive');

                            var priceId = $(this).val();
                            var groupCount = $(this).data('group-count');

                            $.get('/tickets/dates/' + priceId).done(function (res) {
                                validDates = res;

                                var minDate = moment().subtract(1, 'year').startOf('day');
                                dateSelect.datepicker('destroy');
                                dateSelect.datepicker({
                                    minDate: minDate.toDate(),
                                    beforeShowDay: function (date) {
                                        var year = $.datepicker.formatDate('yy', date);
                                        var month = $.datepicker.formatDate('m', date);
                                        var day = $.datepicker.formatDate('d', date);

                                        if (!minDate.isBefore(date, 'day')) {
                                            return [false];
                                        }

                                        if (validDates.hasOwnProperty(year)) {
                                            if (validDates[year].hasOwnProperty(month)) {
                                                if (validDates[year][month].hasOwnProperty(day)) {
                                                    switch (validDates[year][month][day].left) {
                                                        case 0:
                                                            return [false];
                                                        default:
                                                            return [true, moment(date).isBefore(moment(), 'day') ?
                                                                'ui-state-available-before' :
                                                                'ui-state-available'
                                                            ];
                                                    }
                                                }
                                            }
                                        }

                                        return [false];
                                    },
                                    onSelect: function (date) {
                                        setQuotaInfo(date);
                                        setupStep5(groupCount);
                                    }
                                });

                                setGeneralQuota();

                                setupStep4();
                                selDate = null;

                                $('#ticket-save').click(function (evt) {
                                    evt.preventDefault();

                                    $('input[name*="birthday_"]').each(function(index, elem) {
                                        elem.value = parseInt(elem.value, 10);
                                    });

                                    var saveBtn = $(this);
                                    saveBtn.prop('disabled', true);
                                    var formData = new FormData(document.querySelector('form#reseller-form'));

                                    var birthYear = $('#birth_year').val();
                                    var birthMonth = $('#birth_month').val();
                                    var birthDay = $('#birth_day').val();

                                    if (birthYear < 100) {
                                        birthYear = 1900 + parseInt(birthYear, 10);
                                    }

                                    var birthday = birthYear + "-" + birthMonth + "-" + birthDay;
                                    formData.append('birthday', birthday);
                                    formData.append('date', selDate.format('YYYY-MM-DD'));
                                    formData.append('ticket_price_id', priceId);

                                    $.ajax({
                                        url: '/tickets',
                                        data: formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        type: 'POST',
                                        dataType: 'json'
                                    }).done(function (data) {
                                        showSuccess();
                                        setupStep6([ticketUrl, data.id, 'pdf'].join('/'));
                                    }).fail(function(request) {
                                        var fail =  $.parseJSON(request.responseText);
                                        showFailure(fail.message || fail.error);
                                    }).always(function () {
                                        saveBtn.prop('disabled', false);
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    function showSuccess()
    {
        $('#status-error').addClass('hidden');
        $('#status-success').removeClass('hidden');
    }

    function showFailure(message)
    {
        message = message || 'Beim Kauf sind Fehler aufgetreten, bitte überprüfen Sie Ihre Eingabe.<br/>Achten Sie bitte auch darauf, dass alle Pflichtfelder ausgefüllt sind.';
        $('#status-error').html(message);

        $('#status-error').removeClass('hidden');
        $('#status-success').addClass('hidden');
    }

    function setupStep1()
    {
        $('#target-area-select').removeClass('hidden');
        $('#ticket-save').off('click');
    }

    function setupStep2()
    {
        $('#target-price-select,#target-date-select,#target-user-data').addClass('hidden');
        $('#target-ticket-select').removeClass('hidden');
        $('#ticket-save').off('click');

        scrollTo('#target-ticket-select', 60);
    }

    function setupStep3()
    {
        $('#target-date-select,#target-user-data').addClass('hidden');
        $('#target-price-select').removeClass('hidden');
        $('#ticket-save').off('click');

        scrollTo('#target-price-select', 60);
    }

    function setupStep4()
    {
        $('#target-user-data').addClass('hidden');
        $('#target-date-select').removeClass('hidden');
        $('#ticket-save').off('click');

        setQuotaInfo(null);
        scrollTo('#target-date-select', 60);
    }

    function setupStep5(groupCount)
    {
        $('#target-user-data').removeClass('hidden');

        if (groupCount > 0) {
            $('#target-group').removeClass('hidden');
            setupUserGroup(groupCount);
        } else {
            $('#target-group').addClass('hidden');
        }
    }

    function setupStep6(pdf_url)
    {
        $('#ticket-save').addClass('hidden');
        $('#ticket-new,#ticket-print').removeClass('hidden');

        $('#ticket-print').prop('href', pdf_url);

        scrollTo('#target-form-buttons', 60)
    }

    function templateArea(area)
    {
        return [
            '<li>',
            '<label>',
            area.name,
            '<input type="radio" name="area-select" value="' + area.id + '" />',
            '</label>',
            '<span class="area-manager">' + area.manager + '</span>',
            '</li>'
        ].join("\n");
    }

    var groupIndex = 0;
    function setupUserGroup(count)
    {
        $('#btn-add-person').attr('disabled', false);
        $('#target-group-count').text(count - 1);
        var userList = $('#target-group-list');
        userList.empty();

        userList.data('group-count', count - 1);
    }

    function setupRemoveBindings()
    {
        var groupRemove = $('.btn-remove-person');
        groupRemove.off('click');
        groupRemove.on('click', removeFromGroup);
    }

    function removeFromGroup (evt)
    {
        evt.preventDefault();

        var groupList = $('#target-group-list');
        var groupCount = groupList.data('group-count');

        $(this).closest('li').remove();

        if (groupList.children().length < groupCount) {
            $('#btn-add-person').attr('disabled', null);
        }

        setupRemoveBindings();
    }

    $('#btn-add-person').click(function (evt)
    {
        evt.preventDefault();

        var groupList = $('#target-group-list');
        var groupCount = groupList.data('group-count');

        groupList.append(templateUser(groupIndex++));

        if (groupList.children().length >= groupCount) {
            $('#btn-add-person').attr('disabled', true);
        }

        setupRemoveBindings();
    });

    function templateUser(index)
    {
        if (index < 0) {
            return;
        }

        return [
            '<li class="col-lg-5 col-sm-11">',
                '<div class="row">',
                    '<div class="col-sm-12">',
                        '<div class="form-group">',
                            '<label>Vorname</label>',
                            '<input ' +
                                'type="text" ' +
                                'class="form-control" ' +
                                'name="group[' + index + '][first_name]" ' +
                                'id="group[' + index + '][first_name]" />',
                        '</div>',
                    '</div>',
                '</div>',
                '<div class="row">',
                    '<div class="col-sm-12">',
                        '<div class="form-group">',
                            '<label>Nachname</label>',
                                '<input type="text" ' +
                                    'class="form-control" ' +
                                    'name="group[' + index + '][last_name]" ' +
                                    'id="group[' + index + '][last_name]" />',
                        '</div>',
                    '</div>',
                '</div>',
                '<div class="row">',
                    '<div class="col-sm-12">',
                        '<div class="form-group">',
                            '<label>Geburtstag</label>',
                            '<div class="row">',
                                '<div class="col-xs-3">',
                                    '<input type="number" ' +
                                        'min="1" max="31" ' +
                                        'class="form-control" ' +
                                        'placeholder="Tag (TT)" ' +
                                        'name="group[' + index + '][birthday_day]" ' +
                                        'id="group[' + index + '][birthday_day]" />',
                                '</div>',
                                '<div class="col-xs-3">',
                                    '<input ' +
                                        'type="number" ' +
                                        'min="1" max="12" ' +
                                        'class="form-control" ' +
                                        'placeholder="Monat (MM)" ' +
                                        'name="group[' + index + '][birthday_month]" ' +
                                        'id="group[' + index + '][birthday_month]" />',
                                '</div>',
                                '<div class="col-xs-4">',
                                    '<input ' +
                                        'type="number" ' +
                                        'min="1900" max="2050" ' +
                                        'class="form-control" ' +
                                        'placeholder="Jahr (JJJJ)" ' +
                                        'name="group[' + index + '][birthday_year]" ' +
                                        'id="group[' + index + '][birthday_year]" />',
                                '</div>',
                                '<div class="col-xs-2">',
                                    '<button class="btn btn-danger btn-remove-person pull-right">',
                                        '<span class="fa fa-times fa-fw"></span>',
                                    '</button>',
                                '</div>',
                            '</div>',
                        '</div>',
                    '</div>',
                '</div>',
            '</li>'
        ].join("\n");
    }

    function templateTicketGroup(ticketGroup)
    {
        return [
            '<fieldset>',
            '<legend>',
            ticketGroup.name,
            '</legend>',
            ticketGroup.data.map(function (ticket) {
                return templateTicket(ticket) }).join("\n"),
            '</fieldset>',
            ].join("\n");
    }

    function templateNoTickets()
    {
        return [
            '<fieldset>',
            '<legend>',
            'Keine aktiven Angelkarten',
            '</legend>',
            '</fieldset>'
        ].join("\n");
    }

    function templateTicket(ticket)
    {
        return [
            '<li>',
            '<label>',
            ticket.name,
            '<input type="radio" name="ticket-select" value="' + ticket.id + '" data-category="' + ticket.ticket_category_id + '" />',
            '</label>',
            '<span class="ticket-type-info">',
            ticket.group ? 'Gruppenkarte' : '',
            '</span>',
            '</li>'
        ].join("\n");
    }

    function templatePrice(price)
    {
        return [
            '<li>',
            '<label>',
            price.name + '<br/>',
            '<span class="ticket-price">€ ' + (price.value / 100).toFixed(2).replace('.', ',') + '</span>',
            '<input type="radio" name="price-select" value="' + price.id + '" data-group-count="' + price.group_count + '" />',
            '</label>',
            '<span class="ticket-price-info">',
            price.group_count ? price.group_count + ' Personen' : '',
            '</span>',
            '</li>'
        ].join("\n");
    }

    function templateLicenseType(licenseType)
    {
        return [
            '<li>',
            '<label>',
            '<input type="checkbox" name="licenses[]" value="' + licenseType.id + '" />',
            licenseType.name,
            '</label>',
            '</li>'
        ].join("\n");
    }
});