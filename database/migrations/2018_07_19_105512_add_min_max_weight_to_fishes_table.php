<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinMaxWeightToFishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fishes', function (Blueprint $table) {
            $table->integer('min_weight')->after('max_size');
            $table->integer('max_weight')->after('min_weight');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fishes', function (Blueprint $table) {
            $table->dropColumn('min_weight');
            $table->dropColumn('max_weight');
        });
    }
}
