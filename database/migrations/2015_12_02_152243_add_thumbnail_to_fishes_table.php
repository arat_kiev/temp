<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddThumbnailToFishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fishes', function (Blueprint $table) {
            $table->integer('picture_id')->unsigned()->nullable()->after('id');
            $table->integer('gallery_id')->unsigned()->nullable()->after('picture_id');

            $table->foreign('picture_id')->references('id')->on('pictures');
            $table->foreign('gallery_id')->references('id')->on('galleries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fishes', function (Blueprint $table) {
            $table->dropForeign('fishes_picture_id_foreign');
            $table->dropForeign('fishes_gallery_id_foreign');

            $table->dropColumn('picture_id');
            $table->dropColumn('gallery_id');
        });
    }
}
