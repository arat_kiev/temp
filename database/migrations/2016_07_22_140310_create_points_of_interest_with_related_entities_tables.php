<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointsOfInterestWithRelatedEntitiesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points_of_interest', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('street', 100);
            $table->string('post_code', 20);
            $table->string('city', 100);
            $table->integer('country_id')->unsigned()->index();
            $table->integer('picture_id')->unsigned()->index();
            $table->string('email')->unique();
            $table->string('phone', 30);
            $table->string('website');
            $table->nullableTimestamps();
        });

        Schema::create('point_of_interest_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('poi_id')->unsigned()->index();
            $table->string('locale', 5)->index();
            $table->text('description')->default('');
            $table->nullableTimestamps();

            $table->foreign('poi_id')->references('id')->on('points_of_interest')->onDelete('cascade');
        });

        Schema::create('point_of_interest_opening_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('poi_id')->unsigned()->index();
            $table->enum('day', ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']);
            $table->string('time_open', 4);
            $table->string('time_closed', 4);
            $table->nullableTimestamps();

            $table->foreign('poi_id')->references('id')->on('points_of_interest')->onDelete('cascade');
        });

        Schema::create('point_of_interest_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->nullableTimestamps();
        });

        Schema::create('point_of_interest_category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('poi_category_id')->unsigned()->index();
            $table->string('locale', 5)->index();
            $table->string('translation')->default('');
            $table->nullableTimestamps();

            $table->foreign('poi_category_id')->references('id')->on('point_of_interest_categories')->onDelete('cascade');
        });

        Schema::create('point_of_interest_pivot_categories', function (Blueprint $table) {
            $table->integer('poi_id')->unsigned()->index();
            $table->integer('poi_category_id')->unsigned()->index();

            $table->foreign('poi_id')->references('id')->on('points_of_interest')->onDelete('cascade');
            $table->foreign('poi_category_id')->references('id')->on('point_of_interest_categories')->onDelete('cascade');
        });

        Schema::create('point_of_interest_areas', function (Blueprint $table) {
            $table->integer('poi_id')->unsigned()->index();
            $table->integer('area_id')->unsigned()->index();

            $table->foreign('poi_id')->references('id')->on('points_of_interest')->onDelete('cascade');
            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('points_of_interest');
        Schema::drop('point_of_interest_categories');
        Schema::drop('point_of_interest_category_translations');
        Schema::drop('point_of_interest_opening_hours');
        Schema::drop('point_of_interest_translations');

        Schema::drop('points_of_interest_pivot_categories');
        Schema::drop('point_of_interest_areas');
    }
}
