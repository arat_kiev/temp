<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsAndPromotionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['SIGNUP', 'INVITE', 'SALE'])->index();
            $table->dateTime('valid_from')->index();
            $table->dateTime('valid_till')->nullable()->index();
            $table->unsignedInteger('usage_bonus');
            $table->unsignedInteger('owner_bonus')->nullable();
            $table->boolean('is_active')->default(true)->index();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('deleted_by')->references('id')->on('users')->onDelete('set null');
        });

        Schema::create('promotion_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('promo_id')->index();
            $table->string('locale', 5)->index();
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();

            $table->unique(['promo_id', 'locale']);
            $table->foreign('promo_id')->references('id')->on('promotions')->onDelete('cascade');
        });

        Schema::create('promotion_targets', function (Blueprint $table) {
            $table->unsignedInteger('promo_id')->index();
            $table->unsignedInteger('related_id')->index();
            $table->string('related_type');
            $table->timestamps();

            $table->unique(['promo_id', 'related_id', 'related_type']);
            $table->foreign('promo_id')->references('id')->on('promotions')->onDelete('cascade');
        });

        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('promo_id')->nullable()->index();
            $table->string('code', 64)->unique();
            $table->unsignedInteger('usage_bonus');
            $table->boolean('is_public')->default(true)->index();
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('created_for')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('promo_id')->references('id')->on('promotions')->onDelete('restrict');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('created_for')->references('id')->on('users')->onDelete('set null');
            $table->foreign('deleted_by')->references('id')->on('users')->onDelete('set null');
        });

        Schema::create('user_coupons', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('coupon_id')->index();
            $table->timestamps();

            $table->unique(['user_id', 'coupon_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_coupons');
        Schema::dropIfExists('coupons');

        Schema::dropIfExists('promotion_targets');
        Schema::dropIfExists('promotion_translations');
        Schema::dropIfExists('promotions');
    }
}
