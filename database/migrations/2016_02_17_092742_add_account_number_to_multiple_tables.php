<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountNumberToMultipleTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('managers', function (Blueprint $table) {
            $table->string('account_number')->nullable()->after('website');
        });

        Schema::table('resellers', function (Blueprint $table) {
            $table->string('account_number')->nullable();
        });

        Schema::table('areas', function (Blueprint $table) {
            $table->string('account_number')->nullable()->after('rule_id');
        });

        Schema::table('ticket_types', function (Blueprint $table) {
            $table->string('account_number')->nullable()->after('rule_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('managers', function (Blueprint $table) {
            $table->dropColumn('account_number');
        });

        Schema::table('resellers', function (Blueprint $table) {
            $table->dropColumn('account_number');
        });

        Schema::table('areas', function (Blueprint $table) {
            $table->dropColumn('account_number');
        });

        Schema::table('ticket_types', function (Blueprint $table) {
            $table->dropColumn('account_number');
        });
    }
}
