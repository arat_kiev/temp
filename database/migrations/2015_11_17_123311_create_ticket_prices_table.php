<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTicketPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_type_id')->unsigned();
            $table->enum('type', ['DEFAULT', 'MEMBER', 'JUNIOR', 'SENIOR'])->default('DEFAULT');
            $table->integer('value');
            $table->date('valid_from')->index();
            $table->date('valid_to')->index();
            $table->date('visible_from')->index();

            $table->nullableTimestamps();
            $table->softDeletes();

            $table->foreign('ticket_type_id')->references('id')->on('ticket_types')->onDelete('restrict');
        });

        Schema::create('ticket_price_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_price_id')->unsigned();
            $table->string('locale', 5)->index();
            $table->string('name');

            $table->unique(['ticket_price_id', 'locale']);
            $table->foreign('ticket_price_id')->references('id')->on('ticket_prices')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_price_translations');
        Schema::drop('ticket_prices');
    }
}
