<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAreaTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_tags', function (Blueprint $table) {
            $table->integer('area_id')->unsigned();
            $table->string('tag')->index();
            $table->integer('weight')->unsigned()->default(1);
            $table->boolean('custom')->default(false);

            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('area_tags');
    }
}
