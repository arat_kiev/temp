<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagerResellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manager_resellers', function (Blueprint $table) {
            $table->integer('manager_id')->unsigned()->index();
            $table->integer('reseller_id')->unsigned()->index();

            $table->unique(['manager_id', 'reseller_id']);
            $table->foreign('manager_id')->references('id')->on('managers')->onDelete('cascade');
            $table->foreign('reseller_id')->references('id')->on('resellers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('manager_resellers');
    }
}
