<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContactFieldsToOrganizationMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organization_members', function (Blueprint $table) {
            $table->enum('gender', ['MALE', 'FEMALE'])->nullable()->after('last_name');
            $table->date('birthday')->nullable()->after('gender');
            $table->string('street')->nullable()->after('birthday');
            $table->string('post_code', 32)->nullable()->after('street');
            $table->string('city')->nullable()->after('post_code');
            $table->string('phone')->nullable()->after('city');
            $table->date('leave_date')->nullable()->after('phone');
            $table->text('comment')->nullable()->after('leave_date');
            $table->integer('country_id')->unsigned()->nullable()->after('comment');

            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organization_members', function (Blueprint $table) {
            $table->dropColumn('gender');
            $table->dropColumn('birthday');
            $table->dropColumn('street');
            $table->dropColumn('post_code');
            $table->dropColumn('city');
            $table->dropColumn('phone');
            $table->dropColumn('leave_date');
            $table->dropColumn('comment');

            $table->dropForeign('organization_members_country_id_foreign');
            $table->dropColumn('country_id');
        });
    }
}
