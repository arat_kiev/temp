<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddedDraftedAtAndDraftedByFileds extends Migration
{

    private $tables;

    public function __construct()
    {
        $this->tables = ['managers', 'points_of_interest', 'areas', 'fishes', 'techniques'];
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach($this->tables as $table_name) {
            Schema::table($table_name, function (Blueprint $table) use ($table_name) {
                $table->unsignedInteger('drafted_by')->index()->nullable()->after('updated_at');
                $table->boolean('draft')->default(false)->after('drafted_by');

                $table->foreign('drafted_by')->references('id')->on('users')->onDelete('set null');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach($this->tables as $table_name) {
            Schema::table($table_name, function (Blueprint $table) {
                $table->dropColumn('drafted_by');
                $table->dropColumn('draft');
            });
        }
    }

}
