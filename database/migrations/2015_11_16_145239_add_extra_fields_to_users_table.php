<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddExtraFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('balance')->after('password');
            $table->string('first_name')->nullable()->after('balance');
            $table->string('last_name')->nullable()->after('first_name');
            $table->string('street')->nullable()->after('last_name');
            $table->string('post_code', 32)->nullable()->after('street');
            $table->string('city')->nullable()->after('post_code');
            $table->date('birthday')->nullable()->after('city');
            $table->string('phone')->nullable()->after('birthday');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('birthday');
            $table->dropColumn('city');
            $table->dropColumn('post_code');
            $table->dropColumn('street');
            $table->dropColumn('last_name');
            $table->dropColumn('first_name');
            $table->dropColumn('balance');
        });
    }
}
