<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Contact\Manager;
use App\Models\Client;

class CreateInvoicesTableAndExtendTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @throws Exception
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();
            Schema::create('invoices', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamp('date_from')->nullable();
                $table->timestamp('date_till')->nullable();
                $table->unsignedInteger('storno_id')->nullable()->index();  // Relation to id, which was deleted
                $table->boolean('is_deleted')->default(false);
                $table->text('storno_reason')->default('');
                $table->timestamps();
                $table->softDeletes();                                      // This will add a deleted_at field
                $table->unsignedInteger('created_by')->nullable();
                $table->unsignedInteger('updated_by')->nullable();
                $table->unsignedInteger('deleted_by')->nullable();

                $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
                $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
                $table->foreign('deleted_by')->references('id')->on('users')->onDelete('set null');
            });

            Schema::create('manager_invoices', function (Blueprint $table) {
                $table->unsignedInteger('invoice_id');
                $table->unsignedInteger('manager_id');
                $table->unsignedInteger('reseller_id');

                $table->primary(['invoice_id', 'manager_id', 'reseller_id']);
                $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');
                $table->foreign('manager_id')->references('id')->on('managers')->onDelete('cascade');
                $table->foreign('reseller_id')->references('id')->on('resellers')->onDelete('cascade');
            });

            Schema::table('tickets', function (Blueprint $table) {
                $table->softDeletes()->after('updated_at');                 // This will add a deleted_at field
                $table->boolean('is_deleted')->default(false);
                $table->unsignedInteger('invoice_id')->nullable();
                $table->unsignedInteger('storno_id')->nullable();           // Relation to id, which was deleted
                $table->text('storno_reason')->default('');
            });

            // Add default pdf invoice template
            $this->addDefaultPdfInvoiceTemplate();
            // Add default email storno_ticket template
            $this->addDefaultEmailStornoTicketTemplate();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->down();
            throw $e;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->removeDefaultPdfInvoiceTemplate();
        $this->removeDefaultEmailStornoTicketTemplate();

        if (Schema::hasColumn('tickets', 'invoice_id')) {
            Schema::table('tickets', function (Blueprint $table) {
                $table->dropSoftDeletes();
                $table->dropColumn('invoice_id');
                $table->dropColumn('is_deleted');
                $table->dropColumn('storno_id');
                $table->dropColumn('storno_reason');
            });
        }

        if (Schema::hasTable('manager_invoices')) {
            Schema::drop('manager_invoices');
        }

        if (Schema::hasTable('invoices')) {
            Schema::drop('invoices');
        }
    }

    /**
     * Add default invoices template to existing managers
     *
     * @return void
     */
    private function addDefaultPdfInvoiceTemplate()
    {
        $managers = Manager::all();

        foreach ($managers as $manager) {
            $templates = $manager->templates;
            $templates['pdf']['invoice'] = 'bissanzeiger.pdf.invoice';
            $manager->templates = $templates;
            $manager->save();
        }
    }

    /**
     * Add default email `storno_ticket` template to existing clients
     *
     * @return void
     */
    private function addDefaultEmailStornoTicketTemplate()
    {
        $clients = Client::all();

        foreach ($clients as $client) {
            if ($templates = $client->templates) {
                $templates['email']['ticket_storned'] = 'system.email.ticket_storned';
                $client->templates = $templates;
                $client->save();
            }
        }
    }

    /**
     * Remove invoice templates from managers
     *
     * @return void
     */
    private function removeDefaultPdfInvoiceTemplate()
    {
        $managers = Manager::all();

        foreach ($managers as $manager) {
            $templates = $manager->templates;
            unset($templates['pdf']['invoice']);
            $manager->templates = $templates;
            $manager->save();
        }
    }

    /**
     * Remove email `storno_ticket` templates from clients
     *
     * @return void
     */
    private function removeDefaultEmailStornoTicketTemplate()
    {
        $clients = Client::all();

        foreach ($clients as $client) {
            if ($templates = $client->templates) {
                unset($templates['email']['ticket_storned']);
                $client->templates = $templates;
                $client->save();
            }
        }
    }
}
