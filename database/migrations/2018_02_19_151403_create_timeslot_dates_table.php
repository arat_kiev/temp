<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeslotDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timeslot_dates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('timeslot_id');
            $table->date('date');
            $table->unsignedInteger('pool')->nullable();
            $table->timestamps();

            $table->foreign('timeslot_id')->references('id')->on('product_timeslots')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timeslot_dates');
    }
}
