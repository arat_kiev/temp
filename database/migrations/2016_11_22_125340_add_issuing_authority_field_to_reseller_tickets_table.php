<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIssuingAuthorityFieldToResellerTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reseller_tickets', function (Blueprint $table) {
            $table->string('issuing_authority')->nullable()->after('licenses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reseller_tickets', function (Blueprint $table) {
            $table->dropColumn('issuing_authority');
        });
    }
}
