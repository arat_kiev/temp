<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentalReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rental_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('timeslot_date_id');
            $table->text('notice');
            $table->timestamps();

            $table->foreign('timeslot_date_id')->references('id')->on('timeslot_dates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rental_reservations');
    }
}
