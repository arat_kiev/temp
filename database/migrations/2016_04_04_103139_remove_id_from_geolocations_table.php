<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveIdFromGeolocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('geolocations', function (Blueprint $table) {
            $table->dropColumn('id');

            $table->index(['locatable_id', 'locatable_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('geolocations', function (Blueprint $table) {
            $table->dropIndex(['geolocations_locatable_id_locatable_type_index']);

            $table->increments('id');
        });
    }
}
