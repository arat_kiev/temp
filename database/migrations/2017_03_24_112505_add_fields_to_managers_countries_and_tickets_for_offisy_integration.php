<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToManagersCountriesAndTicketsForOffisyIntegration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('managers', function (Blueprint $table) {
            $table->string('offisy_id')->nullable();
            $table->boolean('rksv_enabled')->default(false);
        });

        Schema::table('countries', function (Blueprint $table) {
            $table->string('offisy_vat_id')->nullable();
            $table->string('offisy_no_vat_id')->nullable();
        });

        Schema::table('tickets', function (Blueprint $table) {
            $table->string('offisy_id')->nullable();
            $table->string('offisy_code')->nullable();
            $table->json('offisy_tags')->nullable();
        });

        $this->insertVatIdsIntoCountryTable();
    }

    private function insertVatIdsIntoCountryTable()
    {
        DB::table('countries')
            ->where('country_code', 'de')
            ->update([
                'offisy_vat_id' => '87743401-209d-4783-8d60-e4f32611b794',
                'offisy_no_vat_id' => 'cc5903f8-8bbd-4763-b24e-100f98ce9ed6',
            ]);

        DB::table('countries')
            ->where('country_code', 'at')
            ->update([
                'offisy_vat_id' => '21e845bb-cbb2-4451-8601-d6d11dbde8a6',
                'offisy_no_vat_id' => 'cc5903f8-8bbd-4763-b24e-100f98ce9ed6',
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('managers', function (Blueprint $table) {
            $table->dropColumn('offisy_id');
            $table->dropColumn('rksv_enabled');
        });

        Schema::table('countries', function (Blueprint $table) {
            $table->dropColumn('offisy_vat_id');
            $table->dropColumn('offisy_no_vat_id');
        });

        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('offisy_id');
            $table->dropColumn('offisy_code');
            $table->dropColumn('offisy_tags');
        });
    }
}
