<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingpagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landingpages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->integer('image_id')->unsigned()->index();
            $table->timestamps();
        });

        Schema::create('landingpage_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lp_id')->unsigned()->index();
            $table->string('locale', 5)->index();
            $table->string('slug')->unique();
            $table->string('title');
            $table->text('headline')->nullable();
            $table->text('punchline')->nullable();
            $table->text('description')->nullable();

            $table->unique(['lp_id', 'locale']);
            $table->foreign('lp_id')->references('id')->on('landingpages')->onDelete('cascade');
        });

        Schema::create('landingpages_areas', function (Blueprint $table) {
            $table->integer('lp_id')->unsigned()->index();
            $table->integer('area_id')->unsigned()->index();

            $table->primary(['lp_id', 'area_id']);
            $table->foreign('lp_id')->references('id')->on('landingpages')->onDelete('cascade');
            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('landingpages_areas');
        Schema::drop('landingpage_translations');
        Schema::drop('landingpages');
    }
}
