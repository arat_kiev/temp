<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketInspectionHaulsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_inspection_hauls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inspection_id')->unsigned();
            $table->integer('fish_id')->unsigned();
            $table->integer('length');
            $table->integer('weight');
            $table->longText('comment');
            $table->timestamps();

            $table->foreign('inspection_id')->references('id')->on('ticket_inspections');
            $table->foreign('fish_id')->references('id')->on('fishes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_inspection_hauls');
    }
}
