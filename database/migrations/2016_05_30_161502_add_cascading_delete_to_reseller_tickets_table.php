<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCascadingDeleteToResellerTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reseller_tickets', function (Blueprint $table) {
            $table->dropForeign('reseller_tickets_ticket_id_foreign');
            $table->foreign('ticket_id')->references('id')->on('tickets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reseller_tickets', function (Blueprint $table) {
            $table->dropForeign('reseller_tickets_ticket_id_foreign');
            $table->foreign('ticket_id')->references('id')->on('tickets');
        });
    }
}
