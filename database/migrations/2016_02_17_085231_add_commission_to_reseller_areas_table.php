<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommissionToResellerAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reseller_areas', function (Blueprint $table) {
            $table->enum('commission_type', ['ABSOLUTE', 'RELATIVE'])->nullable();
            $table->integer('commission_value')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reseller_areas', function (Blueprint $table) {
            //
        });
    }
}
