<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommissionsTableAndModifyInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('related_id');
            $table->string('related_type');
            $table->enum('commission_type', ['ABSOLUTE', 'RELATIVE']);
            $table->unsignedInteger('commission_value');
            $table->unsignedInteger('min_value')->default(0);
            $table->boolean('is_online');
            $table->dateTime('valid_from');
            $table->dateTime('valid_till')->nullable();
            $table->timestamps();
        });

        Schema::table('invoices', function (Blueprint $table) {
            $table->unsignedInteger('reseller_id')->nullable()->change();
        });

        Schema::table('tickets', function (Blueprint $table) {
            $table->unsignedInteger('hf_invoice_id')->nullable()->after('invoice_id');
            $table->unsignedInteger('commission_value')->nullable()->after('hf_invoice_id');

            $table->foreign('hf_invoice_id')->references('id')->on('invoices')->onDelete('restrict');
        });

        Schema::table('product_sales', function (Blueprint $table) {
            $table->unsignedInteger('hf_invoice_id')->nullable()->after('invoice_id');
            $table->unsignedInteger('commission_value')->nullable()->after('hf_invoice_id');

            $table->foreign('hf_invoice_id')->references('id')->on('invoices')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('hf_invoice_id');
            $table->dropColumn('commission_value');
        });

        Schema::table('product_sales', function (Blueprint $table) {
            $table->dropColumn('hf_invoice_id');
            $table->dropColumn('commission_value');
        });

        Schema::table('invoices', function (Blueprint $table) {
            $table->unsignedInteger('reseller_id')->nullable(false)->change();
        });

        Schema::dropIfExists('commissions');
    }
}
