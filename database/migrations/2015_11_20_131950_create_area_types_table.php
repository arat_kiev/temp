<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAreaTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_types', function (Blueprint $table) {
            $table->increments('id');
            $table->nullableTimestamps();
        });

        Schema::create('area_type_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('area_type_id')->unsigned();
            $table->string('locale', 5)->index();
            $table->string('name');
            $table->text('description')->nullable();

            $table->unique(['area_type_id', 'locale']);
            $table->foreign('area_type_id')->references('id')->on('area_types')->onDelete('cascade');
        });

        Schema::table('areas', function (Blueprint $table) {
            $table->integer('type_id')->unsigned()->nullable()->after('id');
            $table->foreign('type_id')->references('id')->on('area_types')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('areas', function (Blueprint $table) {
            $table->dropForeign('areas_type_id_foreign');
            $table->dropColumn('type_id');
        });

        Schema::drop('area_type_translations');
        Schema::drop('area_types');
    }
}
