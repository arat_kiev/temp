<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IncreaseFisherIdFieldSizeInUsersAndResellerTicketsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::update('ALTER TABLE `users` MODIFY `fisher_id` VARCHAR(25)');
        DB::update('ALTER TABLE `reseller_tickets` MODIFY `fisher_id` VARCHAR(25)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::update('ALTER TABLE `users` MODIFY `fisher_id` VARCHAR(9)');
        DB::update('ALTER TABLE `reseller_tickets` MODIFY `fisher_id` VARCHAR(9)');
    }
}
