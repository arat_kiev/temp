<?php

use App\Models\Area\Area;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeasonDatesToAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('areas', function (Blueprint $table) {
            $table->date('season_begin')->nullable()->after('rule_id');
            $table->date('season_end')->nullable()->after('season_begin');
        });

        $areaQuery = Area::query()
            ->addSelect('areas.*', 'season.begin', 'season.end')
            ->leftJoin(DB::raw('(
                SELECT
                    a.id AS area_id,
                    CONCAT(\'0001-\', MIN(DATE_FORMAT(tp.valid_from, \'%m-%d\'))) AS `begin`,
                    CONCAT(\'0001-\', MAX(DATE_FORMAT(tp.valid_to, \'%m-%d\'))) AS `end`
                FROM areas a
                LEFT JOIN ticket_types tt ON a.id = tt.area_id
                LEFT JOIN ticket_prices tp ON tt.id = tp.ticket_type_id
                WHERE YEAR(tp.valid_from) > 2016 OR YEAR(tp.valid_to) > 2016
                GROUP BY a.id
            ) season'), 'areas.id', '=', 'season.area_id');

        $areaQuery->chunk(50, function ($areas) {
             $areas->each(function ($area) {
                 $area->season_begin = Carbon::parse($area->begin)->format('d.m');
                 $area->season_end = Carbon::parse($area->end)->format('d.m');
                 $area->save();
             });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('areas', function (Blueprint $table) {
            $table->dropColumn('season_end');
            $table->dropColumn('season_begin');
        });
    }

    private function generateSeasonDatesFromTicketPrices()
    {

    }
}
