<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLicenseTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('fields');
            $table->text('attachments');
            $table->softDeletes();
        });

        Schema::create('license_type_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('license_type_id')->unsigned();
            $table->string('locale', 5)->index();
            $table->text('description');

            $table->unique(['license_type_id', 'locale']);
            $table->foreign('license_type_id')->references('id')->on('license_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('license_type_translations');
        Schema::drop('license_types');
    }
}
