<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommissionToResellerTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reseller_tickets', function (Blueprint $table) {
            $table->enum('commission_type', ['ABSOLUTE', 'RELATIVE'])->nullable();
            $table->double('commission_value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reseller_tickets', function (Blueprint $table) {
            //
        });
    }
}
