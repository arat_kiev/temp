<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHaulsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hauls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('area_id')->unsigned();
            $table->integer('fish_id')->unsigned()->nullable();
            $table->integer('ticket_id')->unsigned()->nullable();
            $table->integer('picture_id')->unsigned()->nullable();
            $table->boolean('public')->default(false);
            $table->boolean('taken')->default(false);
            $table->decimal('size_value')->nullable();
            $table->enum('size_type', ['KG', 'CM'])->nullable();
            $table->text('user_comment');
            $table->date('catch_date');
            $table->time('catch_time')->nullable();
            $table->string('ticket_number')->default('');
            $table->nullableTimestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('area_id')->references('id')->on('areas');
            $table->foreign('fish_id')->references('id')->on('fishes');
            $table->foreign('ticket_id')->references('id')->on('tickets');
            $table->foreign('picture_id')->references('id')->on('pictures');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hauls');
    }
}
