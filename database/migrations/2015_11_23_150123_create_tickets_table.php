<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_type_id')->unsigned();
            $table->integer('ticket_price_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->dateTime('valid_from');
            $table->dateTime('valid_to');
            $table->nullableTimestamps();

            $table->foreign('ticket_type_id')->references('id')->on('ticket_types')->onDelete('restrict');
            $table->foreign('ticket_price_id')->references('id')->on('ticket_prices')->onDelete('restrict');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tickets');
    }
}
