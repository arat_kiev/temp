<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentableTables extends Migration
{
    private $morphMap = [
        'areas'                 => \App\Models\Area\Area::class,
        'cities'                => \App\Models\Location\City::class,
        'hauls'                 => \App\Models\Haul::class,
        'techniques'            => \App\Models\Meta\Technique::class,
        'fishes'                => \App\Models\Meta\Fish::class,
        'users'                 => \App\Models\User::class,
        'points_of_interest'    => \App\Models\PointOfInterest\PointOfInterest::class,
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('user_id')->index();
            // Polymorphic relation
            $table->integer('commentable_id')->unsigned();
            $table->string('commentable_type')->index();

            $table->enum('status', ['accepted', 'review', 'rejected'])->default('accepted');
            $table->text('body')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('comment_flags', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('comment_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->nullable()->index(); // nullable for system reports
            $table->text('description')->nullable();
            $table->timestamps();

            $table->unique(['comment_id', 'user_id']);
            $table->foreign('comment_id')->references('id')->on('comments')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        foreach ($this->morphMap as $alias => $namespace) {
            DB::table('geolocations')
                ->where('locatable_type', $namespace)
                ->update(['locatable_type' => $alias]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comment_flags');
        Schema::drop('comments');

        foreach ($this->morphMap as $alias => $namespace) {
            DB::table('geolocations')
                ->where('locatable_type', $alias)
                ->update(['locatable_type' => $namespace]);
        }
    }
}
