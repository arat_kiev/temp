<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTicketCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->nullableTimestamps();
        });

        Schema::create('ticket_category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_category_id')->unsigned();
            $table->string('locale', 5)->index();
            $table->string('name');

            $table->unique(['ticket_category_id', 'locale']);
            $table->foreign('ticket_category_id')->references('id')->on('ticket_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_category_translations');
        Schema::drop('ticket_categories');
    }
}
