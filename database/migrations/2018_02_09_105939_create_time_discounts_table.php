<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_price_id');
            $table->unsignedInteger('count');
            $table->unsignedInteger('discount');
            $table->boolean('inclusive')->default(false);
            $table->timestamps();

            $table->foreign('product_price_id')->references('id')->on('product_prices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_discounts');
    }
}
