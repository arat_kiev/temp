<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequiredAndOptionalLicenseTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('required_license_types', function (Blueprint $table) {
            $table->integer('ticket_type_id')->unsigned()->index();
            $table->integer('license_type_id')->unsigned()->index();

            $table->foreign('ticket_type_id')->references('id')->on('ticket_types')->onDelete('cascade');
            $table->foreign('license_type_id')->references('id')->on('license_types')->onDelete('cascade');
        });

        Schema::create('optional_license_types', function (Blueprint $table) {
            $table->integer('ticket_type_id')->unsigned()->index();
            $table->integer('license_type_id')->unsigned()->index();

            $table->foreign('ticket_type_id')->references('id')->on('ticket_types')->onDelete('cascade');
            $table->foreign('license_type_id')->references('id')->on('license_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('optional_license_types');
        Schema::drop('required_license_types');
    }
}
