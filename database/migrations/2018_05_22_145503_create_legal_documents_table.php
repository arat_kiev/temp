<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLegalDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legal_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
        });

        Schema::create('legal_document_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('legal_document_id');
            $table->string('locale', 5)->index();
            $table->string('name');
            $table->text('content')->nullable();
            $table->unsignedInteger('file_id')->nullable();

            $table->unique(['legal_document_id', 'locale']);
            $table->foreign('legal_document_id')->references('id')->on('legal_documents')->onDelete('cascade');
            $table->foreign('file_id')->references('id')->on('pictures')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legal_documents');
    }
}
