<?php

use App\Models\Area\Area;
use App\Models\Contact\Reseller;
use App\Models\Ticket\TicketPrice;
use App\Models\Ticket\TicketType;
use Illuminate\Database\Migrations\Migration;

class MigrateResellerTicketPricesData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Reseller::chunk(50, function ($resellers) {
            /** @var Reseller $reseller */
            foreach ($resellers as $reseller) {
                /** @var Area $area */
                foreach ($reseller->areas as $area) {
                    /** @var TicketType $ticketType */
                    foreach ($area->ticketTypes as $ticketType) {
                        /** @var TicketPrice $price */
                        foreach ($ticketType->prices as $price) {
                            $price->resellers()->attach($reseller);
                        }
                    }
                }
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
