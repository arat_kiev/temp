<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductStocksTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('country_id')->nullable()->index();
            $table->string('city', 64)->index();
            $table->string('street', 64);
            $table->string('building', 16)->nullable();
            $table->string('rest_info')->nullable()->comment = 'eg. section, office, apartment number...';
            $table->json('emails')->nullable();
            $table->json('phones')->nullable();
            $table->timestamps();
            $table->softDeletes();                                      // This will add a deleted_at field

            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null');
        });

        DB::table('stocks')->insert([
            'name'          => 'Hauptlager in Linz',
            'country_id'    => 1,
            'city'          => 'Linz',
            'street'        => 'Hopfengasse',
            'building'      => 3,
            'emails'        => json_encode(['info@hejfish.com']),
            'phones'        => json_encode(['+43 677 61 27 53 50']),
        ]);

        Schema::create('product_stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->index();
            $table->unsignedInteger('stock_id')->index();
            $table->unsignedInteger('quantity')->default(0);
            $table->string('unit', 32)->default('piece');
            $table->unsignedInteger('delivery_time')->nullable();
            $table->unsignedInteger('notification_quantity')->default(0);
            $table->nullableTimestamps();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('stock_id')->references('id')->on('stocks')->onDelete('restrict');
        });

        Schema::create('stock_notificatables', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_stock_id');
            $table->unsignedInteger('user_id');
            $table->nullableTimestamps();

            $table->foreign('product_stock_id')->references('id')->on('product_stocks')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_notificatables');
        Schema::dropIfExists('product_stocks');
        Schema::dropIfExists('stocks');
    }
}
