<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyRestrictions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('areas', function (Blueprint $table) {
            $table->dropForeign('areas_manager_id_foreign');
            $table->integer('manager_id')->unsigned()->nullable()->change();
            $table->foreign('manager_id')->references('id')->on('managers')->onDelete('set null');
        });

        Schema::table('ticket_types', function (Blueprint $table) {
            $table->dropForeign('ticket_types_area_id_foreign');
            $table->foreign('area_id')->references('id')->on('areas')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('areas', function (Blueprint $table) {
            $table->dropForeign('areas_manager_id_foreign');
            $table->integer('manager_id')->unsigned()->change();
            $table->foreign('manager_id')->references('id')->on('managers')->onDelete('cascade');
        });

        Schema::table('ticket_types', function (Blueprint $table) {
            $table->dropForeign('ticket_types_area_id_foreign');
            $table->foreign('area_id')->references('id')->on('areas');
        });
    }
}
