<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToObservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('observations', function ($table) {
            $table->foreign('tour_id')->references('id')->on('inspection_tours');
            $table->foreign('observation_category_id')->references('id')->on('observation_categories');
            $table->foreign('gallery_id')->references('id')->on('galleries');
        });

        Schema::table('ticket_inspections', function ($table) {
            $table->foreign('tour_id')->references('id')->on('inspection_tours');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('observations', function (Blueprint $table) {
            $table->dropForeign('observations_tour_id_foreign');
            $table->dropForeign('observations_observation_category_id_foreign');
            $table->dropForeign('observations_gallery_id_foreign');
        });

        Schema::table('ticket_inspections', function (Blueprint $table) {
            $table->dropForeign('ticket_inspections_tour_id_foreign');
        });
    }
}
