<?php

use App\Models\Area\AreaParent;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrateParentAreasDataToTranslatable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        AreaParent::each(function ($parentArea) {
            $t = $parentArea->translateOrNew('de_DE');
            $t->parent_id = $parentArea->id;
            $t->name = $parentArea->getOriginal('name');
            $t->description = $parentArea->getOriginal('description');
            $t->save();
        });

        Schema::table('parent_areas', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parent_areas', function (Blueprint $table) {
            $table->string('name')->nullable();
            $table->text('description')->nullable();
        });
    }
}
