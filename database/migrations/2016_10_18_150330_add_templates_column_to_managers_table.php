<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTemplatesColumnToManagersTable extends Migration
{
    private $defaultPdfTemplate = [];

    /**
     * Run the migrations.
     *
     * @throws Exception
     * @return void
     */
    public function up()
    {
        $pdfTemplates = $this->getClientPdfTemplates();
        $clientManagers = $this->getClientsManagersRelation();

        Schema::table('managers', function (Blueprint $table) {
            $table->text('templates')->after('api_key')->nullable();
        });

        try {
            DB::beginTransaction();
            foreach ($clientManagers as $managerData) {
                // Check if pdf template exists
                if (isset($pdfTemplates[$managerData->client_id])) {
                    DB::table('managers')
                        ->where('id', $managerData->manager_id)
                        ->update([
                            'templates' => json_encode(['pdf' => $pdfTemplates[$managerData->client_id]])
                        ]);
                }
            }
            // Set default templates for all managers, which haven't any template
            DB::table('managers')
                ->whereNull('templates')
                ->update(['templates' => json_encode(['pdf' => $this->defaultPdfTemplate])]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->down();
            throw $e;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('managers', function (Blueprint $table) {
            $table->dropColumn('templates');
        });
    }

    /**
     * Collect information about client pdf templates
     *
     * @return array
     */
    private function getClientPdfTemplates()
    {
        $clientQuery = DB::table('clients')->where('templates', '!=', '[]')->get();

        $clients = [];
        foreach ($clientQuery as $client) {
            $templates = json_decode($client->templates, 1);
            if (isset($templates['pdf'])) {
                $clients[$client->id] = $templates['pdf'];
                if ($client->name == 'bissanzeiger') {
                    $this->defaultPdfTemplate = $templates['pdf'];
                }
            }
        }

        return $clients;
    }

    /**
     * Return relation Manager/Client
     *
     * @return mixed
     */
    private function getClientsManagersRelation()
    {
        $data = DB::table('managers')
            ->join('manager_admins', 'manager_admins.manager_id', '=', 'managers.id')
            ->join('users', 'users.id', '=', 'manager_admins.user_id')
            ->join('clients', 'clients.id', '=', 'users.signup_client_id')
            ->groupBy('managers.id')
            ->select('managers.id as manager_id', 'clients.id as client_id')
            ->get();

        return $data;
    }
}
