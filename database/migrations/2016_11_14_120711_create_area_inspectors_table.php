<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Authorization\Permission;
use App\Models\Authorization\Role;

class CreateAreaInspectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_inspectors', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index();
            $table->integer('area_id')->unsigned()->index();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
        });

        $permission = Permission::create(['name' => 'inspect tickets']);
        $permission->{'description:en_GB'} = 'Inspect Tickets';
        $permission->{'description:de_DE'} = 'Tickets inspizieren';
        $permission->save();

        $newRole = Role::create(["name" => "ticket_inspector"]);
        $newRole->{'description:en_GB'} = 'Ticket Inspector';
        $newRole->{'description:de_DE'} = 'Ticket-Kontrolleur';
        $newRole->givePermissionTo($permission);
        $newRole->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $permission = new Permission();
        $permission->where('name', 'like', '%inspect tickets%')->delete();
        $role = new Role();
        $role->where('name', 'like', '%ticket_inspector%')->delete();

        Schema::drop('area_inspectors');
    }
}
