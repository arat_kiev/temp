<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlacklistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blacklist', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('manager_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('first_name')->nullable()->index();
            $table->string('last_name')->nullable()->index();
            $table->date('birthday')->nullable()->index();
            $table->string('street')->nullable();
            $table->string('post_code', 32)->nullable();
            $table->string('city', 64)->nullable();
            $table->unsignedInteger('country_id')->nullable();
            $table->longText('reason');
            $table->dateTime('banned_till')->nullable();

            $table->foreign('manager_id')->references('id')->on('managers')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blacklist', function (Blueprint $table) {
            $table->dropForeign('blacklist_manager_id_foreign');
            $table->dropForeign('blacklist_user_id_foreign');
            $table->dropForeign('blacklist_country_id_foreign');
            $table->dropIndex('blacklist_first_name_index');
            $table->dropIndex('blacklist_last_name_index');
            $table->dropIndex('blacklist_birthday_index');

            $table->dropIfExists('blacklist');
        });
    }
}
