<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStateFishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('state_fishes', function (Blueprint $table) {
            $table->integer('state_id')->unsigned()->index();
            $table->integer('fish_id')->unsigned()->index();
            $table->date('closed_from')->nullable();
            $table->date('closed_till')->nullable();
            $table->integer('min_length')->nullable();
            $table->integer('max_length')->nullable();

            $table->foreign('state_id')->references('id')->on('states')->onDelete('cascade');
            $table->foreign('fish_id')->references('id')->on('fishes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('state_fishes');
    }
}
