<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTicketDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_days', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_price_id')->unsigned();
            $table->enum('weekday', ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']);

            $table->foreign('ticket_price_id')->references('id')->on('ticket_prices')->onDelete('cascade');
            $table->unique(['ticket_price_id', 'weekday']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_days');
    }
}
