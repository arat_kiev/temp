<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResellerTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reseller_tickets', function (Blueprint $table) {
            $table->integer('ticket_id')->unsigned();
            $table->integer('reseller_id')->unsigned();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('street');
            $table->string('post_code', 32);
            $table->string('city');
            $table->date('birthday');
            $table->string('phone');
            $table->string('email');
            $table->text('licenses')->nullable();

            $table->foreign('ticket_id')->references('id')->on('tickets')->onDelete('restrict');
            $table->foreign('reseller_id')->references('id')->on('resellers')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reseller_tickets');
    }
}
