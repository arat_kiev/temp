<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFishCategoriesTableWithTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fish_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->nullableTimestamps();
        });

        Schema::create('fish_category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fish_category_id')->unsigned()->index();
            $table->string('locale', 5)->index();
            $table->string('name');
            $table->text('description')->nullable();

            $table->unique(['fish_category_id', 'locale']);
            $table->foreign('fish_category_id')->references('id')->on('fish_categories')->onDelete('cascade');
        });

        Schema::table('fishes', function (Blueprint $table) {
            $table->string('latin')->after('id');
            $table->integer('category_id')->after('gallery_id')->unsigned()->index()->nullable();

            $table->foreign('category_id')->references('id')->on('fish_categories')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fishes', function ($table) {
            $table->dropForeign(['category_id']);
            $table->dropColumn(['latin', 'category_id']);
        });

        Schema::drop('fish_category_translations');
        Schema::drop('fish_categories');
    }
}
