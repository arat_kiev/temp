<?php

use App\Models\PointOfInterest\PointOfInterestOpeningHour;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOpeningHoursToTimeValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point_of_interest_opening_hours', function (Blueprint $table) {
            $table->time('opens')->after('day');
            $table->time('closes')->after('opens');
        });

        PointOfInterestOpeningHour::chunk(100, function ($openingHours) {
            foreach ($openingHours as $openingHour) {
                $openingHour->opens = substr($openingHour->time_open, 0, 2).':'.substr($openingHour->time_open, 2, 2);
                $openingHour->closes = substr($openingHour->time_closed, 0, 2).':'.substr($openingHour->time_closed, 2, 2);
                $openingHour->save();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point_of_interest_opening_hours', function (Blueprint $table) {
            $table->dropColumn('opens');
            $table->dropColumn('closes');
        });
    }
}
