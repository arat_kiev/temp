<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAreaTechniquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_techniques', function (Blueprint $table) {
            $table->integer('area_id')->unsigned()->index();
            $table->integer('technique_id')->unsigned()->index();

            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
            $table->foreign('technique_id')->references('id')->on('techniques')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('area_techniques');
    }
}
