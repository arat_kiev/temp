<?php

use Illuminate\Database\Migrations\Migration;
use App\Models\Ticket\Ticket;

class AddStornoReasonToExistedStornedTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $stornedTickets = DB::table('tickets')
            ->where('is_deleted', true)
            ->select(['id', 'storno_reason'])
            ->pluck('storno_reason', 'id');

        Ticket::whereNotNull('storno_id')
            ->each(function ($ticket) use ($stornedTickets) {
                $ticket->storno_reason = $stornedTickets[$ticket->storno_id] ?? '';
                $ticket->save();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Ticket::whereNotNull('storno_id')
            ->each(function ($ticket) {
                $ticket->storno_reason = '';
                $ticket->save();
            });
    }
}
