<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFisherIdToUsersAndResellerTicketsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('fisher_id', 9)->after('api_token')->nullable()->index();
        });

        Schema::table('reseller_tickets', function (Blueprint $table) {
            $table->string('fisher_id', 9)->after('reseller_id')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('fisher_id');
        });

        Schema::table('reseller_tickets', function (Blueprint $table) {
            $table->dropColumn('fisher_id');
        });
    }
}
