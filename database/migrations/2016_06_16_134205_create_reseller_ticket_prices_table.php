<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResellerTicketPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reseller_ticket_prices', function (Blueprint $table) {
            $table->integer('reseller_id')->unsigned()->index();
            $table->integer('ticket_price_id')->unsigned()->index();
            $table->enum('commission_type', ['ABSOLUTE', 'RELATIVE'])->nullable();
            $table->double('commission_value')->nullable();

            $table->unique(['reseller_id', 'ticket_price_id']);
            $table->foreign('reseller_id')->references('id')->on('resellers')->onDelete('cascade');
            $table->foreign('ticket_price_id')->references('id')->on('ticket_prices')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reseller_ticket_prices');
    }
}
