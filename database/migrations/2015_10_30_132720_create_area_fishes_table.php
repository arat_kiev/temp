<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAreaFishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_fishes', function (Blueprint $table) {
            $table->integer('area_id')->unsigned()->index();
            $table->integer('fish_id')->unsigned()->index();

            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
            $table->foreign('fish_id')->references('id')->on('fishes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('area_fishes');
    }
}
