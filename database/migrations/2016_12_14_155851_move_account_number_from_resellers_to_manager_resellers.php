<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveAccountNumberFromResellersToManagerResellers extends Migration
{
    /**
     * Run the migrations.
     *
     * @throws Exception
     * @return void
     */
    public function up()
    {
        $resellerAccountNumbers = $this->getResellerAccountNumber();

        try {
            DB::beginTransaction();

            Schema::table('manager_resellers', function (Blueprint $table) {
                $table->string('account_number')->nullable();
            });

            foreach ($resellerAccountNumbers as $resellerId => $accountNumber) {
                DB::table('manager_resellers')
                    ->where('reseller_id', $resellerId)
                    ->update([
                        'account_number' => $accountNumber
                    ]);
            }

            Schema::table('resellers', function (Blueprint $table) {
                $table->dropColumn('account_number');
            });

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->down();
            throw $e;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('resellers', 'account_number')) {
            Schema::table('resellers', function (Blueprint $table) {
                $table->string('account_number')->nullable();
            });
        }

        if (Schema::hasColumn('manager_resellers', 'account_number')) {
            $resellerAccountNumbers = $this->getManagerResellerAccountNumber();

            foreach ($resellerAccountNumbers as $resellerId => $accountNumber) {
                DB::table('resellers')
                    ->where('id', $resellerId)
                    ->update([
                        'account_number' => $accountNumber
                    ]);
            }

            Schema::table('manager_resellers', function (Blueprint $table) {
                $table->dropColumn('account_number');
            });
        }
    }

    /**
     * Collect information about resellers' account_number
     *
     * @return array
     */
    private function getResellerAccountNumber()
    {
        $resellerQuery = DB::table('resellers')->get();

        $resellers = [];
        foreach ($resellerQuery as $reseller) {
            $resellers[$reseller->id] = $reseller->account_number;
        }

        return $resellers;
    }

    /**
     * Collect information about resellers' account_number by manager
     *
     * @return array
     */
    private function getManagerResellerAccountNumber()
    {
        $resellerQuery = DB::table('manager_resellers')->get();

        $resellers = [];
        foreach ($resellerQuery as $reseller) {
            $resellers[$reseller->reseller_id] = $reseller->account_number;
        }

        return $resellers;
    }
}
