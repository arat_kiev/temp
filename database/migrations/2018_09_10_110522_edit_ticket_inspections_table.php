<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTicketInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_inspections', function (Blueprint $table) {
            $table->integer('tour_id')->unsigned();
            $table->integer('gallery_id')->unsigned();

            $table->foreign('gallery_id')->references('id')->on('galleries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_inspections', function (Blueprint $table) {
            $table->dropForeign('observations_tour_id_foreign');
            $table->dropForeign('observations_gallery_id_foreign');
            $table->dropColumn('tour_id');
            $table->dropColumn('gallery_id');
        });
    }
}
