<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRulesToManagersAreasAndTicketTypesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('managers', function (Blueprint $table) {
            $table->integer('rule_id')->unsigned()->nullable()->after('logo_id');
            $table->foreign('rule_id')->references('id')->on('rules')->onDelete('set null');
        });

        Schema::table('areas', function (Blueprint $table) {
            $table->integer('rule_id')->unsigned()->nullable()->after('gallery_id');
            $table->foreign('rule_id')->references('id')->on('rules')->onDelete('set null');
        });

        Schema::table('ticket_types', function (Blueprint $table) {
            $table->integer('rule_id')->unsigned()->nullable()->after('online_only');
            $table->foreign('rule_id')->references('id')->on('rules')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('managers', function (Blueprint $table) {
            $table->dropForeign('managers_rule_id_foreign');
            $table->dropColumn('rule_id');
        });

        Schema::table('areas', function (Blueprint $table) {
            $table->dropForeign('areas_rule_id_foreign');
            $table->dropColumn('rule_id');
        });

        Schema::table('ticket_types', function (Blueprint $table) {
            $table->dropForeign('ticket_types_rule_id_foreign');
            $table->dropColumn('rule_id');
        });
    }
}
