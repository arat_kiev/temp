<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeslotSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timeslot_sales', function (Blueprint $table) {
            $table->unsignedInteger('product_sale_id');
            $table->unsignedBigInteger('timeslot_date_id');

            $table->foreign('product_sale_id')->references('id')->on('product_sales');
            $table->foreign('timeslot_date_id')->references('id')->on('timeslot_dates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timeslot_sales');
    }
}
