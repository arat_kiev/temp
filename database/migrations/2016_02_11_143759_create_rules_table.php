<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rules', function (Blueprint $table) {
            $table->increments('id');
            $table->text('text');
            $table->timestamps();
        });

        Schema::create('rule_files', function (Blueprint $table) {
            $table->integer('rule_id')->unsigned()->index();
            $table->integer('picture_id')->unsigned()->index();
            $table->boolean('public')->default(false);

            $table->unique(['rule_id', 'picture_id']);
            $table->foreign('rule_id')->references('id')->on('rules')->onDelete('cascade');
            $table->foreign('picture_id')->references('id')->on('pictures')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rule_files');
        Schema::drop('rules');
    }
}
