<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDefaultValuesInOrganizationMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organization_members', function (Blueprint $table) {
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY member_id VARCHAR(255)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY first_name VARCHAR(255)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY last_name VARCHAR(255)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY email VARCHAR(255)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY function VARCHAR(255)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organization_members', function (Blueprint $table) {
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY member_id VARCHAR(255) NOT NULL');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY first_name VARCHAR(255) NOT NULL');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY last_name VARCHAR(255) NOT NULL');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY email VARCHAR(255) NOT NULL');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY function VARCHAR(255) NOT NULL');
        });
    }
}
