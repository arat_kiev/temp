<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountryToResellerTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reseller_tickets', function (Blueprint $table) {
            $table->integer('country_id')->unsigned()->nullable()->after('city');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reseller_tickets', function (Blueprint $table) {
            $table->dropForeign('reseller_tickets_country_id_foreign');
            $table->dropColumn('country_id');
        });
    }
}
