<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCheckinUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkin_units', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->nullableTimestamps();
        });

        Schema::create('checkin_unit_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('checkin_unit_id')->unsigned();
            $table->string('locale', 5)->index();
            $table->string('name');

            $table->unique(['checkin_unit_id', 'locale']);
            $table->foreign('checkin_unit_id')->references('id')->on('checkin_units')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('checkin_unit_translations');
        Schema::drop('checkin_units');
    }
}
