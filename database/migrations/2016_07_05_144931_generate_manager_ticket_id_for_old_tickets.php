<?php

use App\Models\Ticket\ManagerLastId;
use App\Models\Ticket\Ticket;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class GenerateManagerTicketIdForOldTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Ticket::count() > 0) {
            $progress = new ProgressBar(new ConsoleOutput());

            $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s% ');

            $progress->setMessage('Migration in progress...');
            $progress->start(Ticket::count());

            Ticket::orderBy('created_at')->chunk(100, function ($tickets) use ($progress) {
                /** @var Ticket $ticket */
                foreach ($tickets as $ticket) {
                    /** @var \App\Models\Contact\Manager $manager */
                    $manager = $ticket->type->area->manager;
                    $ticket->manager_ticket_id = $manager->getNextTicketId($ticket->valid_from->year);
                    $ticket->save();
                    $progress->advance();
                }
            });

            $progress->setMessage('Migration finished');
            $progress->finish();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        ManagerLastId::truncate();
        Ticket::where('manager_ticket_id', '!=', 0)->update(['manager_ticket_id' => 0]);
    }
}
