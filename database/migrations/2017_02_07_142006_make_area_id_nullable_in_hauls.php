<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeAreaIdNullableInHauls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE hauls CHANGE area_id area_id int(10) unsigned NULL');

        Schema::table('hauls', function (Blueprint $table) {
            $table->string('area_name')->nullable()->after('area_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE hauls CHANGE area_id area_id int(10) unsigned NOT NULL');

        Schema::table('hauls', function (Blueprint $table) {
            $table->dropColumn('area_name');
        });
    }
}
