<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaMetaAndMetaTypesAndParentAreasTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
        });

        Schema::create('meta_type_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('meta_type_id')->unsigned();
            $table->string('locale', 5)->index();
            $table->string('name');

            $table->unique(['meta_type_id', 'locale']);
            $table->foreign('meta_type_id')->references('id')->on('meta_types')->onDelete('cascade');
        });

        Schema::create('area_meta', function (Blueprint $table) {
            $table->integer('area_id')->unsigned();
            $table->integer('meta_type_id')->unsigned();
            $table->string('value');

            $table->primary(array('area_id', 'meta_type_id'));

            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
            $table->foreign('meta_type_id')->references('id')->on('meta_types')->onDelete('cascade');
        });

        Schema::create('parent_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();

            $table->foreign('type_id')->references('id')->on('area_types')->onDelete('restrict');
        });

        Schema::table('areas', function (Blueprint $table) {
            $table->integer('parent_area_id')->unsigned()->nullable()->after('id');
            $table->foreign('parent_area_id')->references('id')->on('parent_areas')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('meta_types');
        Schema::drop('meta_type_translations');
        Schema::drop('area_meta');
        Schema::drop('parent_areas');

        Schema::table('areas', function (Blueprint $table) {
            $table->dropForeign('parent_area_id_foreign');
            $table->dropColumn('parent_area_id');
        });
    }
}
