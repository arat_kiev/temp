<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesLastIdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices_last_ids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('year');
            $table->unsignedInteger('last_id');
        });

        Schema::table('invoices', function (Blueprint $table) {
            $table->unsignedInteger('invoice_id')->after('id');
        });

        $from = Carbon::createFromDate(2017, 1, 1)->startOfYear();
        $till = $from->copy()->endOfYear();

        DB::table('invoices')
            ->whereBetween('created_at', [$from, $till])
            ->update(['invoice_id' => DB::raw('id')]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn('invoice_id');
        });

        Schema::dropIfExists('invoices_last_ids');
    }
}
