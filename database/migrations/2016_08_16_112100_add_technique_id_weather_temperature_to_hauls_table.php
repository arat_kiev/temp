<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTechniqueIdWeatherTemperatureToHaulsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hauls', function (Blueprint $table) {
            $table->integer('technique_id')->unsigned()->nullable()->after('fish_id');
            $table->enum('weather', ['sunny', 'cloudy', 'rain', 'snow'])->nullable()->after('size_type');
            $table->tinyInteger('temperature')->nullable()->after('weather');

            $table->foreign('technique_id')->references('id')->on('techniques')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hauls', function (Blueprint $table) {
            $table->dropColumn('technique_id');
            $table->dropColumn('weather');
            $table->dropColumn('temperature');
        });
    }
}
