<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRestrictionsPivotDataToAreaFishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('area_fishes', function (Blueprint $table) {
            $table->date('closed_from')->nullable();
            $table->date('closed_till')->nullable();
            $table->integer('min_length')->nullable();
            $table->integer('max_length')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('area_fishes', function (Blueprint $table) {
            $table->dropColumn('closed_from');
            $table->dropColumn('closed_till');
            $table->dropColumn('min_length');
            $table->dropColumn('max_length');
        });
    }
}
