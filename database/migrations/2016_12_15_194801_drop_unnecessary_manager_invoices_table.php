<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUnnecessaryManagerInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $managerInvoices = DB::table('manager_invoices')->get();

        Schema::table('invoices', function (Blueprint $table) {
            $table->unsignedInteger('manager_id')->after('id');
            $table->unsignedInteger('reseller_id')->after('manager_id');
        });

        foreach ($managerInvoices as $managerInvoice) {
            DB::table('invoices')
                ->where('id', '=', $managerInvoice->invoice_id)
                ->update([
                    'manager_id'    => $managerInvoice->manager_id,
                    'reseller_id'   => $managerInvoice->reseller_id,
                ]);
        }

        Schema::drop('manager_invoices');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $managerInvoices = DB::table('invoices')
            ->select(['id', 'manager_id', 'reseller_id'])
            ->get();

        Schema::create('manager_invoices', function (Blueprint $table) {
            $table->unsignedInteger('invoice_id');
            $table->unsignedInteger('manager_id');
            $table->unsignedInteger('reseller_id');

            $table->primary(['invoice_id', 'manager_id', 'reseller_id']);
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');
            $table->foreign('manager_id')->references('id')->on('managers')->onDelete('cascade');
            $table->foreign('reseller_id')->references('id')->on('resellers')->onDelete('cascade');
        });

        foreach ($managerInvoices as $managerInvoice) {
            DB::table('manager_invoices')
                ->insert([
                    'invoice_id'    => $managerInvoice->id,
                    'manager_id'    => $managerInvoice->manager_id,
                    'reseller_id'   => $managerInvoice->reseller_id,
                ]);
        }

        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn('manager_id');
            $table->dropColumn('reseller_id');
        });
    }
}
