<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaRatingAndRatingCategoriesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rating_categories', function (Blueprint $table) {
            $table->increments('id');
        });

        Schema::create('rating_category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rating_category_id')->unsigned()->index();
            $table->string('locale', 5)->index();
            $table->string('name');
            $table->text('description')->nullable();

            $table->unique(['rating_category_id', 'locale']);
            $table->foreign('rating_category_id')->references('id')->on('rating_categories')->onDelete('cascade');
        });

        Schema::create('area_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('area_id')->unsigned()->index();
            $table->integer('rating_category_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('value');
            $table->timestamps();

            $table->unique(['area_id', 'rating_category_id', 'user_id']);
            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
            $table->foreign('rating_category_id')->references('id')->on('rating_categories')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('area_ratings');
        Schema::drop('rating_category_translations');
        Schema::drop('rating_categories');
    }
}
