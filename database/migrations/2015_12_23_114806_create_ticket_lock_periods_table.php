<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketLockPeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_lock_periods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_price_id')->unsigned();
            $table->date('lock_from');
            $table->date('lock_till');
            $table->nullableTimestamps();

            $table->foreign('ticket_price_id')->references('id')->on('ticket_prices')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_lock_periods');
    }
}
