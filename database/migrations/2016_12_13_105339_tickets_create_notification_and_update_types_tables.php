<?php

use App\Models\Ticket\TicketCategory;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TicketsCreateNotificationAndUpdateTypesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_types', function (Blueprint $table) {
            $table->unsignedTinyInteger('is_additional')->after('online_only')->default(0);
        });

        Schema::create('ticket_type_notifications', function (Blueprint $table) {
            $table->unsignedInteger('ticket_type_id')->index();
            $table->unsignedInteger('user_id')->index();

            $table->primary(['ticket_type_id', 'user_id']);

            $table->foreign('ticket_type_id')->references('id')->on('ticket_types')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        if (!TicketCategory::where('type', 'NOTIME')->first()) {
            $cat = new TicketCategory([
                'type' => 'NOTIME',
            ]);
            $cat->{'name:de_DE'} = 'Ohne Zeitbeschränkung';
            $cat->{'name:en_GB'} = 'No Time Limit';
            $cat->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_types', function (Blueprint $table) {
            $table->dropColumn('is_additional');
        });

        Schema::drop('ticket_type_notifications');
    }
}
