<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultMinSizeAndMaxSizeForFishes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fishes', function (Blueprint $table) {
            $table->decimal('min_size', 5, 2)->unsigned()->nullable()->after('latin');
            $table->decimal('max_size', 5, 2)->unsigned()->nullable()->after('min_size');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fishes', function (Blueprint $table) {
            $table->dropColumn('min_size');
            $table->dropColumn('max_size');
        });
    }
}
