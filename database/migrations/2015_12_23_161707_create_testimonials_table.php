<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testimonials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('manager_id')->unsigned()->nullable();
            $table->integer('picture_id')->unsigned();
            $table->string('person_name');
            $table->string('person_description')->nullable();
            $table->text('text');
            $table->integer('position')->unsigned()->default(0);
            $table->nullableTimestamps();

            $table->foreign('manager_id')->references('id')->on('managers');
            $table->foreign('picture_id')->references('id')->on('pictures');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('testimonials');
    }
}
