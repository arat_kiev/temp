<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrganizationRelationToCsvDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('csv_data', function (Blueprint $table) {
            $table->unsignedInteger('organization_id')->after('id');
            $table->foreign('organization_id')->references('id')->on('organizations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('csv_data', function (Blueprint $table) {
            $table->dropForeign('csv_data_organization_id_foreign');
            $table->dropColumn('organization_id');
        });
    }
}
