<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Collection;

class AddStateToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('state_id')->nullable()->after('city');

            $table->foreign('state_id')->references('id')->on('states')->onDelete('set null');
        });

        $userPostCodes = $this->fetchUserPostCodes();

        $statePostCodes = $this->fetchStatePostCodes($userPostCodes);

        $this->updateUserPostCodes($statePostCodes);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['state_id']);
            $table->dropColumn('state_id');
        });
    }

    private function fetchUserPostCodes()
    {
        return DB::table('users')
            ->select('post_code', 'country_id')
            ->whereNotNull('country_id')
            ->whereNotNull('post_code')
            ->where('post_code', '!=', '')
            ->get()
            ->unique();
    }

    private function fetchStatePostCodes(Collection $userPostCodes)
    {
        return DB::table('cities')
            ->select('post_code', 'country_id', 'state_id')
            ->join('regions', 'regions.id', '=', 'cities.region_id')
            ->join('states', 'states.id', '=', 'regions.state_id')
            ->whereIn('post_code', $userPostCodes->pluck('post_code')->unique()->toArray())
            ->whereIn('country_id', $userPostCodes->pluck('country_id')->unique()->toArray())
            ->get()
            ->unique();
    }

    private function updateUserPostCodes(Collection $statePostCodes)
    {
        foreach ($statePostCodes as $codeData) {
            DB::table('users')
                ->where('post_code', $codeData->post_code)
                ->where('country_id', $codeData->country_id)
                ->update(['state_id' => $codeData->state_id]);
        }
    }
}
