<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTicketTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('area_id')->unsigned();
            $table->integer('ticket_category_id')->unsigned();
            $table->integer('duration')->unsigned()->default(1);

            $table->boolean('day_starts_at_sunrise')->default(false);
            $table->boolean('day_starts_at_sunset')->default(false);
            $table->time('day_starts_at_time')->nullable();
            $table->boolean('day_ends_at_sunrise')->default(false);
            $table->boolean('day_ends_at_sunset')->default(false);
            $table->time('day_ends_at_time')->nullable();

            $table->integer('checkin_id')->unsigned()->nullable();
            $table->integer('checkin_max')->unsigned()->nullable();

            $table->integer('quota_id')->unsigned()->nullable();
            $table->integer('quota_max')->unsigned()->nullable();

            $table->integer('fish_per_day')->unsigned()->nullable();
            $table->integer('fishing_days')->unsigned()->nullable();

            $table->boolean('online_only')->default(false);

            $table->nullableTimestamps();
            $table->softDeletes();

            $table->foreign('area_id')->references('id')->on('areas')->onDelete('restrict');
            $table->foreign('ticket_category_id')->references('id')->on('ticket_categories')->onDelete('restrict');
            $table->foreign('checkin_id')->references('id')->on('checkin_units')->onDelete('restrict');
            $table->foreign('quota_id')->references('id')->on('quota_units')->onDelete('restrict');
        });

        Schema::create('ticket_type_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_type_id')->unsigned();
            $table->string('locale', 5)->index();
            $table->string('name');

            $table->unique(['ticket_type_id', 'locale']);
            $table->foreign('ticket_type_id')->references('id')->on('ticket_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_type_translations');
        Schema::drop('ticket_types');
    }
}
