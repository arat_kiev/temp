<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCatchlogFlagOnTicketTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_types', function (Blueprint $table) {
            $table->boolean('is_catchlog_required')->default(false)->after('is_additional');
        });

        DB::table('ticket_types')
            ->where('is_additional', false)
            ->update(['is_catchlog_required' => true]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_types', function (Blueprint $table) {
            $table->dropColumn('is_catchlog_required');
        });
    }
}
