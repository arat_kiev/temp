<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('approved')->default(false);
            $table->boolean('public')->default(false);
            $table->boolean('lease')->default(false);
            $table->string('name')->index();
            $table->nullableTimestamps();
        });

        Schema::create('area_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('area_id')->unsigned();
            $table->string('locale', 5)->index();
            $table->text('description')->default('');
            $table->text('borders')->default('');

            $table->unique(['area_id', 'locale']);
            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('area_translations');
        Schema::drop('areas');
    }
}
