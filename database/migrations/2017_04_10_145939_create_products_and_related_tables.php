<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Ticket\TicketType;
use App\Models\Ticket\TicketPrice;
use Carbon\Carbon;

class CreateProductsAndRelatedTables extends Migration
{
    private $ticketPriceToProductPriceMap = [];

    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up()
    {
        Schema::create('fee_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->nullableTimestamps();
        });

        Schema::create('fee_category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id')->index();
            $table->string('locale', 5)->index();
            $table->string('name');

            $table->unique(['category_id', 'locale']);
            $table->foreign('category_id')->references('id')->on('fee_categories')->onDelete('cascade');
        });

        Schema::create('fees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('value');
            $table->nullableTimestamps();

            $table->foreign('category_id')->references('id')->on('fee_categories')->onDelete('restrict');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('restrict');
        });

        Schema::create('fee_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fee_id')->index();
            $table->string('locale', 5)->index();
            $table->string('name');

            $table->unique(['fee_id', 'locale']);
            $table->foreign('fee_id')->references('id')->on('fees')->onDelete('cascade');
        });

        Schema::create('product_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('picture_id')->nullable();
            $table->nullableTimestamps();

            $table->foreign('picture_id')->references('id')->on('pictures')->onDelete('set null');
        });

        Schema::create('product_category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id')->index();
            $table->string('locale', 5)->index();
            $table->string('name');
            $table->text('short_description')->nullable();
            $table->text('long_description')->nullable();

            $table->unique(['category_id', 'locale']);
            $table->foreign('category_id')->references('id')->on('product_categories')->onDelete('cascade');
        });

        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id')->index();
            $table->text('template')->nullable();
            $table->unsignedInteger('gallery_id')->nullable()->index();
            $table->dateTime('featured_from')->nullable();
            $table->dateTime('featured_till')->nullable();
            $table->text('attachment')->nullable();
            $table->boolean('draft')->default(false);
            $table->unsignedInteger('drafted_by')->index()->nullable();
            $table->nullableTimestamps();

            $table->foreign('category_id')->references('id')->on('product_categories')->onDelete('cascade');
            $table->foreign('gallery_id')->references('id')->on('galleries')->onDelete('cascade');
            $table->foreign('drafted_by')->references('id')->on('users')->onDelete('set null');
        });

        Schema::create('product_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->index();
            $table->string('locale', 5)->index();
            $table->string('name');
            $table->text('short_description')->nullable();
            $table->text('long_description')->nullable();

            $table->unique(['product_id', 'locale']);
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });

        Schema::create('product_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->index();
            $table->integer('value');
            $table->unsignedInteger('vat');
            $table->dateTime('visible_from')->nullable();
            $table->dateTime('valid_from')->nullable();
            $table->dateTime('valid_till')->nullable();
            $table->nullableTimestamps();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('restrict');
        });

        Schema::create('product_fees', function (Blueprint $table) {
            $table->unsignedInteger('product_price_id')->index();
            $table->unsignedInteger('fee_id')->index();

            $table->foreign('product_price_id')->references('id')->on('product_prices')->onDelete('cascade');
            $table->foreign('fee_id')->references('id')->on('fees')->onDelete('cascade');
        });

        Schema::create('product_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->index();
            $table->unsignedInteger('user_id')->index()->nullable();
            $table->unsignedInteger('reseller_id')->index()->nullable();
            $table->unsignedInteger('client_id')->index()->nullable();
            $table->unsignedInteger('price_id')->index();
            $table->enum('status', ['RECEIVED', 'SHIPPING', 'SHIPPED'])->default('RECEIVED');
            $table->string('email')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('street')->nullable();
            $table->string('post_code', 32)->nullable();
            $table->string('city')->nullable();
            $table->unsignedInteger('country_id')->nullable();
            $table->unsignedInteger('invoice_id')->nullable();
            $table->unsignedInteger('storno_id')->nullable()->index();  // Relation to id, which was deleted
            $table->boolean('is_deleted')->default(false);
            $table->text('storno_reason')->default('');
            $table->string('offisy_id')->nullable();
            $table->string('offisy_code')->nullable();
            $table->text('offisy_tags')->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();                                      // This will add a deleted_at field

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('reseller_id')->references('id')->on('resellers')->onDelete('set null');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('set null');
            $table->foreign('price_id')->references('id')->on('product_prices')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null');
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('set null');
        });

        Schema::create('has_products', function (Blueprint $table) {
            $table->unsignedInteger('product_id')->index();
            $table->unsignedInteger('related_id')->index();
            $table->string('related_type')->index();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });

        $this->migrateDataFromTicketTypesToProducts();

        Schema::table('ticket_types', function (Blueprint $table) {
            $table->dropColumn('is_additional');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('ticket_types', 'is_additional')) {
            Schema::table('ticket_types', function (Blueprint $table) {
                $table->boolean('is_additional')->after('online_only')->default(false);
            });
        }

        Schema::dropIfExists('has_products');
        Schema::dropIfExists('product_sales');
        Schema::dropIfExists('product_fees');
        Schema::dropIfExists('product_prices');
        Schema::dropIfExists('product_translations');
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_category_translations');
        Schema::dropIfExists('product_categories');
        Schema::dropIfExists('fee_translations');
        Schema::dropIfExists('fees');
        Schema::dropIfExists('fee_category_translations');
        Schema::dropIfExists('fee_categories');
    }

    private function migrateDataFromTicketTypesToProducts()
    {
        try {
            DB::beginTransaction();

            $additionalTickets = TicketType::where('is_additional', true);
            $categoryId = $this->createProductCategory();
            DB::table('ticket_categories')->where('type', 'NOTIME')->delete();

            foreach ($additionalTickets->get() as $additionalTicket) {
                foreach ($additionalTicket->prices as $ticketPrice) {
                    $productId = $this->insertNewProduct($categoryId, $additionalTicket, $ticketPrice);
                    $this->addProductPrices($productId, $additionalTicket, $ticketPrice);
                    $this->addProductSales($productId, $additionalTicket);
                    $this->fillHasProducts($productId, $additionalTicket);
                }

                $this->forceDeleteRelatedInstances($additionalTicket);
            }

            $additionalTickets->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->down();
            throw $e;
        }
    }

    private function createProductCategory()
    {
        $categoryId = DB::table('product_categories')->insertGetId([
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        DB::table('product_category_translations')->insert([
            [
                'category_id'   => $categoryId,
                'locale'        => 'de_DE',
                'name'          => 'Ohne Zeitbeschränkung',
            ], [
                'category_id'   => $categoryId,
                'locale'        => 'en_GB',
                'name'          => 'No Time Limit',
            ]
        ]);

        return $categoryId;
    }

    private function insertNewProduct($categoryId, TicketType $additionalTicket, TicketPrice $ticketPrice)
    {
        $productId = DB::table('products')->insertGetId([
            'category_id'   => $categoryId,
            'created_at'    => $additionalTicket->created_at,
            'updated_at'    => Carbon::now(),
        ]);

        $translatableData = [];
        $ticketPriceTranslations = $ticketPrice->translations;

        foreach ($additionalTicket->translations as $translation) {
            $priceName = $ticketPriceTranslations->reduce(function ($carry, $item) use ($translation) {
                return $translation->locale === $item->locale ? $carry . $item->name : $carry;
            }, '');
            $translatableData[] = [
                'product_id'    => $productId,
                'locale'        => $translation->locale,
                'name'          => $translation->name . ($priceName ? ' - ' . $priceName : ''),
            ];
        }

        if ($translatableData) {
            DB::table('product_translations')->insert($translatableData);
        }

        return $productId;
    }

    private function addProductPrices($productId, TicketType $additionalTicket, TicketPrice $ticketPrice)
    {
        if (!isset($this->ticketPriceToProductPriceMap[$ticketPrice->id])) {
            $priceId = DB::table('product_prices')->insertGetId([
                'product_id'    => $productId,
                'visible_from'  => $ticketPrice->visible_from,
                'valid_from'    => $ticketPrice->valid_from,
                'valid_till'    => $ticketPrice->valid_to,
                'value'         => $ticketPrice->value,
                'vat'           => $additionalTicket->area->manager->country->vat,
                'created_at'    => $ticketPrice->created_at,
                'updated_at'    => Carbon::now(),
            ]);

            $this->ticketPriceToProductPriceMap[$ticketPrice->id] = $priceId;
        }
    }

    private function addProductSales($productId, TicketType $additionalTicket)
    {
        $ticketsMap = [];

        foreach ($additionalTicket->tickets as $ticket) {
            $saleId = DB::table('product_sales')->insertGetId([
                'product_id'    => $productId,
                'user_id'       => $ticket->user_id,
                'reseller_id'   => $ticket->resellerTicket ? $ticket->resellerTicket->reseller->id : null,
                'client_id'     => $ticket->client_id,
                'price_id'      => $this->ticketPriceToProductPriceMap[$ticket->ticket_price_id],
                'email'         => $ticket->user ? $ticket->user->email : null,
                'first_name'    => $ticket->user ? $ticket->user->first_name : null,
                'last_name'     => $ticket->user ? $ticket->user->last_name : null,
                'street'        => $ticket->user ? $ticket->user->street : null,
                'post_code'     => $ticket->user ? $ticket->user->post_code : null,
                'city'          => $ticket->user ? $ticket->user->city : null,
                'country_id'    => $ticket->user ? $ticket->user->country_id : null,
                'storno_id'     => $ticket->storno_id
                    ? (isset($ticketsMap[$ticket->storno_id])
                        ? $ticketsMap[$ticket->storno_id]
                        : null)
                    : $ticket->storno_id,
                'is_deleted'    => $ticket->is_deleted,
                'created_at'    => $ticket->created_at,
                'updated_at'    => $ticket->updated_at,
            ]);

            $ticketsMap[$ticket->id] = $saleId;
            $ticket->delete();
        }
    }

    private function fillHasProducts($productId, TicketType $additionalTicket)
    {
        DB::table('has_products')->insert([
            [
                'product_id'    => $productId,
                'related_id'    => $additionalTicket->area_id,
                'related_type'  => 'areas',
            ], [
                'product_id'    => $productId,
                'related_id'    => $additionalTicket->area->manager_id,
                'related_type'  => 'managers',
            ],
        ]);
    }

    private function forceDeleteRelatedInstances(TicketType $additionalTicket)
    {
        foreach ($additionalTicket->tickets as $ticket) {
            $ticket->hauls()->delete();
        }
        $additionalTicket->tickets()->forceDelete();

        foreach ($additionalTicket->prices as $price) {
            $price->tickets()->forceDelete();
        }
        $additionalTicket->prices()->delete();
    }
}
