<?php

use App\Models\Area\Area;
use App\Models\Contact\Reseller;
use Illuminate\Database\Migrations\Migration;

class MigrateManagerResellersData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Reseller::chunk(50, function ($resellers) {
            /** @var Reseller $reseller */
            foreach ($resellers as $reseller) {
                /** @var Area $area */
                foreach ($reseller->areas as $area) {
                    if (!$reseller->managers()->find($area->manager->id)) {
                        $reseller->managers()->attach($area->manager);
                    }
                }
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
