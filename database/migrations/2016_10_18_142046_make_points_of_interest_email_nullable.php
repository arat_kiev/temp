<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakePointsOfInterestEmailNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('points_of_interest', function (Blueprint $table) {
            $table->string('street')->nullable()->change();
            $table->string('post_code')->nullable()->change();
            $table->string('city')->nullable()->change();
            $table->integer('country_id')->unsigned()->nullable()->change();
            $table->integer('picture_id')->unsigned()->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->string('phone')->nullable()->change();
            $table->string('website')->nullable()->change();

            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null');
            $table->foreign('picture_id')->references('id')->on('pictures')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('points_of_interest', function (Blueprint $table) {
            $table->dropForeign('points_of_interest_country_id_foreign');
            $table->dropForeign('points_of_interest_picture_id_foreign');
        });
    }
}
