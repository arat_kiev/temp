<?php

use App\Models\Meta\Technique;
use App\Models\Gallery;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPicturesAndGalleriesToTechniques extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('techniques', function (Blueprint $table) {
            $table->unsignedInteger('picture_id')->after('id')->nullable();
            $table->unsignedInteger('gallery_id')->after('picture_id')->nullable();

            $table->foreign('picture_id')->references('id')->on('pictures')->onDelete('set null');
            $table->foreign('gallery_id')->references('id')->on('galleries')->onDelete('set null');
        });

        Technique::each(function ($technique) {
            $gallery = Gallery::create([
                'name' => $technique->hasTranslation('de_DE')
                    ? $technique->translate('de_DE')->name
                    : 'Technik #' . $technique->id,
            ]);
            $technique->gallery()->associate($gallery);
            $technique->save();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Technique::each(function ($technique) {
            $technique->gallery()->delete();
            $technique->picture()->delete();
        });

        Schema::table('techniques', function (Blueprint $table) {
            $table->dropForeign(['picture_id']);
            $table->dropForeign(['gallery_id']);
            $table->dropColumn('picture_id');
            $table->dropColumn('gallery_id');
        });
    }
}
