<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoFieldsToAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('areas', function (Blueprint $table) {
            $table->tinyInteger('rods_max')->unsigned()->after('account_number');
            $table->boolean('member_only')->after('rods_max')->default(false);
            $table->boolean('boat')->after('member_only')->default(false);
            $table->boolean('camping')->after('boat')->default(false);
            $table->boolean('nightfishing')->after('camping')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('areas', function (Blueprint $table) {
            $table->dropColumn('rods_max');
            $table->dropColumn('member_only');
            $table->dropColumn('boat');
            $table->dropColumn('camping');
            $table->dropColumn('nightfishing');
        });
    }
}
