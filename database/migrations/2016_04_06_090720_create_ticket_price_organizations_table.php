<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketPriceOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_price_organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_price_id')->unsigned();
            $table->integer('organization_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('ticket_price_id')->references('id')->on('ticket_prices')->onDelete('cascade');
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_price_organizations');
    }
}
