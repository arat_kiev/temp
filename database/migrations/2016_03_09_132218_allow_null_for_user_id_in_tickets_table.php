<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllowNullForUserIdInTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            DB::statement('ALTER TABLE tickets MODIFY COLUMN user_id INT(10) unsigned DEFAULT NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            DB::statement('ALTER TABLE tickets MODIFY COLUMN user_id INT(10) unsigned NOT NULL');
        });
    }
}
