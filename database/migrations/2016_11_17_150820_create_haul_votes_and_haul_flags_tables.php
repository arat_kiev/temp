<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHaulVotesAndHaulFlagsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('haul_votes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('haul_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->tinyInteger('vote')->default(0)->index();
            $table->timestamps();

            $table->unique(['haul_id', 'user_id']);
            $table->foreign('haul_id')->references('id')->on('hauls')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('haul_flags', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('haul_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->nullable()->index(); // nullable for system reports
            $table->text('description')->nullable();
            $table->timestamps();

            $table->unique(['haul_id', 'user_id']);
            $table->foreign('haul_id')->references('id')->on('hauls')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('hauls', function (Blueprint $table) {
            $table->enum('status', ['accepted', 'review', 'rejected'])->default('accepted')->after('picture_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hauls', function ($table) {
            $table->dropColumn('status');
        });

        Schema::drop('haul_flags');
        Schema::drop('haul_votes');
    }
}
