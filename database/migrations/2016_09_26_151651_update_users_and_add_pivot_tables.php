<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersAndAddPivotTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('name_public')->default(0)->after('remember_token');
            $table->boolean('favorite_areas_public')->default(0)->after('name_public');
            $table->boolean('followers_public')->default(0)->after('favorite_areas_public');
            $table->string('about_me')->nullable()->after('fisher_id');
            $table->integer('hero_picture_id')->unsigned()->nullable()->after('country_id');
            $table->integer('avatar_picture_id')->unsigned()->nullable()->after('hero_picture_id');

            $table->foreign('hero_picture_id')->references('id')->on('pictures')->onDelete('set null');
            $table->foreign('avatar_picture_id')->references('id')->on('pictures')->onDelete('set null');
        });

        Schema::create('user_fishes', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index();
            $table->integer('fish_id')->unsigned()->index();

            $table->primary(['user_id', 'fish_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('fish_id')->references('id')->on('fishes')->onDelete('cascade');
        });

        Schema::create('user_techniques', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index();
            $table->integer('technique_id')->unsigned()->index();

            $table->primary(['user_id', 'technique_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('technique_id')->references('id')->on('techniques')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_hero_picture_id_foreign');
            $table->dropForeign('users_avatar_picture_id_foreign');
            $table->dropColumn('name_public');
            $table->dropColumn('favorite_areas_public');
            $table->dropColumn('followers_public');
            $table->dropColumn('about_me');
            $table->dropColumn('hero_picture_id');
            $table->dropColumn('avatar_picture_id');
        });

        Schema::drop('user_fishes');
        Schema::drop('user_techniques');
    }
}
