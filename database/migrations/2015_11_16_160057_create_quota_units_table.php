<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuotaUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quota_units', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->nullableTimestamps();
        });

        Schema::create('quota_unit_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quota_unit_id')->unsigned();
            $table->string('locale', 5)->index();
            $table->string('name');

            $table->unique(['quota_unit_id', 'locale']);
            $table->foreign('quota_unit_id')->references('id')->on('quota_units')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quota_unit_translations');
        Schema::drop('quota_units');
    }
}
