<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaAdditionalManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_additional_managers', function (Blueprint $table) {
            $table->unsignedInteger('area_id')->index();
            $table->unsignedInteger('manager_id')->index();
            $table->unsignedSmallInteger('percentage')->nullable(false);
            $table->boolean('public')->default(true);

            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
            $table->foreign('manager_id')->references('id')->on('managers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('area_additional_managers');
    }
}
