<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fishes', function (Blueprint $table) {
            $table->increments('id');
            $table->nullableTimestamps();
        });

        Schema::create('fish_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fish_id')->unsigned();
            $table->string('locale', 5)->index();
            $table->string('name');
            $table->text('description')->nullable();

            $table->unique(['fish_id', 'locale']);
            $table->foreign('fish_id')->references('id')->on('fishes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fish_translations');
        Schema::drop('fishes');
    }
}
