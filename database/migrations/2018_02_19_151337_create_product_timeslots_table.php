<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTimeslotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_timeslots', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_price_id');
            $table->time('from');
            $table->time('till');
            $table->unsignedInteger('default_pool');
            $table->timestamps();

            $table->foreign('product_price_id')->references('id')->on('product_prices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_timeslots');
    }
}
