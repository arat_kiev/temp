<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicenseTypeStatesPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_type_states', function (Blueprint $table) {
            $table->unsignedInteger('license_type_id');
            $table->unsignedInteger('state_id');
            $table->timestamps();

            $table->foreign('license_type_id')->references('id')->on('license_types')->onDelete('cascade');
            $table->foreign('state_id')->references('id')->on('states')->onDelete('cascade');
        });

        Schema::table('license_types', function (Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('license_types', function (Blueprint $table) {
            $table->dropTimestamps();
        });
        Schema::dropIfExists('license_type_states');
    }
}
