<?php

use App\Models\Contact\Manager;
use App\Models\Picture;
use Illuminate\Database\Seeder;
use Symfony\Component\HttpFoundation\File\File;

class ManagerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $master = Manager::create([
            'name' => 'Master Manager',
            'street' => 'Demostraße 13',
            'area_code' => '3421',
            'city' => 'Demostadt',
            'person' => 'Herr Demo',
            'phone' => '0043512164984',
            'email' => 'master@bewirtschafter.at',
            'website' => 'https://www.bissanzeiger.net',
            'ticket_info' => 'Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.',
        ]);

        $master->country()->associate(1);

        $manager = new Manager([
            'name' => 'Demo Manager 1',
            'street' => 'Demostraße 13',
            'area_code' => '3421',
            'city' => 'Demostadt',
            'person' => 'Herr Demo',
            'phone' => '0043512164984',
            'email' => 'demo@bewirtschafter.at',
            'website' => 'https://www.bissanzeiger.net',
            'ticket_info' => 'Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.',
        ]);

        $manager->country()->associate(1);

        $logoPath = database_path('data/images/manager_logo.png');
        $logoFile = new File($logoPath, \File::name($logoPath));
        
        $logo = Picture::create(['file' => $logoFile]);
        $manager->logo()->associate($logo);
        $manager->parent()->associate($master);

        $manager->save();
    }
}
