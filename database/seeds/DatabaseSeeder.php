<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        File::copyDirectory(database_path('data/images_orig'), database_path('data/images'));
        File::copyDirectory(database_path('data/files_orig'), database_path('data/files'));

        $this->call(ClientTableSeeder::class);

        $this->call(CountryTableSeeder::class);
        $this->call(StateTableSeeder::class);
        $this->call(RegionTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(GeolocationTableSeeder::class);

        $this->call(QuotaUnitTableSeeder::class);
        $this->call(CheckinUnitTableSeeder::class);
        $this->call(TicketCategoryTableSeeder::class);

        $this->call(PermissionTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);

        $this->call(FishTableSeeder::class);
        $this->call(TechniqueTableSeeder::class);
        $this->call(AreaTypeTableSeeder::class);
        $this->call(RuleTableSeeder::class);

        $this->call(ManagerTableSeeder::class);
        $this->call(OrganizationTableSeeder::class);

        $this->call(GalleryTableSeeder::class);
        $this->call(PictureTableSeeder::class);

        $this->call(AreaTableSeeder::class);
        $this->call(FavoritesTableSeeder::class);

        $this->call(LicenseTypeTableSeeder::class);
        $this->call(LicenseTableSeeder::class);

        $this->call(TicketTypeTableSeeder::class);
        $this->call(TicketTableSeeder::class);

        $this->call(HaulTableSeeder::class);
        $this->call(TestimonialTableSeeder::class);

        File::deleteDirectory(database_path('data/images'));
        File::deleteDirectory(database_path('data/files'));

        Model::reguard();
    }
}
