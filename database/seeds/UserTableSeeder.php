<?php

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $u1 = User::create([
            'active' => true,
            'name' => 'User 1',
            'email' => 'user1@example.com',
            'password' => 'user1pass',
            'first_name' => 'Rene',
            'last_name' => 'Reiter',
            'street' => 'Schnallentorweg 1/6',
            'post_code' => '4400',
            'city' => 'Steyr',
            'birthday' => Carbon::createFromDate(1984, 11, 27),
        ]);
        $u1->country()->associate(1);
        $u1->save();
        $u1->assignRole('superadmin');

        $u2 = User::create([
            'active' => true,
            'name' => 'User 2',
            'email' => 'user2@example.com',
            'password' => 'user2pass',
            'first_name' => 'Jürgen',
            'last_name' => 'Reiter',
            'street' => 'Hofmaygasse 6',
            'post_code' => '4407',
            'city' => 'Dietachdorf',
            'birthday' => Carbon::createFromDate(1995, 7, 1),
        ]);
        $u2->country()->associate(1);
        $u2->save();
        $u2->assignRole('manager');
    }
}
