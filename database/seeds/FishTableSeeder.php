<?php

use App\Models\Meta\Fish;

class FishTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fishes = $this->fromJson(database_path('data/fishes.json'));

        foreach ($fishes as $fish) {
            Fish::create($fish);
        }
    }
}
