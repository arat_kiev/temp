<?php

use App\Models\Area\AreaType;

class AreaTypeTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = $this->fromJson(database_path('data/area_types.json'));

        foreach ($types as $type) {
            AreaType::create($type);
        }
    }
}
