<?php

use App\Models\Ticket\TicketCategory as TicketCategory;

class TicketCategoryTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = $this->fromJson(database_path('data/ticket_categories.json'));

        foreach ($categories as $category) {
            TicketCategory::create($category);
        }
    }
}
