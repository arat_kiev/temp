<?php

use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketType;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TicketTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tt = TicketType::all()->first();

        $t1 = new Ticket([
            'valid_from' => Carbon::createFromDate(2016, 5, 13),
            'valid_to' => Carbon::createFromDate(2016, 5, 13),
        ]);
        $t1->type()->associate($tt);
        $t1->price()->associate($tt->prices()->first());
        $t1->user()->associate(1);
        $t1->save();

        $t2 = new Ticket([
            'valid_from' => Carbon::createFromDate(2016, 5, 23),
            'valid_to' => Carbon::createFromDate(2016, 5, 23),
        ]);
        $t2->type()->associate($tt);
        $t2->price()->associate($tt->prices()->first());
        $t2->user()->associate(2);
        $t2->save();
    }
}
