<?php

use App\Models\Ticket\CheckinUnit;

class CheckinUnitTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = $this->fromJson(database_path('data/checkin_units.json'));

        foreach ($units as $unit) {
            CheckinUnit::create($unit);
        }
    }
}
