<?php

use App\Models\Picture;
use App\Models\Testimonial;
use Illuminate\Database\Seeder;
use Symfony\Component\HttpFoundation\File\File;

class TestimonialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('de_AT');

        $p1 = Picture::create([
            'file' => new File(database_path('data/images/testimonial-1.jpg'), \File::name(database_path('data/images/testimonial-1.jpg'))),
        ]);

        $p2 = Picture::create([
            'file' => new File(database_path('data/images/testimonial-2.jpg'), \File::name(database_path('data/images/testimonial-2.jpg'))),
        ]);

        $t1 = new Testimonial([
            'person_name' => 'Christoph Dunzinger',
            'text' => $faker->paragraph,
        ]);
        $t1->picture()->associate($p1);
        $t1->save();

        $t2 = new Testimonial([
            'person_name' => 'Andreas Zachbauer',
            'text' => $faker->paragraph,
        ]);
        $t2->picture()->associate($p2);
        $t2->save();
    }
}
