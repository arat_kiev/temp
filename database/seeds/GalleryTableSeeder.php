<?php

use App\Models\Gallery;

class GalleryTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $galleries = $this->fromJson(database_path('data/galleries.json'));

        $progress = $this->getProgressbar();
        $progress->start(count($galleries));

        foreach ($galleries as $gallery) {
            Gallery::create($gallery);

            $progress->advance();
        }

        $progress->finish();
    }
}
