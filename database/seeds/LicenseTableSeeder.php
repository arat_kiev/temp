<?php

use App\Models\License\License;
use Symfony\Component\HttpFoundation\File\File;

class LicenseTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $licenses = $this->fromJson(database_path('data/licenses.json'));

        $progress = $this->getProgressbar();
        $progress->start(count($licenses));

        foreach ($licenses as $license) {
            $newLicense = new License();
            $newLicense->fields = $license['fields'];
            $newLicense->type()->associate($license['license_type_id']);
            $newLicense->user()->associate($license['user_id']);
            $newLicense->save();

            foreach ($license['attachments'] as $attachment) {
                $newLicense->attachments()->create([
                    'name' => '',
                    'description' => '',
                    'file' => new File(database_path($attachment), \File::name(database_path($attachment))),
                ]);
            }

            $progress->advance();
        }

        $progress->finish();
    }
}
