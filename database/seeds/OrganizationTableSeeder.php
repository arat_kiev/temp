<?php

use App\Models\Contact\Manager;
use App\Models\Organization\Organization;
use App\Models\Picture;
use Illuminate\Database\Seeder;
use Symfony\Component\HttpFoundation\File\File;

class OrganizationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $organization = new Organization([
            'name' => 'Demo Verein 1',
            'street' => 'Demostraße 13',
            'area_code' => '3421',
            'city' => 'Demostadt',
            'person' => 'Herr Demo',
            'phone' => '0043512164984',
            'email' => 'demo@fischereiverein.at',
            'website' => 'https://www.bissanzeiger.net',
        ]);

        $logoPath = database_path('data/images/verein_logo.jpg');
        $logoFile = new File($logoPath, \File::name($logoPath));

        $logo = Picture::create(['file' => $logoFile]);
        $organization->logo()->associate($logo);
        $organization->manager()->associate(Manager::find(1));

        $organization->save();
    }
}
