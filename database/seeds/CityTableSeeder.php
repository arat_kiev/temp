<?php

use App\Models\Location\City;

class CityTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = $this->fromJson(database_path('data/cities.json'));

        $progress = $this->getProgressbar();
        $progress->start(count($cities));

        foreach ($cities as $city) {
            City::create($city);

            $progress->advance();
        }

        $progress->finish();
    }
}
