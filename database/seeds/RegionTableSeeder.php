<?php

use App\Models\Location\Region;

class RegionTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = $this->fromJson(database_path('data/regions.json'));

        $progress = $this->getProgressbar();
        $progress->start(count($regions));

        foreach ($regions as $region) {
            Region::create($region);

            $progress->advance();
        }

        $progress->finish();
    }
}
