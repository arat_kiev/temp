<?php

use Illuminate\Database\Seeder;
use App\Models\Inspection\ObservationCategory;

class ObservationCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ObservationCategory::updateOrCreate(['type' => 'GENERAL']);
        ObservationCategory::updateOrCreate(['type' => 'PREDATOR']);
        ObservationCategory::updateOrCreate(['type' => 'CONTAMINATION']);
    }
}
