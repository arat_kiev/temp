<?php

use App\Models\Meta\Technique;

class TechniqueTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $techniques = $this->fromJson(database_path('data/techniques.json'));

        foreach ($techniques as $technique) {
            Technique::create($technique);
        }
    }
}
