<?php

use App\Models\Picture;
use Symfony\Component\HttpFoundation\File\File;

class PictureTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pictures = $this->fromJson(database_path('data/pictures.json'));

        $progress = $this->getProgressbar();
        $progress->start(count($pictures));

        foreach ($pictures as $picture) {
            $p = Picture::create([
                'name' => $picture['name'],
                'description' => $picture['description'],
                'file' => new File(database_path($picture['file']), \File::name(database_path($picture['file']))),
            ]);

            $p->galleries()->sync($picture['galleries']);

            if (!is_null($picture['author'])) {
                $p->author()->associate($picture['author']);
                $p->save();
            }

            $progress->advance();
        }

        $progress->finish();
    }
}
