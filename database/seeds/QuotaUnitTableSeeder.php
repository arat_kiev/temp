<?php

use App\Models\Ticket\QuotaUnit;

class QuotaUnitTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = $this->fromJson(database_path('data/quota_units.json'));

        foreach ($units as $unit) {
            QuotaUnit::create($unit);
        }
    }
}
