<?php

use App\Models\Client;
use App\Models\Haul;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class HaulTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Config::set('app.client', Client::find(1));

        /** @var User $user */
        $user = User::find(1);

        $haul = new Haul([
            'public' => false,
            'taken' => true,
            'size_value' => 0.85,
            'size_type' => 'KG',
            'user_comment' => 'Testfang 1',
            'catch_date' => Carbon::create(2015, 07, 01),
            'catch_time' => Carbon::create(null, null, null, 14, 05),
            'ticket_number' => 'TEST-TICKET',
        ]);

        $haul->area()->associate(1);
        $haul->fish()->associate(2);
        $user->hauls()->save($haul);

        $haul = new Haul([
            'public' => true,
            'taken' => true,
            'size_value' => 55,
            'size_type' => 'CM',
            'user_comment' => 'Testfang 2',
            'catch_date' => Carbon::create(2015, 07, 01),
            'catch_time' => Carbon::create(null, null, null, 15, 45),
            'ticket_number' => 'TEST-TICKET',
        ]);

        $haul->area()->associate(1);
        $haul->fish()->associate(5);
        $user->hauls()->save($haul);

        $haul = new Haul([
            'public' => true,
            'taken' => false,
            'size_value' => null,
            'size_type' => null,
            'user_comment' => 'Leermeldung',
            'catch_date' => Carbon::create(2015, 07, 02),
            'catch_time' => null,
            'ticket_number' => 'TEST-TICKET',
        ]);

        $haul->area()->associate(1);
        $user->hauls()->save($haul);
    }
}
