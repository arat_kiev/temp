<?php

use App\Models\Authorization\Permission;

class PermissionTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = $this->fromJson(database_path('data/permissions.json'));

        $progress = $this->getProgressbar();
        $progress->start(count($permissions));

        foreach ($permissions as $permission) {
            Permission::create($permission);

            $progress->advance();
        }

        $progress->finish();
    }
}
