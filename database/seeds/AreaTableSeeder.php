<?php

use App\Models\Area\Area;
use App\Models\Client;
use App\Models\Location\City;
use App\Models\Contact\Manager;

class AreaTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $faker = Faker\Factory::create('de_AT');

        $bissanzeiger = Client::where('name', 'bissanzeiger')->first();
        $bundesforste = Client::where('name', 'bundesforste')->first();

        $manager = Manager::where('name', 'like', 'Demo%')->first();

        $a1 = $manager->areas()->create([
            'approved' => true,
            'public' => true,
            'lease' => false,
            'name' => 'Demo Gewässer 1',
            'description' => $faker->paragraph(10),
            'borders' => $faker->paragraph(3),
            'ticket_info' => 'Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. Dynamically innovate resource-leveling customer service for state of the art customer service.',
        ]);
        $a1->clients()->attach($bissanzeiger);
        $a1->clients()->attach($bundesforste);
        $a1->type()->associate(2);
        $a1->fishes()->sync([1, 2, 3, 4]);
        $a1->techniques()->sync([1, 2, 3, 4]);
        $a1->gallery()->associate(1);
        $a1->rule()->associate(1);
        $a1->save();

        $a2 = $manager->areas()->create([
            'approved' => true,
            'public' => true,
            'lease' => false,
            'name' => 'Demo Gewässer 2',
            'description' => $faker->paragraph(10),
            'borders' => $faker->paragraph(3),
            'ticket_info' => 'Objectively innovate empowered manufactured products whereas parallel platforms. Holisticly predominate extensible testing procedures for reliable supply chains. Dramatically engage top-line web services vis-a-vis cutting-edge deliverables.',
        ]);
        $a2->clients()->attach($bissanzeiger);
        $a2->type()->associate(2);
        $a2->fishes()->sync([4, 6, 8]);
        $a2->techniques()->sync([4, 6, 9, 14]);
        $a2->gallery()->associate(2);
        $a2->rule()->associate(2);
        $a2->save();

        $a3 = $manager->areas()->create([
            'approved' => false,
            'public' => false,
            'lease' => false,
            'name' => 'Privates Gewässer 1',
            'description' => $faker->paragraph(10),
            'borders' => $faker->paragraph(3),
            'ticket_info' => 'Proactively envisioned multimedia based expertise and cross-media growth strategies. Seamlessly visualize quality intellectual capital without superior collaboration and idea-sharing. Holistically pontificate installed base portals after maintainable products.',
        ]);
        $a3->clients()->attach($bissanzeiger);
        $a3->type()->associate(2);
        $a3->fishes()->sync([1, 2, 3, 4, 5, 6, 7, 8]);
        $a3->techniques()->sync([1, 2, 3, 4, 5, 6, 7, 8]);
        $a3->gallery()->associate(3);
        $a3->rule()->associate(3);
        $a3->save();

        $a4 = $manager->areas()->create([
            'approved' => true,
            'public' => true,
            'lease' => true,
            'name' => 'Pacht Gewässer 1',
            'description' => $faker->paragraph(10),
            'borders' => $faker->paragraph(3),
            'ticket_info' => 'Phosfluorescently engage worldwide methodologies with web-enabled technology. Interactively coordinate proactive e-commerce via process-centric "outside the box" thinking. Completely pursue scalable customer service through sustainable potentialities.',
        ]);
        $a4->clients()->attach($bissanzeiger);
        $a4->clients()->attach($bundesforste);
        $a4->type()->associate(2);
        $a4->fishes()->sync([21, 22, 23, 24, 25, 26]);
        $a4->techniques()->sync([21, 22, 23, 24, 25]);
        $a4->gallery()->associate(4);
        $a4->rule()->associate(4);
        $a4->save();

        $areas = $this->fromJson(database_path('data/areas.json'));

        $progress = $this->getProgressbar();
        $progress->start(count($areas));

        foreach ($areas as $area) {
        /** @var Area $newArea */
            $newArea = $manager->areas()->create([
                'approved' => $area['approved'],
                'public' => $area['public'],
                'lease' => $area['lease'],
                'name' => $area['name'],
            ]);
            $newArea->clients()->attach($bissanzeiger);
            $newArea->locations()->create([
                'longitude' => $area['longitude'],
                'latitude' => $area['latitude'],
            ]);
            $city = City::where('post_code', $area['postCode'])
                ->whereHas('translations', function ($query) use ($area) {
                    $query->where('locale', 'de_DE')
                        ->where('name', $area['city']);
                })->first();
            $newArea->cities()->attach($city);

            $numFish = $faker->unique(true)->numberBetween(3, 7);
            for ($i = 0; $i < $numFish; $i++) {
                $newArea->fishes()->attach($faker->unique()->numberBetween(1, 42));
            }

            $numTech = $faker->unique(true)->numberBetween(3, 5);
            for ($i = 0; $i < $numTech; $i++) {
                $newArea->techniques()->attach($faker->unique()->numberBetween(1, 25));
            }

            $newArea->type()->associate($faker->unique()->numberBetween(1, 8));

            $newArea->created_at = $faker->dateTimeBetween('-2 years', '-1 year');
            $newArea->updated_at = $faker->dateTimeThisYear();

            $newArea->save();

            $progress->advance();
        }

        $progress->finish();
    }
}
