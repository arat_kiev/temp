<?php

use App\Models\Authorization\Role;

class RoleTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = $this->fromJson(database_path('data/roles.json'));

        $progress = $this->getProgressbar();
        $progress->start(count($roles));

        foreach ($roles as $role) {
            $newRole = Role::create(array_except($role, 'permissions'));

            foreach ($role['permissions'] as $permission) {
                $newRole->givePermissionTo($permission);
            }

            $progress->advance();
        }

        $progress->finish();
    }
}
