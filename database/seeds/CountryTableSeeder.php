<?php

use App\Models\Location\Country;

class CountryTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = $this->fromJson(database_path('data/countries.json'));

        $progress = $this->getProgressbar();
        $progress->start(count($countries));

        foreach ($countries as $country) {
            Country::create($country);

            $progress->advance();
        }

        $progress->finish();
    }
}
