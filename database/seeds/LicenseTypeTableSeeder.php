<?php

use App\Models\License\LicenseType;

class LicenseTypeTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $licenseTypes = $this->fromJson(database_path('data/license_types.json'));

        $progress = $this->getProgressbar();
        $progress->start(count($licenseTypes));

        foreach ($licenseTypes as $type) {
            LicenseType::create($type);

            $progress->advance();
        }

        $progress->finish();
    }
}
