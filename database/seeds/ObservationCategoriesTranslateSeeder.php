<?php

use Illuminate\Database\Seeder;
use App\Models\Inspection\ObservationCategoryTranslation;

class ObservationCategoriesTranslateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ObservationCategoryTranslation::updateOrCreate([
            'locale' => 'de_DE',
            'name' => 'Allgemein',
            'observation_category_id' => 1
        ]);
        ObservationCategoryTranslation::updateOrCreate([
            'locale' => 'de_DE',
            'name' => 'Prädatoren',
            'observation_category_id' => 2
        ]);
        ObservationCategoryTranslation::updateOrCreate([
            'locale' => 'de_DE',
            'name' => 'Verschmutzung',
            'observation_category_id' => 3
        ]);
    }
}
