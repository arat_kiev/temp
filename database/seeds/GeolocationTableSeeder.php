<?php

use App\Models\Location\Geolocation;

class GeolocationTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $geo_cities = $this->fromJson(database_path('data/geo_cities.json'));

        $progress = $this->getProgressbar();
        $progress->start(count($geo_cities));

        foreach ($geo_cities as $geo) {
            Geolocation::create($geo);

            $progress->advance();
        }

        $progress->finish();
    }
}
