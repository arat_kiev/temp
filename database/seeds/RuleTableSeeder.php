<?php

use App\Models\Picture;
use App\Models\Area\Rule;
use Symfony\Component\HttpFoundation\File\File;

class RuleTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rules = $this->fromJson(database_path('data/rules.json'));

        $progress = $this->getProgressbar();
        $progress->start(count($rules));

        foreach ($rules as $rule) {
            $newRule = Rule::create(array_except($rule, 'files'));

            foreach ($rule['files'] as $file) {
                $p = Picture::create([
                    'name' => $file->name,
                    'description' => $file->description,
                    'file' => new File(database_path($file->file), \File::name(database_path($file->file))),
                ]);
                $newRule->files()->attach($p, ['public' => $file->public]);
            }

            $progress->advance();
        }

        $progress->finish();
    }
}
