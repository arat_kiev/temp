<?php

use App\Models\Ticket\TicketPrice;
use App\Models\Ticket\TicketType;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TicketTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tt1 = new TicketType();
        $tt1->{'name:de_DE'} = 'Tageskarte';
        $tt1->area()->associate(2);
        $tt1->category()->associate(2);
        $tt1->day_starts_at_sunrise = true;
        $tt1->day_ends_at_sunset = true;
        $tt1->fish_per_day = 5;
        $tt1->fishing_days = 1;
        $tt1->save();
        $pc1 = new TicketPrice([
            'name' => 'Standardpreis',
            'type' => 'DEFAULT',
            'value' => 1200,
            'valid_from' => Carbon::createFromDate(2016, 3, 1),
            'valid_to' => Carbon::createFromDate(2016, 10, 31),
            'visible_from' => Carbon::createFromDate(2015, 11, 1),
        ]);
        $pc2 = new TicketPrice([
            'name' => 'Jugendpreis',
            'type' => 'JUNIOR',
            'value' => 1000,
            'valid_from' => Carbon::createFromDate(2016, 3, 1),
            'valid_to' => Carbon::createFromDate(2016, 10, 31),
            'visible_from' => Carbon::createFromDate(2015, 11, 1),
        ]);
        $tt1->prices()->saveMany([$pc1, $pc2]);
        $pc2->weekdays()->createMany([
            ['weekday' => 'WED'],
            ['weekday' => 'THU'],
            ['weekday' => 'FRI'],
            ['weekday' => 'SAT'],
            ['weekday' => 'SUN'],
        ]);
        $tt1->requiredLicenseTypes()->sync([2]);
        $tt1->optionalLicenseTypes()->sync([1, 3]);

        $tt2 = new TicketType();
        $tt2->{'name:de_DE'} = '2-Tageskarte';
        $tt2->area()->associate(2);
        $tt2->category()->associate(2);
        $tt2->duration = 2;
        $tt2->day_starts_at_sunrise = true;
        $tt2->day_ends_at_sunset = true;
        $tt2->fish_per_day = 4;
        $tt2->fishing_days = 2;
        $tt2->save();
        $pc3 = new TicketPrice([
            'name' => 'Standardpreis',
            'type' => 'DEFAULT',
            'value' => 2200,
            'valid_from' => Carbon::createFromDate(2016, 3, 1),
            'valid_to' => Carbon::createFromDate(2016, 10, 31),
            'visible_from' => Carbon::createFromDate(2015, 11, 1),
        ]);
        $tt2->prices()->save($pc3);
        $pc3->lockPeriods()->create([
            'lock_from' => Carbon::createFromDate(2016, 6, 1),
            'lock_till' => Carbon::createFromDate(2016, 6, 30),
        ]);
        $tt2->requiredLicenseTypes()->sync([2]);
        $tt2->optionalLicenseTypes()->sync([1, 3]);
        $tt2->rule()->associate(3);

        $tt3 = new TicketType();
        $tt3->{'name:de_DE'} = 'Wochenkarte';
        $tt3->area()->associate(2);
        $tt3->category()->associate(3);
        $tt3->day_starts_at_sunrise = true;
        $tt3->day_ends_at_sunset = true;
        $tt3->fish_per_day = 3;
        $tt3->fishing_days = 4;
        $tt3->save();
        $tt3->prices()->create([
            'name' => 'Standardpreis',
            'type' => 'DEFAULT',
            'value' => 4500,
            'valid_from' => Carbon::createFromDate(2016, 3, 1),
            'valid_to' => Carbon::createFromDate(2016, 10, 31),
            'visible_from' => Carbon::createFromDate(2015, 11, 1),
        ]);
        $tt3->requiredLicenseTypes()->sync([2]);
        $tt3->optionalLicenseTypes()->sync([1, 3]);
    }
}
