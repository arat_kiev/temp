<?php

use App\Models\Client;
use App\Models\Picture;
use Illuminate\Database\Seeder;
use Symfony\Component\HttpFoundation\File\File;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // System client
        Client::create([
            'name' => 'system',
            'filter_areas' => false,
            'locale' => 'de_DE',
            'templates' => [],
            'api_key' => 'o0LqONHM7sFBKQl7a6cAPRv14Job1EG0p85KRiyl',
        ]);

        // Bissanzeiger
        $watermarkFile = 'data/images/biss_logo.png';
        $watermark = Picture::create([
            'name' => 'Bissanzeiger Wasserzeichen',
            'file' => new File(database_path($watermarkFile), \File::name(database_path($watermarkFile))),
        ]);

        $bissanzeiger = Client::create([
            'name' => 'bissanzeiger',
            'locale' => 'de_DE',
            'templates' => [
                'email' => [
                    'welcome' => 'bissanzeiger.email.welcome',
                    'ticket' => 'bissanzeiger.email.ticket',
                    'license_pending' => 'system.email.license_pending',
                    'license_accepted' => 'bissanzeiger.email.license.accepted',
                    'license_rejected' => 'bissanzeiger.email.license.rejected',
                    'password_reset' => 'bissanzeiger.email.password.reset',
                ],
                'pdf' => [
                    'ticket' => 'bissanzeiger.pdf.ticket',
                ],
            ],
            'api_key' => '4ORfBS1w2kICYNNCvpUsb92jAMN5EKrMapEpmI9y',
            'base_url'  => 'https://www.hejfish.com/',
        ]);

        $bissanzeiger->watermark()->associate($watermark);
        $bissanzeiger->save();

        // Bundesforste
        $watermarkFile = 'data/images/bforste_logo.png';
        $watermark = Picture::create([
            'name' => 'Bundesforste Wasserzeichen',
            'file' => new File(database_path($watermarkFile), \File::name(database_path($watermarkFile))),
        ]);

        $bundesforste = Client::create([
            'name' => 'bundesforste',
            'locale' => 'de_DE',
            'templates' => [
                'email' => [
                    'welcome' => 'bundesforste.email.welcome',
                    'ticket' => 'bundesforste.email.ticket',
                    'license_pending' => 'system.email.license_pending',
                    'license_accepted' => 'bissanzeiger.email.license.accepted',
                    'license_rejected' => 'bissanzeiger.email.license.rejected',
                    'password_reset' => 'bissanzeiger.email.password.reset',
                ],
                'pdf' => [
                    'ticket' => 'bundesforste.pdf.ticket',
                ],
            ],
            'api_key' => 'KbmFjIW1JVorhnlRLORaN9b7qOpUinc2yWJOCnWj',
            'base_url'  => 'https://www.bundesforste-fischerei.at/',
        ]);

        $bundesforste->watermark()->associate($watermark);
        $bundesforste->save();

        $bissadmin = Client::create([
            'name' => 'bissadmin',
            'filter_areas' => false,
            'locale' => 'de_DE',
            'templates' => [],
            'api_key' => 'dFbyMs3VvuAUBnFCgSTtxWnjrt5Uxwfb7ouNl4rJ',
        ]);

        // PoS client
        Client::create([
            'name' => 'pos',
            'filter_areas' => false,
            'locale' => 'de_DE',
            'templates' => [
                'pdf' => [
                    'ticket' => 'bissanzeiger.pdf.ticket',
                ],
            ],
            'api_key' => '41BAocoZXBo0tbvSKLTQ0HXZTlxKG13pEeOa9Auo',
            'base_url'  => '',
        ]);

        Config::set('app.client', $bissadmin);
    }
}
