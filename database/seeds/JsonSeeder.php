<?php

use Illuminate\Database\Seeder;
use Symfony\Component\Console\Helper\ProgressBar;

abstract class JsonSeeder extends Seeder
{
    /**
     * @return \Symfony\Component\Console\Helper\HelperInterface
     */
    public function getProgressbar()
    {
        $progressbar = new ProgressBar($this->command->getOutput());

        $progressbar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s% ');

        return $progressbar;
    }

    /**
     * @param $file
     * @return array
     */
    protected function fromJson($file)
    {
        $json = File::get($file);
        $entites = json_decode($json);

        $entites = collect($entites)->transform(function ($item) {
            return !is_object($item) ?: (array) $item;
        })->toArray();

        return $entites;
    }
}
