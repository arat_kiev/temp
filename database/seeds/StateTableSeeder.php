<?php

use App\Models\Location\State;

class StateTableSeeder extends JsonSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = $this->fromJson(database_path('data/states.json'));

        $progress = $this->getProgressbar();
        $progress->start(count($states));

        foreach ($states as $state) {
            State::create($state);

            $progress->advance();
        }

        $progress->finish();
    }
}
