@setup
    $path = isset($env) && $env === 'live' ? 'webroot/biss_2016' : 'webroot/biss_2016_staging';
    $branch = isset($branch) && $branch === 'develop' ? 'develop' : 'master';
@endsetup


@servers(['web' => 'FiOApps_deploy@144.76.148.244'])


@task('deploy', ['on' => 'web', 'confirm' => true])
    echo ">>> Starting deployment: {{ $env }} / {{ $branch }}"
    cd {{ $path }}
    echo "> Maintainance Mode Up"
    php artisan down
    echo "> Git Pull Changes"
    git pull -f origin {{ $branch }}
    echo "> Composer Install"
    php composer.phar install --no-interaction --no-dev --prefer-dist
    echo "> Artisan Migrations"
    php artisan migrate --force
    echo "> Artisan Optimizations"
    php artisan optimize --force
    echo "> Artisan Cache Clear"
    echo "php artisan cache:clear"
    echo "> Artisan Queue Restart"
    php artisan queue:restart
    echo "> Maintainance Mode Down"
    php artisan up
    echo ">>> Finished deployment"
@endtask