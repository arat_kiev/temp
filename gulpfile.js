'use strict';

var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;

elixir(function(mix) {
    mix.less('ticket.less', 'public/css/ticket.css')
        .less('admin.less', 'public/css/admin.css')
        .less('invoice.less', 'public/css/invoice.css')
        .less('web.less', 'public/css/web.css')
        .less('pos.less', 'public/css/pos.css')
        .less('product.less', 'public/css/product.css')
        .styles('jquery-ui.min.css', 'public/css/jquery-ui.min.css')
        .styles('hco.css', 'public/css/hco.css')
        .styles('admin_login.css', 'public/css/admin_login.css')
        .scripts('pos.js', 'public/js/pos.js')
        .scripts('jquery-ui.min.js', 'public/js/jquery-ui.min.js')
        .scripts('datepicker-de.js', 'public/js/datepicker-de.js')
        .scripts('loadingoverlay.min.js', 'public/js/loadingoverlay.min.js')
        .copy('bower_components/*', 'public/lib')
        .copy('resources/assets/img', 'public/img');
});
