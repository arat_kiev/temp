<?php

return [
    'errors' => [
        'attach' => [
            'not_found' => 'Ticket not found'
        ]
    ]
];