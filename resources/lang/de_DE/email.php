<?php

return [

    'welcome' => [
        'subject' => 'Willkommen! Bestätigen Sie Ihre Email-Adresse',
    ],
    'ticket' => [
        'subject' => 'Ihre Angelkarte ist angekommen',
        'attach' => 'Angelkarte',
        'storno' => [
            'subject' => 'Angelkarte :ticketId wurde stornieren',
        ],
    ],
    'product' => [
        'subject' => 'Ihre Rechnung (:code)',
        'attach' => 'Rechnung',
        'ends'  => [
            'subject'   => 'Das Produkt endet auf das Lager ":stockName" (:stockId)',
        ],
    ],
    'coupon'    => [
        'subject'   => 'Dein Gutschein ist angekommen!',
    ],
    'coupons'   => [
        'used'      => [
            'receiver'  => [
                'subject'  => 'Du hast einen Gutschein eingelöst!',
            ],
            'owner'     => [
                'subject'  => 'Dein Gutschein wurde eingelöst!',
            ],
        ],
    ],
    'license' => [
        'pending' => [
            'subject' => 'Neue Berechtigung wurde hochgeladen',
        ],
        'accepted' => [
            'subject' => 'Ihre Berechtigung wurde akzeptiert',
        ],
        'rejected' => [
            'subject' => 'Ihre Berechtigung wurde abgelehnt',
        ],
    ],
    'password' => [
        'subject' => 'Rücksetzung Ihres Passwortes'
    ],
    'change' => [
        'subject' => 'hejfish | Bitte bestätige deine neue E-Mail-Adresse'
    ]

];
