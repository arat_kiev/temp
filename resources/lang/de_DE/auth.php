<?php

return [

    'failed' => 'Anmeldung fehlgeschlagen. Benutzer nicht gefunden oder Passwort falsch.',
    'throttle' => 'Zuviele Anmeldeversuche. Bitte versuchen Sie es noch einmal in :seconds Sekunden.',
    'exists' => 'Benutzer existiert bereits.',
    'token_error' => 'Authentifizierung fehlgeschlagen',
    'email_change_error' => 'Bitte geben Sie die E-Mail zuerst ein',
    'email_change_success' => 'E-Mail wurde erfolgreich geändert',
];
