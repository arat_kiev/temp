@extends('ticket.layout.base')

@inject ('tm', 'App\Managers\TicketManager')

@php
    $manager = $ticket->type->area->manager;
@endphp

@section('extra_after_ticket')
    <div class="ticket-page">
        <img class="extra-logo" src="{{ route('imagecache', [
        'template' => 'lLogo',
        $manager->logo ? $manager->logo->file : 'ticket/hejfish_logo.png'
        ]) }}"/>
        <div class="extra-manager-name">{{ $manager->name }}</div>

        <h1 class="extra-title">Fahrgenehmigung – {{ $ticket->type->area->name }}</h1>
        <h2 class="extra-content">Gültig für den Zeitraum: <br /> {{ $tm->getValidString($ticket) }}</h2>

        <h3 class="extra-content">Kennzeichen: ___________  </h3>
        <p class="extra-content">
            Diese Fahrgenehmigung erlaubt dem Inhaber das Befahren des Weges von der B244 (Zillierbachstraße) bis
            zum
            Parkplatz „Steinbruch“.</p>
        <p class="extra-content">
            Die Fahrgenehmigung ist gut sichtbar im Fahrzeug abzulegen.
            Die Fahrgenehmigung gilt nur zusammen mit einer gültigen Fischereierlaubnis.</p>
        <hr/>
    </div>
@endsection

