<!DOCTYPE html>
<html>
    <head>
        <!--<meta http-equiv="refresh" content="5; URL={{ url()->current() }}">-->

        <title>hejfish API</title>

            <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato', sans-serif;
            }

            .logs-button {
                text-align: right;
                display: table-row;
            }

            .logs-button a {
                position: relative;
                top: 25px;
                margin-top: 15px;
                margin-right: 15px;
                border-radius: 10px;
                padding: 10px;
                color: #f1f1f1;
                background-color: #d34836;
                text-decoration: none;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: top;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 48px;
                margin-bottom: 12px;
            }

            .queue {
                font-size: 24px;
            }
        </style>
    </head>
    <body>
        <div class="logs-button">
            <a href="{{ route('log-viewer::dashboard') }}">Show Logs</a>
        </div>
        <div class="container">
            <div class="content">
                <div class="title">{{ $title }} - {{ App::environment() }}</div>
                <div class="queue">Current date/time: {{ Carbon\Carbon::now()->formatLocalized('%d. %B %Y') }}</div>
                <div class="queue">Delayed Jobs waiting: {{ $delayedJobs }}</div>
                <div class="queue">Queued Jobs waiting: {{ $normalJobs }}</div>
                <div class="queue">Queued Jobs failed: {{ $failedJobs }}</div>
            </div>
        </div>
    </body>
</html>
