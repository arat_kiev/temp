@inject ('productManager', 'App\Managers\ProductSaleManager')
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="{{ asset('css/product.css') }}" />
    <script nonce="{{ csp_nounce() }}">window.status = 'loading';</script>
</head>
<body>
@if (isset($demo) && $demo === true)
    <div id="demo-overlay"></div>
@endif
<div class="user">
    <div class="sender">Fishing & Outdoor Apps GmbH, Hopfengasse 3, A-4020 Linz</div>
    <div class="name">{{ $sale->first_name }} {{ $sale->last_name }}</div>
    <ul class="address">
        <li>{{ $sale->street }}</li>
        <li>{{ $sale->post_code }} {{ $sale->city }}</li>
        <li>{{ $sale->country->name }}</li>
    </ul>
</div>

<div class="manager">
    <img class="logo" src="{{ route('imagecache', [
        'template' => 'lLogo', 'ticket/hejfish_logo.png'
    ]) }}" />
    <div class="name">Fishing & Outdoor Apps GmbH</div>
    <ul class="address">
        <li>Hopfengasse 3</li>
        <li>4020 Linz</li>
        <li>Österreich</li>
    </ul>
    <div class="link">www.hejfish.com</div>
</div>

<div class="purchase">
    <div class="identifier">Rechnungs-Nr.: {{ $productManager->getIdentifier($sale) }}</div>
    <div class="date">Gekauft am: {{ $sale->created_at->format('d.m.Y - H:i') }}</div>
</div>
<div class="product">
    <h3>Vielen Dank für Deinen Einkauf!</h3>
    <span>Hiermit stellen wir Dir wie folgt in Rechnung:</span>
    <div class="title">
        <p>{{ $sale->quantity }}x {{ $sale->product->name }} (Preis: {{ $sale->price->value }} €)</p>
        @if ($sale->product->hidden_info)
            <div class="hidden-info">{{$sale->product->hidden_info}}
                <p><strong>Achtung:</strong>Kann sich ändern! Bitte prüfe den Code tagesaktuell in deinen Käufen auf der Webseite.</p>
            </div>
        @endif
    </div>
    @if ($sale->timeslotDates)
        <h3>Reserviert am:</h3>
        @foreach ($sale->timeslotDates as $timeslot)
            <p>
                {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeslot->date)->format('d.m.Y') }}&nbsp;
                von {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeslot->timeslot->from)->format('H:i')}}&nbsp;
                bis {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeslot->timeslot->till)->format('H:i') }}
            </p>
        @endforeach
    @endif

    <div class="price bold">Gesamtbetrag:  {{ $productManager->getTotalAmount($sale) }} € (inkl. Versand- und Bezahlkosten)</div>
    @foreach($sale->fees as $fee)
        <div>{{ $fee->name }}: {{ $fee->value_with_mark }}</div>
    @endforeach
    <div class="vat">
        MwSt ({{ $productManager->getVatPercent($sale) }}%):
        {{ $productManager->getVatAmount($sale) }} €
    </div>
    <p>Bezahlt mit: Online-Bezahldienst</p>
    <p>Du erhältst eine E-Mail sobald das Produkt verschickt wurde.</p>
    <p>Mit freundlichen Grüßen,</p>
    <p>&nbsp;</p>
    <p>Dein hejfish-Team</p>
    <p></p>
</div>

<script nonce="{{ csp_nounce() }}">
    var img = document.querySelector('.manager > .logo');

    if (img.complete) {
        window.status = 'loaded';
    } else {
        img.addEventListener('load', function() {
            window.status = 'loaded';
        });
    }

    setTimeout(function() {
        window.status = 'loaded';
    }, 5000);
</script>
</body>
</html>