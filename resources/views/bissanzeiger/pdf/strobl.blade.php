@inject ('ticket_manager', 'App\Managers\TicketManager')
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="{{ asset('css/ticket.css') }}" />
    <script nonce="{{ csp_nounce() }}">window.status = 'loading';</script>
</head>
<body>
@if (isset($demo) && $demo === true)
    <div id="demo-overlay"></div>
@endif
<div id="watermark">
    {{ str_repeat("- {$ticket_manager->getUserName($ticket)} - {$ticket->valid_from} - {$ticket_manager->getIdentifier($ticket)}", 5000) }}
</div>
<div class="ticket-page top">
    <div class="content left">
        <div class="manager">
            @if($ticket->type->area->manager->logo)
                <img class="logo" src="{{ route('imagecache', ['template' => 'lLogo', $ticket->type->area->manager->logo->file]) }}" />
            @else
                <img class="logo" src="{{ route('imagecache', ['template' => 'lLogo', 'ticket/hejfish_logo.png']) }}" />
            @endif
            <div class="name">{{ $ticket->type->area->manager->name }}</div>
            <ul class="address">
                <li>{{ $ticket->type->area->manager->street }}</li>
                <li>{{ $ticket->type->area->manager->area_code }} {{ $ticket->type->area->manager->city }}</li>
                <li>{{ $ticket->type->area->manager->country->name }}</li>
                @if ($ticket->type->area->manager->uid)
                    <li>UID: {{ $ticket->type->area->manager->uid }}</li>
                @endif
            </ul>
        </div>
        <div class="user">
            Ausgestellt auf:
            @if($ticket->resellerTicket != null)
                <div class="name">{{ $ticket->resellerTicket->full_name }}</div>
                <div class="birthday">geboren am: {{ $ticket->resellerTicket->birthday->format('d.m.Y') }}</div>
                <ul class="address">
                    <li>{{ $ticket->resellerTicket->street }}</li>
                    <li>{{ $ticket->resellerTicket->post_code }} {{ $ticket->resellerTicket->city }}</li>
                    <li>{{ $ticket->resellerTicket->country->name }}</li>
                </ul>

            @else
                <div class="name">{{ $ticket->user->full_name }}</div>
                <div class="birthday">geboren am: {{ $ticket->user->birthday->format('d.m.Y') }}</div>
                <ul class="address">
                    <li>{{ $ticket->user->street }}</li>
                    <li>{{ $ticket->user->post_code }} {{ $ticket->user->city }}</li>
                    <li>{{ $ticket->user->country->name }}</li>
                </ul>
            @endif
            <hr class="divider" />
            @if($ticket->resellerTicket != null && $ticket->resellerTicket->authorization_id != null)
                <div class="authorization-id">
                    Berechtigungs-Nummer:<br/>
                    {{ $ticket->resellerTicket->authorization_id }}
                    <br/><br/>
                    @if($ticket->resellerTicket->issuing_authority)
                        Ausstellende Behörde:<br/>
                        {{ $ticket->resellerTicket->issuing_authority }}
                    @endif
                </div>
                <hr class="divider" />
            @endif
        </div>
        <div class="area-info">
            {!! $ticket->type->area->ticket_info !!}
        </div>
    </div>
    <div class="content center">
        <div class="ticket">
            <div class="link">www.hejfish.com</div>
            <div class="title">
                {{ $ticket->storno_id ? 'STORNO' : '' }} {{ $ticket->type->name }} für
                <br/>
                {{ $ticket->type->area->name }}
            </div>
            @if(!$ticket->storno_id)
                <div class="valid">
                    Gültigkeit:
                    <span class="date">{{ $ticket_manager->getValidString($ticket) }}</span>
                </div>
            @endif
            <div class="borders">{!! $ticket->type->area->borders !!}</div>
        </div>
        <div class="catch">
            @if ($ticket_manager->shouldDisplayCatchlog($ticket))
                <table class="log">
                    <thead>
                    <tr>
                        <th class="time">Uhrzeit</th>
                        <th class="type">Salmoniden</th>
                        <th class="size">Länge</th>
                        <th class="check">Kontrolle</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach (range(1, $ticket->type->fish_per_day) as $i)
                        <tr>
                            <td></td>
                            <td>{{ $i }}</td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
            <div class="intro">
                Der Fang muss unmittelbar nach der Entnahme auf der ausgedruckten Lizenz eingetragen werden!
                Spätestens nach dem Fischtag muss der Fang in die Fangstatistik auf <i><b>www.hejfish.com</b></i>
                unter <i><b>"Profil &gt; Meine Angelkarten &gt; jeweilige Angelkarte auswählen &gt; Fang eintragen"</b></i>
                eingetragen werden, somit muss die Lizenz nicht händisch an den Gewässerbewirtschafter retourniert werden!
            </div>
            <div class="legal">
                Des Weiteren gelten die fischereigesetzlichen Bestimmungen des jeweiligen Bundeslandes.
            </div>
        </div>
    </div>
    <div class="content right">
        <div class="brand">
            <div class="container">
                <img class="logo" src="{{ route('imagecache', ['template' => 'lLogo', 'ticket/hejfish_logo.png']) }}" />
                <div class="code">{{ $ticket_manager->getControlCode($ticket) }}</div>
            </div>
        </div>
        <div class="purchase">
            <div class="identifier">{{ $ticket_manager->getIdentifier($ticket) }}</div>
            <hr class="divider" />
            <div class="date">Datum: {{ $ticket->created_at->format('d.m.Y - H:i') }}</div>
            <div class="price">Preis: {{ $ticket_manager->getTicketPrice($ticket) }} €</div>
            @if ($ticket->type->area->manager->uid && $ticket->price->show_vat)
                <div class="vat">
                    MwSt ({{ $ticket_manager->getVatPercent($ticket) }}%):
                    {{ $ticket_manager->getVatAmount($ticket) }} €
                </div>
            @endif
            <div class="type">Typ: {{ $ticket->price->name }}</div>
            <div class="status">{{ $ticket->storno_id ? 'STORNO' : '' }}</div>
        </div>
        <img class="qrcode" src="data:image/png;base64, {!! base64_encode($ticket_manager->getQrCode($ticket)) !!} ">
        <div class="manager-info">
            {!! $ticket->type->area->manager->ticket_info !!}
        </div>
        <div class="fisher-id">
            <hr class="divider" />
            Angler-ID:
            @if ($ticket->resellerTicket)
                <div class="bold">{{ $ticket->resellerTicket->fisher_id }}</div>
            @else
                <div class="bold">{{ $ticket->user->fisher_id }}</div>
            @endif
        </div>
    </div>
</div>
<div class="ticket-page bottom">
    <div class="rules-text">
        @if ($ticket->type->ruleWithFallback)
            {!! $ticket->type->ruleWithFallback->text !!}
        @endif
    </div>
    <div class="agent-info">
        Vermittler: Fishing & Outdoor Apps GmbH - Hopfengasse 3 / 5. Stock - A-4020 Linz - UID: ATU67771379 - info@hejfish.com
    </div>
</div>
@unless ($ticket_manager->shouldDisplayCatchlog($ticket) || (isset($demo) && $demo === true))
    <div class="catchlog-page">
        <table>
            <thead>
            <tr>
                <th colspan="{{ $ticket_manager->getMaxFishPerDay($ticket) + 1 }}">
                    <table class="page-header">
                        <tr>
                            <th width="100%" colspan="{{ $ticket_manager->getMaxFishPerDay($ticket)}}">
                                <span class="title">Fangstatistik ({{ $ticket->type->name }})</span>
                            </th>

                            <th class="qrcode-container" rowspan="3">
                                <img class="qrcode" src="data:image/png;base64, {!! base64_encode($ticket_manager->getQrCode($ticket)) !!} ">
                            </th>
                        </tr>
                        <tr>
                            <th colspan="{{ $ticket_manager->getMaxFishPerDay($ticket)}}">
                                <span class="title">{{ $ticket->type->area->name }}</span>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="{{ ceil($ticket_manager->getMaxFishPerDay($ticket) / 2) }}">
                                {{ $ticket_manager->getUserName($ticket) }}<br/>
                                {{ $ticket_manager->getIdentifier($ticket) }}
                            </th>
                            <th class="legend" colspan="{{ floor($ticket_manager->getMaxFishPerDay($ticket) / 2) }}">
                                <strong>JEder Fischtag ist vor Beginn des Fischens einzutragen!<br />
                                    Jede Fischentnahme ist sofort einzutragen!</strong>
                            </th>
                        </tr>
                    </table>
                </th>
            </tr>
            <tr>
                <th>Datum</th>
                @foreach (range(1, $ticket_manager->getMaxFishPerDay($ticket)) as $i)
                    <th>Salmoniden / Länge</th>
                @endforeach
                <th>Kontrolle</th>
            </tr>
            </thead>
            <tbody>
            @foreach (range(1, $ticket_manager->getMaxFishingDays($ticket)) as $day)
                <tr>
                    <td></td>
                    @foreach (range(1, $ticket_manager->getMaxFishPerDay($ticket)) as $i)
                        <td>{{ $i }}</td>
                    @endforeach
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endunless
<script nonce="{{ csp_nounce() }}">
    var img = document.querySelector('.manager > .logo');

    if (img.complete) {
        window.status = 'loaded';
    } else {
        img.addEventListener('load', function() {
            window.status = 'loaded';
        });
    }

    setTimeout(function() {
        window.status = 'loaded';
    }, 5000);
</script>
</body>
</html>