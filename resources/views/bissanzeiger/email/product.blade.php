@extends('bissanzeiger.email.base')

@section('subject')
    {{ trans('email.product.subject', ['code' => $code]) }}
@stop

@section('content')
    <p>
        Im Anhang finden Sie die Rechnung für das bestellte Produkt.
    </p>
    <p>
        Sollten Sie noch Fragen haben, können Sie sich jederzeit an uns wenden!
    </p>
    <br/>
    <p>
        Petri Heil wünscht Ihr hejfish-Team!
    </p>
@stop
