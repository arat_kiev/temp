@extends('bissanzeiger.email.base')

@section('subject')
    {{ trans('email.ticket.storno.subject', ['ticketId' => $ticket->id]) }}
@stop

@section('content')
    <p>
        <b>Angelkarte-ID:</b>
        {{ $ticket->id }}
    </p>
    <p>
        <b>Verkäufer-ID:</b>
        {{ $ticket->resellerTicket ? $ticket->resellerTicket->reseller_id : '' }}
    </p>
    <p>
        <b>Verkäufer:</b>
        {{ $ticket->resellerTicket ? $ticket->resellerTicket->reseller->name : 'online' }}
    </p>
    <p>
        <b>Käufer:</b>
        {{ $ticket->resellerTicket
            ? $ticket->resellerTicket->first_name . ' ' . $ticket->resellerTicket->last_name
            : $ticket->user->first_name . ' ' . $ticket->user->last_name
        }}
    </p>
    <p>
        <b>Storno-Grund:</b>
        {{ $ticket->storno_reason }}
    </p>
@stop