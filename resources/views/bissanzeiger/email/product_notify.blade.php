@extends('bissanzeiger.email.base')

@section('content')
    <h2>{{ $sale->product->name }} wurde bestellt!</h2>
    <p>
        Kunde: <br/>
        {{ $sale->user ? $sale->user->full_name : "$sale->first_name $sale->last_name" }} <br/>
        {{ $sale->user ? $sale->user->street : $sale->street }}<br/>
        {{ $sale->user ? $sale->user->post_code : $sale->post_code }}
        {{ $sale->user ? $sale->user->city : $sale->city }} <br/>
        {{ $sale->user ? $sale->user->country->name : $sale->country->name }}
    </p>
    @if ($sale->timeslotDates)
        <h3>Reserviert am:</h3>
        @foreach ($sale->timeslotDates as $timeslot)
            <p>
                {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeslot->date)->format('d.m.Y') }}<br/>
                von {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeslot->timeslot->from)->format('H:i')}}<br/>
                bis {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeslot->timeslot->till)->format('H:i') }}
            </p>
        @endforeach
    @endif
    <p>
    <h2>Bestelltes Produkt</h2>
    Verkauf-ID: {{ $sale->id }} <br/>
    Produkt: {{ $sale->product->name }} <br/>
    Produkt-Id: {{ $sale->product->id }} <br/>
    Menge: {{ $sale->quantity }}
    </p>

@stop