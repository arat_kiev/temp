@extends('bissanzeiger.email.base')

@section('content')
    <h2>Eine neue Angelkarte wurde gekauft!</h2>
    <p>Kunde: <br/>
        {{ $user->first_name }} {{ $user->last_name }} <br/>
        {{ $user->street }}<br/>
        {{ $user->post_code }} {{ $user->city }} <br/>
        {{ $user->country->name }}<br />
        <a href="mailto:{{ $user->email }}">{{ $user->email }}</a>
    </p>
    <p>
    <h2>Bestellte Angelkarte</h2>
    Typ: {{ $ticket->type->name }} <br/>
    Id: {{ $ticket->id }}<br />
    <a href="{{ route('admin::sales.index') }}">Zu den Verkäufen</a>
@stop