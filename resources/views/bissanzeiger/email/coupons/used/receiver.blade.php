@extends('bissanzeiger.email.base')

@section('subject')
    {{ trans('email.coupons.used.receiver.subject') }}
@stop

@section('content')
    <p>
        Der Gutschein mit dem Code "<b>{{ $code }}</b>" wurde eingel�st und Du hast {{ $bonus }} Euro Bonus auf dein Konto gebucht bekommen.
    </p>
    <br/>
    <p>
        Solltest Du noch Fragen haben, kannst Du Dich jederzeit an uns wenden!
    </p>
    <br/>
    <p>
        Petri Heil w�nscht Dein hejfish-Team!
    </p>
@stop
