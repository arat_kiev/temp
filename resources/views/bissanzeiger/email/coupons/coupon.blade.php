@extends('bissanzeiger.email.base')

@section('subject')
    {{ trans('email.coupon.subject') }}
@stop

@section('content')
    <p>
        Du hast den Gutschein-Code "<b>{{ $code }}</b>" erhalten!
        Gehe jetzt auf <a href="www.hejfish.com">hejfish</a> und löse Ihn in Deinem Profil ein und erhalte: {{ $bonus }} Euro .
    </p>
    <br/>
    <p>
        Solltest Du noch Fragen haben, kannst Du Dich jederzeit an uns wenden!
    </p>
    <br/>
    <p>
        Petri Heil wünscht Dein hejfish-Team!
    </p>
@stop
