@extends('layout.responsive')

@section('page_title')
    E-Mail-Adresse bestätigt
@endsection

@section('page_content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                <div class="alert alert-success alert-password text-center">
                    <strong>
                        Hej!<br />
                        Vielen Dank! Deine E-Mail-Adresse wurde bestätigt. <br />
                        Du kannst jetzt Dein Profil ausfüllen und Deine Berechtigungen hochladen!
                    </strong>
                    <br /><br />
                    @if ($baseUrl)
                    <a href="{{ $baseUrl }}/profile/main" class="btn btn-lg btn-danger">

                        Jetzt Profil ausfüllen

                    </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
