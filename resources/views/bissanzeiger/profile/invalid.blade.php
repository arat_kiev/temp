@extends('layout.responsive')

@section('page_title')
    Wiederherstellungscode ungültig
@endsection

@section('page_content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                <div class="alert alert-danger alert-password text-center">
                    <strong>Der Wiederherstellungscode ist ungültig oder wurde bereits verwendet.</strong>
                </div>
            </div>
        </div>
    </div>
@endsection