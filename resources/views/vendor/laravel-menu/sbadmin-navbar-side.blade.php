<?php $level = $level ?? 2 ?>
@foreach($items as $item)
    <li@lm-attrs($item) {{ $item->hasChildren() ? 'class="dropdown"' : '' }} @lm-endattrs>
        @if($item->link)
            <a  @lm-attrs($item->link)
                    {{ $item->hasChildren() ? 'class="dropdown-toggle" data-toggle="dropdown"' : '' }}
                @lm-endattrs href="{!! $item->url() !!}">
                {!! $item->title !!}
                {!! $item->hasChildren() ? '<b class="caret"></b>' : '' !!}
            </a>
        @else
            {!! $item->title !!}
        @endif

        @if($item->hasChildren())
            <ul class="nav {{ $level >= 3 ? 'nav-third-level' : 'nav-second-level'}}">
                @include('vendor.laravel-menu.sbadmin-navbar-side', ['items' => $item->children(), 'level' => 3])
            </ul>
        @endif
    </li>
    @if($item->divider)
        <li{!! Lavary\Menu\Builder::attributes($item->divider) !!}></li>
    @endif
@endforeach
