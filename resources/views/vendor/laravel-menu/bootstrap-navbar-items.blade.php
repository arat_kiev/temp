@foreach($items as $item)
    <li @lm-attrs($item) {!! $item->hasChildren() ? 'class="dropdown"' : '' !!} @lm-endattrs>
        @if($item->link)
            <a  @lm-attrs($item->link)
                    {!! $item->hasChildren() ? 'class="dropdown-toggle" data-toggle="dropdown"' : '' !!}
                @lm-endattrs href="{!! $item->url() !!}">
                {!! $item->title !!}
                {!! $item->hasChildren() ? '<b class="caret"></b>' : '' !!}
            </a>
        @else
          {!! $item->title !!}
        @endif
        @if($item->hasChildren())
            <ul class="dropdown-menu">
                @include(config('laravel-menu.views.bootstrap-items'), ['items' => $item->children()])
            </ul>
        @endif
    </li>
    @if($item->divider)
        <li{!! Lavary\Menu\Builder::attributes($item->divider) !!}></li>
    @endif
@endforeach
