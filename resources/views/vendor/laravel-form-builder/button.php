<?php if ($options['wrapper'] !== false) : ?>
<div <?= $options['wrapperAttrs'] ?> >
<?php endif; ?>

<?php $options['attr']['class'] = trim(str_replace('form-control', '', $options['attr']['class'])) ?>
<?= Form::button($options['label'], $options['attr']) ?>
<?php include 'help_block.php' ?>

<?php if ($options['wrapper'] !== false) : ?>
</div>
<?php endif; ?>
