@extends('ticket.layout.base')

@inject ('tm', 'App\Managers\TicketManager')

@section('main_ticket_catchlog')
    @if($tm->shouldDisplayCatchlog($ticket) && $ticket->type->fish_per_day > 0)
        <table class="log">
            <thead>
            <tr>
                <th class="type">Abschnitt</th>
                <th class="type">Fischart</th>
                <th class="type">cm/kg</th>
                <th class="size">Kontrolle</th>
            </tr>
            </thead>
            <tbody>
            @foreach (range(1, $ticket->type->fish_per_day) as $i)
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>{{ $i }}</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection

@section('catchlog_table_header')
    <th>Datum</th>
    @foreach (range(1, $tm->getMaxFishPerDay($ticket)) as $i)
        <th>Fischart cm/Abschnitt</th>
    @endforeach
@endsection

@section('catchlog_table_body')
    @foreach (range(1, $tm->getMaxFishingDays($ticket)) as $day)
        <tr>
            <td></td>
            @foreach (range(1, $tm->getMaxFishPerDay($ticket)) as $i)
                <td>{{ $i }}</td>
            @endforeach
        </tr>
    @endforeach
@endsection

