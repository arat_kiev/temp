@extends('ticket.layout.base')

@inject ('tm', 'App\Managers\TicketManager')

@php
    $manager = $ticket->type->area->manager;
@endphp

@section('main_ticket_manager')
    <div class="manager">
        <img class="logo" src="{{ route('imagecache', [
        'template' => 'lLogo',
        $manager->logo ? $manager->logo->file : 'ticket/hejfish_logo.png'
    ]) }}" />
        <div class="name">{{ $manager->name }}</div>
        <ul class="address">
            <li>{{ $manager->street }}</li>
            <li>{{ $manager->area_code }} {{ $manager->city }}</li>
            <li>{{ $manager->country->name }}</li>
            <li>{{ $manager->website }}</li>
            @if($manager->uid)
                <li>UID: {{ $manager->uid }}</li>
            @endif
        </ul>
    </div>
@endsection