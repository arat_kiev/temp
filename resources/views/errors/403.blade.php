@extends('admin.layout.base')

@section('page-title')
    403 - Zugriff verweigert
@endsection

@push('styles')
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
        }

        .container {
            width: 100%;
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .content > a {
            font-size: 28px;
            text-align: center;
            width: 160px;
        }

        .content > a > span {
            display: block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }
    </style>
@endpush

@section('body')
    <div class="container">
        <div class="content">
            <h1>hejfish</h1>
            <h2 class="title">403 - Zugriff verweigert.</h2>
            <h4>{{ $exception->getMessage() }}</h4>
            <a class="btn btn-success btn-xl" href="{{ URL::previous() }}">
                <span class="fa fa-undo fa-3x"></span>
                Zurück
            </a>
            <a class="btn btn-primary btn-xl" href="{{ URL::to('/') }}">
                <span class="fa fa-home fa-3x"></span>
                Startseite
            </a>
            <a class="btn btn-warning btn-xl" href="{{ URL::to('/logout') }}">
                <span class="fa fa-sign-out fa-3x"></span>
                Abmelden
            </a>
        </div>
    </div>
@endsection
