@extends('ticket.layout.base')

@inject ('tm', 'App\Managers\TicketManager')

@php
    $manager = $ticket->type->area->manager;
@endphp

@section('extra_after_ticket')
    <div class="ticket-page extra">
        <img class="extra-logo" src="{{ route('imagecache', [
        'template' => 'lLogo',
        $manager->logo ? $manager->logo->file : 'ticket/hejfish_logo.png'
        ]) }}"/>
        <div class="extra-manager-name">{{ $manager->name }}</div>
        <h4 class="extra-content">
            Der Inhaber ist im Besitz der Kurzfischereierlaubnis:</h4>
        <h2 class="extra-content">{{ $tm->getManagerIdentifier($ticket) }}</h2>
        <h4 class="extra-content">für den Zeitraum:</h4>
        <h2 class="extra-content">{{ $tm->getValidString($ticket) }}</h2>
        <h2 class="extra-content">Kennzeichen: __________ </h2>
        <h3 class="extra-content">Wichtige Hinweise!</h3>
        <p class="extra-content">
            Diese Abstellgenehmigung ist nur für die von der Fischereipachtgemeinschaft ausgeschilderten Plätze gültig.
        </p>
        <p class="extra-content">
            Die Bedingungen des Befahres der Forstwege wurden zwischen der Fischereipachtgemeinschaft und den örtlichen
            Waldbesitzern geregelt.</p>
        <p class="extra-content">
            Der Karteninhaber muss durch sein Verhalten dazu beitragen, dass die Regelung mit den Waldbesitzern ihre
            Gültigkeit behält.</p>
        <h3 class="extra-content">Bitte sichtbar im KFZ ablegen!</h3>
        <hr/>
        <p class="extra-content"><strong>Es ist folgendes zu beachten: </strong></p>
        <ul>
            <li>Es dürfen nur die Forstwege bei max. 30 km/h befahren werden, die auf der Karte im Merkblatt rot
                gekennzeichnet sind.
            </li>
            <li>Dies gilt nur für die Zeit von 1 Stunde vor Sonnenaufgang bis 1 nach Sonnenuntergang, aber nicht ab der
                Waldbrandstufe III.
            </li>
            <li>Die Forstämter und die Pachtgemeinschaft übernehmen keine Haftung im Zusammenhang mit der Benutzung der
                Forstwege und Abstellflächen.
            </li>
            <li>Schäden, die durch den Benutzer bzw. durch Dritte verursacht werden, sind nach dem Verursacherprinzip zu
                regeln.
            </li>
            <li>Ein Abstellen der Fahrzeuge ist nur auf den gekennzeichneten Stellen erlaubt. Die Abstellordnung ist
                einzuhalten.
            </li>
        </ul>
        <h4 class="extra-content">Zuwiderhandlungen werden dem zuständigen Ordnungsamt gemeldet.</h4>

    </div>
@endsection

