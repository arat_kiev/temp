@extends('ticket.layout.base')

@inject ('tm', 'App\Managers\TicketManager')

@section('main_ticket_catchlog')
    @if($tm->shouldDisplayCatchlog($ticket) && $ticket->type->fish_per_day > 0)
        <table class="log">
            <thead>
            <tr>
                <th class="time" style="width: 20%;">Uhrzeit</th>
                <th class="type" style="width: 40%;">Salmoniden</th>
                <th class="size" style="width: 20%;">Länge</th>
                <th class="check" style="width: 20%;">Kontrolle</th>
            </tr>
            </thead>
            <tbody>
            @foreach (range(1, $ticket->type->fish_per_day) as $i)
                <tr>
                    <td></td>
                    <td>{{ $i }}</td>
                    <td></td>
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection

@section('catchlog')
    <div class="catchlog-page">
        <table>
            <thead>
            <tr>
                <th colspan="{{ $tm->getMaxFishPerDay($ticket) + 2 }}">
                    <table class="page-header">
                        <tr>
                            <th width="100%" colspan="{{ $tm->getMaxFishPerDay($ticket)}}">
                                <span class="title">Fangstatistik ({{ $ticket->type->name }})</span>
                            </th>

                            <th class="qrcode-container" rowspan="3">
                                <img class="qrcode"
                                     src="data:image/svg+xml;base64, {!! base64_encode($tm->getQrCode($ticket)) !!} ">
                            </th>
                        </tr>
                        <tr>
                            <th colspan="{{ $tm->getMaxFishPerDay($ticket)}}">
                                <span class="title">{{ $ticket->type->area->name }}</span>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="{{ ceil($tm->getMaxFishPerDay($ticket) / 2) }}">
                                {{ $tm->getUserName($ticket) }}<br/>
                                {{ $tm->getIdentifier($ticket) }}
                            </th>
                            <th class="legend" colspan="{{ floor($tm->getMaxFishPerDay($ticket) / 2) }}">
                                <strong>Jeder Fischtag ist vor Beginn des Fischens einzutragen!<br/>
                                    Jede Fischentnahme ist sofort einzutragen!</strong>
                            </th>
                        </tr>
                    </table>
                </th>
            </tr>
            <tr>
                <th style="min-width: 20mm;">Datum</th>
                @foreach (range(1, $tm->getMaxFishPerDay($ticket)) as $i)
                    <th>Salmoniden / Länge</th>
                @endforeach
                <th style="min-width: 20mm;">Kontrolle</th>
            </tr>
            </thead>
            <tbody>
            @foreach (range(1, $tm->getMaxFishingDays($ticket)) as $day)
                <tr>
                    <td></td>
                    @foreach (range(1, $tm->getMaxFishPerDay($ticket)) as $i)
                        <td>{{ $i }}</td>
                    @endforeach
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection