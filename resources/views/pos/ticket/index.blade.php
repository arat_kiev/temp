@extends('pos.layout.master')

@section('page_content')
    <div id="reseller-page">
        <div class="row">
            <div class="col-lg-12">
                <h1>Angelkarten verkaufen</h1>
            </div>
        </div>
        <form id="reseller-form">
            <div class="row">
                <div class="col-lg-12 hidden" id="target-area-select">
                    <h2>1. Gewässer wählen</h2>
                    <ul class="area-list" id="area-list"></ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 hidden" id="target-ticket-select">
                    <h2>2. Angelkarte wählen</h2>
                    <ul class="ticket-list" id="ticket-list"></ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 hidden" id="target-price-select">
                    <h2>3. Typ wählen</h2>
                    <ul class="price-list" id="price-list"></ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 hidden" id="target-date-select">
                    <h2>4. Datum wählen</h2>
                    <div class="row indent-left">
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div id="date-select" class="datepicker-skin"></div>
                            <div id="quota-selected-text">
                                Gewähltes Datum: <span id="quota-selected-date">---</span>
                            </div>
                            <div id="quota-count-text">
                                Kontingent: <span id="quota-count-selected">---</span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Verfügbarkeit</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <button class="quota-title btn btn-lg btn-cal" id="btn-cal-today">Heute</button>
                                            <span id="quota-count-today"></span>
                                        </div>
                                        <div class="col-sm-12">
                                            <button class="quota-title btn btn-lg btn-cal" id="btn-cal-tomorrow">Morgen</button>
                                            <span id="quota-count-tomorrow"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table class="table table-legend" width="100%">
                                                <tr>
                                                    <td width="50px" class="legend-selected"></td>
                                                    <th>Gewählter Tag</th>
                                                </tr>
                                                <tr>
                                                    <td class="legend-available"></td>
                                                    <th>Karten verfügbar</th>
                                                </tr>
                                                <tr>
                                                    <td class="legend-not-available"></td>
                                                    <th>Nicht verfügbar</th>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 hidden" id="target-user-data">
                    <h2>
                        5. Kundendaten <small>(* Pflichtfeld)</small>
                        @unless($terminal)
                            <button class="btn btn-load btn-sm" type="button" id="btn-load-user">
                                <span class="fa fa-refresh fa-fw"></span>
                                Von letzter Karte übernehmen
                            </button>
                        @endunless
                    </h2>
                    <div class="row indent-left">
                        <div class="col-lg-6 col-sm-8">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="fisher_id">Angler-ID</label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <div class="input-group">
                                            <input class="form-control" id="fisher_id" name="fisher_id" type="text" placeholder="ABC-123-DEF" data-mask="SSS-000-SSS" />
                                            <span class="input-group-btn">
                                                <button class="btn btn-form" id="btn-load-fisher">Daten laden</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-divider-horizontal">
                                    <span>oder</span>
                                </div>
                                <div class="form-group hidden" id="input-ticket-time">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="time_hour">Startzeit *</label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <input class="form-control input-sm" id="time_hour" name="time_hour" type="number" required="required" value="0" min="0" max="23"/>
                                            </div>
                                            <div class="col-xs-3">
                                                <input class="form-control input-sm" id="time_minute" name="time_minute" type="number" required="required" value="0" min="0" max="59" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="first_name">Vorname *</label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <input class="form-control" id="first_name" name="first_name" type="text" required="required" placeholder="Max" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="last_name">Nachname *</label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <input class="form-control" id="last_name" name="last_name" type="text" required="required" placeholder="Mustermann" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="birthday">Geburtsdatum *</label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <div class="row">
                                            <div class="col-lg-3 col-xs-4">
                                                <input class="form-control" id="birth_day" name="birth_day" type="number" required="required" placeholder="Tag" min="1" max="31" />
                                            </div>
                                            <div class="col-lg-3 col-xs-4">
                                                <input class="form-control" id="birth_month" name="birth_month" type="number" required="required" placeholder="Monat" min="1" max="12" />
                                            </div>
                                            <div class="col-lg-4 col-xs-4">
                                                <input class="form-control" id="birth_year" name="birth_year" type="number" required="required" placeholder="Jahr" min="1900" max="2020" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="email">E-Mail-Adresse</label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <input class="form-control" id="email" name="email" type="email" placeholder="max@mustermann.at" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="phone">Telefon</label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <input class="form-control" id="phone" name="phone" type="text" placeholder="+43 ..." />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="street">Straße *</label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <input class="form-control" id="street" name="street" type="text" required="required" placeholder="Musterstraße 3" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="post_code">Postleitzahl *</label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <input class="form-control" id="post_code" name="post_code" type="text" required="required" placeholder="2349" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="city">Ort/Stadt *</label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <input class="form-control" id="city" name="city" type="text" required="required" placeholder="Musterstadt" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="country_id">Land *</label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <select class="form-control" id="country_id" name="country_id" required="required">
                                            @foreach($countries as $country)
                                                <option value="{{ $country->id }}" {{ $country->id == $resellerCountryId ? 'selected="selected"' : '' }}>{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hidden" id="target-group">
                        <h2>Weitere Personen (max. <span id="target-group-count"></span>)</h2>
                        <div class="row indent-left space-after">
                            <div class="col-lg-12">
                                <ul id="target-group-list"></ul>
                            </div>
                        </div>
                        <div class="row indent-left space-after">
                            <div class="col-lg-12">
                                <button class="btn btn-load" id="btn-add-person">
                                    <span class="fa fa-user-plus fa-fw"></span>
                                    Person hinzufügen
                                </button>
                            </div>
                        </div>
                    </div>
                    <h2>Berechtigungen</h2>
                    <div class="row indent-left space-after">
                        <div class="col-lg-6 col-sm-8">
                            <div class="form">
                                <div class="form-group">
                                    <label class="control-label" for="city">Berechtigungs-Nr. *</label>
                                    <input class="form-control" id="authorization_id" name="authorization_id" required="required" type="text" placeholder="86564" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="city">Ausstellende Behörde *</label>
                                    <input class="form-control" id="issuing_authority" name="issuing_authority" required="required" type="text" placeholder="Behörde" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="license-list">Berechtigungen in Besitz</label>
                                    <ul id="license-list"></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row indent-left space-after">
                        <div class="col-lg-6 col-sm-8">
                            <div class="alert alert-danger hidden" id="status-error"></div>
                            <div class="alert alert-success hidden" id="status-success">
                                Eingabe erfolgreich, Sie können nun die Karte drucken oder eine neue Eingabe starten.
                            </div>
                        </div>
                        <div class="col-sm-12" id="target-form-buttons">
                            <button class="btn btn-form btn-lg" id="ticket-save">Speichern</button>
                            <a class="btn btn-form btn-lg hidden" id="ticket-print" href="#" target="_blank">Drucken</a>
                            <a class="btn btn-form btn-lg hidden" id="ticket-new" href="{{ route('pos::tickets.index', ['pos' => $pos]) }}">Neue Karte</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        var ticketUrl = '{{ route('pos::tickets.index', ['pos' => $pos])}}';
    </script>
@endpush