@extends('pos.layout.base')

@section('page_title', 'Angelkartenausgabe')

@section('page_body')
    <nav class="navbar navbar-default navbar-fixed-top top-nav">
        <div class="container-fluid">
            <div class="navbar-brand">
                <img src="{{ asset('img/sales/logo-pos.'.$av.'.png') }}" alt="hejfish-logo" />
            </div>
            <div class="navbar-brand">
                Ausgabestelle
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{ route('pos::tickets.index', ['pos' => $pos]) }}">
                        <span class="fa fa-ticket fa-fw"></span> Angelkarten
                    </a>
                </li>
                {{--<li>
                    <a href="{{ route('pos::products.index', ['pos' => $pos]) }}">
                        <span class="fa fa-cart-arrow-down fa-fw"></span> Produkte
                    </a>
                </li>--}}
                @unless($terminal)
                <li>
                    <a href="{{ route('pos::sales.index', ['pos' => $pos]) }}">
                        <span class="fa fa-bar-chart fa-fw"></span> Statistik
                    </a>
                </li>
                @endunless
                <li>
                    <a href="{{ route('pos::logout', ['pos' => $pos]) }}">
                        <span class="fa fa-sign-out fa-fw"></span> Abmelden
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <nav class="sidebar">
        <img class="top-logo" src="{{ asset('img/sales/logo-pos.'.$av.'.png') }}" alt="hejfish-logo" />
        <h2 class="side-menu-title text-center">Ausgabestelle</h2>
        <ul class="side-menu">
            <li>
                <a
                    href="{{ route('pos::tickets.index', ['pos' => $pos]) }}"
                    @if(Route::currentRouteName() === 'pos::tickets.index')
                        class="active"
                    @endif
                >Angelkarten verkaufen</a>
            </li>
            {{--<li>
                <a
                    href="{{ route('pos::products.index', ['pos' => $pos]) }}"
                    @if(starts_with(Route::currentRouteName(), 'pos::products.'))
                    class="active"
                    @endif
                >Produkt verkaufen</a>
            </li>--}}
            @unless($terminal)
                <li>
                    <a
                        href="{{ route('pos::sales.index', ['pos' => $pos]) }}"
                        @if(Route::currentRouteName() === 'pos::sales.index' ||
                            Route::currentRouteName() === 'pos::sales.storno.index')
                            class="active"
                        @endif
                    >Verkaufsstatistik</a>
                </li>
            @endunless
        </ul>
        <div class="side-menu-footer">
            <ul class="side-menu">
                <li class="login-info">
                    {{ $reseller->name }}
                </li>
                <li>
                    <a href="{{ route('pos::logout', ['pos' => $pos]) }}">Abmelden</a>
                </li>
            </ul>
        </div>
    </nav>
    <main class="container-fluid content" id="main-content">
        @yield('page_content')
    </main>
@endsection