<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="hejAdmin, hejfish">
    <meta name="author" content="FiOApps GmbH">
    <title>
        @if(View::hasSection('page_title'))
            @yield('page_title') -
        @endif
        hejfish.com
    </title>
    <link rel="icon" type="image/png" sizes="48x48" href="{{ asset('favicon.'.$av.'.png') }}">
    {!! Html::style('lib/bootstrap/dist/css/bootstrap.min.'.$av.'.css') !!}
    {!! Html::style('lib/font-awesome/css/font-awesome.min.'.$av.'.css') !!}
    {!! Html::style('lib/datatables.net-bs/css/dataTables.bootstrap.min.'.$av.'.css') !!}
    {!! Html::style('lib/chosen/chosen.'.$av.'.css') !!}
    {!! Html::style('css/jquery-ui.min.'.$av.'.css') !!}
    {!! Html::style('css/pos.'.$av.'.css') !!}
    @stack('styles')
</head>
<body>
@include('analytics.google')
@yield('page_body')

{!! Html::script('lib/jquery/dist/jquery.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/bootstrap/dist/js/bootstrap.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/datatables.net/js/jquery.dataTables.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/datatables.net-bs/js/dataTables.bootstrap.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/moment/min/moment.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/moment/locale/de-at.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/jquery-mask-plugin/dist/jquery.mask.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('js/loadingoverlay.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/chosen/chosen.jquery.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('js/jquery-ui.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('js/datepicker-de.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('js/pos.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
<script nonce="{{ csp_nounce() }}">
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    $.datepicker.setDefaults($.datepicker.regional['de']);
</script>
@stack('scripts')
</body>
</html>