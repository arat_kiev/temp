@extends('pos.layout.master')

@section('page_content')
    <div id="reseller-page">
        <div class="row">
            <div class="col-lg-12">
                <h1>Produkt wählen</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-5 col-md-6 pull-right">
                        <div class="form-group">
                            <label for="area">Gewässer</label>
                            <select class="form-control" name="area" id="filter-area">
                                @foreach($areaList as $areaId => $areaName)
                                    <option
                                            value="{{ $areaId }}"
                                            @if($areaId == $area)
                                            selected="selected"
                                            @endif
                                    >
                                        {{ $areaName }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-products">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Preis</th>
                            <th>Gültig von</th>
                            <th>Gültig bis</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        function updateFilters() {
            var area = $('#filter-area').val(),
                location = '{{ route('pos::products.data', ['pos' => $pos]) }}',
                params = [];

            if (area > 0) {
                params.push('area=' + area);
            }
            $table.ajax.url(location + '?' + params.join('&')).load();
        }

        $('#filter-area').on('change', updateFilters);

        var $table = $('#datatable-products').DataTable({
            bFilter: true,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('pos::products.data', ['pos' => $pos]) }}',
            pagingType: "full_numbers",
            columns: [
                { data: 'name' },
                { data: 'price', sortable: false, searchable: false },
                { data: 'featured_from', searchable: false },
                { data: 'featured_till', searchable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });
    });
</script>
@endpush