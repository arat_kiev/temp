@extends('pos.layout.master')

@inject('commissionManager', 'App\Managers\CommissionManager')

@section('page_content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('pos::products.index', ['pos' => $pos]) }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Produkte <small>{{ $product->name }}</small>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default product-data">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-3"><strong>Preis</strong></div>
                        <div class="col-sm-9">
                            {{ $product->price
                                ? $commissionManager->formatFloat(
                                    $commissionManager->fetchTotalProductPriceValue($product->price, false)
                                    )
                                : '-' }} €
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><strong>Gültig von</strong></div>
                        <div class="col-sm-9">
                            {{ $product->featured_from ? $product->featured_from->format('d.m.Y') : '' }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"><strong>Gültig bis</strong></div>
                        <div class="col-sm-9">
                            {{ $product->featured_till ? $product->featured_till->format('d.m.Y') : '' }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Galerie
                </div>
                <div class="panel-body">
                    @if($product->gallery && $product->gallery->pictures)
                        @foreach($product->gallery->pictures as $picture)
                            <img src="{{ route('imagecache', ['template' => 'lLogo', 'filename' => $picture->file]) }}"
                                 class="thumbnail img-responsive" />
                        @endforeach
                    @else
                        <div class="alert alert-warning">
                            <strong>Keine Bilder</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Produkt verkaufen
                </div>
                <div class="panel-body" id="entity-data">
                    <h2>Kundendaten <small>(* Pflichtfeld)</small></h2>
                    <div class="row indent-left">
                        <div class="col-lg-6 col-sm-8">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="fisher_id">
                                        Angler-ID
                                    </label>
                                    <div class="col-lg-9 col-md-8 col-sm-7" style="z-index:0">
                                        <div class="input-group">
                                            <input class="form-control"
                                                   id="fisher_id"
                                                   name="fisher_id"
                                                   type="text"
                                                   placeholder="ABC-123-DEF"
                                                   data-mask="SSS-000-SSS" />
                                            <span class="input-group-btn">
                                                <button class="btn btn-form" id="btn-load-fisher">Daten laden</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-divider-horizontal">
                                    <span>oder</span>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="first_name">
                                        Vorname *
                                    </label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <input class="form-control"
                                               id="first_name"
                                               name="first_name"
                                               type="text"
                                               required="required"
                                               placeholder="Max" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="last_name">
                                        Nachname *
                                    </label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <input class="form-control"
                                               id="last_name"
                                               name="last_name"
                                               type="text"
                                               required="required"
                                               placeholder="Mustermann" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="birth_day">
                                        Geburtsdatum *
                                    </label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <div class="row">
                                            <div class="col-lg-3 col-xs-4">
                                                <input class="form-control"
                                                       id="birth_day"
                                                       name="birth_day"
                                                       type="number"
                                                       required="required"
                                                       placeholder="Tag"
                                                       min="1"
                                                       max="31" />
                                            </div>
                                            <div class="col-lg-3 col-xs-4">
                                                <input class="form-control"
                                                       id="birth_month"
                                                       name="birth_month"
                                                       type="number"
                                                       required="required"
                                                       placeholder="Monat"
                                                       min="1"
                                                       max="12" />
                                            </div>
                                            <div class="col-lg-4 col-xs-4">
                                                <input class="form-control"
                                                       id="birth_year"
                                                       name="birth_year"
                                                       type="number"
                                                       required="required"
                                                       placeholder="Jahr"
                                                       min="1900"
                                                       max="{{ \Carbon\Carbon::now()->year }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="email">
                                        E-Mail-Adresse
                                    </label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <input class="form-control"
                                               id="email"
                                               name="email"
                                               type="email"
                                               placeholder="max@mustermann.at" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="street">
                                        Straße *
                                    </label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <input class="form-control"
                                               id="street"
                                               name="street"
                                               type="text"
                                               required="required"
                                               placeholder="Musterstraße 3" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="post_code">
                                        Postleitzahl *
                                    </label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <input class="form-control"
                                               id="post_code"
                                               name="post_code"
                                               type="text"
                                               required="required"
                                               placeholder="2349" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="city">
                                        Ort/Stadt *
                                    </label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <input class="form-control"
                                               id="city"
                                               name="city"
                                               type="text"
                                               required="required"
                                               placeholder="Musterstadt" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 col-md-4 col-sm-5 control-label" for="country_id">
                                        Land *
                                    </label>
                                    <div class="col-lg-9 col-md-8 col-sm-7">
                                        <select class="form-control"
                                                id="country_id"
                                                name="country_id"
                                                required="required">
                                            @foreach($countries as $country)
                                                <option value="{{ $country->id }}"
                                                        @if($country->id == $resellerCountryId)
                                                        selected="selected"
                                                        @endif
                                                >
                                                    {{ $country->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row indent-left space-after">
            <div class="col-lg-6 col-sm-8">
                <div class="alert alert-danger hidden" id="status-error">
                    Beim Kauf sind Fehler aufgetreten, bitte überprüfen Sie Ihre Eingabe.<br/>
                    Achten Sie bitte auch darauf, dass alle Pflichtfelder ausgefüllt sind.
                </div>
                <div class="alert alert-success hidden" id="status-success">
                    Eingabe erfolgreich, Sie können nun die Karte drucken oder eine neue Eingabe starten.
                </div>
            </div>
            <div class="col-sm-12" id="target-form-buttons">
                <button class="btn btn-form btn-lg" id="product-buy">
                    Verkaufen
                </button>
                <a class="btn btn-form btn-lg hidden" id="attachment-download" href="#" target="_blank">
                    Herunterladen
                </a>
                <a class="btn btn-form btn-lg hidden"
                   id="product-back"
                   href="{{ route('pos::products.index', ['pos' => $pos]) }}">
                    Zur alle Produkte
                </a>
                <button class="btn btn-form btn-lg hidden" id="refresh-page">
                    Wieder verkaufen
                </button>
            </div>
        </div>
    </div>
@endsection

@push('styles')
<style>
    .product-data .row {
        padding: 10px 0;
    }
</style>
@endpush

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $(document).ready(function () {
        $('#refresh-page').on('click', function (e) {
            e.preventDefault();

            hideMessages();
            $('#product-buy').removeClass('hidden');
            $('#product-back,#refresh-page,#attachment-download').addClass('hidden');
            $('#attachment-download').prop('href', '#');
            $('input').val('');
        });

        $('#product-buy').on('click', function (e) {
            e.preventDefault();

            var saveBtn = $(this);
            saveBtn.prop('disabled', true).addClass('disabled');

            var data = {
                product_id: {{ $product->id }},
                first_name: $('#first_name').val(),
                last_name:  $('#last_name').val(),
                street:     $('#street').val(),
                post_code:  $('#post_code').val(),
                city:       $('#city').val(),
                country_id: $('#country_id').val(),
                birthday:   $('#birth_year').val() + "-" + $('#birth_month').val() + "-" + $('#birth_day').val(),
                email:      $('#email').val(),
                fisher_id:  $('#fisher_id').val()
            };

            $.ajax({
                type: 'POST',
                url: '{{ route('pos::products.buy', ['pos' => $pos]) }}',
                data: data,
                dataType: 'json',
                cache: false
            }).done(function (data) {
                console.log(data);
                var attachment = data.id ? data.links.attachment : undefined;
                showSuccess();
                afterBuyAction(attachment);
            }).fail(function () {
                showFailure();
            }).always(function () {
                saveBtn.prop('disabled', false).removeClass('disabled');
            });
        });

        function showSuccess() {
            $('#status-error').addClass('hidden');
            $('#status-success').removeClass('hidden');
        }

        function showFailure() {
            $('#status-error').removeClass('hidden');
            $('#status-success').addClass('hidden');
        }

        function hideMessages() {
            $('#status-error').addClass('hidden');
            $('#status-success').addClass('hidden');
        }

        function afterBuyAction(attachmentUrl) {
            $('#product-buy').addClass('hidden');
            $('#product-back,#refresh-page').removeClass('hidden');

            if (attachmentUrl) {
                $('#attachment-download').prop('href', attachmentUrl);
                $('#attachment-download').removeClass('hidden');
            }

            scrollTo('#target-form-buttons', 60);
        }

        //--- Scroll to helper
        function scrollTo(el, offset)
        {
            $('html, body').animate({
                scrollTop: $(el).offset().top - offset
            }, 1000);
        }
    });
</script>
@endpush
