@extends('pos.layout.master')

@section('page_content')
    <div id="reseller-page">
        <div class="row">
            <div class="col-lg-12">
                <h1>
                    Stornostatistik <small>Gesamt: {{ $count }} ({{ $total }})</small>
                    <div class="pull-right">
                        <a href="{{ route('pos::sales.index', ['pos' => $pos]) }}" class="btn btn-info">
                            <i class="fa fa-arrow-left fa-fw"></i> Verkaufte Angelkarten
                        </a>
                        <br>
                    </div>
                </h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-5 col-md-6 pull-right" style="min-height: 45px">
                        <div class="input-group input-daterange" style="z-index:0">
                            <div class="input-group-addon input-group-label">Kaufdatum</div>
                            <input type="text"
                                   class="form-control"
                                   id="date-range-from"
                                   value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $from)->format('d.m.Y') }}">
                            <span class="input-group-addon">bis</span>
                            <input type="text"
                                   class="form-control"
                                   id="date-range-till"
                                   value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $till)->format('d.m.Y') }}">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="button" id="filter-date-range-btn">
                                    Filter
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-tickets">
                        <thead>
                        <tr>
                            <th>Karten-Nr.</th>
                            <th>Käufer</th>
                            <th>Fischer-ID</th>
                            <th>Gewässer</th>
                            <th>Kartentyp</th>
                            <th>Gültig am/ab</th>
                            <th>Gekauft am/ab</th>
                            <th>Preis</th>
                            <th>Aktionen</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
{!! Html::style('lib/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.'.$av.'.css') !!}
<style>
    .input-daterange {
        margin-bottom: 10px;
    }

    @media(min-width: 992px) {
        .input-daterange {
            margin-bottom: -30px;
        }
    }
</style>
@endpush

@push('scripts')
{!! Html::script('lib/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/bootstrap-datepicker/dist/locales/bootstrap-datepicker.de.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
<script nonce="{{ csp_nounce() }}">
    $.fn.datepicker.defaults.language = 'de';
    $.fn.datepicker.defaults.zIndexOffset = 1001;

    $('document').ready(function() {
        $('.input-daterange').datepicker();

        function updateFilters() {
            var from = $('#date-range-from').datepicker('getDate'),
                    till = $('#date-range-till').datepicker('getDate'),
                    fromString = moment(from).format('YYYY-MM-DD'),
                    tillString = moment(till).format('YYYY-MM-DD'),
                    location = '{{ route('pos::sales.storno.index', ['pos' => $pos]) }}',
                    params = [
                        'from=' + fromString,
                        'till=' + tillString
                    ];

            window.location = location + '?' + params.join('&');
        }

        $('#filter-date-range-btn').on('click', updateFilters);
        $('#date-range-from').on('change', updateFilters);
        $('#date-range-till').on('change', updateFilters);

        $('#datatable-tickets').DataTable({
            bFilter: true,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('pos::sales.storno.data', ['pos' => $pos]) }}' + window.location.search,
            pagingType: "full_numbers",
            columns: [
                {data: 'ticket_number', sortable: false, searchable: false},    // searchable in BE
                {data: 'user_name', sortable: false, searchable: false},        // searchable in BE
                {data: 'fisher_id', sortable: false, searchable: false},        // searchable in BE
                {data: 'area_name', sortable: false, searchable: false},        // searchable in BE
                {data: 'ticket_type', sortable: false, searchable: false},
                {data: 'valid_from', sortable: false, searchable: false},
                {data: 'created_at', sortable: false, searchable: false},
                {data: 'price', sortable: false, searchable: false},
                {data: 'actions', sortable: false, searchable: false}
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });
    });
</script>
@endpush