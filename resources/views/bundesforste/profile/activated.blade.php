@extends('layout.responsive')

@section('page_title')
    E-Mail-Adresse bestätigt
@endsection

@section('page_content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                <img src="https://api.hejfish.com/img/bund_email_header.png" width="800px" class="img img-responsive" />
                <div class="alert alert-success alert-password text-center">
                    <strong>
                        Ihre E-Mail-Adresse wurde bestätigt. Sie können das Fenster jetzt schließen.
                    </strong>
                    <br />
                    @if ($baseUrl)
                        <a href="{{ $baseUrl }}/profile/main" class="btn btn-lg btn-danger">

                            Jetzt Profil ausfüllen

                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection