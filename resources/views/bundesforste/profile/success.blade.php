@extends('layout.responsive')

@section('page_title')
    Passwort erfolgreich geändert
@endsection

@section('page_content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                <img src="https://api.hejfish.com/img/bund_email_header.png" width="800px" class="img img-responsive" />
                <div class="alert alert-success alert-password text-center">
                    <strong>Passwort erfolgreich geändert. Sie können diese Seite nun schließen.</strong>
                </div>
            </div>
        </div>
    </div>
@endsection