@extends('bissanzeiger.email.base')

@section('subject'){{ trans('email.password.subject') }}@stop

@section('content')
    <h2>Passwort zurücksetzen</h2>
    <br/>
    <p>
        Es wurde eine Anfrage empfangen, Ihr Passwort für hejfish zu ändern.
        Um den Vorgang abzuschließen klicken Sie bitte auf folgenden Link<br/>
        <a href="{{ route('v1.auth.password_new', ['token' => $token]) }}" class="system-link">
            Neues Passwort setzen
        </a>
    </p>
    <br/>
    <p>
        Falls der Link nicht funktioniert können Sie auch folgende Adresse in Ihren Browser eingeben<br/>
        <div class="copy-address">{{ route('v1.auth.password_new', ['token' => $token]) }}</div>
    </p>
@stop