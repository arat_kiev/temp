@extends('ticket.layout.base')

@inject ('tm', 'App\Managers\TicketManager')

@section('main_ticket_user')
    @php
        $user = $ticket->resellerTicket ?: $ticket->user;
    @endphp

    <div class="user">
        Ausgestellt auf:

        <div class="name">{{ $user->full_name }}</div>
        <div class="birthday">geboren am: {{ $user->birthday->format('d.m.Y') }}</div>
        <ul class="address">
            <li>{{ $user->street }}</li>
            <li>{{ $user->post_code }} {{ $user->city }}</li>
            <li>{{ $user->country->name }}</li>
        </ul>

        <hr class="divider"/>
        @if(isset($user->authorization_id))
            <div class="authorization-id">
                Fischereischein-Nummer:<br/>
                {{ $user->authorization_id }}
                <br/><br/>
                @if(isset($user->issuing_authority))
                    Ausstellende Behörde:<br/>
                    {{ $user->issuing_authority }}
                @endif
            </div>
            <hr class="divider"/>
        @else
            <div class="authorization-id">
                Fischereischein-Nummer:<br/>
                {{ $tm->getGermanCardId($ticket) }}
            </div>
            <hr class="divider"/>
        @endif
    </div>
@endsection

@section('main_ticket_header')
    @component('ticket.components.header')
        www.angeln-im-sauerland.de
    @endcomponent
@endsection

@section('main_ticket_intro')
    @component('ticket.components.ticket-intro')
        Bei Tages- und 2-Tageskarten muss der Fang nach dem Angeltag und bei Wochen- und Jahreskarten
        muss der Fang nach Ablauf der Angelerlaubnis in die Fangstatistik auf <i><b>www.shop.angeln-im-sauerland.de</b></i>
        wie folgt eingetragen werden: <i><b>"Jeweiliges Gewässer wählen &gt; Fischart gefangen &gt; Fang eintragen".</b></i>
    @endcomponent
@endsection

@section('main_ticket_legal')
    @component('ticket.components.ticket-legal')
        Es gelten die fischereigesetzlichen Bestimmungen des Landes Nordrhein-Westfalen.
    @endcomponent
@endsection

@section('main_ticket_brand')
    @component('ticket.components.brand')
        @slot('logo'){{ route('imagecache', ['template' => 'lLogo', 'ticket/rz_logo_sauerland.png']) }}@endslot
        @slot('logo_style'){{ 'height: auto; top: 10px;' }}@endslot
        @slot('code'){{ $tm->getControlCode($ticket) }}@endslot
        @slot('code_style'){{ 'top: 3mm; left: 12mm;' }}@endslot
    @endcomponent
@endsection

@section('main_ticket_catchlog')
    @if($tm->shouldDisplayCatchlog($ticket) && $ticket->type->fish_per_day > 0)
        <table class="log">
            <thead>
            <tr>
                <th class="type" style="width: 30%;">Fischart</th>
                <th class="size" style="width: 23%;">Anzahl</th>
                <th class="size" style="width: 19%;">cm</th>
                <th class="check" style="width: 28%">entnommen</th>
            </tr>
            </thead>
            <tbody>
            @foreach (range(1, $ticket->type->fish_per_day) as $i)
                <tr>
                    <td>{{ $i }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection

@section('catchlog')
    <div class="catchlog-page">
        <table>
            <thead>
            <tr>
                <th colspan="10">
                    <table class="page-header">
                        <tr>
                            <th width="100%" colspan="{{ $ticket->type->fish_per_day }}">
                                <span class="title">Fangstatistik ({{ $ticket->type->name }})</span>
                            </th>

                            <th class="qrcode-container" rowspan="3">
                                <img class="qrcode"
                                     src="data:image/svg+xml;base64, {!! base64_encode($tm->getQrCode($ticket)) !!} ">
                            </th>
                        </tr>
                        <tr>
                            <th colspan="{{ $ticket->type->fish_per_day * 3}}">
                                <span class="title">{{ $ticket->type->area->name }}</span>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="{{ ceil($ticket->type->fish_per_day / 2) }}">
                                {{ $tm->getUserName($ticket) }}<br/>
                                {{ $tm->getIdentifier($ticket) }}&nbsp;&nbsp;
                                ({{ $tm->getValidString($ticket) }} )
                            </th>
                        </tr>
                    </table>
                </th>
            </tr>
            <tr>
                <th style="width:12%">Datum</th>
                <th style="width:12%">Fischart</th>
                <th style="width:12%">Anzahl</th>
                <th style="width:7%">cm</th>
                <th style="width:7%" class="vertical-divider">ent-<br/>nommen</th>
                <th style="width:12%">Datum</th>
                <th style="width:12%">Fischart</th>
                <th style="width:12%">Anzahl</th>
                <th style="width:7%">cm</th>
                <th style="width:7%">ent-<br/>nommen</th>
            </tr>
            </thead>
            <tbody>
            {{-- if maxFishPerDay > 3, display two lines per day --}}
            @foreach (range(1, $tm->getMaxFishingDays($ticket) * ceil($ticket->type->fish_per_day / 2)) as $day)
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="vertical-divider"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection