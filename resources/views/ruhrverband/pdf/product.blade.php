@inject ('productManager', 'App\Managers\ProductSaleManager')
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="{{ asset('css/product.css') }}" />
    <script nonce="{{ csp_nounce() }}">window.status = 'loading';</script>
</head>
<body>
@if (isset($demo) && $demo === true)
    <div id="demo-overlay"></div>
@endif
<div class="user">
    <div class="sender">Fishing & Outdoor Apps GmbH, Hopfengasse 3, A-4020 Linz</div>
    <div class="registered-mail">EINSCHREIBEN</div>
    <div class="name">{{ $sale->first_name }} {{ $sale->last_name }}</div>
    <ul class="address">
        <li>{{ $sale->street }}</li>
        <li>{{ $sale->post_code }} {{ $sale->city }}</li>
        <li>{{ $sale->country->name }}</li>
    </ul>
</div>

<div class="manager">
    <img class="logo" src="{{ route('imagecache', [
        'template' => 'lLogo', 'ticket/ruhrverband-logo.png'
    ]) }}" />
    <div class="name">Ruhrverband Flussgebietsmanagement</div>
    <ul class="address">
        <li>Seestraße 48</li>
        <li>59519 Möhnesee</li>
        <li>Deutschland</li>
    </ul>
    <div class="link">shop.angeln-im-sauerland.de</div>
</div>

<div class="purchase">
    <div class="identifier">Rechnungs-Nr.: {{ $productManager->getIdentifier($sale) }}</div>
    <div class="date">Gekauft am: {{ $sale->created_at->format('d.m.Y - H:i') }}</div>
</div>
<div class="product">
    <h3>Vielen Dank für Ihren Einkauf!</h3>
    <span>Hiermit stellen wir Ihnen wie folgt in Rechnung:</span>
    <div class="title">
        <p>{{ $sale->quantity }}x {{ $sale->product->name }} (Preis: {{ $sale->price->value }} €)</p>
        @if ($sale->product->hidden_info)
            <div class="hidden-info">{{$sale->product->hidden_info}}
                <p><strong>Achtung:</strong>Kann sich ändern! Bitte prüfe den Code tagesaktuell in deinen Käufen auf der Webseite.</p>
            </div>
        @endif
    </div>

    <div class="price bold">Gesamtbetrag:  {{ $productManager->getTotalAmount($sale) }} € (inkl. Versand- und Bezahlkosten)</div>
    @foreach($sale->fees as $fee)
        <div>{{ $fee->name }}: {{ $fee->value_with_mark }}</div>
    @endforeach
    <div class="vat">
        MwSt ({{ $productManager->getVatPercent($sale) }}%):
        {{ $productManager->getVatAmount($sale) }} €
    </div>
    <p>Bezahlt mit: Online-Bezahldienst</p>
    <p><strong>Die Plakette wird Ihnen per Einschreiben auf dem Postweg zugestellt.</strong></p>
    <p>Der Ruhrverband ist eine juristische Person des öffentlichen Rechts und gem. §2 Abs. 3 UStG nur im Rahmen seiner Betriebe gewerblicher Art(§ 1 Abs. 1 Nr. 6 und § 4 KStG) gewerblich oder beruflich tätig d.h. steuerpflichtig.</p>
    <p>Steuernummer: 112/5746/0164 |  Umsatzsteuer-IdNr: DE 119 824 147</p><p></p>
    <p>Mit freundlichen Grüßen,</p>
    <p>&nbsp;</p>
    <p>i. A. Ihr Angeln-im-Sauerland-Team</p>
    <p></p>
</div>
<div class="agent-info">
    Vermittler:<br />
    Fishing & Outdoor Apps GmbH, Hopfengasse 3, A-4020 Linz, Österreich<br />
    E-Mail: info&#64;hejfish.com<br />
    www.hejfish.com
</div>
<script nonce="{{ csp_nounce() }}">
    var img = document.querySelector('.manager > .logo');

    if (img.complete) {
        window.status = 'loaded';
    } else {
        img.addEventListener('load', function() {
            window.status = 'loaded';
        });
    }

    setTimeout(function() {
        window.status = 'loaded';
    }, 5000);
</script>
</body>
</html>