@extends('ruhrverband.pdf.ticket')

@inject ('tm', 'App\Managers\TicketManager')

@section('main_ticket_watermark')
    <div id="watermark">
        {{ str_repeat("- {$ticket->first_name} - {$ticket->last_name} - {$ticket->valid_from}", 5000) }}
    </div>
@endsection

@section('main_ticket_purchase')
    <div class="purchase">
        <div class="identifier">BA-17-SONDER001</div>
        <hr class="divider" />
        <div class="date">Datum: {{ $ticket->created_at->format('d.m.Y - H:i') }}</div>
        <div class="price">Preis: {{ $tm->getTicketPrice($ticket) }} €</div>
        @if($ticket->resellerTicket)
            <div class="cash-payment">bar bezahlt</div>
        @endif
        @if($ticket->type->area->manager->uid && $ticket->price->show_vat)
            <div class="vat">
                MwSt ({{ $tm->getVatPercent($ticket) }}%):
                {{ $tm->getVatAmount($ticket) }} €
            </div>
        @endif
        <div class="type">Typ: {{ $ticket->price->name }}</div>
        <div class="status">{{ $ticket->storno_id ? 'STORNO' : '' }}</div>
    </div>

    <div style="margin:1.5mm;">
        <br/><br/><br/><strong>
        Ausgabe des Erlaubnisscheines gemäß Wolfgang Lang (Geschäftsführer Fishing&Outdoor Apps GmbH).<br/>
        Bei Rückfragen im Zuge der Kontrolle am Gewässer wenden Sie sich jederzeit an Herrn Lang unter der Telefonnummer<br/>
        0043 676 849 509 100</strong>
    </div>
@endsection

@section('main_ticket_qrcode')
@endsection

@section('main_ticket_fisher_id')
@endsection