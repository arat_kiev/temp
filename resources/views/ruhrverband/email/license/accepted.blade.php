@extends('ruhrverband.email.base')

@section('subject'){{ trans('email.license.accepted.subject') }}@stop

@section('content')
    <p>Ihre Berechtigung '{{ $license->type->name }}' wurde akzeptiert.</p>
@stop