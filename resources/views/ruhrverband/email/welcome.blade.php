@extends('ruhrverband.email.base')

@section('subject')Willkommen beim Angelkarten-Shop von angeln-im-sauerland.de @stop

@section('content')
    <h2>Willkommen beim Shop von angeln-im-sauerland.de!</h2>
    <br/>
    <p>
        Um Ihre Registrierung abzuschließen und Ihre E-Mail-Adresse zu bestätigen,
        klicken Sie bitte folgenden Link<br/>
        <a href="{{ route('v1.auth.activate', ['activation_code' => $user->activation_code]) }}" class="system-link">
            Registrierung abschließen und E-Mail-Adresse bestätigen
        </a>
    </p>
    <br/>
    <p>
        Falls der Link nicht funktioniert können Sie auch folgende Adresse in Ihren Browser eingeben<br/>
        <div class="copy-address">{{ route('v1.auth.activate', ['activation_code' => $user->activation_code]) }}</div>
    </p>
@stop