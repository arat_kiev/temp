@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Gewässer
                <div class="pull-right">
                    <a href="{{ route('admin::areas.create') }}" class="btn btn-success">
                        <span class="fa fa-plus fa-fw"></span> Gewässer anlegen
                    </a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-areas">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Öffentlich</th>
                    @if($children)<th>Bewirtschafter</th>@endif
                    <th>Verkaufsstellen</th>
                    <th>Angelkarten</th>
                    <th>Galerie</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#datatable-areas').DataTable({
            bFilter: false,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::areas.data') }}{{ $children ? '?affiliate=1' : ''}}',
            pagingType: "full_numbers",
            columns: [
                { data: 'name' },
                { data: 'public', sortable: false },
                @if($children) { data: 'manager', sortable: false }, @endif
                { data: 'resellers', sortable: false },
                { data: 'ticket_types', sortable: false },
                { data: 'gallery', sortable: false },
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });
    });
</script>
@endpush
