<ul class="nav navbar-top-links navbar-right">
    @php
        $topMenu->superadmin_impersonate && $topMenu->superadmin_impersonate->attr('class', 'hidden');
    @endphp
    @include(config('laravel-menu.views.bootstrap-items'), ['items' => $topMenu->roots()])
</ul>