<ul class="nav navbar-top-links navbar-right">
    @include(config('laravel-menu.views.bootstrap-items'), ['items' => $topMenu->roots()])
</ul>
<ul class="nav navbar-top-links navbar-right">
    <p class="navbar-text" id="current-admin-name">{{ $current_organization->name or '' }}</p>

    <div class="navbar-form hidden" id="select-organization-form">
        <div class="form-group">
            <div class="col-sm-4">
                <select id="select-organization" class="form-control" data-placeholder="Verein"></select>
            </div>
        </div>
    </div>
</ul>

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#select-organization').select2({
            width: '400px',
            ajax: {
                url: '{{ route('admin::organizations.index') }}',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.data,
                        pagination: {
                            more: params.page < data.meta.pagination.total_pages
                        }
                    };
                },
                cache: true
            },
            minimumInputLength: 3,
            templateResult: function (organization) {
                if (organization.loading) return organization.name;

                var image = organization.logo ? organization.logo.files.small : '{{ route('imagecache', ['template' => 'lLogo', 'ticket/hejfish_logo.png']) }}';

                return "<div class='select2-result-organization'>" +
                        "<img class='select2-result-organization__avatar' src='" + image + "' />" +
                        "<div class='select2-result-organization__name'>" + organization.name + "</div>" +
                        "<div class='select2-result-organization__address'>" + organization.street + ", " +
                        organization.area_code + " " + organization.city + " / " + organization.country + "</div>";
            },
            templateSelection: function (organization) {
                return organization.text || organization.name;
            },
            escapeMarkup: function (markup) { return markup; }
        }).on('change', function() {
            $.post('{{ route('admin::organizations.index') }}/' + $(this).val())
                    .done(function () {
                        location.reload(true);
                    });
        });

        $('#show-manager-select').on('click', function (evt) {
            evt.preventDefault();

            $('#current-admin-name').addClass('hidden');
            $('#select-organization-form').removeClass('hidden');
            $('#select-organization').select2('open');
        });
    });
</script>
@endpush

@push('styles')
<style>
    .select2-result-organization__avatar {
        height: 44px;
        float: left;
        margin-right: 10px;
    }

    .select2-result-organization__name {
        font-size: 16px;
    }
</style>
@endpush