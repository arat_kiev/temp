<ul class="nav navbar-top-links navbar-right">
    @include(config('laravel-menu.views.bootstrap-items'), ['items' => $topMenu->roots()])
</ul>
<ul class="nav navbar-top-links navbar-right">
    @if (isset($current_manager))
        <p class="navbar-text" id="current-admin-name">{{ $current_manager->name }} ({{ $current_manager->id }})</p>
    @endif
    @if (isset($inspector))
        <p class="navbar-text" id="current-admin-name"><b>Inspektor</b>: {{ $inspector->name }}</p>
    @endif

    <div class="navbar-form hidden" id="select-manager-form">
        <div class="form-group">
            <div class="col-sm-4">
                <select id="select-manager" class="form-control" data-placeholder="Bewirtschafter"></select>
            </div>
        </div>
    </div>

    <div class="navbar-form hidden" id="select-inspector-form">
        <div class="form-group">
            <div class="col-sm-4">
                <select id="select-inspector" class="form-control" data-placeholder="Inspektor"></select>
            </div>
        </div>
    </div>
</ul>

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#select-manager').select2({
            width: '400px',
            ajax: {
                url: '{{ route('admin::managers.index') }}',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.data,
                        pagination: {
                            more: params.page < data.meta.pagination.total_pages
                        }
                    };
                },
                cache: true
            },
            minimumInputLength: 3,
            templateResult: function (manager) {
                if (manager.loading) return manager.name;

                var image = manager.logo ? manager.logo.files.small : '{{ route('imagecache', ['template' => 'lLogo', 'ticket/hejfish_logo.png']) }}';

                return "<div class='select2-result-manager'>" +
                        "<img class='select2-result-manager__avatar' src='" + image + "' />" +
                        "<div class='select2-result-manager__name'>" + manager.name + "</div>" +
                        "<div class='select2-result-manager__address'>" + manager.street + ", " +
                        manager.area_code + " " + manager.city + " / " + manager.country + "</div>";
            },
            templateSelection: function (manager) {
                return manager.text || manager.name;
            },
            escapeMarkup: function (markup) { return markup; }
        }).on('change', function() {
            $.post('{{ route('admin::managers.index') }}/' + $(this).val())
                    .done(function () {
                        location.reload(true);
                    });
        });

        $('#show-manager-select').on('click', function (evt) {
            evt.preventDefault();

            $('#current-admin-name, .navbar-form, .navbar-text').addClass('hidden');
            $('#select-manager-form').removeClass('hidden');
            $('#select-manager').select2('open');
        });

        $('#select-inspector').select2({
            width: '400px',
            ajax: {
                url: '{{ route('admin::inspectors.index') }}',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.data,
                        pagination: {
                            more: params.page < data.meta.pagination.total_pages
                        }
                    };
                },
                cache: true
            },
            minimumInputLength: 3,
            templateResult: function (inspector) {
                if (inspector.loading) return inspector.name;

                var image = inspector.logo
                    ? inspector.logo.files.small
                    : '{{ route('imagecache', ['template' => 'llo', 'ticket/hejfish_logo.png']) }}';

                return "<div class='select2-result-inspector'>"
                    + "    <img class='select2-result-inspector__avatar' src='" + image + "' />"
                    + "    <div class='select2-result-inspector__name'>"
                        + inspector.first_name + " " + inspector.last_name
                    + "    </div>"
                    + "    <div class='select2-result-inspector__username'>" + inspector.name + "</div>"
                    + " </div>";
            },
            templateSelection: function (inspector) {
                return inspector.text || inspector.name;
            },
            escapeMarkup: function (markup) { return markup; }
        }).on('change', function() {
            $.post('{{ route('admin::inspectors.index') }}/' + $(this).val())
                .done(function () {
                    location.reload(true);
                });
        });

        $('#show-inspector-select').on('click', function (e) {
            e.preventDefault();

            $('#current-admin-name, .navbar-form, .navbar-text').addClass('hidden');
            $('#select-inspector-form').removeClass('hidden');
            $('#select-inspector').select2('open');
        });
    });
</script>
@endpush

@push('styles')
<style>
    .select2-result-manager__avatar,
    .select2-result-inspector__avatar {
        height: 44px;
        float: left;
        margin-right: 10px;
    }

    .select2-result-manager__name,
    .select2-result-inspector__name {
        font-size: 16px;
    }
</style>
@endpush