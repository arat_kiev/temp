<ul class="nav navbar-top-links navbar-right">
    @include(config('laravel-menu.views.bootstrap-items'), ['items' => $topMenu->roots()])
</ul>
<ul class="nav navbar-top-links navbar-right">
    <p class="navbar-text" id="current-admin-name">{{ $current_rental->name or '' }}</p>

    <div class="navbar-form hidden" id="select-rental-form">
        <div class="form-group">
            <div class="col-sm-4">
                <select id="select-rental" class="form-control" data-placeholder="Bootsverleih"></select>
            </div>
        </div>
    </div>
</ul>

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $('document').ready(function() {
            $('#select-rental').select2({
                width: '400px',
                ajax: {
                    url: '{{ route('admin::rentals.index') }}',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            search: params.term,
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: params.page < data.meta.pagination.total_pages
                            }
                        };
                    },
                    cache: true
                },
                minimumInputLength: 3,
                templateResult: function (rental) {
                    if (rental.loading) return rental.name;

                    var image = '{{ route('imagecache', ['template' => 'lLogo', 'ticket/hejfish_logo.png']) }}';

                    return "<div class='select2-result-rental'>" +
                        "<div class='select2-result-rental__name'>" + rental.name + "</div>" +
                        "<div class='select2-result-rental__address'>" + rental.street + ", " +
                        rental.area_code + " " + rental.city + " / " + rental.country + "</div>";
                },
                templateSelection: function (rental) {
                    return rental.text || rental.name;
                },
                escapeMarkup: function (markup) { return markup; }
            }).on('change', function() {
                $.post('{{ route('admin::rentals.index') }}/' + $(this).val())
                    .done(function () {
                        location.reload(true);
                    });
            });

            $('#show-manager-select').on('click', function (evt) {
                evt.preventDefault();

                $('#current-admin-name').addClass('hidden');
                $('#select-rental-form').removeClass('hidden');
                $('#select-rental').select2('open');
            });
        });
    </script>
@endpush

@push('styles')
    <style>
        .select2-result-rental__name {
            font-size: 16px;
        }
    </style>
@endpush