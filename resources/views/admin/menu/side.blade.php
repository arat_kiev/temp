<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav in" id="side-menu">
            @include('vendor.laravel-menu.sbadmin-navbar-side', ['items' => $sideMenu->roots()])
        </ul>
    </div>
</div>