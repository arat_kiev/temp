@extends('admin.layout.manager')

@inject('ticket_manager', 'App\Managers\TicketManager')

@section('page-content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Verkaufte Angelkarten
            <small>Gesamt: {{ $count }} Stück, {{ $total }}</small>
            <div class="pull-right">
                <a href="{{ route('admin::sales.storno.index') }}" class="btn btn-info">
                    Stornierte Angelkarten
                </a>
                <div class="dropdown" style="display: inline-block">
                    <button type="button" class="btn btn-default" data-toggle="dropdown">
                        <span class="fa fa-download fa-fw"></span> Export
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="{{ route('admin::sales.export', [
                                    'format'    => 'xls',
                                    'from'      => $from,
                                    'till'      => $till,
                                    'reseller'  => $reseller,
                                    'area'      => $area,
                                    'ticketType' => $ticketType,
                                    'invoiced'  => $invoiced,
                                ]) }}">
                                <span class="fa fa-file-excel-o fa-fw"></span> XLS-Datei
                            </a>
                        </li>
                    </ul>
                </div>
                <br>
            </div>
        </h1>
    </div>
</div>

<div class="row main-content">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-5 col-md-6 pull-right">
                <div class="form-group">
                    <label for="reseller">Verkäufer</label>
                    <select class="form-control" name="reseller" id="filter-reseller">
                        @foreach($resellerList as $resellerId => $resellerName)
                        <option
                                value="{{ $resellerId }}"
                                @if($resellerId == $reseller)
                                        selected="selected"
                                @endif
                        >
                            {{ $resellerName }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="area">Gewässer</label>
                    <select class="form-control" name="area" id="filter-area">
                        @foreach($areaList as $areaId => $areaName)
                        <option
                                value="{{ $areaId }}"
                                @if($areaId == $area)
                                        selected="selected"
                                @endif
                        >
                            {{ $areaName }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="ticketType">Kartentyp</label>
                    <select class="form-control" name="ticketType" id="filter-ticketType">
                        @foreach($ticketTypes as $ticketTypeId => $ticketTypeName)
                            <option
                                    value="{{ $ticketTypeId }}"
                                    @if($ticketTypeId == $ticketType)
                                    selected="selected"
                                    @endif
                            >
                                {{ $ticketTypeName }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="invoiced">Abgerechnet</label>
                    <select class="form-control" name="invoiced" id="filter-invoiced">
                        <option value="-1" @if($invoiced == -1) selected="selected" @endif>Alle</option>
                        <option value="0" @if($invoiced == 0) selected="selected" @endif>Nein</option>
                        <option value="1" @if($invoiced == 1) selected="selected" @endif>Ja</option>
                    </select>
                </div>
                <div class="input-group input-daterange">
                    <div class="input-group-addon input-group-label">Kaufdatum</div>
                    <input type="text"
                           class="form-control"
                           id="date-range-from"
                           value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $from)->format('d.m.Y') }}">
                    <span class="input-group-addon">bis</span>
                    <input type="text"
                           class="form-control"
                           id="date-range-till"
                           value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $till)->format('d.m.Y') }}">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="button" id="filter-date-range-btn">Filter</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover dataTable no-footer" id="datatable-sales">
                <thead>
                <tr>
                    <th></th>
                    <th>#</th>
                    <th>Name</th>
                    <th>Gewässer</th>
                    <th>Fänge</th>
                    <th>Ab Datum</th>
                    <th>Gekauft</th>
                    <th>Verkäufer</th>
                    @if($children)
                    <th>Bewirtschafter</th>
                    @endif
                    <th>Preis</th>
                    <th>Anteil</th>
                    <th>Abgerechnet</th>
                    <th>Aktionen</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

{{-- preview modal --}}
<div class="modal fade in" id="preview-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false"
     style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span>
                </button>
                <h4 class="modal-title">
                    <span class="fa fa-eye fa-fw"></span>
                    <span id="preview-title">Vorschau wird geladen (bitte etwas Geduld)</span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="col-lg-12">
                        <img class="preview-image img-responsive" width="1083px"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
            </div>
        </div>
    </div>
</div>

{{-- delete confirmation modal --}}
<div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false"
     style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form class="form" method="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Schließen">
                        <span>&times;</span></button>
                    <h4 class="modal-title">
                        Stornierung bestätigen
                    </h4>
                </div>
                <div class="modal-body">
                    <p>Sind Sie sicher, dass Sie diese Angelkarte stornieren wollen?</p>
                    <hr>
                    <div class="form-group">
                        <label for="storno-reason">Grund der Stornierung</label>
                        <textarea class="form-control" id="storno-reason"></textarea>
                    </div>
                </div>
                <input type="hidden" name="_method" value="delete"/>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">
                        <span class="fa fa-remove fa-fw"></span> Stornieren
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- add to blacklist confirmation modal --}}
<div class="modal fade in" id="add-to-blacklist-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form class="form" id="add-to-blacklist-form" method="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                    <h4 class="modal-title">
                        <span class="fa fa-ban fa-fw"></span> Sperrliste
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="add-to-blacklist-user-reason" class="control-label"> Begründung </label>
                                <textarea class="form-control" name="reason" id="add-to-blacklist-user-reason" rows="1" required></textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="add-to-blacklist-banned-till"> Gesperrt bis </label>
                                <div class="input-group date" data-provide="datepicker">
                                    <input id="add-to-blacklist-banned-till" name="banned_till" type="text" class="form-control" placeholder="dd.mm.yyyy">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <h5 class="error-message"></h5>
                    <button type="button" class="btn btn-primary" id="add-to-blacklist-submit"><span class="fa fa-save fa-fw"></span> Speichern</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- multi hauls modal --}}
<div class="modal fade in" id="multi-hauls-modal" tabindex="-1" role="dialog" data-backdrop="static"
     aria-hidden="false" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form class="form" id="multi-haul-form">
                {{ csrf_field() }}
                <input type="hidden" name="ticket" id="ticket-id" required>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Schließen">
                        <span>&times;</span></button>
                    <h4 class="modal-title">
                        Fänge eintragen
                    </h4>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">Fänge hochladen</h3>
                    <div class="form-group content">
                        <div class="row">
                            <div class="jumbotron jumbotron-custom">
                                <div class="form-row">
                                    <div class="col-xs-5">
                                        Datum
                                        <input type="text" class="form-control" name="catch_date" id="catch-date"
                                               value="" placeholder="Datum" required>
                                    </div>
                                    <div class="col-xs-5 col-xs-offset-2">
                                        Uhrzeit
                                        <input id="catch-time" type="time" class="form-control" name="catch_time"
                                               placeholder="Uhrzeit"
                                               value="">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-row">
                                    <div class="col-xs-5">
                                        Gewässer
                                        <select class="form-control" id="area-id" name="area_id">
                                            <option default class="option-placeholder">Gewässer</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-5 col-xs-offset-2">
                                            Angelmethode
                                            <select class="form-control" id="fishing-method-id" name="fishing_method_id">
                                                <option default value="NULL" class="option-placeholder">Angelmethode wählen</option>
                                            </select>
                                    </div>
                                </div>
                            </div>
                            <div class="jumbotron container-fluid">
                                <div class="form-row">
                                    <div class="col-xs-6">
                                        Fischart
                                        <select class="form-control" name="fish" id="fish-id" required>
                                            <option default class="option-placeholder">Fischart wählen</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-3">
                                        Größe
                                        <input type="number"
                                               min="1"
                                               step="0.1"
                                               class="form-control "
                                               name="size_value"
                                               placeholder="Größe">
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="btn-group" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="size_type" autocomplete="off" value="CM"
                                                       checked> CM
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="size_type" autocomplete="off" value="KG">
                                                KG
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-row">
                                    <div class="col-xs-6">
                                        Angeldauer in Stunden
                                        <input type="number"
                                               class="form-control"
                                               name="duration"
                                               id="duration"
                                               min="1"
                                               max="23"
                                               step="1"
                                               value="1"
                                               placeholder="Angeldauer"
                                        >
                                    </div>
                                    <div class="col-xs-2">
                                        Anzahl
                                        <input type="number"
                                               min="1"
                                               step="1"
                                               class="form-control "
                                               name="count"
                                               value="1"
                                               placeholder="Anzahl">
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="btn-group" data-toggle="buttons">
                                            <label class="btn active">
                                                <input type="radio" name="taken" autocomplete="off" value="1"
                                                       checked> Entnommen
                                            </label>
                                            <label class="btn">
                                                <input type="radio" name="taken" autocomplete="off" value="0"> Nicht
                                                entnommen
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-row">
                                    <div class="col-xs-6">
                                        Angeltechnik
                                        <select class="form-control" id="technique-id" name="technique_id">
                                            <option default value="NULL" class="option-placeholder">Angeltechnik
                                                wählen
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-xs-12">
                                        <div class="form-row">
                                            Kommentar
                                            <textarea name="user_comment"
                                                      class="form-control"
                                                      placeholder="Kommentar"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="container-fluid">
                                <div class="col-xs-2 col-xs-offset-10">
                                    <button type="submit" class="btn btn-info btn-lg">
                                        <i class="fa fa-save fa-fw"></i> Eintragen
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="hauls-result"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- checkin modal --}}
<div class="modal fade in" id="checkin-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false"
     style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form class="form" method="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Schließen">
                        <span>&times;</span></button>
                    <h4 class="modal-title">
                        Angelzeit eintragen
                    </h4>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">Angelzeit:&nbsp;<span class="duration"></span>&nbsp;(H:m)</h3>
                    <div class="form-group content">
                        {{--  --}}
                    </div>
                    <div class="add-button"><i class="fa fa-plus fa-fw text-info"></i></div>
                </div>
                <input type="hidden" name="_method" value="delete"/>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">
                        <span class="fa fa-save fa-fw"></span> Eintragen
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Empty haul modal -->
<div class="modal fade" id="empty-haul-checkin-modal" tabindex="-1" role="dialog"
     aria-labelledby="empty-haul-checkin-modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="empty-haul-checkin-modal-label">Nichts gefangen</h4>
            </div>
            <div class="modal-body row">
                <form id="empty-haul-form">
                    <div class="col-xs-12 form-group">
                        <span>Datum:</span>
                        <input type="text" name="catch_date" id="catch-date"
                               class="form-control"
                               placeholder="Datum"
                               value="">
                        <input type="hidden" id="ticket-id" name="ticket">
                        <input type="hidden" id="area-id" name="area">
                        <input type="hidden" name="count" value="0">
                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success"><i class="fa fa-save fa-fw"></i> Eintragen</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('styles')
<style>
    .input-daterange {
        margin-bottom: 10px;
    }
</style>
@endpush

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function () {
        $('.input-daterange').datepicker();
        $('input#catch-date').datepicker();

        function updateFilters() {
            var from = $('#date-range-from').datepicker('getDate'),
                till = $('#date-range-till').datepicker('getDate'),
                fromString = moment(from).format('YYYY-MM-DD'),
                tillString = moment(till).format('YYYY-MM-DD'),
                reseller = $('#filter-reseller').val(),
                ticketType = $('#filter-ticketType').val(),
                area = $('#filter-area').val(),
                invoiced = $('#filter-invoiced').val(),
                location = '{{ route('admin::sales.index') }}',
                params = [
                    'from=' + fromString,
                    'till=' + tillString
                ];

            if (reseller >= 0) {
                params.push('reseller=' + reseller);
            }
            if (ticketType > 0) {
                params.push('ticketType=' + ticketType);
            }
            if (area > 0) {
                params.push('area=' + area);
            }
            if (invoiced >= 0) {
                params.push('invoiced=' + invoiced);
            }
            table.state.clear();

            window.location = location + '?' + params.join('&');
        }

        $('#filter-date-range-btn').on('click', updateFilters);
        $('#date-range-from').on('change', updateFilters);
        $('#date-range-till').on('change', updateFilters);
        $('#filter-reseller').on('change', updateFilters);
        $('#filter-ticketType').on('change', updateFilters);
        $('#filter-area').on('change', updateFilters);
        $('#filter-invoiced').on('change', updateFilters);

        var table = $('#datatable-sales').DataTable({
            bFilter: true,
            stateSave: true,
            processing: true,
            serverSide: true,
            searchDelay: 1000,
            ajax: {
                url: '{{ route('admin::sales.data') }}' + window.location.search
            },
            initComplete: initTooltip,
            pagingType: "full_numbers",
            columns: [
                {
                    className: 'details-control',
                    orderable: false,
                    searchable: false,
                    data: null,
                    defaultContent: ''
                },
                {data: 'manager_ticket_id', orderable: true},
                {data: 'user_full_name', orderable: false},
                {data: 'type.area.name', orderable: false, searchable: false},
                {data: 'hauls_count', orderable: false, searchable: false},
                {data: 'valid_from', searchable: false},
                {data: 'created_at', searchable: false},
                {data: 'reseller', orderable: false, searchable: false},
                    @if($children){data: 'manager', orderable: false, searchable: false}, @endif
                {
                    data: 'price_compact', orderable: false, searchable: false
                },
                {data: 'additional_manager_share', orderable: false, searchable: false},
                {data: 'invoiced', orderable: false, searchable: false},
                {data: 'actions', orderable: false, searchable: false}
            ],
            order: [[1, 'desc']],
            columnDefs: [
                {
                    'targets': 5,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).addClass('valid_from');
                        $(td).attr('data-date', rowData.from_date);
                        $(td).attr('data-time', rowData.from_time);
                    }
                }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        function format(d) {
            return d.reseller_ticket ? formatDetail(d.reseller_ticket, d) : formatDetail(d.user, d);
        }

        function formatDetail(user, ticket) {
            return '' +
                '<table class="ticket-user-detail">' +
                '<tr>' +
                '<th>Vorname:</th>' +
                '<td>' + user.first_name + '</td>' +
                '<th>BA-Ticket-Nr:</th>' +
                '<td>' + ticket.ticket_number + '</td>' +
                '</tr>' +
                '<tr>' +
                '<th>Nachname:</th>' +
                '<td>' + user.last_name + '</td>' +
                '<th>Ticket-Typ</th>' +
                '<td>' + ticket.type.name + '</td>' +
                '</tr>' +
                '<tr>' +
                '<th>Geburtstag:</th>' +
                '<td>' + moment(new Date(user.birthday)).format("DD.MM.YYYY") + '</td>' +
                '<th>Preis-Typ:</th>' +
                '<td>' + ticket.price.name + '</td>' +
                '</tr>' +
                '<tr>' +
                '<th>Straße:</th>' +
                '<td>' + user.street + '</td>' +
                '<th>Brutto-Preis:</th>' +
                '<td>' + ticket.price_brutto + '</td>' +
                '</tr>' +
                '<tr>' +
                '<th>Ort:</th>' +
                '<td>' + user.city + '</td>' +
                '<th>Gekauft:</th>' +
                '<td>' + ticket.created_at + '</td>' +
                '</tr>' +
                '<tr>' +
                '<th>PLZ:</th>' +
                '<td>' + user.post_code + '</td>' +
                '<th>Gültig von:</th>' +
                '<td>' + ticket.valid_from + '</td>' +
                '</tr>' +
                '<tr>' +
                '<th>Land:</th>' +
                '<td>' + user.country.name + '</td>' +
                '<th>Gültig bis:</th>' +
                '<td>' + ticket.valid_to + '</td>' +
                '</tr>' +
                '</table>' +
                (ticket.inspections.length ? getTicketInspectionsTable(ticket.inspections) : ''
                );
        }

        function getTicketInspectionsTable(inspections) {
            return '<table class="ticket-inspections">'
                + '     <tr>'
                + '         <th colspan="4">Konrollen</th>'
                + '     </tr>'
                + inspections.reduce(function (carry, inspection) {
                    return carry
                        + '<tr>'
                        + '     <td>' + inspection.created_at + '</td>'
                        + '     <td>' + inspection.inspector + '</td>'
                        + '     <td>' + inspection.status + '</td>'
                        + '     <td>' + inspection.comment + '</td>'
                        + '</tr>';
                }, '')
                + ' </table>';
        }

        $('#datatable-sales tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            } else {
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });

        $('#preview-modal').on('show.bs.modal', function (e) {
            var modal = $(this);
            var button = $(e.relatedTarget);
            var ticketId = button.data('ticketId');
            var format = 'image';

            var title = modal.find('#preview-title');
            title.html('Vorschau wird geladen <span class="fa fa-cog fa-spin"></span>');

            modal.find('.preview-image').attr('src', '{{ URL::to('/preview/ticket/') }}/' + ticketId + '/' + format)
                .on('load', function () {
                    title.html('Vorschau für Ticket: ' + ticketId);
                });
        });

        $('#delete-modal').on('show.bs.modal', function (e) {
            var formAction = $(e.relatedTarget).data('action'),
                formMethod = 'POST',
                $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();
                $(document).find('.response-popup').remove();

                $.ajax({
                    type: formMethod,
                    url: formAction,
                    data: {reason: $(document).find('#storno-reason').val()},
                    success: function (data) {
                        var msgBox = ''
                            + '<div class="row response-popup" style="padding-top: 10px;">'
                            + '    <div class="col-lg-12">'
                            + '        <div class="alert alert-' + data.notification + ' alert-dismissable">'
                            +
                            '            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">'
                            + "&times;"
                            + '            </button>'
                            + data.message
                            + '        </div>'
                            + '    </div>'
                            + '</div>';
                        $('.page-header').parents('.row').before(msgBox);
                        if (data.notification == 'success') {
                            table.ajax.reload();
                        }
                        $('#delete-modal').modal('hide');
                    }
                });
            });
        });

        $('#add-to-blacklist-modal').on('shown.bs.modal', function (e) {
            var rowData = table.row( $(e.relatedTarget).closest('tr') ).data();

            // Submit form's data
            $('#add-to-blacklist-submit').unbind('click').click(function () {
                var form = $(this).closest('form');
                var reason = form.find('#add-to-blacklist-user-reason');

                // required field filled
                if(reason.val()) {
                    var userData = rowData.reseller_ticket || rowData.user;
                    var user = {
                        user_id: userData.id,
                        first_name: userData.first_name,
                        last_name: userData.last_name,
                        birthday: userData.birthday,
                        street: userData.street,
                        city: userData.city,
                        country_id: userData.country_id,
                        reason: reason.val(),
                        banned_till: form.find('#add-to-blacklist-user-banned_till').val()
                    };

                    $.ajax( {
                        type: "POST",
                        url: '{{ route('admin::blacklist.users.add') }}',
                        data: user,
                        success: function( response ) {
                            table.ajax.reload(initTooltip, false);
                            form.closest('.modal').modal('hide');
                        },
                        error: function(request) {
                            var fail =  $.parseJSON(request.responseText);
                            form.find('.error-message').html(fail.message || fail.error)
                        }
                    } );
                } else {
                    reason.addClass('required');
                }
            });
        });

        $('#add-to-blacklist-modal').on('hidden.bs.modal', function (e) {
            var form = $(this).find('.form');
            // clear form
            form.find(':input').each(function(){
                $(this).val('');
                $(this).removeClass('required');
            });
            // clear error messages
            form.find('.error-message').text('');
        });

        // Haul multi edit modal
        $('#multi-hauls-modal').on('show.bs.modal', function (e) {
            if (e.target !== this) return;

            $('#catch-date').datepicker();

            var ticketId = $(e.relatedTarget).data('ticket-id'),
                $ticket = $(this).find('#ticket-id'),
                $validFromDate = $(this).find('#catch-date'),
                $validFromTime = $(this).find('#catch-time'),
                $haulsBlock = $('.hauls-result');

            $ticket.val(ticketId);
            // set ticket date
            $validFromDate.val($(e.relatedTarget).data('valid-from-date'));
            $validFromTime.val($(e.relatedTarget).data('valid-from-time'));

            $.ajax({
                url: '{{ route('admin::tickets.details', ['ticketId' => '__TICKET_ID__']) }}'
                    .replace(/__TICKET_ID__/, ticketId),
                method: 'get',
                success: function (response) {
                    if (response.additional_areas !== null) {
                        $.each(response.areas, function (index, area) {
                            $('#area-id').append($("<option></option>")
                                .attr({"value": area.id})
                                .text(area.name));
                        });
                    } else {
                        $('#area-id').append($("<option></option>")
                            .attr({"value": response.area_id, selected: 'selected'})
                            .text(response.area))
                    }
                    $.each(response.fishes, function (index, fish) {
                        $('#fish-id').append($("<option></option>")
                            .attr({"value": fish.id})
                            .text(fish.name));
                    });
                    $.each(response.techniques, function (index, tech) {
                            $('#technique-id').append($("<option></option>")
                                .attr({"value": tech.id})
                                .text(tech.name));
                    });
                    $.each(response.fishingMethods.data, function (index, method) {
                        $('#fishing-method-id').append($("<option></option>")
                            .attr({"value": method.id})
                            .text(method.name));
                    });
                },
                error: function (error) {
                    console.log(error.message);
                }
            });

            $.ajax({
                url: '{{ route('admin::hauls.get-hauls', ['ticketId' => '__TICKET_ID__']) }}'
                    .replace('__TICKET_ID__', ticketId),
                method: 'get',
                success: function (response) {
                    appendHaul($haulsBlock, response.hauls);
                },
                error: function (error) {
                }
            });
        });

        $('#multi-hauls-modal').on('hidden.bs.modal', function (e) {
            var $options = $(this).find('select > option');
            $options.each(function (index, option) {
                $(option).not('.option-placeholder').remove();
            });
            $('.haul-card-row').each(function (index, row) {
                $(row).remove();
            });
        });

        $('#multi-haul-form').submit(function (e) {
            e.preventDefault();
            if ($("#technique-id").val() === 'NULL') {
                $("select[name='technique_id']").attr("disabled", "disabled");
            }
            var formData = $(this).serialize();
            sendHaul($(this), formData);
        });

        function sendHaul(form, haul) {
            var $submitButton = form.find('button[type=submit]'),
                $haulsBlock = $('.hauls-result');
            $("select[name='fish']").removeClass('error');
            $("input[name='size_value']").removeClass('error');
            $("select[name='technique_id']").removeAttr("disabled");

            $submitButton.addClass('disabled').attr('disabled', true);
            $.ajax({
                url: '{{ route('admin::hauls.multi.create') }}',
                method: 'post',
                data: haul,
                dataType: 'JSON',
                success: function (response) {
                    appendHaul($haulsBlock, response.hauls);
                    $submitButton.removeClass('disabled').removeAttr('disabled');
                },
                error: function (error) {
                    var errorBlock = ''
                        + '<div class="alert alert-danger alert-dismissible response-popup" role="alert">'
                        + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                        + '        <span aria-hidden="true">&times;</span>'
                        + '    </button>'
                        + '    Es ist ein Fehler aufgetreten'
                        + '</div>';
                    $(document).find('.modal-body').prepend(errorBlock);
                    if (error.responseJSON && error.responseJSON.fish) {
                        $("select[name='fish']").addClass('error');
                    }
                    if (error.responseJSON && error.responseJSON.size_value) {
                        $("input[name='size_value']").addClass('error');
                    }
                    $submitButton.removeClass('disabled').removeAttr('disabled');

                },
                statusCode: {
                    404: function (error) {
                        console.log(error.responseText);
                    }
                }
            });
        }

        function appendHaul(wrapperBlock, hauls) {
            hauls.map(function (haul) {
                wrapperBlock.append(haul);
            });
        }

        function removeHaulEntranceRow(e) {
            e.preventDefault();

            if ($(document).find('.multi-haul-row').length > 1) {
                var $row = $(this).closest('.multi-haul-row');
                $row.next('hr').remove();
                $row.remove();
            } else {
                $(this).hide();
            }
        }

        $('#empty-haul-checkin-modal').on('shown.bs.modal', function (e) {
            var $triggerButton = $(e.relatedTarget),
                ticketId = $triggerButton.data('ticket-id'),
                areaId = $triggerButton.data('area-id'),
                ticketDate = $triggerButton.data('valid-date');

            $(this).find('input#catch-date').val(ticketDate);
            $(this).find('input#area-id').val(areaId);
            $(this).find('input#ticket-id').val(ticketId);
        });

        $('#empty-haul-checkin-modal form').submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: '{{ route('admin::hauls.empty.create') }}',
                type: 'post',
                dataType: 'json',
                data: $(this).serialize(),
                success: function (response) {
                    if (response.success) {
                        $('#empty-haul-checkin-modal').modal('hide');
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });

        function addMultiHaulEventListeners($modal, blocksIndex, fishes, validFrom) {
            $modal.find('.multi-haul-row').off('change');
            $modal.find('.multi-haul-row').off('click');

            $modal.find('#date' + blocksIndex).off('click').off('change').datepicker();

            $modal.find('.multi-haul-row').on('click', 'i.fa-trash', removeHaulEntranceRow);
            $modal.find('.multi-haul-row').on('click', 'i.fa-plus', function (e) {
                e.preventDefault();
                blocksIndex++;

                var $block = getNewHaulAddBlock(blocksIndex, fishes, validFrom);
                $modal.find('.modal-body .content').append($block);
                addMultiHaulEventListeners($modal, blocksIndex, fishes, validFrom);
                $modal.find('i.fa-trash').show();
            });
        }

        function removeHaulEntranceRow(e) {
            e.preventDefault();

            if ($(document).find('.multi-haul-row').length > 1) {
                var $row = $(this).closest('.multi-haul-row');
                $row.next('hr').remove();
                $row.remove();
            } else {
                $(this).hide();
            }
        }

        function getNewHaulAddBlock(index, fishes, validFrom) {
            return '<div class="row multi-haul-row" data-index="' + index + '">'
                + '     <div class="col-xs-2">'
                + '         <input  type="text"'
                + '                 class="form-control"'
                + '                 name="catch_date"'
                + '                 id="date' + index + '"'
                + '                 value="' + (validFrom.date || moment().format('DD.MM.YYYY')
                ) + '"'
                + '                 required>'
                + '     </div>'
                + '     <div class="col-xs-2">'
                + '         <input  type="time"'
                + '                 class="form-control"'
                + '                 name="catch_time"'
                + '                 value="' + (validFrom.time || moment().format('HH:mm')
                ) + '">'
                + '     </div>'
                + '     <div class="col-xs-2 checkbox">'
                + '         <label>'
                + '             <input type="checkbox" name="taken" checked> Entnommen'
                + '         </label>'
                + '     </div>'
                + '     <div class="col-xs-2">'
                + '         <select class="form-control " name="fish">'
                + '             <option value="" style="color:#aaa">Fischart wählen</option>'
                + Object.keys(fishes).reduce(function (carry, index) {
                    return carry + '<option value="' + index + '">' + fishes[index] + '</option>';
                }, '')
                + '         </select>'
                + '     </div>'
                + '     <div class="col-xs-2">'
                + '         <input  type="number"'
                + '                 class="form-control"'
                + '                 min="1"'
                + '                 step="1"'
                + '                 name="count"'
                + '                 placeholder="Anzahl">'
                + '     </div>'
                + '     <div class="col-xs-2">'
                + '         <input  type="number"'
                + '                 min="1"'
                + '                 step="0.1"'
                + '                 class="form-control "'
                + '                 name="size_value"'
                + '                 placeholder="CM" >'
                + '     </div>'
                + '     <div class="col-xs-2 actions">'
                + '         <i class="fa fa-trash fa-fw text-danger"></i>'
                + '         <i class="fa fa-plus fa-fw text-info"></i>'
                + '     </div>'
                + ' </div>'
                + ' <hr>';
        }

        $('#checkin-modal').on('show.bs.modal', function (e) {
            if (e.target !== this) return; // Event will not fire for children, such as datepicker modal

            var $modal = $(this),
                $form = $modal.find('.form'),
                $button = $(e.relatedTarget),
                ticketId = $button.data('ticket-id'),
                blocksIndex = 1,
                route = '{{ route('admin::tickets.checkins', ['ticketId' => '__TICKET_ID__']) }}'
                    .replace(/__TICKET_ID__/, ticketId);

            $.getJSON(route, {}, function (res) {
                $modal.find('.modal-body .content').empty();
                $modal.find('.modal-body .duration').empty();
                var totalDuration = 0;
                res.forEach(function (checkin) {
                    var fromDiff = moment(checkin.from);
                    var tillDiff = moment(checkin.till);
                    var duration = moment.duration(tillDiff.diff(fromDiff)),
                        checkinFromDate = moment(checkin.from).format('DD.MM.YYYY'),
                        checkinFromTime = moment(checkin.from).format('HH:mm'),
                        checkinTillDate = moment(checkin.till).format('DD.MM.YYYY'),
                        checkinTillTime = moment(checkin.till).format('HH:mm');
                    totalDuration += duration;

                    var $block = getNewCheckinBlock(blocksIndex, checkinFromDate, checkinFromTime, checkinTillDate, checkinTillTime, checkin.id);
                    $modal.find('.modal-body .content').append($block);
                    addCheckinEventListener($modal, blocksIndex, ticketId);
                    blocksIndex++;
                });
                var d = moment.duration(totalDuration);
                var s = Math.floor(d.asHours()) + moment.utc(totalDuration).format(":mm");
                $modal.find('.modal-body .duration').append(s);
            });

            var checkinFromDate = moment().format('DD.MM.YYYY'),
                checkinFromTime = moment().format('HH:mm'),
                checkinTillDate = moment().format('DD.MM.YYYY'),
                checkinTillTime = moment().format('HH:mm');

            $modal.find('.add-button').unbind().on('click', 'i.fa-plus', function (e) {
                e.preventDefault();
                var $block = getNewCheckinBlock(blocksIndex, checkinFromDate, checkinFromTime, checkinTillDate, checkinTillTime, 0);
                $modal.find('.modal-body .content').append($block);
                addCheckinEventListener($modal, blocksIndex, ticketId);
                blocksIndex++;
            });

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();
                $(document).find('.response-popup').remove();
                $form.find('button[type=submit]').addClass('disabled').attr('disabled', true);

                var params = {
                    ticket: ticketId,
                    checkins: $modal.find('.checkin-row').toArray().map(function (item) {
                        id = $(item).find('input[name=id]').val();
                        fromDate = moment($(item).find('input[name=checkin_from_date]').val(), 'DD.MM.YYYY').format('YYYY-MM-DD');
                        fromTime = $(item).find('input[name=checkin_from_time]').val();
                        fromDateTime = moment(fromDate + " " + fromTime).format('YYYY-MM-DD HH:mm:ss');

                        tillDate = moment($(item).find('input[name=checkin_till_date]').val(), 'DD.MM.YYYY').format('YYYY-MM-DD');
                        tillTime = $(item).find('input[name=checkin_till_time]').val();
                        tillDateTime = moment(tillDate + " " + tillTime).format('YYYY-MM-DD HH:mm:ss');

                        return {
                            id: id,
                            from: fromDateTime,
                            till: tillDateTime,
                        };
                    })
                };

                var updateRoute = '{{ route('admin::tickets.checkins.update', ['ticketId' => '__TICKET_ID__']) }}'
                    .replace(/__TICKET_ID__/, $button.data('ticket-id'));

                $.patchJSON(updateRoute, params, function (res) {

                    $form.find('button[type=submit]').removeClass('disabled').attr('disabled', false);
                    $modal.modal('hide');
                    var successBlock = ''
                        + '<div class="alert alert-success alert-dismissible response-popup" role="alert">'
                        + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                        + '        <span aria-hidden="true">&times;</span>'
                        + '    </button>'
                        + '    Angelzeit wurde erfolgreich eingetragen'
                        + '</div>';
                    $('.main-content').prepend(successBlock);
                    table.ajax.reload();
                }, function (error) {
                    console.error(error);

                    $form.find('button[type=submit]').removeClass('disabled').attr('disabled', false);

                    var errorBlock = ''
                        + '<div class="alert alert-danger alert-dismissible response-popup" role="alert">'
                        + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                        + '        <span aria-hidden="true">&times;</span>'
                        + '    </button>'
                        + '    Es ist ein Fehler aufgetreten'
                        + '</div>';
                    $modal.find('.modal-body').prepend(errorBlock);
                });
            });
        });

        function addCheckinEventListener($modal, blocksIndex, ticketId) {
            $modal.find('.checkin-row').off('change');
            $modal.find('.checkin-row').off('click');

            $modal.find('#date' + blocksIndex).off('click').off('change').datepicker();

            $modal.find('.checkin-row').on('click', 'i.fa-trash', removeCheckinEntranceRow(ticketId));
        }

        function removeCheckinEntranceRow(ticketId) {
            return function (e) {
                e.preventDefault();

                if ($(document).find('.checkin-row').length > 0) {
                    var $row = $(this).closest('.checkin-row');
                    var checkinId = $($row).find('input[name=id]').val();

                    if (checkinId > 0) {
                        var deleteRoute = '{{ route('admin::tickets.checkins.delete', ['ticketId' => '__TICKET_ID__', 'checkinId' => '__CHECKIN_ID__' ] ) }}'
                            .replace(/__CHECKIN_ID__/, checkinId).replace(/__TICKET_ID__/, ticketId);

                        $.deleteJSON(deleteRoute, function () {
                            table.ajax.reload();
                        }, function (error) {
                            console.error(error);
                        });
                    }

                    $row.next('hr').remove();
                    $row.remove();
                }
            }
        }

        function getNewCheckinBlock(index, checkinFromDate, checkinFromTime, checkinTillDate, checkinTillTime, id) {
            return '<div class="row checkin-row" data-index="' + index + '">'
                + '         <div class="invisible">'
                + '             <input  type="hidden"'
                + '                 class="invisible"'
                + '                 name="id"'
                + '                 value="' + id + '"'
                + '                 required/>'
                + '         </div>'
                + '         <div class="col-sm-5 checkin">'
                + '         <h5 class="center">Geangelt von</h5>'
                + '         <div class="col-xs-6">'
                + '             <input  type="text"'
                + '                 class="form-control"'
                + '                 name="checkin_from_date"'
                + '                 id="date' + index + '"'
                + '                 value="' + (checkinFromDate || moment().format('DD.MM.YYYY')
                ) + '"'
                + '                 required>'
                + '         </div>'
                + '     <div class="col-xs-6">'
                + '         <input  type="time"'
                + '                 class="form-control"'
                + '                 name="checkin_from_time"'
                + '                 id="time' + index + '"'
                + '                 value="' + (checkinFromTime || moment().format('HH:mm')
                ) + '"'
                + '                 required>'
                + '     </div>'
                + '         </div>'
                + '         <div class="col-sm-5 checkin">'
                + '         <h5 class="center">Geangelt bis</h5>'
                + '     <div class="col-xs-6">'
                + '         <input  type="text"'
                + '                 class="form-control"'
                + '                 name="checkin_till_date"'
                + '                 id="date' + index + '"'
                + '                 value="' + (checkinTillDate || moment().format('DD.MM.YYYY')
                ) + '">'
                + '     </div>'
                + '     <div class="col-xs-6">'
                + '         <input  type="time"'
                + '                 class="form-control"'
                + '                 name="checkin_till_time"'
                + '                 id="time' + index + '"'
                + '                 value="' + (checkinTillTime || moment().format('HH:mm')
                ) + '">'
                + '     </div>'
                + '     </div>'
                + '     <div class="col-xs-1 actions">'
                + '         <i class="fa fa-trash fa-fw text-danger"></i>'
                + '     </div>'
                + ' </div>'
                + ' <hr>';

        }

        function initTooltip () {
            $('[data-toggle="tooltip"]').tooltip()
        }

        $.postJSON = function (url, data, callback, error) {
            return jQuery.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: 'json',
                success: callback,
                error: error
            });
        };

        $.patchJSON = function (url, data, callback, error) {
            return jQuery.ajax({
                type: 'PATCH',
                url: url,
                data: data,
                dataType: 'json',
                success: callback,
                error: error
            });
        };

        $.deleteJSON = function (url, data, callback, error) {
            return jQuery.ajax({
                type: 'DELETE',
                url: url,
                data: data,
                dataType: 'json',
                success: callback,
                error: error
            });
        };
    });
</script>
@endpush

@push('styles')
<style>
    .jumbotron {
        padding-top: 15px;
        padding-bottom: 20px;
    }

    .jumbotron-custom {
        padding-bottom: 60px;
    }

    label[for=user_id] {
        margin-top: 7px;
    }

    .duration {
        font-size: 1em;
    }

    .checkin-row {
        padding-top: 5px;
        padding-bottom: 5px;

    }

    .checkin {
        border-radius: 5px;
        padding: 5px;
        background-color: #f4f4f4;
        margin: 5px;
        text-align: center;
    }

    .multi-haul-row {
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .multi-haul-row div.checkbox {
        margin-top: 6px;
        margin-bottom: 6px;
    }

    .multi-haul-row div.checkbox label {
        font-size: 1.2em;
    }

    .multi-haul-row div.checkbox input {
        transform: scale(1.5);
    }

    .multi-haul-row div.actions {
        font-size: 1.8em;
    }

    .multi-haul-row ~ hr,
    .form-group ~ hr {
        clear: both;
        margin-top: 15px;
        margin-bottom: 15px;
        padding-top: 5px;
        padding-bottom: 5px;
        border-top: 1px solid #03A9F4;
    }

    .form-group + hr.user-bottom {
        border: none;
    }

    .form-row {
        margin-top: 15px;
    }

    i.fa-trash,
    i.fa-plus {
        font-size: 2em;
        cursor: pointer;
    }

    .hauls-result {
        margin-top: 25px;
    }

    .btn-group > label.btn {
        background-color: #ccc;
        color: #999;
    }

    .btn-group > label.active {
        background-color: #1ebbcf;
        color: #fff;
        font-weight: bold;
    }

    .error {
        border: 1px solid #ed2f43;
    }

    .haul-card {
        font-size: 1.28em;
    }

    .haul-card-edit-btn {
        margin-bottom: 10px;
    }
    .required {
        border: 1px solid #FF614E !important;
    }
    .error-message {
        float: left;
        color: #FF614E;
        transition: opacity 1s ease-out;
    }
</style>
        @endpush
