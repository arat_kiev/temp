@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Stornierte Produkte
                <small>Gesamt: {{ $count }} ({{ $total }})</small>
                <div class="pull-right">
                    <a href="{{ route('admin::sales.index') }}" class="btn btn-info">
                        Verkaufte Angelkarten
                    </a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="reseller">Verkäufer</label>
                        <select class="form-control" name="reseller" id="filter-reseller">
                            @foreach($resellerList as $resellerId => $resellerName)
                                <option
                                        value="{{ $resellerId }}"
                                        @if($resellerId == $reseller)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $resellerName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="area">Gewässer</label>
                        <select class="form-control" name="area" id="filter-area">
                            @foreach($areaList as $areaId => $areaName)
                                <option
                                        value="{{ $areaId }}"
                                        @if($areaId == $area)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $areaName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group input-daterange">
                        <div class="input-group-addon input-group-label">Stornodatum</div>
                        <input type="text"
                               class="form-control"
                               id="date-range-from"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $from)->format('d.m.Y') }}">
                        <span class="input-group-addon">bis</span>
                        <input type="text"
                               class="form-control"
                               id="date-range-till"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $till)->format('d.m.Y') }}">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="button" id="filter-date-range-btn">Filter</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-sales">
                    <thead>
                    <tr>
                        <th></th>
                        <th>#</th>
                        <th>Name</th>
                        <th>Gewässer</th>
                        <th>Typ</th>
                        <th>Storno Datum</th>
                        <th>Verkäufer</th>
                        @if($children)<th>Bewirtschafter</th>@endif
                        <th>Preis</th>
                        <th>Storno-Beleg</th>
                        <th>Original Angelkarte</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {{-- preview modal --}}
    <div class="modal fade in" id="preview-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                    <h4 class="modal-title">
                        <span class="fa fa-eye fa-fw"></span>
                        <span id="preview-title">Vorschau wird geladen (bitte etwas Geduld)</span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="col-lg-12">
                            <img class="preview-image img-responsive" width="1083px" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
<style>
    .input-daterange {
        margin-bottom: 10px;
    }
</style>
@endpush

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('.input-daterange').datepicker();

        function updateFilters() {
            var from = $('#date-range-from').datepicker('getDate');
            var till = $('#date-range-till').datepicker('getDate');

            var fromString = moment(from).format('YYYY-MM-DD');
            var tillString = moment(till).format('YYYY-MM-DD');

            var reseller = $('#filter-reseller').val();
            var area = $('#filter-area').val();

            var location = '{{ route('admin::sales.storno.index') }}';
            var params = [
                'from=' + fromString,
                'till=' + tillString
            ];

            if (reseller >= 0) {
                params.push('reseller=' + reseller);
            }
            if (area > 0) {
                params.push('area=' + area);
            }
            table.state.clear();

            window.location = location + '?' + params.join('&');
        }

        $('#filter-date-range-btn').on('click', updateFilters);
        $('#date-range-from').on('change', updateFilters);
        $('#date-range-till').on('change', updateFilters);
        $('#filter-reseller').on('change', updateFilters);
        $('#filter-area').on('change', updateFilters);

        var table = $('#datatable-sales').DataTable({
            bFilter: true,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('admin::sales.storno.data') }}' + window.location.search
            },
            pagingType: "full_numbers",
            columns: [
                {
                    className: 'details-control',
                    orderable: false,
                    searchable: false ,
                    data: null,
                    defaultContent: ''
                },
                { data: 'manager_ticket_id', orderable: true },
                { data: 'user_full_name', orderable: false },
                { data: 'type.area.name', orderable: false, searchable: false },
                { data: 'type.name', orderable: false, searchable: false },
                { data: 'storno_date', searchable: false },
                { data: 'reseller', orderable: false, searchable: false },
                    @if($children){ data: 'manager', orderable: false, searchable: false },@endif
                { data: 'price_compact', orderable: false, searchable: false },
                { data: 'actions', orderable: false, searchable: false },
                { data: 'original_actions', orderable: false, searchable: false }
            ],
            order: [[1, 'desc']],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        function format(d) {
            return d.reseller_ticket ? formatDetail(d.reseller_ticket, d) : formatDetail(d.user, d);
        }

        function formatDetail(user, ticket) {
            return '' +
                    '<table class="ticket-user-detail">' +
                    '   <tr>' +
                    '       <th>Vorname:</th>' +
                    '       <td>' + user.first_name + '</td>' +
                    '       <th>BA-Ticket-Nr:</th>' +
                    '       <td>' + ticket.ticket_number + '</td>' +
                    '   </tr>' +
                    '   <tr>' +
                    '       <th>Nachname:</th>' +
                    '       <td>' + user.last_name + '</td>' +
                    '       <th>Ticket-Typ</th>' +
                    '       <td>' + ticket.type.name + '</td>' +
                    '   </tr>' +
                    '   <tr>' +
                    '       <th>Geburtstag:</th>' +
                    '       <td>' + moment(new Date(user.birthday)).format("DD.MM.YYYY") + '</td>' +
                    '       <th>Preis-Typ:</th>' +
                    '       <td>' + ticket.price.name + '</td>' +
                    '   </tr>' +
                    '   <tr>' +
                    '       <th>Straße:</th>' +
                    '       <td>' + user.street + '</td>' +
                    '       <th>Brutto-Preis:</th>' +
                    '       <td>' + ticket.price_brutto + '</td>' +
                    '   </tr>' +
                    '   <tr>' +
                    '       <th>Ort:</th>' +
                    '       <td>' + user.city + '</td>' +
                    '       <th>Gültig von:</th>' +
                    '       <td>' + ticket.valid_from + '</td>' +
                    '   </tr>' +
                    '   <tr>' +
                    '       <th>PLZ:</th>' +
                    '       <td>' + user.post_code + '</td>' +
                    '       <th>Storno Datum:</th>' +
                    '       <td>' + ticket.storno_date + '</td>' +
                    '   </tr>' +
                    '   <tr>' +
                    '       <th>Land:</th>' +
                    '       <td>' + user.country.name + '</td>' +
                    '       <th></th>' +
                    '       <td></td>' +
                    '   </tr>' +
                    '</table>';
        }

        $('#datatable-sales tbody').on('click', 'td.details-control', function() {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            } else {
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });

        $('[data-toggle="tooltip"]').tooltip();

        $('#preview-modal').on('show.bs.modal', function(e) {
            var modal = $(this);
            var button = $(e.relatedTarget);
            var ticketId = button.data('ticketId');
            var format = 'image';

            var title = modal.find('#preview-title');
            title.html('Vorschau wird geladen <span class="fa fa-cog fa-spin"></span>');

            modal.find('.preview-image').attr('src', '{{ URL::to('/preview/ticket/') }}/' + ticketId + '/' + format)
                    .on('load', function() {
                        title.html('Vorschau für Ticket: ' + ticketId);
                    });
        });
    });
</script>
@endpush