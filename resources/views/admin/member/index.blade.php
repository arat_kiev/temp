@extends('admin.layout.organization')

@section('page-content')
    @if($runningImports->isNotEmpty())
        <div class="row">
            <div class="col-lg-12 alert alert-info">
                <h4>Daten werden geladen ...</h4>
                <ul>
                    @foreach($runningImports as $import)
                        <li>
                            {{ $import->csv_filename }}
                            ({{ $import->created_at->format('d.m.Y - H:i') }}) -
                            {{ is_null($import->progress) ? 'wartend' : $import->progress . '%' }}
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Mitglieder
                <div class="pull-right">
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#importMembersModal">
                        <i class="fa fa-download"></i> Mitglieder importieren
                    </button>
                    @if(isset($current_organization))
                        <a href="{{ route('admin::members.create') }}" class="btn btn-success">
                            <span class="fa fa-plus fa-fw"></span> Mitglied anlegen
                        </a>
                    @endif
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap"
                   id="datatable-members">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Nachname</th>
                    <th>Fischer ID</th>
                    <th>E-Mail</th>
                    <th><span class="fa fa-link fa-fw"></span></th>
                    <th>Funktion</th>
                    <th>Mitglieds-Nr.</th>
                    <th>Mitglied seit</th>
                    <th>Aktionen</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="importMembersModal" tabindex="-1" role="dialog"
         aria-labelledby="importMembersModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" id="csv-modal-content-block">
                <form action="{{ route('admin::csv-uploads.import') }}" id="upload-csv-form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel">Mitglieder importieren</h3>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row" id="csv-upload-form-block">
                                <div class="form-group">
                                    <label for="csvFile">CSV-Datei hochladen</label>
                                    <input type="file" name="csvFile" id="csvFile" accept="text/csv">
                                </div>
                                <div class="form-group">
                                    <label for="hasHeaders">
                                        <input id="hasHeaders" name="hasHeaders" type="checkbox" checked>
                                        Datei enthält Kopfzeile
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar"
                                         aria-valuenow="100" aria-valuemin="0"
                                         aria-valuemax="100" style="width: 100%">
                                        Lädt Daten, bitte warten ...
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="csv-result"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="csv-upload-submit">Hochladen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $('document').ready(function () {
            $('#datatable-members').DataTable({
                bFilter: true,
                stateSave: true,
                processing: true,
                serverSide: true,
                ajax: '{{ route('admin::members.data') }}',
                pagingType: "full_numbers",
                columns: [
                    {data: 'first_name'},
                    {data: 'last_name'},
                    {data: 'fisher_id', sortable: false, searchable: false},
                    {data: 'email'},
                    {data: 'connected', sortable: false, searchable: false},
                    {data: 'function'},
                    {data: 'member_id'},
                    {data: 'since'},
                    {data: 'member_edit', sortable: false, searchable: false}
                ],
                language: {
                    decimal: ",",
                    thousands: ".",
                    lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                    zeroRecords: "Keine Einträge vorhanden",
                    info: "Seite _PAGE_ von _PAGES_",
                    infoEmpty: "Keine Einträge vorhanden",
                    infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                    search: "Suche",
                    paginate: {
                        first: "&laquo;",
                        previous: "&lsaquo;",
                        next: "&rsaquo;",
                        last: "&raquo;"
                    }
                }
            });

            //upload form
            $('#upload-csv-form .progress').hide();
            $('#upload-csv-form').submit(function (e) {
                e.preventDefault();
                $('#csv-upload-form-block').hide();
                $('#upload-csv-form .progress').show();
                $('#csv-upload-submit').attr('disabled', true);

                var url = $(this).attr('action');
                var type = $(this).attr('method');
                var data = new FormData($(this)[0]);
                data.append('id', $(this).attr('id'));

                $.ajax({
                    url: url,
                    type: type,
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        $('#upload-csv-form').trigger('reset');
                        $('#upload-csv-form .progress').hide();
                        $('.csv-result').append(response.html);
                        $('.csv-result .alert').remove();
                        $('#csv-upload-submit').hide();
                    },
                    error: function (errorResponse) {
                        $('#upload-csv-form .progress').hide();
                        $('#upload-csv-form').trigger('reset');
                        $('#csv-upload-form-block').show();
                        var errors = $.parseJSON(errorResponse.responseText);

                        for (var error in errors) {
                            $.each(errors[error], function (index, message) {
                                $('.csv-result').append('<div class="alert alert-danger" role="alert">' + message + '</div>');
                            });
                        }
                    }
                });
            });

            $('#importMembersModal').on('hidden.bs.modal', function (e) {
                $('#upload-csv-form .progress').hide();
                $('#csv-upload-submit').show();
                $('#csv-upload-form-block').show();
                $('.csv-result').html('');
                $('#upload-csv-form').trigger('reset');
            });
        });
    </script>
@endpush
