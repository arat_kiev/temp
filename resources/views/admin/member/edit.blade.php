@extends('admin.layout.organization')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::members.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                @if($member->id)
                    Mitglied {{ $member->first_name }} {{ $member->last_name }} bearbeiten
                    @if($member->user)
                        <br/>
                        <small>Fisher ID - {{ $member->fisher_id ? $member->fisher_id : $member->user->fisher_id }}</small>
                    @endif
                    @if($member->email)
                        <br/>
                        <small>({{ $member->email }})</small>
                    @endif
                @else
                    Mitglied hinzufügen
                @endif
            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Persönliche Daten
                </div>
                <div class="panel-body">
                    {!! form_row($form->first_name) !!}
                    {!! form_row($form->last_name) !!}
                    {!! form_row($form->email) !!}
                    {!! form_row($form->gender) !!}
                    {!! form_row($form->birthday) !!}
                    {!! form_row($form->street) !!}
                    {!! form_row($form->area_code) !!}
                    {!! form_row($form->city) !!}
                    {!! form_row($form->country) !!}
                    {!! form_row($form->phone) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Vereins-Daten
                </div>
                <div class="panel-body">
                    {!! form_row($form->function) !!}
                    {!! form_row($form->member_id) !!}
                    {!! form_row($form->since) !!}
                    {!! form_row($form->leave_date) !!}
                    {!! form_row($form->comment) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
            {!! form_widget($form->submit_exit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#comment').summernote({
            lang: 'de-DE',
            height: '150px',
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['link', ['linkDialogShow', 'unlink']],
                ['misc', ['fullscreen', 'codeview', 'undo', 'redo', 'reset']]
            ],
            callbacks: {
                onImageUpload: function(files) {
                    return false;
                }
            }
        });

        $('#country').chosen({
            width: '100%',
            allow_single_deselect: true
        });
    });
</script>
@endpush