<h3>Vorschau (Gesamt: {{ $csvCount }})</h3>
<form class="form-horizontal" method="POST" action="{{ route('admin::csv-uploads.store') }}" id="">
    {{ csrf_field() }}
    <input type="hidden" name="csvDataFileId" value="{{ $csvDataFile->id }}">
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                @foreach ($csvData[0] as $key => $value)
                    <th>
                        <select name="fields[{{ $key }}]">
                            @foreach (config('csv-import.db_fields') as $dbField)
                                <option value="{{ (\Request::has('header')) ? $dbField : $loop->index }}"
                                    {{ $csvDataFile->csv_header && $key === $dbField ? 'selected' : '' }}
                                >{{ studly_case($dbField) }}</option>
                            @endforeach
                        </select>
                    </th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach ($csvData as $row)
                <tr>
                    @foreach ($row as $key => $value)
                        <td>{{ $value }}</td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div><br/>
    <button type="submit" class="btn btn-primary pull-right">
        Daten importieren
    </button>
</form>
