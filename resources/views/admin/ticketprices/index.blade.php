@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::areas.tickettypes.index', ['areaId' => $area->id]) }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Preise <small>{{ $ticketType->name }}</small>
                <div class="pull-right">
                    <a href="{{
                        route('admin::ticketprices.create', [
                            'areaId' => $area->id,
                            'ticketTypeId' => $ticketType->id
                        ])
                        }}" class="btn btn-success">
                        <span class="fa fa-plus fa-fw"></span>
                        Preis anlegen</a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-tickettypes">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Preis</th>
                    <th>Kaufbar ab</th>
                    <th>Gültig von</th>
                    <th>Gültig bis</th>
                    <th>Zuletzt bearbeitet</th>
                    <th class="sorting_disabled"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($ticketType->prices as $price)
                    <tr>
                        <td>
                            {!! Html::linkRoute('admin::ticketprices.edit', $price->nameWithYear, [
                                    'areaId'        => $area->id,
                                    'ticketTypeId'  => $ticketType->id,
                                    'ticketPriceId' => $price->id
                                ]) !!}

                        </td>
                        <td>€ {{ number_format($price->value / 100.0, 2, ',', '.') }}</td>
                        <td>{{ $price->visible_from->format('d.m.Y') }}</td>
                        <td>{{ $price->valid_from->format('d.m.Y') }}</td>
                        <td>{{ $price->valid_to->format('d.m.Y') }}</td>
                        <td>{{ $price->updated_at->format('d.m.Y') }}</td>
                        <td>
                            <button type="button"
                                    class="btn btn-xs btn-default"
                                    data-toggle="modal"
                                    data-target="#preview-modal"
                                    data-price-id="{{ $price->id }}">
                                <span class="fa fa-eye fa-fw"></span>
                            </button>
                            @if ($price->type == "MEMBER")
                                <button type="button"
                                        class="btn btn-xs btn-primary"
                                        data-toggle="modal"
                                        data-target="#organization-modal"
                                        data-action="{{ route('admin::ticketprices.organizations.store', [
                                            'areaId'        => $area->id,
                                            'ticketTypeId'  => $ticketType->id,
                                            'ticketPriceId' => $price->id
                                        ]) }}">
                                    <span class="fa fa-group fa-fw"></span>
                                    <span class="badge">{{ $price->organizations()->count() }}</span>
                                </button>
                            @endif
                            @if (Auth::user()->hasRole('superadmin'))
                                <button type="button"
                                        class="btn btn-xs btn-danger @if($price->tickets()->count()) disabled @endif"
                                        data-target="#delete-modal"
                                        data-toggle="modal"
                                        data-action="{{ route('admin::super:ticketprices.delete', [
                                            'ticketPriceId' => $price->id
                                        ]) }}"
                                        @if($price->tickets()->count()) disabled @endif>
                                    Löschen<span class="fa fa-trash-o fa-fw"></span>
                                </button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{-- preview modal --}}
    <div class="modal fade in" id="preview-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <span class="fa fa-eye fa-fw"></span>
                        <span id="preview-title">Vorschau wird geladen (bitte etwas Geduld)</span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="col-lg-12">
                            <img class="preview-image img-responsive" width="1083px" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
                </div>
            </div>
        </div>
    </div>

    {{-- organization modal --}}
    <div class="modal fade in" id="organization-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            <span class="fa fa-group fa-fw"></span> Vereine
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select class="form-control" id="organizations" name="organizations[]" multiple="multiple">
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Schließen"><span>&times;</span></button>
                        <h4 class="modal-title">
                            Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Wirklich löschen?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Löschen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#datatable-tickettypes').DataTable({
            stateSave: true,
            pagingType: "full_numbers",
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#preview-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var priceId = button.data('priceId');
            var format = 'image';

            var modal = $(this);
            var title = modal.find('#preview-title');
            title.html('Vorschau wird geladen <span class="fa fa-cog fa-spin"></span>');

            modal.find('.preview-image')
                .attr('src', '{{ URL::to('/preview/demo/') }}/' + priceId + '/' + format)
                .on('load', function () {
                    title.html('Vorschau für Ticket Preis: ' + priceId);
                });
        });

        $('#organization-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var formAction = button.data('action');

            var modal = $(this);
            modal.find('.form').attr('action', formAction);

            var orgSelect = modal.find('#organizations');
            var orgData = [];

            orgSelect.children().remove();

            $.getJSON(formAction).done(function (data) {
                $.each(data.data, function (index, organization) {
                    orgSelect.append($('<option/>', {
                        value: organization.id,
                        text: organization.name,
                        selected: true
                    }));
                });
            }).done(function () {
                orgSelect.select2({
                    width: '100%',
                    data: orgData,
                    ajax: {
                        url: '{{ route('admin::organizations.index') }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term,
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.data,
                                pagination: {
                                    more: params.page < data.meta.pagination.total_pages
                                }
                            };
                        },
                        cache: true
                    },
                    minimumInputLength: 3,
                    templateResult: function (organization) {
                        if (organization.loading) return organization.name;

                        var image = organization.logo
                                ? organization.logo.files.small
                                : '{{ route('imagecache', ['template' => 'lLogo', 'ticket/hejfish_logo.png']) }}';

                        return ""
                            + "<div class='select2-result-organization'>"
                            + "     <img class='select2-result-organization__avatar' src='" + image + "' />"
                            + "     <div class='select2-result-organization__name'>"
                                        + organization.name
                            + "     </div>"
                            + "     <div class='select2-result-organization__address'>"
                                        + organization.street + ", "
                                        + organization.area_code + " "
                                        + organization.city
                                        + (organization.country ? " / " + organization.country : "")
                            + "     </div>"
                            + "</div>";
                    },
                    templateSelection: function (organization) {
                        return organization.text || organization.name;
                    },
                    escapeMarkup: function (markup) { return markup; }
                });
            });
        });

        $('#delete-modal').on('show.bs.modal', function(e) {
            var formAction = $(e.relatedTarget).data('action'),
                formMethod = 'DELETE',
                $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();
                $(document).find('.response-popup').remove();

                $.ajax({
                    type : formMethod,
                    url: formAction,
                    data: [],
                    success : function(data) {
                        $('#delete-modal').modal('hide');
                        window.location.reload();
                    },
                    error: function (data) {
                        var msgBox = ''
                            + '<div class="row response-popup" style="padding-top: 10px;">'
                            + '    <div class="col-lg-12">'
                            + '        <div class="alert alert-danger alert-dismissable">'
                            + '            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">'
                                            + "&times;"
                            + '            </button>'
                                            + data.responseText
                            + '        </div>'
                            + '    </div>'
                            + '</div>';
                        $('.page-header').parents('.row').before(msgBox);
                        $('#delete-modal').modal('hide');
                    }
                });
            });
        });
    });
</script>
@endpush

@push('styles')
<style>
    .select2-result-organization__avatar {
        height: 44px;
        float: left;
        margin-right: 10px;
    }

    .select2-result-organization__name {
        font-size: 18px;
    }
</style>
@endpush