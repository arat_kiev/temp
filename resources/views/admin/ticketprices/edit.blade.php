@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::ticketprices.index', ['areaId'=> $area->id, 'ticketTypeId' => $ticketType->id]) }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Preis
                @if($ticketPrice->id)
                    bearbeiten <small>{{ $ticketPrice->nameWithYear }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="{{ $form->group_count ? 'col-sm-8' : 'col-sm-12' }}">
                            {!! form_row($form->name) !!}
                        </div>
                        @if($form->group_count)
                            <div class="col-sm-4">
                                {!! form_row($form->group_count) !!}
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-sm-8" id="type-container">
                            {!! form_row($form->type) !!}
                        </div>
                        <div class="col-sm-4 hidden" id="age-container">
                            {!! form_row($form->age) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! form_row($form->value) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="help-block">Wenn USt-ID beim Bewirtschafter hinterlegt ist, wird die USt auf der Angelkarte ausgewiesen.</p>
                        </div>
                    </div>
                    {!! form_row($form->show_vat) !!}
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Zeitraum
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-warning">
                                <strong>Achtung!</strong>
                                Ab dem "Kaufbar"-Datum kann dieser Preis in € nicht mehr bearbeitet werden.
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            {!! form_row($form->visible_from) !!}
                        </div>
                        <div class="col-md-4 col-sm-6">
                            {!! form_row($form->valid_from) !!}
                        </div>
                        <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-6">
                            {!! form_row($form->valid_to) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Beschränkungen
                </div>
                <div class="panel-body">
                    {!! form_row($form->weekdays) !!}
                    <div class="form-group">
                        <label>Sperr-Zeiträume</label>
                        <div class="lock-periods-container" data-prototype="{{ form_row($form->lock_periods->prototype()) }}">
                            {!! form_row($form->lock_periods) !!}
                        </div>
                        <button type="button" class="btn btn-primary" id="lock-periods-btn-add">
                            <span class="fa fa-plus fa-fw"></span> Zeitraum hinzufügen
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#weekdays').chosen({
            'width': '100%'
        });

        function onLockPeriodRemoveClicked(e) {
            e.preventDefault();
            $(this).closest('.row').remove();
        }

        $('.lock-periods-btn-remove').on('click', onLockPeriodRemoveClicked);

        $('#lock-periods-btn-add').on('click', function(e) {
            e.preventDefault();
            var container = $('.lock-periods-container');
            var count = container.children().length;
            var proto = container.data('prototype').replace(/__NAME__/g, count);
            container.append(proto);

            $('.lock-periods-btn-remove').on('click', onLockPeriodRemoveClicked);
        });

        $('#type').on('change', function(e) {
            var ageInput = $('#age');
            var typeSelect = $(e.target);
            var typeContainer = $('#type-container');
            var ageContainer = $('#age-container');

            if (typeSelect.val() == 'JUNIOR' || typeSelect.val() == 'SENIOR') {
                typeContainer.removeClass('col-sm-8').addClass('col-sm-4');
                ageContainer.removeClass('hidden');
            } else {
                typeContainer.removeClass('col-sm-4').addClass('col-sm-8');
                ageContainer.addClass('hidden');
                ageInput.val(undefined);
            }
        }).trigger('change');
    });
</script>
@endpush