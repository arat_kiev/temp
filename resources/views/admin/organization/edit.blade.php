@extends('admin.layout.organization')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Verein
                @if($organization->id)
                    bearbeiten<br/><small>{{ $organization->name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->name) !!}
                    {!! form_row($form->street) !!}
                    {!! form_row($form->area_code) !!}
                    {!! form_row($form->city) !!}
                    {!! form_row($form->country) !!}
                    {!! form_row($form->person) !!}
                    {!! form_row($form->phone) !!}
                    {!! form_row($form->email) !!}
                    {!! form_row($form->website) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Logo
                </div>
                <div class="panel-body">
                    @if($organization->logo)
                        <img src="{{ route('imagecache', ['template' => 'lLogo', 'filename' => $organization->logo->file]) }}" class="thumbnail img-responsive" />
                    @endif
                    {!! form_row($form->logo) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#country').chosen();
    });
</script>
@endpush