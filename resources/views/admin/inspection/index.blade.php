@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::inspector.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Kontrollen von {{ $inspectorName }}
                <small>Gesamt: {{ $count }}</small>
                <div class="pull-right">
                    <div class="dropdown" style="display: inline-block">
                        <button type="button" class="btn btn-default" data-toggle="dropdown">
                            <span class="fa fa-download fa-fw"></span> Export
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="{{ route('admin::inspection.export', [
                                    'format'    => 'xls',
                                    'from'      => $from,
                                    'till'      => $till,
                                    'area'      => $area,
                                    'inspectorId'   => $inspectorId,
                                ]) }}">
                                    <span class="fa fa-file-excel-o fa-fw"></span> XLS-Datei
                                </a>
                            </li>
                            {{--<li>--}}
                                {{--<a href="{{ route('admin::inspection.export', [--}}
                                    {{--'format'    => 'csv',--}}
                                    {{--'from'      => $from,--}}
                                    {{--'till'      => $till,--}}
                                    {{--'area'      => $area,--}}
                                    {{--'inspectorId'   => $inspectorId,--}}
                                {{--]) }}">--}}
                                    {{--<span class="fa fa-file-text-o fa-fw"></span> CSV-Datei--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        </ul>
                    </div>
                    <br>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="area">Gewässer</label>
                        <select class="form-control" name="area" id="filter-area">
                            @foreach($areaList as $areaId => $areaName)
                                <option
                                        value="{{ $areaId }}"
                                        @if($areaId == $area)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $areaName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group input-daterange">
                        <div class="input-group-addon input-group-label">Kontroll-datum</div>
                        <input type="text"
                               class="form-control"
                               id="date-range-from"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $from)->format('d.m.Y') }}">
                        <span class="input-group-addon">bis</span>
                        <input type="text"
                               class="form-control"
                               id="date-range-till"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $till)->format('d.m.Y') }}">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="button" id="filter-date-range-btn">Filter</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer" id="datatable-inspections">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Angler/in</th>
                        <th>Angelkarte</th>
                        <th>Status</th>
                        <th>Kommentar</th>
                        <th>Kontrolliert am</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <hr style="clear: both">

    <div id="map"></div>


    {{-- preview modal --}}
    <div class="modal fade in" id="preview-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                    <h4 class="modal-title">
                        <span class="fa fa-eye fa-fw"></span>
                        <span id="preview-title">Vorschau wird geladen (bitte etwas Geduld)</span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="col-lg-12">
                            <img class="preview-image img-responsive" width="1083px" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
<style>
    .input-daterange {
        margin-bottom: 10px;
    }

    @media(min-width: 992px) {
        .input-daterange {
            margin-bottom: -30px;
        }
    }

    #map {
        height: 100%;
        min-height: 500px;
    }
</style>
@endpush

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('.input-daterange').datepicker();

        function updateFilters() {
            var from = $('#date-range-from').datepicker('getDate'),
                till = $('#date-range-till').datepicker('getDate'),
                fromString = moment(from).format('YYYY-MM-DD'),
                tillString = moment(till).format('YYYY-MM-DD'),
                area = $('#filter-area').val(),
                location = '{{ route('admin::inspection.index', ['inspectorId' => $inspectorId]) }}',
                params = [
                    'from=' + fromString,
                    'till=' + tillString
                ];

            if (area > 0) {
                params.push('area=' + area);
            }
            $table.state.clear();

            window.location = location + '?' + params.join('&');
        }

        $('#filter-date-range-btn').on('click', updateFilters);
        $('#date-range-from').on('change', updateFilters);
        $('#date-range-till').on('change', updateFilters);
        $('#filter-area').on('change', updateFilters);

        var $table = $('#datatable-inspections').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            searching: false,
            ajax: {
                url: '{{ route('admin::inspection.data', ['inspectorId' => $inspectorId]) }}' + window.location.search
            },
            pagingType: "full_numbers",
            columns: [
                { data: 'id' , width: '5%'},
                { data: 'fisher'},
                { data: 'ticket'},
                { data: 'status', width: '5%' },
                { data: 'comment' },
                { data: 'created_at', width: '20%' }
            ],
            columnDefs: [{
                targets : [0, 1, 2, 3, 4],
                orderable: false
            }],
            aaSortingFixed: [[ 4, "desc" ]],
            fnDrawCallback: function (oSettings) {
                console.log('DataTables has redrawn the table');

                var data = oSettings.json.data.map(function (item) {
                    return item.location;
                }).filter(function (item) {
                    return item.latitude && item.longitude;
                }).map(function (item) {
                    return {lat: item.latitude, lng: item.longitude};
                });

                drawMap(data);
            },
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            },
            createdRow: function(row, data) {
                if (data['status'].indexOf("check") >= 0) {
                    $(row).addClass('success');
                } else if ((data['status'].indexOf("times") >= 0)) {
                    $(row).addClass('danger');
                }
            }
        });

        function drawMap(locations) {
            // console.dir(locations);
            var center = locations.length ? locations[0] : {lat: 48.300106, lng: 14.282670};

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 7,
                center: center
            });

            // Add some markers to the map.
            // Note: The code uses the JavaScript Array.prototype.map() method to
            // create an array of markers based on a given "locations" array.
            // The map() method here has nothing to do with the Google Maps API.
            var markers = locations.map(function(location, i) {
                return new google.maps.Marker({
                    position: location
                });
            });

            // Add a marker clusterer to manage the markers.
            var markerCluster = new MarkerClusterer(map, markers, {
                imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
            });
        }

        $('#preview-modal').on('show.bs.modal', function(e) {
            var $modal = $(this);
            var $button = $(e.relatedTarget);
            var ticketId = $button.data('ticket-id');
            var format = 'image';

            var title = $modal.find('#preview-title');
            title.html('Vorschau wird geladen <span class="fa fa-cog fa-spin"></span>');

            $modal.find('.preview-image')
                .attr('src', '{{ URL::to('/preview/ticket/') }}/' + ticketId + '/' + format)
                .on('load', function() {
                    title.html('Vorschau für Ticket: ' + ticketId);
                });
        });
    });
</script>

<script nonce="{{ csp_nounce() }}" src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script nonce="{{ csp_nounce() }}" async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('services.gmaps.api_token') }}"></script>
@endpush