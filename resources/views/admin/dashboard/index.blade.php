@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Übersicht</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-map-marker fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $areasCount }}</div>
                            <div>Gewässer</div>
                        </div>
                    </div>
                </div>
                <a href="{{ route('admin::areas.index') }}">
                    <div class="panel-footer">
                        <span class="pull-left">Details anzeigen</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $haulsCount }}</div>
                            <div>Fänge {{ Carbon\Carbon::now()->year }}</div>
                        </div>
                    </div>
                </div>
                <a href="{{ route('admin::hauls.index') }}">
                    <div class="panel-footer">
                        <span class="pull-left">Details anzeigen</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $salesCount }}</div>
                            <div>Verkäufe {{ Carbon\Carbon::now()->year }}</div>
                        </div>
                    </div>
                </div>
                <a href="{{ route('admin::sales.index') }}">
                    <div class="panel-footer">
                        <span class="pull-left">Details anzeigen</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            {{-- Area-charts --}}
            <div class="panel panel-default">
                <div class="panel-heading">Kartenverkäufe (letzte 7 Tage)</div>
                <div class="panel-body text-center" id="tickets">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Kartenverkäufe pro Gewässer (letzte 7 Tage)</div>
                <div class="panel-body text-center" id="area-tickets">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            {{-- Bar charts --}}
            <div class="panel panel-default">
                <div class="panel-heading">Kartenverkäufe pro Monat - {{ Carbon\Carbon::now()->year }}</div>
                <div class="panel-body text-center" id="ticket-per-month">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
            {{-- Donut charts --}}
            <div class="panel panel-default">
                <div class="panel-heading">Fänge pro Fischart - {{ Carbon\Carbon::now()->year }}</div>
                <div class="panel-body text-center" id="catches-fishtypes">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $(function () {
        $.getJSON('{{ route('admin::dashboard.tickets.area') }}')
            .done(function (data) {
                $('#tickets').empty().height('250px');
                $('#area-tickets').empty().height('250px');

                Morris.Area({
                    element: 'tickets',
                    data: data.total,
                    xkey: 'day',
                    ykeys: ['quantity'],
                    labels: ['Angelkarten']
                });

                if (data.areas.length) {
                    Morris.Line({
                        element: 'area-tickets',
                        data: data.perArea,
                        xkey: 'day',
                        ykeys: data.areas.map(function (area) {
                            return area.id;
                        }),
                        labels: data.areas.map(function (area) {
                            return area.name;
                        })
                    });
                } else {
                    $('#area-tickets').html('<p class="text-error">Zur Zeit keine Daten verfügbar</p>');
                }
            })
            .fail(function () {
                $('#tickets').html('<p class="text-error">Zur Zeit keine Daten verfügbar</p>');
                $('#area-tickets').html('<p class="text-error">Zur Zeit keine Daten verfügbar</p>');
            });
    });

    $.getJSON('{{ route('admin::dashboard.tickets.month') }}')
        .then(function (data) {
            $('#ticket-per-month').empty().height('250px');

            Morris.Bar({
                element: 'ticket-per-month',
                data: data,
                xkey: 'month',
                ykeys: ['quantity'],
                labels: ['Angelkarten']
            });
        })
        .fail(function () {
            $('#ticket-per-month').html('<p class="text-error">Zur Zeit keine Daten verfügbar</p>');
        });

    $.getJSON('{{ route('admin::dashboard.catches.fishtype') }}')
        .then(function (data) {
            $('#catches-fishtypes').empty().height('250px');

            if (data.length) {
                Morris.Donut({
                    element: 'catches-fishtypes',
                    data: data
                });
            } else {
                $('#catches-fishtypes').html('<p class="text-error">Zur Zeit keine Daten verfügbar</p>');
            }
        })
        .fail(function () {
            $('#catches-fishtypes').html('<p class="text-error">Zur Zeit keine Daten verfügbar</p>');
        });
</script>
@endpush

@push('styles')
<style>
    .text-error {
        color: #b94a48;
    }

    a.text-error:hover,
    a.text-error:focus {
        color: #953b39;
    }
</style>
@endpush
