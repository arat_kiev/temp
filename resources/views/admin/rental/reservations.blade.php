<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Online-Buchungen</h4>
            </div>
            @if(count($sales))
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Kunde</th>
                        <th>Gebucht am/um</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sales as $sale)
                        <tr>
                            <td>{{ $sale->first_name }} {{ $sale->last_name }}</td>
                            <td>{{ $sale->created_at->format('d.m.Y - H:i') }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="panel-body">
                    <p>Keine Daten vorhanden.</p>
                </div>
            @endif
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Manuelle Reservierungen</h4>
            </div>
            @if(count($reservations))
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Notiz</th>
                            <th>Gebucht am/um</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reservations as $reservation)
                            <tr>
                                <td>{!! nl2br(e($reservation->notice)) !!}</td>
                                <td>{{ $reservation->created_at->format('d.m.Y - H:i') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="panel-body">
                    <p>Keine Daten vorhanden.</p>
                </div>
            @endif
        </div>
    </div>
</div>