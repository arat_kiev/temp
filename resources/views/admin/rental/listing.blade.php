@extends('admin.layout.rental')

@section('full-page')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Reservierungen
                    <button class="btn btn-default pull-right" id="btn-print" onclick="window.print()">
                        <span class="fa fa-print fa-fw"></span>
                        Drucken
                    </button>
                </h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <table class="rental-table table table-bordered table-striped table-responsive">
                    <thead>
                    <tr>
                        <th>
                            <h4 class="pull-left">
                                Kalenderwoche {{ $firstDay->weekOfYear }}<br/><br/>
                                {{ $firstDay->format('d.m.Y') }} bis
                                {{ $firstDay->copy()->endOfWeek()->format('d.m.Y') }}
                            </h4>
                            <span class="pull-right calendar-action">
                                    <a class="btn btn-default disabled" href="{{ url()->full() }}">
                                        <p>Aktualisieren</p>
                                        <span class="fa fa-fw fa-lg fa-refresh"></span>
                                    </a>
                                    <a class="btn btn-default disabled" href="{{ route('admin::rental.index', ['date' => $firstDay->copy()->subWeek()->format('Y-m-d'), 'view' => 'listing']) }}">
                                        <p>KW{{ $firstDay->copy()->subWeek()->format('W/y') }}</p>
                                        <span class="fa fa-fw fa-lg fa-arrow-left"></span>
                                    </a>
                                    <a class="btn btn-default disabled" id="btn-select-week" href="#">
                                        <p>Woche wählen</p>
                                        <span class="fa fa-fw fa-lg fa-calendar"></span>
                                        <input class="datepicker-week" />
                                    </a>
                                    <a class="btn btn-default disabled" href="{{ route('admin::rental.index', ['date' => $firstDay->copy()->addWeek()->format('Y-m-d'), 'view' => 'listing']) }}">
                                        <p>KW{{ $firstDay->copy()->addWeek()->format('W/y') }}</p>
                                        <span class="fa fa-fw fa-lg fa-arrow-right"></span>
                                    </a>
                                    <a class="btn btn-default disabled" href="{{ route('admin::rental.index', ['date' => $firstDay->format('Y-m-d')]) }}">
                                        <p>Bootsverleih</p>
                                        <span class="fa fa-fw fa-lg fa-table"></span>
                                    </a>
                                </span>
                        </th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                @for($date = $firstDay->copy(); $date->diffInDays($firstDay) < 7; $date->addDay())
                    <h3 class="page-header">{{ $date->formatLocalized('%A, %e. %B %Y') }}</h3>
                    @foreach($products as $product)
                        @php
                            $dateslots = $product->price->timeslotDates()
                                ->whereDate('date', $date)
                                ->orderBy('product_timeslots.from')
                                ->get()->keyBy('timeslot_id');

                            $slotHasSales = $product->price->timeslotDates()
                                ->whereDate('date', $date)
                                ->join('timeslot_sales', 'timeslot_sales.timeslot_date_id', '=', 'timeslot_dates.id')
                                ->count() > 0;

                            $slotHasReservations = $product->price->timeslotDates()
                                ->whereDate('date', $date)
                                ->join('rental_reservations', 'rental_reservations.timeslot_date_id', '=', 'timeslot_dates.id')
                                ->count() > 0;

                            $reservedSlots = $product->price->timeslotDates()
                                ->whereDate('date', $date)
                                ->join('rental_reservations', 'rental_reservations.timeslot_date_id', '=', 'timeslot_dates.id')
                                ->get()->keyBy('timeslot_id');

                        @endphp

                        @continue($product->price->timeslots()->count() && !$slotHasSales && !$slotHasReservations)
                        <p><strong>{{ $product->name }}
                                - {{ $product->areas()->first() ? $product->areas()->first()->name : '' }}:&nbsp;</strong>
                            @foreach($product->price->timeslots()->orderBy('from')->get() as $timeslot)
                                @if($reservedSlots->has($timeslot->id) && $reservedSlots[$timeslot->id]->reservations->count() > 0)
                                    @foreach($reservedSlots[$timeslot->id]->reservations as $reservation)
                                        {{ $reservation->notice }}
                                    @endforeach
                                @elseif($slotHasSales)
                                    @if($dateslots->has($timeslot->id) && $dateslots[$timeslot->id]->productSales->count() > 0)
                                        @foreach($dateslots[$timeslot->id]->productSales as $sale)
                                            {{ $sale->first_name }}&nbsp;
                                            {{ $sale->last_name }},&nbsp;
                                            {{ $sale->email }},&nbsp;
                                            Tel.: {{ $sale->user ? $sale->user->phone : '' }}
                                        @endforeach
                                    @endif
                                @endif
                            @endforeach
                        </p>
                    @endforeach
                @endfor
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <style>
        .datepicker-week {
            visibility: hidden;
            width: 0;
            height: 0;
            margin: 0;
            padding: 0;
        }

        @media print {
            .container {
                width: 100%;
            }

            .rental-table thead {
                height: 50px;
            }

            .calendar-action {
                display: none;
            }

            #btn-print {
                display: none;
            }
        }
    </style>
@endpush

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $('document').ready(function() {
            var calActions = $('.calendar-action > a');

            calActions.removeClass('disabled');
            calActions.on('click', function () {
                $('body').LoadingOverlay('show', {
                    image: '',
                    fontawesome: 'fa fa-spinner fa-pulse',
                    custom: '<h2 class="noselect">Daten werden geladen ...</h2>',
                    zIndex: 2000
                });
            });

            var dateSelect = $('.datepicker-week');
            dateSelect.datepicker({
                format: 'yyyy-mm-dd',
                calendarWeeks: true
            });
            dateSelect.datepicker().on('changeDate', function (ev) {
                var date = moment(ev.date);
                var route = '{{ route('admin::rental.index') }}?date=' + date.format('YYYY-MM-DD') + '&view=listing';

                $('.rental-table').LoadingOverlay('show', {
                    image: '',
                    fontawesome: 'fa fa-spinner fa-pulse',
                    custom: '<h2 class="noselect">Daten werden geladen ...</h2>',
                    zIndex: 2000
                });

                location.replace(route);
            });

            var weekSelectButton = $('#btn-select-week');
            weekSelectButton.off('click');
            weekSelectButton.on('click', function (ev) {
                ev.preventDefault();
                dateSelect.datepicker('show');
            });
        });
    </script>
@endpush