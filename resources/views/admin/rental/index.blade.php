@extends('admin.layout.rental')

@section('full-page')
    @include('admin.flash.message')

    <div class="rental-container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Bootsverleih
                </h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <table class="rental-table table table-bordered table-striped table-responsive">
                    <thead>
                        <tr>
                            <th colspan="8">
                                <h4 class="pull-left">
                                    Kalenderwoche {{ $firstDay->weekOfYear }}<br/><br/>
                                    {{ $firstDay->format('d.m.Y') }} bis
                                    {{ $firstDay->copy()->endOfWeek()->format('d.m.Y') }}
                                </h4>
                                <span class="pull-right calendar-action">
                                    <button class="btn btn-default" id="btn-reserve-slots" disabled="disabled">
                                        <p>Reservieren</p>
                                        <span class="fa fa-fw fa-lg fa-calendar-check-o"></span>
                                    </button>
                                    <a class="btn btn-default disabled" href="{{ url()->full() }}">
                                        <p>Aktualisieren</p>
                                        <span class="fa fa-fw fa-lg fa-refresh"></span>
                                    </a>
                                    <a class="btn btn-default disabled" href="{{ route('admin::rental.index', ['date' => $firstDay->copy()->subWeek()->format('Y-m-d')]) }}">
                                        <p>KW{{ $firstDay->copy()->subWeek()->format('W/y') }}</p>
                                        <span class="fa fa-fw fa-lg fa-arrow-left"></span>
                                    </a>
                                    <a class="btn btn-default disabled" id="btn-select-week" href="#">
                                        <p>Woche wählen</p>
                                        <span class="fa fa-fw fa-lg fa-calendar"></span>
                                        <input class="datepicker-week" />
                                    </a>
                                    <a class="btn btn-default disabled" href="{{ route('admin::rental.index', ['date' => $firstDay->copy()->addWeek()->format('Y-m-d')]) }}">
                                        <p>KW{{ $firstDay->copy()->addWeek()->format('W/y') }}</p>
                                        <span class="fa fa-fw fa-lg fa-arrow-right"></span>
                                    </a>
                                    <a class="btn btn-default disabled" href="{{ route('admin::rental.index', ['date' => $firstDay->format('Y-m-d'), 'view' => 'listing']) }}">
                                        <p>Reservierungen</p>
                                        <span class="fa fa-fw fa-lg fa-list-ul"></span>
                                    </a>
                                </span>
                            </th>
                        </tr>
                        <tr>
                            <th>Produkt</th>
                            @for($date = $firstDay->copy(); $date->diffInDays($firstDay) < 7; $date->addDay())
                                <th>
                                    {{ $date->formatLocalized('%a') }},<br/>
                                    {{ $date->formatLocalized('%e.%m.') }}
                                </th>
                            @endfor
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $product)
                            <tr>
                                <th>{{ $product->name }}<br /> {{ $product->areas()->first() ? $product->areas()->first()->name : '' }}</th>
                                @for($date = $firstDay->copy(); $date->diffInDays($firstDay) < 7; $date->addDay())
                                    <td>
                                        <table class="timeslots-table">
                                            @php
                                                $dateslots = $product->price->timeslotDates()
                                                    ->whereDate('date', $date)
                                                    ->orderBy('product_timeslots.from')
                                                    ->get()->keyBy('timeslot_id');
                                            @endphp
                                            @foreach($product->price->timeslots()->orderBy('from')->get() as $timeslot)
                                                <tr>
                                                    @if($dateslots->has($timeslot->id))
                                                        <td class="{{ $dateslots[$timeslot->id]->available > 0 ? 'available' : 'unavailable' }}" style="width:100%;">
                                                            <a href="#" class="link-to-reservations" data-dateslot-id="{{ $dateslots[$timeslot->id]->id }}">
                                                                {{ $dateslots[$timeslot->id]->timeslot->from->format('H:i') }}<br/>
                                                                {{ $dateslots[$timeslot->id]->available }} /
                                                                {{ $dateslots[$timeslot->id]->pool }}
                                                            </a>
                                                        </td>
                                                        <td class="{{ $dateslots[$timeslot->id]->available > 0 ? 'available' : 'unavailable' }}">
                                                            @if($dateslots[$timeslot->id]->available > 0)
                                                                <label class="checkbox-select">
                                                                    <input type="checkbox" class="datetime-slots" name="datetime-slots" disabled="disabled" value="{{ $dateslots[$timeslot->id]->id }}" />
                                                                </label>
                                                            @endif
                                                        </td>
                                                    @else
                                                        <td>N/A</td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        </table>
                                    </td>
                                @endfor
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- reservation modal --}}
    <div class="modal fade in" id="reservation-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            <span class="fa fa-group fa-fw"></span> Buchungen
                        </h4>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" id="btn-increase-pool" data-dateslot-id="">Boot freigeben</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <style>
        .rental-container {
            width: 100%;
            padding: 0 15px;
            font-size: 16px;
        }

        .rental-table td {
            padding: 4px;
        }

        .rental-table th {
            vertical-align: top;
        }

        .timeslots-table {
            width: 100%;
            border-collapse: collapse;
        }

        .timeslots-table tr {
            height: 50px;
        }

        .timeslots-table td {
            padding: 4px;
        }

        .timeslots-table td > a {
            display: block;
            width: 100%;
            color: black;
        }

        .timeslots-table button {
            height: 42px;
        }

        .timeslots-table td.available {
            background-color: #66cc66;
        }

        .timeslots-table td.unavailable {
            background-color: #dd7777;
        }

        .btn-modal {
            margin: 0 0.5rem;
        }

        .datepicker-week {
            visibility: hidden;
            width: 0;
            height: 0;
            margin: 0;
            padding: 0;
        }

        .checkbox-select {
            padding: 8px;
        }
    </style>
@endpush

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $('document').ready(function() {
            var calActions = $('.calendar-action > a');

            calActions.removeClass('disabled');
            calActions.on('click', function () {
                $('body').LoadingOverlay('show', {
                    image: '',
                    fontawesome: 'fa fa-spinner fa-pulse',
                    custom: '<h2 class="noselect">Daten werden geladen ...</h2>',
                    zIndex: 2000
                });
            });

            var dateSelect = $('.datepicker-week');
            dateSelect.datepicker({
                format: 'yyyy-mm-dd',
                calendarWeeks: true
            });
            dateSelect.datepicker().on('changeDate', function (ev) {
                var date = moment(ev.date);
                var route = '{{ route('admin::rental.index') }}?date=' + date.format('YYYY-MM-DD');

                $('.rental-table').LoadingOverlay('show', {
                    image: '',
                    fontawesome: 'fa fa-spinner fa-pulse',
                    custom: '<h2 class="noselect">Daten werden geladen ...</h2>',
                    zIndex: 2000
                });

                location.replace(route);
            });

            var weekSelectButton = $('#btn-select-week');
            weekSelectButton.off('click');
            weekSelectButton.on('click', function (ev) {
                ev.preventDefault();
                dateSelect.datepicker('show');
            });

            $('.link-to-reservations').on('click', function (ev) {
                ev.preventDefault();

                $('.rental-table').LoadingOverlay('show', {
                    image: '',
                    fontawesome: 'fa fa-spinner fa-pulse',
                    custom: '<h2 class="noselect">Daten werden geladen ...</h2>',
                    zIndex: 2000
                });

                var route = '{{ route('admin::rental.reservations.index', ['timeslotDateId' => '__TIMESLOT_DATE_ID__']) }}';
                route = route.replace('__TIMESLOT_DATE_ID__', $(this).attr('data-dateslot-id'));
                $('#btn-increase-pool').attr('data-dateslot-id', $(this).attr('data-dateslot-id'));

                $('#reservation-modal .modal-body').load(route, function () {
                    $('#reservation-modal').modal({ show: true});
                    $('.rental-table').LoadingOverlay('hide');
                });
            });

            var resCheckboxes = $('input[name="datetime-slots"]');
            var resButton = $('#btn-reserve-slots');

            resCheckboxes.prop('disabled', false);
            resCheckboxes.on('click', function () {
                resButton.prop('disabled',
                    ($('input[name="datetime-slots"]:checked').length === 0)
                );
            });

            resButton.on('click', function () {
                var route = '{{ route('admin::rental.reservations.create') }}';

                swal({
                    title: 'Boot(e) manuell buchen',
                    type: 'question',
                    input: 'textarea',
                    inputPlaceholder: 'Kunde',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-modal btn-success',
                    cancelButtonClass: 'btn btn-modal btn-default',
                    confirmButtonText: 'Ja, buchen',
                    cancelButtonText: 'Nein, abbrechen',
                    buttonsStyling: false
                }).then(function (result) {
                    if (result.value) {
                        var datetimeSlots = $('.datetime-slots:checkbox:checked').map(function (i, slot) {
                            return slot.value;
                        });

                        $('.rental-table').LoadingOverlay('show', {
                            image: '',
                            fontawesome: 'fa fa-spinner fa-pulse',
                            custom: '<h2 class="noselect">Bitte warten ...</h2>',
                            zIndex: 2000
                        });

                        $.postJSON(route, {
                            notice: result.value,
                            timeslot_dates: datetimeSlots.toArray()
                        }).always(function () {
                            location.reload();
                        });
                    }
                });
            });

            $('#btn-increase-pool').on('click', function () {
                var route = '{{ route('admin::rental.reservations.delete') }}';
                var dateSlotId = $(this).attr('data-dateslot-id');

                $(this).html('<span class="fa fa-spinner fa-pulse fa-fw"></span> verabeite...');
                $(this).prop('disabled', true);

                $.postJSON(route, {
                    timeslot_date: dateSlotId
                }).always(function () {
                    location.reload();
                });
            });
        });

        $.postJSON = function (url, data, callback, error) {
            return jQuery.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: 'json',
                success: callback,
                error: error
            });
        };
    </script>
@endpush