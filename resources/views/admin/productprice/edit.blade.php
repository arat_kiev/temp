@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::products.edit', ['productId' => $product]) }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Preis
                @if($productPrice->id)
                    bearbeiten
                @else
                    erstellen
                @endif
                <small>{{ $product->name }}</small>
            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Preis
                </div>
                <div class="panel-body">
                    {!! form_row($form->value) !!}
                    {!! form_row($form->vat) !!}
                    {!! form_row($form->visible_from) !!}
                    {!! form_row($form->valid_from) !!}
                    {!! form_row($form->valid_till) !!}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            @if($productPrice->id)
            <div class="panel panel-default">
                <div class="panel-heading">
                    Zeitfenster
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="timeslots-container" data-prototype="{{ form_row($form->timeslots->prototype()) }}">
                            {!! form_row($form->timeslots) !!}
                        </div>
                        @unless($productPrice->sales()->count())
                            <button type="button" class="btn btn-primary" id="timeslots-btn-add">
                                <span class="fa fa-plus fa-fw"></span> Hinzufügen
                            </button>
                        @endunless
                    </div>
                </div>
            </div>
            @endif
            @if($productPrice->timeslots()->count())
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Rabatte
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="discounts-container" data-prototype="{{ form_row($form->timeDiscounts->prototype()) }}">
                                {!! form_row($form->timeDiscounts) !!}
                            </div>
                            <button type="button" class="btn btn-primary" id="discounts-btn-add">
                                <span class="fa fa-plus fa-fw"></span> Hinzufügen
                            </button>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
            @if($productPrice->id && $productPrice->sales()->count())
                <a class="btn btn-warning"
                   href="{{ route('admin::products.prices.create', ['productId' => $product]) }}">
                    <span class="fa fa-plus fa-fw"></span>
                    Neuer Preis
                </a>
            @endif
            @if($productPrice->sales()->count())
                <div class="row response-popup" style="padding-top: 10px;">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                            Preis kann nicht mehr bearbeitet werden.
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {!! form_end($form) !!}
    <br/>
    @if($productPrice->timeslots()->count())
    <div class="row">
        <div class="col-lg-12">
            <div id="calendar-wrapper">
                <div id="calendar"></div>
            </div>
        </div>
    </div>
    @endif
@endsection

@push('styles')
    <style>
        .fc-day.fc-sat,
        .fc-day.fc-sun {
            background-color: lightgoldenrodyellow;
        }

        .fc-unthemed td.fc-today {
            background: transparent !important;
        }

        #calendar-wrapper {
            border-radius: 8px;
            background-color: #fbfbfb;
            padding: 16px;
        }
    </style>
@endpush

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    //--- AJAX Spinner
    $(document).ajaxStart(function () {
        $('#calendar-wrapper').LoadingOverlay('show', {
            image: '',
            fontawesome: 'fa fa-spinner fa-pulse',
            custom: '<h2 class="noselect">Daten werden aktualisiert ...</h2>',
            zIndex: 2000
        });
    });

    $(document).ajaxStop(function () {
        window.setTimeout(function () {
            $('#calendar-wrapper').LoadingOverlay('hide');
        }, 500);
    });
    //--- End AJAX Spinner

    $('document').ready(function() {
        $('#products').chosen({
            width: '100%'
        });

        updateCalendar();
    });

    var dates = [];
    var calendarConfig = {
        customButtons: {
            save: {
                text: 'Speichern',
                click: saveTimeslotDates
            },
            reload: {
                text: 'Neu laden',
                click: updateCalendar
            },
            checkall: {
                text: 'Alle auswählen',
                click: function () {
                    checkAll(true);
                }
            },
            checknone: {
                text: 'Alle abwählen',
                click: function () {
                    checkAll(false);
                }
            }
        },
        header: {
            center: 'reload checkall checknone',
            right: 'save prev,next'
        },
        footer: {
            right: 'save prev,next'
        },
        bootstrapGlyphicons: {
            reload: 'glyphicon-refresh',
            checkall: 'glyphicon-check',
            checknone: 'glyphicon-ban-circle'
        },
        themeSystem: 'bootstrap3',
        contentHeight: '158px',
        aspectRatio: 1.2,
        defaultView: 'month',
        showNonCurrentDates: false,
        fixedWeekCount: false,
        windowResize: onCalendarResize,
        eventAfterAllRender: onCalendarResize,
        dayRender: renderDayWithTimeslots
    };

    var timeslots = JSON.parse('{!! $productPrice->timeslots->toJson() !!}');
    var priceFrom = moment('{{ $productPrice->valid_from ? $productPrice->valid_from->toDateTimeString() : '' }}');
    var priceTill = moment('{{ $productPrice->valid_till ? $productPrice->valid_till->toDateTimeString() : '' }}');

    $.postJSON = function(url, data, callback, error) {
        return jQuery.ajax({
            type : 'POST',
            url: url,
            data: JSON.stringify(data),
            dataType : 'json',
            processData: false,
            contentType: 'application/json',
            success : callback,
            error : error
        });
    };

    function onCalendarResize() {
        $('#calendar-wrapper').height($('#calendar').first().height() + 15);
    }

    function saveTimeslotDates()
    {
        var dates = {};
        var month = 0;
        var year = 0;

        checkboxes = $('.timeslot_date');

        if (checkboxes.length === 0) {
            return;
        }

        checkboxes.each(function () {
            var slot_date = this.name.split("_");
            var slot = slot_date[0];
            var date = slot_date[1];

            if (year === 0) {
                year = date.split('-')[0];
                month = date.split('-')[1];
            }

            if (dates[date] === undefined) {
                dates[date] = {};
            }

            dates[date][slot] = this.checked;
        });

        console.log(JSON.stringify(dates));

        $.postJSON('{{
            route('admin::products.prices.dates.update', [
                'productId' => $productPrice->product ? $productPrice->product->id : 0,
                'priceId' => $productPrice->id,
            ])
        }}', {
            month: month,
            year: year,
            dates: dates
        }).done(function () {
            updateCalendar();
        });
    }

    function checkAll(checked) {
        $('.timeslot_date').attr('checked', checked);
    }

    function updateCalendar() {
        var year = '{{ \Carbon\Carbon::now()->year }}';
        var month = '{{ \Carbon\Carbon::now()->month }}';
        var date = moment('{{ \Carbon\Carbon::now()->format('Y-m-d') }}');

        if ($('#calendar').fullCalendar('getCalendar')) {
            date = $('#calendar').fullCalendar('getDate');
            year = date.year();
            month = date.month() + 1;
            calendarConfig['defaultDate'] = date;
        }

        $('#calendar').fullCalendar('destroy');

        $.getJSON('{{
            route('admin::products.prices.dates.list', [
                'productId' => $productPrice->product ? $productPrice->product->id : 0,
                'priceId' => $productPrice->id,
            ])
        }}', {
            year: year,
            month: month
        }).done(function (data) {
            dates = data;
            $('#calendar').fullCalendar(calendarConfig);
            $('.fc-prev-button').on('click', updateCalendar);
            $('.fc-next-button').on('click', updateCalendar);
        });
    }

    function renderDayWithTimeslots(date, cell) {
        var calMonth = $('#calendar').fullCalendar('getDate').month();

        if (date.isBetween(priceFrom, priceTill)) {
            if (date.month() === calMonth) {
                cell.append(renderTimeslotContainer(date, timeslots));
            }
        }
    }

    function renderTimeslotContainer(date, timeslots) {
        var container = $("<div style='margin-top: 25px; padding: 0 10px;'></div>");

        timeslots.forEach(function (slot) {
            container.append(renderTimeslot(date, slot));
        });

        return container;
    }

    function renderTimeslot(date, slot) {
        var formattedDate = date.format('YYYY-MM-DD');
        var checkboxName = slot.id + '_' + formattedDate;

        var checked = (Array.isArray(dates[formattedDate])
            && dates[formattedDate].indexOf(slot.id) !== -1)
            ? 'checked' : '';

        var disabled = '{{ $productPrice->sales()->count() ? ' disabled="disabled"' : ''}}';

        var labelBackground = checked !== '' ? 'background-color: lightgreen;' : '';

        return [
            '<label style="display: block;' + labelBackground + '">',
            '<input type="checkbox" class="timeslot_date" style="margin-left: 5px;" name="' + checkboxName + '" ' + checked + disabled + ' />',
            moment(slot.from.date).format('HH:mm'),
            '</label>'
        ].join("\n");
    }

    function onTimeslotRemoveClicked(e) {
        e.preventDefault();
        $(this).closest('.row').remove();
    }

    $('.timeslots-btn-remove').on('click', onTimeslotRemoveClicked);

    $('#timeslots-btn-add').on('click', function(e) {
        e.preventDefault();
        var container = $('.timeslots-container');
        var count = container.children().length;
        var proto = container.data('prototype').replace(/__NAME__/g, count);
        container.append(proto);

        $('.timeslots-btn-remove').on('click', onTimeslotRemoveClicked);
    });

    function onDiscountRemoveClicked(e) {
        e.preventDefault();
        $(this).closest('.row').remove();
    }

    $('.discounts-btn-remove').on('click', onDiscountRemoveClicked);

    $('#discounts-btn-add').on('click', function(e) {
        console.log('add discount');
        e.preventDefault();
        var container = $('.discounts-container');
        var count = container.children().length;
        var proto = container.data('prototype').replace(/__NAME__/g, count);
        container.append(proto);

        $('.discounts-btn-remove').on('click', onDiscountRemoveClicked);
    });
</script>
@endpush