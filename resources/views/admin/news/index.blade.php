@extends('admin.layout.organization')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                News
                @if(isset($current_organization))
                    <div class="pull-right">
                        <a href="{{ route('admin::news.create') }}" class="btn btn-success">
                            <span class="fa fa-plus fa-fw"></span> Eintrag erstellen
                        </a>
                    </div>
                @endif
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-news">
                <thead>
                <tr>
                    <th>Titel</th>
                    <th>Autor</th>
                    <th>Öffentlich</th>
                    <th>Veröffentlicht am</th>
                    <th>Erstellt am</th>
                    <th>Aktionen</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#datatable-news').DataTable({
            bFilter: true,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::news.data') }}',
            pagingType: "full_numbers",
            columns: [
                { data: 'title' },
                { data: 'author_name', sortable: false, searchable: false },
                { data: 'public' },
                { data: 'published_on', searchable: false },
                { data: 'created_at', searchable: false },
                { data: 'actions', sortable: false, searchable: false }
            ],
            order: [[3, 'desc']],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            },
            createdRow: function (row, data) {
                if (data['status'] == 0) {
                    $(row).addClass('warning');
                } else if (data['status'] == 1) {
                    $(row).addClass('success');
                } else if (data['status'] == 2) {
                    $(row).addClass('danger');
                }
            }
        });
    });
</script>
@endpush
