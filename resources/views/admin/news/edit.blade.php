@extends('admin.layout.organization')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::news.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Eintrag
                @if($news->id)
                    bearbeiten<br/><small>{{ $news->name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! form_row($form->public) !!}
                    <div class="row">
                        <div class="col-lg-6">
                            {!! form_row($form->published_on) !!}
                        </div>
                        <div class="col-lg-6">
                            {!! form_row($form->unpublished_on) !!}
                        </div>
                    </div>
                    {!! form_row($form->title) !!}
                    {!! form_row($form->content) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <h3 class="col-lg-12">Bilder</h3>
        @if($news->id)
            @if($news->gallery)
                @foreach($news->gallery->pictures()->orderBy('pivot_priority', 'desc')->get() as $picture)
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="thumbnail text-justify">
                            <img src="{{ route('imagecache', ['template' => 'lre', 'filename' => $picture->file]) }}" />
                            <div class="btn-group btn-group-justified">
                                @if($picture->pivot->priority === 5)
                                    <a href="#" class="btn btn-primary">
                                        <span class="fa fa-check fa-fw"></span>
                                        Titelbild
                                    </a>
                                @else
                                    <a href="{{ route('admin::news.gallery.primary', ['newsId' => $news->id, 'pictureId' => $picture->id]) }}" class="btn btn-warning">
                                        <span class="fa fa-exchange fa-fw"></span>
                                        Titelbild
                                    </a>
                                @endif
                                <div class="btn-group">
                                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="fa fa-remove fa-fw"></span> <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ route('admin::news.gallery.delete', ['newsId' => $news->id, 'pictureId' => $picture->id]) }}">Löschen</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="thumbnail">
                    <div class="dropzone dz-clickable" id="gallery-dropzone">
                        <div class="dz-default dz-message">
                            <span>
                                Bilder hierher ziehen um sie hochzuladen
                                <span class="fa fa-file-image-o fa-4x"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        @else
           <div class="col-lg-12">
               <p>Bitte speichern Sie erst den Eintrag, danach können Sie Bilder anhängen.</p>
           </div>
        @endif
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#content_text').summernote({
            lang: 'de-DE',
            height: '300px',
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['link', ['linkDialogShow', 'unlink']],
                ['misc', ['fullscreen', 'undo', 'redo', 'reset']]
            ],
            callbacks: {
                onImageUpload: function(files) {
                    return false;
                }
            }
        });

        $('#published_on_dtp,#unpublished_on_dtp').datetimepicker({
            locale: 'de-at',
            format: 'DD.MM.YYYY HH:mm',
            showTodayButton: true,
            showClear: true
        });

        $(function()
        {
            Dropzone.options.galleryDropzone = {
                url: '{{ route('admin::news.gallery.upload', ['newsId' => $news->id]) }}',
                paramName: 'file',
                maxFilesize: 20,
                acceptedFiles: 'image/jpeg,image/png',
                dictDefaultMessage: 'Bilder hierher ziehen um sie hochzuladen',
                dictFallbackMessage: 'Ihr Browser unterstützt kein Drag\'n\'Drop',
                dictFallbackText: 'Bitte benutzen Sie das Formular unterhalb',
                dictInvalidFileType: 'Es sind nur JPEG und PNG erlaubt',
                dictFileTooBig: 'Eines der Bilder ist zu groß, maximal 10MB',
                dictResponseError: 'Der Server hatte ein Problem bei der Verarbeitung des/der Bildes/er'
            }
        });
    });
</script>
@endpush