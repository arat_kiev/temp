@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Meine Verkaufsstellen
            </h1>
        </div>
    </div>

    {{-- modal edit dialog --}}
    <div class="modal fade" id="modal-edit" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Zuweisen</h4>
                </div>
                <div class="modal-body">
                    <div id="load" style="text-align: center;">
                        <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
                    </div>
                    <div id="jstree"></div>
                </div>
                <div class="modal-footer" style="clear:both;">
                    <button type="button" class="btn btn-success" id="submit-edit"><span class="fa fa-save fa-fw"></span> Speichern</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close fa-fw"></span> Abbrechen</button>
                </div>
            </div>
        </div>
    </div>

    {{-- Table with resellers --}}
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-resellers">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Zuletzt bearbeitet</th>
                        <th>Links</th>
                        <th>Aktionen</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {{-- product modal --}}
    <div class="modal fade in" id="product-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            <span class="fa fa-cart-arrow-down fa-fw"></span> Produkte
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select class="form-control" id="products" name="products[]" multiple="multiple"></select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

{{-- Include jstree files --}}
@push('scripts')
    {!! Html::style('lib/jstree/dist/themes/default/style.min.'.$av.'.css') !!}
    {!! Html::script('lib/jstree/dist/jstree.min.'.$av.'.js') !!}
@endpush

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {

        $('#jstree').jstree({
            "plugins" : ["checkbox" ],
            'core' : {
                'data' : {!! json_encode($ticketPrices) !!}
            }
        });

        var resellerId = 0;
        var jstree =  $('#jstree').jstree();

        // DataTable
        var tbl = $('#datatable-resellers').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::resellers.data') }}',
            pagingType: "full_numbers",
            columns: [
                { data: 'id', width: '5%', className: 'text-center'},
                { data: 'name'},
                { data: 'updated_at' },
                { data: 'links', orderable: false, searchable: false },
                { data: 'actions', name: 'action', orderable: false, searchable: false, width: '10%'}
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        // Preparing modals content, before showing
        $('#modal-edit').on('show.bs.modal', function(e) {
            resellerId = $(e.relatedTarget).data('record');

            jstree.hide_all();
            $('#load').show();

            $.get({
                url: '{{ route('admin::resellers.ticket_prices.list') }}' + '/' + resellerId,
                success: function(data) {
                    // Fill jstree, hide loader, show jstree
                    jstree.select_node(data);
                    $('#load').hide();
                    jstree.show_all();
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });

        // Cleaning after closing modal
        $('#modal-edit').on('hide.bs.modal', function() {
            jstree.deselect_all();
            jstree.close_all();
            jstree.hide_all();
        });

        // Edit record
        $('#submit-edit').click(function(e) {
            var ticketPriceIds = $('#jstree').jstree().get_bottom_selected();
            $.post({
                url: '{{ route('admin::resellers.ticket_prices.update') }}' + '/' + resellerId,
                contentType: 'application/json',
                data: JSON.stringify({ticketPriceIds: ticketPriceIds}),
                success: function(data) {
                    $('#modal-edit').modal('hide');
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });

        $('#product-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var formAction = button.data('action');

            var modal = $(this);
            modal.find('.form').attr('action', formAction);

            var productSelect = modal.find('#products');
            var productData = [];

            productSelect.children().remove();

            $.getJSON(formAction).done(function (data) {
                $.each(data.data, function (index, area) {
                    productSelect.append($('<option/>', {
                        value: area.id,
                        text: area.name,
                        selected: true
                    }));
                });
            }).done(function () {
                productSelect.select2({
                    width: '100%',
                    data: productData,
                    ajax: {
                        url: '{{ route('admin::products.all') }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term,
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.data,
                                pagination: {
                                    more: params.page < data.meta.pagination.total_pages
                                }
                            };
                        },
                        cache: true
                    },
                    templateResult: function (product) {
                        if (product.loading) return product.name;

                        return "<div class='select2-result-admin'>"
                            + "     <div class='select2-result-admin__name'>" + product.name + " (#" + product.id + ")</div>"
                            + "     <div class='select2-result-admin__address'>"
                            +           (product.featured_from
                                            ? 'seit ' + moment(product.featured_from).format('DD.MM.YYYY')
                                            : '')
                            +           (product.featured_till
                                            ? ' bis ' + moment(product.featured_till).format('DD.MM.YYYY')
                                            : '')
                            + "     </div>"
                            + " </div>";
                    },
                    templateSelection: function (product) {
                        return product.text || product.name;
                    },
                    escapeMarkup: function (markup) { return markup; }
                });
            });
        });
    });
</script>
@endpush

@push('styles')
<style>
    .glyphicon-menu-hamburger {
        color: #6495ED;
    }
    .glyphicon-euro {
        color: #6B8E23;
    }
    .glyphicon-chevron-right {
        color: #F0FFFF;
    }
</style>
@endpush