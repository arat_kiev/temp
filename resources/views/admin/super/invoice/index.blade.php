@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Abrechnungen
                <div class="pull-right">
                    <a href="{{ route('admin::super:invoices.create', ['manager' => $manager]) }}"
                       class="btn btn-success">
                        <span class="fa fa-plus fa-fw"></span> Abrechnung anlegen
                    </a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="manager">Bewirtschafter</label>
                        <select class="form-control" name="manager" id="filter-manager">
                            @foreach($managerList as $managerId => $managerName)
                                <option
                                        value="{{ $managerId }}"
                                        @if($managerId == $manager)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $managerName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group input-daterange">
                        <div class="input-group-addon input-group-label">Datum</div>
                        <input type="text"
                               class="form-control"
                               id="date-range-from"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $from)->format('d.m.Y') }}">
                        <span class="input-group-addon">bis</span>
                        <input type="text"
                               class="form-control"
                               id="date-range-till"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $till)->format('d.m.Y') }}">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="button" id="filter-date-range-btn">Filter</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-sales">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Bewirtschafter</th>
                        <th>Anzahl der Produkte</th>
                        <th>Online</th>
                        <th>Von Datum</th>
                        <th>Bis Datum</th>
                        <th>Erstellt am</th>
                        <th>Aktionen</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {{-- preview modal --}}
    <div class="modal fade in" id="preview-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                    <h4 class="modal-title">
                        <span class="fa fa-eye fa-fw"></span>
                        <span id="preview-title">Vorschau wird geladen (bitte etwas Geduld)</span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="col-lg-12">
                            <img class="preview-image img-responsive" width="1083px" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
                </div>
            </div>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Schließen"><span>&times;</span></button>
                        <h4 class="modal-title">
                            </span> Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie diese Abrechnung stornieren wollen?</p>
                        <hr>
                        <div class="form-group">
                            <label for="storno-reason">Beschreibung der Stornierung</label>
                            <textarea class="form-control" id="storno-reason"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Stornieren
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('.input-daterange').datepicker();

        function updateFilters() {
            var from = $('#date-range-from').datepicker('getDate'),
                till = $('#date-range-till').datepicker('getDate'),
                fromString = moment(from).format('YYYY-MM-DD'),
                tillString = moment(till).format('YYYY-MM-DD'),
                manager = $('#filter-manager').val(),
                location = '{{ route('admin::super:invoices.index') }}',
                params = [
                    'from=' + fromString,
                    'till=' + tillString
                ];

            if (manager >= 0) {
                params.push('manager=' + manager);
            }

            window.location = location + '?' + params.join('&');
        }

        $('#filter-manager').on('change', updateFilters);
        $('#filter-date-range-btn').on('click', updateFilters);
        $('#date-range-from').on('change', updateFilters);
        $('#date-range-till').on('change', updateFilters);

        var table = $('#datatable-sales').DataTable({
            bFilter: false,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('admin::super:invoices.data') }}' + window.location.search
            },
            pagingType: "full_numbers",
            columns: [
                { data: 'invoice_number', orderable: false },
                { data: 'manager', orderable: false },
                { data: 'tickets_count', orderable: false },
                { data: 'online' },
                { data: 'date_from' },
                { data: 'date_till' },
                { data: 'created_at' },
                { data: 'actions', orderable: false }
            ],
            order: [[1, 'desc']],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('[data-toggle="tooltip"]').tooltip();

        $('#preview-modal').on('show.bs.modal', function(e) {
            var modal = $(this);
            var button = $(e.relatedTarget);
            var invoiceId = button.data('invoice-id');
            var format = 'image';

            var title = modal.find('#preview-title');
            title.html('Vorschau wird geladen <span class="fa fa-cog fa-spin"></span>');

            modal.find('.preview-image').attr('src', '{{ URL::to('/preview/invoice/') }}/' + invoiceId + '/' + format)
                    .on('load', function() {
                        title.html('Vorschau für Abrechnung: ' + invoiceId);
                    });
        });

        $('#delete-modal').on('show.bs.modal', function(e) {
            var formAction = $(e.relatedTarget).data('action'),
                formMethod = 'POST',
                $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();
                $(document).find('.response-popup').remove();

                $.ajax({
                    type : formMethod,
                    url: formAction,
                    data: { reason: $(document).find('#storno-reason').val() },
                    success : function(data) {
                        var msgBox = ''
                            + '<div class="row response-popup" style="padding-top: 10px;">'
                            + '    <div class="col-lg-12">'
                            + '        <div class="alert alert-' + data.notification + ' alert-dismissable">'
                            + '            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">'
                                            + "&times;"
                            + '            </button>'
                                            + data.message
                            + '        </div>'
                            + '    </div>'
                            + '</div>';
                        $('.page-header').parents('.row').before(msgBox);
                        table.ajax.reload();
                        $('#delete-modal').modal('hide');
                    }
                });
            });
        });
    });
</script>
@endpush