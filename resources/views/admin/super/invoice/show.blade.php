@extends('admin.layout.default')

@inject('invoiceManager', 'App\Managers\InvoiceManager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:invoices.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Abrechnung <small>#{{ $invoiceManager->getInvoiceNumber($invoice) }}</small>
                <div class="pull-right">
                    <div class="dropdown">
                        <button type="button" class="btn btn-default" data-toggle="dropdown">
                            <span class="fa fa-download fa-fw"></span> Export
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="{{ route('admin::super:invoices.export', [
                                    'invoice'   => $invoice->id,
                                    'format'    => 'xls',
                                    'from'      => $invoice->date_from->format('Y-m-d'),
                                    'till'      => $invoice->date_till->format('Y-m-d'),
                                    'manager'   => $manager->id,
                                ]) }}">
                                    <span class="fa fa-file-excel-o fa-fw"></span> XLS-Datei
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-md-6 pull-left">
                    <table class="table">
                        <tr>
                            <th>Umsatz gesamt</th>
                            <td>{{ number_format($invoiceManager->getTotalPrice($invoice) / 100, 2, ',', '.') . ' €' }}</td>
                        </tr>
                        <tr>
                            <th>Provision-netto</th>
                            <td>{{ number_format($invoiceManager->getCommissionNet($invoice) / 100, 2, ',', '.') . ' €' }}</td>
                        </tr>
                        <tr>
                            <th>Provision-brutto</th>
                            <td>{{ number_format($invoiceManager->getCommissionGross($invoice) / 100, 2, ',', '.') . ' €' }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6 pull-right">
                    <table class="table">
                        <tr>
                            <th>Bewirtschafter</th>
                            <td>{{ $manager->name }}</td>
                        </tr>
                        <tr>
                            <th>Zeitspanne</th>
                            <td>von {{ $from }} bis {{ $till }}</td>
                        </tr>
                        <tr>
                            <th>Abrechnung</th>
                            <td>wurde am {{ $createdAt }} generiert</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-sales">
                    <thead>
                    <tr>
                        <th></th>
                        <th>#</th>
                        <th>Name</th>
                        <th>Gewässer</th>
                        <th>Typ</th>
                        <th>Ab Datum</th>
                        <th>Gekauft</th>
                        <th>Preis</th>
                        <th>Provision</th>
                        <th>Aktionen</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Schließen"><span>&times;</span></button>
                        <h4 class="modal-title">
                            </span> Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie diese Produkte stornieren wollen?</p>
                        <hr>
                        <div class="form-group">
                            <label for="storno-reason">Beschreibung der Stornierung</label>
                            <textarea class="form-control" id="storno-reason"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Stornieren
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        var table = $('#datatable-sales').DataTable({
            bFilter: false,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('admin::super:invoices.show.data', ['invoiceId' => $invoice->id]) }}'
            },
            pagingType: "full_numbers",
            columns: [
                {
                    className: 'details-control',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                { data: 'manager_ticket_id', orderable: false },
                { data: 'user_full_name', orderable: false },
                { data: 'area_name', orderable: false },
                { data: 'type_name', orderable: false },
                { data: 'valid_from' },
                { data: 'created_at' },
                { data: 'price_compact', orderable: false },
                { data: 'commission', orderable: false },
                { data: 'actions', orderable: false }
            ],
            order: [[1, 'desc']],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        function format(d) {
            return d.reseller_ticket
                ? formatDetail(d.reseller_ticket, d)
                : (d.user ? formatDetail(d.user, d) : '');
        }

        function formatDetail(user, ticket) {
            return '' +
                '<table class="ticket-user-detail">' +
                '   <tr>' +
                '       <th>Vorname:</th>' +
                '       <td>' + (user.first_name || ticket.first_name) + '</td>' +
                '       <th>BA-Ticket-Nr:</th>' +
                '       <td>' + ticket.ticket_number + '</td>' +
                '   </tr>' +
                '   <tr>' +
                '       <th>Nachname:</th>' +
                '       <td>' + (user.last_name || ticket.last_name) + '</td>' +
                '       <th>Ticket-Typ</th>' +
                '       <td>' + ticket.type_name + '</td>' +
                '   </tr>' +
                '   <tr>' +
                '       <th>Geburtstag:</th>' +
                '       <td>' + moment(new Date(user.birthday)).format("DD.MM.YYYY") + '</td>' +
                '       <th>Preis-Typ:</th>' +
                '       <td>' + (ticket.price.name || 'Normalpreis') + '</td>' +
                '   </tr>' +
                '   <tr>' +
                '       <th>Straße:</th>' +
                '       <td>' + user.street + '</td>' +
                '       <th>Brutto-Preis:</th>' +
                '       <td>' + ticket.price_brutto + '</td>' +
                '   </tr>' +
                '   <tr>' +
                '       <th>Ort:</th>' +
                '       <td>' + user.city + '</td>' +
                '       <th>Gekauft:</th>' +
                '       <td>' + ticket.created_at + '</td>' +
                '   </tr>' +
                '   <tr>' +
                '       <th>PLZ:</th>' +
                '       <td>' + user.post_code + '</td>' +
                '       <th>Gültig von:</th>' +
                '       <td>' + (ticket.valid_from || ticket.featured_from || '') + '</td>' +
                '   </tr>' +
                '   <tr>' +
                '       <th>Land:</th>' +
                '       <td>' + (user.country && user.country.name) + '</td>' +
                '       <th>Gültig bis:</th>' +
                '       <td>' + (ticket.valid_to || ticket.featured_till || '') + '</td>' +
                '   </tr>' +
                '</table>';
        }

        $('#datatable-sales tbody').on('click', 'td.details-control', function() {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            } else {
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });

        $('[data-toggle="tooltip"]').tooltip();

        $('#delete-modal').on('show.bs.modal', function(e) {
            var formAction = $(e.relatedTarget).data('action'),
                    formMethod = 'POST',
                    $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();
                $(document).find('.response-popup').remove();

                $.ajax({
                    type : formMethod,
                    url: formAction,
                    data: { reason: $(document).find('#storno-reason').val() },
                    success : function(data) {
                        var msgBox = ''
                            + '<div class="row response-popup" style="padding-top: 10px;">'
                            + '    <div class="col-lg-12">'
                            + '        <div class="alert alert-' + data.notification + ' alert-dismissable">'
                            + '            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">'
                                            + "&times;"
                            + '            </button>'
                                            + data.message
                            + '        </div>'
                            + '    </div>'
                            + '</div>';
                        $('.page-header').parents('.row').before(msgBox);
                        $('#delete-modal').modal('hide');
                    }
                });
            });
        });
    });
</script>
@endpush