@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Berechtigungen

                {{-- Button create new license --}}
                <div class="pull-right">
                    <button type="button" class="btn btn-success" data-target="#modal-create-license" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                        <span class="fa fa-plus fa-fw"></span> Neue Berechtigung anlegen
                    </button>
                </div>
            </h1>
        </div>
    </div>

    {{-- modal create new license --}}
    <div class="modal fade" id="modal-create-license" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Neue Berechtigung anlegen</h4>
                </div>
                <div class="modal-body">
                    {!! form($form, ['id' => 'form-create-license']) !!}
                </div>
                <div class="modal-footer" style="clear:both;">
                    <div class="pull-left">
                        <span id="form-not-filled" style="display: none; color: red;">All fields should be filled</span>
                        <span id="server-error" style="display: none; color: red;">Error occurred, please, try reload page</span>
                    </div>
                    <button type="button" class="btn btn-sm btn-success" id="submit-create"><span class="fa fa-save fa-fw"></span> Speichern</button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><span class="fa fa-close fa-fw"></span> Abbrechen</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2 col-lg-push-10">
            <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control" name="status" id="filter-status">
                    @foreach($statusesList as $statusValue)
                        <option
                            value="{{ $statusValue }}"
                            @if($statusValue == $status)
                                selected="selected"
                            @endif
                        >
                            {{ $statusValue }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-lg-12">
            <table class="table table-bordered table-hover dataTable no-footer nowrap" id="datatable-licenses">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Vorname</th>
                    <th>Nachname</th>
                    <th>Typ</th>
                    <th>Status</th>
                    <th>Zuletzt bearbeitet</th>
                    <th>Aktionen</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        function updateFilters() {
            var status = $('#filter-status').val();

            var location = '{{ route('admin::super:licenses.index') }}';
            var params = [];

            params.push('status=' + status);
            window.location = location + '?' + params;
        }

        $('#filter-status').on('change', updateFilters);

        $("#users").prepend('<option selected></option>').select2();
        $("#licenses").prepend('<option selected></option>').select2().on("select2:select", function (e) {
            $.get('{{ route('admin::super:license.type.attributes') }}' + '/' + $(this).val(), function(data) {
                // remove previous fields
                $('.lt-attr').remove();
                // add license based fields
                $.each(data, function (attrName, attrVal) {
                    // attachments
                    if (attrName == 'attachments' && attrVal.length > 0) {
                        for (var a = 0; a < attrVal.length; a++) {
                            $('#form-create-license .col-md-6:last-of-type').after(''
                                + '<div class="form-group col-md-6 lt-attr" style="padding-right: 0;">'
                                + ' <label for="attach-'+a+'" class="control-label">'
                                        + attrVal[a]
                                + ' </label>'
                                + ' <input class="form-control" name="'+attrVal[a]+'" type="file">'
                                + '</div>'
                            );
                        }
                        // fields
                    } else if (attrVal === Object(attrVal) && !Array.isArray(attrVal)) {
                        for (var f in attrVal) {
                            if (attrVal[f] == 'date') {
                                $('#form-create-license .col-md-6:last-of-type').after(''
                                    + '<div class="form-group col-md-6 lt-attr" style="padding-right: 0;">'
                                    + ' <label for="' + f + '" class="control-label">' + f + '</label>'
                                    + ' <input class="form-control" name="' + f + '" type="text"  data-provide="datepicker">'
                                    + '</div>'
                                );
                            } else {
                                $('#form-create-license .col-md-6:last-of-type').after(''
                                    + '<div class="form-group col-md-6 lt-attr" style="padding-right: 0;">'
                                    + ' <label for="' + f + '" class="control-label">' + f + '</label>'
                                    + ' <input class="form-control" name="' + f + '" type="text">'
                                    + '</div>'
                                );
                            }
                        }
                    }
                });
            });
        });

        // save license
        $('#submit-create').click(function(){
            var formData = new FormData($('#form-create-license')[0]);

            var notFilled = false;

            $('#form-create-license input').each(function(){
                if ($(this).val() === '') {
                    notFilled = true;
                }
            });
            // all fields should be filled
            if ($('#users').val() === '' || $('#licenses').val() === '' || notFilled) {
                $('#form-not-filled').fadeIn("slow");
                setTimeout(function () {
                    $('#modal-create-license').find('#form-not-filled').fadeOut("slow");
                }, 3000);
                return false;
            }

            $.ajax({
                type:'POST',
                url: '{{ route('admin::super:licenses.create') }}',
                data: formData,
                contentType: false,
                processData: false,
                cache: false,
                success:function(data){
                    // reload datatable
                    tbl.ajax.url('{{ route('admin::super:licenses.data') }}').load(null, false);
                    // close modal
                    $('#modal-create-license').modal('hide');
                },
                error: function(error){
                    $('#server-error').fadeIn("slow");
                    $('#submit-create').prop('disabled', true);
                }
            });

        });

        // clear create form in modal window
        $('#modal-create-license').on('hidden.bs.modal', function (e) {
            $('.lt-attr').remove();

            $('#licenses').select2("val", "");
            $('#users').select2("val", "");
        });

        var tbl = $('#datatable-licenses').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('admin::super:licenses.data') }}' + window.location.search
            },
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'first_name' },
                { data: 'last_name' },
                { data: 'type_name', searchable: false, sortable: false },
                { data: 'status' },
                { data: 'updated_at' },
                { data: 'actions', searchable: false, sortable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            },
            createdRow: function(row, data) {
                // console.log(data);
                if (data['status'] == "ACCEPTED") {
                    $(row).addClass('success');
                } else if (data['status'] == 'PENDING') {
                    $(row).addClass('warning');
                } else {
                    $(row).addClass('danger');
                }
            }
        });
    });
</script>
@endpush

@push('styles')
<style>
    .lt-attr:nth-child(odd) {
        padding-left: 0;
    }
</style>
@endpush