@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:licenses.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Berechtigung bearbeiten<br/>
                <small>{{ $license->type->name }} (#{{ $license->id }})</small>
            </h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
    {!! form_start($form, ['enctype' => 'multipart/form-data']) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="alert alert-info">
                {{ $license->user->first_name }} {{ $license->user->last_name }}<br/>
                {{ $license->user->birthday->format('d.m.Y') }}<br/>
                {{ $license->user->street }}<br/>
                {{ $license->user->post_code }} {{ $license->user->city }}<br/>
                {{ $license->user->country->name }}<br/>
                {{ $license->user->phone }}<br/>
                <a href="mailto:{{ $license->user->email }}">{{ $license->user->email }}</a>

            </div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                               aria-expanded="true" aria-controls="collapseOne" class="admin-collapse-link">
                                Personalien ändern
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body"> {!! form_row($form->email) !!}
                            {!! form_row($form->first_name) !!}
                            {!! form_row($form->last_name) !!}
                            {!! form_row($form->street) !!}
                            {!! form_row($form->post_code) !!}
                            {!! form_row($form->city) !!}
                            {!! form_row($form->country) !!}
                            {!! form_row($form->birthday) !!}
                            {!! form_row($form->phone) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Gültigkeitsdatum
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            {!! form_row($form->valid_from) !!}
                        </div>
                        <div class="col-sm-6">
                            {!! form_row($form->valid_to) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ablehnungsgrund
                </div>
                <div class="panel-body">
                    {!! form_row($form->status_text) !!}
                    <div class="alert alert-info">
                        <p>Hej, da ist anscheinend leider etwas schiefgelaufen.<br/>
                            Bei der EDV-Nummer benötigen wir das Format 12345/02, um diese prüfen beim
                            Landesfischereiverband zu überprüfen. <br/>
                            Du findest diese auf deiner Fischerkarte auf der Innenseite, bzw. auf dem Erlagschein.<br/>
                            Solltest du diese nicht finden, auf dem Erlagschein steht auch die Zahlungsreferenz, diese
                            kannst du uns ebenfalls schicken.
                        </p>
                    </div>
                    <div class="alert alert-info">
                        <p>Hej, da ist anscheinend leider etwas schiefgelaufen.<br/>
                            Die Überprüfung beim Landesfischereiverband meldete, dass für 2018 noch keine Abgabe
                            eingezahlt wurde.<br/>
                            Solltest du dies erst vor kurzem erledigt haben, so schick uns bitte die
                            Überweisungsbestätigung an: <a href="mailto:info@hejfish.com">info@hejfish.com</a>.<br/>
                            Wir schalten dich dann sofort frei. Vielen Dank!
                        </p></div>
                    <div class="alert alert-info">
                        <p>Hej, da ist anscheinend leider etwas schiefgelaufen.<br/>
                            Wir können leider auf den Fotos keine Gültigkeit für das aktuelle Jahr erkennen.<br/>
                            Schick uns doch bitte noch ein Foto, wo die Verlängerung ersichtlich ist an:
                            <a href="mailto:info@hejfish.com">info@hejfish.com</a>.<br/>
                            Vielen Dank!
                        </p></div>
                    <div class="alert alert-info">
                        <p>Hej, da ist anscheinend leider etwas schiefgelaufen.<br/>
                            Der Name auf der eingereichten Berechtigung stimmt nicht mit deinem Profil überein.<br/>
                            Bitte überprüfe dein Profil bzw. schick uns die richtige Berechtigung.<br/>
                            Du willst auch für einen Freund kaufen?<br/>
                            Das geht leider nur mit einem eigenen Benutzerkonto. <br/>
                            Bitte lege ein zweites Konto an (kann auch mit einer Alias-E-Mail Adresse sein) und reiche
                            die Berechtigung dort ein.
                        </p></div>
                    <div class="alert alert-info">
                        <p>Hej, da ist anscheinend leider etwas schiefgelaufen.<br/>
                            Du hast leider kein Foto von deiner Berechtigung eingereicht.<br/>
                            Bitte mach ein Foto von deiner Fischereiberechtigung und lade diese hoch.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Anhänge
                </div>
                <div class="panel-body">
                    <div class="row" id="attachments">
                        @foreach($license->attachments as $attachment)
                            @if(ends_with($attachment->file, '.pdf'))
                                <div class="col-md-12">
                                    {!!
                                        Html::linkRoute(
                                            'filestream',
                                            'Anhang (PDF)',
                                            ['name' => $attachment->filePathWithName],
                                            ['target' => '_blank']
                                        )
                                    !!}
                                </div>
                            @else
                                <div class="col-sm-6">
                                    <a href="{{ route('imagecache', ['template' => 'org', 'filename' => $attachment->file]) }}"
                                       data-toggle="lightbox"
                                       data-title="Original">
                                        <img src="{{ route('imagecache', ['template' => 'lre', 'filename' => $attachment->file]) }}"
                                             width="600"
                                             class="img-responsive img-thumbnail"/>
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Custom Fields
                </div>
                <div class="panel-body" id="custom-fields">
                    {!! form_rest($form) !!}
                </div>
            </div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>
    </div>
    <div class="row">
        <div class="btn-fixed">
            <button name="action" value="save" type="submit" class="btn btn-default">Speichern</button>
            @if($license->status !== "ACCEPTED")
                <button name="action" value="accept" type="submit" class="btn btn-success">
                    <span class="fa fa-check fa-fw"></span> Akzeptieren
                </button>
            @endif
            @if($license->status !== "REJECTED")
                <button name="action" value="reject" type="submit" class="btn btn-danger">
                    <span class="fa fa-times fa-fw"></span> Ablehnen
                </button>
            @endif
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $(document).ready(function () {
            $("#license_type").select2().on("select2:select", function (e) {
                // remove previous fields
                $('#custom-fields div.form-group').remove();
                $('#attachments').empty();

                $.get('{{ route('admin::super:license.type.attributes') }}' + '/' + $(this).val(), function (data) {
                    // add license based fields
                    $.each(data, function (attrName, attrVal) {
                        // attachments
                        if (attrName == 'attachments' && attrVal.length > 0) {
                            for (var a = 0; a < attrVal.length; a++) {
                                $('#custom-fields').append(''
                                    + '<div class="form-group">'
                                    + ' <label for="attach-' + a + '" class="control-label">'
                                    + attrVal[a]
                                    + ' </label>'
                                    + ' <input class="form-control" name="' + attrVal[a] + '" type="file">'
                                    + '</div>'
                                );
                            }
                            // fields
                        } else if (attrVal === Object(attrVal) && !Array.isArray(attrVal)) {
                            for (var f in attrVal) {
                                if (attrVal[f] == 'date') {
                                    $('#custom-fields').append(''
                                        + '<div class="form-group" style="padding-right: 0;">'
                                        + ' <label for="' + f + '" class="control-label">' + f + '</label>'
                                        + ' <input class="form-control" name="' + f +
                                        '" type="text"  data-provide="datepicker">'
                                        + '</div>'
                                    );
                                } else {
                                    $('#custom-fields').append(''
                                        + '<div class="form-group" style="padding-right: 0;">'
                                        + ' <label for="' + f + '" class="control-label">' + f + '</label>'
                                        + ' <input class="form-control" name="' + f + '" type="text">'
                                        + '</div>'
                                    );
                                }
                            }
                        }
                    });
                });
            });
        });
    </script>
@endpush