@extends('admin.layout.manager')

    @if($area->id)
        @include('admin.common.draft', [
            'drafted' => $area->draft,
            'publish' => route('admin::super:areas.publish', ['poiId' => $area->id]),
            'delete' => route('admin::super:areas.delete', ['poiId' => $area->id]),
            'list' => route('admin::super:areas.index')])
    @endif

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:areas.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Gewässer
                @if($area->id)
                    bearbeiten<br/><small>{{ $area->name }}</small>
                @else
                    erstellen
                @endif

            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            {!! form_row($form->public) !!}
                        </div>
                        <div class="col-md-4">
                            {!! form_row($form->lease) !!}
                        </div>
                        <div class="col-md-4">
                            {!! form_row($form->boat) !!}
                        </div>
                        <div class="col-md-4">
                            {!! form_row($form->member_only) !!}
                        </div>
                        <div class="col-md-4">
                            {!! form_row($form->phone_ticket) !!}
                        </div>
                        <div class="col-md-4">
                            {!! form_row($form->nightfishing) !!}
                        </div>
                    </div>
                    {!! form_row($form->name) !!}
                    <div class="row">
                        <div class="col-md-6">
                            {!! form_row($form->type) !!}
                        </div>
                        <div class="col-md-6">
                            {!! form_row($form->manager) !!}
                        </div>
                    </div>

                    {!! form_row($form->rods_max) !!}
                    <div class="row">
                        <div class="col-md-6">
                            {!! form_row($form->season_begin) !!}
                        </div>
                        <div class="col-md-6">
                            {!! form_row($form->season_end) !!}
                        </div>
                    </div>
                    {!! form_row($form->description) !!}
                    {!! form_row($form->borders) !!}
                    <div class="form-group">
                        <label for="{{ $form->ticket_info->getName() }}" class="control-label">
                            {{ $form->ticket_info->getOption('label') }}
                            <a href="{{ asset('img/admin/area_text_ref.jpg') }}" data-title="Text auf Angelkarte (links)" data-toggle="lightbox">
                                <span class="fa fa-info-circle fa-fw"></span>
                            </a>
                        </label>
                        {!! form_widget($form->ticket_info) !!}
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Suchbegriffe
                </div>
                <div class="panel-body">
                    @if($area->tags()->count() > 0)
                        <div class="form-group">
                            <label>Automatisch</label>
                            <div class="row">
                                @foreach($area->tags()->where('custom', '=', 0)->orderBy('weight', 'desc')->get() as $tag)
                                    <div class="col-sm-6">
                                        <span class="fa fa-circle-o fa-fw"></span>
                                        {{ $tag->tag }} ({{ $tag->weight }})
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    {!! form_row($form->custom_tags) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Bestimmungen
                </div>
                <div class="panel-body">
                    <div class="alert alert-info">
                        @include('admin.partials.rules.general')
                    </div>
                    {!! form_row($form->rule_text) !!}
                    <div class="form-group">
                        {!! form_label($form->rule_files_public) !!}
                        <div class="help-block">
                            @include('admin.partials.rules.public')
                        </div>
                        {!! form_widget($form->rule_files_public) !!}
                    </div>
                    @foreach($errors->getMessages() as $error => $messages)
                        @if(starts_with($error, 'rule_files_public.'))
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($messages as $msg)
                                        <li>{{ $msg }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    @endforeach
                    <div class="form-group">
                        {!! form_label($form->rule_files) !!}
                        <div class="help-block">
                            @include('admin.partials.rules.private')
                        </div>
                        {!! form_widget($form->rule_files) !!}
                    </div>
                    @foreach($errors->getMessages() as $error => $messages)
                        @if(starts_with($error, 'rule_files.'))
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($messages as $msg)
                                        <li>{{ $msg }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    @endforeach
                    <div class="form-group">
                        <label>Vorhandene Dateien</label>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Datei</th>
                                    <th>Öffentlich</th>
                                    <th>Löschen</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($filteredStates = $area->states->filter(function ($state) {
                                            return $state->rule && $state->rule->files()->count() > 0;
                                        })
                                    )
                                    @foreach($filteredStates as $filteredState)
                                        @foreach($filteredState->rule->files as $file)
                                            <tr style="background: #dfe8f7">
                                                <td>
                                                    {!! Html::linkRoute('filestream', $file->fileName, ['name' => $file->filePathWithName], ['target' => '_blank']) !!}
                                                </td>
                                                <td>
                                                    <span class="fa {{ $file->pivot->public ? 'fa-check' : 'fa-times' }} fa-fw"></span>
                                                </td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                @endif
                                @if($area->rule && $area->rule->files()->count() > 0)
                                    @foreach($area->rule->files as $file)
                                        <tr>
                                            <td>
                                                {!! Html::linkRoute('filestream', $file->fileName, ['name' => $file->filePathWithName], ['target' => '_blank']) !!}
                                            </td>
                                            <td>
                                                <span class="fa {{ $file->pivot->public ? 'fa-check' : 'fa-times' }} fa-fw"></span>
                                            </td>
                                            <td>
                                                {!! Form::checkbox('rule_files_delete[]', $file->id) !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3" class="text-center">
                                            Keine Dateien vorhanden
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Fisch & Technik
                </div>
                <div class="panel-body">
                    {!! form_row($form->techniques) !!}
                    <label>Zielfische</label>
                    <div class="fishes-container" data-prototype="{{ form_row($form->fishes->prototype()) }}">
                        {!! form_row($form->fishes) !!}
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <select class="form-control" id="fish">
                                @foreach($fishes as $id => $name)
                                    <option value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary" id="fishes-btn-add">
                                    <span class="fa fa-plus fa-fw"></span> Fisch hinzufügen
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Geodaten
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>Standort/Grenzen (Latitude,Longitude)</label>
                        <div class="locations-container" data-prototype="{{ form_row($form->locations->prototype()) }}">
                            {!! form_row($form->locations) !!}
                        </div>
                        <button type="button" class="btn btn-primary" id="locations-btn-add">
                            <span class="fa fa-plus fa-fw"></span> Standort hinzufügen
                        </button>
                    </div>
                    {!! form_row($form->cities) !!}
                    {!! form_row($form->polyfield) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#ticket_info,#description,#borders,#rule_text').summernote({
            lang: 'de-DE',
            height: '150px',
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['link', ['linkDialogShow', 'unlink']],
                ['misc', ['fullscreen', 'codeview', 'undo', 'redo', 'reset']]
            ],
            callbacks: {
                onImageUpload: function(files) {
                    return false;
                }
            }
        });

        $('#techniques').chosen({
            width: '100%'
        });

        $('#custom-tags').select2({
            width: '100%',
            tags: true,
            multiple: true,
            minimumInputLength: 3,
            tokenSeparators: [',', ' ']
        });

        var container = $('.fishes-container');
        var fishSelect = $('#fish');
        var fishCount = container.children('.form-group').children().length;

        function onRemoveClicked(e) {

        }

        $('.locations-btn-remove').on('click', function(e) {
            e.preventDefault();
            var group = $(this).closest('.form-group');
            var id = group.data('id');

            $('#fish option[value="' + id + '"]').prop('disabled', false);

            group.remove();
        });

        $('#locations-btn-add').on('click', function(e) {
            e.preventDefault();
            var container = $('.locations-container');
            var count = container.children().length;
            var proto = container.data('prototype').replace(/__NAME__/g, count);
            container.append(proto);

            $('.locations-btn-remove').on('click', onRemoveClicked);
        });

        $('.fish-id').each(function(index, element) {
            $('#fish option[value="' + element.value + '"]').prop('disabled', true);
        });

        function onRemoveFishClicked(e) {
            e.preventDefault();
            var group = $(this).closest('.form-group');
            var id = group.find('.fish-id').val();

            $('#fish option[value="' + id + '"]').prop('disabled', false);

            group.remove();
        }

        $('.fishes-btn-remove').on('click', onRemoveFishClicked);

        $('#fishes-btn-add').on('click', function(e) {
            e.preventDefault();

            var selectedFish = fishSelect.find(':selected');
            var fishId = fishSelect.val();

            if (selectedFish.prop('disabled')) {
                return false;
            }

            var fishName = selectedFish.text();
            var proto = container.data('prototype')
                    .replace(/__FISH_ID__/g, fishId)
                    .replace(/__NAME__/g, fishCount++)
                    .replace(/__FISH__/g, fishName);
            container.children('.form-group').append(proto);

            fishSelect.find(':selected').prop('disabled', true);

            $('.fishes-btn-remove').on('click', onRemoveFishClicked);
        });

        $(".rule_files").change(function(){
            $("input[name^=rule_files_name]").each(function(){
                $(this).prev('label').remove();
                $(this).remove();
            });
            var names = [];
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                names.push($(this).get(0).files[i].name);
                // console.log($(this).get(0).files[i].name);
                $("<div class='form-group'>"
                    + "<label for='Dateiname' class='control-label'>Dateiname: " + $(this).get(0).files[i].name + "</label>"
                    + "<input class='form-control' "
                    + "       pattern=\"[a-zA-Z\\s]+\""
                    + "       type=\"text\""
                    + "       name=\"rule_files_name[" + $(this).get(0).files[i].name + "]\" />"
                + "</div>").insertAfter($(this).parent());
            }
        });

        $(".rule_files_public").change(function(){
            $("input[name^=rule_public_files_name]").each(function(){
                $(this).prev('label').remove();
                $(this).remove();
            });
            var names = [];
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                names.push($(this).get(0).files[i].name);
                // console.log($(this).get(0).files[i].name);
                $("<div class='form-group'>" +
                    + "<label for='Dateiname' class='control-label'>Dateiname: " + $(this).get(0).files[i].name + "</label>"
                    + "<input class='form-control'"
                    + "       pattern=\"[a-zA-Z\\s]+\""
                    + "       type=\"text\" name=\"rule_public_files_name[" + $(this).get(0).files[i].name + "]\" />"
                + "</div>").insertAfter($(this).parent());
            }
        });
    });
</script>
@endpush