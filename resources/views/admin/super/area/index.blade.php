@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Gewässer
                <div class="pull-right">
                    <a href="{{ route('admin::super:areas.create') }}" class="btn btn-success"><span class="fa fa-plus fa-fw"></span> Gewässer anlegen</a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="manager">Bewirtschafter</label>
                        <select class="form-control" name="manager" id="filter-manager">
                            @foreach($managerList as $managerId => $managerName)
                                <option
                                        value="{{ $managerId }}"
                                        @if($managerId == $manager)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $managerName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="client">Client</label>
                        <select class="form-control" name="client" id="filter-client">
                            @foreach($clientList as $clientId => $clientName)
                                <option
                                        value="{{ $clientId }}"
                                        @if($clientId == $client)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $clientName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-areas">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Öffentlich</th>
                    @if($children)<th>Bewirtschafter</th>@endif
                    <th>Verkaufsstellen</th>
                    <th>Koppelrecht</th>
                    <th>Angelkarten</th>
                    <th>Galerie</th>
                    <th>Clients</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- client modal --}}
    <div class="modal fade in" id="client-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            <span class="fa fa-group fa-fw"></span> Clients
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select class="form-control" id="clients" name="clients[]" multiple="multiple"></select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- additional managers modal --}}
    <div class="modal fade in" id="am-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>

                    <h4 class="modal-title pull-left" style="display: inline-block">
                        <span class="fa fa-briefcase fa-fw"></span> Bewirtschafter
                    </h4>
                    <span class="alert alert-danger text-center am-percentage-error" role="alert">
                        <span class="glyphicon glyphicon-remove"></span> <strong>Achtung:</strong> Die Summe der Anteile ist nicht gleich 100%
                    </span>
                    <span class="alert alert-success text-center am-saved" role="alert">
                        <strong><span class="glyphicon glyphicon-ok"></span> Erfolgreich aktualisiert</strong>
                    </span>
                </div>
                <div class="modal-body">
                    <div id="am-table-loader" style="text-align: center;">
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <span class="sr-only">Loading...</span>
                    </div>
                    <div id="am-table-wrapper" style="display: none;">
                        <table class="table table-bordered table-hover dataTable no-footer nowrap" id="am-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Öffentlich</th>
                                    <th>Anteil, %</th>
                                    <th>Aktionen</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-sm pull-right am-create">
                        <span class="fa fa-plus fa-fw"></span>
                        Bewirtschafter hinzufügen
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {

        var managersList,
            areaId;

        function updateFilters() {
            var location = '{{ route('admin::super:areas.index') }}',
                params = [],
                manager = $('#filter-manager').val(),
                client = $('#filter-client').val();

            if (manager > 0) {
                params.push('manager=' + manager);
            }
            if (client >= 0) {
                params.push('client=' + client);
            }
            $table.state.clear();

            window.location = location + '{{ $children ? '?affiliate=1' : '?'}}' + params.join('&');
        }

        $('#filter-manager').on('change', updateFilters);
        $('#filter-client').on('change', updateFilters);

        var queryString = window.location.search.replace(/^\?/, '');
        var $table = $('#datatable-areas').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::super:areas.data') }}{{ $children ? '?affiliate=1&' : '?'}}' + queryString,
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'name' },
                { data: 'public', sortable: false },
                @if($children) { data: 'manager', searchable: true, sortable: false }, @endif
                { data: 'resellers', searchable: false, sortable: false },
                { data: 'additional_managers', searchable: false, sortable: false },
                { data: 'ticket_types', searchable: false, sortable: false },
                { data: 'gallery', searchable: false, sortable: false },
                { data: 'clients', searchable: true, sortable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        var additionalManagersTable = $('#am-table').DataTable({
            stateSave: true,
            processing: true,
            searching: false,
            pagingType: "full_numbers",
            bPaginate: false,
            bInfo : false,
            columns: [
                { data: 'id', searchable: false, sortable: false },
                { data: 'manager', searchable: false, sortable: false },
                { data: 'public', searchable: false, sortable: false },
                { data: 'percentage', searchable: false, sortable: false },
                { data: 'actions', searchable: false, sortable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#client-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var formAction = button.data('action');

            var modal = $(this);
            modal.find('.form').attr('action', formAction);

            var clientSelect = modal.find('#clients');
            var clientData = [];

            clientSelect.children().remove();

            $.getJSON(formAction).done(function (data) {
                $.each(data.data, function (index, client) {
                    clientSelect.append($('<option/>', {
                        value: client.id,
                        text: client.name,
                        selected: true
                    }));
                });
            }).done(function () {
                clientSelect.select2({
                    width: '100%',
                    data: clientData,
                    ajax: {
                        url: '{{ route('admin::super:areas.clients.all') }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term,
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.data,
                                pagination: {
                                    more: params.page < data.meta.pagination.total_pages
                                }
                            };
                        },
                        cache: true
                    },
                    templateResult: function (client) {
                        if (client.loading) return client.name;

                        return "<div class='select2-result-admin'>" +
                                "<div class='select2-result-admin__name'>" + client.name + " (" + client.id + ")</div>";
                    },
                    templateSelection: function (client) {
                        return client.text || client.name;
                    },
                    escapeMarkup: function (markup) { return markup; }
                });
            });
        });

        $('#am-modal').on('show.bs.modal', function(e) {

            areaId = $(e.relatedTarget).data('area-id');

            // retrieve data for managers list
            if(typeof managersList === 'undefined') {
                $.get({
                    'url': "{{ route('admin::super:managers.additional') }}",
                    'success' : function(data) {
                        managersList = data;
                    }
                });
            }

            $.get({
                'url': $(e.relatedTarget).data('action'),
                'success' : function(data) {
                    // populate table
                    additionalManagersTable.clear().draw();
                    additionalManagersTable.rows.add(data.data).draw();

                    // loader animation
                    $('#am-table-loader').fadeOut('slow', function() {
                        // fill additional managers list
                        drawAdditionalManagersList('.am-list', managersList);
                        $('#am-table-wrapper').fadeIn("slow");
                    });

                }
            });
        });

        $('#am-modal').on('hide.bs.modal', function(e) {
            $('.am-create').removeAttr('disabled');
        });

        $(document).on('click', '[class*="am-"]', function (e) {
            var button = $(this),
                action = button.data('action'),
                method = button.data('method');

            // edit
            if(button.attr('class').indexOf('am-edit') !== -1) {
                updateAdditionalManagers($(this));
            }

            // delete
            if(button.attr('class').indexOf('am-delete') !== -1) {
                updateAdditionalManagers($(this), true);

                // reload main table to update managers counter
                $table.ajax.reload(null, false);
            }

            // create
            if(button.attr('class').indexOf(' am-create') !== -1) {

                var amCreateUrl = '{{ route("admin::super:create.areas.additional.managers", ":id") }}';
                amCreateUrl = amCreateUrl.replace(':id', areaId);

                // draw additional row
                additionalManagersTable.row.add( {
                    id:"",
                    manager: "<select class='am-list-create form-control'></select>",
                    public: "<input type='checkbox' class='am-public'>",
                    percentage: "<input type='text' class='am-percentage form-control' data-old-value=0>",
                    actions: "<button type='button' data-action='" + amCreateUrl + "' data-method='POST' class='btn btn-xs btn-success confirm-am-create'>Speichern<span class='fa fa-save fa-fw'></span></button>" +
                    " <button type='button' class='btn btn-xs btn-warning cancel-am-create'>Stornieren<span class='fa fa-times fa-fw'></span></button>"
                } ).draw(false);

                    // reorder table to add new row at the beginning
                    additionalManagersTable.order([0, 'desc']).draw();
                    button.attr('disabled','disabled');

                    // fill additional managers list
                    managersList.unshift({id: "", text: ""});
                    drawAdditionalManagersList('.am-list-create', managersList);


                // creation cancelled
                $('.cancel-am-create').on('click', function (e) {
                    // remove added row
                    additionalManagersTable.row($(this).closest('tr')).remove().draw( false );
                    button.removeAttr('disabled');
                });

                // creation confirmed
                $('.confirm-am-create').on('click', function (e) {
                    if(updateAdditionalManagers($(this))) {
                        button.removeAttr('disabled');
                        // reload main table to update managers counter
                        $table.ajax.reload(null, false);
                    }
                });
            }
        });

        /**
         * Collect and validate data and performs create, update and delete actions
         * @param button
         * @param del
         */
        function updateAdditionalManagers(button, del) {

            var del = typeof del !== 'undefined' ? del : false,
            container = button.closest('tr');
            data;

            // hide previous alert
            container.closest('.modal').find('.alert').hide();
            // disable action button to prevent double click
            button.attr('disabled','disabled');

            if(!del) {
                var manager = container.find('[class*=am-list]'),
                    managerVal = manager.val() ? manager.val() : manager.data('am-id'),
                    percentage = container.find('.am-percentage'),
                    percentageVal = percentage.val().replace(/,/g, ".");
                    totalPercentage = 0;

                // calculate total percentage value by all manager's for current area
                $('.am-percentage').each(function () {
                    totalPercentage += +($(this)[0] == percentage[0] ? percentageVal : $(this).data('old-value'));
                });

                // validate data before submit
                percentage.closest('td').removeClass('has-error');
                manager.closest('td').removeClass('has-error');
                $('.am-percentage-error').hide();

                if(!managerVal || !percentageVal || totalPercentage > 100) {
                    button.removeAttr('disabled');

                    if(!managerVal) {
                        manager.closest('td').addClass('has-error')
                    }

                    if(!percentageVal || totalPercentage > 100) {
                        percentage.closest('td').addClass('has-error');

                        if(totalPercentage > 100) {
                            $('.am-percentage-error').fadeIn(100).delay(2000).fadeOut("fast");
                        }

                    }
                    return false;
                }


                var data = {
                    manager_id: managerVal,
                    public: container.find('.am-public').is(":checked") ? 1 : 0,
                    percentage: percentageVal
                };
            }

            $.ajax({
                type: button.data('method'),
                url: button.data('action'),
                dataType: "json",
                data: data,
                success: function( data ) {
                    // populate table
                    additionalManagersTable.clear().draw();
                    additionalManagersTable.rows.add(data.data).draw();
                    // loader animation
                    $('#am-table-loader').fadeOut('slow', function() {
                        $('.am-create').removeAttr('disabled');
                        $('#am-table-wrapper').fadeIn("slow");
                        $('.am-saved').fadeIn("fast").delay(2000).fadeOut("fast");

                        drawAdditionalManagersList('.am-list', managersList);
                    });
                }

            });
        return true;
        }

        /**
         * Draw additional managers list
         * @param container
         * @param data
         */
        function drawAdditionalManagersList(container, data) {

            // fill additional managers list
            $(container).select2({
                data: data,
                dropdownParent: $(container).closest('.modal')
            });

            var amList = $(container);

            // exclude default manager - set as disabled
            $(container + ' [value="' + areaId + '"]').attr('disabled','disabled');

            if(amList.first().data('am-id')) {
                // fill all lists
                amList.each(function (i) {
                    // fill all lists with additional and default managers
                    $(this).val($(this).data('am-id')).trigger('change');
                    // disable already selected additional managers to prevent duplicates
                    $(container + ' [value="' + $(this).data('am-id') + '"]').attr('disabled','disabled');
                });
            } else {
                // create action: empty list with selected additional and default managers
                $('.am-list').each(function (i) {
                    // disable already selected additional managers to prevent duplicates
                    amList.find('[value="' + $(this).data('am-id') + '"]').attr('disabled','disabled');
                });
            }

            amList.select2({
                dropdownParent: $(container).closest('.modal')
            });
        }
    });

</script>
@endpush

@push('styles')
    <style>
        .am-list,
        .am-list-create {
            width: 300px !important;
        }
        .has-error .select2-selection {
            border-color: rgb(185, 74, 72) !important;
        }
        .form-control {
            padding-top: 3px;
            padding-bottom: 3px;
            height: auto;
        }
        #am-modal .alert {
            display: none;
            padding: 5px 15px;
            margin: 0;
        }
    </style>
@endpush
