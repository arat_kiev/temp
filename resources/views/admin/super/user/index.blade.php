@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Benutzer
                <div class="pull-right">
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapsedData"
                            aria-expanded="true" aria-controls="collapseExample">
                        Daten exportieren & Statistiken
                    </button>
                </div>
            </h1>
        </div>
    </div>

    <div class="collapse" id="collapsedData">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">

                    <form class="form-horizontal" id="export-users">
                        <fieldset>
                            <legend>Daten exportieren
                                <span class="pull-right" id="export-error" style="color: red;display: none;">Keine Daten zum Exportieren</span>
                            </legend>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="email">E-Mail-Adresse</label>
                                <div class="col-md-8">
                                    <input id="email" name="emai2l" placeholder="%mail.example.com%"
                                           class="form-control input-md" type="text">
                                </div>
                                <label class="col-md-4 control-label" for="activeDefault">Aktiv</label>
                                <div class="col-md-8">
                                    <label class="radio-inline" for="activeDefault">
                                        <input name="active" id="activeDefault" value="" checked="checked" type="radio">
                                        Alle
                                    </label>
                                    <label class="radio-inline" for="activeTrue">
                                        <input name="active" id="activeTrue" value="1" type="radio">
                                        Aktiv
                                    </label>
                                    <label class="radio-inline" for="activeFalse">
                                        <input name="active" id="activeFalse" value="0" type="radio">
                                        Nicht aktiv
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="balance">Guthaben</label>
                                <div class="col-md-8">
                                    <select name="compareBalance" class="form-control"></select>
                                    <input id="balance" name="balance" placeholder="Guthaben"
                                           class="form-control input-md pull-right part-width" type="text"
                                           onkeypress='return event.charCode >= 48 && event.charCode <= 57' disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="tickets">Angelkarten im Zeitraum</label>
                                <div class="col-md-8">
                                    <div class="input-group tickets">
                                        <input type="text" name="ticket_from" placeholder="Von"
                                               class="form-control datepicker" id="tickets">
                                        <div class="input-group-addon">-</div>
                                        <input type="text" name="ticket_till" placeholder="Bis"
                                               class="form-control datepicker">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="birthday">Geburtstag</label>
                                <div class="col-md-8">
                                    <select name="compareBirthday" class="form-control"></select>
                                    <input id="birthday" name="birthday" placeholder="Geburtstag"
                                           class="form-control input-md pull-right part-width datepicker" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="activity">Letzte Aktivität</label>
                                <div class="col-md-8">
                                    <select name="compareActivity" class="form-control"></select>
                                    <input id="activity" name="activity" placeholder="Datum"
                                           class="form-control input-md pull-right part-width datepicker" type="text">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="select-license">Berechtigung</label>
                                <div class="col-md-8">
                                    <div id="select-license"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 pull-right">
                                    <button type="button" class="btn btn-primary btn-block" id="export">
                                        <span class="fa fa-cloud-download"></span> Exportieren
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>

                <div class="col-md-6">
                    <form class="form-horizontal">
                        <fieldset>
                            <legend>Statistiken</legend>

                            <div class="form-group well well-sm text-center">
                                <label class="col-md-12" for="totalUsers">Registrierte User</label>
                                <h2 id="totalUsers">0</h2>
                            </div>

                            <div class="form-group well well-sm text-center">
                                <label class="col-md-12" for="usersWithBalance">Anzahl User mit
                                    Guthaben</label>
                                <h3 id="usersWithBalance">0</h3>
                            </div>

                            <div class="form-group well well-sm text-center">
                                <label class="col-md-12" for="totalBalance">Gesamtes Guthaben</label>
                                <h3 id="totalBalance">0</h3>
                                </div>
                                <div class="form-group well well-sm text-center">
                                <label class="col-md-12" for="usersPerPeriod">Neue User</label>
                                <div class="col-md-6">
                                    <select name="usersStats" id="usersPerPeriod" class="form-control">
                                        <option value="24 hour">letzte 24h</option>
                                        <option value="72 hour">letzte 72h</option>
                                        <option value="1 week">letzte Woche</option>
                                        <option value="1 month">letztes Monat</option>
                                        <option value="1 year">letztes Jahr</option>
                                    </select>
                                    <select id="usersStatus" name="usersStatusStats" class="form-control">
                                        <option value="all">gesamt</option>
                                        <option value="active">davon aktiv</option>
                                    </select>
                                </div>

                                <h3 id="usersPeriod">0</h3>
                            </div>

                            <div class="form-group well well-sm text-center">
                                <label class="col-md-12" for="usersWithLicenses">User mit
                                    Berechtigungen</label>
                                <h3 id="usersWithLicenses">0</h3>
                            </div>
                            <div class="form-group well well-sm text-center">
                                <label class="col-md-12" for="ticketsPerPeriod">Anzahl User mit Käufen in</label>
                                <div class="col-md-6">
                                    <select name="ticketsStats" id="ticketsPerPeriod" class="form-control label label-info">
                                        <option value="day">Tagen</option>
                                        <option value="month">Monaten</option>
                                        <option value="year">Jahren</option>
                                    </select>
                                    <input id="ticketsPerPeriodValue" name="ticketsPerPeriodValue"
                                           placeholder="number"
                                           value=5 class="form-control"
                                           type="text"
                                           onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                </div>
                                <h3 id="ticketsPeriod">0</h3>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row main-content">
        <div class="col-lg-12">
            <table class="table table-bordered table-hover dataTable no-footer nowrap" id="datatable-users">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Vorname</th>
                    <th>Nachname</th>
                    <th>Fischer ID</th>
                    <th>Zuletzt aktiv</th>
                    <th>Rollen</th>
                    <th>Aktionen</th>
                    <th>Gekaufte Angelkarten</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- role modal --}}
    <div class="modal fade in" id="role-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false"
         style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            <span class="fa fa-group fa-fw"></span> Rollen
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select class="form-control" id="roles" name="roles[]" multiple="multiple"></select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- password reset confirmation modal --}}
    <div class="modal fade in" id="password-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="POST">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            Sicher?
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Bist du sicher, dass du das Passwort für diesen Benutzer zurücksetzen willst?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">
                            <span class="fa fa-check fa-fw"></span> Zurücksetzen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- delete user confirmation modal --}}
    <div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="POST">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            Sicher?
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Bist du sicher, dass du diesen Benutzer löschen willst?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">
                            <span class="fa fa-check fa-fw"></span> Löschen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- history of bought tickests modal --}}
    <div class="modal fade in" id="ticket-history-modal" tabindex="-1" role="dialog"aria-hidden="false" style="display: none; max-height: calc(100vh - 10px);">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                    <h4 class="modal-title">
                        </span> Angelkartenkäufe
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="ticket-history-table-loader" style="text-align: center;">
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <span class="sr-only">Loading...</span>
                    </div>
                    <div id="ticket-history-table-wrapper" style="display: none;">
                        <table class="table table-bordered table-hover dataTable no-footer nowrap" id="ticket-history-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Gekauft am</th>
                                <th>Typ</th>
                                <th>Gewässer</th>
                                <th>Preis</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function () {
        $('#datatable-users').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::super:users.data') }}',
            pagingType: "full_numbers",
            columns: [
                {data: 'id'},
                {data: 'name'},
                {data: 'email'},
                {data: 'first_name'},
                {data: 'last_name'},
                {data: 'fisher_id'},
                {data: 'last_activity_at'},
                {data: 'roles', searchable: false, sortable: false},
                {data: 'actions', searchable: false, sortable: false},
                {data: 'history', searchable: false, sortable: false}

            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        var ticketHistoryTable = $('#ticket-history-table').DataTable({
            stateSave: true,
            processing: true,
            searching: false,
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'date', searchable: false, sortable: false },
                { data: 'type', searchable: false, sortable: false },
                { data: 'area', searchable: false, sortable: false },
                { data: 'price', searchable: false, sortable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#role-modal').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var formAction = button.data('action');

            var modal = $(this);
            modal.find('.form').attr('action', formAction);

            var roleSelect = modal.find('#roles');
            var roleData = [];

            roleSelect.children().remove();

            $.getJSON(formAction).done(function (data) {
                $.each(data.data, function (index, role) {
                    roleSelect.append($('<option/>', {
                        value: role.id,
                        text: role.name,
                        selected: true
                    }));
                });
            }).done(function () {
                roleSelect.select2({
                    width: '100%',
                    data: roleData,
                    ajax: {
                        url: '{{ route('admin::super:users.roles.all') }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term,
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.data,
                                pagination: {
                                    more: params.page < data.meta.pagination.total_pages
                                }
                            };
                        },
                        cache: true
                    },
                    templateResult: function (role) {
                        if (role.loading) return role.name;

                        return "<div class='select2-result-admin'>" +
                                "<div class='select2-result-admin__name'>" + role.name + " (" + role.id + ")</div>" +
                                "<div class='select2-result-admin__address'>" + role.description + "</div>";
                    },
                    templateSelection: function (role) {
                        return role.text || role.name;
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    }
                });
            });
        });

        // Collapsed view
        $('#collapsedData').one('show.bs.collapse', function () {

            /* Stats */

            // Retrieve and fill stats and licenses id
            $.get('{{ route('admin::super:users.stats') }}' + '?' + $.param({
                        // collecting existing data
                        upp: $('#usersPerPeriod').val(),
                        us: $('#usersStatus').val(),
                        tpp: $('#ticketsPerPeriod').val(),
                        tv: $('#ticketsPerPeriodValue').val()
                    }), function (data) {
                // fill stats
                $('#totalBalance').text(data.stats.totalBalance + ' €');
                $('#totalUsers').text(data.stats.totalUsers);
                $('#usersWithLicenses').text(data.stats.totalLicenses);
                $('#usersWithBalance').text(data.stats.usersWithBalance);
                $('#usersPeriod').text(data.stats.usersPerPeriod);
                $('#ticketsPeriod').text(data.stats.ticketsPerPeriod);
                // Fill license select
                $('#select-license').select2({
                    data: data.license,
                    placeholder: 'Berechtigungstyp ID',
                    multiple: true,
                    allowClear: true
                });
            });

            // Init selects for stats
            $(this).find('select[name*=Stats]').each(function () {
                $(this).select2({
                    width: '100%',
                    minimumResultsForSearch: Infinity,
                });
            });

            // Update stats on change
            $('#ticketsPerPeriod').on("select2:select", selectTicketsChanged);
            $('#ticketsPerPeriodValue').on("input", selectTicketsChanged);
            $('#usersPerPeriod, #usersStatus').on("select2:select", selectUsersChanged);


            // Get stats for users
            function selectTicketsChanged() {
                $.get('{{ route('admin::super:users.stats.tickets') }}' + '?' + $.param({
                            tpp: $('#ticketsPerPeriod').val(),
                            tv: $('#ticketsPerPeriodValue').val()
                        }),
                        function (tickets) {
                            $('#ticketsPeriod').text(tickets);
                        });
            }

            // Get stats for tickets
            function selectUsersChanged() {
                $.get('{{ route('admin::super:users.stats.users') }}' + '?' + $.param({
                            upp: $('#usersPerPeriod').val(),
                            us: $('#usersStatus').val()
                        }),
                        function (users) {
                            $('#usersPeriod').text(users);
                        });
            }

            /* Export */

            // Init selects
            $(this).find('select[name*=compare]').each(function () {
                $(this).append('<option value=""></option><option value=">">></option><option value="=">=</option><option value="<"><</option>');
                $(this).select2({
                    width: '19%',
                    minimumResultsForSearch: Infinity,
                    placeholder: '?',
                    allowClear: true
                });
                // enable or disable input depending on select's status
                $(this).on("select2:close", function (e) {
                    $(this).val() ? $(this).nextAll().eq(1).removeAttr('disabled') : $(this).nextAll().eq(1).attr("disabled", "true");
                });
            });

            // Init Datepickers
            $(this).find('.datepicker').each(function (index) {
                $(this).datepicker();
                if ($(this).attr('id') == 'birthday' || $(this).attr('id') == 'activity') {
                    $(this).attr("disabled", "true");
                }
            });

        });

        // Send data to generate an export
        $('#export').click(function () {
            var button = this;
            var form = $('#export-users');
            var license = [];

            // Button animation
            $(button).prop('disabled', true).find('span').addClass('fa-spin');
            // Get selected licenses
            $.each($('#select-license').select2('data'), function (key, value) {
                license.push(value.id);
            });

            $.post('{{ route('admin::super:users.export.create') }}', form.serializeArray().concat({
                name: 'license',
                value: license
            }), function (data) {
                $(button).prop('disabled', false).find('span').removeClass('fa-spin');
                // Download file or show error message (in case if there is no data to return)
                data.file
                        ? window.location = '{{ route('admin::super:users.export.download')  }}' + '/' + data.file
                        : form.find('#export-error').fadeIn("slow"), setTimeout(function () {
                    form.find('#export-error').fadeOut("slow");
                }, 3000);
            });
        });

        $('#delete-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget),
                formAction = button.data('action'),
                formMethod = button.data('method'),
                $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();
                $(document).find('.response-popup').remove();

                $.ajax({
                    'type' : formMethod,
                    'url': formAction,
                    'success' : function(data) {
                        $('#delete-modal').modal('hide');
                        var successBlock = ''
                            + '<div class="alert alert-success alert-dismissible response-popup" role="alert">'
                            + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                            + '        <span aria-hidden="true">&times;</span>'
                            + '    </button>'
                            +       data.message
                            + '</div>';
                        $('.main-content').prepend(successBlock);

                    },
                    'error': function (error) {
                        $('#delete-modal').modal('hide');
                        var errorMsg = error.responseText || 'Es ist ein Fehler aufgetreten';
                        var errorBlock = ''
                            + '<div class="alert alert-danger alert-dismissible response-popup" role="alert">'
                            + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                            + '        <span aria-hidden="true">&times;</span>'
                            + '    </button>'
                            +       errorMsg
                            + '</div>';
                        $('.main-content').prepend(errorBlock);
                    }
                });
            })
        });

        $('#password-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget),
                formAction = button.data('action'),
                formMethod = button.data('method'),
                $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();
                $(document).find('.response-popup').remove();

                $.ajax({
                    'type' : formMethod,
                    'url': formAction,
                    'success' : function(data) {
                        $('#password-modal').modal('hide');
                        var successBlock = ''
                            + '<div class="alert alert-success alert-dismissible response-popup" role="alert">'
                            + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                            + '        <span aria-hidden="true">&times;</span>'
                            + '    </button>'
                            +       data.message
                            + '</div>';
                        $('.main-content').prepend(successBlock);

                        if (data.attemptsExhausted) {
                            var errorBlock = ''
                                + '<div class="alert alert-danger alert-dismissible response-popup" role="alert">'
                                + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                                + '        <span aria-hidden="true">&times;</span>'
                                + '    </button>'
                                + '     Zu viele Zurücksetzungsversuche'
                                + '</div>';
                            $('.main-content').prepend(errorBlock);
                        }
                    },
                    'error': function (error) {
                        $('#password-modal').modal('hide');
                        var errorMsg = error.responseText || 'Es ist ein Fehler aufgetreten';
                        var errorBlock = ''
                            + '<div class="alert alert-danger alert-dismissible response-popup" role="alert">'
                            + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                            + '        <span aria-hidden="true">&times;</span>'
                            + '    </button>'
                            +       errorMsg
                            + '</div>';
                        $('.main-content').prepend(errorBlock);
                    }
                });
            })
        });

        $('#ticket-history-modal').on('show.bs.modal', function(e) {
            $.get({
                'url': $(e.relatedTarget).data('action'),
                'success' : function(data) {
                    // populate table
                    ticketHistoryTable.clear().draw();
                    ticketHistoryTable.rows.add(data.data).draw();
                    // loader animation
                    $('#ticket-history-table-loader').fadeOut('slow', function() {
                        $('#ticket-history-table-wrapper').fadeIn("slow");
                    });

                }
            });
        });

        $('#ticket-history-modal').on('hide.bs.modal', function(e) {
            // loader animation
            $('#ticket-history-table-wrapper').hide();
            $('#ticket-history-table-loader').fadeIn("slow");
        });

    });
</script>
@endpush

@push('styles')
<style>
    .part-width {
        width: 80%;
    }

    .input-group-addon {
        padding: 0;
    }

    .select2-selection__arrow {
        display: none;
    }

    .select2-container .select2-selection--single .select2-selection__rendered {
        padding-right: 3px;
    }

    .select2-container .select2-selection--single {
        height: 34px;
        border: 1px solid #ccc;
    }
</style>
@endpush