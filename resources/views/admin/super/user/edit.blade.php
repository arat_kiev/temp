@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:users.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Benutzer bearbeiten<br/>
                <small>{{ $user->name }} (Fischer ID - {{ $user->fisher_id }})</small>
            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->email) !!}
                    {!! form_row($form->first_name) !!}
                    {!! form_row($form->last_name) !!}
                    {!! form_row($form->street) !!}
                    {!! form_row($form->post_code) !!}
                    {!! form_row($form->city) !!}
                    {!! form_row($form->country) !!}
                    {!! form_row($form->birthday) !!}
                    {!! form_row($form->phone) !!}
                    {!! form_row($form->balance) !!}
                    {!! form_row($form->active) !!}
                    {!! form_row($form->newsletter) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Admin von
                </div>
                <div class="panel-body">
                    <fieldset>
                        <legend>Bewirtschafter</legend>
                        <ul>
                            @foreach($user->managers as $manager)
                                <li>{{ $manager->name }}</li>
                            @endforeach
                        </ul>
                    </fieldset>
                    <fieldset>
                        <legend>Verkaufsstellen</legend>
                        <ul>
                            @foreach($user->resellers as $reseller)
                                <li>{{ $reseller->name }}</li>
                            @endforeach
                        </ul>
                    </fieldset>
                    <fieldset>
                        <legend>Bootsverleih</legend>
                        <ul>
                            @foreach($user->rentals as $rental)
                                <li>{{ $rental->name }}</li>
                            @endforeach
                        </ul>
                    </fieldset>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Berechtigungen
                </div>
                <div class="panel-body">
                    <ul>
                        @foreach($user->licenses as $license)
                            <li>
                                <a href="{{ route('admin::super:licenses.edit', $license->id) }}" target="_blank">{{ $license->type->name }}
                                    @if($license->status === "ACCEPTED")
                                        <span class="fa fa-check"></span>
                                    @elseif($license->status === "PENDING")
                                        <span class="fa fa-question"></span>
                                    @else
                                        <span class="fa fa-times"></span>
                                    @endif
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @if($userTicketsCount)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Angelkartenkäufe {{ $userTicketsCount  }}
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover dataTable no-footer nowrap" id="ticket-history-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Gekauft am</th>
                                <th>Typ</th>
                                <th>Gewässer</th>
                                <th>Preis</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#country').chosen({
             "allow_single_deselect": true
        });

        $('#ticket-history-table').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            searching: false,
            ajax: '{{ route('admin::super:users.tickets.history', ['userId' => $user->id]) }}',
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'date', searchable: false, sortable: false },
                { data: 'type', searchable: false, sortable: false },
                { data: 'area', searchable: false, sortable: false },
                { data: 'price', searchable: false, sortable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });
    });
</script>
@endpush
