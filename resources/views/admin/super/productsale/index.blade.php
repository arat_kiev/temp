@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Verkaufte Produkte
                <small>Gesamt: {{ $count }} ({{ $total }} / {{ $commissionTotal }})</small>
                <div class="pull-right">
                    <a href="{{ route('admin::super:sales.product.storno.index') }}" class="btn btn-info">
                        Stornierte Produkte Verkäufe
                    </a>
                    <div class="dropdown" style="display: inline-block">
                        <button type="button" class="btn btn-default" data-toggle="dropdown">
                            <span class="fa fa-download fa-fw"></span> Export
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="{{ route('admin::super:sales.product.export', [
                                    'format'    => 'xls',
                                    'from'      => $from,
                                    'till'      => $till,
                                    'reseller'  => $reseller,
                                    'invoiced'  => $invoiced,
                                    'status'    => $status,
                                ]) }}">
                                    <span class="fa fa-file-excel-o fa-fw"></span> XLS-Datei
                                </a>
                            </li>
                        </ul>
                    </div>
                    <br>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="reseller">Verkäufer</label>
                        <select class="form-control" name="reseller" id="filter-reseller">
                            @foreach($resellerList as $resellerId => $resellerName)
                                <option
                                        value="{{ $resellerId }}"
                                        @if($resellerId == $reseller)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $resellerName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="invoiced">Abgerechnet</label>
                        <select class="form-control" name="invoiced" id="filter-invoiced">
                            <option value="-1" @if($invoiced == -1) selected="selected" @endif>Alle</option>
                            <option value="0" @if($invoiced == 0) selected="selected" @endif>Nein</option>
                            <option value="1" @if($invoiced == 1) selected="selected" @endif>Ja</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" name="status" id="filter-status">
                            <option value="RECEIVED" @if($status == 'RECEIVED') selected="selected" @endif>Eingegangen</option>
                            <option value="SHIPPING" @if($status == 'SHIPPING') selected="selected" @endif>In Bearbeitung</option>
                            <option value="SHIPPED" @if($status == 'SHIPPED') selected="selected" @endif>Versendet</option>
                        </select>
                    </div>
                    <div class="input-group input-daterange">
                        <div class="input-group-addon input-group-label">Kaufdatum</div>
                        <input type="text"
                               class="form-control"
                               id="date-range-from"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $from)->format('d.m.Y') }}">
                        <span class="input-group-addon">bis</span>
                        <input type="text"
                               class="form-control"
                               id="date-range-till"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $till)->format('d.m.Y') }}">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="button" id="filter-date-range-btn">Filter</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-sales">
                    <thead>
                    <tr>
                        <th></th>
                        <th>#</th>
                        <th>Produkt</th>
                        <th>Anzahl</th>
                        <th>Käufer</th>
                        <th>Gekauft</th>
                        <th>Status</th>
                        <th>Preis</th>
                        <th>Gesamtpreis</th>
                        <th>Abgerechnet</th>
                        <th>Aktionen</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Schließen">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie diese Produkte Verkäufe stornieren wollen?</p>
                        <hr>
                        <div class="form-group">
                            <label for="storno-reason">Beschreibung der Stornierung</label>
                            <textarea class="form-control" id="storno-reason"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Stornieren
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('styles')
<style>
    .input-daterange {
        margin-bottom: 10px;
    }

    @media(min-width: 992px) {
        .input-daterange {
            margin-bottom: -30px;
        }
    }

    .cell-label {
        display: block;
        margin: 5px 10px 0;
    }
</style>
@endpush

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('.input-daterange').datepicker();

        function updateFilters() {
            var from = $('#date-range-from').datepicker('getDate');
            var till = $('#date-range-till').datepicker('getDate');

            var fromString = moment(from).format('YYYY-MM-DD');
            var tillString = moment(till).format('YYYY-MM-DD');

            var reseller = $('#filter-reseller').val();
            var invoiced = $('#filter-invoiced').val();

            var status = $('#filter-status').val();

            var location = '{{ route('admin::super:sales.product.index') }}';
            var params = [
                'from=' + fromString,
                'till=' + tillString
            ];

            if (reseller >= 0) {
                params.push('reseller=' + reseller);
            }

            if (invoiced >= 0) {
              params.push('invoiced=' + invoiced);
            }

            params.push('status=' + status);


            window.location = location + '?' + params.join('&');
        }

        $('#filter-date-range-btn').on('click', updateFilters);
        $('#date-range-from').on('change', updateFilters);
        $('#date-range-till').on('change', updateFilters);
        $('#filter-reseller').on('change', updateFilters);
        $('#filter-invoiced').on('change', updateFilters);
        $('#filter-status').on('change', updateFilters);


        var table = $('#datatable-sales').DataTable({
            bFilter: false,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('admin::super:sales.product.data') }}' + window.location.search
            },
            pagingType: "full_numbers",
            columns: [
                {
                    className: 'details-control',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                { data: 'sale_number', orderable: false },
                { data: 'name', orderable: false },
                { data: 'quantity', orderable: false },
                { data: 'user.full_name', orderable: false },
                { data: 'created_at' },
                { data: 'status', orderable: false },
                { data: 'price.value', orderable: false },
                { data: 'total_price', orderable: false },
                { data: 'invoiced', orderable: false },
                { data: 'actions', orderable: false }
            ],
            order: [[1, 'desc']],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        function format(d) {
            return d.user ? formatDetail(d.user, d) : formatDetail(d, d);
        }

        function formatDetail(user, sale) {
            return '' +
                '<table class="ticket-user-detail">' +
                '   <tr>' +
                '       <th>Vorname:</th>' +
                '       <td>' + (user.first_name || '') + '</td>' +
                '       <th>Nachname:</th>' +
                '       <td>' + (user.last_name || '') + '</td>' +
                '   </tr>' +
                '   <tr>' +
                '       <th>Gekauft:</th>' +
                '       <td>' + sale.created_at + '</td>' +
                '       <th>Brutto-Preis:</th>' +
                '       <td>' + sale.price.value + '</td>' +
                '   </tr>' +
                '   <tr>' +
                '       <th>Straße:</th>' +
                '       <td>' + (user.street || '') + '</td>' +
                '       <th>Ort:</th>' +
                '       <td>' + (user.city || '') + '</td>' +
                '   </tr>' +
                '   <tr>' +
                '       <th>PLZ:</th>' +
                '       <td>' + (user.post_code || '') + '</td>' +
                '       <th>Land:</th>' +
                '       <td>' + (user.country ? user.country.name : sale.country.name) + '</td>' +
                '   </tr>' +
                '   <tr>' +
                '       <th>Status:</th>' +
                '       <td colspan="3">' + sale.status + '</td>' +
                '   </tr>' +
                '   <tr style="display:none">' +
                '       <th>Lager:</th>' +
                '       <td colspan="3">' + sale.stocks + '</td>' +
                '   </tr>' +
                '</table>';
        }

        $('#datatable-sales tbody').on('click', 'td.details-control', function() {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            } else {
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });

        $('#datatable-sales tbody').delegate('select.statuses', 'change', function(e) {
            var choice = $(this).val();
            var stockChoice = $('#datatable-sales tbody').find('select.stocks[data-id='+$(this).data('id')+']').val();
            $(this).closest('td').find('button').removeClass('disabled').attr('disabled', false);

            if (choice === 'SHIPPING' && stockChoice !== undefined) {
                $(this).closest('tr').next().show();
                $(this).closest('td').append(
                    '<p class="label label-warning cell-label">Menge auf das Lager wird automatisch reduziert</p>'
                );
            } else {
                $(this).closest('tr').next().hide();
                $(this).closest('td').find('.label').remove();
            }
        });

        $('#datatable-sales tbody').on('click', 'button.status-change', function(e) {
            e.preventDefault();
            $(this).addClass('disabled').attr('disabled', true);

            var self = this,
                saleId = $(e.target).data('id'),
                route = '{{ route('admin::super:sales.product.status', ['saleId' => '__SALE_ID__']) }}',
                updateRoute = route.replace(/__SALE_ID__/, saleId),
                status = $('#datatable-sales tbody').find('select.statuses[data-id='+saleId+']').val(),
                params = {
                    status: status,
                    stock: (status === 'SHIPPING'
                            ? $('#datatable-sales tbody').find('select.stocks[data-id='+saleId+']').val()
                            : undefined)
                };

            $.ajax({
                type: 'POST',
                url: updateRoute,
                data: params,
                success: function(data) {
                    $('.response-popup').remove();
                    var msgBox = ''
                        + '<div class="row response-popup" style="padding-top: 10px;">'
                        + '    <div class="col-lg-12">'
                        + '        <div class="alert alert-' + data.notification + ' alert-dismissable">'
                        + '            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">'
                                            + "&times;"
                        + '            </button>'
                                        + data.message
                        + '        </div>'
                        + '    </div>'
                        + '</div>';
                    $('.page-header').parents('.row').before(msgBox);
                },
                error: function (err) {
                    console.error(err);
                }
            });
        });

        $('[data-toggle="tooltip"]').tooltip();

        $('#delete-modal').on('show.bs.modal', function(e) {
            var formAction = $(e.relatedTarget).data('action'),
                formMethod = 'POST',
                $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();
                $(document).find('.response-popup').remove();

                $.ajax({
                    type : formMethod,
                    url: formAction,
                    data: { reason: $(document).find('#storno-reason').val() },
                    success : function(data) {
                        $('.response-popup').remove();
                        var msgBox = ''
                            + '<div class="row response-popup" style="padding-top: 10px;">'
                            + '    <div class="col-lg-12">'
                            + '        <div class="alert alert-' + data.notification + ' alert-dismissable">'
                            + '            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">'
                                                + "&times;"
                            + '            </button>'
                                            + data.message
                            + '        </div>'
                            + '    </div>'
                            + '</div>';
                        $('.page-header').parents('.row').before(msgBox);
                        $('#delete-modal').modal('hide');

                        if (data.notification == 'success') {
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }
                    }
                });
            });
        });
    });
</script>
@endpush
