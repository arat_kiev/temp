@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Stornierte Produkte Verkäufe
                <small>Gesamt: {{ $count }} ({{ $total }})</small>
                <div class="pull-right">
                    <a href="{{ route('admin::super:sales.product.index') }}" class="btn btn-info">
                        Verkaufte Produkte
                    </a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="reseller">Verkäufer</label>
                        <select class="form-control" name="reseller" id="filter-reseller">
                            @foreach($resellerList as $resellerId => $resellerName)
                                <option
                                        value="{{ $resellerId }}"
                                        @if($resellerId == $reseller)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $resellerName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group input-daterange">
                        <div class="input-group-addon input-group-label">Stornodatum</div>
                        <input type="text"
                               class="form-control"
                               id="date-range-from"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $from)->format('d.m.Y') }}">
                        <span class="input-group-addon">bis</span>
                        <input type="text"
                               class="form-control"
                               id="date-range-till"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $till)->format('d.m.Y') }}">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="button" id="filter-date-range-btn">Filter</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-sales">
                    <thead>
                    <tr>
                        <th></th>
                        <th>#</th>
                        <th>Produkt</th>
                        <th>Anzahl</th>
                        <th>Käufer</th>
                        <th>Gekauft</th>
                        <th>Storno Datum</th>
                        <th>Verkäufer</th>
                        <th>Preis</th>
                        <th>Preis mit Gebühr</th>
                        <th>Anhang</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('styles')
<style>
    .input-daterange {
        margin-bottom: 10px;
    }

    @media(min-width: 992px) {
        .input-daterange {
            margin-bottom: -30px;
        }
    }
</style>
@endpush

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('.input-daterange').datepicker();

        function updateFilters() {
            var from = $('#date-range-from').datepicker('getDate');
            var till = $('#date-range-till').datepicker('getDate');

            var fromString = moment(from).format('YYYY-MM-DD');
            var tillString = moment(till).format('YYYY-MM-DD');

            var reseller = $('#filter-reseller').val();

            var location = '{{ route('admin::super:sales.product.storno.index') }}';
            var params = [
                'from=' + fromString,
                'till=' + tillString
            ];

            if (reseller >= 0) {
                params.push('reseller=' + reseller);
            }

            window.location = location + '?' + params.join('&');
        }

        $('#filter-date-range-btn').on('click', updateFilters);
        $('#date-range-from').on('change', updateFilters);
        $('#date-range-till').on('change', updateFilters);
        $('#filter-reseller').on('change', updateFilters);

        var table = $('#datatable-sales').DataTable({
            bFilter: false,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('admin::super:sales.product.storno.data') }}' + window.location.search
            },
            pagingType: "full_numbers",
            columns: [
                {
                    className: 'details-control',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                { data: 'sale_number', orderable: false },
                { data: 'name', orderable: false },
                { data: 'quantity', orderable: false },
                { data: 'user.full_name', orderable: false },
                { data: 'created_at' },
                { data: 'storno_date' },
                { data: 'reseller', orderable: false },
                { data: 'price.value', orderable: false },
                { data: 'total_price', orderable: false },
                { data: 'attachment', orderable: false }
            ],
            order: [[1, 'desc']],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        function format(d) {
            return d.user ? formatDetail(d.user, d) : formatDetail(d, d);
        }

        function formatDetail(user, sale) {
            return '' +
                '<table class="ticket-user-detail">' +
                '   <tr>' +
                '       <th>Vorname:</th>' +
                '       <td>' + (user.first_name || '') + '</td>' +
                '       <th>Nachname:</th>' +
                '       <td>' + (user.last_name || '') + '</td>' +
                '   </tr>' +
                '   <tr>' +
                '       <th>Gekauft:</th>' +
                '       <td>' + sale.created_at + '</td>' +
                '       <th>Brutto-Preis:</th>' +
                '       <td>' + sale.price.value + '</td>' +
                '   </tr>' +
                '   <tr>' +
                '       <th>Straße:</th>' +
                '       <td>' + (user.street || '') + '</td>' +
                '       <th>Ort:</th>' +
                '       <td>' + (user.city || '') + '</td>' +
                '   </tr>' +
                '   <tr>' +
                '       <th>PLZ:</th>' +
                '       <td>' + (user.post_code || '') + '</td>' +
                '       <th>Land:</th>' +
                '       <td>' + (user.country ? user.country.name : sale.country.name) + '</td>' +
                '   </tr>' +
                '</table>';
        }

        $('#datatable-sales tbody').on('click', 'td.details-control', function() {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            } else {
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });

        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@endpush