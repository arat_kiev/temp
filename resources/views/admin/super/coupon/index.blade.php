@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Gutscheine <small>Gesamt: {{ $count }}</small>
            </h1>
        </div>
    </div>

    <div class="row main-content">
        <div class="col-lg-12">
            <div class="row" style="margin-bottom: 15px;">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="promo-types">Promotionstyp</label>
                        <select class="form-control" name="promo-types" id="filter-promo-type">
                            @foreach($promoTypeList as $typeId => $typeName)
                                <option
                                        value="{{ $typeId }}"
                                        @if($typeId == $promoType)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $typeName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group input-daterange">
                        <div class="input-group-addon input-group-label">Gültigkeit</div>
                        <input type="text"
                               class="form-control"
                               id="date-range-from"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $from)->format('d.m.Y') }}">
                        <span class="input-group-addon">bis</span>
                        <input type="text"
                               class="form-control"
                               id="date-range-till"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $till)->format('d.m.Y') }}">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="button" id="filter-date-range-btn">Filter</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-coupons">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Code</th>
                        <th>Promotion</th>
                        <th>Öffentlich</th>
                        <th>Bonus</th>
                        <th>Benützt</th>
                        <th>Aktionen</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie dieses Gutschein endgültig löschen wollen?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Löschen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $.putJSON = function(url, data, callback, error) {
            return jQuery.ajax({
                'type' : 'PUT',
                'url': url,
                'data': data,
                'dataType' : 'json',
                'success' : callback,
                'error' : error
            });
        };

        $('.input-daterange').datepicker();

        function updateFilters() {
            var from = $('#date-range-from').datepicker('getDate'),
                till = $('#date-range-till').datepicker('getDate'),
                fromString = moment(from).format('YYYY-MM-DD'),
                tillString = moment(till).format('YYYY-MM-DD'),
                type = $('#filter-promo-type').val(),
                location = '{{ route('admin::super:coupons.index') }}',
                params = [
                    'from=' + fromString,
                    'till=' + tillString
                ];

            if (type.length) {
                params.push('promo_type=' + type);
            }

            window.location = location + '?' + params.join('&');
        }

        $('#filter-date-range-btn').on('click', updateFilters);
        $('#date-range-from').on('change', updateFilters);
        $('#date-range-till').on('change', updateFilters);
        $('#filter-promo-type').on('change', updateFilters);

        var $table = $('#datatable-coupons').DataTable({
            bFilter: false,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::super:coupons.data') }}' + window.location.search,
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'code' },
                { data: 'promotion', sortable: false },
                { data: 'is_public' },
                { data: 'bonus' },
                { data: 'usages', sortable: false },
                { data: 'actions', sortable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('table').on('click', 'td button.code-edit', function (e) {
            e.preventDefault();
            var $elem = $(e.target),
                couponId = $elem.data('id') || $elem.closest('button').data('id');

            $elem.closest('.preview-text').hide();
            $('form[data-id='+couponId+']').show();
        });

        $('table').on('submit', 'form.aui', function (e) {
            e.preventDefault();
            var $elem = $(e.target),
                couponId = $elem.data('id'),
                params = { code: $elem.find('input.code').val() },
                route = '{{ route('admin::super:coupons.update.code', ['couponId' => '__COUPON_ID__']) }}'
                        .replace(/__COUPON_ID__/, couponId);

            $elem.find('input,button').addClass('disabled').attr('disable', true);

            $.putJSON(route, params, function (res) {
                if (res.success) {
                    $elem.find('input,button').removeClass('disabled').attr('disable', false);
                    $table.ajax.reload();
                } else {
                    $elem.find('input').css('background', '#ffc2c2');
                    $elem.find('input,button').removeClass('disabled').attr('disable', false);
                }
            }, function (error) {
                $elem.find('input').css('background', '#ffc2c2');
                $elem.find('input,button').removeClass('disabled').attr('disable', false);
            });
        });

        $('table').on('click', 'button.cancel', function (e) {
            e.preventDefault();
            var $form = $(e.target).closest('form'),
                couponId = $form.data('id');

            $form.hide();
            $('p.preview-text[data-id='+couponId+']').show();
        });

        $('#delete-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget),
                formAction = button.data('action'),
                formMethod = button.data('method'),
                $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();
                $(document).find('.response-popup').remove();

                $.ajax({
                    'type' : formMethod,
                    'url': formAction,
                    'success' : function() {
                        $('#delete-modal').modal('hide');
                        var successBlock = ''
                                + '<div class="alert alert-success alert-dismissible response-popup" role="alert">'
                                + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                                + '        <span aria-hidden="true">&times;</span>'
                                + '    </button>'
                                + '    Gutschein wurde erfolgreich entfernt'
                                + '</div>';
                        $('.main-content').prepend(successBlock);
                        $table.ajax.reload();
                    },
                    'error': function (error) {
                        $('#delete-modal').modal('hide');
                        var errorMsg = error.responseText || 'Es ist ein Fehler aufgetreten';
                        var errorBlock = ''
                                + '<div class="alert alert-danger alert-dismissible response-popup" role="alert">'
                                + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                                + '        <span aria-hidden="true">&times;</span>'
                                + '    </button>'
                                +       errorMsg
                                + '</div>';
                        $('.main-content').prepend(errorBlock);
                    }
                });
            })
        });
    });
</script>
@endpush

@push('styles')
<style>
    td p.preview-text {
        margin: 0;
    }
    td form.aui {
        position: relative;
        display: none;
    }
    td form.aui input.code {
        width: 80% !important;
    }
    td form.aui div.save-options {
        border: none;
        padding: 0;
        margin-right: 20%;
        line-height: 20px;
        background-color: #f0f0f0;
        border-radius: 0 0 3px 3px;
        box-shadow: 0 3px 6px rgba(111,111,111,0.2);
        outline: none;
        position: absolute;
        right: 0;
        top: 100%;
        z-index: 1;
    }
    .save-options .aui-button {
        height: 24px;
        padding: 0 4px;
        color: #505f79;
        background: #f4f5f7;
        border: none;
        outline: none;
        line-height: 1.57142857em;
        box-sizing: border-box;
        border-radius: 3px;
        cursor: pointer;
        display: inline-block;
        font-family: inherit;
        font-size: 14px;
        font-variant: normal;
        font-weight: normal;
        margin: 0;
        text-decoration: none;
        vertical-align: baseline;
        white-space: nowrap;
    }
    /*.save-options .aui-button+.aui-button {*/
        /*margin-left: 3px;*/
    /*}*/
    form.aui .cancel {
        cursor: pointer;
        font-size: 14px;
        display: inline-block;
        vertical-align: baseline;
    }
    .aui-icon {
        background: no-repeat 0 0;
        border: none;
        display: inline-block;
        height: 16px;
        margin: 0;
        padding: 0;
        text-align: left;
        text-indent: -999em;
        vertical-align: text-bottom;
        width: 16px;
    }
    .aui-button .aui-icon {
        color: #505f79;
    }
    .save-options .aui-button .aui-iconfont-success {
        color: #707070;
    }
    .aui-button .aui-icon-small, .aui-button .aui-icon-large {
        vertical-align: text-bottom;
        line-height: 0;
        position: relative;
    }
    .aui-button .aui-icon-small {
        height: 16px;
        width: 16px;
    }

    .aui-icon-small:before, .aui-icon-large:before {
        color: inherit;
        font-family: FontAwesome;
        font-weight: normal;
        -webkit-font-smoothing: antialiased;
        font-style: normal;
        left: 0;
        line-height: 1;
        position: absolute;
        text-indent: 0;
        speak: none;
        top: 50%;
    }
    .aui-iconfont-success:before {
        content: "\f00c";
    }
    .aui-iconfont-close-dialog:before {
        content: "\f00d";
    }
    .aui-icon-small:before {
        font-size: 16px;
        margin-top: -8px;
    }
    .editable-field .save-options .aui-button .aui-iconfont-success::before {
        font-size: 14px;
        left: 1px;
        margin-top: -7px;
    }
</style>
@endpush
