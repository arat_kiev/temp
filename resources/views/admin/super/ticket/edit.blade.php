@extends('admin.layout.default')

@inject ('ticketPrices', 'App\Models\Ticket\TicketPrice')

<?php
$ticketTypePrices = [];
foreach ($ticketPrices::without('translations')->select(['id', 'ticket_type_id'])->get() as $ticketPrice) {
    $ticketTypePrices[$ticketPrice->ticket_type_id][] = $ticketPrice->id;
}

?>

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:tickets.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Angelkarte
                @if($ticket->id)
                    bearbeiten<br/>
                    <small>{{ $ticket->name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->client) !!}
                    {!! form_row($form->type) !!}
                    {!! form_row($form->price) !!}
                    {!! form_row($form->valid_from) !!}
                    {!! form_row($form->valid_to) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Angelzeit
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="checkins-container" data-prototype="{{ form_row($form->checkins->prototype()) }}">
                            {!! form_row($form->checkins) !!}
                        </div>
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary" id="checkin-btn-ad">
                                    <span class="fa fa-plus fa-fw"></span> Angelzeit
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $( document ).ready( function () {
            $( '#price' ).chosen( { width: '100%' } );
            $( '#client' ).chosen( { width: '100%' } );
            $( '#valid_from' ).datetimepicker();
            $( '#valid_to' ).datetimepicker();

            var ticketTypePrices = {!! json_encode($ticketTypePrices) !!};

            // Check if ticket type and ticket price has been correctly set
            var selectedType = parseInt( $( '#type' ).val() ),
                selectedPrice = parseInt( $( '#price' ).val() ),
                isCorrectTypePrice = ticketTypePrices[selectedType].indexOf( selectedPrice ) >= 0,
                hasPrice = $( "#price option:selected" ) && isCorrectTypePrice;

            displayTypePrices( hasPrice );
            $( '#type' ).on( 'change', function () {
                displayTypePrices( false );
            } );

            function displayTypePrices( hasSelectedOption ) {
                $( '#price option' ).prop( "disabled", true ).removeClass( 'active' ).hide();

                var ticketTypeId = $( '#type' ).val(),
                    typePrices = ticketTypePrices[ticketTypeId];

                // Show only states for selected country
                for ( priceIndex in typePrices ) {
                    var priceId = typePrices[priceIndex];
                    $( '#price' ).find( 'option[value="' + priceId + '"]' ).prop( "disabled", false ).addClass( 'active' ).show();
                }

                // Select first option, if select has any selected option
                if ( !hasSelectedOption ) {
                    $( "#price option:selected" ).prop( "selected", false );
                    $( '#price option.active' ).first().attr( 'selected', true ).prop( 'selected', true );
                }

                $( '#price' ).chosen( 'destroy' ).chosen( { width: '100%' } );
                if ( !$( "#price option:selected" ) ) {
                    $( '#price_chosen a.chosen-single span' ).text( '' );
                }
            }
        } );
        $( '.checkin-btn-remove' ).on( 'click', function ( e ) {
            e.preventDefault();
            var group = $( this ).closest( '.form-group' );
            var id = group.data( 'id' );

            $( '#checkins option[value="' + id + '"]' ).prop( 'disabled', false );

            group.remove();
        } );
        $( '#checkin-btn-add' ).on( 'click', function ( e ) {
            e.preventDefault();
            var container = $( '.checkins-container' );
            var count = container.children().length;
            var proto = container.data( 'prototype' ).replace( /__NAME__/g, count );
            container.append( proto );

            $( '.checkin-btn-remove' ).on( 'click', onRemoveClicked );
        } );

    </script>
@endpush
