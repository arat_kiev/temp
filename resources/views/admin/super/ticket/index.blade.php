@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Verkäufe
                <small>Gesamt: {{ $countTickets }} ({{ $sumTickets }} / {{ $commissionTotal }})</small>
                <div class="pull-right">
                    <div class="dropdown">
                        <button type="button" class="btn btn-default" data-toggle="dropdown">
                            <span class="fa fa-download fa-fw"></span> Export
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="{{ route('admin::super:tickets.export', [
                                    'format'    => 'xls',
                                    'from'      => $from,
                                    'till'      => $till,
                                    'reseller'  => $reseller,
                                    'invoiced'  => $invoiced,
                                ]) }}">
                                    <span class="fa fa-file-excel-o fa-fw"></span> XLS-Datei
                                </a>
                            </li>
                        </ul>
                    </div>
                    <br>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row" style="margin-bottom: 15px;">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="reseller">Verkäufer</label>
                        <select class="form-control" name="reseller" id="filter-reseller">
                            @foreach($resellerList as $resellerId => $resellerName)
                                <option
                                    value="{{ $resellerId }}"
                                    @if($resellerId == $reseller)
                                        selected="selected"
                                    @endif
                                >
                                    {{ $resellerName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="invoiced">Abgerechnet</label>
                        <select class="form-control" name="invoiced" id="filter-invoiced">
                            <option value="-1" @if($invoiced == -1) selected="selected" @endif>Alle</option>
                            <option value="0" @if($invoiced == 0) selected="selected" @endif>Nein</option>
                            <option value="1" @if($invoiced == 1) selected="selected" @endif>Ja</option>
                        </select>
                    </div>
                    <div class="input-group input-daterange">
                        <div class="input-group-addon input-group-label">Kaufdatum</div>
                        <input type="text"
                               class="form-control"
                               id="date-range-from"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $from)->format('d.m.Y') }}">
                        <span class="input-group-addon">bis</span>
                        <input type="text"
                               class="form-control"
                               id="date-range-till"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $till)->format('d.m.Y') }}">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="button" id="filter-date-range-btn">Filter</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer" id="datatable-tickets">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Gewässer</th>
                            <th>Verkäufer</th>
                            <th>Fänge</th>
                            <th>Gekauft</th>
                            <th>Preis</th>
                            <th>Provision</th>
                            <th>Abgerechnet</th>
                            <th>Aktionen</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {{-- preview modal --}}
    <div class="modal fade in" id="preview-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                    <h4 class="modal-title">
                        <span class="fa fa-eye fa-fw"></span>
                        <span id="preview-title">Vorschau wird geladen (bitte etwas Geduld)</span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="col-lg-12">
                            <img class="preview-image img-responsive" width="1083px" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
                </div>
            </div>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Schließen"><span>&times;</span></button>
                        <h4 class="modal-title">
                            Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie diese Produkte stornieren wollen?</p>
                        <hr>
                        <div class="form-group">
                            <label for="storno-reason">Beschreibung der Stornierung</label>
                            <textarea class="form-control" id="storno-reason"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Stornieren
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('.input-daterange').datepicker();

        function updateFilters() {
            var from = $('#date-range-from').datepicker('getDate'),
                till = $('#date-range-till').datepicker('getDate'),
                fromString = moment(from).format('YYYY-MM-DD'),
                tillString = moment(till).format('YYYY-MM-DD'),
                resellerId = $('#filter-reseller').val(),
                location = '{{ route('admin::super:tickets.index') }}',
                invoiced = $('#filter-invoiced').val(),
                params = [
                    'from=' + fromString,
                    'till=' + tillString
                ];

            if (resellerId >= 0) {
              params.push('resellerId=' + resellerId);
            }

            if (invoiced >= 0) {
                params.push('invoiced=' + invoiced);
            }

            table.state.clear();

            window.location = location + '?' + params.join('&');
        }

        $('#filter-date-range-btn').on('click', updateFilters);
        $('#date-range-from').on('change', updateFilters);
        $('#date-range-till').on('change', updateFilters);
        $('#filter-reseller').on('change', updateFilters);
        $('#filter-invoiced').on('change', updateFilters);

        var table = $('#datatable-tickets').DataTable({
            bFilter: true,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::super:tickets.data') }}' + window.location.search,
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'user_full_name', sortable: false },
                { data: 'type.area.name', sortable: false },
                { data: 'reseller', sortable: false, searchable: false },
                { data: 'hauls_count', sortable: false, searchable: false },
                { data: 'created_at' },
                { data: 'price.name', sortable: false, searchable: false },
                { data: 'commission_value', orderable: false },
                { data: 'invoiced', sortable: false, searchable: false },
                { data: 'actions', sortable: false, searchable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#preview-modal').on('show.bs.modal', function(e) {
            var modal = $(this);
            var button = $(e.relatedTarget);
            var ticketId = button.data('ticketId');
            var format = 'image';

            var title = modal.find('#preview-title');
            title.html('Vorschau wird geladen <span class="fa fa-cog fa-spin"></span>');

            modal.find('.preview-image')
                .attr('src', '{{ URL::to('/preview/ticket/') }}/' + ticketId + '/' + format)
                .on('load', function() {
                    title.html('Vorschau für Ticket: ' + ticketId);
                });
        });

        $('#delete-modal').on('show.bs.modal', function(e) {
            var formAction = $(e.relatedTarget).data('action'),
                formMethod = 'POST',
                $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();
                $(document).find('.response-popup').remove();

                $.ajax({
                    type : formMethod,
                    url: formAction,
                    data: { reason: $(document).find('#storno-reason').val() },
                    success : function(data) {
                        var msgBox = ''
                            + '<div class="row response-popup" style="padding-top: 10px;">'
                            + '    <div class="col-lg-12">'
                            + '        <div class="alert alert-' + data.notification + ' alert-dismissable">'
                            + '            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">'
                                            + "&times;"
                            + '            </button>'
                                            + data.message
                            + '        </div>'
                            + '    </div>'
                            + '</div>';
                        $('.page-header').parents('.row').before(msgBox);
                        if (data.notification == 'success') {
                            table.ajax.reload();
                        }
                        $('#delete-modal').modal('hide');
                    }
                });
            });
        });
    });
</script>
@endpush
