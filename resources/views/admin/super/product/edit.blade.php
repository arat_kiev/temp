@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:products.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Produkte
                @if($product->id)
                    bearbeiten<br/><small>{{ $product->name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    @if($product->id)
                        <div class="row">
                            <div class="col-md-9">
                                <h4>Preis {{ $product->price ? $product->price->value . ' €' : '-' }}</h4>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ route('admin::super:product_prices.index', ['product' => $product->id]) }}"
                                   class="btn btn-default"
                                   role="button">
                                    Ändern
                                </a>
                            </div>
                        </div>
                        <hr>
                    @endif
                    {!! form_row($form->category) !!}
                    {!! form_row($form->featured_from) !!}
                    {!! form_row($form->featured_till) !!}
                    {!! form_row($form->account_number) !!}
                    {!! form_row($form->max_quantity) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Vorlage
                </div>
                <div class="panel-body">
                    {!! form_row($form->template) !!}
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Daten
                </div>
                <div class="panel-body" id="entity-data">
                    <div class="loading">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        var summernoteConfig = {
            lang: 'de-DE',
            height: '150px',
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['link', ['linkDialogShow', 'unlink']],
                ['misc', ['fullscreen', 'codeview', 'undo', 'redo', 'reset']]
            ],
            callbacks: {
                onImageUpload: function(files) {
                    return false;
                }
            }
        };
        $('#template').summernote(summernoteConfig);

        $('#categories').chosen({
            width: '100%'
        });

        @if($product->id)
            var url = "{{ route('admin::super:products.show', ['productId' => $product->id]) }}";

            $.getJSON(url, [], function (data) {
                var html = '';
                data.translations.forEach(function(item, index) {
                    html += drawData(index, item);
                });
                $('#entity-data').html(html);
                $(document).find('.entity-btn-remove').on('click', onRemoveRow);
                $('#entity-data').prepend(drawAddBlockBtn(--data.translations.length));
                $('#entity-data').find('#add-new-block').on('click', addNewItem);
                $('.translation-row textarea').summernote(summernoteConfig);
            });
        @else
            $('#entity-data').html(drawData(0, []));
            $('#entity-data').prepend(drawAddBlockBtn(0));
            $(document).find('.entity-btn-remove').on('click', onRemoveRow);
            $('#entity-data').find('#add-new-block').on('click', addNewItem);
            $('.translation-row textarea').summernote(summernoteConfig);
        @endif

        function drawData(index, data) {
            return '<div class="form-group col-lg-6 translation-row">'
                + '    <fieldset>'
                + '        <legend>' + (data.name || '<i>neuer Eintrag</i>')
                + '            <button class="btn btn-xs btn-danger pull-right entity-btn-remove"'
                + '                    role="button">×</button>'
                + '        </legend>'
                + '        <input class="form-control" name="data['+index+'][id]" type="hidden" value="'+index+'">'
                + '        <div class="form-group">'
                + '            <label for="locale-'+index+'" class="control-label">Locale</label>'
                + '            <input class="form-control" '
                + '                   name="data['+index+'][locale]" '
                + '                   type="text" '
                + '                   value="'+(data.locale || '')+'" '
                + '                   id="locale-'+index+'"'
                +                     (data.name ? ' readonly' : '') + '>'
                + '        </div>'
                + '        <div class="form-group">'
                + '            <label for="name-'+index+'" class="control-label">Name</label>'
                + '            <input class="form-control" '
                + '                   name="data['+index+'][name]" '
                + '                   type="text" '
                + '                   value="'+(data.name || '')+'" '
                + '                   id="name-'+index+'">'
                + '        </div>'
                + '        <div class="form-group">'
                + '            <label for="short_description-'+index+'" class="control-label">Kurz Beschreibung</label>'
                + '            <textarea class="form-control"'
                + '                      name="data['+index+'][short_description]"'
                + '                      id="short_description-'+index+'">'
                +                   (data.short_description || '')
                            + '</textarea>'
                + '        </div>'
                + '        <div class="form-group">'
                + '            <label for="long_description-'+index+'" class="control-label">Lang Beschreibung</label>'
                + '            <textarea class="form-control"'
                + '                      name="data['+index+'][long_description]"'
                + '                      id="long_description-'+index+'">'
                +                   (data.long_description || '')
                            + '</textarea>'
                + '        </div>'
                + '    </fieldset>'
                + '</div>';
        }

        function drawAddBlockBtn(index) {
            return '<button class="btn btn-success pull-right" data-id="'+index+'" id="add-new-block">'
                    + '    <span class="fa fa-plus fa-fw"></span> Neue Übersetzung hinzufügen'
                    + ' </button>'
                    + ' <hr style="clear:both">';
        }

        function addNewItem(e) {
            e.preventDefault();
            var lastId = $(e.target).attr('data-id');
            var nextId = ++lastId;

            $('#entity-data').find('#add-new-block').attr('data-id', nextId);
            $('#entity-data').append(drawData(nextId, []));
            $('#entity-data').find('#add-new-block').off('click').on('click', addNewItem);
            $(document).find('.entity-btn-remove').off('click').on('click', onRemoveRow);
            $('#short_description-'+nextId+',#long_description-'+nextId).summernote(summernoteConfig);
        }

        function onRemoveRow(e) {
            e.preventDefault();
            if ($(document).find('.translation-row').length > 1) {
                var group = $(this).closest('.form-group');
                group.remove();
            } else {
                $(this).remove();
            }
        }
    });
</script>
@endpush