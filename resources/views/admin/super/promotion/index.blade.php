@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Promotions <small>Gesamt: {{ $count }}</small>
                <div class="pull-right">
                    <a href="{{ route('admin::super:promotions.create') }}" class="btn btn-success">
                        <span class="fa fa-plus fa-fw"></span> Promotion anlegen
                    </a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row main-content">
        <div class="col-lg-12">
            <div class="row" style="margin-bottom: 15px;">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="types">Typ</label>
                        <select class="form-control" name="types" id="filter-type">
                            @foreach($typeList as $typeId => $typeName)
                                <option
                                        value="{{ $typeId }}"
                                        @if($typeId == $type)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $typeName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group input-daterange">
                        <div class="input-group-addon input-group-label">Gültigkeit</div>
                        <input type="text"
                               class="form-control"
                               id="date-range-from"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $from)->format('d.m.Y') }}">
                        <span class="input-group-addon">bis</span>
                        <input type="text"
                               class="form-control"
                               id="date-range-till"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $till)->format('d.m.Y') }}">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="button" id="filter-date-range-btn">Filter</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-promos">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Typ</th>
                        <th>Gültig von</th>
                        <th>Gültig bis</th>
                        <th>Bonus</th>
                        <th>Gutscheine</th>
                        <th>Ziel</th>
                        <th>Aktionen</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {{-- products modal --}}
    <div class="modal fade in" id="products-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            <span class="fa fa-cart-arrow-down fa-fw"></span> Produkte
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Eingeben Produkt-IDs unter:</p>
                        <div class="form-group">
                            <select class="form-control" id="products" name="products[]" multiple="multiple"></select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- ticket_types modal --}}
    <div class="modal fade in" id="ticket-types-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            <span class="fa fa-ticket fa-fw"></span>
                            Angelkarten
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Eingeben Ticket-Type-IDs unter:</p>
                        <div class="form-group">
                            <select class="form-control" id="ticketTypes" name="ticketTypes[]" multiple="multiple"></select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie diese Promotion endgültig löschen wollen?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Löschen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('.input-daterange').datepicker();

        function updateFilters() {
            var from = $('#date-range-from').datepicker('getDate'),
                till = $('#date-range-till').datepicker('getDate'),
                fromString = moment(from).format('YYYY-MM-DD'),
                tillString = moment(till).format('YYYY-MM-DD'),
                type = $('#filter-type').val(),
                location = '{{ route('admin::super:promotions.index') }}',
                params = [
                    'from=' + fromString,
                    'till=' + tillString
                ];

            if (type.length) {
                params.push('type=' + type);
            }

            window.location = location + '?' + params.join('&');
        }

        $('#filter-date-range-btn').on('click', updateFilters);
        $('#filter-type').on('change', updateFilters);
        $('#date-range-from').on('change', updateFilters);
        $('#date-range-till').on('change', updateFilters);

        var $table = $('#datatable-promos').DataTable({
            bFilter: false,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::super:promotions.data') }}' + window.location.search,
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'name', sortable: false },
                { data: 'status' },
                { data: 'type' },
                { data: 'valid_from' },
                { data: 'valid_till' },
                { data: 'bonus' },
                { data: 'coupons', sortable: false },
                { data: 'targets', sortable: false },
                { data: 'actions', sortable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#products-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var formAction = button.data('action');

            var modal = $(this);
            modal.find('.form').attr('action', formAction);

            var productSelect = modal.find('#products');
            var productData = [];

            productSelect.children().remove();

            $.getJSON(formAction).done(function (data) {
                $.each(data.data, function (index, product) {
                    productSelect.append($('<option/>', {
                        value: product.id,
                        text: product.name,
                        selected: true
                    }));
                });
            }).done(function () {
                productSelect.select2({
                    width: '100%',
                    data: productData,
                    ajax: {
                        url: '{{ route('admin::super:products.all') }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                // search: params.term,
                                ids: [ params.term ],
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.data,
                                pagination: {
                                    more: params.page < data.meta.pagination.total_pages
                                }
                            };
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                    templateResult: function (product) {
                        if (product.loading) return product.name;

                        return "<div class='select2-result-product'>"
                            + "     <div class='select2-result-product__name'>" + product.name + " (#" + product.id + ")</div>"
                            + "     <div class='select2-result-product__address'>"
                            +           (product.featured_from
                                            ? 'seit ' + moment(product.featured_from).format('DD.MM.YYYY')
                                            : '')
                            +           (product.featured_till
                                            ? ' bis ' + moment(product.featured_till).format('DD.MM.YYYY')
                                            : '')
                            + "     </div>"
                            + " </div>";
                    },
                    templateSelection: function (product) {
                        return product.text || product.name;
                    },
                    escapeMarkup: function (markup) { return markup; }
                });
            });
        });

        $('#ticket-types-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var formAction = button.data('action');

            var modal = $(this);
            modal.find('.form').attr('action', formAction);

            var typeSelect = modal.find('#ticketTypes');
            var typeData = [];

            typeSelect.children().remove();

            $.getJSON(formAction).done(function (data) {
                $.each(data.data, function (index, ticketType) {
                    typeSelect.append($('<option/>', {
                        value: ticketType.id,
                        text: ticketType.name,
                        selected: true
                    }));
                });
            }).done(function () {
                typeSelect.select2({
                    width: '100%',
                    data: typeData,
                    ajax: {
                        url: '{{ route('admin::super:tickettypes.all') }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                // search: params.term,
                                ticket_type_id: [ params.term ],
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.data,
                                pagination: {
                                    more: params.page < data.meta.pagination.total_pages
                                }
                            };
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                    templateResult: function (ticketType) {
                        if (ticketType.loading) return ticketType.name;

                        return ""
                            + "<div class='select2-result-ticketType'>"
                            + "     <div class='select2-result-ticketType__name'>"
                            +           ticketType.name + " (" + ticketType.id + ")"
                            + "     </div>"
                            + "     <div class='select2-result-ticketType__address'>"
                            +           (ticketType.area ? ticketType.area.name : '')
                            + "     </div>"
                            + "</div>";
                    },
                    templateSelection: function (ticketType) {
                        return ticketType.text || ticketType.name;
                    },
                    escapeMarkup: function (markup) { return markup; }
                });
            });
        });

        $('#delete-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget),
                formAction = button.data('action'),
                formMethod = button.data('method'),
                $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();
                $(document).find('.response-popup').remove();

                $.ajax({
                    'type' : formMethod,
                    'url': formAction,
                    'success' : function() {
                        $('#delete-modal').modal('hide');
                        var successBlock = ''
                            + '<div class="alert alert-success alert-dismissible response-popup" role="alert">'
                            + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                            + '        <span aria-hidden="true">&times;</span>'
                            + '    </button>'
                            + '    Promotion wurde gelöscht'
                            + '</div>';
                        $('.main-content').prepend(successBlock);
                        $table.ajax.reload();
                    },
                    'error': function (error) {
                        $('#delete-modal').modal('hide');
                        var errorMsg = error.responseText || 'Es ist ein Fehler aufgetreten';
                        var errorBlock = ''
                            + '<div class="alert alert-danger alert-dismissible response-popup" role="alert">'
                            + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                            + '        <span aria-hidden="true">&times;</span>'
                            + '    </button>'
                            +       errorMsg
                            + '</div>';
                        $('.main-content').prepend(errorBlock);
                    }
                });
            })
        });
    });
</script>
@endpush
