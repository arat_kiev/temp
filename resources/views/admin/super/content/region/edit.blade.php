@extends('admin.layout.default')

@inject ('states', 'App\Models\Location\State')

<?php
    $countryStates = [];
    foreach ($states::without('translations')->select(['id', 'country_id'])->get() as $state) {
        $countryStates[$state->country_id][] = $state->id;
    }
?>

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:content.regions.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Kreis
                @if($region->id)
                    bearbeiten<br/><small>{{ $region->name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if($errors->any())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->country) !!}
                    {!! form_row($form->state) !!}
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Daten
                </div>
                <div class="panel-body" id="region-data">
                    <div class="loading">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#countries').chosen({width: '100%'});

        var countries = {!! json_encode($countryStates) !!};

        // Check if state and region has been correctly set
        var selectedState = parseInt($('#states').val()),
            selectedCountry = parseInt($('#countries').val()),
            isCorrectCountryState = countries[selectedCountry].indexOf(selectedState) >= 0,
            hasState = $("#states option:selected") && isCorrectCountryState;

        displayCountryStates(hasState);
        $('#countries').on('change', function () {
            displayCountryStates(false);
        });

        @if($region->id)
            var url = "{{ route('admin::super:content.regions.show', ['regionId' => $region->id]) }}";

            $.getJSON(url, [], function (data) {
                var html = '';
                data.translations.forEach(function(item, index) {
                    html += drawData(index, item);
                });
                $('#region-data').html(html);
                $(document).find('.region-btn-remove').on('click', onRemoveRow);
                $('#region-data').prepend(drawAddBlockBtn(--data.translations.length));
                $('#region-data').find('#add-new-block').on('click', addNewItem);
            });
        @else
            $('#region-data').html(drawData(0, []));
            $('#region-data').prepend(drawAddBlockBtn(0));
            $(document).find('.region-btn-remove').on('click', onRemoveRow);
            $('#region-data').find('#add-new-block').on('click', addNewItem);
        @endif

        function drawData(index, data) {
            return '<div class="form-group col-sm-6 region-row">'
                    + '    <fieldset>'
                    + '        <legend>' + (data.name || '<i>neuer Eintrag</i>')
                    + '            <button class="btn btn-xs btn-danger pull-right region-btn-remove" role="button">×</button>'
                    + '        </legend>'
                    + '        <input class="form-control" name="data['+index+'][id]" type="hidden" value="'+index+'">'
                    + '        <div class="form-group">'
                    + '            <label for="locale-'+index+'" class="control-label">Locale</label>'
                    + '            <input class="form-control" '
                    + '                   name="data['+index+'][locale]" '
                    + '                   type="text" '
                    + '                   value="'+(data.locale || '')+'" '
                    + '                   id="locale-'+index+'"'
                    +                     (data.name ? 'readonly' : '') + '>'
                    + '        </div>'
                    + '        <div class="form-group">'
                    + '            <label for="name-'+index+'" class="control-label">Name</label>'
                    + '            <input class="form-control" '
                    + '                   name="data['+index+'][name]" '
                    + '                   type="text" '
                    + '                   value="'+(data.name || '')+'" '
                    + '                   id="name-'+index+'"'
                    +                     (data.name ? 'readonly' : '') + '>'
                    + '        </div>'
                    + '        <div class="form-group">'
                    + '            <label for="description-'+index+'" class="control-label">Beschreibung</label>'
                    + '            <textarea class="form-control"'
                    + '                      name="data['+index+'][description]"'
                    + '                      id="description-'+index+'">' + (data.description || '') + '</textarea>'
                    + '        </div>'
                    + '    </fieldset>'
                    + '</div>';
        }

        function drawAddBlockBtn(index) {
            return '<button class="btn btn-success pull-right" data-id="'+index+'" id="add-new-block">'
                    + '    <span class="fa fa-plus fa-fw"></span> Neue Übersetzung hinzufügen'
                    + ' </button>'
                    + ' <hr style="clear:both">';
        }

        function addNewItem(e) {
            e.preventDefault();
            var lastId = $(e.target).attr('data-id');
            var nextId = ++lastId;

            $('#region-data').find('#add-new-block').attr('data-id', nextId);
            $('#region-data').append(drawData(nextId, []));
            $('#region-data').find('#add-new-block').off('click').on('click', addNewItem);
            $(document).find('.region-btn-remove').off('click').on('click', onRemoveRow);
        }

        function onRemoveRow(e) {
            e.preventDefault();
            if ($(document).find('.region-row').length > 1) {
                var group = $(this).closest('.form-group');
                group.remove();
            } else {
                $(this).remove();
            }
        }

        function displayCountryStates(hasSelectedOption) {
            $('#states option').prop( "disabled", true).removeClass('active').hide();

            var countryId = $('#countries').val(),
                countryStates = countries[countryId];

            // Show only states for selected country
            for (var stateIndex in countryStates) {
                var stateId = countryStates[stateIndex];
                $('#states').find('option[value="'+stateId+'"]').prop( "disabled", false).addClass('active').show();
            }

            // Select first option, if select has any selected option
            if (!hasSelectedOption) {
                $("#states option:selected").prop("selected", false);
                $('#states option.active').first().attr('selected', true).prop('selected', true);
            }

            $('#states').chosen('destroy').chosen({width: '100%'});
            if (!$("#states option:selected")) {
                $('#states_chosen a.chosen-single span').text('');
            }
        }
    });
</script>
@endpush