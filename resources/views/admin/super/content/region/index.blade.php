@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Bezirke
                <small>Gesamt: {{ $count }}</small>
                <div class="pull-right">
                    <a href="{{ route('admin::super:content.regions.create') }}" class="btn btn-success">
                        <span class="fa fa-plus fa-fw"></span> Bezirk anlegen
                    </a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="state">Bundesland</label>
                        <select class="form-control" name="state" id="filter-state">
                            @foreach($stateList as $stateId => $stateName)
                                <option
                                        value="{{ $stateId }}"
                                        @if($stateId == $state)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $stateName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <table class="table table-bordered table-hover dataTable no-footer nowrap" id="datatable-regions">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Bundesland</th>
                    <th>Städte</th>
                    <th>Aktionen</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="regions-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            </span> Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie diesen Kreis endgültig löschen wollen?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Löschen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        function updateFilters() {
            var state = $('#filter-state').val();

            var location = '{{ route('admin::super:content.regions.index') }}';
            var params = [];

            if (state >= 0) {
                params.push('stateId=' + state);
            }

            window.location = location + '?' + params.join('&');
        }

        $('#filter-state').on('change', updateFilters);

        $('#datatable-regions').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::super:content.regions.data') }}' + window.location.search,
            pagingType: "full_numbers",
            columns: [
                { data: 'id', width: '10%' },
                { data: 'name' },
                { data: 'state', searchable: false, sortable: false },
                { data: 'cities', searchable: false, sortable: false },
                { data: 'actions', searchable: false, sortable: false, width: '30%' }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#regions-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget),
                formAction = button.data('action'),
                formMethod = button.data('method'),
                $form = $(this).find('.form');

            $form.on('submit', function (e) {
                e.preventDefault();

                $.ajax({
                    'type' : formMethod,
                    'url': formAction,
                    'success' : function() {
                        $('#regions-modal').modal('hide');
                        window.location.reload();
                    }
                });
            })
        });
    });
</script>
@endpush