@extends('admin.layout.default')

@if($technique->id)
    @include('admin.common.draft', [
        'drafted' => $technique->draft,
        'publish' => route('admin::super:content.techniques.publish', ['techniquesId' => $technique->id]),
        'delete' => route('admin::super:content.techniques.delete', ['techniquesId' => $technique->id]),
        'list' => route('admin::super:content.techniques.index')])
@endif

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:content.techniques.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Technik
                @if($technique->id)
                    bearbeiten<br/><small>{{ $technique->name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if($errors->any())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Image
                </div>
                <div class="panel-body">
                    @if($technique->picture)
                        <img src="{{ route('imagecache', ['template' => 'lLogo', 'filename' => $technique->picture->file]) }}" class="thumbnail img-responsive" />
                    @endif
                    {!! form_row($form->picture) !!}
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Daten
                </div>
                <div class="panel-body" id="technique-data">
                    <div class="loading">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        @if($technique->id)
            var url = "{{ route('admin::super:content.techniques.show', ['techniqueId' => $technique->id]) }}";

            $.getJSON(url, [], function (data) {
                var html = '';
                data.translations.forEach(function(item, index) {
                    html += drawData(index, item);
                });
                $('#technique-data').html(html);
                $(document).find('.technique-btn-remove').on('click', onRemoveRow);
                $('#technique-data').prepend(drawAddBlockBtn(--data.translations.length));
                $('#technique-data').find('#add-new-block').on('click', addNewItem);
            });
        @else
            $('#technique-data').html(drawData(0, []));
            $('#technique-data').prepend(drawAddBlockBtn(0));
            $(document).find('.technique-btn-remove').on('click', onRemoveRow);
            $('#technique-data').find('#add-new-block').on('click', addNewItem);
        @endif

        function drawData(index, data) {
            return '<div class="form-group col-sm-6 technique-row">'
                    + '    <fieldset>'
                    + '        <legend>' + (data.name || '<i>neuer Eintrag</i>')
                    + '            <button class="btn btn-xs btn-danger pull-right technique-btn-remove" role="button">×</button>'
                    + '        </legend>'
                    + '        <input class="form-control" name="data['+index+'][id]" type="hidden" value="'+index+'">'
                    + '        <div class="form-group">'
                    + '            <label for="locale-'+index+'" class="control-label">Locale</label>'
                    + '            <input class="form-control" '
                    + '                   name="data['+index+'][locale]" '
                    + '                   type="text" '
                    + '                   value="'+(data.locale || '')+'" '
                    + '                   id="locale-'+index+'"'
                    +                     (data.name ? 'readonly' : '') + '>'
                    + '        </div>'
                    + '        <div class="form-group">'
                    + '            <label for="name-'+index+'" class="control-label">Name</label>'
                    + '            <input class="form-control" '
                    + '                   name="data['+index+'][name]" '
                    + '                   type="text" '
                    + '                   value="'+(data.name || '')+'" '
                    + '                   id="name-'+index+'"'
                    +                     (data.name ? 'readonly' : '') + '>'
                    + '        </div>'
                    + '        <div class="form-group">'
                    + '            <label for="description-'+index+'" class="control-label">Beschreibung</label>'
                    + '            <textarea class="form-control"'
                    + '                      name="data['+index+'][description]"'
                    + '                      id="description-'+index+'">' + (data.description || '') + '</textarea>'
                    + '        </div>'
                    + '    </fieldset>'
                    + '</div>';
        }

        function drawAddBlockBtn(index) {
            return '<button class="btn btn-success pull-right" data-id="'+index+'" id="add-new-block">'
                    + '    <span class="fa fa-plus fa-fw"></span> Neue Übersetzung hinzufügen'
                    + ' </button>'
                    + ' <hr style="clear:both">';
        }

        function addNewItem(e) {
            e.preventDefault();
            var lastId = $(e.target).attr('data-id');
            var nextId = ++lastId;

            $('#technique-data').find('#add-new-block').attr('data-id', nextId);
            $('#technique-data').append(drawData(nextId, []));
            $('#technique-data').find('#add-new-block').off('click').on('click', addNewItem);
            $(document).find('.technique-btn-remove').off('click').on('click', onRemoveRow);
        }

        function onRemoveRow(e) {
            e.preventDefault();
            if ($(document).find('.technique-row').length > 1) {
                var group = $(this).closest('.form-group');
                group.remove();
            } else {
                $(this).remove();
            }
        }
    });
</script>
@endpush