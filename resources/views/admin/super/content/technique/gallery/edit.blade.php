@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default"
                   href="{{ route('admin::super:content.techniques.gallery.list', ['techId' => $technique->id]) }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Bild bearbeiten
            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Informationen
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-9">
                            {!! form_row($form->name) !!}
                        </div>
                        <div class="col-sm-3">
                            {!! form_row($form->priority) !!}
                        </div>
                    </div>
                    {!! form_row($form->description) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Bild
                </div>
                <div class="panel-body">
                    <img src="{{ route('imagecache', ['template' => 'org', 'filename' => $picture->file]) }}" class="img-responsive" />
                </div>
            </div>
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#description').summernote({
            lang: 'de-DE',
            height: '150px',
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['link', ['linkDialogShow', 'unlink']],
                ['misc', ['fullscreen', 'codeview', 'undo', 'redo', 'reset']]
            ],
            callbacks: {
                onImageUpload: function (files) {
                    return false;
                }
            }
        });
    });
</script>
@endpush