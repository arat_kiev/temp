@extends('admin.layout.default')

@if($fish->id)
    @include('admin.common.draft', [
        'drafted'   => $fish->draft,
        'publish'   => route('admin::super:content.fishes.publish', ['fishId' => $fish->id]),
        'delete'    => route('admin::super:content.fishes.delete', ['fishId' => $fish->id]),
        'list'      => route('admin::super:content.fishes.index'),
    ])
@endif

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:content.fishes.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Fisch
                @if($fish->id)
                    bearbeiten<br/><small>{{ $fish->name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if($errors->any())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->latin) !!}
                    {!! form_row($form->category) !!}
                    {!! form_row($form->min_size) !!}
                    {!! form_row($form->max_size) !!}
                    {!! form_row($form->min_weight) !!}
                    {!! form_row($form->max_weight) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Image
                </div>
                <div class="panel-body">
                    @if($fish->picture)
                        <img src="{{ route('imagecache', ['template' => 'lLogo', 'filename' => $fish->picture->file]) }}" class="thumbnail img-responsive" />
                    @endif
                    {!! form_row($form->picture) !!}
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Daten
                </div>
                <div class="panel-body" id="fish-data">
                    <div class="loading">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#categories').prepend('<option value="">-- Keine --</option>').chosen({width: '100%'});

        @if($fish->id)
            var url = "{{ route('admin::super:content.fishes.show', ['fishId' => $fish->id]) }}";

            $.getJSON(url, [], function (data) {
                var html = '';
                data.translations.forEach(function(item, index) {
                    html += drawData(index, item);
                });
                $('#fish-data').html(html);
                $(document).find('.fish-btn-remove').on('click', onRemoveRow);
                $('#fish-data').prepend(drawAddBlockBtn(--data.translations.length));
                $('#fish-data').find('#add-new-block').on('click', addNewItem);
            });
        @else
            $('#fish-data').html(drawData(0, []));
            $('#fish-data').prepend(drawAddBlockBtn(0));
            $(document).find('.fish-btn-remove').on('click', onRemoveRow);
            $('#fish-data').find('#add-new-block').on('click', addNewItem);
        @endif

        function drawData(index, data) {
            return '<div class="form-group col-sm-6 fish-row">'
                    + '    <fieldset>'
                    + '        <legend>' + (data.name || '<i>neuer Eintrag</i>')
                    + '            <button class="btn btn-xs btn-danger pull-right fish-btn-remove" role="button">×</button>'
                    + '        </legend>'
                    + '        <input class="form-control" name="data['+index+'][id]" type="hidden" value="'+index+'">'
                    + '        <div class="form-group">'
                    + '            <label for="locale-'+index+'" class="control-label">Locale</label>'
                    + '            <input class="form-control" '
                    + '                   name="data['+index+'][locale]" '
                    + '                   type="text" '
                    + '                   value="'+(data.locale || '')+'" '
                    + '                   id="locale-'+index+'"'
                    +                     (data.name ? 'readonly' : '') + '>'
                    + '        </div>'
                    + '        <div class="form-group">'
                    + '            <label for="name-'+index+'" class="control-label">Name</label>'
                    + '            <input class="form-control" '
                    + '                   name="data['+index+'][name]" '
                    + '                   type="text" '
                    + '                   value="'+(data.name || '')+'" '
                    + '                   id="name-'+index+'"'
                    +                     (data.name ? 'readonly' : '') + '>'
                    + '        </div>'
                    + '        <div class="form-group">'
                    + '            <label for="description-'+index+'" class="control-label">Beschreibung</label>'
                    + '            <textarea class="form-control"'
                    + '                      name="data['+index+'][description]"'
                    + '                      id="description-'+index+'">' + (data.description || '') + '</textarea>'
                    + '        </div>'
                    + '    </fieldset>'
                    + '</div>';
        }

        function drawAddBlockBtn(index) {
            return '<button class="btn btn-success pull-right" data-id="'+index+'" id="add-new-block">'
                    + '    <span class="fa fa-plus fa-fw"></span> Neue Übersetzung hinzufügen'
                    + ' </button>'
                    + ' <hr style="clear:both">';
        }

        function addNewItem(e) {
            e.preventDefault();
            var lastId = $(e.target).attr('data-id');
            var nextId = ++lastId;

            $('#fish-data').find('#add-new-block').attr('data-id', nextId);
            $('#fish-data').append(drawData(nextId, []));
            $('#fish-data').find('#add-new-block').off('click').on('click', addNewItem);
            $(document).find('.fish-btn-remove').off('click').on('click', onRemoveRow);
        }

        function onRemoveRow(e) {
            e.preventDefault();
            if ($(document).find('.fish-row').length > 1) {
                var group = $(this).closest('.form-group');
                group.remove();
            } else {
                $(this).remove();
            }
        }
    });
</script>
@endpush