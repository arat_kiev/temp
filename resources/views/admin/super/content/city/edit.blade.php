@extends('admin.layout.default')

@inject ('states', 'App\Models\Location\State')
@inject ('regions', 'App\Models\Location\Region')

<?php
    $countryStates = [];
    foreach ($states::without('translations')->select(['id', 'country_id'])->get() as $state) {
        $countryStates[$state->country_id][] = $state->id;
    }

    $stateRegions = [];
    foreach ($regions::without('translations')->select(['id', 'state_id'])->get() as $region) {
        $stateRegions[$region->state_id][] = $region->id;
    }
?>

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:content.cities.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Stadt
                @if($city->id)
                    bearbeiten<br/><small>{{ $city->name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if($errors->any())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->country) !!}
                    {!! form_row($form->state) !!}
                    {!! form_row($form->region) !!}
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Daten
                </div>
                <div class="panel-body" id="city-data">
                    <div class="loading">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#countries').chosen({width: '100%'});

        var countries = {!! json_encode($countryStates) !!},
            states = {!! json_encode($stateRegions) !!};

        // Check if state and region has been correctly set
        var selectedState = parseInt($('#states').val()),
            selectedCountry = parseInt($('#countries').val()),
            selectedRegion = parseInt($('#regions').val()),
            isCorrectCountryState = countries[selectedCountry].indexOf(selectedState) >= 0,
            hasState = $("#states option:selected") && isCorrectCountryState,
            isCorrectStateRegion = states[selectedState].indexOf(selectedRegion) >= 0,
            hasRegion = $("#regions option:selected") && hasState && isCorrectStateRegion;

        displayCountryStates(hasState);
        displayStateRegions(hasRegion);
        $('#countries').on('change', function () {
            displayCountryStates(false);
        });
        $('#states').on('change', function () {
            displayStateRegions(false);
        });

        @if($city->id)
            var url = "{{ route('admin::super:content.cities.show', ['cityId' => $city->id]) }}";

            $.getJSON(url, [], function (data) {
                var html = '';
                data.translations.forEach(function(item, index) {
                    html += drawData(index, item);
                });
                $('#city-data').html(html);
                $(document).find('.city-btn-remove').on('click', onRemoveRow);
                $('#city-data').prepend(drawAddBlockBtn(--data.translations.length));
                $('#city-data').find('#add-new-block').on('click', addNewItem);
            });
        @else
            $('#city-data').html(drawData(0, []));
            $('#city-data').prepend(drawAddBlockBtn(0));
            $(document).find('.city-btn-remove').on('click', onRemoveRow);
            $('#city-data').find('#add-new-block').on('click', addNewItem);
        @endif

        function drawData(index, data) {
            return '<div class="form-group col-sm-6 city-row">'
                    + '    <fieldset>'
                    + '        <legend>' + (data.name || '<i>neuer Eintrag</i>')
                    + '            <button class="btn btn-xs btn-danger pull-right city-btn-remove" role="button">×</button>'
                    + '        </legend>'
                    + '        <input class="form-control" name="data['+index+'][id]" type="hidden" value="'+index+'">'
                    + '        <div class="form-group">'
                    + '            <label for="locale-'+index+'" class="control-label">Locale</label>'
                    + '            <input class="form-control" '
                    + '                   name="data['+index+'][locale]" '
                    + '                   type="text" '
                    + '                   value="'+(data.locale || '')+'" '
                    + '                   id="locale-'+index+'"'
                    +                     (data.name ? 'readonly' : '') + '>'
                    + '        </div>'
                    + '        <div class="form-group">'
                    + '            <label for="name-'+index+'" class="control-label">Name</label>'
                    + '            <input class="form-control" '
                    + '                   name="data['+index+'][name]" '
                    + '                   type="text" '
                    + '                   value="'+(data.name || '')+'" '
                    + '                   id="name-'+index+'"'
                    +                     (data.name ? 'readonly' : '') + '>'
                    + '        </div>'
                    + '        <div class="form-group">'
                    + '            <label for="description-'+index+'" class="control-label">Beschreibung</label>'
                    + '            <textarea class="form-control"'
                    + '                      name="data['+index+'][description]"'
                    + '                      id="description-'+index+'">' + (data.description || '') + '</textarea>'
                    + '        </div>'
                    + '    </fieldset>'
                    + '</div>';
        }

        function drawAddBlockBtn(index) {
            return '<button class="btn btn-success pull-right" data-id="'+index+'" id="add-new-block">'
                    + '    <span class="fa fa-plus fa-fw"></span> Neue Übersetzung hinzufügen'
                    + ' </button>'
                    + ' <hr style="clear:both">';
        }

        function addNewItem(e) {
            e.preventDefault();
            var lastId = $(e.target).attr('data-id');
            var nextId = ++lastId;

            $('#city-data').find('#add-new-block').attr('data-id', nextId);
            $('#city-data').append(drawData(nextId, []));
            $('#city-data').find('#add-new-block').off('click').on('click', addNewItem);
            $(document).find('.city-btn-remove').off('click').on('click', onRemoveRow);
        }

        function onRemoveRow(e) {
            e.preventDefault();
            if ($(document).find('.city-row').length > 1) {
                var group = $(this).closest('.form-group');
                group.remove();
            } else {
                $(this).remove();
            }
        }

        function displayCountryStates(hasSelectedOption) {
            $('#states option').prop( "disabled", true).removeClass('active').hide();

            var countryId = $('#countries').val(),
                countryStates = countries[countryId];

            // Show only states for selected country
            for (stateIndex in countryStates) {
                var stateId = countryStates[stateIndex];
                $('#states').find('option[value="'+stateId+'"]').prop( "disabled", false).addClass('active').show();
            }

            // Select first option, if select has any selected option
            if (!hasSelectedOption) {
                $("#states option:selected").prop("selected", false);
                $('#states option.active').first().attr('selected', true).prop('selected', true);
                $('#states').change();
            }

            $('#states').chosen('destroy').chosen({width: '100%'});
            if (!$("#states option:selected")) {
                $('#states_chosen a.chosen-single span').text('');
            }
        }

        function displayStateRegions(hasSelectedOption) {
            $('#regions option').prop( "disabled", true).removeClass('active').hide();

            var stateId = $('#states').val(),
                stateRegions = states[stateId];

            // Show only states for selected country
            for (regionIndex in stateRegions) {
                var regionId = stateRegions[regionIndex];
                $('#regions').find('option[value="'+regionId+'"]').prop( "disabled", false).addClass('active').show();
            }

            // Select first option, if select has any selected option
            if (!hasSelectedOption) {
                $("#regions option:selected").prop("selected", false);
                $('#regions option.active').first().attr('selected', true).prop('selected', true);
            }

            $('#regions').chosen('destroy').chosen({width: '100%'});
            if (!$($("#regions option:selected"))) {
                $('#regions_chosen a.chosen-single span').text('');
            }
        }
    });
</script>
@endpush
