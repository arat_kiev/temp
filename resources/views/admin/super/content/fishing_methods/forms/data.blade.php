@if($fishingMethod)
    @foreach($fishingMethod->translations()->get() as $translation)
        <div class="form-group col-md-6 fishing-method-row" id="fishing-method-form-{{ $translation->id }}"
             data-method-id="{{ $translation->id }}">
            <fieldset>
                <legend>
                    {{ $translation->name }}
                    <button
                            class="btn btn-xs btn-danger pull-right fishing-method-btn-remove"
                            type="button" data-translate-id="{{ $translation->id }}" id="fishing-method-btn-remove">
                        ×
                    </button>
                </legend>
                <div class="form-group">
                    <label for="locale-{{ $translation->id }}" class="control-label">Locale</label>
                    <input class="form-control" name="data[{{ $translation->id }}][locale]" type="text"
                           value="{{ $translation->locale }}"
                           id="locale-{{ $translation->id }}">
                </div>
                <div class="form-group">
                    <label for="name-{{ $translation->id }}" class="control-label">Name</label>
                    <input class="form-control" name="data[{{ $translation->id }}][name]" type="text"
                           value="{{ $translation->name }}"
                           id="name-{{ $translation->id }}">
                </div>
                <div class="form-group">
                    <label for="description-{{ $translation->id }}" class="control-label">Beschreibung</label>
                    <textarea class="form-control" name="data[{{ $translation->id }}][description]"
                              id="description-{{ $translation->id }}">{{ $translation->description }}</textarea>
                </div>
            </fieldset>
        </div>
    @endforeach
@else
    <div class="form-group col-md-6 fishing-method-row" id="fishing-method-form-{{ $rowId }}"
         data-method-id="{{ $rowId }}">
        <fieldset>
            <legend>
                Neuer Eintrag
                <button
                        class="btn btn-xs btn-danger pull-right fishing-method-btn-remove"
                        type="button" data-translate-id="{{ $rowId }}">×
                </button>
            </legend>
            <div class="form-group">
                <label for="locale-{{ $rowId }}" class="control-label">Locale</label>
                <input class="form-control" name="data[{{ $rowId }}][locale]" type="text" id="locale-{{ $rowId }}">
            </div>
            <div class="form-group">
                <label for="name-{{ $rowId }}" class="control-label">Name</label>
                <input class="form-control" name="data[{{ $rowId }}][name]" type="text" id="name-{{ $rowId }}">
            </div>
            <div class="form-group">
                <label for="description-{{ $rowId }}" class="control-label">Beschreibung</label>
                <textarea class="form-control" name="data[{{ $rowId }}][description]"
                          id="description-{{ $rowId }}"></textarea>
            </div>
        </fieldset>
    </div>
@endif
