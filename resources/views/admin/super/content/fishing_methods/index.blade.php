@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Angelmethoden
                <div class="pull-right">
                    <a href="{{ route('admin::super:content.fishing-methods.create') }}" class="btn btn-success">
                        <span class="fa fa-plus fa-fw"></span> Angelmethoden anlegen
                    </a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row main-content">
        <div class="col-lg-12">
            <table class="table table-bordered table-hover dataTable no-footer nowrap" id="datatable-fishing-methods">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Aktionen</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="fishing-method-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie dieses Fisch endgültig löschen wollen?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Löschen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        var $table = $('#datatable-fishing-methods').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::super:content.fishing-methods.data') }}',
            pagingType: "full_numbers",
            columns: [
                { data: 'id', width: '10%' },
                { data: 'name' },
                { data: 'actions', searchable: false, sortable: false, width: '30%' }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#fishing-method-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget),
                formAction = button.data('action'),
                formMethod = button.data('method'),
                $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();
                $(document).find('.response-popup').remove();

                $.ajax({
                    'type' : formMethod,
                    'url': formAction,
                    success: function() {
                        $('#fishing-method-modal').modal('hide');
                        var successBlock = ''
                                + '<div class="alert alert-success alert-dismissible response-popup" role="alert">'
                                + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                                + '        <span aria-hidden="true">&times;</span>'
                                + '    </button>'
                                + '    Angelmethoden wurde erfolgreich entfernt'
                                + '</div>';
                        $('.main-content').prepend(successBlock);
                        $table.ajax.reload();
                    },
                    error: function (error) {
                        $('#fishing-method-modal').modal('hide');
                        var errorMsg = error.responseText || 'Es ist ein Fehler aufgetreten';
                        var errorBlock = ''
                            + '<div class="alert alert-danger alert-dismissible response-popup" role="alert">'
                            + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                            + '        <span aria-hidden="true">&times;</span>'
                            + '    </button>'
                            +       errorMsg
                            + '</div>';
                        $('.main-content').prepend(errorBlock);
                    }
                });
            })
        });
    });
</script>
@endpush