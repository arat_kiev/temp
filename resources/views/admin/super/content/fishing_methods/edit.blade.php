@extends('admin.layout.default')

@if($fishingMethod->id)
    @include('admin.common.draft', [
        'drafted'   => $fishingMethod->draft, //FIXME can't re,ove this
        'publish'   => route('admin::super:content.fishes.publish', ['fishingMethodId' => $fishingMethod->id]), //FIXME can't re,ove this
        'delete'    => route('admin::super:content.fishing-methods.delete', ['fishingMethodId' => $fishingMethod->id]),
        'list'      => route('admin::super:content.fishing-methods.index'),
    ])
@endif

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:content.fishing-methods.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Angelmethoden
                @if($fishingMethod->id)
                    <small>{{ $fishingMethod->translate(App::getLocale())->name }}</small>
                    @endif
            </h1>
            @if($errors->any())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        {!! form_row($form->fishingMethodId) !!}
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Daten
                    <button class="btn btn-xs btn-primary pull-right" type="button" id="fishing-method-new-btn">
                        <i class="fa fa-plus fa-fw"></i> Neue Übersetzung hinzufügen
                    </button>
                </div>
                <div class="panel-body row" id="fishing-method-data">
                    <div class="loading">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
    <script nonce="{{ csp_nounce() }}">

        $('document').ready(function () {
            var dataBlock = $('#fishing-method-data'),
                spinner = $('.loading'),
                fishingMethodId = $('#fishing-method-id').data('fishing-method-id'),
                rowId = 0;

            getForm(rowId, fishingMethodId);

            $('#fishing-method-new-btn').click(function (e) {
                e.preventDefault();
                getForm(rowId);
            });

            function getForm(rowIdParam, fishingMethodIdParam) {
                if (fishingMethodIdParam === undefined) {
                    fishingMethodIdParam = 0;
                }

                $.ajax({
                    url: '{{ route('admin::super:content.fishing-methods.get-form') }}',
                    method: 'get',
                    data: {
                        fishing_method_id: fishingMethodIdParam,
                        rowId: rowIdParam
                    },
                    beforeSend: function () {
                        spinner.show();
                    },
                    success: function (response) {
                        if (response.data) {
                            spinner.hide();
                            dataBlock.append(response.data);
                            rowId = $('.fishing-method-row').last().data('method-id');
                            rowId++;
                        }
                    },
                    error: function (error) {
                        console.log(error.responseText);
                    },
                    complete: function() {
                        $('.fishing-method-btn-remove').click(function () {
                            $(this).closest('.fishing-method-row').remove();
                            rowId--;
                        });
                    }
                });
            }
        });
    </script>
@endpush