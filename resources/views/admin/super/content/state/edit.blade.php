@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:content.states.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Bundesland
                @if($state->id)
                    bearbeiten<br/><small>{{ $state->name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if($errors->any())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->country) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Bestimmungen
                </div>
                <div class="panel-body">
                    <div class="alert alert-info">
                        @include('admin.partials.rules.state')
                    </div>
                    {!! form_row($form->rule_text) !!}
                    {!! form_row($form->rule_files) !!}
                    @foreach($errors->getMessages() as $error => $messages)
                        @if(starts_with($error, 'rule_files.'))
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($messages as $msg)
                                        <li>{{ $msg }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    @endforeach
                    <div class="form-group">
                        <label>Vorhandene Dateien</label>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Datei</th>
                                <th>Löschen</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($state->rule && $state->rule->files()->count() > 0)
                                @foreach($state->rule->files as $file)
                                    <tr>
                                        <td>
                                            {!! Html::linkRoute(
                                                    'filestream',
                                                    $file->fileName,
                                                    ['name' => $file->filePathWithName],
                                                    ['target' => '_blank']
                                                ) !!}
                                        </td>
                                        <td>
                                            {!! Form::checkbox('rule_files_delete[]', $file->id) !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3" class="text-center">
                                        Keine Dateien vorhanden
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Daten
                </div>
                <div class="panel-body" id="state-data">
                    <div class="loading">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#countries').chosen({width: '100%'});
        $('#rule_text').summernote({
            lang: 'de-DE',
            height: '150px',
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['link', ['linkDialogShow', 'unlink']],
                ['misc', ['fullscreen', 'codeview', 'undo', 'redo', 'reset']]
            ],
            callbacks: {
                onImageUpload: function(files) {
                    return false;
                }
            }
        });

        $(".rule_files").change(function(){
            $("input[name^=rule_files_name]").each(function(){
                $(this).prev('label').remove();
                $(this).remove();
            });
            var names = [];
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                names.push($(this).get(0).files[i].name);
                // console.log($(this).get(0).files[i].name);
                $("<div class='form-group'>"
                        + "<label for='Dateiname' class='control-label'>Dateiname: " + $(this).get(0).files[i].name + "</label>"
                        + "<input class='form-control' "
                        + "       pattern=\"[a-zA-Z\\s]+\""
                        + "       type=\"text\""
                        + "       name=\"rule_files_name[" + $(this).get(0).files[i].name + "]\" />"
                        + "</div>").insertAfter($(this).parent());
            }
        });

        @if($state->id)
            var url = "{{ route('admin::super:content.states.show', ['stateId' => $state->id]) }}";

            $.getJSON(url, [], function (data) {
                var html = '';
                data.translations.forEach(function(item, index) {
                    html += drawData(index, item);
                });
                $('#state-data').html(html);
                $(document).find('.state-btn-remove').on('click', onRemoveRow);
                $('#state-data').prepend(drawAddBlockBtn(--data.translations.length));
                $('#state-data').find('#add-new-block').on('click', addNewItem);
            });
        @else
            $('#state-data').html(drawData(0, []));
            $('#state-data').prepend(drawAddBlockBtn(0));
            $(document).find('.state-btn-remove').on('click', onRemoveRow);
            $('#state-data').find('#add-new-block').on('click', addNewItem);
        @endif

        function drawData(index, data) {
            return '<div class="form-group col-sm-6 state-row">'
                    + '    <fieldset>'
                    + '        <legend>' + (data.name || '<i>neuer Eintrag</i>')
                    + '            <button class="btn btn-xs btn-danger pull-right state-btn-remove" role="button">×</button>'
                    + '        </legend>'
                    + '        <input class="form-control" name="data['+index+'][id]" type="hidden" value="'+index+'">'
                    + '        <div class="form-group">'
                    + '            <label for="locale-'+index+'" class="control-label">Locale</label>'
                    + '            <input class="form-control" '
                    + '                   name="data['+index+'][locale]" '
                    + '                   type="text" '
                    + '                   value="'+(data.locale || '')+'" '
                    + '                   id="locale-'+index+'"'
                    +                     (data.name ? 'readonly' : '') + '>'
                    + '        </div>'
                    + '        <div class="form-group">'
                    + '            <label for="name-'+index+'" class="control-label">Name</label>'
                    + '            <input class="form-control" '
                    + '                   name="data['+index+'][name]" '
                    + '                   type="text" '
                    + '                   value="'+(data.name || '')+'" '
                    + '                   id="name-'+index+'"'
                    +                     (data.name ? 'readonly' : '') + '>'
                    + '        </div>'
                    + '        <div class="form-group">'
                    + '            <label for="description-'+index+'" class="control-label">Beschreibung</label>'
                    + '            <textarea class="form-control"'
                    + '                      name="data['+index+'][description]"'
                    + '                      id="description-'+index+'">' + (data.description || '') + '</textarea>'
                    + '        </div>'
                    + '    </fieldset>'
                    + '</div>';
        }

        function drawAddBlockBtn(index) {
            return '<button class="btn btn-success pull-right" data-id="'+index+'" id="add-new-block">'
                    + '    <span class="fa fa-plus fa-fw"></span> Neue Übersetzung hinzufügen'
                    + ' </button>'
                    + ' <hr style="clear:both">';
        }

        function addNewItem(e) {
            e.preventDefault();
            var lastId = $(e.target).attr('data-id');
            var nextId = ++lastId;

            $('#state-data').find('#add-new-block').attr('data-id', nextId);
            $('#state-data').append(drawData(nextId, []));
            $('#state-data').find('#add-new-block').off('click').on('click', addNewItem);
            $(document).find('.state-btn-remove').off('click').on('click', onRemoveRow);
        }

        function onRemoveRow(e) {
            e.preventDefault();
            if ($(document).find('.state-row').length > 1) {
                var group = $(this).closest('.form-group');
                group.remove();
            } else {
                $(this).remove();
            }
        }
    });
</script>
@endpush