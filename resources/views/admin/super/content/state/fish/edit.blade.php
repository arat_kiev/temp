@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default"
                   href="{{ route('admin::super:content.states.fishes.index', ['stateId' => $stateId]) }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Schonzeiten und Mindestmaße
                @if($stateFish->fish_id)
                    bearbeiten<br/><small>{{ $stateFish->state->name }} - {{ $stateFish->fish->name }}</small>
                @else
                    eintragen<br/><small>{{ $stateName }}</small>
                @endif
            </h1>
            @if($errors->any())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->fish) !!}
                    {!! form_row($form->closed_from) !!}
                    {!! form_row($form->closed_till) !!}
                    {!! form_row($form->min_length) !!}
                    {!! form_row($form->max_length) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#fishes').chosen({width: '100%'});
    });
</script>
@endpush