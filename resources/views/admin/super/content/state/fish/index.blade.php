@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:content.states.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Schonzeiten & Mindestmaße {{ $stateName }}
                <div class="pull-right">
                    <a href="{{ route('admin::super:content.states.fishes.create', ['stateId' => $stateId]) }}"
                       class="btn btn-success">
                        <span class="fa fa-plus fa-fw"></span> Neuer Eintrag
                    </a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered table-hover dataTable no-footer nowrap" id="datatable-state_fishes">
                <thead>
                <tr>
                    <th>Fisch</th>
                    <th>Schonzeit von</th>
                    <th>Schonzeit bis</th>
                    <th>Mindestmaß</th>
                    <th>Maximalmaß</th>
                    <th>Aktionen</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="fishes-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            </span> Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie diesen Fisch endgültig löschen wollen?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Löschen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#datatable-state_fishes').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::super:content.states.fishes.data', ['stateId' => $stateId]) }}',
            pagingType: "full_numbers",
            columns: [
                { data: 'fish', searchable: false, sortable: false },
                { data: 'closed_from' },
                { data: 'closed_till' },
                { data: 'min_length' },
                { data: 'max_length' },
                { data: 'actions', searchable: false, sortable: false, width: '30%' }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#fishes-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget),
                    formAction = button.data('action'),
                    formMethod = button.data('method'),
                    $form = $(this).find('.form');

            $form.on('submit', function (e) {
                e.preventDefault();

                $.ajax({
                    'type' : formMethod,
                    'url': formAction,
                    'success' : function() {
                        $('#fishes-modal').modal('hide');
                        window.location.reload();
                    }
                });
            })
        });
    });
</script>
@endpush