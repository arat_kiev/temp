@extends('admin.layout.default')

@push('styles')
<style>
    .tab-content>div {
        margin-top: 20px;
    }
    .modal-body input {
        border-radius: 0;
    }
    .success {
        color: #3c763d;
        text-align: right;
    }
</style>
@endpush

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:content.lp.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Landingpage
                @if($lp->id)
                    bearbeiten<br/><small>{{ $lp->title }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if($errors->any())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    @if(preg_match('/(^SQLSTATE.+?:\w*)|(^ERROR)/', $errors->first()))
                        <br> {{ $errors->first() }}
                    @endif
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->type) !!}
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a data-toggle="tab" href="#areas-tab">
                                Gewässer wählen
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#filter-tab">
                                Gewässer Filter
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="areas-tab" class="tab-pane fade in active">
                            {!! form_row($form->areas) !!}
                            <button type="button" class="btn btn-success pull-right" data-target="#query-modal" data-toggle="modal">
                                <span class="fa fa-plus fa-fw"></span> Gewässer über Abfrage hinzufügen
                            </button>
                        </div>
                        <div id="filter-tab" class="tab-pane fade">
                            {!! form_row($form->{'search[area_types]'}) !!}
                            {!! form_row($form->{'search[fishes]'}) !!}
                            {!! form_row($form->{'search[techniques]'}) !!}
                            {!! form_row($form->{'search[manager]'}) !!}
                            {!! form_row($form->{'search[is_ticket]'}) !!}
                            {!! form_row($form->{'search[ticket_categories]'}) !!}
                            {!! form_row($form->{'search[lease]'}) !!}
                            {!! form_row($form->{'search[city]'}) !!}
                            {!! form_row($form->{'search[region]'}) !!}
                            {!! form_row($form->{'search[state]'}) !!}
                            {!! form_row($form->{'search[country]'}) !!}
                            {!! form_row($form->{'search[member_only]'}) !!}
                            {!! form_row($form->{'search[boat]'}) !!}
                            {!! form_row($form->{'search[phone_ticket]'}) !!}
                            {!! form_row($form->{'search[nightfishing]'}) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Image
                </div>
                <div class="panel-body">
                    @if($lp->picture)
                        <img src="{{ route('imagecache', ['template' => 'lLogo', 'filename' => $lp->picture->file]) }}" class="thumbnail img-responsive" />
                    @endif
                    <input class="form-control" name="pic_changed" type="hidden" value="0">
                    {!! form_row($form->picture) !!}
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Daten
                </div>
                <div class="panel-body" id="lp-data">
                    <div class="loading">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}

    {{-- add areas by custom query modal --}}
    <div class="modal fade in" id="query-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            <span class="fa fa-search fa-fw"></span> Gewässer über Abfrage
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="area-query">Gewässer Abfrage:</label>
                            <div class="row">
                                <div class="col-xs-3" style="padding-right: 0;">
                                    <input type="text"
                                           class="form-control"
                                           id="area-query-endpoint"
                                           placeholder="/v1/areas/"
                                           style="text-align: right;"
                                           disabled>
                                </div>
                                <div class="col-xs-9" style="padding-left: 0;">
                                    <input type="text"
                                           class="form-control"
                                           id="area-query"
                                           placeholder="Abfrage Daten eingeben... (Muster: '?state=1&amp;fish[]=2')">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="areas-select">Gewässer:</label>
                            <select class="form-control" id="areas-select" name="areas-select[]" multiple="multiple">
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#areas').chosen({width: '100%'});
        $('#fishes').chosen({width: '100%'});
        $('#techniques').chosen({width: '100%'});
        $('#managers').chosen({width: '100%'});
        $('#categories').chosen({width: '100%'});
        $('#areaTypes').chosen({width: '100%'});

        $('#query-modal').on('show.bs.modal', function() {
            var modal = $(this);
            var $form = $(this).find('.form');
            var areaSelect = modal.find('#areas-select');
            var areaData = '';
            areaSelect.children().remove();

            @if($lp->id)
                var formAction = "{{ route('admin::super:content.lp.areas.index', ['lpId' => $lp->id]) }}";

                $.getJSON(formAction).done(function (data) {
                    $.each(data.data, function (index, area) {
                        areaSelect.append($('<option/>', {
                            value: area.id,
                            text: area.name,
                            selected: true
                        }));
                    });
                    areasQuerySelect(areaSelect, areaData);
                });
            @else
                areasQuerySelect(areaSelect, areaData);
            @endif

            $form.on('submit', function(e) {
                e.preventDefault();
                $('#areas option').prop('selected', false);
                areaSelect.val().forEach(function (value) {
                    $('#areas').find('option[value='+value+']').prop('selected', true);
                });

                $('#areas').chosen("destroy");
                $('#areas').chosen({width: '100%'});
                $('#query-modal').modal('hide');
            })
        });

        @if($lp->id)
            var url = "{{ route('admin::super:content.lp.show', ['lpId' => $lp->id]) }}";

            $.getJSON(url, [], function (data) {
                var html = '';
                data.translations.forEach(function(item, index) {
                    html += drawData(index, item);
                });
                $('#lp-data').html(html);
                $(document).find('.lp-btn-remove').on('click', onRemoveRow);
                $('#lp-data').prepend(drawAddBlockBtn(--data.translations.length));
                $('#lp-data').find('#add-new-block').on('click', addNewItem);
            });

            $('input[name=picture]').on('change', function() {
                $('input[name=pic_changed]').val(1);
            });
        @else
            $('#lp-data').html(drawData(0, []));
            $('#lp-data').prepend(drawAddBlockBtn(0));
            $(document).find('.lp-btn-remove').on('click', onRemoveRow);
            $('#lp-data').find('#add-new-block').on('click', addNewItem);
        @endif

        function drawData(index, data) {
            return '<div class="form-group col-sm-6 langingpage-row">'
                + '    <fieldset>'
                + '    <legend>' + (data.slug || '<i>neuer Eintrag</i>')
                + '        <button class="btn btn-xs btn-danger pull-right lp-btn-remove" role="button">×</button>'
                + '    </legend>'
                + '        <input class="form-control" name="data['+index+'][id]" type="hidden" value="'+index+'">'
                + '        <div class="form-group">'
                + '            <label for="locale-'+index+'" class="control-label">Locale</label>'
                + '            <input class="form-control" '
                                    + 'name="data['+index+'][locale]" '
                                    + 'type="text" '
                                    + 'value="'+(data.locale || '')+'" '
                                    + 'id="locale-'+index+'"'
                                    + (data.slug ? 'readonly' : '') + '>'
                + '        </div>'
                + '        <div class="form-group">'
                + '            <label for="slug-'+index+'" class="control-label">Slug</label>'
                + '            <input class="form-control" '
                                    + 'name="data['+index+'][slug]" '
                                    + 'type="text" '
                                    + 'value="'+(data.slug || '')+'" '
                                    + 'id="slug-'+index+'"'
                                    + (data.slug ? 'readonly' : '') + '>'
                + '        </div>'
                + '        <div class="form-group">'
                + '            <label for="title-'+index+'" class="control-label">Title</label>'
                + '            <input class="form-control" '
                                    + 'name="data['+index+'][title]" '
                                    + 'type="text" '
                                    + 'value="'+(data.title || '')+'" '
                                    + 'id="title-'+index+'">'
                + '        </div>'
                + '        <div class="form-group">'
                + '            <label for="headline-'+index+'" class="control-label">Headline</label>'
                + '            <textarea class="form-control" name="data['+index+'][headline]" id="headline-'+index+'">'
                                + (data.headline || '')
                            + '</textarea>'
                + '        </div>'
                + '        <div class="form-group">'
                + '            <label for="punchline-'+index+'" class="control-label">Punchline</label>'
                + '            <textarea class="form-control" name="data['+index+'][punchline]" id="punchline-'+index+'">'
                                + (data.punchline || '')
                            + '</textarea>'
                + '        </div>'
                + '        <div class="form-group">'
                + '            <label for="description-'+index+'" class="control-label">Beschreibung</label>'
                + '            <textarea class="form-control" name="data['+index+'][description]" id="description-'+index+'">'
                                + (data.description || '')
                            + '</textarea>'
                + '        </div>'
                + '    </fieldset>'
                + '</div>';
        }

        function drawAddBlockBtn(index) {
            return '<button class="btn btn-success pull-right" data-id="'+index+'" id="add-new-block">'
                + '    <span class="fa fa-plus fa-fw"></span> Neue Übersetzung hinzufügen'
                + ' </button>'
                + ' <hr style="clear:both">';
        }

        function addNewItem(e) {
            e.preventDefault();
            var lastId = $(e.target).attr('data-id');
            var nextId = ++lastId;

            $('#lp-data').find('#add-new-block').attr('data-id', nextId);
            $('#lp-data').append(drawData(nextId, []));
            $('#lp-data').find('#add-new-block').off('click').on('click', addNewItem);
            $(document).find('.lp-btn-remove').off('click').on('click', onRemoveRow);
        }

        function onRemoveRow(e) {
            e.preventDefault();
            if ($(document).find('.langingpage-row').length > 1) {
                var group = $(this).closest('.form-group');
                group.remove();
            } else {
                $(this).remove();
            }
        }

        function areasQuerySelect(areaSelect, areaData) {
            areaSelect.select2({
                width: '100%',
                data: [],
                ajax: {
                    url: '{{ route('admin::super:content.lp.areas.all') }}' + areaData,
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            search: params.term,
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: params.page < data.meta.pagination.total_pages
                            }
                        };
                    },
                    cache: true
                },
                templateResult: function (area) {
                    if (area.loading) return area.name;

                    return "<div class='select2-result-admin'>"
                        + "    <div class='select2-result-admin__name'>"
                                    + area.name
                                    + (area.type ? ' <b>('+area.type+')</b>' : '')
                        + "    </div>"
                        + "    <div class='select2-result-admin__address'>"
                                    + (area.city ? area.city + ', ' : '')
                                    + (area.region ? area.region+ ', ' : '')
                                    + (area.country ? area.country : '')
                        + "    </div>"
                        + "</div>";
                },
                templateSelection: function (area) {
                    return area.text || area.name;
                },
                escapeMarkup: function (markup) { return markup; }
            }).select2('open');

            $('#area-query').off('change').on('change', function(e) {
                e.preventDefault();
                areaSelect.select2('destroy');
                areaSelect.after('<div class="success">Gewässer Abfrage wurde geändert</div>');
                setTimeout(function() {$('.success').slideUp()}, 3000);
                areasQuerySelect(areaSelect, $(e.target).val());
            }).off('keypress').on('keypress', function(e) {
                if (e.which === 13) {
                    areaSelect.select2('destroy');
                    areaSelect.after('<div class="success">Gewässer Abfrage wurde geändert</div>');
                    setTimeout(function() {$('.success').slideUp()}, 3000);
                    areasQuerySelect(areaSelect, $(e.target).val());
                }
                return e.which !== 13;
            });
        }
    });
</script>
@endpush