@extends('admin.layout.default')

@if($poi->id)
    @include('admin.common.draft', [
        'drafted' => $poi->draft,
        'publish' => route('admin::super:content.poi.publish', ['poiId' => $poi->id]),
        'delete' => route('admin::super:content.poi.delete', ['poiId' => $poi->id]),
        'list' => route('admin::super:content.poi.index')])
@endif

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:content.poi.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Point Of Interests
                @if($poi->id)
                    bearbeiten<br/><small>{{ $poi->name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if($errors->any())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->name) !!}
                    {!! form_row($form->street) !!}
                    {!! form_row($form->post_code) !!}
                    {!! form_row($form->city) !!}
                    {!! form_row($form->country) !!}
                    {!! form_row($form->email) !!}
                    {!! form_row($form->phone) !!}
                    {!! form_row($form->website) !!}
                    {!! form_row($form->categories) !!}
                    {!! form_row($form->areas) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Image
                </div>
                <div class="panel-body">
                    @if($poi->picture)
                        <img src="{{ route('imagecache', ['template' => 'lLogo', 'filename' => $poi->picture->file]) }}" class="thumbnail img-responsive" />
                    @endif
                    {!! form_row($form->picture) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Öffnungszeiten
                </div>
                <div class="panel-body">
                    <div class="opening_hours-container" data-prototype="{{ form_row($form->opening_hours->prototype()) }}">
                        {!! form_row($form->opening_hours) !!}
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <select class="form-control" id="opening_hours">
                                @foreach($weekDays as $id => $name)
                                    <option value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary" id="opening_hours-btn-add">
                                    <span class="fa fa-plus fa-fw"></span> Neuer Tag
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12" style="clear: both">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Daten
                </div>
                <div class="panel-body" id="poi-data">
                    @if($poi->translations)
                        <button class="btn btn-success pull-right" data-id="{{ $poi->translations->count() }}" id="add-new-block">
                            <span class="fa fa-plus fa-fw"></span> Neue Übersetzung hinzufügen
                        </button>
                        <hr style="clear:both">
                        @foreach($poi->translations as $key => $item)
                            <div class="form-group col-sm-6 poi-row">
                                <fieldset>
                                    <legend>{{ $item['locale'] }}
                                        <button class="btn btn-xs btn-danger pull-right poi-btn-remove" role="button">×</button>
                                    </legend>
                                    <input class="form-control" name="data[ {{ $key }} ][id]" type="hidden" value=" {{ $key }} ">
                                    <div class="form-group">
                                            <label for="locale-{{ $key }} " class="control-label">Locale</label>
                                            <input class="form-control"
                                                   name="data[ {{ $key }} ][locale]"
                                                   type="text"
                                                   value="{{ $item['locale'] }}"
                                                   id="locale-{{ $key }}"
                                                   readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="description-{{ $key }}" class="control-label">Beschreibung</label>
                                        <textarea class="form-control"
                                                  name="data[{{ $key }}][description]"
                                                  id="description-{{ $key }}">{{ $item['description'] }}</textarea>
                                    </div>
                                </fieldset>
                            </div>
                        @endforeach
                    @else
                        <button class="btn btn-success pull-right" data-id="0" id="add-new-block">
                            <span class="fa fa-plus fa-fw"></span> Neue Übersetzung hinzufügen
                        </button>
                        <hr style="clear:both">
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        @if($poi->country)
            $('#countries').prepend('<option value="">-- Keine --</option>').chosen({width: '100%'});
        @else
            $('#countries').prepend('<option value="" selected>-- Keine --</option>').chosen({width: '100%'});
        @endif
        $('#categories').chosen({width: '100%'});
        $('#areas').chosen({width: '100%'});

        // ************
        // Translations

        $('#poi-data').find('#add-new-block').on('click', addNewItem);
        $(document).find('.poi-btn-remove').on('click', onRemoveRow);

        function addNewItem(e) {
            e.preventDefault();
            var lastId = $(e.target).attr('data-id');
            var nextId = ++lastId;

            $('#poi-data').find('#add-new-block').attr('data-id', nextId);
            $('#poi-data').append(drawData(nextId));
            $('#poi-data').find('#add-new-block').off('click').on('click', addNewItem);
            $(document).find('.poi-btn-remove').off('click').on('click', onRemoveRow);
        }

        function onRemoveRow(e) {
            e.preventDefault();
            $(this).closest('.form-group').remove();
        }

        function drawData(index) {
            return '<div class="form-group col-sm-6 poi-row">'
                + '    <fieldset>'
                + '        <legend><i>neuer Eintrag</i>'
                + '            <button class="btn btn-xs btn-danger pull-right poi-btn-remove" role="button">×</button>'
                + '        </legend>'
                + '        <input class="form-control" name="data['+index+'][id]" type="hidden" value="'+index+'">'
                + '        <div class="form-group">'
                + '            <label for="locale-'+index+'" class="control-label">Locale</label>'
                + '            <input class="form-control"'
                + '                   name="data['+index+'][locale]"'
                + '                   type="text"'
                + '                   id="locale-'+index+'">'
                + '        </div>'
                + '        <div class="form-group">'
                + '            <label for="description-'+index+'" class="control-label">Description</label>'
                + '            <textarea class="form-control"'
                + '                      name="data['+index+'][description]"'
                + '                      id="description-'+index+'"></textarea>'
                + '        </div>'
                + '    </fieldset>'
                + '</div>';
        }

        // *************
        // Opening Hours

        var openingHoursContainer = $('.opening_hours-container'),
            openingHoursSelect = $('#opening_hours'),
            openingHoursCount = openingHoursContainer.children('.form-group').children().length;

        $('.date').datetimepicker({
            format: 'LT',
            allowInputToggle: true
        });

        $('.opening_hour-id').each(function(index, element) {
            $('#opening_hours option[value="' + element.value + '"]').prop('disabled', true);
        });

        function onRemoveOpeningHourClicked(e) {
            e.preventDefault();
            var group = $(this).closest('.form-group'),
                id = group.find('.opening_hour-id').val();

            $('#opening_hours option[value="' + id + '"]').prop('disabled', false);

            group.remove();
        }

        $('.opening_hours-btn-remove').on('click', onRemoveOpeningHourClicked);

        $('#opening_hours-btn-add').on('click', function(e) {
            e.preventDefault();

            var selectedOpeningHour = openingHoursSelect.find(':selected');

            if (selectedOpeningHour.prop('disabled')) {
                return false;
            }
            var openingHourValue = openingHoursSelect.val(),
                openingHourName = selectedOpeningHour.text(),
                proto = openingHoursContainer.data('prototype')
                    .replace(/__OPENINIG_HOUR_TITLE__/g, openingHourName)
                    .replace(/__NAME__/g, openingHoursCount++)
                    .replace(/__OPENING_HOUR_DAY__/g, openingHourValue);
            openingHoursContainer.children('.form-group').append(proto);

            openingHoursSelect.find(':selected').prop('disabled', true);

            $('.date').datetimepicker({
                format: 'LT',
                allowInputToggle: true
            });

            $('.opening_hours-btn-remove').on('click', onRemoveOpeningHourClicked);
        });
    });
</script>
@endpush