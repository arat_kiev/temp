@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:content.legaldocs.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Dokument
                @if($document->id)
                    bearbeiten<br/><small>{{ $document->internal_name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if($errors->any())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                </div>
            @endif
        </div>
    </div>
    {!! form_start($form) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->internal_name) !!}
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Mehrsprachige Felder
                    <div class="pull-right" style="margin-top: -4px;">
                        <button type="button" class="btn btn-sm btn-success" id="translations-btn-add">
                            <span class="fa fa-plus fa-fw"></span> Hinzufügen
                        </button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row" id="translations-container" data-prototype="{{ form_row($form->translations->prototype()) }}">
                        {!! form_row($form->translations) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_row($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $('document').ready(function() {
            enableRichText('.translations-rich-text');

            var container = $('#translations-container');
            var translationCount = container.children().length;

            function onTranslationRemoveClicked(e) {
                e.preventDefault();
                $(this).closest('.form-group').remove();
            }

            $('.translations-btn-remove').on('click', onTranslationRemoveClicked);

            $('#translations-btn-add').on('click', function(e) {
                e.preventDefault();
                translationCount++;
                var proto = container.attr('data-prototype').replace(/__NAME__/g, translationCount);
                container.append(proto);

                $('.translations-btn-remove').on('click', onTranslationRemoveClicked);
                enableRichText('.translations-rich-text');
            });
        });

        function enableRichText(selector) {
            $(selector).summernote({
                lang: 'de-DE',
                height: 200,
                minHeight: 200,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'clear']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['link', ['linkDialogShow', 'unlink']],
                    ['misc', ['fullscreen', 'codeview', 'undo', 'redo', 'reset']]
                ],
                callbacks: {
                    onImageUpload: function(files) {
                        return false;
                    }
                }
            });
        }
    </script>
@endpush
