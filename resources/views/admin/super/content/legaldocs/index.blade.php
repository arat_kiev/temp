@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Rechtliche Dokumente
                <small>Gesamt: {{ $count }}</small>
                <div class="pull-right">
                    <a href="{{ route('admin::super:content.legaldocs.create') }}" class="btn btn-success">
                        <span class="fa fa-plus fa-fw"></span> Dokument erstellen
                    </a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered table-hover dataTable no-footer nowrap" id="datatable-legaldocs">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Erstellt am</th>
                    <th>Bewirtschafter</th>
                    <th>Aktionen</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="legaldocs-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            Bestätigung
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie dieses Dokument löschen wollen?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Löschen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $('document').ready(function() {
            $('#datatable-legaldocs').DataTable({
                stateSave: true,
                processing: true,
                serverSide: true,
                ajax: '{{ route('admin::super:content.legaldocs.data') }}',
                pagingType: "full_numbers",
                columns: [
                    { data: 'id', width: '10%' },
                    { data: 'name' },
                    { data: 'created_at', searchable: false, sortable: false },
                    { data: 'managers', searchable: false, sortable: false },
                    { data: 'actions', searchable: false, sortable: false, width: '30%' }
                ],
                language: {
                    decimal: ",",
                    thousands: ".",
                    lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                    zeroRecords: "Keine Einträge vorhanden",
                    info: "Seite _PAGE_ von _PAGES_",
                    infoEmpty: "Keine Einträge vorhanden",
                    infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                    search: "Suche",
                    paginate: {
                        first: "&laquo;",
                        previous: "&lsaquo;",
                        next: "&rsaquo;",
                        last: "&raquo;"
                    }
                }
            });

            $('#legaldocs-modal').on('show.bs.modal', function(e) {
                var button = $(e.relatedTarget);
                var formAction = button.attr('data-action');
                var formMethod = button.attr('data-method');
                var $form = $(this).find('.form');

                $form.on('submit', function (e) {
                    e.preventDefault();

                    $.ajax({
                        'type' : formMethod,
                        'url': formAction,
                        'success' : function() {
                            $('#legaldocs-modal').modal('hide');
                            window.location.reload();
                        }
                    });
                })
            });
        });
    </script>
@endpush