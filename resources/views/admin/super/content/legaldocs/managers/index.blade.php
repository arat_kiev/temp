@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:content.legaldocs.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Bewirtschafter für Dokument
                <small>{{ $document->internal_name }}</small>
                <div class="pull-right">
                    <button id="btn-manager-add" class="btn btn-success" data-toggle="modal" data-target="#manager-modal">
                        <span class="fa fa-plus fa-fw"></span> Bewirtschafter hinzufügen
                    </button>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-lg-push-9">
            <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control" name="status" id="filter-status">
                    @foreach($filter['status']['values'] as $statusValue)
                        <option value="{{ $statusValue }}"
                            {{ $statusValue == $filter['status']['select'] ? 'selected="selected"' : '' }}>
                            {{ $statusValue }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-lg-12">
            <table class="table table-bordered table-hover dataTable no-footer nowrap" id="datatable-doc-managers">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Bewirtschafter</th>
                    <th>Hinzugefügt am/um</th>
                    <th>Akzeptiert am/um</th>
                    <th>Akzeptiert von</th>
                    <th>Aktionen</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="manager-docs-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            Bestätigung
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie diesen Bewirtschafter entfernen wollen?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Entfernen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- manager modal --}}
    <div class="modal fade in" id="manager-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post" action="{{ route('admin::super:content.legaldocs.managers.add', ['docId' => $document]) }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            <span class="fa fa-group fa-fw"></span> Bewirtschafter hinzufügen
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select class="form-control" id="managers" name="managers[]" multiple="multiple"></select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Hinzufügen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $('document').ready(function() {
            $('#datatable-doc-managers').DataTable({
                stateSave: true,
                processing: true,
                serverSide: true,
                ajax: '{{ route('admin::super:content.legaldocs.managers.data', ['docId' => $document]) }}' + window.location.search,
                pagingType: "full_numbers",
                columns: [
                    { data: 'id', searchable: true, sortable: true },
                    { data: 'name', searchable: true, sortable: false },
                    { data: 'created_at', searchable: false, sortable: true },
                    { data: 'signed_at', searchable: false, sortable: true },
                    { data: 'signed_by', searchable: false, sortable: false },
                    { data: 'actions', searchable: false, sortable: false, width: '150px' }
                ],
                language: {
                    decimal: ",",
                    thousands: ".",
                    lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                    zeroRecords: "Keine Einträge vorhanden",
                    info: "Seite _PAGE_ von _PAGES_",
                    infoEmpty: "Keine Einträge vorhanden",
                    infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                    search: "Suche",
                    paginate: {
                        first: "&laquo;",
                        previous: "&lsaquo;",
                        next: "&rsaquo;",
                        last: "&raquo;"
                    }
                }
            });

            $('#manager-docs-modal').on('show.bs.modal', function(e) {
                var button = $(e.relatedTarget),
                    formAction = button.data('action'),
                    formMethod = button.data('method'),
                    $form = $(this).find('.form');

                $form.on('submit', function (e) {
                    e.preventDefault();

                    $.ajax({
                        'type' : formMethod,
                        'url': formAction,
                        'success' : function() {
                            $('#techniques-modal').modal('hide');
                            window.location.reload();
                        }
                    });
                })
            });

            $('#manager-modal').on('show.bs.modal', function(e) {
                var button = $(e.relatedTarget);
                var formAction = button.attr('data-action');

                var modal = $(this);
                modal.find('.form').attr('action', formAction);

                var manSelect = modal.find('#managers');
                var manData = [];

                manSelect.children().remove();
                manSelect.select2({
                    width: '100%',
                    data: manData,
                    ajax: {
                        url: '{{ route('admin::super:managers.all') }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term,
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.data,
                                pagination: {
                                    more: params.page < data.meta.pagination.total_pages
                                }
                            };
                        },
                        cache: true
                    },
                    minimumInputLength: 3,
                    templateResult: function (manager) {
                        if (manager.loading) return manager.name;

                        var image = manager.logo ? manager.logo.files.small : '{{ route('imagecache', ['template' => 'lLogo', 'ticket/hejfish_logo.png']) }}';

                        return "<div class='select2-result-manager'>" +
                            "<img class='select2-result-manager__avatar' src='" + image + "' />" +
                            "<div class='select2-result-manager__name'>" + manager.name + "</div>" +
                            "<div class='select2-result-manager__address'>" + manager.street + ", " +
                            manager.area_code + " " + manager.city + " / " + manager.country + "</div>";
                    },
                    templateSelection: function (manager) {
                        return manager.text || manager.name;
                    },
                    escapeMarkup: function (markup) { return markup; }
                });
            });

            function updateFilters() {
                var status = $('#filter-status').val();

                var location = '{{ route('admin::super:content.legaldocs.managers.index', ['docId' => $document]) }}';
                var params = [];

                params.push('status=' + status);
                window.location = location + '?' + params;
            }

            $('#filter-status').select2();
            $('#filter-status').on('change', updateFilters);
        });
    </script>
@endpush

@push('styles')
    <style>
        .select2-result-manager__avatar {
            height: 44px;
            float: left;
            margin-right: 10px;
        }

        .select2-result-manager__name {
            font-size: 16px;
        }
    </style>
@endpush