@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Dokumentation
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if($fileExists)
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Informationen über die Datei
                    </div>
                    <div class="panel-body">
                        <p><b>Zuletzt bearbeitet</b>: {{ $lastModified }}</p>
                        <p class="col-lg-offset-2 col-lg-8">
                            <a href="{{ route('apidoc.view') }}" type="button" class="btn btn-outline btn-success btn-lg btn-block">
                                Dokumentation anzeigen
                                <span class="fa fa-arrow-right fa-fw"></span>
                            </a>
                        </p>
                        <p class="text-center info-msg col-xs-12"></p>
                    </div>
                    <div class="panel-footer">
                        <button type="button" class="btn btn-outline btn-primary btn-lg btn-block btn-gen">
                            <span class="fa fa-refresh fa-fw"></span>
                            Dokumentation neu generieren
                        </button>
                    </div>
                </div>
            @else
                <div class="panel panel-red">
                    <div class="panel-heading">
                        Informationen über die Datei
                    </div>
                    <div class="panel-body">
                        <p>Die Dokumentation ist noch nicht erzeugt worden ist</p>
                        <p class="col-lg-offset-2 col-lg-8">
                            <button type="button" class="btn btn-outline btn-primary btn-lg btn-block btn-gen">
                                <span class="fa fa-refresh fa-fw"></span>
                                Dokumentation generieren
                            </button>
                        </p>
                        <p class="text-center info-msg col-xs-12"></p>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function () {
        $('button.btn-gen').on('click', function (e) {
            e.preventDefault();
            var $btn = $(e.target),
                msg = '<i class="fa fa-spinner fa-pulse fa-fw"></i>' +
                      '<span class="sr-only">Loading...</span>';

            $btn.addClass('disabled').attr('disabled', true).text('').html('In Bearbeitung ' + msg);
            $('p.info-msg').removeClass('text-danger').text('');

            $.post({
                url: '{{ route('admin::super:apidoc.refresh') }}',
                success: function(data) {
                    window.location.reload();
                },
                error: function(error) {
                    $btn
                        .removeClass('disabled')
                        .attr('disabled', false)
                        .html('<span class="fa fa-refresh fa-fw"></span> Dokumentation generieren');
                    $('p.info-msg')
                        .addClass('text-danger')
                        .text('Es ist ein Fehler aufgetreten. Bitte kontaktieren Sie Ihren Administrator');
                    console.log(error);
                }
            });
        });
    });
</script>
@endpush
