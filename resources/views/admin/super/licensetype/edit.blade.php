@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:licensetypes.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Berechtigungstypen
                @if($lType->id)
                    bearbeiten<br/><small>{{ $lType->name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->name) !!}
                    {!! form_row($form->attachments) !!}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Felder
                </div>
                <div class="panel-body">
                    <div class="fields-container" data-prototype="{{ form_row($form->fields->prototype()) }}">
                        {!! form_row($form->fields) !!}
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <select class="form-control" id="field">
                                @foreach(array_flip(\App\Models\License\LicenseType::ALLOWED_FIELD_TYPES) as $id => $name)
                                    <option value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                            @php
                                $fieldDisabled = $lType->licenses()->count()
                                    ? 'disabled'
                                    : '';
                            @endphp
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary {{ $fieldDisabled }}"
                                        id="field-btn-add"
                                        {{ $fieldDisabled }}>
                                    <span class="fa fa-plus fa-fw"></span> Feld hinzufügen
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#attachments').select2({
            width: '100%',
            tags: true,
            multiple: true,
            minimumInputLength: 3,
            tokenSeparators: [';']
        });

        function onRemoveFieldClicked(e) {
            e.preventDefault();
            var group = $(this).closest('.form-group');
            var id = group.find('.field-id').val();

            $('#field option[value="' + id + '"]').prop('disabled', false);

            group.remove();
        }

        var fieldContainer = $('.fields-container');
        var fieldSelect = $('#field');
        var fieldCount = fieldContainer.children('.form-group').children().length;

        $('.field-btn-remove').on('click', onRemoveFieldClicked);

        $('#field-btn-add').on('click', function(e) {
            e.preventDefault();

            var selectedField = fieldSelect.find(':selected');
            var fieldId = fieldSelect.val();

            var fieldName = selectedField.text();
            var proto = fieldContainer.data('prototype')
                    .replace(/__FIELD_ID__/g, fieldId)
                    .replace(/__FIELD_TYPE__/g, fieldId)
                    .replace(/__NAME__/g, fieldCount++)
                    .replace(/__FIELD__/g, fieldName);
            fieldContainer.children('.form-group').append(proto);

            fieldSelect.find(':selected').prop('disabled', true);

            $('.field-btn-remove').on('click', onRemoveFieldClicked);
        });
    });
</script>
@endpush