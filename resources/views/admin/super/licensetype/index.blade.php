@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Berechtigungstypen
                <div class="pull-right">
                    <a href="{{ route('admin::super:licensetypes.create') }}" class="btn btn-success">
                        <span class="fa fa-plus fa-fw"></span> Berechtigungstyp anlegen
                    </a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row" style="margin-bottom: 15px;">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="state-list">Bundesländer</label>
                        <select class="form-control" name="state-list" id="filter-state">
                            @foreach($stateList as $stateId => $stateName)
                                <option
                                        value="{{ $stateId }}"
                                        @if($stateId == $state)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $stateName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap"
                       id="datatable-license-types">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Zuletzt bearbeitet</th>
                        <th>Bundesländer</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {{-- states modal --}}
    <div class="modal fade in" id="state-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            <span class="fa fa-compass fa-fw"></span> Bundesländer
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select class="form-control" id="states" name="states[]" multiple="multiple"></select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        function updateFilters() {
            var type = $('#filter-state').val(),
                location = '{{ route('admin::super:licensetypes.index') }}',
                params = [];

            if (type.length) {
                params.push('stateId=' + type);
            }

            window.location = location + '?' + params.join('&');
        }

        $('#filter-state').on('change', updateFilters);

        $('#datatable-license-types').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::super:licensetypes.data') }}' + window.location.search,
            pagingType: "full_numbers",
            columns: [
                { data: 'id', searchable: false },
                { data: 'name' },
                { data: 'updated_at', searchable: false },
                { data: 'states', searchable: false, sortable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#state-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var formAction = button.data('action');

            var modal = $(this);
            modal.find('.form').attr('action', formAction);

            var stateSelect = modal.find('#states');
            var stateData = [];

            stateSelect.children().remove();

            $.getJSON(formAction).done(function (data) {
                $.each(data.data, function (index, state) {
                    stateSelect.append($('<option/>', {
                        value: state.id,
                        text: state.name,
                        selected: true
                    }));
                });
            }).done(function () {
                stateSelect.select2({
                    width: '100%',
                    data: stateData,
                    ajax: {
                        url: '{{ route('admin::super:content.states.all') }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term,
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.data,
                                pagination: {
                                    more: params.page < data.meta.pagination.total_pages
                                }
                            };
                        },
                        cache: true
                    },
                    templateResult: function (state) {
                        // console.log(state);

                        if (state.loading) return state.name;

                        return ""
                            + "<div class='select2-result-admin'>"
                            + "     <div class='select2-result-admin__name'>"
                            +           state.name + " (" + state.id + ")"
                            + "     </div>"
                            + "     <div class='select2-result-admin__address'>"
                            +           state.country
                            + "     </div>"
                            + "</div>";
                    },
                    templateSelection: function (state) {
                        return state.text || state.name;
                    },
                    escapeMarkup: function (markup) { return markup; }
                });
            });
        });
    });
</script>
@endpush

@push('styles')
<style>
    .select2-result-admin__name {
        font-size: 18px;
    }
</style>
@endpush
