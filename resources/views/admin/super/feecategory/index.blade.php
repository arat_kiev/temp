@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Gebührkategorien
                <small>Gesamt: {{ $count }}</small>
                <div class="pull-right">
                    <a href="{{ route('admin::super:fee_categories.create') }}" class="btn btn-success">
                        <span class="fa fa-plus fa-fw"></span> Gebührkategorie anlegen
                    </a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="fee">Gebühr</label>
                        <select class="form-control" name="fee" id="filter-fee">
                            @foreach($feeList as $feeId => $feeName)
                                <option
                                        value="{{ $feeId }}"
                                        @if($feeId == $fee)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $feeName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-categories">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Aktionen</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            </span> Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie diese Gebührkategorie endgültig löschen wollen?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Löschen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        function updateFilters() {
            var fee = $('#filter-fee').val(),
                location = '{{ route('admin::super:fee_categories.data') }}',
                params = [];

            if (fee > 0) {
                params.push('fee=' + fee);
            }
            $table.ajax.url(location + '?' + params.join('&')).load();
        }

        $('#filter-fee').on('change', updateFilters);

        var $table = $('#datatable-categories').DataTable({
            bFilter: false,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::super:fee_categories.data') }}' + window.location.search,
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'name', sortable: false },
                { data: 'actions', sortable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#delete-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget),
                formAction = button.data('action'),
                formMethod = button.data('method'),
                $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();

                $.ajax({
                    type : formMethod,
                    url: formAction,
                    success : function() {
                        $('#delete-modal').modal('hide');
                        window.location.reload();
                    },
                    error: function (error) {
                        console.log(error);
                        if (error.status == 403) {
                            var msgBox = ''
                                + '<div class="row response-popup" style="padding-top: 10px;">'
                                + '    <div class="col-lg-12">'
                                + '        <div class="alert alert-danger alert-dismissable">'
                                + '            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">'
                                                + "&times;"
                                + '            </button>'
                                                + 'Gebührkategorie könnte nicht gelöscht werden'
                                + '        </div>'
                                + '    </div>'
                                + '</div>';
                            $('.page-header').parents('.row').before(msgBox);
                            $('#delete-modal').modal('hide');
                        }
                    }
                });
            })
        });
    });
</script>
@endpush
