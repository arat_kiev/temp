@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Lagermengen
                <div class="pull-right">
                    <a href="{{ route('admin::super:product_stocks.create') }}" class="btn btn-success">
                        <span class="fa fa-plus fa-fw"></span> Wareneingang
                    </a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="stocks">Lager</label>
                        <select class="form-control" name="stocks" id="filter-stocks">
                            @foreach($stockList as $stockId => $stockName)
                                <option
                                        value="{{ $stockId }}"
                                        @if($stockId == $stock)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $stockName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-stocks">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Produkt</th>
                        <th>Lager</th>
                        <th>Lagermenge</th>
                        <th>Benutzer benachrichtigen</th>
                        <th>Aktionen</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {{-- users modal --}}
    <div class="modal fade in" id="users-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false"
         style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            <span class="fa fa-group fa-fw"></span> Benutzer benachrichtigen
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select class="form-control" id="users" name="users[]" multiple="multiple"></select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Schließen">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie diese Menge löschen wollen?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Löschen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        function updateFilters() {
            var stock = $('#filter-stocks').val(),
                location = '{{ route('admin::super:product_stocks.data') }}',
                params = [];

            if (stock >= 0) {
                params.push('stock=' + stock);
            }
            $table.ajax.url(location + '?' + params.join('&')).load();
        }

        $('#filter-stocks').off().on('change', updateFilters);

        var $table = $('#datatable-stocks').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::super:product_stocks.data') }}' + window.location.search,
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'product', sortable: false, searchable: false  },
                { data: 'stock', sortable: false, searchable: false  },
                { data: 'quantity' },
                { data: 'notify_users', sortable: false, searchable: false  },
                { data: 'actions', sortable: false, searchable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#delete-modal').on('show.bs.modal', function(e) {
            var formAction = $(e.relatedTarget).data('action'),
                formMethod = 'DELETE',
                $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();
                $(document).find('.response-popup').remove();

                $.ajax({
                    type : formMethod,
                    url: formAction,
                    success : function(data) {
                        $('#delete-modal').modal('hide');
                        window.location.reload();
                    },
                    error: function (error) {
                        console.log(error);

                        var msgBox = ''
                                + '<div class="row response-popup" style="padding-top: 10px;">'
                                + '    <div class="col-lg-12">'
                                + '        <div class="alert alert-danger alert-dismissable">'
                                + '            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">'
                                +               "&times;"
                                + '            </button>'
                                +               'Lagermenge konnte nicht gelöscht werden'
                                + '        </div>'
                                + '    </div>'
                                + '</div>';
                        $('.page-header').parents('.row').before(msgBox);
                        $('#delete-modal').modal('hide');
                    }
                });
            });
        });

        $('#users-modal').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var formAction = button.data('action');

            var modal = $(this);
            modal.find('.form').attr('action', formAction);

            var userSelect = modal.find('#users');
            var userData = [];

            userSelect.children().remove();

            $.getJSON(formAction).done(function (data) {
                $.each(data.data, function (index, user) {
                    var userName = user.first_name && user.last_name
                                    ? user.first_name + ' ' + user.last_name + ' (#' + user.id +')'
                                    : user.name + ' (#' + user.id +')';
                    userSelect.append($('<option/>', {
                        value: user.id,
                        text: userName,
                        selected: true
                    }));
                });
            }).done(function () {
                userSelect.select2({
                    width: '100%',
                    data: userData,
                    ajax: {
                        url: '{{ route('admin::super:users.all') }}' + '?roles[]=manager&roles[]=superadmin',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term,
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.data,
                                pagination: {
                                    more: params.page < data.meta.pagination.total_pages
                                }
                            };
                        },
                        cache: true
                    },
                    templateResult: function (user) {
                        if (user.loading) return user.name;
                        var userName = user.first_name && user.last_name
                                ? user.first_name + ' ' + user.last_name + ' (#' + user.id +')'
                                : user.name + ' (#' + user.id +')';

                        return "<div class='select2-result-admin'>" +
                            "       <div class='select2-result-admin__name'>" + userName + "</div>" +
                            "       <div class='select2-result-admin__address'>" + user.email + "</div>" +
                            "   </div>";
                    },
                    templateSelection: function (user) {
                        return user.text || (user.first_name && user.last_name
                                            ? user.first_name + ' ' + user.last_name + ' (#' + user.id +')'
                                            : user.name + ' (#' + user.id +')');
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    }
                });
            });
        });
    });
</script>
@endpush
