@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:product_stocks.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Lagermengen
                @if($stock->id)
                    bearbeiten <small>#{{ $stock->id }}</small>
                @else
                    hinzufügen
                @endif
            </h1>
            @if ($errors->count())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->product) !!}
                    {!! form_row($form->stock) !!}
                    {!! form_row($form->quantity) !!}
                    {!! form_row($form->unit) !!}
                    {!! form_row($form->notification_quantity) !!}
                    {!! form_row($form->delivery_time) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#stocks,#products').chosen({
            width: '100%'
        });
    });
</script>
@endpush