@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:stocks.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Lager
                @if($stock->id)
                    bearbeiten <small>{{ $stock->name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if ($errors->count())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->name) !!}
                    {!! form_row($form->country) !!}
                    {!! form_row($form->city) !!}
                    {!! form_row($form->street) !!}
                    {!! form_row($form->building) !!}
                    {!! form_row($form->rest_info) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Kontakte
                </div>
                <div class="panel-body">
                    {!! form_row($form->emails) !!}
                    {!! form_row($form->phones) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#country').chosen({
            width: '100%'
        });

        $('#emails').select2({
            width: '100%',
            tags: true,
            multiple: true,
            minimumInputLength: 6,
            tokenSeparators: [',', ';', ' ']
        });

        $('#phones').select2({
            width: '100%',
            tags: true,
            multiple: true,
            minimumInputLength: 3,
            tokenSeparators: [',', ';']
        });
    });
</script>
@endpush