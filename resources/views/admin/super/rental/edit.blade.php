@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:rentals.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Bootsverleih
                @if($rental->id)
                    bearbeiten<br/><small>{{ $rental->name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->name) !!}
                    {!! form_row($form->street) !!}
                    {!! form_row($form->area_code) !!}
                    {!! form_row($form->city) !!}
                    {!! form_row($form->country) !!}
                    {!! form_row($form->person) !!}
                    {!! form_row($form->phone) !!}
                    {!! form_row($form->email) !!}
                    {!! form_row($form->website) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $('document').ready(function() {
            $('#country').chosen();
        });
    </script>
@endpush