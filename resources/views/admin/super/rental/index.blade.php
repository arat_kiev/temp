@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Bootsverleihe
                <div class="pull-right">
                    <a href="{{ route('admin::super:rentals.create') }}" class="btn btn-success"><span class="fa fa-plus fa-fw"></span> Bootsverleih anlegen</a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-rentals">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Zuletzt bearbeitet</th>
                    <th>Admins</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- admin modal --}}
    <div class="modal fade in" id="admin-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            <span class="fa fa-group fa-fw"></span> Admins
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select class="form-control" id="admins" name="admins[]" multiple="multiple"></select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        var datatable = $('#datatable-rentals').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::super:rentals.data') }}',
            pagingType: "full_numbers",
            columns: [
                {data: 'id'},
                {data: 'name'},
                {data: 'updated_at'},
                {data: 'admin_count', sortable: false},
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });
    });

    $('#admin-modal').on('show.bs.modal', function(e) {
        var button = $(e.relatedTarget);
        var formAction = button.data('action');

        var modal = $(this);
        modal.find('.form').attr('action', formAction);

        var admSelect = modal.find('#admins');
        var admData = [];

        admSelect.children().remove();

        $.getJSON(formAction).done(function (data) {
            $.each(data.data, function (index, admin) {
                admSelect.append($('<option/>', {
                    value: admin.id,
                    text: admin.email,
                    selected: true
                }));
            });
        }).done(function () {
            admSelect.select2({
                width: '100%',
                data: admData,
                ajax: {
                    url: '{{ route('admin::super:users.all') }}',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            search: params.term,
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: params.page < data.meta.pagination.total_pages
                            }
                        };
                    },
                    cache: true
                },
                minimumInputLength: 3,
                templateResult: function (admin) {
                    if (admin.loading) { return admin.name; }

                    return "<div class='select2-result-admin'>" +
                        "<div class='select2-result-admin__name'>" + admin.name + " (" + admin.id + ")</div>" +
                        "<div class='select2-result-admin__address'>" + admin.email + "</div>";
                },
                templateSelection: function (admin) {
                    return admin.text || admin.email;
                },
                escapeMarkup: function (markup) { return markup; }
            });
        });
    });
</script>
@endpush

@push('styles')
<style>
    .select2-result-admin__name {
        font-size: 18px;
    }
</style>
@endpush
