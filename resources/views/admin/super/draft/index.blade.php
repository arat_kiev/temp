@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Vorschläge
                {{-- Select draft type --}}
                <div class="pull-right">
                    <select id="types" name="types[]"></select>
                </div>
            </h1>
        </div>
    </div>

    {{-- Table with drafts --}}
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-drafts">
                    <thead>
                     <tr>
                         <th>ID</th>
                         <th>Name</th>
                         <th>Typ</th>
                         <th>Aktionen</th>
                    </tr>
                     </thead>
                    <tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $('document').ready(function() {

            // DataTable
            var tbl = $('#datatable-drafts').DataTable({
                stateSave: true,
                processing: true,
                pagingType: "full_numbers",
                columns: [
                    { data: 'id', orderable: false, width: '5%', className: 'text-center'},
                    { data: 'name', orderable: false, width: '80%'},
                    { data: 'type', orderable: false, searchable: false, width: '10%', className: 'text-center'},
                    { data: 'action', name: 'action', orderable: false, searchable: false, width: '5%', className: 'text-center'}
                ],
                language: {
                    decimal: ",",
                    thousands: ".",
                    lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                    zeroRecords: "Keine Einträge vorhanden",
                    info: "Seite _PAGE_ von _PAGES_",
                    infoEmpty: "Keine Einträge vorhanden",
                    infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                    search: "Suche",
                    paginate: {
                        first: "&laquo;",
                        previous: "&lsaquo;",
                        next: "&rsaquo;",
                        last: "&raquo;"
                    }
                }
            });

            // Fill select
            var selectType = $('#types').select2({
                width: 'resolve',
                placeholder: "Please select a type",
                data: {!! $types !!}
            });

            // Select change listener
            selectType.on("select2:select", function (e) {
                tbl.ajax.url('{{ route('admin::super:draft.data') }}' + '/' + selectType.val()).load(null, false);
            });

        });
    </script>
@endpush

@push('styles')
    <style>
        .selection {
            font-size: 16px;
        }
        #types {
            min-width: 250px;
        }
    </style>
@endpush
