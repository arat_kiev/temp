@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Kommentare
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4 pull-right">
            <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control" name="status" id="filter-status">
                    @foreach($statusesList as $value => $name)
                        <option
                                value="{{ $value }}"
                                @if($value == $status)
                                selected="selected"
                                @endif
                        >
                            {{ $name }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-comments">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Status</th>
                    <th>Benutzer</th>
                    <th>Typ</th>
                    <th>Kommentar</th>
                    <th>Aktionen</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        function updateFilters() {
            var status = $('#filter-status').val();

            var location = '{{ route('admin::super:reports.comments.index') }}';
            var params = {
                'status': status
            };

            console.log(location + '?' + $.param(params));

            window.location = location + '?' + $.param(params);
        }

        $('#filter-status').on('change', updateFilters);

        var $table = $('#datatable-comments').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            searching: false,
            ajax: {
                url: '{{ route('admin::super:reports.comments.data') }}' + window.location.search
            },
            pagingType: "full_numbers",
            columns: [
                { data: 'id' , width: '5%'},
                { data: 'status', width: '5%' },
                { data: 'user.name', sortable: false , width: '15%'},
                { data: 'type', sortable: false },
                { data: 'body', sortable: false, width: '30%' },
                { data: 'actions', sortable: false, width: '15%' }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            },
            createdRow: function(row, data) {
                if (data['status'] == "accepted") {
                    $(row).addClass('success');
                } else if (data['status'] == 'review') {
                    $(row).addClass('warning');
                } else if (data['status'] == 'rejected') {
                    $(row).addClass('danger');
                }
            }
        });

        $('#datatable-comments tbody').on('click', 'button.action', function (e) {
            e.preventDefault();
            var url = $(this).attr('data-action'),
                action = $(this).val();

            $.ajax({
                'type' : 'POST',
                'url': url,
                'data': {action: action},
                'success' : function() {
                    $table.ajax.reload();
                }
            });
        });
    });
</script>
@endpush