@extends('admin.layout.default')

@push('styles')
<style>
    table, td {
        padding: 5px 15px;
    }
    tr > td:first-child {
        font-weight: bold;
    }
</style>
@endpush

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default back-btn" href="{{ route('admin::super:reports.comments.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Kommentar <small>#{{ $comment->id }}</small>
            </h1>
            @if($comment->status == 'review' && $comment->report()->user)
                <h4>Meldung von {{ $comment->report()->user->first_name }} {{ $comment->report()->user->last_name }}</h4>
            @elseif($comment->status == 'review')
                <h4>Meldung hat vom System generiert worden</h4>
            @else
                <h4>Status: {{ $comment->status }}</h4>
            @endif
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    <table>
                        <tr>
                            <td>Benutzer</td>
                            <td>
                                @if($comment->user)
                                    {{ $comment->user->first_name }} {{ $comment->user->last_name }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Typ</td>
                            <td>{{ $type }}</td>
                        </tr>
                        <tr>
                            <td>Kommentar</td>
                            <td>
                                {{ $comment->body }}
                            </td>
                        </tr>
                        <tr>
                            <td>Kommentardatum/Zeit</td>
                            <td>
                                @if($comment->created_at)
                                    {{ $comment->created_at->format('d.m.Y H:i:s') }}
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        @if(in_array($comment->status, ['review', 'rejected']) && $comment->report() && $comment->report()->description)
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Meldung Beschreibung
                    </div>
                    <div class="panel-body">
                        {{ $comment->report()->description }}
                    </div>
                </div>
            </div>
        @endif
        <div class="col-lg-12">
            @if(in_array($comment->status, ['review', 'rejected']))
                <button value="accepted" class="btn btn-success action"
                        data-action="{{route('admin::super:reports.comments.update', ['commentId' => $comment->id])}}">
                    <span class="fa fa-check fa-fw"></span> Alles in Ordnung
                </button>
            @endif
            @if(in_array($comment->status, ['review', 'accepted']))
                <button value="rejected" class="btn btn-danger action"
                        data-action="{{route('admin::super:reports.comments.update', ['commentId' => $comment->id])}}">
                    <span class="fa fa-times fa-fw"></span> Kommentar blockieren
                </button>
            @endif
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('button.action').on('click', function (e) {
            e.preventDefault();
            var url = $(this).attr('data-action'),
                action = $(this).val();

            $.ajax({
                'type' : 'POST',
                'url': url,
                'data': {action: action},
                'success' : function() {
                    var backLink = $('a.back-btn').attr('href');
                    window.location.href = backLink;
                }
            });
        });
    });
</script>
@endpush
