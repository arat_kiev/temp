@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Fänge
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4 pull-right">
            <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control" name="status" id="filter-status">
                    @foreach($statusesList as $value => $name)
                        <option
                            value="{{ $value }}"
                            @if($value == $status)
                                selected="selected"
                            @endif
                        >
                            {{ $name }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-hauls">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Status</th>
                    <th>Fischart</th>
                    <th>Fischer</th>
                    <th>Gewässer</th>
                    <th>Fangdatum/Zeit</th>
                    <th>Bild</th>
                    <th>Aktionen</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        function updateFilters() {
            var status = $('#filter-status').val();

            var location = '{{ route('admin::super:reports.hauls.index') }}';
            var params = {
                'status': status
            };

            // console.log(location + '?' + $.param(params));
            window.location = location + '?' + $.param(params);
        }

        $('#filter-status').on('change', updateFilters);

        $('#datatable-hauls').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            searching: false,
            ajax: {
                url: '{{ route('admin::super:reports.hauls.data') }}' + window.location.search
            },
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'status' },
                { data: 'fish.name', sortable: false },
                { data: 'user.full_name', sortable: false },
                { data: 'area.name', sortable: false },
                { data: 'catch_date' },
                { data: 'picture', width: '30px', sortable: false },
                { data: 'actions', sortable: false, width: '5%' }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            },
            createdRow: function(row, data) {
                if (data['status'] == "accepted") {
                    $(row).addClass('success');
                } else if (data['status'] == 'review') {
                    $(row).addClass('warning');
                } else if (data['status'] == 'rejected') {
                    $(row).addClass('danger');
                }
            }
        });
    });
</script>
@endpush