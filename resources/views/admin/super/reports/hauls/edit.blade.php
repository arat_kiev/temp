@extends('admin.layout.default')

@push('styles')
<style>
    table, td {
        padding: 5px 15px;
    }
    tr > td:first-child {
        font-weight: bold;
    }
</style>
@endpush

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default back-btn" href="{{ route('admin::super:reports.hauls.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Fang <small>#{{ $haul->id }}</small>
            </h1>
            @if($haul->status == 'review' && $haul->report()->user)
                <h4>Meldung von {{ $haul->report()->user->first_name }} {{ $haul->report()->user->last_name }}</h4>
            @elseif($haul->status == 'review')
                <h4>Meldung hat vom System generiert worden</h4>
            @else
                <h4>Status: {{ $haul->status }}</h4>
            @endif
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    <table>
                        <tr>
                            <td>Fischart</td>
                            <td>{{ $haul->fish ? $haul->fish->name : 'Leermeldung' }}</td>
                        </tr>
                        <tr>
                            <td>Größe/Gewicht</td>
                            <td>
                                @if($haul->fish)
                                    {{ $haul->size_value }} {{ $haul->size_type }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Fischer</td>
                            <td>
                                @if($haul->user)
                                    {{ $haul->user->first_name }} {{ $haul->user->last_name }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Angelkarten-Nr.</td>
                            <td>{{ $haul->ticket_number }}</td>
                        </tr>
                        <tr>
                            <td>Gewässer</td>
                            <td>{{ $haul->area ? $haul->area->name : '' }}</td>
                        </tr>
                        <tr>
                            <td>Fangdatum/Zeit</td>
                            <td>
                                {{ $haul->catch_date->format('d.m.Y') }}
                                @if($haul->catch_time)
                                    - {{ $haul->catch_time }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Technik</td>
                            <td>{{ $haul->technique ? $haul->technique->name : '' }}</td>
                        </tr>
                        <tr>
                            <td>Kommentare</td>
                            <td>{{ $haul->user_comment ?: '' }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Bild
                </div>
                <div class="panel-body">
                    @if($haul->picture)
                        @if(ends_with($haul->picture->file, '.pdf'))
                            <div class="col-md-12">
                                <a href="{{ route('filestream', ['file' => $haul->picture->name]) }}" target="_blank">Anhang (PDF)</a>
                            </div>
                        @else
                            <div class="col-sm-6">
                                <a href="{{ route('imagecache', ['template' => 'org', 'filename' => $haul->picture->file]) }}"
                                   data-toggle="lightbox"
                                   data-title="Original">
                                    <img src="{{ route('imagecache', ['template' => 'lre', 'filename' => $haul->picture->file]) }}"
                                         alt="{{ $haul->picture->name }}"
                                         width="600"
                                         class="img-responsive img-thumbnail" />
                                </a>
                            </div>
                        @endif
                    @else
                        Es gibt kein Bild
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        @if(in_array($haul->status, ['review', 'rejected']) && $haul->report() && $haul->report()->description)
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Meldung Beschreibung
                    </div>
                    <div class="panel-body">
                        {{ $haul->report()->description }}
                    </div>
                </div>
            </div>
        @endif
        <div class="col-lg-12">
            @if(in_array($haul->status, ['review', 'rejected']))
                <button value="accepted" class="btn btn-success action"
                        data-action="{{route('admin::super:reports.hauls.update', ['haulId' => $haul->id])}}">
                    <span class="fa fa-check fa-fw"></span> Alles in Ordnung
                </button>
            @endif
            @if(in_array($haul->status, ['review', 'accepted']))
                <button value="rejected" class="btn btn-danger action"
                        data-action="{{route('admin::super:reports.hauls.update', ['haulId' => $haul->id])}}">
                    <span class="fa fa-times fa-fw"></span> Fang blockieren
                </button>
            @endif
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('button.action').on('click', function (e) {
            e.preventDefault();
            var url = $(this).attr('data-action'),
                action = $(this).val();

            $.ajax({
                'type' : 'POST',
                'url': url,
                'data': {action: action},
                'success' : function() {
                    var backLink = $('a.back-btn').attr('href');
                    window.location.href = backLink;
                }
            });
        });
    });
</script>
@endpush
