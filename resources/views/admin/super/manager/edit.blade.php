@extends('admin.layout.default')

@if($manager->id)
    @include('admin.common.draft', [
        'drafted' => $manager->draft,
        'publish' => route('admin::super:managers.publish', ['managersId' => $manager->id]),
        'delete' => route('admin::super:managers.delete', ['managersId' => $manager->id]),
        'list' => route('admin::super:managers.index')])
@endif

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::super:managers.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Bewirtschafter
                @if($manager->id)
                    bearbeiten<br/><small>{{ $manager->name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <a href="{{ route('admin::super:invoices.index', ['manager' => $manager]) }}" class="btn btn-info pull-right">
                Zu den Abrechnungen
                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
            </a>
        </div>
        <br><br>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->name) !!}
                    {!! form_row($form->street) !!}
                    {!! form_row($form->area_code) !!}
                    {!! form_row($form->city) !!}
                    {!! form_row($form->country) !!}
                    {!! form_row($form->person) !!}
                    {!! form_row($form->phone) !!}
                    {!! form_row($form->email) !!}
                    {!! form_row($form->website) !!}
                    {!! form_row($form->commission_included) !!}
                    {!! form_row($form->uid) !!}
                    {!! form_row($form->account_number) !!}
                    {!! form_row($form->short_code) !!}
                    <div class="form-group">
                        <label for="{{ $form->ticket_info->getName() }}" class="control-label">
                            {{ $form->ticket_info->getOption('label') }}
                            <a href="{{ asset('img/admin/manager_text_ref.jpg') }}" data-title="Text auf Angelkarte (rechts)" data-toggle="lightbox">
                                <span class="fa fa-info-circle fa-fw"></span>
                            </a>
                        </label>
                        {!! form_widget($form->ticket_info) !!}
                    </div>
                    {!! form_row($form->storno_emails) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Logo
                </div>
                <div class="panel-body">
                    @if($manager->logo)
                        <img src="{{ route('imagecache', ['template' => 'lLogo', 'filename' => $manager->logo->file]) }}" class="thumbnail img-responsive" />
                    @endif
                    {!! form_row($form->logo) !!}
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Bestimmungen
                </div>
                <div class="panel-body">
                    <div class="alert alert-info">
                        @include('admin.partials.rules.general')
                    </div>
                    {!! form_row($form->rule_text) !!}
                    <div class="form-group">
                        {!! form_label($form->rule_files_public) !!}
                        <div class="help-block">
                            @include('admin.partials.rules.public')
                        </div>
                        {!! form_widget($form->rule_files_public) !!}
                    </div>
                    @foreach($errors->getMessages() as $error => $messages)
                        @if(starts_with($error, 'rule_files_public.'))
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($messages as $msg)
                                        <li>{{ $msg }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    @endforeach
                    <div class="form-group">
                        {!! form_label($form->rule_files) !!}
                        <div class="help-block">
                            @include('admin.partials.rules.private')
                        </div>
                        {!! form_widget($form->rule_files) !!}
                    </div>
                    @foreach($errors->getMessages() as $error => $messages)
                        @if(starts_with($error, 'rule_files.'))
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($messages as $msg)
                                        <li>{{ $msg }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    @endforeach
                    <div class="form-group">
                        <label>Vorhandene Dateien</label>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Datei</th>
                                <th>Öffentlich</th>
                                <th>Löschen</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($manager->rule && $manager->rule->files()->count() > 0)
                                @foreach($manager->rule->files as $file)
                                    <tr>
                                        <td>
                                            {!! Html::linkRoute('filestream', $file->fileName, ['name' => $file->filePathWithName], ['target' => '_blank']) !!}
                                        </td>
                                        <td>
                                            <span class="fa {{ $file->pivot->public ? 'fa-check' : 'fa-times' }} fa-fw"></span>
                                        </td>
                                        <td>
                                            {!! Form::checkbox('rule_files_delete[]', $file->id) !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3" class="text-center">
                                        Keine Dateien vorhanden
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $('document').ready(function() {
            $('#country').chosen();
            $('#storno-emails').select2({
                width: '100%',
                tags: true,
                multiple: true,
                minimumInputLength: 6,
                tokenSeparators: [',', ';', ' ']
            });

            $('#ticket_info,#rule_text').summernote({
                lang: 'de-DE',
                height: '150px',
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'clear']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['link', ['linkDialogShow', 'unlink']],
                    ['misc', ['fullscreen', 'codeview', 'undo', 'redo']]
                ]
            });

            $(".rule_files").change(function () {
                $("input[name^=rule_files_name]").each(function () {
                    $(this).prev('label').remove();
                    $(this).remove();
                });
                var names = [];
                for (var i = 0; i < $(this).get(0).files.length; ++i) {
                    names.push($(this).get(0).files[i].name);
                    // console.log($(this).get(0).files[i].name);
                    var block = "" +
                        "<div class='form-group'>" +
                            "<label for='Dateiname' class='control-label'>" +
                                "Dateiname: " + $(this).get(0).files[i].name +
                            "</label>" +
                            "<input class='form-control' " +
                                    "pattern='^[\\w,\\s-]+\\.?[A-Za-z]{0,3}$' " +
                                    "type='text' " +
                                    "name='rule_files_name[" + $(this).get(0).files[i].name + "]' " +
                                    "value='" +$(this).get(0).files[i].name + "'/>" +
                        "</div>";
                    $(block).insertAfter($(this).parent());
                }
            });

            $(".rule_files_public").change(function(){
                $("input[name^=rule_public_files_name]").each(function(){
                    $(this).prev('label').remove();
                    $(this).remove();
                });
                var names = [];
                for (var i = 0; i < $(this).get(0).files.length; ++i) {
                    names.push($(this).get(0).files[i].name);
                    // console.log($(this).get(0).files[i].name);
                    var block = "" +
                        "<div class='form-group'>" +
                            "<label for='Dateiname' class='control-label'>" +
                                "Dateiname: " + $(this).get(0).files[i].name +
                            "</label>" +
                            "<input class='form-control' " +
                                    "pattern='^[\\w,\\s-]+\\.?[A-Za-z]{0,3}$' " +
                                    "type='text' " +
                                    "name='rule_public_files_name[" + $(this).get(0).files[i].name + "]' " +
                                    "value='" +$(this).get(0).files[i].name + "'/>" +
                        "</div>";
                    $(block).insertAfter($(this).parent());
                }
            });
        });
    </script>
@endpush