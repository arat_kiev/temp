@extends('admin.layout.default')

@section('page-content')
    @if($errors->any())
        <div class="row" style="padding-top: 10px;">
            <div class="col-lg-12">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>RKSV-Anbindung fehlgeschlagen!</b> Es wurden nicht alle Felder ausgefüllt.
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Bewirtschafter
                <div class="pull-right">
                    <a href="{{ route('admin::super:managers.create') }}" class="btn btn-success"><span class="fa fa-plus fa-fw"></span> Bewirtschafter anlegen</a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-managers">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Zuletzt bearbeitet</th>
                    <th>Admins</th>
                    <th>RKSV</th>
                    <th>Provisionen</th>
                    <th>Links</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- rksv modal --}}
    <div class="modal fade in" id="rksv-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                {!! form_start($rksvForm) !!}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            <span class="fa fa-link fa-fw"></span> Registrierkassen-Anbindung
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <fieldset>
                                    <legend>
                                        Unternehmen
                                        <button class="btn btn-xs btn-default pull-right" id="btn-load-manager"
                                                data-action="{{ route('admin::super:managers.rksv.data', [
                                                    'managerId' => '__manager__',
                                                ]) }}">
                                            <span class="fa fa-clipboard fa-fw"></span>
                                            von Bewirtschafter
                                        </button>
                                    </legend>
                                    {!! form_row($rksvForm->company_name) !!}
                                    {!! form_row($rksvForm->company_street) !!}
                                    {!! form_row($rksvForm->company_plz) !!}
                                    {!! form_row($rksvForm->company_city) !!}
                                    {!! form_row($rksvForm->company_country) !!}
                                </fieldset>
                                <fieldset>
                                    <legend>
                                        Benutzer

                                        <div class="pull-right">
                                            <select class="form-control input-sm" id="select-load-user">
                                                <option value="">=== von Benutzer ===</option>
                                            </select>
                                        </div>
                                    </legend>
                                    {!! form_row($rksvForm->user_first_name) !!}
                                    {!! form_row($rksvForm->user_last_name) !!}
                                    {!! form_row($rksvForm->user_email) !!}
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {!! form_widget($rksvForm->submit) !!}
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <span class="fa fa-times fa-fw"></span> Abbrechen
                        </button>
                    </div>
                {!! form_end($rksvForm) !!}
            </div>
        </div>
    </div>

    {{-- activation modal --}}
    <div class="modal fade in" id="activation-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            <span class="fa fa-warning fa-fw"></span> RKSV
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="#" class="btn btn-lg btn-block btn-primary" id="activation-button"></a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- admin modal --}}
    <div class="modal fade in" id="admin-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            <span class="fa fa-group fa-fw"></span> Admins
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select class="form-control" id="admins" name="admins[]" multiple="multiple"></select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal ticket commission dialog --}}
    <div class="modal fade" id="tickets-modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="pull-right">
                        <button type="button" class="btn btn-success submit-edit">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <span class="fa fa-close fa-fw"></span> Abbrechen
                        </button>
                    </div>
                    <h4 class="modal-title">Angelkarten Provisionen</h4>
                </div>
                <div class="modal-body">
                    <div class="load" style="text-align: center;">
                        <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
                    </div>
                    <div class="tree-view"></div>
                </div>
                <div class="modal-footer" style="clear:both;">
                    <button type="button" class="btn btn-success submit-edit">
                        <span class="fa fa-save fa-fw"></span> Speichern
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <span class="fa fa-close fa-fw"></span> Abbrechen
                    </button>
                </div>
            </div>
        </div>
    </div>

    {{-- modal product commission dialog --}}
    <div class="modal fade" id="products-modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Produkte Provisionen</h4>
                </div>
                <div class="modal-body">
                    <div class="load" style="text-align: center;">
                        <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
                    </div>
                    <div class="tree-view"></div>
                </div>
                <div class="modal-footer" style="clear:both;">
                    <button type="button" class="btn btn-success submit-edit">
                        <span class="fa fa-save fa-fw"></span> Speichern
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <span class="fa fa-close fa-fw"></span> Abbrechen
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
{!! Html::script('lib/bootstrap-treeview/dist/bootstrap-treeview.min.js', ['nounce' => csp_nounce()]) !!}
<script nonce="{{ csp_nounce() }}">
    Object.filter = function (obj, predicate) {
        return Object.keys(obj)
            .filter(function (key) {
                return predicate(obj[key]);
            })
            .reduce(function (res, key) {
                return Object.assign(res, { [key]: obj[key] });
            }, {});
    };

    $.postJSON = function(url, data, callback, error) {
        return jQuery.ajax({
            type : 'POST',
            url: url,
            data: data,
            dataType : 'json',
            success : callback,
            error : error
        });
    };

    $('document').ready(function() {
        var datatable = $('#datatable-managers').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::super:managers.data') }}',
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'name' },
                { data: 'updated_at' },
                { data: 'admin_count', sortable: false },
                { data: 'rksv', sortable: false },
                { data: 'commissions', sortable: false, searchable: false },
                { data: 'links', sortable: false, searchable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#admin-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var formAction = button.data('action');

            var modal = $(this);
            modal.find('.form').attr('action', formAction);

            var admSelect = modal.find('#admins');
            var admData = [];

            admSelect.children().remove();

            $.getJSON(formAction).done(function (data) {
                $.each(data.data, function (index, admin) {
                    admSelect.append($('<option/>', {
                        value: admin.id,
                        text: admin.email,
                        selected: true
                    }));
                });
            }).done(function () {
                admSelect.select2({
                    width: '100%',
                    data: admData,
                    ajax: {
                        url: '{{ route('admin::super:users.all') }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term,
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.data,
                                pagination: {
                                    more: params.page < data.meta.pagination.total_pages
                                }
                            };
                        },
                        cache: true
                    },
                    minimumInputLength: 3,
                    templateResult: function (admin) {
                        if (admin.loading) { return admin.name; }

                        return "<div class='select2-result-admin'>" +
                                "<div class='select2-result-admin__name'>" + admin.name + " (" + admin.id + ")</div>" +
                                "<div class='select2-result-admin__address'>" + admin.email + "</div>";
                    },
                    templateSelection: function (admin) {
                        return admin.text || admin.email;
                    },
                    escapeMarkup: function (markup) { return markup; }
                });
            });
        });

        $('#rksv-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var managerId = button.data('manager');

            var modal = $(this);
            var form = modal.find('form');
            var formAction = '{{ route('admin::super:managers.rksv.create', ['managerId' => '__manager__']) }}';

            form.data('manager', managerId);
            form.attr('action', formAction.replace('__manager__', managerId));
            form.trigger('reset');

            var selectUser = $('#select-load-user');
            selectUser.children().not('option[value=""]').remove();

            var usersAction = "{{ route('admin::super:managers.admins.index', ['managerId' => '__manager__']) }}";

            $.getJSON(usersAction.replace('__manager__', managerId)).done(function (response) {
                response.data.forEach(function(user) {
                    selectUser.append('<option value="' + user.id + '">' + user.email + '</option>');
                });
            });
        });

        $('#activation-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var managerId = button.data('manager');
            var status = button.data('status');

            var modal = $(this);
            var actionButton = modal.find('#activation-button')[0];
            var action = status === 'active' ?
                '{{ route('admin::super:managers.rksv.deactivate', ['managerId' => '__manager__']) }}' :
                '{{ route('admin::super:managers.rksv.activate', ['managerId' => '__manager__']) }}';
            action = action.replace('__manager__', managerId);

            actionButton.text = status === 'active' ? 'Deaktivieren' : 'Aktivieren';
            actionButton.href = action;
        });

        $('#btn-load-manager').on('click', function(e) {
            e.preventDefault();

            var button = $(this);
            var buttonIcon = button.children('.fa');
            var form = button.closest('form');
            var managerId = form.data('manager');
            var formAction = button.data('action').replace('__manager__', managerId);

            buttonIcon.removeClass('fa-clipboard').addClass('fa-spinner fa-pulse');

            $.getJSON(formAction).done(function (data) {
                form.find('#company_name').val(data.name);
                form.find('#company_street').val(data.street);
                form.find('#company_plz').val(data.area_code);
                form.find('#company_city').val(data.city);
                form.find('#company_country').val(data.country_code).change();

                buttonIcon.removeClass('fa-spinner fa-pulse').addClass('fa-clipboard');
            });
        });

        $('#select-load-user').on('change', function(e) {
            var button = $(this);
            var form = button.closest('form');

            var userId = $(this).val();
            var route = '{{ route('admin::super:managers.rksv.user', ['id' => '__user_id__']) }}'.replace('__user_id__', userId);

            if (userId) {
                $.getJSON(route).done(function (data) {
                    form.find('#user_first_name').val(data.first_name);
                    form.find('#user_last_name').val(data.last_name);
                    form.find('#user_email').val(data.email);
                });
            }
        });

        $('#tickets-modal').on('show.bs.modal', function(e) {
            // load commission tree
            var modal = this;
            var managerId = $(e.relatedTarget).data('record');
            var treeUrl = "{{ route('admin::super:managers.commissions.tickets.tree', ['managerId' => '__MANAGER_ID__']) }}"
                    .replace(/__MANAGER_ID__/gi, managerId);
            $.getJSON(treeUrl, function (res) {
                buildCommissionTree(res, modal);
                $(modal).off('click', '.submit-edit').on('click', '.submit-edit', function (e) {
                    e.preventDefault();
                    var params = {
                        managers: collectCommissionData(modal, 'managers'),
                        areas: collectCommissionData(modal, 'areas'),
                        ticket_types: collectCommissionData(modal, 'ticket_types'),
                        ticket_prices: collectCommissionData(modal, 'ticket_prices')
                    };
                    $.postJSON(treeUrl, params, function (response) {
                        console.log(response);
                        window.location.reload();
                    });
                });
            });
        });

        $('#products-modal').on('show.bs.modal', function(e) {
            // load commission tree
            var modal = this;
            var managerId = $(e.relatedTarget).data('record');
            var treeUrl = "{{ route('admin::super:managers.commissions.products.tree', ['managerId' => '__MANAGER_ID__']) }}"
                    .replace(/__MANAGER_ID__/gi, managerId);
            $.getJSON(treeUrl, function (res) {
                buildCommissionTree(res, modal);
                $(modal).off('click', '.submit-edit').on('click', '.submit-edit', function (e) {
                    e.preventDefault();
                    var params = {
                        managers: collectCommissionData(modal, 'managers'),
                        products: collectCommissionData(modal, 'products'),
                        product_prices: collectCommissionData(modal, 'product_prices')
                    };
                    $.postJSON(treeUrl, params, function (response) {
                        console.log(response);
                        window.location.reload();
                    });
                });
            });
        });

        $('#tickets-modal,#products-modal').on('show.bs.modal', function(e) {
            $(this).find('.load').show();
            $(this).find('.tree-view').text('');
        });

        function collectCommissionData(modal, object) {
            var elements = $(modal).find('.tree-form[data-object="'+object+'"]'),
                    data = [];

            elements.each(function (index) {
                var $form = $(elements[index]);
                data.push(collectRawCommissionData($form));
            });

            // return data;
            return data.filter(function (item) {
                item.commissions = Object.filter(item.commissions, function (commission) {
                    return commission.commission_value != '';
                });
                return Object.keys(item.commissions).length;
            });
        }

        function collectRawCommissionData($form) {
            return {
                id: $form.data('id'),
                commissions: {
                    online: {
                        commission_value: $form.find('.online-value').val(),
                        commission_type: $form.find('.online-type').val(),
                        min_value: $form.find('.online-min').val()
                    },
                    reseller: {
                        commission_value: $form.find('.reseller-value').val(),
                        commission_type: $form.find('.reseller-type').val(),
                        min_value: $form.find('.reseller-min').val()
                    }
                }
            };
        }

        function buildCommissionTree(data, modal) {
            var treeData = getCommissionTree(data);
            $(modal).find('.load').hide();
            var $treeView = $(modal).find('.tree-view');
            $treeView.treeview({
                data: [treeData],
                levels: 5,
                highlightSelected: false
            });
            $treeView.treeview('expandAll', { silent: true });
            initTreeEventBinders(modal);
        }

        function initTreeEventBinders(modal) {
            $(modal).find('.list-group-item').off('click').on('click', function (e) {
                e.preventDefault();
                var childrenBlocks = getChildrenBlocks(modal, $(this), true);
                var isCollapsed = false;

                if (childrenBlocks.length) {
                    $(this).find('.expand-icon').toggleClass("glyphicon-minus").toggleClass("glyphicon-plus");
                    getChildrenBlocks(modal, $(this)).forEach(function ($child) {
                        isCollapsed = !$child.is(':visible');
                    });
                }

                childrenBlocks.forEach(function ($item) {
                    if (isCollapsed) {
                        $item.show();
                        $item.find('.expand-icon').addClass("glyphicon-minus").removeClass("glyphicon-plus");
                    } else {
                        $item.hide();
                        $item.find('.expand-icon').addClass("glyphicon-plus").removeClass("glyphicon-minus");
                    }
                });

                return false;
            });
            $(modal).find('.tree-block').off('click', '.data-edit').on('click', '.data-edit', function (e) {
                e.preventDefault();
                var $form = $(this).closest('.list-group-item').find('.tree-form');
                var $dataRaw = $(this).parent().find('.tree-data');
                updateCommissionLine($form, $dataRaw);

                $form.toggle();
                $dataRaw.toggle();
                return false;
            });
            $(modal).find('.tree-form').off('click', 'input,select').on('click', 'input,select', function (e) {
                e.preventDefault();
                return false;
            });
            $(modal).find('.undertake-value').off('click').on('click', function (e) {
                e.preventDefault();

                var $elem = $(e.target),
                    $nodeBlock = $elem.parent().parent(),
                    $form = $nodeBlock.find('.tree-form'),
                    data = collectRawCommissionData($form);

                getChildrenBlocks(modal, $nodeBlock, true).forEach(function ($item, i) {
                    var $form = $item.find('.tree-form'),
                        $dataRaw = $item.find('.tree-data'),
                        wasUpdated = updateChildrenForm($form, data);
                    if (wasUpdated) {
                        updateCommissionLine($form, $dataRaw);
                    }
                });

                return false;
            });
        }

        function updateChildrenForm($form, data) {
            var wasUpdated = false;
            var rawDataCommissions = collectRawCommissionData($form).commissions;

            if (!rawDataCommissions.online.commission_value.length && data.commissions.online.commission_value.length) {
                $form.find('.online-value').val(data.commissions.online.commission_value);
                $form.find('.online-type').val(data.commissions.online.commission_type);
                wasUpdated = true;
            }

            if (!rawDataCommissions.online.min_value.length && data.commissions.online.min_value.length) {
                $form.find('.online-min').val(data.commissions.online.min_value);
                wasUpdated = true;
            }

            if (!rawDataCommissions.reseller.commission_value.length && data.commissions.reseller.commission_value.length) {
                $form.find('.reseller-value').val(data.commissions.reseller.commission_value);
                $form.find('.reseller-type').val(data.commissions.reseller.commission_type);
                wasUpdated = true;
            }

            if (!rawDataCommissions.reseller.min_value.length && data.commissions.reseller.min_value.length) {
                $form.find('.reseller-min').val(data.commissions.reseller.min_value);
                wasUpdated = true;
            }

            return wasUpdated;
        }

        function updateCommissionLine($form, $dataRaw) {
            var data = collectRawCommissionData($form);

            $dataRaw.find('.online-data').text(
                    data.commissions.online ? getCommissionLine(data.commissions.online) : '- €'
            );
            $dataRaw.find('.reseller-data').text(
                    data.commissions.reseller ? getCommissionLine(data.commissions.reseller) : '- €'
            );
        }

        function getChildrenBlocks(modal, $nodeBlock, isRecursive) {
            var node = $(modal).find('.tree-view').treeview('getNode', $nodeBlock.data('nodeid'));
            var children = [];

            if (node.nodes && node.nodes.length && Array.isArray(node.nodes)) {
                node.nodes.forEach(function (item, index) {
                    var $block = $(modal).find('li[data-nodeid='+item.nodeId+']');
                    children.push($block);

                    if (isRecursive) {
                        var nodeChildren = getChildrenBlocks(modal, $block, isRecursive);
                        children = children.concat(nodeChildren);
                    }
                });
            }

            return children;
        }

        function getCommissionTree(item) {
            return {
                text: getCommissionTreeTemplate(item),
                nodes: item.children.length ? getCommissionTreeData(item.children) : undefined,
                backColor: getTreeObjectColor(item.object),
                data: {params: 1},
                selectable: true
            };
        }

        function getCommissionTreeData(items) {
            return items.map(function (item) {
                return getCommissionTree(item);
            });
        }

        function getCommissionTreeTemplate(item) {
            return '    <div class="tree-block">'
                    +       item.name
                    + '     <button class="btn btn-xs btn-success data-edit">'
                    + '         <i class="fa fa-edit fa-fw"></i> '
                    + '     </button>'
                    + '     <div class="tree-data">'
                    + '         <i class="fa fa-credit-card fa-fw"></i> '
                    + '         <span class="online-data">'
                    +               (item.commissions.online ? getCommissionLine(item.commissions.online) : '- €')
                    + '         </span>'
                    + '         <span class="fa fa-shopping-cart fa-fw"></span> '
                    + '         <span class="reseller-data">'
                    +               (item.commissions.reseller ? getCommissionLine(item.commissions.reseller) : '- €')
                    + '         </span>'
                    + '     </div>'
                    + ' </div>'
                    + (item.children.length
                        ? ' <span class="pull-right">'
                        + '     <button class="btn btn-xs btn-default undertake-value">'
                        + '         <i class="fa fa-chevron-down fa-fw"></i> Übernehmen'
                        + '     </button>'
                        + ' </span>'
                        : '')
                    + ' <div class="tree-form" data-object="'+item.object+'" data-id="'+item.id+'">'
                    +       getCommissionForm(item)
                    + ' </div>'
                    + ' <div style="clear=both"></div>';
        }

        function getCommissionLine(commission) {
            return commission.commission_value
                ? (commission.commission_value
                    + getCommisionMark(commission.commission_type)
                    + ', min. ' + (commission.min_value || '- ') + '€')
                : '- €';
        }

        function getCommissionForm(item) {
            return ''
                + '<form class="row">'
                + '     <div class="col-sm-4 text-right"><b>Online</b></div>'
                + '     <div class="form-group col-sm-3">'
                + '         <input  type="number"'
                + '                 class="form-control input-sm online-value"'
                + '                 data-id="'+item.id+'"'
                + '                 value="'+(item.commissions.online
                                        ? item.commissions.online.commission_value.toString().replace(/,/g, '.')
                                        : '')+'"'
                + '                 placeholder="Online Provision">'
                + '     </div>'
                + '     <div class="form-group col-sm-2">'
                + '         <select class="form-control input-sm online-type"'
                + '                 data-id="'+item.id+'">'
                + '             <option value="RELATIVE" '
                +                   (item.commissions.online && item.commissions.online.commission_type === 'RELATIVE'
                                        ? 'selected="selected"'
                                        : '')+'>%</option>'
                + '             <option value="ABSOLUTE" '
                +                   (item.commissions.online && item.commissions.online.commission_type === 'ABSOLUTE'
                                        ? 'selected="selected"'
                                        : '')+'>€</option>'
                + '         </select>'
                + '     </div>'
                + '     <div class="form-group col-sm-3">'
                + '         <input  type="number"'
                + '                 class="form-control input-sm online-min"'
                + '                 data-id="'+item.id+'"'
                + '                 value="'+(item.commissions.online
                                        ? item.commissions.online.min_value.toString().replace(/,/g, '.')
                                        : '')+'"'
                + '                 placeholder="Min. Preis (in €)">'
                + '     </div>'
                + '     <div class="col-sm-4 text-right"><b>Verkaufsstellen</b></div>'
                + '     <div class="form-group col-sm-3">'
                + '         <input  type="number"'
                + '                 class="form-control input-sm reseller-value"'
                + '                 data-id="'+item.id+'"'
                + '                 value="'+(item.commissions.reseller
                                        ? item.commissions.reseller.commission_value.toString().replace(/,/g, '.')
                                        : '')+'"'
                + '                 placeholder="Verkaufsstellen Provision">'
                + '     </div>'
                + '     <div class="form-group col-sm-2">'
                + '         <select class="form-control input-sm reseller-type"'
                + '                 data-id="'+item.id+'">'
                + '             <option value="RELATIVE" '
                +                   (item.commissions.reseller && item.commissions.reseller.commission_type === 'RELATIVE'
                                        ? 'selected="selected"'
                                        : '')+'>%</option>'
                + '             <option value="ABSOLUTE" '
                +                   (item.commissions.reseller && item.commissions.reseller.commission_type === 'ABSOLUTE'
                                        ? 'selected="selected"'
                                        : '')+'>€</option>'
                + '         </select>'
                + '     </div>'
                + '     <div class="form-group col-sm-3">'
                + '         <input  type="number"'
                + '                 class="form-control input-sm reseller-min"'
                + '                 data-id="'+item.id+'"'
                + '                 value="'+(item.commissions.reseller
                                        ? item.commissions.reseller.min_value.toString().replace(/,/g, '.')
                                        : '')+'"'
                + '                 placeholder="Min. Preis (in €)">'
                + '     </div>'
                + '</form>';
        }

        function getCommisionMark(type) {
            return (type === 'RELATIVE') ? '%' : '€';
        }

        function getTreeObjectColor(object) {
            switch (object) {
                case 'managers':
                    return '#ddd';
                case 'areas':
                    return '#e4e4e4';
                case 'ticket_types':
                case 'products':
                    return '#eee';
                case 'ticket_prices':
                case 'product_prices':
                    return '#f4f4f4';
                default:
                    return '#ddd';
            }
        }
    });
</script>
@endpush

@push('styles')
{!! Html::style('lib/bootstrap-treeview/dist/bootstrap-treeview.min.css') !!}
<style>
    .select2-result-admin__name {
        font-size: 18px;
    }

    .tree-view .list-group-item > span {
        vertical-align: top;
    }
    .tree-block {
        display: inline-block;
    }
    .tree-form {
        max-width: 100%;
        display: none;
    }
    .tree-form .form-group {
        margin-bottom: 0;
    }
    .tree-form form div:first-child b {
        vertical-align: middle;
        vertical-align: -webkit-baseline-middle;
    }
    .tree-form .col-lg-1, .tree-form .col-lg-10, .tree-form .col-lg-11, .tree-form .col-lg-12, .tree-form .col-lg-2,
    .tree-form .col-lg-3, .tree-form .col-lg-4, .tree-form .col-lg-5, .tree-form .col-lg-6, .tree-form .col-lg-7,
    .tree-form .col-lg-8, .tree-form .col-lg-9, .tree-form .col-md-1, .tree-form .col-md-10, .tree-form .col-md-11,
    .tree-form .col-md-12, .tree-form .col-md-2, .tree-form .col-md-3, .tree-form .col-md-4, .tree-form .col-md-5,
    .tree-form .col-md-6, .tree-form .col-md-7, .tree-form .col-md-8, .tree-form .col-md-9, .tree-form .col-sm-1,
    .tree-form .col-sm-10, .tree-form .col-sm-11, .tree-form .col-sm-12, .tree-form .col-sm-2, .tree-form .col-sm-3,
    .tree-form .col-sm-4, .tree-form .col-sm-5, .tree-form .col-sm-6, .tree-form .col-sm-7, .tree-form .col-sm-8,
    .tree-form .col-sm-9, .tree-form .col-xs-1, .tree-form .col-xs-10, .tree-form .col-xs-11, .tree-form .col-xs-12,
    .tree-form .col-xs-2, .tree-form .col-xs-3, .tree-form .col-xs-4, .tree-form .col-xs-5, .tree-form .col-xs-6,
    .tree-form .col-xs-7, .tree-form .col-xs-8, .tree-form .col-xs-9 {
        padding-right: 5px;
        padding-left: 5px;
    }
</style>
@endpush
