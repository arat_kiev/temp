@extends('admin.layout.default')

@php
$backUrl = $productPrice->id
    ? route('admin::super:product_prices.index', ['product' => $productPrice->product_id])
    : route('admin::super:product_prices.index');
@endphp

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ $backUrl }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Produkt Preis
                @if($productPrice->id)
                    bearbeiten <small># {{ $productPrice->id }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    {!! form_row($form->value) !!}
                    {!! form_row($form->vat) !!}
                    {!! form_row($form->visible_from) !!}
                    {!! form_row($form->valid_from) !!}
                    {!! form_row($form->valid_till) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
            @if($productPrice->sales()->count())
                <div class="row response-popup" style="padding-top: 10px;">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                            Preis könnte nicht bearbeiten werden
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#products').chosen({
            width: '100%'
        });
    });
</script>
@endpush
