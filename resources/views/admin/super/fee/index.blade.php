@extends('admin.layout.default')
@include('admin.layout.custom_dz_styles')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Gebühren
                <small>Gesamt: {{ $count }}</small>
                <div class="pull-right">
                    <a href="{{ route('admin::super:fees.create') }}" class="btn btn-success">
                        <span class="fa fa-plus fa-fw"></span> Gebühr anlegen
                    </a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="category">Kategorie</label>
                        <select class="form-control" name="category" id="filter-category">
                            @foreach($categoryList as $categoryId => $categoryName)
                                <option
                                        value="{{ $categoryId }}"
                                        @if($categoryId == $category)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $categoryName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="country">Land</label>
                        <select class="form-control" name="country" id="filter-country">
                            @foreach($countryList as $countryId => $countryName)
                                <option
                                        value="{{ $countryId }}"
                                        @if($countryId == $country)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $countryName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-fees">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Wert</th>
                        <th>Typ</th>
                        <th>Kategorie</th>
                        <th>Land</th>
                        <th>Aktionen</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            </span> Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie diese Gebühr endgültig löschen wollen?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Löschen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        function updateFilters() {
            var category = $('#filter-category').val(),
                country = $('#filter-country').val(),
                location = '{{ route('admin::super:fees.data') }}',
                params = [];

            if (category > 0) {
                params.push('category=' + category);
            }
            if (country > 0) {
                params.push('country=' + country);
            }
            $table.ajax.url(location + '?' + params.join('&')).load();
        }

        $('#filter-category').off().on('change', updateFilters);
        $('#filter-country').off().on('change', updateFilters);

        var $table = $('#datatable-fees').DataTable({
            bFilter: true,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::super:fees.data') }}',
            pagingType: "full_numbers",
            columns: [
                { data: 'id', searchable: false },
                { data: 'name' },
                { data: 'value', searchable: false },
                { data: 'type', searchable: false },
                { data: 'category', sortable: false, searchable: false },
                { data: 'country', sortable: false, searchable: false },
                { data: 'actions', sortable: false, searchable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#delete-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget),
                formAction = button.data('action'),
                formMethod = button.data('method'),
                $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();

                $.ajax({
                    type : formMethod,
                    url: formAction,
                    success : function() {
                        $('#delete-modal').modal('hide');
                        window.location.reload();
                    },
                    error: function (error) {
                        console.log(error);
                        if (error.status == 403) {
                            var msgBox = ''
                                + '<div class="row response-popup" style="padding-top: 10px;">'
                                + '    <div class="col-lg-12">'
                                + '        <div class="alert alert-danger alert-dismissable">'
                                + '            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">'
                                                + "&times;"
                                + '            </button>'
                                                + 'Gebühr könnte nicht gelöscht werden'
                                + '        </div>'
                                + '    </div>'
                                + '</div>';
                            $('.page-header').parents('.row').before(msgBox);
                            $('#delete-modal').modal('hide');
                        }
                    }
                });
            })
        });
    });
</script>
@endpush
