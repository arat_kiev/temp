@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Fangstatistik
                <div class="pull-right">
                    <div class="dropdown">
                        <button type="button" class="btn btn-default" data-toggle="dropdown">
                            <span class="fa fa-download fa-fw"></span> Export
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="{{ route('admin::super:hauls.export', ['format' => 'xls', 'ticket' => $ticket ?? null]) }}">
                                    <span class="fa fa-file-excel-o fa-fw"></span> XLS-Datei
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin::super:hauls.export', ['format' => 'csv', 'ticket' => $ticket ?? null]) }}">
                                    <span class="fa fa-file-text-o fa-fw"></span> CSV-Datei
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-hauls">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Fischart</th>
                    <th>Größe/Gewicht</th>
                    <th>Fischer</th>
                    <th>Angelkarten-Nr.</th>
                    <th>Angelmethode</th>
                    <th>Gewässer</th>
                    <th>Fangdatum/Zeit</th>
                    <th>Kommentar</th>
                    <th>Aktionen</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            </span> Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie diesen Fang endgültig löschen wollen?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Löschen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#datatable-hauls').DataTable({
            bFilter: false,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::super:hauls.data') }}' + window.location.search,
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'fish_name', sortable: false },
                { data: 'fish_size', sortable: false },
                { data: 'user_name', sortable: false },
                { data: 'ticket_number' },
                { data: 'fishing_methods', sortable: false},
                { data: 'area_name', sortable: false },
                { data: 'catch_date', sortable: false },
                { data: 'user_comment', sortable: false },
                { data: 'actions', sortable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#delete-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget),
                formAction = button.data('action'),
                formMethod = button.data('method'),
                $form = $(this).find('.form');

            $form.on('submit', function (e) {
                e.preventDefault();

                $.ajax({
                    type : formMethod,
                    url: formAction,
                    success : function() {
                        $('#delete-modal').modal('hide');
                        window.location.reload();
                    },
                    error: function(error) {
                        console.error(error);
                        alert(error.responseText || 'Sorry, some error occured');
                    }
                });
            })
        });
    });
</script>
@endpush