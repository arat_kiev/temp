@extends('admin.layout.master')

@section('menu')
    @include('admin.menu.top.organization')
    @include('admin.menu.side')
@endsection