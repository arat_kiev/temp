<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="hejAdmin, hejfish">
    <meta name="author" content="FiOApps GmbH">
    <title>@yield('page-title')</title>
    <link rel="icon" type="image/png" sizes="48x48" href="{{ asset('favicon.'.$av.'.png') }}">
{!! Html::style('lib/metisMenu/dist/metisMenu.min.'.$av.'.css') !!}
{!! Html::style('lib/bootstrap/dist/css/bootstrap.min.'.$av.'.css') !!}
{!! Html::style('lib/startbootstrap-sb-admin-2/dist/css/sb-admin-2.'.$av.'.css') !!}
{!! Html::style('lib/font-awesome/css/font-awesome.min.'.$av.'.css') !!}
{!! Html::style('css/admin.'.$av.'.css') !!}

@stack('styles')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    {!! Html::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
    {!! Html::script('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
    <![endif]-->
</head>
<body>
@include('analytics.google')
@yield('body')
{!! Html::script('lib/jquery/dist/jquery.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/moment/min/moment.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/moment/locale/de-at.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/bootstrap/dist/js/bootstrap.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/metisMenu/dist/metisMenu.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/startbootstrap-sb-admin-2/dist/js/sb-admin-2.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('js/promise-polyfill.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/sweetalert2/dist/sweetalert2.all.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
@stack('scripts')
</body>
</html>