@extends('admin.layout.base')

@section('page-title')
    hejfishAdmin
@endsection

@push('styles')
{!! Html::style('lib/startbootstrap-sb-admin-2/dist/css/timeline.'.$av.'.css') !!}
{!! Html::style('lib/morrisjs/morris.'.$av.'.css') !!}
{!! Html::style('lib/datatables.net-bs/css/dataTables.bootstrap.min.'.$av.'.css') !!}
{!! Html::style('lib/summernote/dist/summernote.'.$av.'.css') !!}
{!! Html::style('lib/chosen/chosen.'.$av.'.css') !!}
{!! Html::style('lib/dropzone/dist/min/dropzone.min.'.$av.'.css') !!}
{!! Html::style('lib/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.'.$av.'.css') !!}
{!! Html::style('lib/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.'.$av.'.css') !!}
{!! Html::style('lib/select2/dist/css/select2.min.'.$av.'.css') !!}
{!! Html::style('lib/ekko-lightbox/dist/ekko-lightbox.min.'.$av.'.css') !!}
{!! Html::style('lib/fullcalendar/dist/fullcalendar.'.$av.'.css') !!}
@endpush

@push('scripts')
{!! Html::script('lib/raphael/raphael-min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/morrisjs/morris.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/datatables.net/js/jquery.dataTables.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/datatables.net-bs/js/dataTables.bootstrap.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/summernote/dist/summernote.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/summernote/dist/lang/summernote-de-DE.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/chosen/chosen.jquery.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/dropzone/dist/min/dropzone.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/bootstrap-datepicker/dist/locales/bootstrap-datepicker.de.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/select2/dist/js/select2.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/select2/dist/js/i18n/de.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/ekko-lightbox/dist/ekko-lightbox.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/fullcalendar/dist/fullcalendar.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('lib/fullcalendar/dist/locale/de.'.$av.'.js', ['nounce' => csp_nounce()]) !!}
{!! Html::script('js/loadingoverlay.min.'.$av.'.js', ['nounce' => csp_nounce()]) !!}

<script nonce="{{ csp_nounce() }}">
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    $.fn.datepicker.defaults.zIndexOffset = 1001;
    $.fn.datepicker.defaults.language = 'de';
</script>
@endpush

@section('body')
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation"
             style="margin-bottom: 0; {{ config('app.env') !== 'production' ? 'background-color: #f4645f;' : '' }}">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="{{ route('admin::dashboard') }}" class="navbar-brand">
                    {!! Html::image('img/admin/logo-admin.'.$av.'.png', 'logo') !!}
                </a>
                @if($currentTemplateName === 'admin.rental.index')
                    <a href="{{ route('admin::dashboard') }}" class="navbar-text">Zurück zur Administration</a>
                @endif
                @if(config('app.env') !== 'production')
                    <p class="navbar-text" style="color: #fff; font-size: 20px; line-height: 20px; font-weight: bold;">Development</p>
                @endif
            </div>
            @yield('menu')
        </nav>
        @section('full-page')
            <div id="page-wrapper">
                @include('admin.flash.message')
                @yield('page-content')
                <br/><br/>
            </div>
        @show
    </div>
@endsection