@extends('admin.layout.master')

@section('menu')
    @include('admin.menu.top.default')
    @include('admin.menu.side')
@endsection