@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default back-btn" href="{{ route('admin::invoices.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Abrechnung erstellen
            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Allgemein
            </div>
            <div class="panel-body">
                {!! form_row($form->date_from) !!}
                {!! form_row($form->date_till) !!}
                {!! form_row($form->reseller) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $('document').ready(function() {
            $('#date_from').datepicker();
            $('#date_till').datepicker();
            $('#reseller').find('option').first().attr('disabled', true);
        });
    </script>
@endpush
