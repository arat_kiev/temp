@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $('document').ready(function() {

            // Initial buttons build
            var deleteButton = '{!! isset($delete) !!}' ? '<button type="button" class="btn btn-danger" id="deleteAction"><span class="fa fa fa-trash-o fa-fw"></span>Löschen</button>' : '';
            var publishButton = '{!! (isset($publish) && isset($drafted)) or '' !!}'
            ? '{!! $drafted == 1 !!}'
                ? '<button type="button" class="btn btn-success publish" id="publishAction" style="margin-right: 5px;"><span class="fa fa-exchange fa-fw"></span><span id="buttonText">Veröffentlichen</span></button>'
                : '<button type="button" class="btn btn-warning unpublish" id="publishAction" style="margin-right: 5px;"><span class="fa fa-exchange fa-fw"></span> <span id="buttonText">Nicht Veröffentlichen</span></button>'
            : '';

            $('<div class="pull-right" id="actions">'+publishButton+deleteButton+'</div>').appendTo(".page-header");

            // Publish action
            $('#publishAction').click(function(){
                var button = $(this);
                var drafted = button.hasClass('publish') ? 0 : 1;
                $.post({
                    url: '{!! $publish or '' !!}',
                    data: {"drafted":drafted},
                    success:function(data){
                        drafted
                            ? button.toggleClass('btn-success btn-warning').toggleClass('unpublish publish').find('#buttonText').text('Veröffentlichen')
                            : button.toggleClass('btn-warning btn-success').toggleClass('publish unpublish').find('#buttonText').text('Nicht Veröffentlichen');
                    },
                    error: function(error){
                        showMsg('alert-danger', 'Fehler!', error.status == 400 ? error.responseText : 'Versuchen Sie, die Seite neu laden');
                    }
                });
            });

            // Delete action
            $('#deleteAction').click(function(){
                $.ajax({
                    type: 'DELETE',
                    url: '{!! $delete or '' !!}',
                    success:function(data){
                        showMsg('alert-success', 'Erfolg!', (data || '') + ' Sie werden zur Hauptseite weitergeleitet', '{!! $list or '' !!}');
                    },
                    error: function(error){
                        showMsg('alert-danger', 'Fehler!', error.status == 400 ? error.responseText : 'Versuchen Sie, die Seite neu laden');
                    }
                });
            });

            // Notification Message
            var showMsg = function(className, title, body, redirect) {
                var msg = $('#notif-msg');
                msg.removeClass('alert-success alert-danger').addClass(className).css("left", "50%");
                msg.find('#notif-msg-title').text(title);
                msg.find('#notif-msg-body').text(body);

                // Hide massage container
                setTimeout(function() {
                    msg.css("left", "150%");
                    if(redirect) {
                        window.location.replace(redirect);
                    }
                }, 3000);
            };

        });

    </script>
@endpush

{{-- Message Container--}}
<div id="notif-msg" class="alert alert-success" style="left: 150%; overflow: hidden; position: fixed; width: 50%; opacity: 0.9; z-index: 1050; transition: left 0.6s ease-out 0s; bottom: 10px;">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    <h1 id="notif-msg-title"></h1>
    <p id="notif-msg-body"></p>
</div>
