@extends('admin.layout.default')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center">
                Wichtige Informationen
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-lg-push-3 col-md-8 col-md-push-2 col-sm-10 col-sm-push-1">
            <p>{{ $document->content }}</p>
            @if($document->file)
                <a href="{{ route('fileload', ['file' => $document->file->filePathWithName]) }}" class="btn btn-info btn-block" target="_blank">
                    <span class="fa fa-fw fa-file-pdf-o"></span>
                    Dokument anzeigen
                </a>
            @endif
            <hr/>
            <form action="{{ route('admin::docs:manager.legal.update', ['docId' => $document]) }}" method="post">
                <button type="submit" class="btn btn-primary btn-lg btn-block">
                    <span class="fa fa-fw fa-check-square-o"></span>
                    Bestätigen
                </button>
            </form>
        </div>
    </div>
@endsection