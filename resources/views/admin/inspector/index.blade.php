@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Kontrolleur
                <div class="pull-right">
                    <button class="btn btn-success" data-target="#inspector-modal" data-toggle="modal">
                        <span class="fa fa-plus fa-fw"></span> Kontrolleur hinzufügen
                    </button>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered table-hover dataTable no-footer nowrap" id="datatable-users">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Vorname</th>
                    <th>Nachname</th>
                    <th>Zuletzt aktiv</th>
                    <th>Gewässer</th>
                    <th>Aktionen</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- inspector modal --}}
    <div class="modal fade in" id="inspector-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            <span class="fa fa-group fa-fw"></span> Kontrolleur hinzufügen
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text"
                                   class="form-control"
                                   id="inspectors"
                                   name="inspectors"
                                   placeholder="Benutzer E-Mail oder Fisher-ID eingeben"
                                   autocomplete="off"
                            >
                        </div>
                        <div class="selected-inspector"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- area modal --}}
    <div class="modal fade in" id="area-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            <span class="fa fa-group fa-fw"></span> Gewässer
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="public-areas">Öffentlicher Kontrolleur (Wird auf hejfish angezeigt)</label>
                            <select class="form-control" id="public-areas" name="public-areas[]" multiple="multiple"></select>
                        </div>
                        <div class="form-group">
                            <label for="private-areas">Nicht-öffentlicher Kontrolleur</label>
                            <select class="form-control" id="private-areas" name="private-areas[]" multiple="multiple"></select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Schließen"><span>&times;</span></button>
                        <h4 class="modal-title">
                            </span> Entfernen
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Kontrolleur entfernen?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Entfernen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        var table = $('#datatable-users').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::inspector.data') }}',
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'name' },
                { data: 'email' },
                { data: 'first_name' },
                { data: 'last_name' },
                { data: 'last_activity_at' },
                { data: 'areas', searchable: false, sortable: false },
                { data: 'actions', searchable: false, sortable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#inspector-modal').on('show.bs.modal', function() {
            var $modal = $(this);
            var $input = $modal.find('.form #inspectors');

            $input.on('keyup', debounce(fetchUsersToBeInspectors, 500));

            $modal.find('.form').on('submit', function (e) {
                e.preventDefault();
            });
        });

        $('#area-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget),
                formAction = button.data('action'),
                publicAction = button.data('public-action'),
                privateAction = button.data('private-action'),
                modal = $(this);
            modal.find('.form').attr('action', formAction);

            var publicSelect = modal.find('#public-areas'),
                privateSelect = modal.find('#private-areas');

            publicSelect.children().remove();
            privateSelect.children().remove();

            fetchInspectedAreas(publicAction, publicSelect);
            fetchInspectedAreas(privateAction, privateSelect);
        });

        $('#delete-modal').on('show.bs.modal', function(e) {
            var formAction = $(e.relatedTarget).data('action'),
                formMethod = 'DELETE',
                $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();
                $(document).find('.response-popup').remove();

                $.ajax({
                    type : formMethod,
                    url: formAction,
                    success : function(data) {
                        var msgBox = getPopUpBox(data);
                        $('.page-header').parents('.row').before(msgBox);
                        table.ajax.reload();
                        $('#delete-modal').modal('hide');
                    },
                    error: function (data) {
                        console.log(data);
                        var msgBox = getPopUpBox({notification: 'danger', message: 'Something went wrong'});
                        $('.page-header').parents('.row').before(msgBox);
                        table.ajax.reload();
                        $('#delete-modal').modal('hide');
                    }
                });
            });
        });

        function fetchInspectedAreas(formAction, areaSelect) {
            var areaData = [];

            $.getJSON(formAction).done(function (data) {
                $.each(data.data, function (index, area) {
                    areaSelect.append($('<option/>', {
                        value: area.id,
                        text: area.name,
                        selected: true
                    }));
                });
            }).done(function () {
                areaSelect.select2({
                    width: '100%',
                    data: areaData,
                    ajax: {
                        url: '{{ route('admin::areas.all') }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term,
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.data,
                                pagination: {
                                    more: params.page < data.meta.pagination.total_pages
                                }
                            };
                        },
                        cache: true
                    },
                    templateResult: function (area) {
                        if (area.loading) return area.name;

                        return "<div class='select2-result-admin'>" +
                                "    <div class='select2-result-admin__name'>" + area.name + " (" + area.id + ")</div>" +
                                "</div>";
                    },
                    templateSelection: function (area) {
                        return area.text || area.name;
                    },
                    escapeMarkup: function (markup) { return markup; }
                });
            });
        }

        function fetchUsersToBeInspectors() {
            var $modal = $('#inspector-modal');
            var inputValue = $modal.find('#inspectors').val();

            if (inputValue.length <= 3) {
                var alert = '<div class="alert alert-info">Mehr als drei Zeichen eingeben</div>';
                $('.selected-inspector').html(alert);
                return;
            }

            var params = {
                user: inputValue,
                isNotManagerInspector: true
            };
            $.getJSON('{{ route("admin::users.all") }}', params, function (res) {
                var rows = drawInspectorAssignButtons(res);

                showInspectorAssignButtons(rows);

                $modal.find('.btn-add-inspector').off('click').on('click', addInspectorHandler);
            });
        }

        function drawInspectorAssignButtons(items) {
            return items.reduce(function (sum, current) {
                var firstName = current.first_name || null,
                    lastName = current.last_name || null,
                    plus = '<span class="col-xs-3">'
                        + '<span class="fa fa-plus fa-fw"></span> <b>Zuweisen</b>'
                        + '</span>';

                return sum
                    + '<div>'
                        + '<button class="btn btn-primary btn-add-inspector" data-id="' + current.id + '">'
                            + plus
                            + '#' + current.id + ' '
                            + ((firstName || lastName) ? firstName + ' ' + lastName : current.name)
                            + ' (' + current.email + ')'
                        + '</button>'
                    + '</div>';
            }, '');
        }

        function showInspectorAssignButtons(rows) {
            if (rows.length) {
                $('.selected-inspector').html(rows);
            } else {
                var alert = '<div class="alert alert-danger">Kein Benutzer wurde gefunden oder bereits als Kontrolleur zugewiesen</div>';
                $('.selected-inspector').html(alert);
            }
        }

        function addInspectorHandler(e) {
            e.preventDefault();
            $(document).find('.response-popup').remove();
            $('#inspector-modal').find('.btn-add-inspector').addClass('disabled').prop('disabled', true);

            var inspectorId = $(e.target).data('id');
            var route = '{{ route('admin::inspector.manager.attach', ['userId' => '%USERID%']) }}';
            route = route.replace(/%USERID%/, inspectorId);

            $.postJSON(route, [], function(data) {
                var msgBox = getPopUpBox(data);
                $('.page-header').parents('.row').before(msgBox);
                table.ajax.reload();
                $(document).find('#inspector-modal').modal('hide');
            }, function(error) {
                console.log(error);
                $('.selected-inspector').html('<div class="alert alert-danger">Es ist ein Fehler aufgetreten, bitte versuchen Sie es erneut</div>');
            });
        }

        function getPopUpBox(data) {
            return ''
                + '<div class="row response-popup" style="padding-top: 10px;">'
                + '    <div class="col-lg-12">'
                + '        <div class="alert alert-' + data.notification + ' alert-dismissable">'
                + '            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">'
                                + "&times;"
                + '            </button>'
                                + data.message
                + '        </div>'
                + '    </div>'
                + '</div>';
        }

        function debounce(func, wait, immediate) {
            var timeout;
            return function() {
                var context = this,
                    args = arguments;
                var later = function() {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        }

        $.postJSON = function(url, data, callback, error) {
            return jQuery.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType : 'json',
                success : callback,
                error : error
            });
        };
    });
</script>
@endpush

@push('styles')
<style>
    .btn-add-inspector {
        width: 80%;
        margin: 2px 10%;
        text-align: left;
    }
</style>
@endpush