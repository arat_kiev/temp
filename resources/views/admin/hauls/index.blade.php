@extends('admin.layout.manager')

@inject('ticketManager', 'App\Managers\TicketManager')

@php
    $dbTicket = $ticket ? \App\Models\Ticket\Ticket::find($ticket) : null;
@endphp

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Fangstatistik
                <small>Gesamt: {{ $count }}</small>
                <div class="pull-right">
                    <div class="dropdown">
                        <button type="button" class="btn btn-default" data-toggle="dropdown">
                            <span class="fa fa-download fa-fw"></span> Export
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="{{ route('admin::hauls.export', [
                                        'format'    => 'xls',
                                        'ticket'    => $ticket ?: null,
                                        'taken'     => $taken ?: null,
                                        'area'      => $area ?: null,
                                        'fish'      => $fish ?: null,
                                        'count'     => $count ?: null,
                                        'fishingMethod' => $fishingMethod ?: null,
                                        'from'      => $from ?: null,
                                        'till'      => $till ?: null,
                                    ]) }}">
                                    <span class="fa fa-file-excel-o fa-fw"></span> XLS-Datei
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin::hauls.export', [
                                        'format'    => 'csv',
                                        'ticket'    => $ticket ?: null,
                                        'taken'     => $taken ?: null,
                                        'area'      => $area ?: null,
                                        'fish'      => $fish ?: null,
                                        'count'     => $count ?: null,
                                        'fishingMethod' => $fishingMethod ?: null,
                                        'from'      => $from ?: null,
                                        'till'      => $till ?: null,
                                    ]) }}">
                                    <span class="fa fa-file-text-o fa-fw"></span> CSV-Datei
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 pull-left">
                    <div class="form-group">
                        <label for="fish">Fische</label>
                        <select class="form-control" name="fish" id="filter-fish">
                            @foreach($fishList as $fishId => $fishName)
                                <option
                                        value="{{ $fishId }}"
                                        @if($fishId == $fish)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $fishName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="invoiced">Entnommen</label>
                        <select class="form-control" name="taken" id="filter-taken">
                            <option value="-1" @if($taken == -1) selected="selected" @endif>Alle</option>
                            <option value="0" @if($taken == 0) selected="selected" @endif>Nicht entnommen</option>
                            <option value="1" @if($taken == 1) selected="selected" @endif>Entnommen</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="form-group">
                        <label for="fish">Angelmethoden</label>
                        <select class="form-control" name="fish" id="filter-fishing-method">
                            @foreach($fishingMethodsList as $fishingMethodId => $fishingMethodName)
                                <option value="{{ $fishingMethodId }}"
                                        @if($fishingMethodId == $fishingMethod)
                                            selected="selected"
                                        @endif
                                >
                                    {{ $fishingMethodName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 pull-right">
                    <div class="form-group">
                        <label for="area">Gewässer</label>
                        <select class="form-control" name="area" id="filter-area">
                            @foreach($areaList as $areaId => $areaName)
                                <option
                                        value="{{ $areaId }}"
                                        @if($areaId == $area)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $areaName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="ticket">Angelkarten-Nr.</label>
                        <div class="input-group">
                            <input class="form-control"
                                   name="ticket"
                                   id="ticket"
                                   value="{{ $dbTicket ? $ticketManager->getIdentifier($dbTicket) : '' }}">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="button" id="filter-ticket">Filter</button>
                            </div>
                        </div>
                    </div>
                    <div class="input-group input-daterange">
                        <div class="input-group-addon input-group-label">Fangdatum</div>
                        <input type="text"
                               class="form-control"
                               id="date-range-from"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $from)->format('d.m.Y') }}">
                        <span class="input-group-addon">bis</span>
                        <input type="text"
                               class="form-control"
                               id="date-range-till"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $till)->format('d.m.Y') }}">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="button" id="filter-date-range-btn">Filter</button>
                        </div>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap"
                   id="datatable-hauls">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Fischart</th>
                    <th>Anzahl</th>
                    <th>Größe/Gewicht</th>
                    <th>Entnommen</th>
                    <th>Angelmethode</th>
                    <th>Fischer</th>
                    <th>Angelkarten-Nr.</th>
                    <th>Gewässer</th>
                    <th>Fangdatum/Zeit</th>
                    <th>Kommentar</th>
                    <th>Aktionen</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false"
         style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Sind Sie sicher, dass Sie diesen Fang endgültig löschen wollen?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete"/>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Löschen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <style>
        .input-daterange {
            margin-bottom: 10px;
        }

        @media (min-width: 992px) {
            .input-daterange {
                margin-bottom: -30px;
            }
        }
    </style>
@endpush

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $('document').ready(function () {
            $('.input-daterange').datepicker();

            function updateFilters() {
                var location = '{{ route('admin::hauls.index') }}',
                    ticketNumber = $('#ticket').val(),
                    ticketIdMatch = ticketNumber.match({!! \App\Models\Ticket\Ticket::IDENT_REGEX !!}),
                    area = $('#filter-area').val(),
                    fish = $('#filter-fish').val(),
                    taken = $('#filter-taken').val(),
                    fishingMethod = $('#filter-fishing-method').val(),
                    from = $('#date-range-from').datepicker('getDate'),
                    till = $('#date-range-till').datepicker('getDate'),
                    fromString = moment(from).format('YYYY-MM-DD'),
                    tillString = moment(till).format('YYYY-MM-DD'),
                    params = [
                        'from=' + fromString,
                        'till=' + tillString
                    ];

                if (ticketIdMatch && ticketIdMatch[2]) {
                    params.push('ticket=' + parseInt(ticketIdMatch[2]));
                }

                if (area > 0) {
                    params.push('area=' + area);
                }
                if (fish >= 0) {
                    params.push('fish=' + fish);
                }
                if (fishingMethod >= 0) {
                    params.push('fishingMethod=' + fishingMethod);
                }

                params.push('taken=' + taken);

                window.location = location + '?' + params.join('&');
            }

            $('#filter-ticket').on('click', updateFilters);
            $('#date-range-from').on('change', updateFilters);
            $('#date-range-till').on('change', updateFilters);
            $('#filter-area').on('change', updateFilters);
            $('#filter-fish').on('change', updateFilters);
            $('#filter-taken').on('change', updateFilters);
            $('#filter-fishing-method').on('change', updateFilters);
            $('#filter-date-range-btn').on('click', updateFilters);

            $('#datatable-hauls').DataTable({
                bFilter: false,
                stateSave: true,
                processing: true,
                serverSide: true,
                ajax: '{{ route('admin::hauls.data') }}' + window.location.search,
                pagingType: "full_numbers",
                columns: [
                    {data: 'id'},
                    {data: 'fish_name', sortable: false},
                    {data: 'count', sortable: false},
                    {data: 'fish_size', sortable: false},
                    {data: 'taken', sortable: false},
                    {data: 'fishing_method', sortable: false},
                    {data: 'user_name', sortable: false},
                    {data: 'ticket_number'},
                    {data: 'area_name', sortable: false},
                    {data: 'catch_date'},
                    {data: 'user_comment', sortable: false},
                    {data: 'actions', sortable: false}
                ],
                order: [[6, "desc"]],
                language: {
                    decimal: ",",
                    thousands: ".",
                    lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                    zeroRecords: "Keine Einträge vorhanden",
                    info: "Seite _PAGE_ von _PAGES_",
                    infoEmpty: "Keine Einträge vorhanden",
                    infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                    search: "Suche",
                    paginate: {
                        first: "&laquo;",
                        previous: "&lsaquo;",
                        next: "&rsaquo;",
                        last: "&raquo;"
                    }
                }
            });

            $('#delete-modal').on('show.bs.modal', function (e) {
                var button = $(e.relatedTarget),
                    formAction = button.data('action'),
                    formMethod = button.data('method'),
                    $form = $(this).find('.form');

                $form.on('submit', function (e) {
                    e.preventDefault();

                    $.ajax({
                        type: formMethod,
                        url: formAction,
                        success: function () {
                            $('#delete-modal').modal('hide');
                            window.location.reload();
                        },
                        error: function (error) {
                            console.error(error);
                            alert('Sorry, some error occured');
                        }
                    });
                })
            });
        });
    </script>
@endpush