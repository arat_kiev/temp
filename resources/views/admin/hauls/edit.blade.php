@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::sales.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Fang
                @if($haul->id)
                    bearbeiten <small>#{{ $haul->id }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form, ['id' => 'haul-create-form']) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    <button class="btn btn btn-warning pull-right empty-catch-btn">Leermeldung</button>
                    <br style="clear: both">

                    <div class="row">
                        <div class="col-md-6">
                            {!! form_row($form->catch_date) !!}
                        </div>
                        <div class="col-md-6">
                            {!! form_row($form->catch_time) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            {!! form_row($form->fish) !!}
                        </div>
                        <div class="col-md-6">
                            {!! form_row($form->count) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            {!! form_row($form->size_value) !!}
                        </div>
                        <div class="col-md-6">
                            {!! form_row($form->size_type) !!}
                        </div>
                    </div>
                    {!! form_row($form->taken) !!}
                </div>
            </div>
            <div class="panel panel-default">
                {{-- TODO: make it collapsible --}}
                <div class="panel-heading">
                    Ohne hejfish-Angelkarte
                </div>
                <div class="panel-body">
                    {!! form_row($form->ticket) !!}
                    @unless($haul->id)
                        {!! form_row($form->user) !!}
                    @endunless
                    @unless($haul->ticket)
                        {!! form_row($form->ticket_number) !!}
                    @endunless
                    {!! form_row($form->area) !!}
                    @unless($haul->area)
                        {!! form_row($form->area_name) !!}
                    @endunless
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Bild
                </div>
                <div class="panel-body">
                    @if($haul->picture)
                        <img src="{{ route('imagecache', ['template' => 'lLogo', 'filename' => $haul->picture->file]) }}"
                             class="thumbnail img-responsive" />
                    @endif
                    {!! form_row($form->picture) !!}
                    {!! form_row($form->public) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Andere Informationen
                </div>
                <div class="panel-body">
                    {!! form_row($form->weather) !!}
                    {!! form_row($form->temperature) !!}
                    {!! form_row($form->technique) !!}
                    {!! form_row($form->user_comment) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}

    {{-- empty catch confirmation modal --}}
    <div class="modal fade in" id="empty-catch-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            </span> Leermeldung Bestätigung
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Leermeldung eintragen?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-warning accept">
                            <span class="fa fa-shopping-basket fa-fw"></span> Ja
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Nein</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#areas,#fishes,#techniques,#tickets,#users').chosen({width: '100%'});

        $('.empty-catch-btn').on('click', function (e) {
            e.preventDefault();

            $('#user_comment').text('Leermeldung');
            $('#empty-catch-modal').modal('show');
        });

        $('#empty-catch-modal').on('show.bs.modal', function(e) {
            var $form = $(this).find('.form');

            $form.on('submit', function (e) {
                e.preventDefault();
                $('#haul-create-form').submit();
            });
        });

        $('#tickets').on('change', function (e) {
            e.preventDefault();

            var ticketId = $(e.target).val();
            var url = '{{ route('admin::tickets.details', ['ticketId' => '__TICKET_ID__']) }}'
                    .replace(/__TICKET_ID__/g, ticketId);

            $.getJSON(url, [], function (res) {
                if (res && res.valid_from) {
                    var date = moment(res.valid_from).format('DD.MM.YYYY');
                    var time = moment(res.valid_from).format('HH:mm');
                    $('#catch_date').val(date);
                    $('#catch_time').val(time);
                }
            });
        });
    });
</script>
@endpush
