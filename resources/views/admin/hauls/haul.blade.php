<div class="row haul-card-row" id="haul-card-row-{{ $haul->id }}">
    <div class="jumbotron">
        <div class="container-fluid">
            <div class="haul-card">
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-4">
                            <strong>{{ $haul->count ? $haul->count : 1 }} x {{ $haul->fish ? $haul->fish->name : '' }}</strong>
                        </div>
                        <div class="col-md-4">{{ $haul->size_value }} {{ $haul->size_type }}</div>
                        <div class="col-md-4">{{ $haul->technique ? $haul->technique->name : '' }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">{{ \Carbon\Carbon::parse($haul->catch_date)->format('d.m.Y') }}</div>
                        <div class="col-md-4">{{ \Carbon\Carbon::parse($haul->catch_time)->format('H:i') }}</div>
                        <div class="col-md-4">{{ $haul->area ? $haul->area->name : '' }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">{{ $haul->taken ? 'Entnommen' : 'Nicht entnommen'}}</div>
                        <div class="col-md-8">{{ $haul->user_comment }}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 form-group-md">
                <div class="form-row">
                    <a class="btn btn-info form-control haul-card-edit-btn" type="button"
                       href="{{ route('admin::hauls.edit', ['haulId' => $haul->id]) }}">Bearbeiten</a>
                </div>
                <div class="form-row">
                    <button class="btn btn-danger form-control haul-delete-btn" type="button"
                            data-haul-id="{{ $haul->id }}">Löschen
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.haul-delete-btn').on('click', function (e) {
        confirm('Wirklich löschen?');

        $.ajax({
            url: '{{ route('admin::hauls.delete', ['ticketId' => '__TICKET_ID__']) }}'
                .replace('__TICKET_ID__', $(this).data('haul-id')),
            method: 'delete',
            success: function () {
            },
            error: function (error) {
            }
        });

        $('div#haul-card-row-' + $(this).data('haul-id')).remove();
    });
</script>
