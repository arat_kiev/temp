@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::areas.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Angelkarten <small>{{ $area->name }}</small>
                <div class="pull-right">
                    <a href="{{ route('admin::areas.tickettypes.create', ['areaId' => $area->id]) }}"
                       class="btn btn-success">
                        <span class="fa fa-plus fa-fw"></span> Produkt anlegen
                    </a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap"
                   id="datatable-tickettypes">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Zuletzt bearbeitet</th>
                    <th>Preise</th>
                    <th>Benutzer benachrichtigen</th>
                    <th>Kombikarte</th>
                    @if(Auth::user()->hasRole('superadmin'))
                        <th class="sorting_disabled">Aktionen</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($area->ticketTypes as $ticketType)
                    <tr>
                        <td>
                            {!! Html::linkRoute('admin::areas.tickettypes.edit', $ticketType->name, [
                                    'areaId'        => $area->id,
                                    'ticketTypeId'  => $ticketType->id
                                ]) !!}
                        </td>
                        <td>{{ $ticketType->updated_at->format('d.m.Y - H:i') }}</td>
                        <td>
                            <a href="{{ route('admin::ticketprices.index', [
                                        'areaId' => $area->id,
                                        'ticketTypeId' => $ticketType->id
                                    ]) }}"
                               class="btn btn-xs btn-primary">
                                <span class="fa fa-picture-o fa-fw"></span>
                                Preise
                                <span class="badge">
                                    {{ $ticketType->prices()->active()->count() }} /
                                    {{ $ticketType->prices()->count() }}
                                </span>
                            </a>
                        </td>
                        <td>
                            <button type="button"
                                    data-users=" {{ $ticketType->productBasedNotifications()->pluck('id')->toJson() }} "
                                    data-ticket-type-id="{{ $ticketType->id }}"
                                    class="btn btn-xs btn-primary"
                                    data-target="#notify-users-modal"
                                    data-toggle="modal">
                                <span class="fa fa-envelope-o"></span>
                                Users
                                <span class="badge"> {{ $ticketType->productBasedNotifications()->count() }} </span>
                            </button>
                        </td>
                        <td>
                            <button type="button"
                                    data-additional-areas=" {{ $ticketType->additionalAreas()->pluck('id')->toJson() }} "
                                    data-ticket-type-id="{{ $ticketType->id }}"
                                    class="btn btn-xs btn-primary"
                                    data-target="#additional-areas-modal"
                                    data-toggle="modal">
                                <span class="fa fa-map-marker fa-fw"></span>
                                Zusätzliche Gewässer
                                <span class="badge"> {{ $ticketType->additionalAreas()->count() }} </span>
                            </button>
                        </td>
                        @if(Auth::user()->hasRole('superadmin'))
                            <th>
                                <button type="button"
                                        class="btn btn-xs btn-danger @if($ticketType->tickets()->count()) disabled @endif"
                                        data-target="#delete-modal"
                                        data-toggle="modal"
                                        data-action="{{ route('admin::super:tickettypes.delete', [
                                            'ticketTypeId' => $ticketType->id
                                        ]) }}"
                                        @if($ticketType->tickets()->count()) disabled @endif>
                                    Löschen<span class="fa fa-trash-o fa-fw"></span>
                                </button>
                            </th>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{-- `notify users` modal --}}
    <div class="modal fade in" id="notify-users-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            <span class="fa fa-group fa-fw"></span> Users
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select class="form-control" id="users" name="users[]" multiple="multiple"></select>
                            <input type="hidden" name="id" value="1">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="delete-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Schließen"><span>&times;</span></button>
                        <h4 class="modal-title">
                            Bestätigung des Löschvorgangs
                        </h4>
                    </div>
                    <div class="modal-body">
                        <p>Wirklich löschen?</p>
                    </div>
                    <input type="hidden" name="_method" value="delete" />
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-remove fa-fw"></span> Löschen
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- `additional areas` modal --}}
    <div class="modal fade in" id="additional-areas-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                        <h4 class="modal-title">
                            <span class="fa fa-group fa-fw"></span> Zusätzliche Gewässer
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                <span class="sr-only"></span>
                            </div>
                        </div>

                        <div class="form-group multiselect-wrapper hidden">
                            <select class="form-control additional-areas-select" id="additional-areas" name="additional-areas[]" multiple="multiple">
                                <option value="{{ $area->id }}" selected disabled>{{ $area->name }}</option>
                            </select>
                            <input type="hidden" name="id" value="1">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('#datatable-tickettypes').DataTable({
            stateSave: true,
            pagingType: "full_numbers",
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });
        $('#users').select2({ width: '100%', data: {!! $users !!} });
        $('.additional-areas-select').select2({ width: '100%', data: {!! $additionalAreas !!} });

        $('#notify-users-modal').on('shown.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var selectedUsers = button.data('users');
            var ticketTypeId = button.data('ticket-type-id');

            $(this).find('input[name=id]').val(ticketTypeId);
            // change form action
            $(this).find('.form').attr('action', location.protocol + '//' + location.host + location.pathname+'/'+ticketTypeId+'/partial');
            // convert string into array with strings
            selectedUsers = selectedUsers.substring(2, selectedUsers.length - 2).split(',');
            //set selected
            $(this).find('#users').val(selectedUsers).change();
        });

        $('#notify-users-modal').on('hide.bs.modal', function (e) {
            // clear selected
            $('#users').val('').change();
            $(this).find('input[name=id]').val('');
        });

        //todo fix areas init fro select2 input
        $('#additional-areas-modal').on('shown.bs.modal', function (e) {
            var $modal = $(e.target);
            var $button = $(e.relatedTarget);
            var selectedAdditionalAreas = $button.data('additional-areas');
            var ticketTypeId = $button.data('ticket-type-id');

            $modal.find('.progress').removeClass('hidden');

            $.ajax({
                url: location.protocol + '//' + location.host + location.pathname+'/'+ticketTypeId+'/getAdditionalAreas',
                method: 'post',
                dataType: 'json',
                success: function (result) {
                    $modal.find('.progress').toggleClass('hidden');
                    $modal.find('.multiselect-wrapper').toggleClass('hidden');
                    $('#additional-areas').select2({ width: '100%', data: result});
                },
                error: function (error) {
                    console.log(error.message);
                }
            });

            $(this).find('input[name=id]').val(ticketTypeId);
            // change form action
            $(this).find('.form').attr('action', location.protocol + '//' + location.host + location.pathname+'/'+ticketTypeId+'/additionalAreas');
            // convert string into array with strings
            selectedAdditionalAreas = selectedAdditionalAreas.substring(2, selectedAdditionalAreas.length - 2).split(',');
            //set selected
            $(this).find('#additional-areas').val(selectedAdditionalAreas).change();
        });

        $('#additional-areas-modal').on('hidden.bs.modal', function (e) {
            var $modal = $(e.target);
            // clear selected
            $modal.find('.progress').toggleClass('hidden');
            $modal.find('.multiselect-wrapper').toggleClass('hidden');
            $('#additional-areas').val('').change();
            $(this).find('input[name=id]').val('');
        });

        $('#delete-modal').on('show.bs.modal', function(e) {
            var formAction = $(e.relatedTarget).data('action'),
                formMethod = 'DELETE',
                $form = $(this).find('.form');

            $form.off('submit').on('submit', function (e) {
                e.preventDefault();
                $(document).find('.response-popup').remove();

                $.ajax({
                    type : formMethod,
                    url: formAction,
                    data: [],
                    success : function() {
                        $('#delete-modal').modal('hide');
                        window.location.reload();
                    },
                    error: function(data) {
                        var msgBox = ''
                            + '<div class="row response-popup" style="padding-top: 10px;">'
                            + '    <div class="col-lg-12">'
                            + '        <div class="alert alert-danger alert-dismissable">'
                            + '            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">'
                                            + "&times;"
                            + '            </button>'
                                            + data.responseText
                            + '        </div>'
                            + '    </div>'
                            + '</div>';
                        $('.page-header').parents('.row').before(msgBox);
                        $('#delete-modal').modal('hide');
                    }
                });
            });
        });
    });
</script>
@endpush
