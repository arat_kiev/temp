@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default"
                   href="{{ route('admin::areas.tickettypes.index', ['areaId'=> $area->id]) }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Produkt
                @if($ticketType->id)
                    bearbeiten
                    <small>{{ $ticketType->name }}</small>
                @else
                    erstellen
                @endif
            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Allgemein
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">{!! form_row($form->name) !!}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">{!! form_row($form->category) !!}</div>
                        <div class="col-sm-4">{!! form_row($form->duration) !!}</div>

                    </div>
                    <div class="alert alert-info">
                        Wochenkarte muss als Kategorie Tageskarte und Dauer 7 für eine Woche angelegt werden.<br />
                        Monatskarte ist gültig von 1. bis zum Letzten des Monats! Wenn eine 30 (31) Tage gültige Karte gewünscht ist, dann muss diese
                        ebenfalls als Tageskarte mit Dauert 30 bzw. 31 angelegt werden.
                    </div>
                    <div class="row">
                        <div class="col-sm-6">{!! form_row($form->is_catchlog_required) !!}</div>
                        <div class="col-sm-6">{!! form_row($form->group) !!}</div>
                    </div>
                </div>
            </div>

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                               aria-expanded="true" aria-controls="collapseOne" class="admin-collapse-link">
                                Zeitbeschränkung
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <p class="help-block">
                                        Falls nicht Sonnenauf-/untergang gewählt wird, wird die eingetragene Uhrzeit
                                        verwendet.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <fieldset>
                                    <div class="col-sm-12">
                                        <legend>
                                            <small>Angeltag beginnt</small>
                                        </legend>
                                    </div>
                                    <div class="col-sm-8">
                                        {!! form_row($form->day_starts_at_sunrise) !!}
                                        {!! form_row($form->day_starts_at_sunset) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! form_row($form->day_starts_at_time) !!}
                                    </div>
                                </fieldset>
                            </div>
                            <div class="row">
                                <fieldset>
                                    <div class="col-sm-12">
                                        <legend>
                                            <small>Angeltag endet</small>
                                        </legend>
                                    </div>
                                    <div class="col-sm-8">
                                        {!! form_row($form->day_ends_at_sunrise) !!}
                                        {!! form_row($form->day_ends_at_sunset) !!}
                                    </div>
                                    <div class="col-sm-4">
                                        {!! form_row($form->day_ends_at_time) !!}
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Einschränkungen
                </div>
                <div class="panel-body">
                    <div class="row">
                        <fieldset>
                            <div class="col-sm-12">
                                <legend>
                                    <small>Ausfang</small>
                                </legend>
                            </div>
                            <div class="col-sm-6">
                                {!! form_row($form->fish_per_day) !!}
                            </div>
                            <div class="col-sm-6">
                                {!! form_row($form->fishing_days) !!}
                            </div>
                        </fieldset>
                    </div>
                    <div class="row">
                        <fieldset>
                            <div class="col-sm-12">
                                <legend>
                                    <small>Angeltage</small>
                                </legend>
                                <p class="help-block">
                                    Wie oft kann im Zeitraum (pro Angelkarte) geangelt werden.
                                </p>
                            </div>
                            <div class="col-sm-6">
                                {!! form_row($form->checkin_id) !!}
                            </div>
                            <div class="col-sm-6">
                                {!! form_row($form->checkin_max) !!}
                            </div>
                        </fieldset>
                    </div>
                    <div class="row">
                        <fieldset>
                            <div class="col-sm-12">
                                <legend>
                                    <small>Kontingent</small>
                                </legend>
                                <p class="help-block">
                                    Wie viele Angelkarten können im Zeitraum verkauft werden.
                                </p>
                            </div>
                            <div class="col-sm-6">
                                {!! form_row($form->quota_id) !!}
                            </div>
                            <div class="col-sm-6">
                                {!! form_row($form->quota_max) !!}
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Bestimmungen
                </div>
                <div class="panel-body">
                    <div class="alert alert-info">
                        @include('admin.partials.rules.general')
                    </div>
                    {!! form_row($form->rule_text) !!}
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dateien
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        {!! form_label($form->rule_files_public) !!}
                        <div class="help-block">
                            @include('admin.partials.rules.public')
                        </div>
                        {!! form_widget($form->rule_files_public) !!}
                    </div>
                    @foreach($errors->getMessages() as $error => $messages)
                        @if(starts_with($error, 'rule_files_public.'))
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($messages as $msg)
                                        <li>{{ $msg }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    @endforeach
                    <div class="form-group">
                        {!! form_label($form->rule_files) !!}
                        <div class="help-block">
                            @include('admin.partials.rules.private')
                        </div>
                        {!! form_widget($form->rule_files) !!}
                    </div>
                    @foreach($errors->getMessages() as $error => $messages)
                        @if(starts_with($error, 'rule_files.'))
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($messages as $msg)
                                        <li>{{ $msg }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    @endforeach
                    <div class="form-group">
                        <label>Vorhandene Dateien</label>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Datei</th>
                                <th>Öffentlich</th>
                                <th>Löschen</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($ticketType->rule && $ticketType->rule->files()->count() > 0)
                                @foreach($ticketType->rule->files as $file)
                                    <tr>
                                        <td>
                                            {!! Html::linkRoute('filestream', $file->fileName, ['name' => $file->filePathWithName], ['target' => '_blank']) !!}
                                        </td>
                                        <td>
                                            <span class="fa {{ $file->pivot->public ? 'fa-check' : 'fa-times' }} fa-fw"></span>
                                        </td>
                                        <td>
                                            {!! Form::checkbox('rule_files_delete[]', $file->id) !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3" class="text-center">
                                        Keine Dateien vorhanden
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Berechtigungen
                </div>
                <div class="panel-body">
                    {!! form_row($form->required_licenses) !!}
                    {!! form_row($form->optional_licenses) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}
@endsection

@push('scripts')
    <script nonce="{{ csp_nounce() }}">
        $('document').ready(function () {
            $('#rule_text').summernote({
                lang: 'de-DE',
                height: '150px',
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'clear']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['link', ['linkDialogShow', 'unlink']],
                    ['misc', ['fullscreen', 'codeview', 'undo', 'redo', 'reset']]
                ],
                callbacks: {
                    onImageUpload: function (files) {
                        return false;
                    }
                }
            });

            $('#required_licenses,#optional_licenses').chosen({
                width: '100%'
            });

            $(".rule_files").change(function () {
                $("input[name^=rule_files_name]").each(function () {
                    $(this).prev('label').remove();
                    $(this).remove();
                });
                var names = [];
                for (var i = 0; i < $(this).get(0).files.length; ++i) {
                    names.push($(this).get(0).files[i].name);
                    // console.log($(this).get(0).files[i].name);
                    $("<div class='form-group'>" +
                        "   <label for='Dateiname' class='control-label'>" +
                        "Dateiname: " + $(this).get(0).files[i].name +
                        "   </label>" +
                        "   <input class='form-control' " +
                        "pattern='[a-zA-Z\\s]+' " +
                        "type='text' " +
                        "name='rule_files_name[" + $(this).get(0).files[i].name + "]' />" +
                        "</div>").insertAfter($(this).parent());
                }
            });

            $(".rule_files_public").change(function () {
                $("input[name^=rule_public_files_name]").each(function () {
                    $(this).prev('label').remove();
                    $(this).remove();
                });
                var names = [];
                for (var i = 0; i < $(this).get(0).files.length; ++i) {
                    names.push($(this).get(0).files[i].name);
                    // console.log($(this).get(0).files[i].name);
                    $("<div class='form-group'>" +
                        "   <label for='Dateiname' class='control-label'>" +
                        "Dateiname: " + $(this).get(0).files[i].name +
                        "   </label>" +
                        "   <input class='form-control' " +
                        "pattern='[a-zA-Z\\s]+' " +
                        "type='text' " +
                        "name='rule_public_files_name[" + $(this).get(0).files[i].name + "]' />" +
                        "</div>").insertAfter($(this).parent());
                }
            });
        });
    </script>
@endpush

@push('styles')
    <style>
        .admin-collapse-link {
            font-size: 14px;
            color: #333;
        }
        .admin-collapse-link:hover,  .admin-collapse-link:visited, .admin-collapse-link:focus {
            text-decoration: none;
            outline: none;
        }
    </style>
@endpush