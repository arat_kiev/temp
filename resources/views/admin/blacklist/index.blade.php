@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Sperrliste
                <div class="pull-right" style="padding-left: 8px;">
                    <button class="btn btn-success" data-target="#add-to-blacklist-by-user-data-modal" data-toggle="modal">
                        <span class="fa fa-plus fa-fw"></span> Person hinzufügen
                    </button>
                </div>
                <div class="pull-right">
                    <button class="btn btn-success" data-target="#add-to-blacklist-existing-user-modal" data-toggle="modal">
                        <span class="fa fa-plus fa-fw"></span> User per E-Mail / Fischer-ID sperren
                    </button>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered table-hover dataTable no-footer nowrap" id="datatable-users">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Manager ID</th>
                    <th>Vorname</th>
                    <th>Nachname</th>
                    <th>Geburtstag</th>
                    <th>Straße</th>
                    <th>Postleitzahl</th>
                    <th>Ort</th>
                    <th>Land ID</th>
                    <th>Land</th>
                    <th>Begründung</th>
                    <th>Gesperrt bis</th>
                    <th>Aktionen</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    {{-- block by user's email / fisher_id modal --}}
    <div class="modal fade in" id="add-to-blacklist-existing-user-modal" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" id="add-to-blacklist-existing-user-from" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            <span class="fa fa-ban fa-fw"></span> Sperrliste
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="existing-user-id" class="control-label"> User per E-Mail / Fischer-ID sperren </label>
                                    <select class="form-control" name="user_id" id="existing-user-id" required></select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="existing-user-banned-till"> Gesperrt bis </label>
                                    <div class="input-group date" data-provide="datepicker">
                                        <input id="existing-user-banned-till" name="banned_till" type="text" class="form-control" placeholder="dd.mm.yyyy">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="existing-user-reason" class="control-label"> Begründung </label>
                                    <textarea class="form-control" name="reason" id="existing-user-reason" rows="3" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="add-to-blacklist-existing-user-submit"><span class="fa fa-save fa-fw"></span> Speichern</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    {{-- block by user's data modal --}}
    <div class="modal fade in" id="add-to-blacklist-by-user-data-modal" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" id="add-to-blacklist-by-user-data-form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            <span class="fa fa-ban fa-fw"></span> Sperrliste
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4" style="display: none;">
                                <div class="form-group">
                                    <label for="blacklist_id" class="control-label">ID</label>
                                    <input class="form-control" name="id" type="text" id="id">
                                </div>
                                <div class="form-group">
                                    <label for="manager_id" class="control-label">ID</label>
                                    <input class="form-control" name="manager_id" type="text" id="manager_id">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="first_name" class="control-label">Vorname</label>
                                    <input class="form-control" name="first_name" type="text" id="first_name" placeholder="Max" required>
                                </div>
                                <div class="form-group">
                                    <label for="last_name" class="control-label">Nachname</label>
                                    <input class="form-control" name="last_name" type="text" id="last_name" placeholder="Mustermann" required>
                                </div>
                                <div class="form-group">
                                    <label for="birthday" class="control-label">Geburtstag</label>
                                    <div class="input-group date" data-provide="datepicker">
                                        <input type="text" class="form-control" name="birthday" id="birthday" placeholder="dd.mm.yyyy" required>
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="street" class="control-label">Straße</label>
                                    <input class="form-control" name="street" type="text" id="street" placeholder="Musterstraße 3">
                                </div>
                                <div class="form-group">
                                    <label for="city" class="control-label">Ort</label>
                                    <input class="form-control" name="city" type="text" id="city" placeholder="Musterstadt">
                                </div>
                                <div class="form-group">
                                    <label for="post_code" class="control-label">Postleitzahl</label>
                                    <input class="form-control" name="post_code" type="text" id="post_code" placeholder="2349">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="country" class="control-label">Land</label>
                                    <select class="form-control" name="country_id" id="country"></select>
                                </div>
                                <div class="form-group">
                                    <label for="banned_till">Gesperrt bis</label>
                                    <div class="input-group date" data-provide="datepicker">
                                        <input type="text" class="form-control" name="banned_till" id="banned_till" placeholder="dd.mm.yyyy">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="reason" class="control-label">Begründung</label>
                                    <textarea class="form-control" name="reason" id="reason" rows="1" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <h5 class="error-message"></h5>
                        <button type="button" class="btn btn-primary" id="add-to-blacklist-by-user-data-submit"><span class="fa fa-save fa-fw"></span> Speichern</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- delete confirmation modal --}}
    <div class="modal fade in" id="remove-from-blacklist-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Schließen"><span>&times;</span></button>
                    <h4 class="modal-title">
                        </span> Entfernen
                    </h4>
                </div>
                <div class="modal-body">
                    <p>Löschvorgang bestätigen</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="remove-from-blacklist">
                        <span class="fa fa-remove fa-fw"></span> Entfernen
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        var table = $('#datatable-users').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::blacklist.data') }}',
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'manager_id', visible: false },
                { data: 'first_name' },
                { data: 'last_name' },
                { data: 'birthday', class:"text-center" },
                { data: 'street' },
                { data: 'post_code', class:"text-center" },
                { data: 'city' },
                { data: 'country_id', visible: false },
                { data: 'country', 'searchable': false },
                { data: 'reason' },
                { data: 'banned_till', render: function (data) {
                        if(!!data) {
                            data = new Date(data)
                            var dateFormat = moment(data).format('DD.MM.YYYY');
                            if(moment(data).isBefore(moment())) {
                                return '<span class="text-muted">' + dateFormat + '</span>';
                            } else {
                                return dateFormat;
                            }
                        }
                        return '<div>&#8734;</div>';
                    },
                    class:"text-center"
                },
                { data: 'actions', searchable: false, sortable: false, class:"text-center" }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#add-to-blacklist-by-user-data-modal').on('show.bs.modal', function(e) {
            // Init country's search via select2
            $('#country').select2({
                width: '100%',
                minimumInputLength: 2,
                placeholder: 'Österreich',
                ajax: {
                    url: '{{ route('admin::blacklist.country') }}',
                    data: function (params) {
                        var query = {
                            search: params.term
                        }
                        return query;
                    }
                }
            });

            // Edit existing record
            if($(e.relatedTarget).hasClass('edit-user')) {
                var form = $(this).find('.form');
                var row = $("#datatable-users").dataTable().fnGetData($(e.relatedTarget).closest('tr'));
                form.find(':input').each(function(){
                    var field = $(this);
                    var id = field.attr('id');
                    // property exists in datatable row
                    if(row[id]) {
                        switch(id) {
                            // select2
                            case 'country':
                                var selectId = row[id+'_id'];
                                if (!field.find("option[value='" + selectId + "']").length) {
                                    field.append(new Option(row[id], selectId, false, false));
                                }
                                // change key name
                                id += '_id';
                                break;
                            // birthday
                            case  'banned_till':
                                row[id] = moment(row[id]).format('DD.MM.YYYY');
                                break;
                        }
                        field.val(row[id]).trigger('change');
                    }
                });
            }
        });

        // Clear form on window close
        $('#add-to-blacklist-by-user-data-modal, #add-to-blacklist-existing-user-modal').on('hidden.bs.modal', function(e) {
            var form = $(this).find('.form');
            // clear form
            form.find(':input').each(function(){
                $(this).val('');
                $(this).removeClass('required');
            });
            // clear error messages
            form.find('.error-message').text('');
        });

        $('#add-to-blacklist-existing-user-modal').on('show.bs.modal', function() {
            // Init user's search via select2
            $('#existing-user-id').select2({
                width: '100%',
                minimumInputLength: 6,
                placeholder: 'max@mustermann.at / ABC-123-DEF',
                ajax: {
                    url: '{{ route('admin::blacklist.users') }}',
                    data: function (params) {
                        var query = {
                            search: params.term
                        }
                        return query;
                    }
                }
            });
        });

        // Submit form's data
        $('#add-to-blacklist-existing-user-submit, #add-to-blacklist-by-user-data-submit').click(function () {
            var form = $('#' + $(this).closest('form').attr('id'));

            if(formValid(form)) {
                $.ajax( {
                    type: "POST",
                    url: '{{ route('admin::blacklist.users.add') }}',
                    data: form.serialize(),
                    success: function( response ) {
                        table.ajax.reload(null, false);
                        form.closest('.modal').modal('hide');
                    },
                    error: function(request) {
                        var fail =  $.parseJSON(request.responseText);
                        form.find('.error-message').html(fail.message || fail.error)
                    }
                } );
            }
        });

        // Remove from blacklist
        $('#remove-from-blacklist-modal').on('show.bs.modal', function (e) {
            var recordId = $(e.relatedTarget).closest('tr').find('td:first').text();

            $('#remove-from-blacklist').unbind('click').click(function () {
                var url = '{{ route("admin::blacklist.users.delete", ":id") }}'.replace(':id', recordId);
                $.ajax({
                    type : "DELETE",
                    url : url,
                    success: function (result) {
                        table.ajax.reload(null, false);
                        $('#remove-from-blacklist-modal').modal('hide')
                    }
                });
            });
        });

        // Form Validation
        function formValid(form) {
            var passed = true;
            // Validate form fields
            form.find(':input').each(function(){
                if($(this).attr('required') && !$(this).val()) {
                    passed = false;
                    if($(this).prop('tagName') == 'SELECT') {
                        $(this).next('.select2').find('.select2-selection').addClass('required');
                    } else {
                        $(this).addClass('required');
                    }
                }
            });
            return passed;
        }

    });
</script>
@endpush

@push('styles')
<style>
    #add-to-blacklist-by-user-data-modal .select2-selection,
    #add-to-blacklist-by-user-data-modal .select2-selection__arrow,
    #add-to-blacklist-existing-user-modal .select2-selection,
    #add-to-blacklist-existing-user-modal .select2-selection__arrow {
        height: 34px;
    }
    #add-to-blacklist-by-user-data-modal .select2-selection__rendered,
    #add-to-blacklist-existing-user-modal .select2-selection__rendered {
        line-height: 34px;
    }
    .required {
        border: 1px solid #FF614E !important;
    }
    .error-message {
        float: left;
        color: #FF614E;
        transition: opacity 1s ease-out;
    }
</style>
@endpush