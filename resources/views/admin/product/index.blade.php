@extends('admin.layout.manager')
@include('admin.layout.custom_dz_styles')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Produkte
                <small>Gesamt: {{ $count }}</small>
                <div class="pull-right">
                    <a href="{{ route('admin::products.create') }}" class="btn btn-success">
                        <span class="fa fa-plus fa-fw"></span> Produkt anlegen
                    </a>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="area">Gewässer</label>
                        <select class="form-control" name="area" id="filter-area">
                            @foreach($areaList as $areaId => $areaName)
                                <option
                                        value="{{ $areaId }}"
                                        @if($areaId == $area)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $areaName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-products">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Preis</th>
                        <th>Galerie</th>
                        <th>Gewässer</th>
                        <th>Zusätzliche Info</th>
                        <th>Anhang</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {{-- area modal --}}
    <div class="modal fade in" id="area-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form class="form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                        <h4 class="modal-title">
                            <span class="fa fa-group fa-fw"></span> Gewässer
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select class="form-control" id="areas" name="areas[]" multiple="multiple"></select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-save fa-fw"></span> Speichern
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- upload modal --}}
    <div class="modal fade in" id="upload-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <span class="fa fa-upload fa-fw"></span> Anhang hochladen
                    </h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center text-danger">
                        Achtung! Neue Dateien werden den vorhandenen Anlage ersetzen
                    </h4>
                    <form action="#" method="post" class="dz dz-clickable" id="gallery-dropzone">
                        <div class="dz-default dz-message">
                            <span>Anhang hierher ziehen um sie hochzuladen</span>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="upload-btn">Hochladen</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        function updateFilters() {
            var area = $('#filter-area').val(),
                location = '{{ route('admin::products.data') }}',
                params = [];

            if (area >= 0) {
                params.push('area=' + area);
            }
            $table.ajax.url(location + '?' + params.join('&')).load();
        }

        $('#filter-area').on('change', updateFilters);

        var $table = $('#datatable-products').DataTable({
            bFilter: true,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::products.data') }}',
            pagingType: "full_numbers",
            columns: [
                { data: 'id', searchable: false },
                { data: 'name' },
                { data: 'price', sortable: false, searchable: false },
                { data: 'gallery', sortable: false, searchable: false },
                { data: 'areas', sortable: false, searchable: false },
                { data: 'info_action', sortable: false, searchable: false },
                { data: 'attachment', sortable: false, searchable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#area-modal').on('show.bs.modal', function(e) {
            var button = $(e.relatedTarget);
            var formAction = button.data('action');

            var modal = $(this);
            modal.find('.form').attr('action', formAction);

            var areaSelect = modal.find('#areas');
            var areaData = [];

            areaSelect.children().remove();

            $.getJSON(formAction).done(function (data) {
                $.each(data.data, function (index, area) {
                    areaSelect.append($('<option/>', {
                        value: area.id,
                        text: area.name,
                        selected: true
                    }));
                });
            }).done(function () {
                areaSelect.select2({
                    width: '100%',
                    data: areaData,
                    ajax: {
                        url: '{{ route('admin::areas.all') }}',
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                search: params.term,
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            return {
                                results: data.data,
                                pagination: {
                                    more: params.page < data.meta.pagination.total_pages
                                }
                            };
                        },
                        cache: true
                    },
                    templateResult: function (area) {
                        if (area.loading) return area.name;

                        return "<div class='select2-result-admin'>"
                            + "     <div class='select2-result-admin__name'>" + area.name + " (" + area.id + ")</div>"
                            + "     <div class='select2-result-admin__address'>"
                                        + (area.country || '')
                                        + (area.state ? ' (' + area.state + ')' : '')
                            + "     </div>"
                            + " </div>";
                    },
                    templateSelection: function (area) {
                        return area.text || area.name;
                    },
                    escapeMarkup: function (markup) { return markup; }
                });
            });
        });

        $(function() {
            Dropzone.options.galleryDropzone = {
                paramName: 'file',
                maxFilesize: 10,
                maxFiles: 10,
                parallelUploads: 10,
                addRemoveLinks: true,
                autoProcessQueue: false,
                uploadMultiple: true,
                acceptedFiles: 'image/jpeg,image/png,application/pdf',
                dictDefaultMessage: 'Anhang hierher ziehen um sie hochzuladen',
                dictFallbackMessage: 'Ihr Browser unterstützt kein Drag\'n\'Drop',
                dictFallbackText: 'Bitte benutzen Sie das Formular unterhalb',
                dictInvalidFileType: 'Es sind nur JPEG, PNG und PDF erlaubt',
                dictFileTooBig: 'Eines der Anhang ist zu groß, maximal 10MB',
                dictResponseError: 'Der Server hatte ein Problem bei der Verarbeitung des/der Bildes/er',
                dictRemoveFile: 'Datei löschen',
                dictMaxFilesExceeded: "Sie können keine Dateien mehr hochladen"
            }
        });

        $('#upload-modal').off('show.bs.modal').on('show.bs.modal', function(e) {
            var $button = $(e.relatedTarget),
                formAction = $button.data('action'),
                myDropzone = new Dropzone("#upload-modal form", { url: formAction});

            myDropzone
                .on('successmultiple', function (file, response) {
                    if (response.status === 1) {
                        location.reload(true);
                    }
                })
                .on('error', function(file, errorMsg) {
                    console.log('Some error occured. Please reload the page');
                    $('#upload-modal .modal-body').html(errorMsg);
                });

            $('#upload-btn').on('click', function (e) {
                e.preventDefault();
                myDropzone.processQueue();
            });
        });

        $('#upload-modal').on('hide.bs.modal', function() {
            location.reload(true);
        });

        $('#datatable-products').on('submit', '.delete-attachment', deleteAttachmentCallback);

        function deleteAttachmentCallback(e) {
            e.preventDefault();
            var productId = $(this).data('id');

            if (confirm('Sind Sie sicher?')) {
                $('#datatable-products').off('submit', '.delete-attachment');
                $('#datatable-products').find('form.delete-attachment[data-id='+productId+']').submit();
            } else {
                $('#datatable-products')
                    .off('submit', '.delete-attachment')
                    .on('submit', '.delete-attachment', deleteAttachmentCallback);
            }
        }

        $('#datatable-products').on('click', '.btn-hidden-info', function () {
            var button = $(this);
            var infoText = button.data('text');
            var storeInfoAction = button.data('action');

            swal({
                title: 'Zusätzliche Information',
                type: 'info',
                input: 'textarea',
                inputValue: infoText ? infoText : '',
                inputPlaceholder: 'Bitte Text eintragen',
                showCancelButton: true,
                confirmButtonClass: 'btn btn-modal btn-success',
                cancelButtonClass: 'btn btn-modal btn-default',
                confirmButtonText: 'Speichern',
                cancelButtonText: 'Abbrechen',
                buttonsStyling: false
            }).then(function (result) {
                if (!result.dismiss) {
                    $.postJSON(storeInfoAction, {
                        info: result.value
                    }).always(function () {
                        location.reload();
                    });
                }
            });
        });
    });

    $.postJSON = function (url, data, callback, error) {
        return jQuery.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: callback,
            error: error
        });
    };
</script>
@endpush

@push('styles')
<style>
    .btn-modal {
        margin: 0 0.5rem;
    }

    #upload-modal .modal-body {
        overflow: overlay;
    }
</style>
@endpush
