<p>Bestimmungen können auf folgenden Ebenen eingetragen werden: Bewirschafter, Gewässer, Angelkarten.</p>

<p>
    Es gilt die Reihenfolge: Angelkarten vor Gewässer vor Bewirtschafter.
    Zum Beispiel: Wenn auf Angelkartenebene ein Bestimmungstext oder ein Dokument hinterlegt wurde,
    so werden Bestimmungen auf Gewässer- bzw. Bewirtschafterebene ignoriert!
</p>