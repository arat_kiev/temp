@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::areas.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Gallerie <small>{{ $area->name }}</small>
                <div class="pull-right">
                    <a href="#" class="btn btn-success" data-toggle="modal" data-target="#upload-modal"><span class="fa fa-upload fa-fw"></span> Bilder hochladen</a>
                </div>
            </h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        @if($gallery)
            @foreach($gallery->pictures()->orderBy('pivot_priority', 'desc')->get() as $picture)
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="thumbnail">
                        <img src="{{ route('imagecache', ['template' => 'lre', 'filename' => $picture->file]) }}" />
                        <div class="caption">
                            <p class="image-caption-name">
                                <span class="badge">{{ $picture->pivot->priority }}</span>
                                <b>{{ $picture->name or '<Kein Name>' }}</b>
                            </p>
                        </div>
                        <div class="btn-group btn-group-justified">
                            <a href="{{ route('admin::areas.gallery.edit', ['areaId' => $area->id, 'pictureId' => $picture->id]) }}" class="btn btn-primary">
                                Bearbeiten<span class="fa fa-edit fa-fw"></span>
                            </a>
                            <!-- Single button -->
                            <div class="btn-group">
                                <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="fa fa-remove fa-fw"></span> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('admin::areas.gallery.delete', ['areaId' => $area->id, 'pictureId' => $picture->id]) }}">Löschen</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="col-lg-12">
                <div class="alert alert-warning">
                    Noch keine Bilder vorhanden.
                </div>
            </div>
        @endif
    </div>

    {{-- upload modal --}}
    <div class="modal fade in" id="upload-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                    <h4 class="modal-title"><span class="fa fa-upload fa-fw"></span> Bilder hochladen</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin::areas.gallery.upload', ['areaId' => $area->id]) }}" class="dropzone dz-clickable" id="gallery-dropzone">
                        <div class="dz-default dz-message"><span>Bilder hierher ziehen um sie hochzuladen</span></div></form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $(function()
    {
        Dropzone.options.galleryDropzone = {
            paramName: 'file',
            maxFilesize: 20,
            acceptedFiles: 'image/jpeg,image/png',
            dictDefaultMessage: 'Bilder hierher ziehen um sie hochzuladen',
            dictFallbackMessage: 'Ihr Browser unterstützt kein Drag\'n\'Drop',
            dictFallbackText: 'Bitte benutzen Sie das Formular unterhalb',
            dictInvalidFileType: 'Es sind nur JPEG und PNG erlaubt',
            dictFileTooBig: 'Eines der Bilder ist zu groß, maximal 10MB',
            dictResponseError: 'Der Server hatte ein Problem bei der Verarbeitung des/der Bildes/er'
        }
    });

    $('#upload-modal').on('hide.bs.modal', function()
    {
        location.reload(true);
    })
</script>
@endpush