@extends('admin.layout.manager')

@inject('ticketManager', 'App\Managers\TicketManager')

@php
$dbTicket = $ticket ? \App\Models\Ticket\Ticket::find($ticket) : null;
@endphp

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Fangstatistik
                <div class="pull-right">
                    <div class="dropdown">
                        <button type="button" class="btn btn-default" data-toggle="dropdown">
                            <span class="fa fa-download fa-fw"></span> Export
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="{{ route('admin::inspect:hauls.export', [
                                        'format' => 'xls',
                                        'ticket' => $ticket ?: null,
                                    ]) }}">
                                    <span class="fa fa-file-excel-o fa-fw"></span> XLS-Datei
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin::inspect:hauls.export', [
                                        'format' => 'csv',
                                        'ticket' => $ticket ?: null
                                    ]) }}">
                                    <span class="fa fa-file-text-o fa-fw"></span> CSV-Datei
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="ticket">Angelkarten-Nr.</label>
                        <div class="input-group">
                            <input class="form-control"
                                   name="ticket"
                                   id="ticket"
                                   value="{{ $dbTicket ? $ticketManager->getIdentifier($dbTicket) : '' }}">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="button" id="filter-ticket">Filter</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover dataTable no-footer" id="datatable-hauls">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Fischart</th>
                    <th>Größe/Gewicht</th>
                    <th>Fischer</th>
                    <th>Angelkarten-Nr.</th>
                    <th>Gewässer</th>
                    <th>Fangdatum/Zeit</th>
                    <th>Kommentar</th>
                </tr>
                </thead>
            </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function () {
        $('#filter-ticket').on('click', function (e) {
            e.preventDefault();
            var ticketNumber = $('#ticket').val(),
                ticketIdMatch = ticketNumber.match({!! \App\Models\Ticket\Ticket::IDENT_REGEX !!});

            window.location = '{{ route('admin::inspect:hauls.index') }}'
                                + (ticketIdMatch && ticketIdMatch[2] ? '?ticket='+parseInt(ticketIdMatch[2]) : '');
        });

        $('#datatable-hauls').DataTable({
            bFilter: false,
            // stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::inspect:hauls.data') }}' + window.location.search,
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'fish_name', orderable: false },
                { data: 'fish_size', orderable: false },
                { data: 'user_name', orderable: false },
                { data: 'ticket_number', orderable: false },
                { data: 'area_name', orderable: false },
                { data: 'catch_date' },
                { data: 'user_comment', orderable: false }
            ],
            aaSorting: [[ 6, "desc" ]],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });
    });
</script>
@endpush