@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Angelkarten
                {{--<small>Gesamt: <span class="count-tickets">{{ $countTickets }}</span></small>--}}
                <small class="sub">
                    {{ \Carbon\Carbon::createFromFormat('Y-m-d', $from)->format('d.m.Y') }} -
                    {{ \Carbon\Carbon::createFromFormat('Y-m-d', $till)->format('d.m.Y') }}
                </small>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="reseller">Gewässer</label>
                        <select class="form-control" name="reseller" id="filter-area">
                            @foreach($areaList as $areaId => $areaName)
                                <option
                                        value="{{ $areaId }}"
                                        @if($areaId == $area)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $areaName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="reseller">Angelkarte Typ</label>
                        <select class="form-control" name="reseller" id="filter-ticketType">
                            @foreach($typeList as $typeId => $typeName)
                                <option
                                        value="{{ $typeId }}"
                                        @if($typeId == $ticketType)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $typeName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group input-daterange">
                        <div class="input-group-addon input-group-label">Gültig</div>
                        <input type="text"
                               class="form-control"
                               id="date-range-from"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $from)->format('d.m.Y') }}">
                        <span class="input-group-addon">bis</span>
                        <input type="text"
                               class="form-control"
                               id="date-range-till"
                               value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $till)->format('d.m.Y') }}">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="button" id="filter-date-range-btn">Filter</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer" id="datatable-tickets">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Aktionen</th>
                        <th>Name</th>
                        <th>Gewässer</th>
                        <th>Verkäufer</th>
                        <th>Typ</th>
                        <th>Gekauft am</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {{-- preview modal --}}
    <div class="modal fade in" id="preview-modal" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                    <h4 class="modal-title">
                        <span class="fa fa-eye fa-fw"></span>
                        <span id="preview-title">Vorschau wird geladen (bitte etwas Geduld)</span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="col-lg-12">
                            <img class="preview-image img-responsive" width="1083px" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Schließen</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('.input-daterange').datepicker();

        function updateFilters() {
            var from = $('#date-range-from').datepicker('getDate'),
                till = $('#date-range-till').datepicker('getDate'),
                fromString = moment(from).format('YYYY-MM-DD'),
                tillString = moment(till).format('YYYY-MM-DD'),
                area = $('#filter-area').val(),
                ticketType = $('#filter-ticketType').val(),
                location = '{{ route('admin::inspect:tickets.data') }}',
                params = [
                    'from=' + fromString,
                    'till=' + tillString
                ];

            if (area > 0) {
                params.push('area=' + area);
            }
            if (ticketType > 0) {
                params.push('ticketType=' + ticketType);
            }
            table.ajax.url(location + '?' + params.join('&')).load();
            $('small.sub').text(moment(from).format('DD.MM.YYYY') + ' - ' + moment(till).format('DD.MM.YYYY'));
        }

        $('#filter-date-range-btn').on('click', updateFilters);
        $('#date-range-from').on('change', updateFilters);
        $('#date-range-till').on('change', updateFilters);
        $('#filter-area').on('change', updateFilters);
        $('#filter-ticketType').on('change', updateFilters);

        var table = $('#datatable-tickets').DataTable({
            bFilter: false,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::inspect:tickets.data') }}',
            pagingType: "full_numbers",
            columns: [
                { data: 'id' },
                { data: 'actions', sortable: false },
                { data: 'user.full_name', sortable: false },
                { data: 'type.area.name', sortable: false },
                { data: 'reseller', sortable: false },
                { data: 'type.name', sortable: false },
                { data: 'created_at' }

            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });

        $('#preview-modal').on('show.bs.modal', function(e) {
            var modal = $(this);
            var button = $(e.relatedTarget);
            var ticketId = button.data('ticketId');
            var format = 'image';

            var title = modal.find('#preview-title');
            title.html('Vorschau wird geladen <span class="fa fa-cog fa-spin"></span>');

            modal.find('.preview-image')
                    .attr('src', '{{ URL::to('/preview/ticket/') }}/' + ticketId + '/' + format)
                    .on('load', function() {
                        title.html('Vorschau für Ticket: ' + ticketId);
                    });
        });
    });
</script>
@endpush

@push('styles')
<style>
    .page-header small.sub {
        display: block;
        font-size: 55%;
        padding-top: 10px;
    }
</style>
@endpush
