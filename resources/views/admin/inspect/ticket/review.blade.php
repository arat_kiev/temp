@extends('admin.layout.manager')

@inject('ticketManager', 'App\Managers\TicketManager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <a class="btn btn-sm btn-default" href="{{ route('admin::inspect:tickets.index') }}">
                    <i class="fa fa-chevron-left"></i>
                </a>
                Ticket Kontrolle <small>{{ $ticketManager->getIdentifier($ticket) }}</small>
            </h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    Es sind Fehler aufgetreten. Bitte kontrollieren Sie Ihre Eingaben.
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

    {!! form_start($form) !!}
    <div class="row">
        <div class="col-lg-6 col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Kontrolle
                </div>
                <div class="panel-body">
                    <h2>Kontroll-Nummer: {{ $ticketManager->getControlCode($ticket) }}</h2>
                    <div class="funkyradio">
                        {!! form_row($form->status) !!}
                    </div>
                    {!! form_row($form->comment) !!}
                    {!! form_row($form->latitude) !!}
                    {!! form_row($form->longitude) !!}
                    {!! form_row($form->accuracy) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            {!! form_widget($form->submit) !!}
        </div>
    </div>
    {!! form_end($form) !!}

    <hr>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover dataTable no-footer" id="datatable-inspections">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Kontrolleur</th>
                        <th>Status</th>
                        <th>Kommentar</th>
                        <th>Datum</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        $('input[type=radio][value=OK]').closest('div').addClass('funkyradio-success');
        $('input[type=radio][value=NOT_OK]').closest('div').addClass('funkyradio-danger');

        getLocation();

        var $table = $('#datatable-inspections').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            searching: false,
            ajax: {
                url: '{{ route('admin::inspect:tickets.inspect.data', ['ticketId' => $ticket->id]) }}'
            },
            pagingType: "full_numbers",
            columns: [
                { data: 'id' , width: '5%'},
                { data: 'inspector'},
                { data: 'status', width: '5%' },
                { data: 'comment' },
                { data: 'created_at', width: '20%' }
            ],
            columnDefs: [{
                targets : [0, 1, 2, 3, 4],
                orderable: false
            }],
            aaSortingFixed: [[ 4, "desc" ]],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            },
            createdRow: function(row, data) {
                if (data['status'].indexOf("check") >= 0) {
                    $(row).addClass('success');
                } else if ((data['status'].indexOf("times") >= 0)) {
                    $(row).addClass('danger');
                }
            }
        });

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition, showError);
            } else {
                showNotSupportedError();
            }
        }

        function showNotSupportedError() {
            var msgBox = ''
                + '<div class="row response-popup" style="padding-top: 10px;">'
                + '    <div class="col-lg-12">'
                + '        <div class="alert alert-danger alert-dismissable">'
                + '            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">'
                + '                &times;'
                + '            </button>'
                + '            Geolocation wird von diesem Browser nicht unterstützt.'
                + '        </div>'
                + '    </div>'
                + '</div>';
            $('.page-header').parents('.row').before(msgBox);
        }

        function showPosition(position) {
            // console.log(position);
            $('#latitude').val(position.coords.latitude);
            $('#longitude').val(position.coords.longitude);
            $('#accuracy').val(position.coords.accuracy);
        }

        function showError(error) {
            var errMsg = fetchGeoResponseMessage(error);

            var msgBox = ''
                + '<div class="row response-popup" style="padding-top: 10px;">'
                + '    <div class="col-lg-12">'
                + '        <div class="alert alert-danger alert-dismissable">'
                + '            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">'
                + '                &times;'
                + '            </button>'
                +               errMsg
                + '        </div>'
                + '    </div>'
                + '</div>';
            $('.page-header').parents('.row').before(msgBox);
        }

        function fetchGeoResponseMessage(error) {
            switch (error.code) {
                case error.PERMISSION_DENIED:
                    return "Bitte aktivieren Sie der Geolocation in Ihren Browser.";
                case error.POSITION_UNAVAILABLE:
                    return "Standortinformationen sind nicht verfügbar.";
                case error.TIMEOUT:
                    return "Der Antrag Benutzerstandort erhalten timed out.";
                case error.UNKNOWN_ERROR:
                    return "Ein unbekannter Fehler ist aufgetreten.";
                default:
                    return "Ein Fehler ist aufgetreten.";
            }
        }
    });
</script>
@endpush

@include('admin.layout.funkyradio')

@push('styles')
<style>
    .funkyradio label {
        font-weight: bold !important;
    }
    .funkyradio div.funkyradio-danger input[type="radio"]:hover:not(:checked) ~ label:before,
    .funkyradio div.funkyradio-danger input[type="checkbox"]:hover:not(:checked) ~ label:before,
    .funkyradio div.funkyradio-danger input[type="radio"]:checked ~ label:before,
    .funkyradio div.funkyradio-danger input[type="checkbox"]:checked ~ label:before {
        content:'\2715';
    }
</style>
@endpush
