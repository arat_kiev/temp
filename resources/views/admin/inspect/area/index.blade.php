@extends('admin.layout.manager')

@section('page-content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Gewässer
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5 col-md-6 pull-right">
                    <div class="form-group">
                        <label for="reseller">Verfügbare Angelkarte Typen</label>
                        <select class="form-control" name="reseller" id="filter-ticketCategory">
                            @foreach($categoryList as $categoryId => $categoryName)
                                <option
                                        value="{{ $categoryId }}"
                                        @if($categoryId == $ticketCategory)
                                        selected="selected"
                                        @endif
                                >
                                    {{ $categoryName }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer nowrap" id="datatable-areas">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Öffentlich</th>
                        <th>Bewirtschafter</th>
                        <th>Verkaufsstellen</th>
                        <th>Produkte</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script nonce="{{ csp_nounce() }}">
    $('document').ready(function() {
        function updateFilters() {
            var ticketCategory = $('#filter-ticketCategory').val(),
                location = '{{ route('admin::inspect:areas.data') }}',
                params = [];

            if (ticketCategory > 0) {
                params.push('ticketCategory=' + ticketCategory);
            }
            $table.ajax.url(location + '?' + params.join('&')).load();
        }

        $('#filter-ticketCategory').on('change', updateFilters);

        var $table = $('#datatable-areas').DataTable({
            bFilter: false,
            stateSave: true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin::inspect:areas.data') }}',
            pagingType: "full_numbers",
            columns: [
                { data: 'name' },
                { data: 'public', sortable: false },
                { data: 'manager', sortable: false },
                { data: 'resellers', sortable: false },
                { data: 'ticket_types', sortable: false }
            ],
            language: {
                decimal: ",",
                thousands: ".",
                lengthMenu: "Zeige _MENU_ Einträge pro Seite",
                zeroRecords: "Keine Einträge vorhanden",
                info: "Seite _PAGE_ von _PAGES_",
                infoEmpty: "Keine Einträge vorhanden",
                infoFiltered: "(gefiltert von _MAX_ gesamten Einträgen)",
                search: "Suche",
                paginate: {
                    first: "&laquo;",
                    previous: "&lsaquo;",
                    next: "&rsaquo;",
                    last: "&raquo;"
                }
            }
        });
    });
</script>
@endpush
