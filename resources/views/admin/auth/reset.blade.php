@extends('admin.layout.base')

@push('styles')
{!! Html::style('css/admin_login.css') !!}
@endpush

<?php
$loginLink = $portal == 'admin'
    ? route('admin::login')
    : route('pos::login', ['pos' => $portal]);
?>

@section('body')
    <div class="container-fluid reset">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3">
                        <div class="reset-panel panel panel-default">
                            <div class="panel-heading text-center">
                                {!! Html::image('img/admin/logo-admin.png', 'logo') !!}
                            </div>
                            <div class="panel-body">
                                {!! Form::open(['route' => 'admin::reset', 'method' => 'post']) !!}
                                <h3>Passwort zurücksetzen</h3>
                                <fieldset>
                                    <div class="form-group input-group">
                                            <span class="input-group-addon">
                                                <span class="fa fa-user fa-fw"></span>
                                            </span>
                                        {!! Form::input(
                                            'email',
                                            'email',
                                            null,
                                            ['class' => 'form-control', 'placeholder' => 'E-Mail', 'autofocus']
                                        ) !!}
                                    </div>
                                    <button type="submit" class="btn btn-lg btn-hej btn-block">
                                        Passwort zurücksetzen
                                    </button>
                                </fieldset>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <a href="{{ $loginLink }}" class="links">
                            Zurück zum Login
                        </a>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @include('flash::message')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
