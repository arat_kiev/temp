@extends('admin.layout.base')

@push('styles')
    {!! Html::style('css/admin_login.css') !!}
@endpush

@section('body')
    <div class="container-fluid login">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3">
                        <div class="login-panel panel panel-default">
                            <div class="panel-heading text-center">
                                {!! Html::image('img/admin/logo-admin.png', 'logo') !!}
                            </div>
                            <div class="panel-body">
                                {!! Form::open(['route' => ['pos::login', Route::input('pos', $portal)], 'method' => 'post']) !!}
                                    <fieldset>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon">
                                                <span class="fa fa-user fa-fw"></span>
                                            </span>
                                            {!! Form::input(
                                                'email',
                                                'email',
                                                null,
                                                ['class' => 'form-control', 'placeholder' => 'E-Mail', 'autofocus']
                                            ) !!}
                                        </div>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon">
                                                <span class="fa fa-lock fa-fw"></span>
                                            </span>
                                            {!! Form::input(
                                                'password',
                                                'password',
                                                null,
                                                ['class' => 'form-control', 'placeholder' => 'Passwort']
                                            ) !!}
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                {!! Form::input(
                                                    'checkbox',
                                                    'remember',
                                                    'Remember Me',
                                                    ['checked']
                                                ) !!} An mich erinnern
                                            </label>
                                        </div>
                                        <button type="submit" class="btn btn-lg btn-hej btn-block">Anmelden</button>
                                    </fieldset>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <a href="{{ $resetLink }}" class="links">
                            Ich habe mein Passwort vergessen
                        </a>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @include('flash::message')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
