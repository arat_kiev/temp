<div class="form-group col-md-6">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="text-right" style="margin-top: -16px;">
                    <button class="btn btn-sm btn-danger translations-btn-remove">
                        <span class="fa fa-fw fa-times"></span>
                    </button>
                </div>
                <div class="col-sm-12">
                    {!! form_row($options['children']['id']) !!}
                    {!! form_row($options['children']['locale']) !!}
                    {!! form_row($options['children']['name']) !!}
                    {!! form_row($options['children']['content']) !!}
                    {!! form_row($options['children']['file']) !!}
                    @if($options['children']['file']->getValue())
                        <span class="fa fa-fw fa-file-pdf-o"></span>
                        {!!
                            Html::linkRoute('filestream',
                                $options['children']['file']->getValue()->fileName,
                                [
                                    'name' => $options['children']['file']->getValue()->filePathWithName
                                ],
                                [
                                    'target' => '_blank'
                                ]);
                        !!}
                    @else
                        Keine Datei vorhanden.
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>