<?php
    $weekDays = [
        'MON'   => 'Montag',
        'TUE'   => 'Dienstag',
        'WED'   => 'Mittwoch',
        'THU'   => 'Donnerstag',
        'FRI'   => 'Freitag',
        'SAT'   => 'Samstag',
        'SUN'   => 'Sonntag',
    ];
?>

<div class="form-group poi-group">
    <fieldset>
        <legend>
            @if($options['value'])
                {{ $weekDays[$options['value']['day']] }}
            @else
                __OPENINIG_HOUR_TITLE__
            @endif
            <button class="btn btn-xs btn-danger pull-right opening_hours-btn-remove">&times;</button>
        </legend>

        @if($showLabel && $options['label'] !== false)
            {!! Form::label($name, $options['label'], $options['label_attr']) !!}
        @endif

        <div class="row">
            {!! $options['children']['day']->render() !!}
            {{--{!! $options['children']['name']->render() !!}--}}

            @if(is_object($options['value']))
                <div class="col-sm-6">
                    <div class='input-group date'>
                        {!! $options['children']['opens']->render(['value' => $options['value']->opens ?: null]) !!}
                        <span class="input-group-addon" style="display: none">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class='input-group date'>
                        {!! $options['children']['closes']->render(['value' => $options['value']->closes ?: null]) !!}
                        <span class="input-group-addon" style="display: none">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
            @elseif($options['value'])
                <div class="col-sm-6">
                    <div class='input-group date'>
                        {!! $options['children']['opens']->render(['value' => $options['value']['opens']]) !!}
                        <span class="input-group-addon" style="display: none">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class='input-group date'>
                        {!! $options['children']['closes']->render(['value' => $options['value']['closes']]) !!}
                        <span class="input-group-addon" style="display: none">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
            @else
                <div class="col-sm-6">
                    <div class='input-group date'>
                        {!! $options['children']['opens']->render() !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class='input-group date'>
                        {!! $options['children']['closes']->render() !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
            @endif
        </div>
    </fieldset>
</div>
