<div class="form-group">
    @if($showLabel && $options['label'] !== false)
        {!! Form::label($name, $options['label'], $options['label_attr']) !!}
    @endif
    <div class="row">
        <div class="col-sm-12">
            {!! $options['children']['id']->render() !!}
            <div class="input-group">
                <span class="input-group-addon">Anzahl</span>
                {!! $options['children']['count']->render() !!}
                <span class="input-group-addon">Rabatt (&euro;)</span>
                {!! $options['children']['discount']->render([
                    'value' => $options['children']['discount']->getValue() ?
                        is_int($options['children']['discount']->getValue()) ?
                        number_format($options['children']['discount']->getValue() / 100, 2, ',', '.') :
                        $options['children']['discount']->getValue() : '0,00',
                ]) !!}
                <span class="input-group-addon">Inklusive</span>
                {!! $options['children']['inclusive']->render([], false, true, false) !!}
                @unless(isset($options['formOptions']['disabled']))
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Aktion
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="#" class="discounts-btn-remove bg-danger">
                                    <span class="fa fa-remove fa-fw"></span>
                                    Löschen
                                </a>
                            </li>
                        </ul>
                    </div>
                @endunless
            </div>
        </div>
    </div>
</div>