<div class="form-group">
    @if($showLabel && $options['label'] !== false)
        {!! Form::label($name, $options['label'], $options['label_attr']) !!}
    @endif
    <div class="row">
        <div class="col-sm-12">
            {!! $options['children']['id']->render() !!}
            <div class="input-group input-daterange">
                {!! $options['children']['from']->render(
                    ['value' => $options['children']['from']->getValue() ? Carbon\Carbon::parse($options['children']['from']->getValue())->format('H:i') : ''],
                    false, true, false
                ) !!}
                <span class="input-group-addon">bis</span>
                {!! $options['children']['till']->render(
                    ['value' => $options['children']['till']->getValue() ? Carbon\Carbon::parse($options['children']['till']->getValue())->format('H:i') : ''],
                    false, true, false
                ) !!}
                <span class="input-group-addon">Menge</span>
                {!! $options['children']['default_pool']->render([], false, true, false) !!}
                @unless(isset($options['formOptions']['disabled']))
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Aktion
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="#" class="timeslots-btn-remove bg-danger">
                                    <span class="fa fa-remove fa-fw"></span>
                                    Löschen
                                </a>
                            </li>
                        </ul>
                    </div>
                @endunless
            </div>
        </div>
    </div>
</div>