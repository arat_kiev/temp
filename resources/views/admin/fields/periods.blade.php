<div class="form-group">
    @if($showLabel && $options['label'] !== false)
        {!! Form::label($name, $options['label'], $options['label_attr']) !!}
    @endif
    <div class="row">
        <div class="col-sm-12">
            <div class="input-group input-daterange">
                {!! $options['children']['lock_from_string']->render() !!}
                <span class="input-group-addon">bis</span>
                {!! $options['children']['lock_till_string']->render() !!}
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Aktion
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="#" class="lock-periods-btn-remove bg-danger">
                                <span class="fa fa-remove fa-fw"></span>
                                Löschen
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>