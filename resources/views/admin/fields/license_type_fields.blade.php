@php $disabled = isset($options['value']['disabled']) && $options['value']['disabled'] ? 'disabled' : ''; @endphp
<div class="form-group field-group">
    <fieldset>
        <legend>
            @if($options['value'])
                {{ $options['value']['name'] }}
            @else
                __FIELD__
            @endif
            <button class="btn btn-xs btn-danger pull-right field-btn-remove {{ $disabled }}"
                    {{ $disabled }}>&times;</button>
        </legend>

        @if($showLabel && $options['label'] !== false)
            {!! Form::label($name, $options['label'], $options['label_attr']) !!}
        @endif
        <div class="row">
            {!! $options['children']['field_type']->render() !!}
            {{--{!! $options['children']['name']->render() !!}--}}

            <div class="col-sm-12">
                @if($options['value'])
                    {!! $options['children']['field_name']->render(['value' => $options['value']['field_name']]) !!}
                @else
                    {!! $options['children']['field_name']->render() !!}
                @endif
            </div>
        </div>
    </fieldset>
</div>