<div class="form-group">
    <label>{{ $options['label'] }}</label>
    <br />
    <div class="btn-group" data-toggle="buttons">
        @foreach($options['options'] as $option)
            <label class="btn btn-{{ $option['class'] }}">
                <input type="radio" name="{{ $option['name'] }}" value="{{ $option['value'] }}" autocomplete="off">
                {{ $option['label'] }}
            </label>
        @endforeach
    </div>
</div>