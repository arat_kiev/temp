<div class="form-group">
    @if($showLabel && $options['label'] !== false)
        {!! Form::label($name, $options['label'], $options['label_attr']) !!}
    @endif
        <div class="row">
            @if(is_object($options['value']))
                <div class="col-sm-6">
                    <div class='input-group date'>
                        {!! $options['children']['checkin_from']->render(['value' => $options['value']->checkin_from ?: null]) !!}
                        <span class="input-group-addon" style="display: none">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class='input-group date'>
                        {!! $options['children']['checkin_till']->render(['value' => $options['value']->checkin_till ?: null]) !!}
                        <span class="input-group-addon" style="display: none">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
            @elseif($options['value'])
                <div class="col-sm-6">
                    <div class='input-group date'>
                        {!! $options['children']['checkin_from']->render(['value' => $options['value']['checkin_from']]) !!}
                        <span class="input-group-addon" style="display: none">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class='input-group date'>
                        {!! $options['children']['checkin_till']->render(['value' => $options['value']['checkin_till']]) !!}
                        <span class="input-group-addon" style="display: none">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
            @else
                <div class="col-sm-6">
                    <div class='input-group date'>
                        {!! $options['children']['checkin_from']->render() !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class='input-group date'>
                        {!! $options['children']['checkin_till']->render() !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
            @endif
        </div>
</div>