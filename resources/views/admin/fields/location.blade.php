<div class="form-group">
    @if($showLabel && $options['label'] !== false)
        {!! Form::label($name, $options['label'], $options['label_attr']) !!}
    @endif

    <div class="input-group">
        {!! Form::input($type, $name, $options['value'], $options['attr']) !!}
        <div class="input-group-btn">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Aktion
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                    <a href="#" class="locations-btn-remove bg-danger">
                        <span class="fa fa-remove fa-fw"></span>
                        Löschen
                    </a>
                </li>
            </ul>
        </div>
    </div>
    @include('laravel-form-builder::help_block')
    @include('laravel-form-builder::errors')
</div>