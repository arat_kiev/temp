<div class="form-group fish-group">
    <fieldset>
        <legend>
            @if($options['value'])
                {{ $options['value']['name'] }}
            @else
                __FISH__
            @endif
            <button class="btn btn-xs btn-danger pull-right fishes-btn-remove">&times;</button>
        </legend>

        @if($showLabel && $options['label'] !== false)
            {!! Form::label($name, $options['label'], $options['label_attr']) !!}
        @endif
        <div class="row">
            {!! $options['children']['id']->render() !!}
            {{--{!! $options['children']['name']->render() !!}--}}

            <div class="col-sm-6">
                @if(is_object($options['value']))
                    {!! $options['children']['min_length']->render(['value' => $options['value']->pivot->min_length]) !!}
                @elseif($options['value'])
                    {!! $options['children']['min_length']->render(['value' => $options['value']['min_length']]) !!}
                @else
                    {!! $options['children']['min_length']->render() !!}
                @endif
            </div>
            <div class="col-sm-6">
                @if(is_object($options['value']))
                    {!! $options['children']['max_length']->render(['value' => $options['value']->pivot->max_length]) !!}
                @elseif($options['value'])
                    {!! $options['children']['max_length']->render(['value' => $options['value']['max_length']]) !!}
                @else
                    {!! $options['children']['max_length']->render() !!}
                @endif
            </div>
            <div class="col-sm-6">
                @if(is_object($options['value']))
                    {!! $options['children']['closed_from']->render(['value' => $options['value']->closed_from ? $options['value']->closed_from->format('d.m.Y') : null]) !!}
                @elseif($options['value'])
                    {!! $options['children']['closed_from']->render(['value' => $options['value']['closed_from']]) !!}
                @else
                    {!! $options['children']['closed_from']->render() !!}
                @endif
            </div>
            <div class="col-sm-6">
                @if(is_object($options['value']))
                    {!! $options['children']['closed_till']->render(['value' => $options['value']->closed_till ? $options['value']->closed_till->format('d.m.Y') : null]) !!}
                @elseif($options['value'])
                    {!! $options['children']['closed_till']->render(['value' => $options['value']['closed_till']]) !!}
                @else
                    {!! $options['children']['closed_till']->render() !!}
                @endif
            </div>
        </div>
    </fieldset>
</div>