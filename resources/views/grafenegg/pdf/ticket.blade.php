@extends('ticket.layout.base')

@inject ('tm', 'App\Managers\TicketManager')
@php
    $hideCashPayment = true;
@endphp

@section('main_ticket_catchlog')
    @if($tm->shouldDisplayCatchlog($ticket) && $ticket->type->fish_per_day > 0)
        <table class="log">
            <thead>
            <tr>
                <th class="type">Fischart</th>
                <th class="size">kg</th>
                <th class="type">Fischart</th>
                <th class="size">kg</th>
            </tr>
            </thead>
            <tbody>
            @foreach (range(1, $ticket->type->fish_per_day) as $i)
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>{{ $i }}</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection
@section('main_ticket_intro')
    @component('ticket.components.ticket-intro')
        @if(!$ticket->resellerTicket)
            Der Fang muss unmittelbar nach der Entnahme auf der ausgedruckten Lizenz eingetragen werden.
            Bitte tragen Sie nach dem Fischtag Ihren Fang in den Fangbericht auf <b>www.hejfish.com unter „Profil >
                Meine Angelkarten > jeweilige Angelkarte auswählen > Fang
                eintragen“ </b>ein. Sind alle Fänge dort eingetragen,
            muss die Lizenz nicht händisch an den Gewässerbewirtschafter retourniert werden!
        @endif
    @endcomponent

@endsection

@section('main_ticket_right')
    @component('ticket.components.brand')
        @slot('logo'){{ route('imagecache', ['template' => 'lLogo', 'ticket/hejfish_logo.png']) }}@endslot
        @slot('code'){{ $tm->getControlCode($ticket) }}@endslot
    @endcomponent

    @include('ticket.components.purchase', compact('tm', 'ticket', 'showCashPayment'))

    @component('ticket.components.fisher-id')
        {{ $ticket->resellerTicket ? $ticket->resellerTicket->fisher_id : $ticket->user->fisher_id }}
    @endcomponent
    <div style="height: 30mm;display:block;">&nbsp;</div>
    @component('ticket.components.qrcode')
        {{ base64_encode($tm->getQrCode($ticket)) }}
    @endcomponent
@endsection

@section('catchlog')
    <div class="catchlog-page">
        <table>
            <thead>
            <tr>
                <th colspan="@yield('catchlog_table_columns', $tm->getMaxFishPerDay($ticket) + 1)">
                    <table class="page-header">
                        <tr>
                            @section('catchlog_title')
                                <th width="100%" colspan="{{ $tm->getMaxFishPerDay($ticket)}}">
                                    <span class="title">Fangbericht</span>
                                </th>
                            @show

                            @section('catchlog_qrcode')
                                <th class="qrcode-container" rowspan="3">
                                    <img class="qrcode"
                                         src="data:image/svg+xml;base64, {!! base64_encode($tm->getQrCode($ticket)) !!} ">
                                </th>
                            @show
                        </tr>
                        <tr>
                            @section('catchlog_area')
                                <th colspan="{{ $tm->getMaxFishPerDay($ticket)}}">
                                    <span class="title">{{ $ticket->type->area->name }}</span>
                                </th>
                            @show
                        </tr>
                        <tr>
                            @section('catchlog_user')
                                <th colspan="{{ ceil($tm->getMaxFishPerDay($ticket) / 2) }}">
                                    @section('catchlog_user_name')
                                        {{ $tm->getUserName($ticket) }}<br/>
                                    @show
                                    @section('catchlog_ticket_identifier')
                                        {{ $tm->getIdentifier($ticket) }}{{ isset($iteration) ? '-G'.($iteration+1) : '' }}
                                    @show
                                    @yield('catchlog_user_extra')
                                </th>
                            @show

                            @section('catchlog_legend')
                                <th class="legend" colspan="{{ floor($tm->getMaxFishPerDay($ticket) / 2) }}">

                                </th>
                            @show
                        </tr>
                    </table>
                </th>
            </tr>
            <tr>
                @section('catchlog_table_header')
                    <th>Datum</th>
                    @foreach (range(1, $tm->getMaxFishPerDay($ticket)) as $i)
                        <th>Fischart kg</th>
                    @endforeach
                @show
            </tr>
            </thead>
            <tbody>
            @section('catchlog_table_body')
                @foreach (range(1, $tm->getMaxFishingDays($ticket)) as $day)
                    <tr>
                        <td></td>
                        @foreach (range(1, $tm->getMaxFishPerDay($ticket)) as $i)
                            <td>{{ $i }}</td>
                        @endforeach
                    </tr>
                @endforeach
            @show
            </tbody>
        </table>
    </div>
@endsection