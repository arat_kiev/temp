@extends('edersee.pdf.ticket')

@section('main_ticket_watermark')
    <div id="watermark">
        {{ str_repeat("- HEJFISH - EDERSEE ", 5000) }}
    </div>
@endsection

@section('main_ticket_user')
    <div class="user">
        Ausgestellt auf:
        <div class="name">&nbsp;<br/>&nbsp;</div>
        <div class="birthday">geboren am: </div>
        Adresse:
        <ul class="address">
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
        </ul>

        <hr class="divider" />

        <div class="authorization-id">
            Fischereischein-Nummer:<br/>
            <br/><br/><br/>
            Ausstellende Behörde:<br/>
            <br/><br/><br/>
        </div>
        <hr class="divider" />
    </div>
@endsection

@section('main_ticket_center')
    <div class="ticket">
        @section('main_ticket_header')
            @component('ticket.components.header')
                angeln.naturpark-kellerwald-edersee.de
            @endcomponent
        @show

        <div class="title"><br/>für Edersee</div>

        <div class="valid">
            Gültigkeit:
            <span class="date">&nbsp;</span>
        </div>

        @include('ticket.components.ticket-borders', compact('ticket'))
    </div>
    <div class="catch">
        @section('main_ticket_catchlog')
            <table class="log">
                <thead>
                <tr>
                    <th class="time" style="width: 21%;">Angelzeit<br />(in Std.)</th>
                    <th class="type" style="width: 21%;">Methode<br />(Kennziffer)</th>
                    <th class="type" style="width: 34%;">Fischart</th>
                    <th class="size" style="width: 24%;">Länge (cm)</th>
                </tr>
                </thead>
                <tbody>
                @foreach (range(1, $ticket->type->fish_per_day) as $i)
                    <tr>
                        <td>{{ $i }}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @show

        @section('main_ticket_intro')
            @component('ticket.components.ticket-intro')
                Bei Tages- und 2-Tageskarten muss der Fang nach dem Angeltag und bei Wochen- und Jahreskarten
                muss der Fang nach Ablauf der Angelerlaubnis in die Fangstatistik auf <i><b>http://angeln.naturpark-kellerwald-edersee.de</b></i>
                wie folgt eingetragen werden: <i><b>"Jeweiliges Gewässer wählen &gt; Fischart gefangen &gt; Fang eintragen".</b></i>
            @endcomponent
        @show

        @section('main_ticket_legal')
            @component('ticket.components.ticket-legal')
                Es gilt das Hessische Fischereigesetz und die Hessische Fischereiverordnung, sowie die besonderen Bedingungen des Naturpark Kellerwald-Edersee.
            @endcomponent
        @show
    </div>
@endsection

@section('main_ticket_brand')
    @component('ticket.components.brand')
        @slot('logo'){{ route('imagecache', ['template' => 'lLogo', 'ticket/hejfish_logo.png']) }}@endslot
        @slot('code')@endslot
    @endcomponent
@endsection

@section('main_ticket_purchase')
    <div class="purchase">
        <div class="identifier">&nbsp;</div>
        <hr class="divider" />
        <br/>
        <div class="date" style="text-align: left; margin-left: 2mm;">Ausstellungsdatum: </div>
        <br/><br/><br/>
        <div class="price" style="text-align: left; margin-left: 2mm;">Preis: </div>
        <br/>
        <div class="cash-payment">bar bezahlt</div>
    </div>
@endsection

@section('main_ticket_qrcode')
@endsection

@section('main_ticket_fisher_id')
@endsection