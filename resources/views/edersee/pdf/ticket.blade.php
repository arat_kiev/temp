@extends('ticket.layout.base')

@inject ('tm', 'App\Managers\TicketManager')

@section('main_ticket_user')
    @php
        $user = $ticket->resellerTicket ?: $ticket->user;
    @endphp

    <div class="user">
        Ausgestellt auf:

        <div class="name">{{ $user->full_name }}</div>
        <div class="birthday">geboren am: {{ $user->birthday->format('d.m.Y') }}</div>
        <ul class="address">
            <li>{{ $user->street }}</li>
            <li>{{ $user->post_code }} {{ $user->city }}</li>
            <li>{{ $user->country->name }}</li>
        </ul>

        <hr class="divider"/>
        @if(isset($user->authorization_id))
            <div class="authorization-id">
                Fischereischein-Nummer:<br/>
                {{ $user->authorization_id }}
                <br/><br/>
                @if(isset($user->issuing_authority))
                    Ausstellende Behörde:<br/>
                    {{ $user->issuing_authority }}
                @endif
            </div>
            <hr class="divider"/>
        @else
            <div class="authorization-id">
                Fischereischein-Nummer:<br/>
                {{ $tm->getGermanCardId($ticket) }}
            </div>
            <hr class="divider"/>
        @endif
    </div>
@endsection

@section('main_ticket_header')
    @component('ticket.components.header')
        angeln.naturpark-kellerwald-edersee.de
    @endcomponent
@endsection

@section('main_ticket_intro')
    @component('ticket.components.ticket-intro')
        <h4>Bitte beachten Sie die Hinweise zur Führung der Fangliste.</h4>
    @endcomponent
@endsection

@section('main_ticket_legal')
    @component('ticket.components.ticket-legal')
        Es gilt das Hessische Fischereigesetz und die Hessische Fischereiverordnung, sowie die besonderen Bedingungen des Naturpark Kellerwald-Edersee.
    @endcomponent
@endsection

@section('main_ticket_catchlog')
    @if($tm->shouldDisplayCatchlog($ticket) && $ticket->type->fish_per_day > 0)
        <table class="log">
            <thead>
            <tr>
                <th class="time" style="width: 21%;">Angelzeit<br/>(in Std.)</th>
                <th class="type" style="width: 21%;">Methode<br/>(Kennziffer)</th>
                <th class="type" style="width: 34%;">Fischart</th>
                <th class="size" style="width: 24%;">Länge (cm)</th>
            </tr>
            </thead>
            <tbody>
            @foreach (range(1, $ticket->type->fish_per_day) as $i)
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection

@section('catchlog_table_columns')
    10
@endsection

@section('catchlog_user_extra')
    ({{ $tm->getValidString($ticket) }})
@endsection

@section('catchlog_table_header')
    <th style="width: 10%;">Datum</th>
    <th style="width: 8%;">Angelzeit<br/>(in Std.) *</th>
    <th style="width: 12%;">Methode<br/>(Kennziffer)</th>
    <th style="width: 12%;">Fischart</th>
    <th style="width: 8%;" class="vertical-divider">Länge (cm)</th>
    <th style="width: 10%;">Datum</th>
    <th style="width: 8%;">Angelzeit<br/>(in Std.) *</th>
    <th style="width: 12%;">Methode<br/>(Kennziffer)</th>
    <th style="width: 12%;">Fischart</th>
    <th style="width: 8%;">Länge (cm)</th>
@endsection

@section('catchlog_table_body')
    @foreach (range(1, $tm->getMaxFishingDays($ticket) * ceil($ticket->type->fish_per_day / 2)) as $day)
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="vertical-divider"></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    @endforeach
@endsection