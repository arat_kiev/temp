@inject ('tm', 'App\Managers\TicketManager')
@php
    $demo = $demo ?? false;
@endphp
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="{{ asset('css/ticket.css') }}"/>
    @stack('styles')
</head>
<body>

@if($demo)
    @include('ticket.components.demo-overlay')
@endif

@section('main_page')
    <div class="ticket-page top">
        @include('ticket.components.ticket', compact('tm', 'ticket'))
    </div>

    <div class="ticket-page bottom">
        @include('ticket.components.rules', compact('ticket'))
    </div>
@show

@yield('extra_after_ticket')

@if($ticket->type->group && $ticket->group_data)
    @include('ticket.components.group-ticket', compact('tm', 'ticket'))
@endif

@yield('extra_before_catchlog')

@unless($tm->shouldDisplayCatchlog($ticket) || $ticket->type->fish_per_day === 0 || $demo)
    @include('ticket.components.catchlog', compact('tm', 'ticket'))

    @if($ticket->type->group && $ticket->group_data)
        @include('ticket.components.group-catchlog', compact('tm', 'ticket'))
    @endif
@endunless
</body>
</html>