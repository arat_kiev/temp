<div class="borders">
    @if($ticket->type->additionalAreas->count() == 0)
        {!! $ticket->type->area->borders !!}
    @else
        Die Grenzen sind in den Bestimmungen angeführt.
    @endif
</div>