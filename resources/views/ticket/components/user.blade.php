<div class="user">
    Ausgestellt auf:

    <div class="name">{{ $user->full_name }}</div>
    <div class="birthday">geboren am: {{ $user->birthday->format('d.m.Y') }}</div>
    <ul class="address">
        <li>{{ $user->street }}</li>
        <li>{{ $user->post_code }} {{ $user->city }}</li>
        <li>{{ $user->country->name }}</li>
    </ul>

    <hr class="divider" />
    @if(isset($user->authorization_id))
        @section($name . '_authorization_id')
        <div class="authorization-id">
            Berechtigungs-Nummer:<br/>
            {{ $user->authorization_id }}
            <br/><br/>
            @if(isset($user->issuing_authority))
                Ausstellende Behörde:<br/>
                {{ $user->issuing_authority }}
            @endif
        </div>
        @show
        <hr class="divider" />
    @endif
</div>