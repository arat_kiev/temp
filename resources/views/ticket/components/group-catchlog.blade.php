@foreach($ticket->group_data as $person)
    @include('ticket.components.additional-catchlog', array_merge([
        'userName' => "{$person['first_name']} {$person['last_name']}",
        'iteration' => $loop->iteration
    ], compact('ticket', 'tm', 'person')))
@endforeach