<div class="title">
    @if(isset($iteration))
        <div class="group-number">G{{ $iteration+1 }}</div>
    @elseif($ticket->type->group)
        <div class="group-number">G1</div>
    @endif
    @if(count($ticket->type->additionalAreas))
        {{ $ticket->storno_id ? 'STORNO' : '' }} {{ $ticket->type->name }}
    @else
        {{ $ticket->storno_id ? 'STORNO' : '' }} {{ $ticket->type->name }} für
        <br/>
        {{ $ticket->type->area->name }}
    @endif
</div>