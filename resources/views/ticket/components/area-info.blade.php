<div class="area-info">
    @if(count($ticket->type->additionalAreas) > 0 && count($ticket->type->additionalAreas) <= 10)
        Gültig für folgende Gewässer:
        <ol>
            <li>{{ $ticket->type->area->name }}</li>
            @foreach($ticket->type->additionalAreas as $area)
                <li>{{ $area->name }}</li>
            @endforeach
        </ol>
        <p>Bitte Gewässer-Nummer in Fangstatistik eintragen.</p>
    @elseif (count($ticket->type->additionalAreas) > 10)
        Bitte die gültigen Gewässer für diese Angelkarte den Bestimmungen entnehmen.
    @else
        {!! $info !!}
    @endif
</div>