<div class="rksv">
    <img class="qrcode" src="data:image/svg+xml;base64, {!! base64_encode($tm->getQrCodeFromString($ticket->offisy_code)) !!} ">
    @foreach($ticket->offisy_tags as $tag)
        {{ $tag['Label'] ? "{$tag['Label']}:" : '' }}
        {{ $tag['Value'] }} -
    @endforeach
</div>