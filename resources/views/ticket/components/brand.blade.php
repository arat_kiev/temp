<div class="brand">
    <div class="container">
        <img class="logo" style="{{ $logo_style or '' }}" src="{{ $logo }}" />
        <div class="code" style="{{ $code_style or '' }}">{{ $code }}</div>
    </div>
</div>