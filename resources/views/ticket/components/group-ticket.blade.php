@foreach($ticket->group_data as $person)
    <div class="ticket-page {{ $loop->iteration % 2 ? 'top' : 'bottom' }}">
        @include('ticket.components.additional-ticket', array_merge(
            ['iteration' => $loop->iteration],
            compact('tm', 'ticket', 'person')
        ))
    </div>
@endforeach
