@php
    $name = 'add_catchlog_' . $iteration;
@endphp

@section($name)
    <div class="catchlog-page">
        <table>
            <thead>
            <tr>
                <th colspan="@yield('catchlog_table_columns', $tm->getMaxFishPerDay($ticket) + 1)">
                    <table class="page-header">
                        <tr>
                            @yield('catchlog_title')

                            @yield('catchlog_qrcode')
                        </tr>
                        <tr>
                            @yield('catchlog_area')
                        </tr>
                        <tr>
                            <th colspan="{{ ceil($tm->getMaxFishPerDay($ticket) / 2) }}">
                                {{ $person['first_name'] }} {{ $person['last_name'] }}<br/>
                                {{ $tm->getIdentifier($ticket) }}{{ isset($iteration) ? '-G'.($iteration+1) : '' }}
                                @yield('catchlog_user_extra')
                            </th>

                            @yield('catchlog_legend')
                        </tr>
                    </table>
                </th>
            </tr>
            <tr>
                @yield('catchlog_table_header')
            </tr>
            </thead>
            <tbody>
                @yield('catchlog_table_body')
            </tbody>
        </table>
    </div>
@show