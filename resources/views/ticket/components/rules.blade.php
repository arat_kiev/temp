@php
    $stateRules = $ticket->type->area->states->filter(function ($state) {
        return $state->rule && $state->rule->text;
    })->map(function ($state) {
        return $state->rule->text;
    });
@endphp

@section('ticket_bottom_page')
    <div class="rules">
        @section('ticket_rules')
            @component('ticket.components.rules-text')
                @if ($ticket->type->ruleWithFallback)
                    {!! $ticket->type->ruleWithFallback->text !!}
                @endif
                @slot('stateRules')
                    {!! $stateRules->implode('<br>') !!}
                @endslot
            @endcomponent
        @show

        @section('ticket_agent')
            @component('ticket.components.agent-info')
                Vermittler: Fishing & Outdoor Apps GmbH -
                Hopfengasse 3 / 5. Stock - A-4020 Linz -
                UID: ATU67771379 - info@hejfish.com <br /><strong>Unsere Datenschutzbestimmungen: www.hejfish.com/datenschutz</strong>
            @endcomponent
        @show
    </div>
@show