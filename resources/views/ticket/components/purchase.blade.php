<div class="purchase">
    <div class="identifier">{{ $tm->getIdentifier($ticket) }}</div>
    <hr class="divider" />
    <div class="date">Datum: {{ $ticket->created_at->format('d.m.Y - H:i') }}</div>
    <div class="price">Preis: {{ $tm->getTicketPrice($ticket) }} €</div>
    @if($ticket->resellerTicket && empty($hideCashPayment))
        <div class="cash-payment">bar bezahlt</div>
    @endif
    @if($ticket->type->area->manager->uid && $ticket->price->show_vat)
        <div class="vat">
            MwSt ({{ $tm->getVatPercent($ticket) }}%):
            {{ $tm->getVatAmount($ticket) }} €
        </div>
    @endif
    <div class="type">Typ: {{ $ticket->price->name }}</div>
    <div class="status">{{ $ticket->storno_id ? 'STORNO' : '' }}</div>
</div>