@php
    $name = 'add_ticket_' . $iteration;
@endphp

@section($name . '_watermark')
    @component('ticket.components.watermark')
        @slot('user_name'){{ $person['first_name'] }} {{ $person['last_name'] }}@endslot
        @slot('valid_from'){{ $ticket->valid_from }}@endslot
        @slot('identifier'){{ $tm->getIdentifier($ticket) }}@endslot
    @endcomponent
@show

<div class="content left">
    @section($name . '_left')

        @yield('main_ticket_manager')

        @section($name . '_user')
            <div class="user">
                Ausgestellt auf:

                <div class="name">{{ $person['first_name'] }} {{ $person['last_name'] }}</div>
                <div class="birthday">geboren am: {{ Carbon\Carbon::parse($person['birthday']['date'])->format('d.m.Y') }}</div>
                <hr class="divider" />
            </div>
        @show

        @yield('main_ticket_area_info')

    @show
</div>
<div class="content center">
    @section($name . '_center')
        <div class="ticket">
            @yield('main_ticket_header')

            @include('ticket.components.ticket-name', compact('ticket', 'iteration'))

            @include('ticket.components.ticket-valid', compact('tm', 'ticket'))

            @include('ticket.components.ticket-borders', compact('ticket'))
        </div>
        <div class="catch">
            @if($tm->shouldDisplayCatchlog($ticket))
                @yield('main_ticket_catchlog')
            @endif

            @yield('main_ticket_intro')

            @yield('main_ticket_legal')
        </div>
    @show
</div>
<div class="content right">
    @section($name . '_right')

        @yield('main_ticket_brand')

        @yield('main_ticket_purchase')

        @yield('main_ticket_qrcode')

    @show
</div>