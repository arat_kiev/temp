<table class="log">
    <thead>
    <tr>
        <th class="time">Uhrzeit</th>
        @if(count($ticket->type->additionalAreas) > 0)
            <th>Gewässer-Nr.</th>
        @endif
        <th class="type">Fischart</th>
        <th class="size">cm/kg</th>
        <th class="check">Kontrolle</th>
    </tr>
    </thead>
    <tbody>
    @foreach (range(1, $ticket->type->fish_per_day) as $i)
        <tr>
            <td></td>
            @if(count($ticket->type->additionalAreas) > 0)
                <td></td>
            @endif
            <td>{{ $i }}</td>
            <td></td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>