@php
    $name = $name ?? 'main_ticket';
@endphp

@section($name . '_watermark')
    @component('ticket.components.watermark')
        @slot('user_name'){{ $tm->getUserName($ticket) }}@endslot
        @slot('valid_from'){{ $ticket->valid_from }}@endslot
        @slot('identifier'){{ $tm->getIdentifier($ticket) }}@endslot
    @endcomponent
@show

<div class="content left">
    @section($name . '_left')

        @section($name . '_manager')
            @include('ticket.components.manager', ['manager' => $ticket->type->area->manager])
        @show

        @section($name . '_user')
            @include('ticket.components.user', ['user' => $ticket->resellerTicket ?: $ticket->user])
        @show

        @section($name . '_area_info')
            @include('ticket.components.area-info', ['info' => $ticket->type->area->ticket_info])
        @show

    @show
</div>
<div class="content center">
    @section($name . '_center')
        <div class="ticket">
            @section($name . '_header')
                @component('ticket.components.header')
                    www.hejfish.com
                @endcomponent
            @show

            @include('ticket.components.ticket-name', compact('ticket'))

            @include('ticket.components.ticket-valid', compact('tm', 'ticket'))

            @include('ticket.components.ticket-borders', compact('ticket'))
        </div>
        <div class="catch">
            @section($name . '_catchlog')
                @if($tm->shouldDisplayCatchlog($ticket) && $ticket->type->fish_per_day > 0)
                    @include('ticket.components.mini-catchlog', compact('ticket'))
                @endif
            @show

            @section($name . '_intro')
                @component('ticket.components.ticket-intro')
                    Der Fang muss unmittelbar nach der Entnahme auf der ausgedruckten Lizenz eingetragen werden!
                    Spätestens nach dem Fischtag muss der Fang in die Fangstatistik auf <i><b>www.hejfish.com</b></i>
                    unter <i><b>"Profil &gt; Meine Angelkarten &gt; jeweilige Angelkarte auswählen &gt; Fang eintragen"</b></i>
                    eingetragen werden, somit muss die Lizenz nicht händisch an den Gewässerbewirtschafter retourniert
                    werden!
                @endcomponent
            @show

            @section($name . '_legal')
                @component('ticket.components.ticket-legal')
                    Des Weiteren gelten die fischereigesetzlichen Bestimmungen des jeweiligen Bundeslandes.
                @endcomponent
            @show
        </div>
    @show
</div>
<div class="content right">
    @section($name . '_right')
        @section($name . '_brand')
            @component('ticket.components.brand')
                @slot('logo'){{ route('imagecache', ['template' => 'lLogo', 'ticket/hejfish_logo.png']) }}@endslot
                @slot('code'){{ $tm->getControlCode($ticket) }}@endslot
            @endcomponent
        @show

        @section($name . '_purchase')
            @include('ticket.components.purchase', compact('tm', 'ticket'))
        @show

        @section($name . '_qrcode')
            @component('ticket.components.qrcode')
                {{ base64_encode($tm->getQrCode($ticket)) }}
            @endcomponent
        @show

        @section($name . '_fisher_id')
            @component('ticket.components.fisher-id')
                {{ $ticket->resellerTicket ? $ticket->resellerTicket->fisher_id : $ticket->user->fisher_id }}
            @endcomponent
        @show

        @if($ticket->offisy_code)
            @include('ticket.components.offisy', compact('ticket'))
        @endif

        @if($ticket->type->area->manager->signature)
            @include('ticket.components.manager-signature', ['signature' => $ticket->type->area->manager->signature])
        @endif
    @show
</div>