@extends('layout.responsive')

@section('page_title')
    Passwort zurücksetzen
@endsection

@section('page_content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                <img class="img img-responsive" src="{{ route('imagecache', ['template' => 'lLogo', 'ticket/hejfish_logo.png']) }}" />
                <div class="well well-password">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {!! Form::open(['route' => ['v1.auth.password_save', $token]]) !!}
                        <div class="form-group">
                            <label for="password">Passwort</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="********">
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Passwort bestätigen</label>
                            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="********">
                        </div>
                        <button class="btn btn-hej btn-block" type="submit">Passwort setzen</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection