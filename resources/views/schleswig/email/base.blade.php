@section('body')
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>@yield('subject')</title>
    <style type="text/css">

        #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        body, table, td, p, a, li, blockquote {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        body {
            margin: 0;
            padding: 0;
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body, #bodyTable, #bodyCell {
            height: 100% !important;
            margin: 0;
            padding: 0;
            width: 100% !important;
        }

        #bodyCell {
            padding: 20px;
        }

        #templateContainer {
            width: 600px;
        }

        body, #bodyTable {
            background-color: #FFF;
        }

        h1 {
            color: #202020 !important;
            display: block;
            font-family: Helvetica;
            font-size: 26px;
            font-style: normal;
            font-weight: bold;
            line-height: 100%;
            letter-spacing: normal;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            text-align: left;
        }

        h2 {
            color: #404040 !important;
            display: block;
            font-family: Helvetica;
            font-size: 20px;
            font-style: normal;
            font-weight: bold;
            line-height: 100%;
            letter-spacing: normal;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            text-align: left;
        }

        h3 {
            color: #606060 !important;
            display: block;
            font-family: Helvetica;
            font-size: 16px;
            font-style: italic;
            font-weight: normal;
            line-height: 100%;
            letter-spacing: normal;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            text-align: left;
        }

        h4 {
            color: #808080 !important;
            display: block;
            font-family: Helvetica;
            font-size: 14px;
            font-style: italic;
            font-weight: normal;
            line-height: 100%;
            letter-spacing: normal;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 10px;
            margin-left: 0;
            text-align: left;
        }

        #templatePreheader {
            background-color: #FFF;
        }

        .preheaderContent {
            color: #808080;
            font-family: Helvetica;
            font-size: 14px;
            line-height: 125%;
            text-align: left;
        }

        .preheaderContent a:link, .preheaderContent a:visited {
            color: #606060;
            font-weight: normal;
            text-decoration: underline;
        }

        #templateHeader {
            background-color: #FFF;

        }

        .headerContent {
            color: #505050;
            font-family: Helvetica;
            font-size: 20px;
            font-weight: bold;
            line-height: 100%;
            padding-top: 0;
            padding-right: 0;
            padding-bottom: 0;
            padding-left: 0;
            text-align: left;
            vertical-align: middle;
        }

        .headerContent a:link, .headerContent a:visited {
            color: #EB4102;
            font-weight: normal;
            text-decoration: underline;
        }

        #headerImage {
            height: auto;
            max-width: 600px;
        }

        #templateBody {
            background-color: #FFF;
        }

        .bodyContent {
            color: #505050;
            font-family: Helvetica;
            font-size: 14px;
            line-height: 150%;
            padding-top: 20px;
            padding-right: 20px;
            padding-bottom: 20px;
            padding-left: 20px;
            text-align: left;
        }

        .bodyContent a:link, .bodyContent a:visited {
            color: #EB4102;
            font-weight: normal;
            text-decoration: underline;
        }

        .bodyContent img {
            display: inline;
            height: auto;
            max-width: 560px;
        }

        #templateFooter {
            background-color: #FFF;
            border-top: 1px solid #f1f1f1;
        }

        .footerContent {
            color: #808080;
            font-family: Helvetica;
            font-size: 10px;
            line-height: 150%;
            padding-top: 20px;
            padding-right: 20px;
            padding-bottom: 20px;
            padding-left: 20px;
            text-align: left;
        }

        .footerContent a:link, .footerContent a:visited {
            color: #606060;
            font-weight: normal;
            text-decoration: underline;
        }

        .copy-address {
            font-size: 12px;
            color: #606060;
            background-color: #FFF;
            margin: 5px;
            padding: 10px;
        }

        .system-link {
            display: block;
            margin: 15px;
        }

        @media only screen and (max-width: 480px) {

            body, table, td, p, a, li, blockquote {
                -webkit-text-size-adjust: none !important;
            }

            body {
                width: 100% !important;
                min-width: 100% !important;
            }

            #bodyCell {
                padding: 10px !important;
            }

            #templateContainer {
                max-width: 600px !important;
                width: 100% !important;
            }

            h1 {
                font-size: 24px !important;
                line-height: 100% !important;
            }

            h2 {
                font-size: 20px !important;
                line-height: 100% !important;
            }

            h3 {
                font-size: 18px !important;
                line-height: 100% !important;
            }

            h4 {
                font-size: 16px !important;
                line-height: 100% !important;
            }

            #templatePreheader {
                display: none !important;
            }

            #headerImage {
                height: auto !important;
                max-width: 600px !important;
                width: 100% !important;
            }

            .headerContent {
                font-size: 20px !important;
                line-height: 125% !important;
            }

            #bodyImage {
                height: auto !important;
                max-width: 560px !important;
                width: 100% !important;
            }

            .bodyContent {
                font-size: 18px !important;
                line-height: 125% !important;
            }

            .footerContent {
                font-size: 14px !important;
                line-height: 115% !important;
            }

            .footerContent a {
                display: inline !important;
            }
        }
    </style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        <tr>
            <td align="center" valign="top" id="bodyCell">
                <table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                    <tr>
                        <td align="center" valign="top">
                            <!-- BEGIN PREHEADER // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader">
                                <tr>
                                    <td valign="top" class="preheaderContent"
                                        style="padding-top:10px; padding-right:20px; padding-bottom:10px; padding-left:20px;">
                                        @yield('subject')
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                <tr>
                                    <td valign="top" class="bodyContent">
                                        @yield('content')
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter">
                                <tr>
                                    <td valign="top" class="footerContent" style="padding-top:5px;">
                                        LSFV Schleswig-Holstein e.V.
                                        <br/>
                                        Papenkamp 52
                                        <br/>
                                        24114 Kiel
                                        <br/>
                                        Deutschland
                                    </td>
                                    <td valign="top" class="footerContent" style="padding-top:0;">
                                        <a href="https://www.hejfish.com">
                                            <img src="{{ $message->embed(base_path('resources/assets/img/ticket/hejfish_logo.png'))}}" style="width:150px;height:auto;"/>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="footerContent" style="padding-top:5px;">
                                        <strong>
                                            Bei Problemen oder Fragen, erreichen Sie uns unter:
                                        </strong>
                                        <br/>
                                        <strong>Tel.:</strong> +49 2208 901 6924 /
                                        <strong>E-Mail:</strong> <a href="mailto:info@hejfish.com">info@hejfish.com</a>
                                        <br/><br/>
                                        <em>Copyright &copy; {{ \Carbon\Carbon::now()->year }}</em><br/>
                                        Technischer Support:
                                        <br/>
                                        Fishing &amp; Outdoor Apps GmbH
                                        <br/>
                                        Hopfengasse 3 / 5.Stock
                                        <br/>
                                        4020 Linz
                                        <br/>
                                        Österreich
                                    </td>
                                    <td valign="top" class="footerContent" style="padding-top:0;">
                                        <a href="https://www.hejfish.com">
                                            <img src="{{ $message->embed(base_path('resources/assets/img/ticket/hejfish_logo.png'))}}" style="width:150px;height:auto;"/>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="footerContent" style="padding-top:0;">
                                        <a href="https://www.facebook.com/hejfish">Like uns auf Facebook</a><br/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</center>
</body>
</html>
@show