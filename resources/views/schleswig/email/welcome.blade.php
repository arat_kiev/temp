@extends('schleswig.email.base')

@section('subject'){{ trans('email.welcome.subject') }}@stop

@section('content')
    <h2>Willkommen beim Erlaubnisschein-Portal des LandesSportFischereiverbandes Schleswig-Holstein e.V.</h2>
    <br/>
    <p>
        Um Deine Registrierung abzuschließen und E-Mail-Adresse zu bestätigen,
        klicke bitte folgenden Link:<br/>
        <a href="{{ route('v1.auth.activate', ['activation_code' => $user->activation_code]) }}" class="system-link">
            Registrierung abschließen und E-Mail-Adresse bestätigen.
        </a>
    </p>
    <br/>
    <p>
        Falls der Link nicht funktioniert kannst Du auch folgende Adresse in Deinem Browser eingeben<br/>
        <div class="copy-address">{{ route('v1.auth.activate', ['activation_code' => $user->activation_code]) }}</div>
    </p>
@stop