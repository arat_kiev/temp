@extends('schleswig.email.base')

@section('subject')
    {{ trans('email.product.subject', ['code' => $code]) }}
@stop

@section('content')
    <p>
        Im Anhang finden Sie die Rechnung für das bestellte Produkt.
    </p>
    <p style="color: red; font-weight: bold;">
        DIE ANGELKARTE KOMMT IN EINER EIGENEN E-MAIL!<br />Beziehungsweise kann diese auch unter Ihrem Profil direkt gedruckt werden.
    </p>
    <p style="color:red;"><strong>Zahlenschloss muss nach Einstellen des Codes zusammengedrückt werden, damit es aufgeht!</strong></p>
    <p>
        Sollten Sie noch Fragen haben, können Sie sich jederzeit an uns wenden!
    </p>
    <br/>
    <p>
        Petri Heil wünscht Ihr hejfish-Team!
    </p>
@stop
