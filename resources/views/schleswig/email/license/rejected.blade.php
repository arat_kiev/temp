@extends('schleswig.email.base')

@section('subject'){{ trans('email.license.rejected.subject') }}@stop

@section('content')
    <p>Ihre Berechtigung '{{ $license->type->name }}' wurde abgelehnt.</p>
    <br/>
    <p>Begründung: {{ $license->status_text }}</p>
@stop