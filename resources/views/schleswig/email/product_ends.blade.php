@extends('schleswig.email.base')

@section('subject')
    {{ trans('email.product.ends.subject', ['stockName' => $stockName, 'stockId' => $stockId]) }}
@stop

@section('content')
    <p>
        Beachten Sie, dass das Produkt "{{ $productName }}" ({{ $productId }})
        endet auf das Lager "{{ $stockName }}" ({{ $stockId }}).
    </p>
    <br/>
    <p>
        Sollten Sie noch Fragen haben, können Sie sich jederzeit an uns wenden!
    </p>
    <br/>
    <p>
        Petri Heil wünscht Ihr hejfish-Team!
    </p>
@stop