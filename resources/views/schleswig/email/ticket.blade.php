@extends('schleswig.email.base')

@section('subject')
    {{ trans('email.ticket.subject') }}
@stop

@section('content')
    <p>
        Im Anhang finden Sie die erworbene Angelkarte und die dazugehörigen Bestimmungen!
    </p>
    <p style="font-weight: bold; color: red">
        ACHTUNG: DIE RECHNUNG FÜR RESERVIERTE BOOTE KOMMT IN EINER EIGENEN E-MAIL.<br/> Beziehungsweise kann diese auch unter ihrem Profile direkt heruntergeladen werden.
    </p>

    <p style="font-weight: bold;">
        Die gedruckte Angelkarte, die gedruckten Bestimmungen und die gültigen Fischereidokumente
        des jeweiligen Bundeslandes sind stets beim Angeln mitzuführen.
        Bei Verstößen gegen diese Bestimmungen können gesetzlich festgelegte Verwaltungsstrafen zur Anwendung kommen.
    </p>
    <p>
        Die Fänge können nach Eintragung auf der gedruckten Angelkarte sofort online abgegeben werden.
        Dazu die gekaufte Angelkarte im persönlichen Profil unter "Meine Angelkarten" auswählen und
        auf "Fang eintragen" klicken.
    </p>
    <p>
        Wir weisen darauf hin, dass Angelkarten nicht storniert werden können und
        der Käufer bei einem Angelkartenkauf
        in Deutschland gem. § 312 b Abs. 3 Nr. 6 BGB nach § 312b BGB kein Rücktrittsrecht hat.
        Weitere Informationen finden Sie in unseren AGBs.
    </p>
    <p>
        Sollten Sie noch Fragen haben, können Sie sich jederzeit an uns wenden!
    </p>
    <br/>
    <p>
        Petri Heil wünscht Ihr hejfish-Team!
    </p>
@stop