@inject ('invoiceManager', 'App\Managers\InvoiceManager')
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="{{ asset('css/invoice.css') }}"/>
</head>
<body>
<div class="page">
    <div class="manager">
        <img class="logo" src="{{ route('imagecache', [
            'template' => 'lLogo', 'ticket/hejfish_logo.png'
        ]) }}"/>
    </div>
    <div class="customer">
        <span class="back_adress">Fishing & Outdoor Apps GmbH, Hopfengasse 3, A-4020 Linz</span><br/><br/>
        {{ $invoice->manager->name }} <br>
        {{ $invoice->manager->street }} <br>
        {{ $invoice->manager->area_code }}
        {{ $invoice->manager->city }} <br>
        {{ $invoice->manager->country->name }} <br>
        <br>
    </div>
    <div class="general-data">
        <table>
            <tr>
                <td>Rechnung-Nr.:</td>
                <td class="bold">{{ $invoiceManager->getInvoiceNumber($invoice) }} </td>
            </tr>
            <tr>
                <td>Rechnungsdatum:</td>
                <td class="bold">{{ isset($invoice->created_at) ? $invoice->created_at->format('d.m.Y') : '' }}</td>
            </tr>
            <tr>
                <td>Abrechnungszeitraum:</td>
                <td class="bold"> {{ $invoice->date_from->format('d.m.Y') }}
                    bis {{ $invoice->date_till->format('d.m.Y') }}</td>
            </tr>
            <tr>
                <td>Erstellt von:</td>
                <td class="bold"> {{ isset($invoice->createdBy) ? $invoice->createdBy->first_name : '' }}
                    {{ isset($invoice->createdBy) ? $invoice->createdBy->last_name : '' }}</td>
            </tr>
            @unless(empty($invoice->manager->uid))
                <tr>
                    <td>Ihre UID:</td>
                    <td class="bold"> {{ $invoice->manager->uid }}</td>
                </tr>
            @endunless
        </table>
    </div>
    <div class="invoice-body">
        Sehr geehrte Damen und Herren,<br/><br/>
        <p>nachfolgend finden Sie unsere geleistete Vermittlungsleistung:</p>
        @if ($invoice->manager->commission_included)
            <table>
                <tr>
                    <th>Bezeichnung</th>
                    <th class="right">Menge</th>
                    @unless ($invoice->online)
                        <th class="right">Netto</th>
                        <th class="right">USt.</th>
                    @endunless
                    <th class="right">Brutto</th>
                </tr>
                <tr>
                    @if ($invoice->online)
                        <td>Online-Angelkarten</td>
                    @else
                        <td>Provision POS-Angelkarten</td>
                    @endif
                    <td class="right">{{ $invoice->hfTickets->count() }}</td>
                    @unless ($invoice->online)
                        <td class="right">
                            {{ number_format($invoiceManager->getCommissionNet($invoice) / 100, 2, ',', '.') . ' €' }}
                        </td>
                        <td class="right">
                            {{ number_format($invoiceManager->getCommissionVatAmount($invoice) / 100, 2, ',', '.') . ' €' }}
                        </td>
                    @endunless
                    @if ($invoice->online)
                        <td class="right">
                            {{ number_format($invoiceManager->getTotalPrice($invoice, $invoice->manager->commission_included) / 100, 2, ',', '.') . ' €' }}
                        </td>
                    @else
                        <td class="right">
                            {{ number_format($invoiceManager->getCommissionGross($invoice) / 100, 2, ',', '.') . ' €' }}
                        </td>
                    @endif
                </tr>
            </table>
        @else
        <table>
            <tr>
                <th>Bezeichnung</th>
                <th class="right">Menge</th>
                <th class="right">Gesamt</th>
            </tr>
            <tr>
                @if ($invoice->online)
                    <td>Online-Angelkarten</td>
                @else
                    <td>POS-Angelkarten</td>
                @endif
                <td class="right">{{ $invoice->hfTickets->count() }}</td>
                <td class="right">
                    {{ number_format($invoiceManager->getTotalPrice($invoice, $invoice->manager->commission_included) / 100, 2, ',', '.') . ' €' }}
                </td>
            </tr>
        </table>
        @endif

        <p class="legal-text">
            Wie vertraglich vereinbart ist im vorgenannten Betrag die von Ihnen gegebenenfalls abzuführende
            Umsatzsteuer
            enthalten. Der korrekte Ausweis und die Abführung der von Ihnen geschuldeten Steuern und Abgaben obliegen
            nicht
            der Fishing & Outdoor Apps GmbH, sondern allein Ihnen. Sie können unter admin.hejfish.com jederzeit die
            Verkaufszahlen einsehen. Die Fishing & Outdoor Apps GmbH übernimmt dafür keine Haftung.
        </p>

        @unless($invoice->manager->commission_included)
            <div id="provision-table">

                <p>Wie vereinbart erlauben wir uns folgenden Betrag in Rechnung zu stellen:</p>
                <table>
                    <tr>
                        <th>Pos.</th>
                        <th>Bezeichnung</th>
                        <th class="right">Provision netto</th>
                        <th class="right">USt.(%)</th>
                        <th class="right">USt.</th>
                        <th class="right">Provision brutto</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Provision Angelkartenkauf</td>
                        <td class="right">{{ number_format($invoiceManager->getCommissionNet($invoice) / 100, 2, ',', '.') . ' €' }} </td>
                        <td class="right">{{ $invoice->manager->country->vat }} %</td>
                        <td class="right">{{ number_format($invoiceManager->getCommissionVatAmount($invoice) / 100, 2, ',', '.') . ' €' }}</td>
                        <td class="right">{{ number_format($invoiceManager->getCommissionGross($invoice) / 100, 2, ',', '.') . ' €' }}</td>
                    </tr>
                    @if ($invoice->online)
                        <tr class="table-sum">
                            <td class="right bold" colspan="5">Auszahlungsbetrag:</td>
                            <td class="right bold">{{ number_format(($invoiceManager->getTotalPrice($invoice) - $invoiceManager->getCommissionGross($invoice)) / 100, 2, ',', '.') . ' €' }}</td>
                        </tr>
                    @endif
                </table>
            </div>
        @endunless

        <p class="info">
            Der Auszahlungsbetrag wird gem. des vertraglich vereinbarten Zahlungszieles auf Ihr Konto überwiesen.
          <br/><br/>
            Für Fragen stehen wir Ihnen gerne von:<br/>
            Montag bis Freitag von 09:00 - 18:00 Uhr <br/>
            unter der Rufnummer +43 676 849 509 100 oder per E-Mail an info@hejfish.com zur Verfügung.
        </p>
        <p class="info">
            Dies ist eine automatisch generierte Rechnung und bedarf keiner Unterschrift.
        </p>
    </div>
    <div class="footer">
        <table>
            <tr>
                <td>
                    Sparkasse Mühlviertel West
                    <br/>
                    Landesgericht Linz
                </td>
                <td>AT94 2033 4000 0124 3419
                    <br/>
                    FN 392658p
                </td>
                <td>BIC: SMWRAT21XXX<br/>
                    UID: ATU 67771379
                </td>
                <td>+43 677 6127 5350<br/>
                    info@hejfish.com<br/>
                    hejfish.com<br/>
                    <span class="bold">KOMM ANS WASSER</span><br/>
                    Fishing & Outdoor Apps GmbH<br/>
                    Hopfengasse 3<br/>
                    4020 Linz, Austria
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>
