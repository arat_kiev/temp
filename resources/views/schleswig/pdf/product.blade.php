@inject ('productManager', 'App\Managers\ProductSaleManager')
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="{{ asset('css/product.css') }}" />
    <script>window.status = 'loading';</script>
</head>
<body>
@if (isset($demo) && $demo === true)
    <div id="demo-overlay"></div>
@endif
<div class="user">
    <div class="sender">Fishing & Outdoor Apps GmbH, Hopfengasse 3, A-4020 Linz</div>
    <div class="name">{{ $sale->first_name }} {{ $sale->last_name }}</div>
    <ul class="address">
        <li>{{ $sale->street }}</li>
        <li>{{ $sale->post_code }} {{ $sale->city }}</li>
        <li>{{ $sale->country->name }}</li>
    </ul>
</div>

<div class="manager">
    <img class="logo" src="{{ route('imagecache', [
        'template' => 'lLogo', 'ticket/hejfish_logo.png'
    ]) }}" />
    <div class="name">Fishing & Outdoor Apps GmbH</div>
    <ul class="address">
        <li>Hopfengasse 3</li>
        <li>4020 Linz</li>
        <li>Österreich</li>
    </ul>
    <div class="link">www.hejfish.com</div>
</div>

<div class="purchase">
    <div class="identifier">Rechnungs-Nr.: {{ $productManager->getIdentifier($sale) }}</div>
    <div class="date">Gekauft am: {{ $sale->created_at->format('d.m.Y - H:i') }}</div>
</div>
<div class="product">
    <h3>Vielen Dank für Deinen Einkauf!</h3>
    <span>Hiermit stellen wir Dir wie folgt in Rechnung:</span>
    <div class="title">
        <p>{{ $sale->quantity }}x {{ $sale->product->name }} (Preis: {{ $sale->price->value }} €)</p>
        @if ($sale->product->hidden_info)
            <div class="hidden-info">
                <span class="hidden-text">{{$sale->product->hidden_info}}</span>
                <p style="color:red;"><strong>Zahlenschloss muss nach Einstellen des Codes zusammengedrückt werden, damit es aufgeht!</strong></p>
                <p>
                    <strong>Achtung:</strong> Die Zugangscodes können sich ändern!
                    Bitte prüfe den Code tagesaktuell unter <strong>"Meine Boote"</strong>
                    im persönlichen Profil unter <u>erlaubnisschein.lsfv-sh.de</u>.
                </p>
            </div>
        @endif
    </div>
    @if ($sale->timeslotDates)
        <h3>Reserviert am:</h3>
        @foreach ($sale->timeslotDates as $timeslot)
            <p>
                {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeslot->date)->format('d.m.Y') }}&nbsp;, gültig 1 Stunde vor Sonnenaufgang bis 1 Stunde nach Sonnenuntergang
            </p>
        @endforeach
    @endif
    <p>
        <strong>Achtung: Jugendliche müssen am Boot stets Schwimmwesten tragen!</strong>
    </p>
    <div class="price bold">Gesamtbetrag:  {{ $productManager->getTotalAmount($sale) }} € (inkl. Versand- und Bezahlkosten)</div>
    @foreach($sale->fees as $fee)
        <div>{{ $fee->name }}: {{ $fee->value_with_mark }}</div>
    @endforeach
    <div class="vat">
        inklusive 7% MwSt. ({{ $productManager->getVatAmountFormatted($sale, 7) }} €) für LSFV-Mitglieder und 19% MwSt. ({{ $productManager->getVatAmountFormatted($sale, 19) }} €) für Nicht-LSFV-Mitglieder.
    </div>
    <p>Bezahlt mit: Online-Bezahldienst</p>

    <p>Mit freundlichen Grüßen,</p>
    <p>&nbsp;</p>
    <p>Dein hejfish-Team</p>
    <p></p>
</div>

<script>
    var img = document.querySelector('.manager > .logo');

    if (img.complete) {
        window.status = 'loaded';
    } else {
        img.addEventListener('load', function() {
            window.status = 'loaded';
        });
    }

    setTimeout(function() {
        window.status = 'loaded';
    }, 5000);
</script>
</body>
</html>