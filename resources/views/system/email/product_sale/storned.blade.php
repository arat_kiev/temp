@extends('bissanzeiger.email.base')

@section('content')
    <p>Abfrage um Produkte Verkäufe #{{ $sale->id }} zu stornieren.</p>
    <h3>Produkte</h3>
    <ul>
        <li>ID: {{ $sale->id }}</li>
        <li>User:
            @if($sale->user)
                {{ $sale->user->first_name . ' ' . $sale->user->last_name }} (#{{ $sale->user->id }})
            @else
                {{ "$sale->first_name $sale->last_name ($sale->email)" }}
            @endif
        </li>
        <li>Verkäufer:
            @if($sale->user)
                online
            @else
                {{ str_limit($sale->reseller->name, 30) }}
            @endif
        </li>
        <li>Preis: {{ $sale->price->value . ' €)' }}</li>
        <li>Storno Beschreibung: {{ $reason }}</li>
    </ul>
@endsection
