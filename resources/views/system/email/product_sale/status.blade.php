@extends('bissanzeiger.email.base')

@section('content')
    <h3>Neuer Status:</h3>
    <p>
        @if($status === 'RECEIVED')
            Wir haben Deine Bestellung erhalten und werden sie umgehend bearbeiten.
        @elseif($status === 'SHIPPING')
            Deine Bestellung wird versandt.
        @elseif($status === 'SHIPPED')
            Deine Bestellung wurde versandt.
        @endif
    </p>
@endsection
