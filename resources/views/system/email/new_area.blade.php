@extends('bissanzeiger.email.base')

@section('content')
    <h3>Neues Gewässer erstellt:</h3>
    <ul>
        <li>ID: {{ $area->id }}</li>
        <li>Name: {{ $area->name }}</li>
        <li>Bewirtschafter: {{ $area->manager->name }}</li>
    </ul>
    <br/>
    Von User: {{ $user->name }} (#{{ $user->id }})
@endsection