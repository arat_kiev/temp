@extends('bissanzeiger.email.base')

@section('subject'){{ trans('email.license.pending.subject') }}@stop

@section('content')
    <p>Neue Berechtigung wurde hochgeladen.</p>
    <h3>User</h3>
    <ul>
        <li>ID: {{ $license->id }}</li>
        <li>Typ: {{ $license->type->name }}</li>
        <li>User: {{ $license->user->first_name.' '.$license->user->last_name }} (#{{ $license->user->id }})</li>
        <li>Geburtstag: {{ $license->user->birthday->format('d.m.Y') }}</li>
    </ul>
    <h3>Custom Fields</h3>
    <ul>
        @foreach($license->fields as $field => $value)
            <li>{{ $field }}: {{ $value }}</li>
        @endforeach
    </ul>
    <a href="{{ route('admin::license.accept', ['licenseID' => $license->id]) }}">Berechtigung freischalten</a>
@stop