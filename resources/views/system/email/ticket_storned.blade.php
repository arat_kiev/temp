@extends('bissanzeiger.email.base')

@section('content')
    <p>Abfrage um Angelkarte (Produkte) #{{ $ticket->id }} zu stornieren.</p>
    <h3>Angelkarte</h3>
    <ul>
        <li>ID: {{ $ticket->id }}</li>
        <li>User:
            @if($ticket->user)
                {{ $ticket->user->first_name . ' ' . $ticket->user->last_name }} (#{{ $ticket->user->id }})
            @else
                {{ $ticket->resellerTicket->first_name . ' ' . $ticket->resellerTicket->last_name }}
            @endif
        </li>
        <li>Gewässer: {{ str_limit($ticket->type->area->name, 30) }}</li>
        <li>Verkäufer:
            @if($ticket->user)
                online
            @else
                {{ str_limit($ticket->resellerTicket->reseller->name, 30) }}
            @endif
        </li>
        <li>Typ: {{ str_limit($ticket->type->area->name, 30) }}</li>
        <li>Preis: {{ $ticket->price->name.' ('.number_format($ticket->price->value / 100.0, 2, ",", ".").' €)' }}</li>
        <li>Storno Beschreibung: {{ $reason }}</li>
    </ul>
@endsection
