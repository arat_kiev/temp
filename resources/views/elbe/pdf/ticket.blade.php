@extends('ticket.layout.base')

@inject ('tm', 'App\Managers\TicketManager')
@section('main_ticket_user')
    @php
        $user = $ticket->resellerTicket ?: $ticket->user;
    @endphp

    <div class="user">
        Ausgestellt auf:

        <div class="name">{{ $user->full_name }}</div>
        <div class="birthday">geboren am: {{ $user->birthday->format('d.m.Y') }}</div>
        <ul class="address">
            <li>{{ $user->street }}</li>
            <li>{{ $user->post_code }} {{ $user->city }}</li>
            <li>{{ $user->country->name }}</li>
        </ul>

        <hr class="divider"/>
    </div>
@endsection

@section('main_ticket_catchlog')
    <p style="font-size: 11px; text-align: justify;">
        Hiermit wird die Erlaubnis erteilt, in der Elbe mit bis zu drei
        (im Bereich der Freien und Hansestadt Hamburg (FHH) zwei)
        Handangeln mit jeweils höchstens zwei Haken von den genannten Uferbereichen
        und nicht von Wasserfahrzeugen aus die Fischerei unter Beachtung
        der nachstehenden Auflagen und Bedingungen auszuüben.
    </p>
    <p style="margin-bottom: 5mm; font-size: 11px; text-align: justify;">
        Die Benutzung einer Köderfischsenke mit einer Netzgröße bis zu 1 x 1 m
        ist vom Ufer zusätzlich erlaubt. Kinder, die noch nicht das 12. Lebensjahr
        (Nds.: Jugendliche, die noch nicht das 14. Lebensjahr) vollendet haben,
        dürfen unter Aufsicht eines volljährigen Fischereierlaubnisscheininhabers
        die Fischerei mit einer Handangel ausüben.
    </p>
@endsection

@section('main_ticket_intro')
    <div class="intro" style="margin-bottom: 5mm; font-size: 11px;">
        Der Fang muss nach der Entnahme auf dem ausgedruckten Erlaubnisschein eingetragen werden.
        Unmittelbar nach dem Angeltag muss der Fang auf <i><b>erlaubnisschein.lsfv-sh.de</b></i> nach erfolgtem Login
        unter <i><b>"Meine Angelkarten &gt; Angelkarte auswählen &gt; Fang eintragen"</b></i> eingetragen werden!
    </div>
@endsection

@section('main_ticket_legal')
    <div class="legal" style="font-size: 11px; text-align: justify;">
        Des Weiteren gelten die fischereigesetzlichen Bestimmungen
        der Bundesländer Schleswig-Holstein, Niedersachsen und Hamburg.
    </div>
@endsection

@section('ticket_bottom_page')
    <div class="rules sh">
        @php
            $stateRules = $ticket->type->area->states->filter(function ($state) {
                return $state->rule && $state->rule->text;
            })->map(function ($state) {
                return $state->rule->text;
            });
        @endphp
        @component('ticket.components.rules-text')
            @if ($ticket->type->ruleWithFallback)
                {!! $ticket->type->ruleWithFallback->text !!}
            @endif
            @slot('stateRules')
                {!! $stateRules->implode('<br>') !!}
            @endslot
        @endcomponent

        @component('ticket.components.agent-info')
            Vermittler: Fishing & Outdoor Apps GmbH -
            Hopfengasse 3 / 5. Stock - A-4020 Linz -
            UID: ATU67771379 - info@hejfish.com
        @endcomponent
    </div>
@endsection

@section('catchlog')
@endsection