<!DOCTYPE html>
<html>
<head>
    <title>QRCode-Test</title>
    <style  nonce="{{ csp_nounce() }}">
        .qrcode {
            display: inline-block;
            position: relative;
            margin: 12.5%;
        }

        .qrcode.qr-hejfish::after {
            content: "HEJFISH";
            text-align: center;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-weight: 700;
            font-size: 20px;
            position: absolute;
            top: -22px;
            left: -3px;
            width: 100%;
        }

        .qrcode.qr-rksv::after {
            content: "RKSV";
            text-align: center;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-weight: 700;
            font-size: 20px;
            position: absolute;
            top: -22px;
            left: -3px;
            width: 100%;
        }
    </style>
</head>
<body>
    <div class="qrcode qr-hejfish">
        <img src="data:image/svg+xml;base64, {!! base64_encode($qrcode_hejfish) !!}" />
    </div>
    <div class="qrcode qr-rksv">
        <img src="data:image/svg+xml;base64, {!! base64_encode($qrcode_rksv) !!}" />
    </div>
    <div class="qrcode">
        <img src="data:image/svg+xml;base64, {!! base64_encode($qrcode_svg) !!}" />
    </div>
</body>
</html>