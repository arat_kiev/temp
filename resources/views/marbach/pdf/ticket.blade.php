@extends('ticket.layout.base')

@inject ('tm', 'App\Managers\TicketManager')

@section('catchlog')
    <div class="catchlog-page">
        <table>
            <thead>
            <tr>
                <th colspan="@yield('catchlog_table_columns', $tm->getMaxFishPerDay($ticket) + 2)">
                    <table class="page-header">
                        <tr>
                            @section('catchlog_title')
                                <th width="100%" colspan="{{ $tm->getMaxFishPerDay($ticket)}}">
                                    <span class="title">Fangbericht</span>
                                </th>
                            @show

                            @section('catchlog_qrcode')
                                <th class="qrcode-container" rowspan="3">
                                    <img class="qrcode" src="data:image/svg+xml;base64, {!! base64_encode($tm->getQrCode($ticket)) !!} ">
                                </th>
                            @show
                        </tr>
                        <tr>
                            @section('catchlog_area')
                                <th colspan="{{ $tm->getMaxFishPerDay($ticket)}}">
                                    <span class="title">{{ $ticket->type->area->name }}</span>
                                </th>
                            @show
                        </tr>
                        <tr>
                            @section('catchlog_user')
                                <th colspan="{{ ceil($tm->getMaxFishPerDay($ticket) / 2) }}">
                                    @section('catchlog_user_name')
                                        {{ $tm->getUserName($ticket) }}<br/>
                                    @show
                                    @section('catchlog_ticket_identifier')
                                        {{ $tm->getIdentifier($ticket) }}{{ isset($iteration) ? '-G'.($iteration+1) : '' }}
                                    @show
                                    @yield('catchlog_user_extra')
                                </th>
                            @show

                            @section('catchlog_legend')
                                <th class="legend" colspan="{{ floor($tm->getMaxFishPerDay($ticket) / 2) }}">

                                </th>
                            @show
                        </tr>
                    </table>
                </th>
            </tr>
            <tr>
                @section('catchlog_table_header')
                    <th>Fangtag</th>
                    @foreach (range(1, $tm->getMaxFishPerDay($ticket)) as $i)
                        <th>Fischart<br />cm/kg</th>
                    @endforeach
                    <th>Kontrollen</th>
                @show
            </tr>
            </thead>
            <tbody>
            @section('catchlog_table_body')
                @foreach (range(1, $tm->getMaxFishingDays($ticket)) as $day)
                    <tr>
                        <td></td>
                        @foreach (range(1, $tm->getMaxFishPerDay($ticket)) as $i)
                            <td>{{ $i }}</td>
                        @endforeach
                        <td></td>

                    </tr>
                @endforeach
            @show
            </tbody>
        </table>
    </div>
@endsection