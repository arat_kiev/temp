<?php

namespace App\Http\Helpers;

use App\Models\Notification;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Notify
{

    private $type = null;
    private $user_id = null;
    private $subject_id = null;
    private $object_id = null;
    private $object_type = null;

    /**
     * Init via static call
     */
    private function __construct()
    {
    }

    /**
     * Setter for $type, $object_type, $object_id
     * Method name will be treated as `type` (camelCase -> camel-case)
     * Accepts Model as a single parameter or class namespace + object id
     * @param $name
     * @param array $params
     * @return object
     */
    public static function __callStatic($name, array $params)
    {
        $obj = new self();
        $obj->type = str_replace('_', '-', snake_case($name));

        if (count($params) == 1 && $params[0] instanceof Model) {
            $obj->object_type = get_class($params[0]);
            $obj->object_id = $params[0]->getKey();
        } elseif (count($params) == 2 && is_string($params[0]) && is_int($params[1])) {
            $obj->object_type = $obj->chkClass($params[0]);
            $obj->object_id = $obj->chkId($params[1]);
        }

        return $obj;
    }

    /**
     * Setter for $user_id and $subject_id
     * Accepts user's models or user's ids
     * @param $to_user
     * @param bool $from_user
     * @return Notification
     */
    public function send($to_user, $from_user = false)
    {
        $this->user_id = $this->getUserId($to_user);
        $this->subject_id = $from_user ? $this->getUserId($from_user) : null;
        return $this->save();
    }

    public function sendTo($to_user)
    {
        return $this->send($to_user);
    }

    /**
     * Save notification
     * @return Notification
     */
    private function save()
    {
        $notification = new Notification();
        $notification->create(get_object_vars($this));
        return $notification;
    }

    /**
     * Validate user's id
     * @param $user
     * @return mixed
     */
    private function getUserId($user)
    {
        return $user instanceof User ? $user->getKey() : $this->chkId($user, User::class);
    }

    /**
     * Validate id
     * @param $id
     * @return mixed
     */
    private function chkId($id, $class = null)
    {
        $model = $class && class_exists($class) ? $class : $this->object_type;
        return $model::where('id', $id)->exists() ? $id : abort(500, "'" . class_basename($model) . "' with id '$id' doesn't exists");
    }

    /**
     * Check class existence
     * @param $class
     */
    private function chkClass($class)
    {
        return class_exists($class) ? $class : abort(500, "Class '$class' doesn't exists");
    }
}
