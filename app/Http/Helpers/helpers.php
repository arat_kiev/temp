<?php

/**
 * @param int $str_chrs
 * @param int $chunk_size
 * @param string $order = alternate | shuffle
 * @param string $glue
 * @return string
 */
function str_random_formatted($str_chrs = 9, $chunk_size = 3, $order = 'shuffle', $glue = '-')
{
    // remainder of division $str_chrs by $chunk_size should be zero
    if ($str_chrs % $chunk_size != 0) {
        return null;
    }

    // acceptable chars array
    $chrs = array_merge(range('A', 'N'), range('P', 'Z'), range('1', '9'));
    $chrs_length = count($chrs);

    $output_string = '';
    $chunk_chrs = '';

    while ($str_chrs + 1) {
        shuffle($chrs);
        $chr = $chrs[rand(0, $chrs_length - 1)];

        //  chunk full, merge it with main string
        if (strlen($chunk_chrs) == $chunk_size) {
            $glue = $str_chrs ? $glue : '';
            $output_string .= $chunk_chrs . $glue;
            $chunk_chrs = '';
        }

        // `alternate` order: skip numbers for odd chunks and letters for even chunks
        $chunk = ($str_chrs + strlen($chunk_chrs)) % 2 == 0 ? 'even' : 'odd';
        if ($order == 'alternate' && (($chunk == 'odd' && is_int($chr)) || ($chunk == 'even' && !is_int($chr)))) {
            continue;
        }
            $chunk_chrs .= $chr;
            $str_chrs--;
    }
    return $output_string;
}

function csp_nounce()
{
    return \Bepsvpt\SecureHeaders\SecureHeaders::nonce();
}
