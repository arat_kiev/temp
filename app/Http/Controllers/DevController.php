<?php

namespace App\Http\Controllers;

use App\Models\Area\Area;
use App\Models\User;
use DB;
use LRedis;

class DevController extends Controller
{
    public function index()
    {
        $title = 'bissanzeiger API';
        $delayedJobs = LRedis::connection('queue')->zcard('queues:default:delayed');
        $normalJobs = LRedis::connection('queue')->llen('queues:default');
        $failedJobs = DB::table('failed_jobs')->count();

        return view('welcome', compact('title', 'delayedJobs', 'normalJobs', 'failedJobs'));
    }

    public function calculateLength($weight)
    {
        $corpulence_factor = 0.19;
        $length = pow($weight * 100 / $corpulence_factor, (1/3));
        return $length;
    }
    public function calculateWeight($length)
    {
        $corpulence_factor = 0.19;
        $weight = pow($length, 3) * $corpulence_factor / 100;
        return $weight;
    }

    public function calculateCorpulenceFactor($length, $weight)
    {
        $corpulence_factor = $weight * 100 / pow($length, 3);
        return $corpulence_factor;
    }
    public function info()
    {
        phpinfo();
    }

    public function qrcode()
    {
        return view('development.qrcode')->with([]);
    }

    public function logo()
    {
        return public_path('img/ticket/hejfish_logo.png');
    }

    public function related($userId)
    {
        /** @var User $user */
        $user = User::find($userId);
        $userLicenseIds = $user->licenses()->valid()->pluck('license_type_id');

        $query = Area::query()
            ->where(function ($query) use ($userId) {
                $query->whereIn('areas.id', function ($query) use ($userId) {
                    $query->select('areas.id')
                        ->distinct()
                        ->from('areas')
                        ->join('ticket_types', 'ticket_types.area_id', '=', 'areas.id')
                        ->join('required_license_types', 'required_license_types.ticket_type_id', '=', 'ticket_types.id')
                        ->join('license_types', 'license_types.id', '=', 'required_license_types.license_type_id')
                        ->leftJoin('licenses', function ($join) use ($userId) {
                            $join->on('licenses.license_type_id', '=', 'license_types.id')
                                ->where('licenses.user_id', '=', $userId);
                        })
                        ->groupBy('ticket_types.id')
                        ->havingRaw('SUM(CASE WHEN licenses.id IS NULL THEN 1 END) IS NULL');
                })
                ->orWhereIn('areas.id', function ($query) {
                   $query->select('areas.id')
                       ->distinct()
                       ->from('areas')
                       ->join('ticket_types', 'ticket_types.area_id', '=', 'areas.id')
                       ->leftJoin('required_license_types', 'required_license_types.ticket_type_id', '=', 'ticket_types.id')
                       ->whereNull('required_license_types.license_type_id');
                });
            })
            ->whereIn('areas.id', function ($query) use ($userLicenseIds) {
                $query->select('areas.id')
                    ->distinct()
                    ->from('areas')
                    ->join('ticket_types', 'ticket_types.area_id', '=', 'areas.id')
                    ->leftJoin('optional_license_types', 'optional_license_types.ticket_type_id', '=', 'ticket_types.id')
                    ->leftJoin('license_types', 'license_types.id', '=', 'optional_license_types.license_type_id')
                    ->whereIn('license_types.id', $userLicenseIds)
                    ->orWhereNull('license_types.id');
            });

        dump($userLicenseIds);
        dump($query->toSql());
        dump($query->pluck('id')->reduce(function ($carry, $item) {
            return $carry . $item . ', ';
        }, 'Areas: '));
    }
}