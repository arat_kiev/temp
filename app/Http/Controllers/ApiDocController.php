<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Debugbar;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ApiDocController extends Controller
{
    public function __construct()
    {
        Debugbar::disable();

        $this->middleware(function ($request, $next) {
            if (!Auth::user() || !Auth::user()->hasAnyRole(['superadmin', 'apidoc_viewer'])) {
                throw new AccessDeniedHttpException();
            }

            return $next($request);
        });
    }

    public function getView()
    {
        if (!File::exists(storage_path('app/docs/index.html'))) {
            throw new \Exception('Datei "index.html" konnte nicht gefunden werden');
        }

        return response()->file(storage_path('app/docs/index.html'));
    }

    public function getPostmanTests()
    {
        if (!File::exists(storage_path('app/docs/collection.json'))) {
            throw new \Exception('Datei "collection.json" konnte nicht gefunden werden');
        }

        return response()->download(storage_path('app/docs/collection.json'));
    }
}
