<?php

namespace App\Http\Controllers;

use App\Models\Product\ProductSale;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\Invoice;

class PdfController extends Controller
{
    public function __construct()
    {
        \Debugbar::disable();
    }

    public function ticket($id)
    {
        $ticket = Ticket::withTrashed()->findOrFail($id); // withTrashed() display also softDeleted row
        $managerTemplates = $ticket->type->area->manager->templates;

        $template = $managerTemplates['pdf']['ticket'] ?? 'bissanzeiger.pdf.ticket';

        return view($template)->with(compact('ticket'));
    }

    public function product($id)
    {
        $sale = ProductSale::withTrashed()->findOrFail($id); // withTrashed() display also softDeleted row

        $managerTemplates = $sale->product->managers->count() === 1
            ? $sale->product->managers->first()->templates : null;

        $template = $managerTemplates['pdf']['product'] ?? 'bissanzeiger.pdf.product';

        return view($template)->with(compact('sale'));
    }

    public function invoice($id)
    {
        $invoice = Invoice::withTrashed()->findOrFail($id); // withTrashed() display also softDeleted row
        $managerTemplates = $invoice->manager->templates;

        $template = $managerTemplates['pdf']['invoice'] ?? 'bissanzeiger.pdf.invoice';

        return view($template)->with(compact('invoice'));
    }
}
