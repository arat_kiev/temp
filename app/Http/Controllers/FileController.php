<?php

namespace App\Http\Controllers;

use App\Models\Picture;
use Symfony\Component\HttpFoundation\File\File;

class FileController extends Controller
{
    public function __construct()
    {
        \Debugbar::disable();
    }

    public function download($name)
    {
        /** @var Picture $file */
        $file = $this->getFile($name);
        $download = new File($file->fileWithFullPath);

        if ($name = $file->name) {
            $name .= '.' . $download->getExtension();
        }

        return response()->download($download, $name);
    }

    public function stream($name)
    {
        /** @var Picture $file */
        $file = $this->getFile($name);

        return response()->file(new File($file->fileWithFullPath));
    }

    private function getFile($name)
    {
        if (ends_with($name, '.pdf')) {
            $file = explode('/', $name)[0];

            if (!ends_with($file, '.pdf')) {
                $file .= '.pdf';
            }
        } else {
            $file = $name;
        }

        return Picture::where('file', $file)->firstOrFail();
    }
}
