<?php

namespace App\Http\Controllers;

use App\Managers\TicketManager;
use App\Models\Client;
use App\Models\Location\Country;
use App\Models\Product\ProductSale;
use App\Models\Ticket\Invoice;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketPrice;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Config;
use SnappyImage;
use SnappyPDF;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class PreviewController extends Controller
{
    /** @var TicketManager */
    private $ticketManager;

    public function __construct(TicketManager $ticketManager)
    {
        $this->ticketManager = $ticketManager;

        //\Debugbar::disable();
    }

    public function ticket($ticketId, $format)
    {
        if (!Auth::user()->hasAnyRole(['manager', 'superadmin', 'ticket_inspector']) && !Auth::user()->resellers()->first()) {
            throw new AccessDeniedHttpException();
        }

        Config::set('app.client', Client::find(2));
        $ticket = Ticket::withTrashed()->findOrFail($ticketId); // withTrashed() display also softDeleted row

        $demo = false;

        $managerTemplates = $ticket->type->area->manager->templates;

        $template = $managerTemplates['pdf']['ticket'] ?? 'bissanzeiger.pdf.ticket';
        $tplData = compact('ticket', 'demo');

        switch ($format) {
            case 'html':
                return view($template)->with($tplData);

            case 'image':
                $image = SnappyImage::loadView($template, $tplData);
                $image->setOptions([
                    'crop-w' => 755,
                    'crop-h' => 1083,
                    'disable-smart-width' => true,
                ]);
                return $image->inline('preview.jpg');

            case 'pdf':
                $pdf = SnappyPDF::loadView($template, $tplData);
                $pdf->setOptions([
                    'custom-header' => [
                        'X-Api-Key' => Config::get('app.client')->api_key,
                    ],
                    'margin-left'   => 5,
                    'margin-top'    => 5,
                    'margin-right'  => 5,
                    'margin-bottom' => 5,
                ]);
                return $pdf->inline('preview.pdf');
            
            default:
                return abort(403, 'Not allowed format');
        }
    }

    public function product($productSaleId, $format)
    {
        if (!Auth::user()->hasRole('superadmin') && !Auth::user()->resellers()->first()) {
            throw new AccessDeniedHttpException();
        }

        Config::set('app.client', Client::find(2));
        $sale = ProductSale::withTrashed()->findOrFail($productSaleId); // withTrashed() display also softDeleted row

        $demo = false;

        $managerTemplates = $sale->product->managers->first()->templates ?? null;

        $template = $managerTemplates['pdf']['product'] ?? 'bissanzeiger.pdf.product';
        $tplData = compact('sale', 'demo');

        switch ($format) {
            case 'html':
                return view($template)->with($tplData);

            case 'image':
                $image = SnappyImage::loadView($template, $tplData);
                $image->setOptions([
                    'crop-w' => 755,
                    'crop-h' => 1083,
                    'disable-smart-width' => true,
                    'window-status' => 'loaded',
                ]);
                return $image->inline('preview.jpg');

            case 'pdf':
                $pdf = SnappyPDF::loadView($template, $tplData);
                $pdf->setOptions([
                    'custom-header' => [
                        'X-Api-Key' => Config::get('app.client')->api_key,
                    ],
                    'margin-left'   => 5,
                    'margin-top'    => 5,
                    'margin-right'  => 5,
                    'margin-bottom' => 5,
                    'window-status' => 'loaded',
                ]);
                return $pdf->inline('preview.pdf');

            default:
                return abort(403, 'Not allowed format');
        }
    }

    public function invoice($invoiceId, $format)
    {
        if (!Auth::user()->hasAnyRole('superadmin', 'manager')) {
            throw new AccessDeniedHttpException();
        }

        Config::set('app.client', Client::find(2));
        $invoice = Invoice::withTrashed()->findOrFail($invoiceId); // withTrashed() display also softDeleted row
        $managerTemplates = $invoice->manager->templates;

        $template = $managerTemplates['pdf']['invoice'] ?? 'bissanzeiger.pdf.invoice';
        $tplData = compact('invoice');

        switch ($format) {
            case 'html':
                return view($template)->with($tplData);

            case 'image':
                $image = SnappyImage::loadView($template, $tplData);
                $image->setOptions([
                    'crop-w' => 755,
                    'crop-h' => 1083,
                    'disable-smart-width' => true,
                ]);
                return $image->inline('preview.jpg');

            case 'pdf':
                $pdf = SnappyPDF::loadView($template, $tplData);
                $pdf->setOptions([
                    'custom-header' => [
                        'X-Api-Key'     => Config::get('app.client')->api_key,
                    ],
                    'margin-left'   => 5,
                    'margin-top'    => 5,
                    'margin-right'  => 5,
                    'margin-bottom' => 5,
                ]);
                return $pdf->inline('preview.pdf');

            default:
                return abort(403, 'Not allowed format');
        }
    }

    public function ticketDemo($ticketPriceId, $format)
    {
        Config::set('app.client', Client::find(2));
        $price = TicketPrice::findOrFail($ticketPriceId);
        $user = $this->createDemoUser();
        $ticket = $this->createDemoTicket($price, $user);

        $demo = true;

        $managerTemplates = $ticket->type->area->manager->templates;

        $template = isset($managerTemplates['pdf']['ticket']) && $managerTemplates['pdf']['ticket']
            ? $managerTemplates['pdf']['ticket']
            : 'bissanzeiger.pdf.ticket';
        $tplData = compact('ticket', 'demo');

        switch ($format) {
            case 'html':
                return view($template)->with($tplData);

            case 'image':
                $image = SnappyImage::loadView($template, $tplData);
                $image->setOptions([
                    'crop-w' => 755,
                    'crop-h' => 1083,
                    'disable-smart-width' => true,
                ]);
                return $image->inline('preview.jpg');

            case 'pdf':
                $pdf = SnappyPDF::loadView($template, $tplData);
                $pdf->setOptions([
                    'custom-header' => [
                        'X-Api-Key' => Config::get('app.client')->api_key,
                    ],
                    'margin-left'   => 5,
                    'margin-top'    => 5,
                    'margin-right'  => 5,
                    'margin-bottom' => 5,
                ]);
                return $pdf->inline('preview.pdf');

            default:
                return abort(403, 'Not allowed format');
        }
    }

    private function createDemoUser()
    {
        //County
        $country = Country::where('country_code', '=', 'at')->first();

        $user = new User([
            'name' => 'Demo-User',
            'first_name' => 'Maximilian',
            'last_name' => 'Mustermann',
            'birthday' => '1970-06-29',
            'street' => 'Musterstraße 15/a',
            'post_code' => '1234',
            'city' => 'Musterstadt',
        ]);

        $user->country()->associate($country);

        return $user;
    }

    private function createDemoTicket(TicketPrice $price, User $user)
    {
        $ticket = new Ticket([
            'id' => 999999,
            'valid_from' => $this->ticketManager->validFrom($price, Carbon::now()),
            'valid_to' => $this->ticketManager->validTo($price, Carbon::now()),
        ]);

        $ticket->setCreatedAt(Carbon::now());

        $ticket->type()->associate($price->ticketType);
        $ticket->price()->associate($price);
        $ticket->user()->associate($user);

        return $ticket;
    }
}
