<?php

namespace App\Http\Middleware;

use App\Exceptions\ApiKeyNotFoundException;
use App\Models\Client;
use Config;
use Illuminate\Http\Request;

class ExtractApiKey
{

    /**
     * The URIs that should be excluded
     *
     * @var array
     */
    protected $except = [
        '/v1/payment/response',
    ];

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     * @throws ApiKeyNotFoundException
     */
    public function handle(Request $request, \Closure $next)
    {
        if (!$this->shouldPassThrough($request)) {
            $apiKey = $request->header('X-Api-Key');

            if (!$client = Client::where('api_key', $apiKey)->first()) {
                throw new ApiKeyNotFoundException();
            }

            Config::set('app.client', $client);
        }

        return $next($request);
    }

    /**
     * Determine if the request has a URI that should pass through without processing
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function shouldPassThrough($request)
    {
        foreach ($this->except as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if ($request->is($except)) {
                return true;
            }
        }

        return false;
    }
}
