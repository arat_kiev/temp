<?php

namespace App\Http\Middleware;

use Closure;

class LogAccessMiddleware
{
    public function handle($request, Closure $next)
    {
        if (\App::environment() !== 'local') {
            abort(404);
        }

        return $next($request);
    }
}
