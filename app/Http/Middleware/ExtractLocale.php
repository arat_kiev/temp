<?php

namespace App\Http\Middleware;

use App;
use Carbon\Carbon;
use Closure;
use Config;

class ExtractLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->header('X-Locale', Config::get('app.locale'));
        App::setLocale($locale);
        Carbon::setLocale(substr($locale, 0, 2));
        setlocale(LC_ALL, [$locale.'.utf8', $locale]);

        return $next($request);
    }
}
