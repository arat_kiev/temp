<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class ManagerDocuments
{
    public function handle($request, Closure $next)
    {
        if (Auth::user()->hasRole('superadmin')) {
            return $next($request);
        }

        if ($document  = $this->unconfirmedDocument(Auth::user())) {
            return redirect()->route('admin::docs:manager.legal.show', $document);
        }

        return $next($request);
    }

    private function unconfirmedDocument($user)
    {
        if (!$user) {
            return false;
        }

        /** @var $manager \App\Models\Contact\Manager */
        if (!$manager = $user->managers()->first()) {
            return false;
        }

        if ($document = $manager->legalDocuments()->unsigned()->first()) {
            return $document;
        }

        return false;
    }
}
