<?php

namespace App\Http\Middleware;

use App\Models\Authorization\Role;
use App\Models\Contact\Manager;
use App\Models\User;
use Auth;
use Closure;
use Flash;
use Menu;

class AdminMiddleware
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if ($user && !$user->hasAnyRole(['superadmin', 'manager', 'ticket_inspector', 'organization'])) {
            if ($user->resellers()->count() === 0 && $user->rentals()->count() === 0) {
                Auth::logout();
                Flash::error('Keine Berechtigung');
                return redirect('/login');
            }
        }

        Menu::make('topMenu', function ($menu) {
            // User menu
            $user = $menu->add('');
            $user->prepend('<span class="fa fa-user fa-fw"></span>');

            // Change manager (superadmin)
            if (Auth::user()->hasRole('superadmin')) {
                $change = $user->add('Bewirtschafter/Verein wechseln')
                    ->nickname('superadmin_impersonate');
                $change->link->attr(['id' => 'show-manager-select']);
                $change->prepend('<span class="fa fa-refresh fa-fw"></span>');
            }

            // Logout
            $logout = $user->add('Abmelden', ['route' => 'admin::logout']);
            $logout->prepend('<span class="fa fa-sign-out fa-fw"></span> ');
            $logout->link->secure();
        });

        Menu::make('sideMenu', function ($menu) {
            $menu->group(['secure' => true], function ($m) {
                if (Auth::user()->hasAnyRole(['manager', 'superadmin'])) {
                    $m->add('Übersicht', ['route' => 'admin::dashboard'])
                        ->prepend('<span class="fa fa-dashboard fa-fw"></span> ');
                }

                $selManager = Auth::user()->managers()->first() ?: Manager::find(session('manager'));

                if (Auth::user()->hasAnyRole(['manager', 'superadmin']) && $selManager) {
                    $manager = $m->add('Bewirtschafter')
                        ->prepend('<span class="fa fa-briefcase fa-fw"></span> ');
                    $manager->add('Informationen', ['route' => 'admin::manager.index'])
                        ->prepend('<span class="fa fa-bookmark fa-fw"></span> ');
                    $manager->add('Meine Verkaufsstellen', ['route' => 'admin::resellers.index'])
                        ->prepend('<span class="fa fa-shopping-cart fa-fw"></span> ');
                    $manager->add('Gewässer', ['route' => 'admin::areas.index'])
                        ->prepend('<span class="fa fa-map-marker fa-fw"></span> ');

                    if ($selManager->childrenAreas()->count() > 0) {
                        $manager->add('Gewässer (zugehörig)', ['route' => 'admin::areas.affiliated'])
                            ->prepend('<span class="fa fa-map-marker fa-fw"></span> ');
                    }

                    $manager->add('Produkte', ['route' => 'admin::products.index'])
                        ->prepend('<span class="fa fa-ticket fa-fw"></span> ');
                    $manager->add('Verkaufte Produkte', ['route' => 'admin::sales.product.index'])
                        ->prepend('<span class="fa fa-cart-arrow-down fa-fw"></span> ');
                    $manager->add('Verkaufte Angelkarten', ['route' => 'admin::sales.index'])
                        ->prepend('<span class="fa fa-ticket fa-fw"></span> ');
                    /*$manager->add('Abrechnung', ['route' => 'admin::invoices.index'])
                        ->prepend('<span class="fa fa-file-text-o fa-fw"></span> ');*/
                    $manager->add('Fangstatistik', ['route' => 'admin::hauls.index'])
                        ->prepend('<span class="fa fa-bar-chart fa-fw"></span> ');
                    $manager->add('Kontrolleur', ['route' => 'admin::inspector.index'])
                        ->prepend('<span class="fa fa-street-view fa-fw"></span> ');
                    $manager->add('Sperrliste', ['route' => 'admin::blacklist.index'])
                        ->prepend('<span class="fa fa-ban fa-fw"></span> ');
                }

                if (Auth::user()->hasAnyRole(['organization', 'superadmin'])) {
                    $organization = $m->add('Verein')
                        ->prepend('<span class="fa fa-user-circle-o fa-fw"></span> ');
                    $organization->add('Informationen', ['route' => 'admin::organization.index'])
                        ->prepend('<span class="fa fa-bookmark fa-fw"></span> ');
                    $organization->add('News', ['route' => 'admin::news.index'])
                        ->prepend('<span class="fa fa-newspaper-o fa-fw"></span> ');
                    $organization->add('Mitglieder', ['route' => 'admin::members.index'])
                        ->prepend('<span class="fa fa-group fa-fw"></span> ');
                }

                $selInspector = Auth::user()->hasRole('ticket_inspector') || Auth::user()->inspectedAreas()->first()
                    ? Auth::user()->id
                    : User::find(session('inspector'));

                if (Auth::user()->hasAnyRole(['superadmin', 'ticket_inspector']) && $selInspector) {
                    $inspector = $m->add('Kontrolleur')
                        ->prepend('<span class="fa fa-street-view fa-fw"></span> ');
                    $inspector->add('Angelkarten', ['route' => 'admin::inspect:tickets.index'])
                        ->prepend('<span class="fa fa-ticket fa-fw"></span> ');
                    $inspector->add('Fangstatistik', ['route' => 'admin::inspect:hauls.index'])
                        ->prepend('<span class="fa fa-bar-chart fa-fw"></span> ');
                }

                if (Auth::user()->rentals()->count() || Auth::user()->hasRole('superadmin')) {
                    $m->add('Bootsverleih', ['route' => 'admin::rental.index'])
                        ->prepend('<span class="fa fa-ship fa-fw"></span> ');
                }

                if (Auth::user()->hasRole('superadmin')) {
                    $super = $m->add('Administration')
                        ->prepend('<span class="fa fa-rebel fa-fw"></span> ');
                    $super->add('Bewirtschafter', ['route' => 'admin::super:managers.index'])
                        ->prepend('<span class="fa fa-briefcase fa-fw"></span> ');
                    $super->add('Verkaufsstellen', ['route' => 'admin::super:resellers.index'])
                        ->prepend('<span class="fa fa-shopping-cart fa-fw"></span> ');
                    $super->add('Bootsverleihe', ['route' => 'admin::super:rentals.index'])
                        ->prepend('<span class="fa fa-ship fa-fw"></span> ');
                    $super->add('Benutzer', ['route' => 'admin::super:users.index'])
                        ->prepend('<span class="fa fa-users fa-fw"></span> ');
                    $super->add('Gewässer', ['route' => 'admin::super:areas.index'])
                        ->prepend('<span class="fa fa-map-marker fa-fw"></span> ');
                    $super->add('Berechtigungen', ['route' => 'admin::super:licenses.index'])
                        ->prepend('<span class="fa fa-id-card-o fa-fw"></span> ');
                    $super->add('Berechtigungstypen', ['route' => 'admin::super:licensetypes.index'])
                        ->prepend('<span class="fa fa-id-card fa-fw"></span> ');

                    $super->add('Promotions', ['route' => 'admin::super:promotions.index'])
                        // or "fa-bullhorn"
                        ->prepend('<span class="fa fa-bullhorn fa-fw"></span> ');
                    $super->add('Gutscheine', ['route' => 'admin::super:coupons.index'])
                        ->prepend('<span class="fa fa-ticket fa-fw"></span> ');

                    $products = $super->add('Produkte')
                        ->prepend('<span class="fa fa-tags fa-fw"></span> ');
                    $products->add('Produktübersicht', ['route' => 'admin::super:products.index'])
                        ->prepend('<span class="fa fa-money fa-fw"></span> ');
                    $products->add('Produktkategorien', ['route' => 'admin::super:product_categories.index'])
                        ->prepend('<span class="fa fa-clone fa-fw"></span> ');
                    $products->add('Gebühren', ['route' => 'admin::super:fees.index'])
                        ->prepend('<span class="fa fa-suitcase fa-fw"></span> ');
                    $products->add('Gebührenkategorien', ['route' => 'admin::super:fee_categories.index'])
                        ->prepend('<span class="fa fa-clone fa-fw"></span> ');
                    $products->add('Lager', ['route' => 'admin::super:stocks.index'])
                        ->prepend('<span class="fa fa-university fa-fw"></span> ');
                    $products->add('Lagermengen', ['route' => 'admin::super:product_stocks.index'])
                        ->prepend('<span class="fa fa-shopping-bag fa-fw"></span> ');

                    $super->add('Verkaufte Produkte', ['route' => 'admin::super:sales.product.index'])
                        ->prepend('<span class="fa fa-cart-arrow-down fa-fw"></span> ');
                    $super->add('Verkaufte Karten', ['route' => 'admin::super:tickets.index'])
                        ->prepend('<span class="fa fa-ticket fa-fw"></span> ');
                    $super->add('Abrechnungen', ['route' => 'admin::super:invoices.index'])
                        ->prepend('<span class="fa fa-file-text-o fa-fw"></span> ');
                    $super->add('Fangstatistik', ['route' => 'admin::super:hauls.index'])
                        ->prepend('<span class="fa fa-bar-chart fa-fw"></span> ');
                    $super->add('Api Doc', ['route' => 'admin::super:apidoc.index'])
                        ->prepend('<span class="fa fa-book fa-fw"></span> ');
                    $super->add('Vorschläge', ['route' => 'admin::super:draft.index'])
                        ->prepend('<span class="fa fa-commenting fa-fw"></span> ');

                    $content = $super->add('Content')
                        ->prepend('<span class="fa fa-calendar-o fa-fw"></span> ');
                    $content->add('Angelmethoden', ['route' => 'admin::super:content.fishing-methods.index'])
                        ->prepend('<span class="fa fa-dot-circle-o fa-fw"></span> ');
                    $content->add('Fische', ['route' => 'admin::super:content.fishes.index'])
                        ->prepend('<span class="fa fa-dot-circle-o fa-fw"></span> ');
                    $content->add('Techniken', ['route' => 'admin::super:content.techniques.index'])
                        ->prepend('<span class="fa fa-dot-circle-o fa-fw"></span> ');
                    $content->add('Länder', ['route' => 'admin::super:content.countries.index'])
                        ->prepend('<span class="fa fa-dot-circle-o fa-fw"></span> ');
                    $content->add('Bundesländer', ['route' => 'admin::super:content.states.index'])
                        ->prepend('<span class="fa fa-dot-circle-o fa-fw"></span> ');
                    $content->add('Bezirke', ['route' => 'admin::super:content.regions.index'])
                        ->prepend('<span class="fa fa-dot-circle-o fa-fw"></span> ');
                    $content->add('Städte', ['route' => 'admin::super:content.cities.index'])
                        ->prepend('<span class="fa fa-dot-circle-o fa-fw"></span> ');
                    $content->add('Gewässersysteme', ['route' => 'admin::super:content.parentareas.index'])
                        ->prepend('<span class="fa fa-map-marker fa-fw"></span> ');
                    $content->add('Points of Interest', ['route' => 'admin::super:content.poi.index'])
                        ->prepend('<span class="fa fa-dot-circle-o fa-fw"></span> ');
                    $content->add('Landingpages', ['route' => 'admin::super:content.lp.index'])
                        ->prepend('<span class="fa fa-map-o fa-fw"></span> ');
                    $content->add('Rechtliche Dokumente', ['route' => 'admin::super:content.legaldocs.index'])
                        ->prepend('<span class="fa fa-fw">§</span> ');

                    $reports = $m->add('Meldungen')
                        ->prepend('<span class="fa fa-warning fa-fw"></span> ');
                    $reports->add('Fänge', ['route' => 'admin::super:reports.hauls.index'])
                        ->prepend('<span class="fa fa-shopping-basket fa-fw"></span> ');
                    $reports->add('Kommentare', ['route' => 'admin::super:reports.comments.index'])
                        ->prepend('<span class="fa fa-comments fa-fw"></span> ');
                }
            });
        });

        return $next($request);
    }
}
