<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Models\Contact\Manager;

class AreaManagerMiddleware
{

    public function handle($request, Closure $next)
    {
        $apiKey = $request->header('X-Api-Key');
        $area_id = $request->route()->parameter('area_id');

        // manager has proper api-key and (if area_id was set in URL) manager has area
        $manager = $area_id
            ? Manager::where('api_key', $apiKey)->whereHas('areas', function ($query) use ($area_id) {
                $query->where('areas.id', '=', $area_id);
            })->first()
            : Manager::where('api_key', $apiKey)->first();

        if ($manager) {
            Config::set('app.manager', $manager);
            return $next($request);
        }
        throw new AccessDeniedHttpException();
    }
}
