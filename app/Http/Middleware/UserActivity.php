<?php

namespace App\Http\Middleware;

use Auth;
use Carbon\Carbon;
use Closure;

class UserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($user = Auth::user()) {
            $now = Carbon::now();

            if ($user->last_activity_at === null || $now->diffInMinutes($user->last_activity_at) >= 15) {
                $user->last_activity_at = $now;
                $user->save();
            }
        }
        
        return $next($request);
    }
}
