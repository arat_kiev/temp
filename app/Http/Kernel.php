<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Barryvdh\Cors\HandleCors::class,
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \App\Http\Middleware\ExtractLocale::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \Fideloper\Proxy\TrustProxies::class,
        \Bepsvpt\SecureHeaders\SecureHeadersMiddleware::class,
        \App\Http\Middleware\NoHttpCache::class,

        \App\Http\Middleware\ParseAuthToken::class,
        \App\Http\Middleware\UserActivity::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,

        'jwt.auth' => \Tymon\JWTAuth\Middleware\GetUserFromToken::class,
        'jwt.refresh' => \Tymon\JWTAuth\Middleware\RefreshToken::class,

        'api.key' => \App\Http\Middleware\ExtractApiKey::class,
        'local' => \App\Http\Middleware\OnlyLocalAccess::class,

        'admin' => \App\Http\Middleware\AdminMiddleware::class,
        'manager.docs' => \App\Http\Middleware\ManagerDocuments::class,
        'area.manager' => \App\Http\Middleware\AreaManagerMiddleware::class,

        'cache.response' => \App\Cache\Middleware\CacheResponse::class,
    ];
}
