<?php

namespace App\Cache;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ResponseCache
{
    /** @var ResponseCacheRepository */
    protected $cache;

    /** @var RequestHasher */
    protected $hasher;

    /** @var CacheProfile */
    protected $cacheProfile;

    public function __construct(ResponseCacheRepository $cache, RequestHasher $hasher)
    {
        $this->cache = $cache;
        $this->hasher = $hasher;
    }

    public function enabled(Request $request): bool
    {
        $routeName = $request->route()->getName();

        if (isset(config('responsecache.profiles')[$routeName])) {
            $profileClass = config('responsecache.profiles')[$routeName];
            $this->cacheProfile = new $profileClass;
            $this->hasher->setCacheProfile($this->cacheProfile);

            return $this->cacheProfile->enabled($request);
        }

        return false;
    }

    public function shouldCache(Request $request, Response $response): bool
    {
        if (!$this->cacheProfile->shouldCacheRequest($request)) {
            return false;
        }

        return $this->cacheProfile->shouldCacheResponse($response);
    }

    public function cacheResponse(Request $request, Response $response): Response
    {
        if (config('responsecache.header')) {
            $response = $this->addCachedHeader($response);
        }

        $cacheTags = $this->cacheProfile->getCacheTagsFor($response);

        $this->cache->put(
            $this->hasher->getHashFor($request),
            $response,
            $this->cacheProfile->cacheRequestUntil($request),
            $cacheTags
        );

        return $response;
    }

    protected function addCachedHeader(Response $response): Response
    {
        $clonedResponse = clone $response;

        $clonedResponse->headers->set('laravel-responsecache', 'cached on ' . Carbon::now());

        return $clonedResponse;
    }

    public function hasBeenCached(Request $request): bool
    {
        return config('responsecache.enabled')
            ? $this->cache->has($this->hasher->getHashFor($request))
            : false;
    }

    public function getCachedResponseFor(Request $request): Response
    {
        return $this->cache->get($this->hasher->getHashFor($request));
    }

    public function flush(array $tags = [])
    {
        $this->cache->flush($tags);
    }
}