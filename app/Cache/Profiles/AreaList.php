<?php

namespace App\Cache\Profiles;

use App\Cache\BaseCacheProfile;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AreaList extends BaseCacheProfile
{
    public function shouldCacheRequest(Request $request): bool
    {
        if ($request->query->has('bbox')) {
            return false;
        }

        return true;
    }

    public function shouldCacheResponse(Response $response): bool
    {
        return $response->getStatusCode() === 200;
    }

    public function cacheNameSuffix(Request $request): string
    {
        $parent = parent::cacheNameSuffix($request);

        if (auth()->check()) {
            return $parent . '/' . auth()->id();
        }

        return $parent;
    }

    public function getCacheTagsFor(Response $response): array
    {
        $areas = json_decode($response->getContent())->data;
        $tags = ['areas'];

        foreach ($areas as $area) {
            // area id
            $tags[] = 'area-' . $area->id;

            // fish ids
            foreach ($area->fish_ids as $fish_id) {
                $tags[] = 'fish-' . $fish_id;
            }

            // technique ids
            foreach ($area->technique_ids as $technique_id) {
                $tags[] = 'technique-' . $technique_id;
            }

            // manager id
            $tags[] = 'manager-' . $area->manager_id;
        }

        return array_unique($tags);
    }
}