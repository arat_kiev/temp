<?php

namespace App\Cache\Profiles;

use App\Cache\BaseCacheProfile;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AreaDetails extends BaseCacheProfile
{
    public function shouldCacheRequest(Request $request): bool
    {
        return true;
    }

    public function shouldCacheResponse(Response $response): bool
    {
        return $response->getStatusCode() === 200;
    }

    public function cacheNameSuffix(Request $request): string
    {
        $parent = parent::cacheNameSuffix($request);

        if (auth()->check()) {
            return $parent . '/' . auth()->id();
        }

        return $parent;
    }

    public function getCacheTagsFor(Response $response): array
    {
        $area = json_decode($response->getContent());
        $tags = [];

        // if logged in user, tag it with user id
        if (auth()->check()) {
            $tags[] = 'user-' . auth()->id();
        }

        // area id
        $tags[] = 'area-' . $area->id;

        // fish ids
        foreach ($area->fish_ids as $fish_id) {
            $tags[] = 'fish-' . $fish_id;
        }

        // technique ids
        foreach ($area->technique_ids as $technique_id) {
            $tags[] = 'technique-' . $technique_id;
        }

        // manager id
        $tags[] = 'manager-' . $area->manager->id;

        return $tags;
    }
}