<?php

namespace App\Cache\Profiles;

use App\Cache\BaseCacheProfile;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CountryList extends BaseCacheProfile
{
    public function shouldCacheRequest(Request $request): bool
    {
        return true;
    }

    public function shouldCacheResponse(Response $response): bool
    {
        return $response->getStatusCode() === 200;
    }

    public function getCacheTagsFor(Response $response): array
    {
        $countries = json_decode($response->getContent())->data;
        $tags = ['countries'];

        return array_merge($tags, array_map(function ($country) {
            return 'country-' . $country->id;
        }, $countries));
    }
}