<?php

namespace App\Cache\Profiles;

use App\Cache\BaseCacheProfile;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CountryDetails extends BaseCacheProfile
{
    public function shouldCacheRequest(Request $request): bool
    {
        return true;
    }

    public function shouldCacheResponse(Response $response): bool
    {
        return $response->getStatusCode() === 200;
    }

    public function getCacheTagsFor(Response $response): array
    {
        $country = json_decode($response->getContent());
        $tags = ['country-' . $country->id];

        return array_merge($tags, array_map(function ($state) {
            return 'state-' . $state->id;
        }, $country->states->data));
    }
}