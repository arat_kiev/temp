<?php

namespace App\Cache\Profiles;

use App\Cache\BaseCacheProfile;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class HaulDetails extends BaseCacheProfile
{
    public function shouldCacheRequest(Request $request): bool
    {
        return true;
    }

    public function shouldCacheResponse(Response $response): bool
    {
        return $response->getStatusCode() === 200;
    }

    public function getCacheTagsFor(Response $response): array
    {
        $haul = json_decode($response->getContent());
        return ['haul-' . $haul->id];
    }
}