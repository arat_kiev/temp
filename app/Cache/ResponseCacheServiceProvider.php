<?php

namespace App\Cache;

use App\Cache\Commands\Flush;
use App\Cache\Observers\AreaObserver;
use App\Cache\Observers\CountryObserver;
use App\Cache\Observers\FishObserver;
use App\Cache\Observers\HaulObserver;
use App\Cache\Observers\ManagerObserver;
use App\Cache\Observers\StateObserver;
use App\Cache\Observers\TechniqueObserver;
use App\Cache\Observers\TicketPriceObserver;
use App\Cache\Observers\TicketTypeObserver;
use App\Cache\Observers\UserObserver;
use App\Models\Area\Area;
use App\Models\Contact\Manager;
use App\Models\Haul;
use App\Models\Location\Country;
use App\Models\Location\State;
use App\Models\Meta\Fish;
use App\Models\Meta\Technique;
use App\Models\Ticket\TicketPrice;
use App\Models\Ticket\TicketType;
use App\Models\User;
use Illuminate\Cache\Repository;
use Illuminate\Support\ServiceProvider;

class ResponseCacheServiceProvider extends ServiceProvider
{
    const CMD_FLUSH = 'command.responsecache::flush';

    public function boot()
    {
        $this->app->when(ResponseCacheRepository::class)
            ->needs(Repository::class)
            ->give(function(): Repository {
                return $this->app['cache']->store(config('responsecache.store'));
            });

        $this->app->singleton('responsecache', ResponseCache::class);

        // Observe model events to invalidate specific response caches
        Area::observe(AreaObserver::class);
        Fish::observe(FishObserver::class);
        Manager::observe(ManagerObserver::class);
        Technique::observe(TechniqueObserver::class);
        TicketPrice::observe(TicketPriceObserver::class);
        TicketType::observe(TicketTypeObserver::class);
        Country::observe(CountryObserver::class);
        State::observe(StateObserver::class);
        Haul::observe(HaulObserver::class);
        User::observe(UserObserver::class);

        // Flush command
        $this->app[self::CMD_FLUSH] = $this->app->make(Flush::class);
        $this->commands([self::CMD_FLUSH]);
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/Config/responsecache.php', 'responsecache');
    }
}