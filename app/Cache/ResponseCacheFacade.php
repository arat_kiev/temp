<?php

namespace App\Cache;

use Illuminate\Support\Facades\Facade;

class ResponseCacheFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'responsecache';
    }
}