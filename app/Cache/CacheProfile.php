<?php

namespace App\Cache;

use DateTime;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

interface CacheProfile
{
    public function enabled(Request $request): bool;

    public function shouldCacheRequest(Request $request): bool;

    public function shouldCacheResponse(Response $response): bool;

    public function cacheRequestUntil(Request $request): DateTime;

    public function cacheNameSuffix(Request $request): string;

    public function getCacheTagsFor(Response $response): array;
}