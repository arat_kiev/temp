<?php

namespace App\Cache;

use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App;

abstract class BaseCacheProfile implements CacheProfile
{
    public function enabled(Request $request): bool
    {
        return config('responsecache.enabled');
    }

    public function cacheRequestUntil(Request $request): DateTime
    {
        return Carbon::now()->addMinutes(
            config('responsecache.default_ttl')
        );
    }

    public function cacheNameSuffix(Request $request): string
    {
        $client = config('app.client');
        $locale = config('app.locale');

        return '/' . $client->id . '/' . $locale;
    }

    public function getCacheTagsFor(Response $response): array
    {
        return [];
    }

    public function isRunningInConsole(): bool
    {
        if (app()->environment('testing')) {
            return false;
        }

        return app()->runningInConsole();
    }
}