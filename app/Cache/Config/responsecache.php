<?php

return [

    'enabled' => env('RESPONSE_CACHE_ENABLED', false),

    'header' => env('APP_DEBUG', true),

    'store' => env('RESPONSE_CACHE_STORE', 'api-cache'),

    'default_ttl' => env('RESPONSE_CACHE_TTL', 60 * 24 * 365),

    'profiles' => [

        'v1.areas.index' => App\Cache\Profiles\AreaList::class,
        'v1.areas.show' => App\Cache\Profiles\AreaDetails::class,

        'v1.countries.index' => App\Cache\Profiles\CountryList::class,
        'v1.countries.show' => App\Cache\Profiles\CountryDetails::class,

        'v1.hauls.index' => App\Cache\Profiles\HaulList::class,
        'v1.hauls.show' => App\Cache\Profiles\HaulDetails::class,
    ],

];