<?php

namespace App\Cache\Observers;

use App\Models\Haul;
use ResponseCache;

class HaulObserver
{
    public function created(Haul $haul)
    {
        ResponseCache::flush(['hauls', 'haul-' . $haul->id]);
    }

    public function deleted(Haul $haul)
    {
        ResponseCache::flush(['hauls', 'haul-' . $haul->id]);
    }

    public function updated(Haul $haul)
    {
        ResponseCache::flush(['hauls', 'haul-' . $haul->id]);
    }
}