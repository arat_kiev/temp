<?php

namespace App\Cache\Observers;

use App\Models\Location\State;
use ResponseCache;

class StateObserver
{
    public function created(State $state)
    {
        ResponseCache::flush(['state-' . $state->id]);
    }

    public function deleted(State $state)
    {
        ResponseCache::flush(['state-' . $state->id]);
    }

    public function updated(State $state)
    {
        ResponseCache::flush(['state-' . $state->id]);
    }
}