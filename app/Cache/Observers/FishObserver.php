<?php

namespace App\Cache\Observers;

use App\Models\Meta\Fish;
use ResponseCache;

class FishObserver
{
    public function deleted(Fish $fish)
    {
        ResponseCache::flush(['fish-' . $fish->id]);
    }

    public function updated(Fish $fish)
    {
        ResponseCache::flush(['fish-' . $fish->id]);
    }
}