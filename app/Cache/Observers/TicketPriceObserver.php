<?php

namespace App\Cache\Observers;

use App\Models\Ticket\TicketPrice;
use ResponseCache;

class TicketPriceObserver
{
    public function created(TicketPrice $ticketPrice)
    {
        $ticketType = $ticketPrice->ticketType;
        $area = $ticketType->area;

        ResponseCache::flush(['area-' . $area->id, 'ticket-type-' . $ticketType->id]);
    }

    public function deleted(TicketPrice $ticketPrice)
    {
        $ticketType = $ticketPrice->ticketType;
        $area = $ticketType->area;

        ResponseCache::flush([
            'area-' . $area->id,
            'ticket-type-' . $ticketType->id,
            'ticket-price-' . $ticketPrice->id,
        ]);
    }

    public function updated(TicketPrice $ticketPrice)
    {
        $ticketType = $ticketPrice->ticketType;
        $area = $ticketType->area;

        ResponseCache::flush([
            'area-' . $area->id,
            'ticket-type-' . $ticketType->id,
            'ticket-price-' . $ticketPrice->id,
        ]);
    }
}