<?php

namespace App\Cache\Observers;

use App\Models\Ticket\TicketType;
use ResponseCache;

class TicketTypeObserver
{
    public function created(TicketType $ticketType)
    {
        $area = $ticketType->area;

        ResponseCache::flush(['area-' . $area->id]);
    }

    public function deleted(TicketType $ticketType)
    {
        $area = $ticketType->area;

        ResponseCache::flush(['area-' . $area->id, 'ticket-type-' . $ticketType->id]);
    }

    public function updated(TicketType $ticketType)
    {
        $area = $ticketType->area;

        ResponseCache::flush(['area-' . $area->id, 'ticket-type-' . $ticketType->id]);
    }
}