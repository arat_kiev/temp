<?php

namespace App\Cache\Observers;

use App\Models\Meta\Technique;
use ResponseCache;

class TechniqueObserver
{
    public function deleted(Technique $technique)
    {
        ResponseCache::flush(['technique-' . $technique->id]);
    }

    public function updated(Technique $technique)
    {
        ResponseCache::flush(['technique-' . $technique->id]);
    }
}