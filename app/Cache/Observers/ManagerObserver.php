<?php

namespace App\Cache\Observers;

use App\Models\Contact\Manager;
use ResponseCache;

class ManagerObserver
{
    public function deleted(Manager $manager)
    {
        ResponseCache::flush(['manager-' . $manager->id]);
    }

    public function updated(Manager $manager)
    {
        ResponseCache::flush(['manager-' . $manager->id]);
    }
}