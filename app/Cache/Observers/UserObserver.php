<?php

namespace App\Cache\Observers;

use App\Models\User;
use ResponseCache;

class UserObserver
{
    public function saved(User $user)
    {
        ResponseCache::flush(['user-' . $user->id]);
    }
}