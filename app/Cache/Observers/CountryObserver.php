<?php

namespace App\Cache\Observers;

use App\Models\Location\Country;
use ResponseCache;

class CountryObserver
{
    public function created(Country $country)
    {
        ResponseCache::flush(['countries']);
    }

    public function deleted(Country $country)
    {
        ResponseCache::flush(['countries', 'country-' . $country->id]);
    }

    public function updated(Country $country)
    {
        ResponseCache::flush(['countries', 'country-' . $country->id]);
    }
}