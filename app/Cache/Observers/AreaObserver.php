<?php

namespace App\Cache\Observers;

use App\Models\Area\Area;
use ResponseCache;

class AreaObserver
{
    public function created(Area $area)
    {
        ResponseCache::flush(['areas']);
    }

    public function deleted(Area $area)
    {
        ResponseCache::flush(['areas', 'area-' . $area->id]);
    }

    public function updated(Area $area)
    {
        ResponseCache::flush(['areas', 'area-' . $area->id]);
        \Log::info('Flushed area ' . $area->id . ' and all others');
    }
}