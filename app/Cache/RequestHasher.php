<?php

namespace App\Cache;

use Illuminate\Http\Request;

class RequestHasher
{
    /** @var CacheProfile */
    protected $cacheProfile;

    public function __construct()
    {
        $this->cacheProfile = null;
    }

    public function setCacheProfile(CacheProfile $cacheProfile)
    {
        $this->cacheProfile = $cacheProfile;
    }

    public function getHashFor(Request $request): string
    {
        if (is_null($this->cacheProfile)) {
            return '';
        }

        return 'responsecache-' . md5(
            "{$request->getUri()}" . $this->cacheProfile->cacheNameSuffix($request)
        );
    }
}