<?php

namespace App\Cache\Commands;

use App\Cache\ResponseCacheRepository;
use Illuminate\Console\Command;

class Flush extends Command
{
    protected $signature = 'responsecache:flush';

    protected $description = 'Flush the response cache';

    public function handle(ResponseCacheRepository $cache)
    {
        $cache->flush();
    }
}