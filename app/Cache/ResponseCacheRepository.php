<?php

namespace App\Cache;

use Illuminate\Cache\Repository;
use Symfony\Component\HttpFoundation\Response;

class ResponseCacheRepository
{
    protected $cache;

    protected $responseSerializer;

    public function __construct(ResponseSerializer $responseSerializer, Repository $cache)
    {
        $this->cache = $cache;
        $this->responseSerializer = $responseSerializer;
    }

    public function put(string $key, $response, $minutes, $tags)
    {
        $this->cache->keywords($tags)->put($key, $this->responseSerializer->serialize($response), $minutes);
    }

    public function has(string $key): bool
    {
        return $this->cache->has($key);
    }

    public function get(string $key): Response
    {
        return $this->responseSerializer->unserialize($this->cache->get($key));
    }

    public function flush(array $tags = [])
    {
        if (!empty($tags)) {
            $this->cache->keywords($tags)->flush();
        } else {
            $this->cache->flush();
        }
    }
}