<?php

namespace App\Listeners;

use App\Events\ProductSaleStorned;
use App\Jobs\SendProductSaleStornedEmail;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ProductSaleStornedMessage
{
    use DispatchesJobs;

    /**
     * Handle the event
     *
     * @param  ProductSaleStorned  $event
     * @return void
     */
    public function handle(ProductSaleStorned $event)
    {
        $this->dispatch(with(new SendProductSaleStornedEmail($event)));
    }
}
