<?php

namespace App\Listeners;

use App\Events\AreaChanged;
use App\Jobs\GenerateAreaTags;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;

class AreaTags
{
    use DispatchesJobs;

    /**
     * Handle the event.
     *
     * @param  AreaChanged  $event
     * @return void
     */
    public function handle(AreaChanged $event)
    {
        info('event: area-changed / generate tags', ['areaId' => $event->area->id]);
        $this->dispatch(new GenerateAreaTags($event));
    }
}
