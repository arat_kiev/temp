<?php

namespace App\Listeners;

use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\WorkerStopping;

class JobEventListener
{
    /**
     * Handle user login events.
     * @param WorkerStopping $event
     */
    public function onWorkerStop(WorkerStopping $event)
    {
        \Log::info('QUEUE: worker ('.getmypid() .') stopped/restarted');
    }

    public function onJobDone(JobProcessed $event)
    {
        \Log::info('QUEUE: worker ('.getmypid().') finished');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Queue\Events\WorkerStopping',
            'App\Listeners\JobEventListener@onWorkerStop'
        );

        $events->listen(
            'Illuminate\Queue\Events\JobProcessed',
            'App\Listeners\JobEventListener@onJobDone'
        );
    }
}
