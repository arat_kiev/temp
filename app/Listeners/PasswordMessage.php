<?php

namespace App\Listeners;

use App\Events\PasswordReset;
use App\Jobs\SendPasswordEmail;
use Illuminate\Foundation\Bus\DispatchesJobs;

class PasswordMessage
{
    use DispatchesJobs;

    /**
     * Handle the event.
     *
     * @param  PasswordReset  $event
     * @return void
     */
    public function handle(PasswordReset $event)
    {
        $this->dispatch(with(new SendPasswordEmail($event)));
    }
}
