<?php

namespace App\Listeners;

use App\Events\AreaChanged;
use App\Jobs\AssignCitiesToArea;
use App\Jobs\GenerateAreaTags;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;

class AssignCities
{
    use DispatchesJobs;

    /**
     * Handle the event.
     *
     * @param  AreaChanged  $event
     * @return void
     */
    public function handle(AreaChanged $event)
    {
        info('event: area-changed / assign cities', ['areaId' => $event->area->id]);
        $this->dispatch(with(new AssignCitiesToArea($event))->delay(Carbon::now()->addSeconds(15)));
    }
}
