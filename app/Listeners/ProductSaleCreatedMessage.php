<?php

namespace App\Listeners;

use App\Events\ProductSaleCreated;
use App\Jobs\SendCouponEmail;
use App\Jobs\SendProductSaleEmail;
use App\Jobs\SendProductSaleNotificationEmail;
use App\Models\Product\Product;
use App\Models\Promo\Coupon;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ProductSaleCreatedMessage
{
    use DispatchesJobs;

    /**
     * Handle the event.
     *
     * @param  ProductSaleCreated  $event
     * @return void
     */
    public function handle(ProductSaleCreated $event)
    {
        $this->dispatch(with(new SendProductSaleEmail($event)));
            $this->dispatch(with(new SendProductSaleNotificationEmail($event)));

        $user = $event->sale->user
            ?: ($event->sale->email
                ? User::where('email', $event->sale->email)->first()
                : null);

        if ($user) {
            $this->sendSaleCouponIfApplicable($user, $event->sale->product);
        }
    }

    private function sendSaleCouponIfApplicable(User $user, Product $product)
    {
        $userCoupons = $user->usedCoupons()->pluck('id')->toArray();

        $coupons = Coupon::whereHas('promotion', function ($promotion) use ($product) {
            $promotion
                ->where('type', 'SALE')
                ->where(function ($query) use ($product) {
                    $query
                        ->whereHas('products', function ($types) use ($product) {
                            $types->where('id', $product->id);
                        })
                        ->orWhere(function ($q) {
                            $q->doesntHave('ticketTypes')
                                ->doesntHave('products');
                        });
                })
                ->whereDate('valid_from', '<=', Carbon::now())
                ->where(function ($q) {
                    $q->whereDate('valid_till', '>=', Carbon::now())
                        ->orWhereNull('valid_till');
                });
        });

        if ($userCoupons) {
            $coupons->whereNotIn('id', $userCoupons);
        }

        if ($coupon = $coupons->first()) {
            $this->dispatch(with(new SendCouponEmail($user, $coupon)));
        }
    }
}
