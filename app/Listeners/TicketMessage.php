<?php

namespace App\Listeners;

use App\Events\TicketCreated;
use App\Jobs\SendCouponEmail;
use App\Jobs\SendTicketEmail;
use App\Jobs\SendNotificationEmail;
use App\Models\Promo\Coupon;
use App\Models\Ticket\TicketType;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;

class TicketMessage
{
    use DispatchesJobs;

    /**
     * Handle the event.
     *
     * @param  TicketCreated  $event
     * @return void
     */
    public function handle(TicketCreated $event)
    {
        $this->dispatch(with(new SendTicketEmail($event)));
        $this->dispatch(with(new SendNotificationEmail($event)));
        
        $user = $event->ticket->user ?: User::where('email', $event->ticket->resellerTicket->email)->first();
        
        if ($user) {
            $this->sendSaleCouponIfApplicable($user, $event->ticket->type);
        }
    }

    private function sendSaleCouponIfApplicable(User $user, TicketType $ticketType)
    {
        $userCoupons = $user->usedCoupons()->pluck('id')->toArray();

        $coupons = Coupon::whereHas('promotion', function ($promotion) use ($ticketType) {
            $promotion
                ->where('type', 'SALE')
                ->where(function ($query) use ($ticketType) {
                    $query
                        ->whereHas('ticketTypes', function ($types) use ($ticketType) {
                            $types->where('id', $ticketType->id);
                        })
                        ->orWhere(function ($q) {
                            $q->doesntHave('ticketTypes')
                                ->doesntHave('products');
                        });
                })
                ->whereDate('valid_from', '<=', Carbon::now())
                ->where(function ($q) {
                    $q->whereDate('valid_till', '>=', Carbon::now())
                        ->orWhereNull('valid_till');
                });
        });

        if ($userCoupons) {
            $coupons->whereNotIn('id', $userCoupons);
        }

        if ($coupon = $coupons->first()) {
            $this->dispatch(with(new SendCouponEmail($user, $coupon)));
        }
    }
}
