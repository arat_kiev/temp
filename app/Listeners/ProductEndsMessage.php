<?php

namespace App\Listeners;

use App\Events\ProductEndsInStock;
use App\Jobs\SendProductEndsInStockEmail;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ProductEndsMessage
{
    use DispatchesJobs;

    /**
     * Handle the event.
     *
     * @param  ProductEndsInStock  $event
     * @return void
     */
    public function handle(ProductEndsInStock $event)
    {
        if ($event->stock->notificatableUsers()->count()) {
            $this->dispatch(with(new SendProductEndsInStockEmail($event)));
        }
    }
}
