<?php

namespace App\Listeners;

use App\Events\TicketStorned;
use App\Jobs\SendTicketStornoEmail;
use Illuminate\Foundation\Bus\DispatchesJobs;

class TicketStornoMessage
{
    use DispatchesJobs;

    /**
     * Handle the event.
     *
     * @param  TicketStorned  $event
     * @return void
     */
    public function handle(TicketStorned $event)
    {
        $this->dispatch(with(new SendTicketStornoEmail($event)));
    }
}
