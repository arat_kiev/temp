<?php

namespace App\Listeners;

use App\Events\TicketPriceCreated;
use App\Jobs\AddDefaultCommissions;
use Illuminate\Foundation\Bus\DispatchesJobs;

class AssignDefaultCommissionsToTicketPrice
{
    use DispatchesJobs;

    /**
     * Handle the event.
     *
     * @param  TicketPriceCreated  $event
     * @return void
     */
    public function handle(TicketPriceCreated $event)
    {
        $this->dispatch(with(new AddDefaultCommissions($event)));
    }
}
