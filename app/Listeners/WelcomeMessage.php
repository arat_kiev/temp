<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\Jobs\SendWelcomeEmail;
use Illuminate\Foundation\Bus\DispatchesJobs;

class WelcomeMessage
{
    use DispatchesJobs;

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $this->dispatch(with(new SendWelcomeEmail($event)));
    }
}
