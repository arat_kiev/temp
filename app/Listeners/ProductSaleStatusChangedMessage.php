<?php

namespace App\Listeners;

use App\Events\ProductSaleStatusChanged;
use App\Jobs\SendProductSaleStatusChangedEmail;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ProductSaleStatusChangedMessage
{
    use DispatchesJobs;

    /**
     * Handle the event
     *
     * @param  ProductSaleStatusChanged  $event
     * @return void
     */
    public function handle(ProductSaleStatusChanged $event)
    {
        $this->dispatch(with(new SendProductSaleStatusChangedEmail($event)));
    }
}
