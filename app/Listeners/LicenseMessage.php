<?php

namespace App\Listeners;

use App\Events\LicenseChanged;
use App\Jobs\SendLicenseEmail;
use Illuminate\Foundation\Bus\DispatchesJobs;

class LicenseMessage
{
    use DispatchesJobs;

    /**
     * Handle the event.
     *
     * @param  LicenseChanged  $event
     * @return void
     */
    public function handle(LicenseChanged $event)
    {
        $this->dispatch(with(new SendLicenseEmail($event)));
    }
}
