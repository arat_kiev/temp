<?php

namespace App\Listeners;

use App\Events\MemberChanged;
use App\Jobs\ConnectMemberToUser;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ConnectMember
{
    use DispatchesJobs;

    public function handle(MemberChanged $event)
    {
        $this->dispatch(new ConnectMemberToUser($event));
    }
}
