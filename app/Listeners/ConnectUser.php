<?php

namespace App\Listeners;

use App\Events\UserActivated;
use App\Jobs\ConnectUserToMember;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ConnectUser
{
    use DispatchesJobs;

    public function handle(UserActivated $event)
    {
        $this->dispatch(new ConnectUserToMember($event));
    }
}
