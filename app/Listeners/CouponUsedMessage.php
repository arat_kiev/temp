<?php

namespace App\Listeners;

use App\Events\CouponUsed;
use App\Jobs\SendUsedCouponReceiverEmail;
use App\Jobs\SendUsedCouponOwnerEmail;
use App\Models\Ticket\TicketType;
use Illuminate\Foundation\Bus\DispatchesJobs;

class CouponUsedMessage
{
    use DispatchesJobs;

    /**
     * Handle the event.
     *
     * @param  CouponUsed  $event
     * @return void
     */
    public function handle(CouponUsed $event)
    {
        $this->dispatch(with(new SendUsedCouponReceiverEmail($event)));

        if ($event->coupon->promotion && $event->coupon->promotion->type === 'INVITE') {
            $this->dispatch(with(new SendUsedCouponOwnerEmail($event)));
        }
    }
}
