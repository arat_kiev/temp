<?php

namespace App\Listeners;

use App\Events\UserChangeEmailEvent;
use App\Mail\UserChangeEmail;
use JWTAuth;
use Mail;

class UserChangeEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserChangeEmailEvent  $event
     * @return void
     */
    public function handle(UserChangeEmailEvent $event)
    {
        try {
            // Get client template
            $template = $event->client->templates['email']['welcome'];

            $event->user->update([
                'email' => $event->request->email,
                'active' => false,
                'activation_code' => str_random(15)
            ]);

            Mail::to($event->user->email)->queue(new UserChangeEmail($event->user, $template));

            return response()->json(['success' => trans('email_change_success')], 200);

        } catch (JWTException $e) {
            return response()->json(['error' => trans('auth.email_change_error')], 401);
        }
    }
}
