<?php

namespace App\Listeners;

use App\Events\AreaCreated;
use App\Jobs\SendAreaNotification;
use Illuminate\Foundation\Bus\DispatchesJobs;

class AreaMessage
{
    use DispatchesJobs;

    /**
     * Handle the event.
     *
     * @param AreaCreated $event
     */
    public function handle(AreaCreated $event)
    {
        $this->dispatch(with(new SendAreaNotification($event)));
    }
}
