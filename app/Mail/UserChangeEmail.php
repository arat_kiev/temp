<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;

class UserChangeEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user instance.
     *
     * @var App\Models\User
     */
    public $user;

    /**
     * @var
     */
    public $template;


    /**
     * UserChangeEmail constructor.
     *
     * @param User $user
     * @param $template
     */
    public function __construct(User $user, $template)
    {
        $this->user = $user;
        $this->template = $template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('EMAIL_INFO'))
            ->subject(trans('email.change.subject'))
            ->view($this->template);
    }
}
