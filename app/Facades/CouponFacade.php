<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class CouponFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'couponManager';
    }
}
