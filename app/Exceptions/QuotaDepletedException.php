<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class QuotaDepletedException extends HttpException
{
    public function __construct($message = null, \Exception $previous = null, $code = 0)
    {
        parent::__construct(400, 'Quota depleted', $previous, array(), $code);
    }
}
