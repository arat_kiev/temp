<?php

namespace App\Exceptions;

class AreaHasNoLocationsException extends \Exception
{
}
