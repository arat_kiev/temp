<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Support\MessageBag;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ApplicationException) {
            return $this->convertApplicationExceptionToResponse($e, $request);
        }

        if ($request->wantsJson()) {
            if ($e instanceof ModelNotFoundException) {
                $e = new NotFoundHttpException($e->getMessage(), $e);
            }

            if ($e instanceof NotFoundHttpException) {
                return response()->json([
                    'error' => 'Not found',
                ], $e->getStatusCode());
            }

            if ($e instanceof MethodNotAllowedHttpException) {
                return response()->json([
                    'error' => 'Method not allowed',
                ], $e->getStatusCode());
            }

            if ($e instanceof AccessDeniedHttpException) {
                return response()->json([
                    'error' => 'Access denied',
                    'message' => $e->getMessage(),
                ], $e->getStatusCode());
            }

            if ($e instanceof BadRequestHttpException) {
                return response()->json([
                    'error' => 'Bad request',
                ], $e->getStatusCode());
            }

            if ($e instanceof TokenExpiredException) {
                return response()->json([
                    'error' => 'Token expired',
                ], $e->getStatusCode());
            }

            if ($e instanceof TokenInvalidException) {
                return response()->json([
                    'error' => 'Token invalid',
                ], $e->getStatusCode());
            }

            if ($e instanceof ApiKeyNotFoundException) {
                return response()->json([
                    'error' => 'API key invalid',
                ], $e->getStatusCode());
            }

            if ($e instanceof TicketLicenseMissingException) {
                return response()->json([
                    'error' => 'License is missing',
                ], $e->getStatusCode());
            }

            if ($e instanceof NotEnoughBalanceException) {
                return response()->json([
                    'error' => 'Not enough balance',
                    'missing' => $e->missing,
                ], $e->getStatusCode());
            }

            if ($e instanceof TicketDateNotValidException) {
                return response()->json([
                    'error' => 'Ticket date invalid',
                ], $e->getStatusCode());
            }

            if ($e instanceof ProfileNotCompleteException) {
                return response()->json([
                    'error' => 'Profile not complete',
                ], $e->getStatusCode());
            }

            if ($e instanceof QuotaDepletedException) {
                return response()->json([
                    'error' => 'Quota depleted',
                ], $e->getStatusCode());
            }

            if ($e instanceof ActiveTicketsException) {
                return response()->json([
                    'error' => 'Tickets active',
                ], $e->getStatusCode());
            }

            if ($e instanceof InvalidGroupCountException) {
                return response()->json([
                    'error' => 'Group size does not match',
                ], $e->getStatusCode());
            }

            if ($e instanceof ModelException) {
                return response()->json([
                    'error' => $e->getMessage(),
                ], $e->getStatusCode());
            }
        }

        return parent::render($request, $e);
    }

    private function convertApplicationExceptionToResponse(ApplicationException $e, $request)
    {
        $errors = (array) ($e->getMessage() ?: $request->session()->get('errors', new MessageBag())->toArray());

        if ($request->expectsJson()) {
            return response()->json(['errors' => $errors], 422);
        }

        return redirect()->back()->withInput(
            $request->input()
        )->withErrors($errors);
    }
}
