<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;

class NotEnoughBalanceException extends HttpException
{
    public $missing;

    public function __construct($missing, $message = '', $code = 400, Exception $previous = null)
    {
        $this->missing = $missing;
        parent::__construct($code, 'Not enough balance', $previous);
    }
}
