<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\GenerateAreaTags::class,
        \App\Console\Commands\AnonymizeData::class,
        \App\Console\Commands\GenerateFisherIds::class,
        \App\Console\Commands\ConnectMembersToUsers::class,
        \App\Console\Commands\CopyPrices::class,
        \App\Console\Commands\AddCommissionValueToTickets::class,
        \App\Console\Commands\MigrateHaultToCheckins::class,
        \App\Console\Commands\InspectionsMigrations::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();
    }
}
