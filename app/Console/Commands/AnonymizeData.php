<?php

namespace App\Console\Commands;

use App\Models\Contact\Manager;
use App\Models\Contact\Reseller;
use App\Models\Organization\Organization;
use App\Models\Picture;
use App\Models\PointOfInterest\PointOfInterest;
use App\Models\Product\ProductSale;
use App\Models\Ticket\ResellerTicket;
use App\Models\User;
use Illuminate\Console\Command;
use Faker\Factory as Faker;
use Illuminate\Database\QueryException;
use File as Filesystem;
use Illuminate\Http\File;
use Symfony\Component\Console\Helper\ProgressBar;

class AnonymizeData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anonymize
                            {--a|all} 
                            {--u|user}
                            {--m|manager}
                            {--o|organization}
                            {--r|reseller}
                            {--t|tickets}
                            {--s|sales}
                            {--p|poi}
                            {--i|images}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fake user data';

    /**
     * The format of the progressbar
     *
     * @var string
     */
    protected $progressFormat = ' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s% ';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (app()->environment() === 'production') {
            $this->error('Not allowed in production');
            exit(1);
        }

        if ($this->option('all') || $this->option('user')) {
            $this->handleUsers();
        }

        if ($this->option('all') || $this->option('manager')) {
            $this->handleManagers();
        }

        if ($this->option('all') || $this->option('organization')) {
            $this->handleOrganizations();
        }

        if ($this->option('all') || $this->option('reseller')) {
            $this->handleResellers();
        }

        if ($this->option('all') || $this->option('tickets')) {
            $this->handleTickets();
        }

        if ($this->option('all') || $this->option('sales')) {
            $this->handleSales();
        }

        if ($this->option('all') || $this->option('poi')) {
            $this->handlePOIs();
        }

        if ($this->option('all') || $this->option('images')) {
            $this->handleImages();
        }

        return 0;
    }

    private function handleUsers()
    {
        $faker = Faker::create('de_AT');

        $progress = new ProgressBar($this->getOutput());

        $progress->setFormat($this->progressFormat);

        $progress->setMessage('Anonymizing user data...');
        $progress->start(User::count());

        User::where('id', '>', 100)->chunk(50, function ($users) use ($faker, $progress) {
            foreach ($users as $user) {
                $user->email = "u{$user->id}-{$faker->safeEmail}";
                $user->name = $faker->unique()->userName;
                $user->balance = 0;
                $user->first_name = $faker->firstName;
                $user->last_name = $faker->lastName;
                $user->street = $faker->streetAddress;
                $user->post_code = $faker->postcode;
                $user->city = $faker->city;
                $user->birthday = $faker->dateTimeThisCentury('1998-01-01');
                $user->phone = $faker->phoneNumber;
                $user->fisher_id = User::uniqueFisherId();

                try {
                    $user->save();
                } catch (QueryException $e) {
                    echo $e->getMessage();
                }

                $progress->advance();
            }
        });

        $progress->setMessage('Users anonymized!');
        $progress->finish();
    }

    private function handleManagers()
    {
        $faker = Faker::create('de_AT');
        $fakerAT = Faker::create('at_AT');

        $progress = new ProgressBar($this->getOutput());

        $progress->setFormat($this->progressFormat);

        $progress->setMessage('Anonymizing manager data...');
        $progress->start(Manager::count());

        Manager::chunk(50, function ($managers) use ($faker, $fakerAT, $progress) {
            foreach ($managers as $manager) {
                $manager->email = $faker->unique()->safeEmail;
                $manager->name = $faker->company;
                $manager->person = "{$faker->firstName} {$faker->lastName}";
                $manager->street = $faker->streetAddress;
                $manager->area_code = $faker->postcode;
                $manager->city = $faker->city;
                $manager->phone = $faker->phoneNumber;
                $manager->website = "http://{$faker->domainName}";
                $manager->account_number = null;
                $manager->short_code = null;
                $manager->uid = !empty($manager->uid) ? $fakerAT->vat(false) : "";

                try {
                    $manager->save();
                } catch (QueryException $e) {
                    echo $e->getMessage();
                }

                $progress->advance();
            }
        });

        $progress->setMessage('Managers anonymized!');
        $progress->finish();
    }

    private function handleOrganizations()
    {
        $faker = Faker::create('de_AT');

        $progress = new ProgressBar($this->getOutput());

        $progress->setFormat($this->progressFormat);

        $progress->setMessage('Anonymizing organization data...');
        $progress->start(Organization::count());

        Organization::chunk(50, function ($organizations) use ($faker, $progress) {
            foreach ($organizations as $organization) {
                $organization->email = $faker->unique()->safeEmail;
                $organization->name = $faker->company;
                $organization->person = "{$faker->firstName} {$faker->lastName}";
                $organization->street = $faker->streetAddress;
                $organization->area_code = $faker->postcode;
                $organization->city = $faker->city;
                $organization->phone = $faker->phoneNumber;
                $organization->website = "http://{$faker->domainName}";

                try {
                    $organization->save();
                } catch (QueryException $e) {
                    echo $e->getMessage();
                }

                $progress->advance();
            }
        });

        $progress->setMessage('Organizations anonymized!');
        $progress->finish();
    }

    private function handleResellers()
    {
        $faker = Faker::create('de_AT');
        $fakerAT = Faker::create('at_AT');

        $progress = new ProgressBar($this->getOutput());

        $progress->setFormat($this->progressFormat);

        $progress->setMessage('Anonymizing reseller data...');
        $progress->start(Reseller::count());

        Reseller::chunk(50, function ($resellers) use ($faker, $fakerAT, $progress) {
            foreach ($resellers as $reseller) {
                $reseller->email = $faker->unique()->safeEmail;
                $reseller->name = $faker->company;
                $reseller->person = "{$faker->firstName} {$faker->lastName}";
                $reseller->street = $faker->streetAddress;
                $reseller->area_code = $faker->postcode;
                $reseller->city = $faker->city;
                $reseller->phone = $faker->phoneNumber;
                $reseller->website = "http://{$faker->domainName}";
                $reseller->uid = !empty($reseller->uid) ? $fakerAT->vat(false) : "";

                try {
                    $reseller->save();
                } catch (QueryException $e) {
                    echo $e->getMessage();
                }

                $progress->advance();
            }
        });

        $progress->setMessage('Resellers anonymized!');
        $progress->finish();
    }

    private function handleTickets()
    {
        $faker = Faker::create('de_AT');

        $progress = new ProgressBar($this->getOutput());

        $progress->setFormat($this->progressFormat);

        $progress->setMessage('Anonymizing ticket data...');
        $progress->start(ResellerTicket::count());

        ResellerTicket::chunk(50, function ($tickets) use ($faker, $progress) {
            foreach ($tickets as $ticket) {
                $ticket->first_name = $faker->firstName;
                $ticket->last_name = $faker->lastName;
                $ticket->street = $faker->streetAddress;
                $ticket->post_code = $faker->postcode;
                $ticket->city = $faker->city;
                $ticket->birthday = $faker->dateTimeThisCentury('1998-01-01');
                $ticket->phone = $faker->phoneNumber;
                $ticket->email = $faker->unique()->safeEmail;

                try {
                    $ticket->save();
                } catch (QueryException $e) {
                    echo $e->getMessage();
                }

                $progress->advance();
            }
        });

        $progress->setMessage('Tickets anonymized!');
        $progress->finish();
    }

    private function handleSales()
    {
        $faker = Faker::create('de_AT');

        $progress = new ProgressBar($this->getOutput());

        $progress->setFormat($this->progressFormat);

        $progress->setMessage('Anonymizing sales data...');
        $progress->start(ProductSale::count());

        ProductSale::chunk(50, function ($sales) use ($faker, $progress) {
            foreach ($sales as $sale) {
                $sale->email = $faker->unique()->safeEmail;
                $sale->first_name = $faker->firstName;
                $sale->last_name = $faker->lastName;
                $sale->street = $faker->streetAddress;
                $sale->post_code = $faker->postcode;
                $sale->city = $faker->city;

                try {
                    $sale->save();
                } catch (QueryException $e) {
                    echo $e->getMessage();
                }

                $progress->advance();
            }
        });

        $progress->setMessage('Sales anonymized!');
        $progress->finish();
    }

    private function handlePOIs()
    {
        $faker = Faker::create('de_AT');

        $progress = new ProgressBar($this->getOutput());

        $progress->setFormat($this->progressFormat);

        $progress->setMessage('Anonymizing POI data...');
        $progress->start(PointOfInterest::count());

        PointOfInterest::chunk(50, function ($pois) use ($faker, $progress) {
            foreach ($pois as $poi) {
                $poi->name = $faker->company;
                $poi->street = $faker->streetAddress;
                $poi->post_code = $faker->postcode;
                $poi->city = $faker->city;
                $poi->email = $faker->unique()->safeEmail;
                $poi->phone = $faker->phoneNumber;
                $poi->website = "http://{$faker->domainName}";

                try {
                    $poi->save();
                } catch (QueryException $e) {
                    echo $e->getMessage();
                }

                $progress->advance();
            }
        });

        $progress->setMessage('POIs anonymized!');
        $progress->finish();
    }

    private function handleImages()
    {
        Filesystem::copy(resource_path('fake/fake.jpg'), storage_path('temp/fake.jpg'));
        $fakeImage = new Picture();
        $fakeImage->file = new File(storage_path('temp/fake.jpg'));

        Filesystem::copy(resource_path('fake/fake.pdf'), storage_path('temp/fake.pdf'));
        $fakeDocument = new Picture();
        $fakeDocument->file =  new File(storage_path('temp/fake.pdf'));

        $progress = new ProgressBar($this->getOutput());

        $progress->setFormat($this->progressFormat);

        $progress->setMessage('Anonymizing images and documents...');
        $progress->start(Picture::count());

        Picture::chunk(50, function ($pictures) use ($progress, $fakeDocument, $fakeImage) {
            foreach ($pictures as $picture) {
                if (ends_with($picture->file, '.pdf')) {
                    $picture->filePath = $fakeDocument->fileName;
                } else {
                    $picture->filePath = $fakeImage->fileName;
                }

                try {
                    $picture->save();
                } catch (QueryException $e) {
                    echo $e->getMessage();
                }

                $progress->advance();
            }
        });

        $progress->setMessage('Images and documents anonymized!');
        $progress->finish();
    }
}
