<?php

namespace App\Console\Commands;

use App\Models\Inspection\InspectionTour;
use Illuminate\Console\Command;
use App\Models\Ticket\TicketInspection;

class InspectionsMigrations extends Command
{
    const TICKET = 'ticket';
    const AREA = 'area';
    const HAUL = 'haul';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inspection:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate to new inspections system. Need argument for inspection type ("ticket", "area", "haul")';

    /**
     * Inspection type
     *
     * @var
     */
    protected $inspectionType;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->inspectionType = $this->anticipate('Type inspection:',
            [self::TICKET, self::AREA, self::HAUL], self::TICKET);

        $this->info('Inspection converting started');

        switch ($this->inspectionType) {
            case self::TICKET:
                TicketInspection::all()->each(function ($ticketInspection) {
                    $inspectionTour = InspectionTour::create([
                        'inspector_id' => 1
                    ]);

                    $ticketInspection->inspectionTour()->associate($inspectionTour);
                    $ticketInspection->save();
                });
                break;
            case self::AREA:
                break;
            case self::HAUL:
                break;
            default:
                $this->alert('This inspection type is not available. Please choose one: '. self::TICKET .', '. self::AREA .', '. self::HAUL);
        }

        $this->info('Inspection converting finished');
    }
}
