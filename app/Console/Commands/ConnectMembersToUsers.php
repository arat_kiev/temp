<?php

namespace App\Console\Commands;

use App\Models\Organization\Member;
use App\Models\User;
use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class ConnectMembersToUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'member:connect';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $query = Member::whereNull('user_id');
        $count = $query->count();

        if ($count > 0) {
            $progress = new ProgressBar(new ConsoleOutput());
            $progress->setFormat('Checking member %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s% --- %message%');
            $progress->setMessage('Connecting members to users');
            $progress->start($count);

            $connectedMembers = 0;

            $query->chunk(50, function ($members) use ($progress, &$connectedMembers) {
                $members->each(function ($member) use ($progress, &$connectedMembers) {
                    if ($user = User::where('email', $member->email)->first()) {
                        $member->user()->associate($user);
                        $member->save();

                        $connectedMembers++;
                    }

                    $progress->advance();
                });
            });

            $progress->setMessage("Connected $connectedMembers members\n");
            $progress->finish();
        }
    }
}
