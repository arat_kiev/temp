<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\Organization\Member;
use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class GenerateFisherIds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fisher-ids:generate
                            {--members : generate for members}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate fisher ids for users which miss one';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $isMember = $this->option('members');

        $query = $isMember ? Member::query() : User::query();

        $count = (clone $query)->whereNull('fisher_id')->count();

        if ($count > 0) {
            $progress = new ProgressBar(new ConsoleOutput());
            $progress->setFormat('Checking user %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s% --- %message%');
            $progress->setMessage('Generating fisher-ids...');
            $progress->start($count);

            $updatedUsers = 0;

            $query->chunk(50, function ($users) use ($progress, &$updatedUsers) {
                $users->each(function ($user) use ($progress, &$updatedUsers) {
                    if ($user->fisher_id == null && !$user->user) {
                        $user->fisher_id = User::uniqueFisherId();
                        $user->save();
                        $updatedUsers++;
                    }

                    $progress->advance();
                });
            });

            $progress->setMessage("\nGenerated fisher-id for " . $updatedUsers . " Users\n");
            $progress->finish();
        } else {
            echo "No users without fisher_id...\n";
            echo "──────█▀▄─▄▀▄─▀█▀─█─█─▀─█▀▄─▄▀▀▀─────\n";
            echo "──────█─█─█─█──█──█▀█─█─█─█─█─▀█─────\n";
            echo "──────▀─▀──▀───▀──▀─▀─▀─▀─▀──▀▀──────\n";
            echo "─────────────────────────────────────\n";
            echo "───────────────▀█▀─▄▀▄───────────────\n";
            echo "────────────────█──█─█───────────────\n";
            echo "────────────────▀───▀────────────────\n";
            echo "─────────────────────────────────────\n";
            echo "─────█▀▀▄─█▀▀█───█──█─█▀▀─█▀▀█─█▀▀───\n";
            echo "─────█──█─█──█───█▀▀█─█▀▀─█▄▄▀─█▀▀───\n";
            echo "─────▀▀▀──▀▀▀▀───▀──▀─▀▀▀─▀─▀▀─▀▀▀───\n";
            echo "─────────────────────────────────────\n";
            echo "─────────▄███████████▄▄──────────────\n";
            echo "──────▄██▀──────────▀▀██▄────────────\n";
            echo "────▄█▀────────────────▀██───────────\n";
            echo "──▄█▀────────────────────▀█▄─────────\n";
            echo "─█▀──██──────────────██───▀██────────\n";
            echo "█▀──────────────────────────██───────\n";
            echo "█──███████████████████───────█───────\n";
            echo "█────────────────────────────█───────\n";
            echo "█────────────────────────────█───────\n";
            echo "█────────────────────────────█───────\n";
            echo "█────────────────────────────█───────\n";
            echo "█────────────────────────────█───────\n";
            echo "█▄───────────────────────────█───────\n";
            echo "▀█▄─────────────────────────██───────\n";
            echo "─▀█▄───────────────────────██────────\n";
            echo "──▀█▄────────────────────▄█▀─────────\n";
            echo "───▀█▄──────────────────██───────────\n";
            echo "─────▀█▄──────────────▄█▀────────────\n";
            echo "───────▀█▄▄▄──────▄▄▄███████▄▄───────\n";
            echo "────────███████████████───▀██████▄───\n";
            echo "─────▄███▀▀────────▀███▄──────█─███──\n";
            echo "───▄███▄─────▄▄▄▄────███────▄▄████▀──\n";
            echo "─▄███▓▓█─────█▓▓█───████████████▀────\n";
            echo "─▀▀██▀▀▀▀▀▀▀▀▀▀███████████────█──────\n";
            echo "────█─▄▄▄▄▄▄▄▄█▀█▓▓─────██────█──────\n";
            echo "────█─█───────█─█─▓▓────██────█──────\n";
            echo "────█▄█───────█▄█──▓▓▓▓▓███▄▄▄█──────\n";
            echo "────────────────────────██───────────\n";
            echo "────────────────────────██───▄███▄───\n";
            echo "────────────────────────██─▄██▓▓▓██──\n";
            echo "───────────────▄██████████─█▓▓▓█▓▓██▄\n";
            echo "─────────────▄██▀───▀▀███──█▓▓▓██▓▓▓█\n";
            echo "─▄███████▄──███───▄▄████───██▓▓████▓█\n";
            echo "▄██▀──▀▀█████████████▀▀─────██▓▓▓▓███\n";
            echo "██▀─────────██──────────────██▓██▓███\n";
            echo "██──────────███──────────────█████─██\n";
            echo "██───────────███──────────────█─██──█\n";
            echo "██────────────██─────────────────█───\n";
            echo "██─────────────██────────────────────\n";
            echo "██─────────────███───────────────────\n";
            echo "██──────────────███▄▄────────────────\n";
            echo "███──────────────▀▀███───────────────\n";
            echo "─███─────────────────────────────────\n";
            echo "──███────────────────────────────────\n";
        }
    }
}
