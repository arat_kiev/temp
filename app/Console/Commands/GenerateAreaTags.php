<?php

namespace App\Console\Commands;

use App\Events\AreaChanged;
use App\Jobs\GenerateAreaTags as GenAreaTagsJob;
use App\Models\Area\Area;
use App\Models\Area\AreaTag;
use App\Models\Client;
use Config;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Symfony\Component\Console\Helper\ProgressBar;

class GenerateAreaTags extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'area-tags:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'regenerate area search tags';

    /** @var Area */
    protected $areaRepository;

    /** @var AreaTag */
    protected $tagRepository;

    /** @var Progressbar */
    protected $progress;

    /**
     * GenerateAreaTags constructor.
     * @param Area $areaRepository
     * @param AreaTag $tagRepository
     */
    public function __construct(Area $areaRepository, AreaTag $tagRepository)
    {
        parent::__construct();

        $this->areaRepository = $areaRepository;
        $this->tagRepository = $tagRepository;

        if (\Schema::hasTable('clients')) {
            Config::set('app.client', Client::where('name', 'bissadmin')->first());
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->progress = new ProgressBar($this->getOutput(), $this->areaRepository->count());
        $this->progress->setBarWidth(50);
        $this->progress->setProgressCharacter('D');
        $this->progress->setEmptyBarCharacter(' ');
        $this->progress->setFormat(' %current%/%max% [8%bar%O] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%');
        $this->progress->start();

        $this->areaRepository->chunk(100, function ($areas) {
            $areas->each(function (Area $area) {
                $this->dispatch(new GenAreaTagsJob(new AreaChanged($area, null, []), false));
                $this->progress->advance();
            });
        });

        $this->progress->finish();
    }
}
