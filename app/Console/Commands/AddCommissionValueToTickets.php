<?php

namespace App\Console\Commands;

use App\Models\Product\ProductSale;
use App\Models\Ticket\Ticket;
use Illuminate\Console\Command;
use CommissionManager;

class AddCommissionValueToTickets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commission:add-to-tickets
                            {--all : generate for all (even with already set)}
                            {--manager= : only for this managers id}
                            {--onlyTickets : just tickets}
                            {--onlyProducts : just product sales}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate commission value for old tickets without them';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $manager = $this->hasOption('manager') ? $this->option('manager') : null;

        if ($this->option('onlyTickets')) {
            $this->handleTickets($this->option('all'), $manager);
        } elseif ($this->option('onlyProducts')) {
            $this->handleProducts($this->option('all'), $manager);
        } else {
            $this->handleTickets($this->option('all'), $manager);
            $this->handleProducts($this->option('all'), $manager);
        }
    }

    private function handleTickets($allTickets = false, $manager = null)
    {
        $query = Ticket::whereNull('storno_id');

        if ($manager) {
            $query->join('ticket_types', 'tickets.ticket_type_id', '=', 'ticket_types.id')
                ->join('areas', 'ticket_types.area_id', '=', 'areas.id')
                ->join('managers', 'areas.manager_id', '=', 'managers.id')
                ->where('managers.id', '=', $manager)
                ->select('tickets.*');
        }

        $count = $query->count();

        if ($count > 0) {
            $progress = $this->output->createProgressBar($count);
            $progress->setFormat('Ticket %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s% --- %message%');
            $progress->setMessage('Regenerating commission for tickets');
            $progress->start($count);

            $callback = function ($ticket) use ($progress, $allTickets) {
                if ($allTickets || $ticket->commission_value_origin === null) {
                    $ticket->commission_value = CommissionManager::fetchTicketPriceCommission($ticket->price, $ticket->resellerTicket === null) * 100;
                    $ticket->save();
                }

                $progress->advance();
            };

            $query->chunk(50, function ($tickets) use ($callback) {
                $tickets->each($callback);
            });

            $progress->finish();
        } else {
            $this->info("\nNo tickets for operation");
        }
    }

    private function handleProducts($allProducts = false, $manager = null)
    {
        $query = ProductSale::whereNull('storno_id');

        if ($manager) {
            $query->join('products', 'product_sales.product_id', '=', 'products.id')
                ->join('has_products', function ($join) {
                    $join->on('has_products.product_id', '=', 'products.id');
                    $join->where('has_products.related_type', '=', 'managers');
                })
                ->join('managers', 'has_products.related_id', '=', 'managers.id')
                ->where('managers.id', '=', $manager)
                ->select('product_sales.*');
        }

        $count = $query->count();

        if ($count > 0) {
            $progress = $this->output->createProgressBar($count);
            $progress->setFormat('ProductSale %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s% --- %message%');
            $progress->setMessage('Regenerating commission for product sales');
            $progress->start($count);

            $callback = function ($product) use ($progress, $allProducts) {
                if ($allProducts || $product->commission_value_origin === null) {
                    $product->commission_value = CommissionManager::fetchProductPriceCommission($product->price) * 100;
                    $product->save();
                }

                $progress->advance();
            };


            $query->chunk(50, function ($tickets) use ($callback) {
                $tickets->each($callback);
            });

            $progress->finish();
        } else {
            $this->info("\nNo product sales for operation");
        }
    }
}