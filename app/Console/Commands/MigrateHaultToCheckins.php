<?php

namespace App\Console\Commands;

use App\Models\Haul;
use App\Models\Ticket\Checkin;
use App\Models\Ticket\Ticket;
use Carbon\Carbon;
use Illuminate\Console\Command;

class MigrateHaultToCheckins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkins:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrating from old halus system to checkins system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Checkins migration has been started");
        $bar = $this->output->createProgressBar(Ticket::count());

        Ticket::chunk(100, function ($tickets) use ($bar) {
            $bar->advance(100);
            $tickets->each(function ($ticket) {
                $diff = date_diff($ticket->valid_from, $ticket->valid_to);
                $checkin = null;

                if ($diff->d > 1 && $ticket->checkins->count() <= 0) {
                    $till = $ticket->valid_from->copy()->endOfDay();
                    $checkin = Checkin::create([
                        'ticket_id' => $ticket->id,
                        'from' => $ticket->valid_from,
                        'till' => $till,
                    ]);
                }
                if ($diff->d <= 1 && $ticket->checkins->count() <= 0) {
                    $checkin = Checkin::create([
                        'ticket_id' => $ticket->id,
                        'from' => $ticket->valid_from,
                        'till' => $ticket->valid_to,
                    ]);
                }

                if ($ticket->hauls->count() > 0) {
                    $ticket->hauls->each(function ($haul) use ($checkin) {
                        if (is_null($haul->checkin)) {
                            if ($haul->catch_date > $checkin->till) {
                                $newCheckin = Checkin::create([
                                    'ticket_id' => $haul->ticket_id,
                                    'from' => $haul->catch_date->startOfDay(),
                                    'till' => $haul->catch_date->endOfDay(),
                                ]);
                                $haul->checkin_id = $newCheckin->id;
                                $haul->save();
                            }
                            if ($haul->catch_date < $checkin->till) {
                                $haul->checkin_id = $checkin->id;
                                $haul->save();
                            }
                            $haul->save();
                        }
                    });
                }
            });
        });

        $bar->finish();
        $this->info("Checkins migration has been finished successfully");
    }
}
