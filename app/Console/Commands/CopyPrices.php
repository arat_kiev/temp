<?php

namespace App\Console\Commands;

use App\Models\Ticket\TicketPrice;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\ProgressBar;

class CopyPrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prices:copy
                            {year : Select the year}
                            {--only= : include these managers by id}
                            {--except= : exclude managers by id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy tickets for next/selected year';

    /** @var ProgressBar */
    protected $progress;

    /** @var integer */
    protected $sourceYear;

    /** @var integer */
    protected $targetYear;

    /** @var integer[] */
    protected $only;

    /** @var integer[] */
    protected $except;

    /** @var Carbon */
    protected $now;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();

        $this->now = Carbon::now();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->extractAndValidateParameters()) { return 1; }

        // Output some info about process
        $this->info('Copying prices from year ' . $this->sourceYear . ' to year ' . $this->targetYear);

        if ($this->only->count() > 0) {
            $this->info('> for managers with id in (' . $this->only->implode(', ') . ')');
        } elseif ($this->except->count() > 0) {
            $this->info('> for managers without id in (' . $this->except->implode(', ') . ')');
        } else {
            $this->info('> for all managers');
        }

        /** COPY PRICES
         *
         * - get all ticket types (with additionals)
         * - foreach ticket type get prices with valid_till in year-1
         * - foreach price clone and advance dates by 1 year
         */
        $this->progress = $this->output->createProgressBar();

        // Modify query according to parameters
        $query = TicketPrice::with(['weekdays', 'lockPeriods', 'ticketType.area.manager', 'commissions'])
            ->where('created_at', '<', $this->now)
            ->where(function ($query) {
                $query->where(\DB::raw('YEAR(ticket_prices.valid_from)'), '=', $this->sourceYear)
                    ->orWhere(\DB::raw('YEAR(ticket_prices.valid_to)'), '=', $this->sourceYear);
            });

        if ($this->only->count()) {
            $query->whereHas('ticketType.area.manager', function ($query) {
                $query->whereIn('id', $this->only);
            });
        } elseif ($this->except->count()) {
            $query->whereHas('ticketType.area.manager', function ($query) {
                $query->whereNotIn('id', $this->except);
            });
        }

        $query->chunk(50, function ($prices) {
            $prices->each(function ($price) {
                $this->copyPriceWithNewDates($price);
                $this->progress->advance();
            });
        });

        $this->progress->finish();

        $this->line("... done\n");

        return 0;
    }

    private function copyPriceWithNewDates(TicketPrice $price)
    {
        $type = $price->ticketType;

        $newPrice = $type->prices()->create([
            'name' => $price->name,
            'valid_from' => $price->valid_from->copy()->addYear(),
            'valid_to' => $price->valid_to->copy()->addYear(),
            'visible_from' => $price->visible_from->copy()->addYear(),
            'group_count' => $price->group_count,
            'type' => $price->type,
            'value' => $price->value,
            'show_vat' => $price->show_vat,
            'age' => $price->age,
        ]);

        $this->copyDateRestrictions($price, $newPrice);
        $this->copyPriceCommissions($price, $newPrice);
        $this->copyResellers($price, $newPrice);
        $this->copyOrganizations($price, $newPrice);
    }

    /**
     * Extract array of indizies from string
     *
     * @param $string
     * @return array
     */
    private function extractArrayValues($string)
    {
        if (empty($string)) {
            return [];
        }

        return explode(',', $string);
    }

    protected function extractAndValidateParameters()
    {
        $this->targetYear = $targetYear = $this->argument('year');
        $this->sourceYear = $sourceYear = $targetYear - 1;

        $this->only = collect($this->hasOption('only') ?
            $this->extractArrayValues($this->option('only')) : [])
            ->transform(function ($item) {
                return intval($item);
            });

        $this->except = collect($this->hasOption('except') ?
            $this->extractArrayValues($this->option('except')) : [])
            ->transform(function ($item) {
                return intval($item);
            });

        // Check if only integers are inputted
        if (!$this->only->reduce(function ($carry, $item) {
            return $carry && $item > 0;
        }, true)) {
            $this->error('Option --only : only positive integer indizes are allowed');
            return false;
        }

        if (!$this->except->reduce(function ($carry, $item) {
            return $carry && $item > 0;
        }, true)) {
            $this->error('Option --except : only positive integer indizes are allowed');
            return false;
        }

        // Check if no or only one option is used
        if ($this->only->count() && $this->except->count()) {
            $this->error('--only and --except are mutually exclusive');
            return false;
        }

        return true;
    }

    /**
     * @param TicketPrice $price
     * @param TicketPrice $newPrice
     */
    private function copyDateRestrictions(TicketPrice $price, TicketPrice $newPrice)
    {
        foreach ($price->weekdays as $weekday) {
            $newPrice->weekdays()->create(['weekday' => $weekday->weekday]);
        }

        foreach ($price->lockPeriods as $period) {
            $newPrice->lockPeriods()->create([
                'lock_from' => $period->lock_from->copy()->addYear(),
                'lock_till' => $period->lock_till->copy()->addYear()
            ]);
        }
    }

    /**
     * @param TicketPrice $price
     * @param TicketPrice $newPrice
     */
    private function copyPriceCommissions(TicketPrice $price, TicketPrice $newPrice)
    {
        foreach ($price->commissions as $commission) {
            $newCommission = $commission->replicate();
            $newPrice->commissions()->save($newCommission);
        }
    }

    /**
     * @param TicketPrice $price
     * @param $newPrice
     */
    private function copyResellers(TicketPrice $price, TicketPrice $newPrice)
    {
        $newPrice->resellers()->sync($price->resellers()->pluck('resellers.id')->toArray());
    }

    /**
     * @param TicketPrice $price
     * @param $newPrice
     */
    private function copyOrganizations(TicketPrice $price, TicketPrice $newPrice)
    {
        $newPrice->organizations()->sync($price->organizations()->pluck('organizations.id')->toArray());
    }
}
