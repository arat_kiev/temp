<?php

namespace App\PoS\Controllers;

use App\Http\Controllers\Controller;
use App;
use App\Models\Client;
use Auth;
use Config;
use Route;
use View;

class PoSController extends Controller
{
    public function __construct()
    {
        $pos = 'pos';

        $this->middleware(function ($request, $next) use ($pos) {
            if (!$client = Client::where('name', $pos)->first()) {
                abort(403, 'Ungültige Klientenkennung');
            }
            Config::set('app.client', $client);

            $pos = Route::input('pos');

            // Global variables for master layout
            View::share('client', config('app.client'));
            View::share('reseller', $this->getReseller());
            View::share('pos', $pos);
            View::share('terminal', $pos === 'terminal');

            return $next($request);
        });
    }

    /**
     * @return App\Models\Contact\Reseller
     */
    protected function getReseller()
    {
        if (Auth::user()) {
            if (!$reseller = Auth::user()->resellers()->first()) {
                abort(403, 'Benutzer hat keine Berechtigung für Verkaufsplatform');
            }

            return $reseller;
        }

        return null;
    }
}
