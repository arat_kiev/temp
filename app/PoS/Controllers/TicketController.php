<?php

namespace App\PoS\Controllers;

use App\Api1\Controllers\Reseller\TicketController as ResellerTicketController;
use App\Api1\Requests\ResellerTicketRequest;
use App\Api1\Transformers\MemberTransformer;
use App\Api1\Transformers\ResellerTicketTransformer;
use App\Api1\Transformers\UserTransformer;
use App\Managers\CommissionManager;
use App\Managers\TicketManager;
use App\Models\Area\Area;
use App\Models\Contact\Reseller;
use App\Models\Location\Country;
use App\Models\Ticket\ResellerTicket;
use App\Models\Ticket\TicketPrice;
use App\Models\Ticket\TicketType;
use App\Models\User;
use App\Models\Organization\Member;
use App\Blacklist\Blacklist;
use Carbon\Carbon;

class TicketController extends PoSController
{
    /**
     * Start page for ticket seller
     *
     * @param string $pos PoS client
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex($pos)
    {
        $countries = Country::all();

        $reseller = $this->getReseller();
        $resellerCountryId = $reseller->country ? $reseller->country->id : 1;

        return view('pos.ticket.index', compact('countries', 'resellerCountryId'));
    }

    /**
     * Get available areas for reseller as json (for ajax calls)
     *
     * @param string $pos PoS client
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAreas($pos)
    {
        $reseller = $this->getReseller();

        $now = Carbon::now()->toDateString();

        $areas = Area::setEagerLoads([])
            ->select(['areas.*', 'managers.name as manager', 'areas.id as id'])
            ->join('managers', 'areas.manager_id', '=', 'managers.id')
            ->join('ticket_types', 'areas.id', '=', 'ticket_types.area_id')
            ->join('ticket_prices', 'ticket_types.id', '=', 'ticket_prices.ticket_type_id')
            ->join('reseller_ticket_prices', 'ticket_prices.id', '=', 'reseller_ticket_prices.ticket_price_id')
            ->where('reseller_ticket_prices.reseller_id', '=', $reseller->id)
            ->whereRaw('? BETWEEN visible_from AND valid_to', [$now])
            ->groupBy('areas.id')
            ->orderBy('areas.name')
            ->get();

        return response()->json($areas);
    }

    /**
     * Get available ticket type for area as json (ajax)
     *
     * @param string $pos PoS client
     * @param int $areaId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTicketTypes($pos, $areaId)
    {
        $reseller = $this->getReseller();

        $now = Carbon::now()->toDateString();

        $ticketTypes = TicketType::with(['category.translations'])
            ->select('ticket_types.*')
            ->join('ticket_prices', 'ticket_types.id', '=', 'ticket_prices.ticket_type_id')
            ->join('reseller_ticket_prices', 'ticket_prices.id', '=', 'reseller_ticket_prices.ticket_price_id')
            ->where('reseller_ticket_prices.reseller_id', '=', $reseller->id)
            ->where('ticket_types.area_id', '=', $areaId)
            ->whereRaw('? BETWEEN visible_from AND valid_to', [$now])
            ->groupBy('ticket_types.id')
            ->get();

        $groupedTypes = collect([
            [
                'name' => 'bis zu 3 Tagen',
                'data' => collect(),
            ],
            [
                'name' => 'bis zu 2 Wochen',
                'data' => collect(),
            ],
            [
                'name' => 'bis zu 2 Monaten',
                'data' => collect(),
            ],
            [
                'name' => 'mehr als 2 Monate',
                'data' => collect(),
            ],
        ]);

        $ticketTypes->each(function ($type) use (&$groupedTypes) {
            $period = -1;

            switch ($type->category->id) {
                case 1:
                    $period = ((float)$type->duration) / 24;
                    break;
                case 2:
                    $period = $type->duration;
                    break;
                case 3:
                    $period = $type->duration * 7;
                    break;
                case 4:
                    $period = $type->duration * 30;
                    break;
                case 5:
                    $period = 100;
            }

            if ($period <= 3) {
                $groupedTypes[0]['data']->push($type);
            } elseif ($period <= 14) {
                $groupedTypes[1]['data']->push($type);
            } elseif ($period <= 60) {
                $groupedTypes[2]['data']->push($type);
            } else {
                $groupedTypes[3]['data']->push($type);
            }
        });

        return response()->json($groupedTypes);
    }

    /**
     * Get available ticket prices for ticket type as json (ajax)
     *
     * @param string $pos PoS client
     * @param int $ticketTypeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTicketPrices($pos, $ticketTypeId, CommissionManager $commissionManager)
    {
        $ticketType = TicketType::findOrFail($ticketTypeId);
        $reseller = $this->getReseller();

        $commissionIncluded = $ticketType->area->manager->commission_included;

        $ticketPrices = TicketPrice::active()
            ->select('ticket_prices.*')
            ->join('reseller_ticket_prices', 'ticket_prices.id', '=', 'reseller_ticket_prices.ticket_price_id')
            ->where('reseller_ticket_prices.reseller_id', '=', $reseller->id)
            ->where('ticket_prices.ticket_type_id', '=', $ticketType->id)
            ->groupBy('ticket_prices.id')
            ->get();

        $ticketPrices->each(function (TicketPrice $price) use ($commissionIncluded, $commissionManager) {
            $price->name = $price->nameWithYear;

            if ($commissionIncluded) {
                $price->value -= $commissionManager->fetchTicketPriceCommission($price) * 100;
            }
        });

        return response()->json($ticketPrices);
    }

    /**
     * Delegate ticket buy request to api controller
     *
     * @param $pos
     * @param ResellerTicketRequest $request
     * @param TicketManager $ticketManager
     * @param Blacklist $blacklist
     * @return \Illuminate\Http\Response
     */
    public function postNewTicket($pos, ResellerTicketRequest $request, TicketManager $ticketManager, Blacklist $blacklist)
    {
        return (new ResellerTicketController(new Reseller()))->store($request, $ticketManager, $this->getReseller()->id, $blacklist);
    }

    public function getTicketPdf($pos, $ticketId)
    {
        $reseller = $this->getReseller();

        return (new ResellerTicketController(new Reseller()))->pdf($reseller->id, $ticketId);
    }

    /**
     * Get valid dates for ticket price as json (ajax)
     *
     * @param string $pos PoS client
     * @param int $ticketPriceId
     * @param TicketManager $ticketManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function getValidDates($pos, $ticketPriceId, TicketManager $ticketManager)
    {
        $ticketPrice = TicketPrice::findOrFail($ticketPriceId);

        $dates = $ticketManager->getValidDates($ticketPrice);

        return response()->json($dates);
    }

    /**
     * Get the user data from the last reseller ticket
     * for quick fill of reseller form.
     *
     * @param string $pos PoS client
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLastUserData($pos)
    {
        $lastTicket = $this->getReseller()->tickets()->orderBy('ticket_id', 'desc')->firstOrNew([]);
        $data = [];

        if ($lastTicket->ticket_id) {
            $data = fractal()
                ->item($lastTicket, new ResellerTicketTransformer())
                ->toArray();
        }

        return response()->json($data);
    }

    /**
     * Retrieve User by fisher_id
     *
     * @param int $fisherId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserByFisherId($pos, $fisherId)
    {
        $fisherId = strtoupper($fisherId);

        $user = User::where('fisher_id', $fisherId)->firstOrNew([]);
        $member = Member::where('fisher_id', $fisherId)->firstOrNew([]);
        $resellerTicket = ResellerTicket::where('fisher_id', $fisherId)->orderBy('ticket_id', 'desc')->firstOrNew([]);
        $data = [];

        if ($user->id) {
            $data += fractal()
                ->item($user, new UserTransformer(true))
                ->toArray();
        }
        if ($resellerTicket->ticket_id) {
            $data += fractal()
                ->item($resellerTicket, new ResellerTicketTransformer())
                ->toArray();
        }
        if ($member->id) {
            $data += fractal()
                ->item($member, new MemberTransformer())
                ->toArray();
        }

        return response()->json($data);
    }

    /**
     * Get available license types for reseller form as json (ajax)
     *
     * @param string $pos PoS client
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLicenseTypes($pos, $ticketTypeId)
    {
        /** @var TicketType $ticketType */
        $ticketType = TicketType::findOrFail($ticketTypeId);
        $licenseTypes = $ticketType->requiredLicenseTypes
            ->merge($ticketType->optionalLicenseTypes)
            ->sortBy(function ($licenseType) {
                return $licenseType->name;
            })->values();

        return response()->json($licenseTypes);
    }
}
