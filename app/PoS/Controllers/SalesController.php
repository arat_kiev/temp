<?php

namespace App\PoS\Controllers;

use App\Events\TicketStorned;
use App\Managers\Export\TicketExportManager;
use App\Managers\Export\ProductSaleExportManager;
use App\Managers\ProductSaleManager;
use App\Managers\TicketManager;
use App\Models\Product\ProductSale;
use App\Models\Ticket\Ticket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Route;
use Datatables;
use Event;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class SalesController extends PoSController
{
    private $productSales = [];

    private $ticketSelect = [
        "ticket_id AS id",
        "ticket_id",
        "first_name",
        "last_name",
        "fisher_id",
        "'ticket' AS 'data_type'",
    ];

    private $productSaleSelect = [
        "product_sales.id AS id",
        "product_sales.id AS ticket_id",
        "product_sales.first_name AS first_name",
        "product_sales.last_name AS last_name",
        "users.fisher_id AS fisher_id",
        "'product' AS 'data_type'",
    ];

    private $isStorno = false;

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (Route::input('pos') === 'terminal') {
                throw new AccessDeniedHttpException();
            }

            return $next($request);
        });
    }

    public function getIndex(Request $request)
    {
        return view('pos.sales.index', $this->indexData($request));
    }

    public function getStornoIndex(Request $request)
    {
        $this->isStorno = true;

        return view('pos.sales.storno.index', $this->indexData($request));
    }

    private function indexData(Request $request)
    {
        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));

        $tickets = $this->getResellerTickets($from, $till);
        $productSales = $this->getResellerProductSales($from, $till);

        $count = $tickets->count() + $productSales->count();
        $total = $tickets->get()->sum('baseTicket.price.value') + $productSales->get()->sum('price.value_origin');
        $total = number_format($total / 100.0, 2, ",", ".").' €';

        return compact('count', 'total', 'from', 'till');
    }

    public function anyData($pos, Request $request, TicketManager $ticketManager, ProductSaleManager $saleManager)
    {
        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));

        $tickets = $this->filterTickets($this->getResellerTickets($from, $till), $request);
        $productSales = $this->filterProductSales($this->getResellerProductSales($from, $till), $request);

        $results = $tickets->unionAll($productSales);

        return Datatables::of($results)
            ->addColumn('ticket_number', function ($sale) use ($ticketManager, $saleManager) {
                if ($sale->data_type === 'ticket') {
                    return $ticketManager->getIdentifier($sale->baseTicket);
                } else {
                    return $saleManager->getIdentifier($this->fetchProductSale($sale->id));
                }
             })
            ->addColumn('user_name', function ($sale) {
                return $sale->first_name.' '.$sale->last_name;
            })
            ->addColumn('area_name', function ($sale) {
                if ($sale->data_type === 'ticket') {
                    return $sale->baseTicket->type->area->name;
                } else {
                    $areas = $this->fetchProductSale($sale->id)->product->areas->pluck('name')->toArray();
                    return str_limit(implode(', ', $areas), 50);
                }
            })
            ->addColumn('ticket_type', function ($sale) {
                if ($sale->data_type === 'ticket') {
                    return $sale->baseTicket->type->name;
                } else {
                    return 'Produkt';
                }
            })
            ->addColumn('valid_from', function ($sale) {
                if ($sale->data_type === 'ticket') {
                    $validFrom = $sale->baseTicket->valid_from;

                    return $sale->baseTicket->type->category->type === 'HOUR'
                        ? $validFrom->format('d.m.Y - H:i')
                        : $validFrom->format('d.m.Y');
                } else {
                    $from = $this->fetchProductSale($sale->id)->product->featured_from;
                    return $from ? $from->format('d.m.Y') : null;
                }
            })
            ->addColumn('created_at', function ($sale) {
                if ($sale->data_type === 'ticket') {
                    return $sale->baseTicket->created_at->format('d.m.Y - H:i');
                } else {
                    return $this->fetchProductSale($sale->id)->created_at->format('d.m.Y - H:i');
                }
            })
            ->addColumn('price', function ($sale) {
                if ($sale->data_type === 'ticket') {
                    return number_format($sale->baseTicket->price->value / 100.0, 2, ",", ".") . ' €';
                } else {
                    return $this->fetchProductSale($sale->id)->price->value . ' €';
                }
            })
            ->addColumn('actions', function ($sale) use ($pos) {
                if ($sale->data_type === 'ticket') {
                    $url = route('preview.ticket', ['ticket' => $sale->ticket_id, 'format' => 'pdf']);
                    $removeTicketRoute = route('pos::sales.ticket.storno', [
                        'pos'      => $pos,
                        'ticketId' => $sale->baseTicket->id,
                    ]);

                    return '
                        <a href="' . $url . '"
                           type="button"
                           class="btn btn-xs btn-warning"
                           target="_blank">
                            <span class="fa fa-file-pdf-o fa-fw"></span>
                            Angelkarte (PDF)                      
                        </a>
                        <button type="button"
                                class="btn btn-xs btn-danger"
                                data-target="#delete-modal"
                                data-toggle="modal"
                                data-action="' . $removeTicketRoute . '">
                            Stornieren<span class="fa fa-trash-o fa-fw"></span>
                        </button>
                    ';
                } else {
                    $productSale = $this->fetchProductSale($sale->id);
                    $route = route('imagecache', [
                        'template' => 'download',
                        'filename' => $productSale->product->attachment,
                    ]);
                    $removeTicketRoute = route('pos::sales.sale.storno', [
                        'pos'       => $pos,
                        'saleId'    => $sale->id,
                    ]);
                    $downloadBtn = '
                        <a href="' . $route . '"
                           type="button"
                           class="btn btn-xs btn-warning"
                           target="_blank">
                            <span class="fa fa-file-archive-o fa-fw"></span>
                            ZIP
                        </a>
                    ';
                    $stornoBtn = '
                        <button type="button"
                                class="btn btn-xs btn-danger"
                                data-target="#delete-modal"
                                data-toggle="modal"
                                data-action="' . $removeTicketRoute . '">
                            Stornieren<span class="fa fa-trash-o fa-fw"></span>
                        </button>
                    ';

                    return $productSale->product->attachment ? $downloadBtn.' '.$stornoBtn : $stornoBtn;
                }
            })
            ->make(true);
    }

    public function anyStornoData($pos, Request $request, TicketManager $ticketManager, ProductSaleManager $saleManager)
    {
        $this->isStorno = true;
        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));

        $tickets = $this->filterTickets($this->getResellerTickets($from, $till), $request);
        $productSales = $this->filterProductSales($this->getResellerProductSales($from, $till), $request);

        $results = $tickets->unionAll($productSales);

        return Datatables::of($results)
            ->addColumn('ticket_number', function ($sale) use ($ticketManager, $saleManager) {
                if ($sale->data_type === 'ticket') {
                    return $ticketManager->getIdentifier($sale->baseTicket);
                } else {
                    return $saleManager->getIdentifier($this->fetchProductSale($sale->id));
                }
            })
            ->addColumn('user_name', function ($sale) {
                return $sale->first_name.' '.$sale->last_name;
            })
            ->addColumn('area_name', function ($sale) {
                if ($sale->data_type === 'ticket') {
                    return $sale->baseTicket->type->area->name;
                } else {
                    $areas = $this->fetchProductSale($sale->id)->product->areas->pluck('name')->toArray();
                    return str_limit(implode(', ', $areas), 50);
                }
            })
            ->addColumn('ticket_type', function ($sale) {
                if ($sale->data_type === 'ticket') {
                    return $sale->baseTicket->type->name;
                } else {
                    return 'Produkt';
                }
            })
            ->addColumn('valid_from', function ($sale) {
                if ($sale->data_type === 'ticket') {
                    $validFrom = $sale->baseTicket->valid_from;

                    return $sale->baseTicket->type->category->type === 'HOUR'
                        ? $validFrom->format('d.m.Y - H:i')
                        : $validFrom->format('d.m.Y');
                } else {
                    $from = $this->fetchProductSale($sale->id)->product->featured_from;
                    return $from ? $from->format('d.m.Y') : null;
                }
            })
            ->addColumn('created_at', function ($sale) {
                if ($sale->data_type === 'ticket') {
                    return $sale->baseTicket->created_at->format('d.m.Y - H:i');
                } else {
                    return $this->fetchProductSale($sale->id)->created_at->format('d.m.Y - H:i');
                }
            })
            ->addColumn('price', function ($sale) {
                if ($sale->data_type === 'ticket') {
                    return number_format($sale->baseTicket->price->value / 100.0, 2, ",", ".") . ' €';
                } else {
                    return $this->fetchProductSale($sale->id)->price->value . ' €';
                }
            })
            ->addColumn('actions', function ($sale) {
                if ($sale->data_type === 'ticket') {
                    $url = route('preview.ticket', ['ticket' => $sale->ticket_id, 'format' => 'pdf']);

                    return '
                        <a href="' . $url . '"
                           type="button"
                           class="btn btn-xs btn-warning"
                           target="_blank">
                            <span class="fa fa-file-pdf-o fa-fw"></span>
                            PDF
                        </a>
                    ';
                } else {
                    $productSale = $this->fetchProductSale($sale->id);
                    $route = route('imagecache', [
                        'template' => 'download',
                        'filename' => $productSale->product->attachment,
                    ]);
                    $downloadBtn = '
                        <a href="' . $route . '"
                           type="button"
                           class="btn btn-xs btn-warning"
                           target="_blank">
                            <span class="fa fa-file-archive-o fa-fw"></span>
                            ZIP
                        </a>
                    ';

                    return $productSale->product->attachment ? $downloadBtn : '';
                }
            })
            ->make(true);
    }

    private function fetchProductSale($productSaleId)
    {
        if (isset($this->productSales[$productSaleId])) {
            return $this->productSales[$productSaleId];
        }

        return $this->productSales[$productSaleId] = ProductSale::select([
            'id',
            'first_name',
            'last_name',
            'email',
            'user_id',
            'reseller_id',
            'price_id',
            'product_id',
            'created_at',
        ])
            ->with(['user', 'reseller', 'price', 'product.areas', 'user.country'])
            ->find($productSaleId);
    }

    public function getTicketExport($pos, Request $request, $format = 'xls', TicketExportManager $exportManager)
    {
        if ($format != 'xls') {
            throw new \Exception('Invalid format parameter');
        }

        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $reseller = $this->getReseller()->id;

        $tickets = $this->getResellerBaseTickets($from, $till)->orderBy('created_at');

        $file = $exportManager->run($tickets, $format, compact('from', 'till', 'reseller'));

        return response()->download($file);
    }

    public function getProductSaleExport($pos, Request $request, $format = 'xls', ProductSaleExportManager $exportManager)
    {
        if ($format != 'xls') {
            throw new \Exception('Invalid format parameter');
        }

        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $reseller = $request->query->getInt('reseller', -1);

        $productSales = $this->getResellerProductSales($from, $till)
            ->orderBy('product_sales.created_at');

        $file = $exportManager->run($productSales, $format, compact('from', 'till', 'reseller'));

        return response()->download($file);
    }

    private function getResellerTickets($from, $till)
    {
        $query = $this->getReseller()->tickets()
            ->with(['baseTicket.price', 'baseTicket.type.area']); // eager load relations

        if ($from && $till) {
            try {
                $from = Carbon::createFromFormat('Y-m-d', $from)->startOfDay();
                $till = Carbon::createFromFormat('Y-m-d', $till)->endOfDay();

                $query->whereHas('baseTicket', function ($q) use ($from, $till) {
                    $q->whereBetween('tickets.created_at', [$from, $till]);
                });
            } catch (\InvalidArgumentException $e) {
            }
        }

        if ($this->isStorno) {
            // show storned ticket
            $query->whereHas('baseTicket', function ($q) {
                $q->whereNotNull('storno_id');
                $q->withTrashed();
            });
        } else {
            // show active ticket
            $query->whereHas('baseTicket', function ($q) {
                $q->whereNull('storno_id');
                $q->where('is_deleted', false);
            });
        }

        return $query;
    }

    private function getResellerProductSales($from, $till)
    {
        $query = ProductSale::with('user')
            ->where('reseller_id', $this->getReseller()->id);

        if ($from && $till) {
            try {
                $from = Carbon::createFromFormat('Y-m-d', $from)->startOfDay();
                $till = Carbon::createFromFormat('Y-m-d', $till)->endOfDay();

                $query->whereBetween('product_sales.created_at', [$from, $till]);
            } catch (\InvalidArgumentException $e) {
            }
        }

        if ($this->isStorno) {
            // show storned ticket
            $query->whereNotNull('storno_id');
            $query->withTrashed();
        } else {
            // show active ticket
            $query->whereNull('storno_id');
            $query->where('is_deleted', false);
        }

        return $query;
    }

    private function getResellerBaseTickets($from, $till)
    {
        $query = Ticket::whereHas('resellerTicket', function ($q) {
            $q->where('reseller_id', $this->getReseller()->id);
        });

        if ($from && $till) {
            try {
                $from = Carbon::createFromFormat('Y-m-d', $from)->startOfDay();
                $till = Carbon::createFromFormat('Y-m-d', $till)->endOfDay();

                $query->whereBetween('tickets.created_at', [$from, $till]);
            } catch (\InvalidArgumentException $e) {
            }
        }

        if ($this->isStorno) {
            // show storned ticket
            $query->whereNotNull('storno_id');
            $query->withTrashed();
        } else {
            // show active ticket
            $query->whereNull('storno_id');
            $query->where('is_deleted', false);
        }

        return $query;
    }

    private function filterTickets($tickets, Request $request)
    {
        $tickets
            ->selectRaw(implode(', ', $this->ticketSelect))
            ->orderBy('ticket_id', 'desc')
            ->join('tickets', 'tickets.id', '=', 'reseller_tickets.ticket_id')
            ->join('ticket_types', 'ticket_types.id', '=', 'tickets.ticket_type_id')
            ->join('areas', 'areas.id', '=', 'ticket_types.area_id');

        if ($request->has('search') && $request->search['value']) {
            $tickets->where(function ($query) use ($request) {
                $query
                    ->whereRaw(
                        "CONCAT_WS(' ',first_name,last_name) LIKE ?",
                        ['%'.$request->search['value'].'%']
                    )
                    ->orWhere('areas.name', 'LIKE', "%".$request->search['value']."%")
                    ->orWhere('fisher_id', 'LIKE', $request->search['value']."%");
                if (preg_match(Ticket::IDENT_REGEX, $request->search['value'], $matches)) {
                    $ticketId = (int) $matches[2];
                    $query->orWhere('ticket_id', 'LIKE', "$ticketId%")->having('data_type', 'ticket');
                }
            });
        }

        return $tickets;
    }

    private function filterProductSales($productSales, Request $request)
    {
        $productSales
            ->selectRaw(implode(',', $this->productSaleSelect))
            ->leftJoin('users', 'users.id', '=', 'product_sales.user_id')
            ->groupBy('product_sales.id')
            ->orderBy('product_sales.id', 'desc')
            ->leftJoin('has_products', function ($query) {
                $query->on('has_products.product_id', '=', 'product_sales.product_id')
                    ->where('has_products.related_type', 'areas');
            })
            ->leftJoin('areas', 'areas.id', '=', 'has_products.related_id');

        if ($request->has('search') && $request->search['value']) {
            $productSales->where(function ($query) use ($request) {
                $query
                    ->whereRaw(
                        "CONCAT_WS(' ',product_sales.first_name,product_sales.last_name) LIKE ?",
                        ['%'.$request->search['value'].'%']
                    )
                    ->orWhere('areas.name', 'LIKE', "%".$request->search['value']."%")
                    ->orWhere('fisher_id', 'LIKE', $request->search['value']."%");
                if (preg_match(ProductSale::IDENT_REGEX, $request->search['value'], $matches)) {
                    $ticketId = (int) $matches[2];
                    $query->orWhere('ticket_id', 'LIKE', "$ticketId%")->having('data_type', 'product');
                }
            });
        }

        return $productSales;
    }

    public function softTicketDelete($pos, $ticketId, Request $request, TicketManager $ticketManager)
    {
        if (!$ticket = Ticket::find($ticketId)) {
            return response()->json([
                'notification' => 'danger',
                'message' => 'Ticket wurde nicht gefunden',
            ]);
        }

        try {
            $ticketManager->createStornoTicket($ticket, $request->get('reason', ''));
            Event::fire(new TicketStorned($ticket));

            return response()->json([
                'notification' => 'success',
                'message' => 'Ticket wurde gelöscht',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'notification' => 'danger',
                'message' => 'Ticket konnte nicht gelöscht werden',
            ]);
        }
    }

    public function softProductSaleDelete($pos, $saleId, Request $request, ProductSaleManager $saleManager)
    {
        if (!$sale = ProductSale::find($saleId)) {
            return response()->json([
                'notification' => 'danger',
                'message' => 'Verkauf wurde nicht gefunden',
            ]);
        }

        try {
            $saleManager->createStornoSale($sale, $request->get('reason', ''));

            return response()->json([
                'notification' => 'success',
                'message' => 'Verkauf wurde gelöscht',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'notification' => 'danger',
                'message' => 'Verkauf konnte nicht gelöscht werden',
            ]);
        }
    }
}
