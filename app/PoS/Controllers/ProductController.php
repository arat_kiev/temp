<?php

namespace App\PoS\Controllers;

use App\Api1\Controllers\Reseller\ProductSaleController as ResellerProductSaleController;
use App\Api1\Requests\ResellerProductSaleRequest;
use App\Managers\ProductSaleManager;
use App\Models\Contact\Reseller;
use App\Models\Location\Country;
use App\Models\Product\Product;
use App\Models\Product\ProductSale;
use App\Models\Ticket\ResellerTicket;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Datatables;
use Html;

class ProductController extends PoSController
{
    public function getIndex($pos, Request $request)
    {
        return view('pos.product.index', $this->indexData($request));
    }
    
    public function getShow($pos, $productId)
    {
        $product = Product::with(['prices', 'areas', 'gallery.pictures'])->findOrFail($productId);

        $this->checkResellerProductAccess($product);

        $countries = Country::all();

        $reseller = $this->getReseller();
        $resellerCountryId = $reseller->country ? $reseller->country->id : 1;
        
        return view('pos.product.show', compact('product', 'countries', 'resellerCountryId'));
    }

    private function checkResellerProductAccess(Product $product)
    {
        $resellerId = $this->getReseller()->id;

        switch (true) {
            case (!in_array($resellerId, $product->resellers()->pluck('id')->toArray())):
                throw new AccessDeniedHttpException('Keine Berechtigung für dieses Produkt');
            case (!$this->checkProductAccessPerAreas($product)):
                throw new AccessDeniedHttpException(
                    'Das Produkt bezieht sich auf die Gewässer, auf die der Verkäufer keinen Zugriff hat'
                );
        }
    }

    private function checkProductAccessPerAreas(Product $product)
    {
        $productAreas = $product->areas()->pluck('id')->toArray();

        if (!$productAreas) {
            return true;
        }

        return array_intersect($this->getReseller()->areas()->pluck('id')->toArray(), $productAreas)
            || in_array($this->getReseller()->id, $product->resellers()->pluck('id')->toArray());
    }

    public function postNewSale($pos, ResellerProductSaleRequest $request, ProductSaleManager $saleManager)
    {
        return (new ResellerProductSaleController(new Reseller()))
            ->store($request, $saleManager, $this->getReseller()->id);
    }

    public function getProductAttachment($pos, $saleId)
    {
        $reseller = $this->getReseller();
        $sale = ProductSale::findOrFail($saleId);

        if ($sale->reseller_id !== $reseller->id) {
            throw new AccessDeniedHttpException('You have not permission to this purchase');
        }
        if (!$attachment = $sale->product->attachment) {
            throw new NotFoundHttpException('No attachment found for selected product');
        }

        return route('imagecache', ['template' => 'download', 'filename' => $attachment]);
    }

    public function anyData($pos, Request $request)
    {
        $products = $this->fetchProducts($request->query->getInt('area', -1))
            ->select(['products.id', 'featured_from', 'featured_till', 'gallery_id', 'attachment'])
            ->leftJoin('product_translations', 'product_translations.product_id', '=', 'products.id')
            ->where('product_translations.locale', '=', \App::getLocale())
            ->with(['category', 'prices', 'areas'])
            ->groupBy('products.id');

        return Datatables::of($products)
            ->editColumn('name', function ($product) use ($pos) {
                return Html::linkRoute(
                    'pos::products.show',
                    str_limit($product->name, 50),
                    ['productId' => $product->id, 'pos' => $pos]
                );
            })
            ->editColumn('featured_from', function ($product) {
                return $product->featured_from ? $product->featured_from->format('d.m.Y') : '';
            })
            ->editColumn('featured_till', function ($product) {
                return $product->featured_till ? $product->featured_till->format('d.m.Y') : '';
            })
            ->addColumn('price', function ($product) {
                $price = $product->prices()
                    ->where('valid_from', '<', Carbon::now())
                    ->where('valid_till', '>', Carbon::now())
                    ->first();

                return $price ? "$price->value €" : '';
            })
            ->addColumn('attachment', function ($product) {
                return $product->attachment
                    ? '<span class="fa fa-fw fa-check"></span>'
                    : '<span class="fa fa-fw fa-times"></span>';
            })
            ->removeColumn('category', 'prices', 'short_description', 'long_description')
            ->blacklist(['price_value', 'gallery'])
            ->make(true);
    }

    private function indexData(Request $request) : array
    {
        $area = $request->query->getInt('area', -1);
        $products = $this->fetchProducts($area);

        $count = $products->count();

        $areaList = collect([
            '-1'    => 'Alle',
            '0'     => 'Alle mit Gewässer',
        ]);

        $areas = $this->getReseller()->areas()
            ->select(['id', 'name'])
            ->without(['rule', 'gallery.pictures', 'ticketTypes', 'resellers', 'locations'])
            ->each(function ($area) use ($areaList) {
                $areaList[$area->id] = $area->name;
            });

        $areaList->merge($areas);

        return compact('count', 'area', 'areaList');
    }

    private function fetchProducts(int $area = -1)
    {
        $now = Carbon::now();

        $products = Product::with(['areas', 'managers', 'prices'])
            ->where(function ($query) use ($now) {
                $query->whereNull('featured_from')
                    ->orWhere('featured_from', '<=', $now);
            })
            ->where(function ($query) use ($now) {
                $query->whereNull('featured_till')
                    ->orWhere('featured_till', '>=', $now);
            })
            ->where(function ($query) {
                $query->doesntHave('areas')
                    ->orWhereHas('areas', function ($q) {
                        $q->whereIn('id', $this->getReseller()->areas()->pluck('id')->toArray());
                    })
                    ->orWhereHas('resellers', function ($query) {
                        $query->whereId($this->getReseller()->id);
                    });
            })
            ->whereHas('prices', function ($query) use ($now) {
                $query->where('valid_from', '<=', $now)
                    ->where('valid_till', '>=', $now);
            });

        if ($area > 0) {
            $products->whereHas('areas', function ($query) use ($area) {
                $query->where('id', $area);
            });
        } elseif ($area === 0) {
            $products->has('areas');
        }

        return $products;
    }
}
