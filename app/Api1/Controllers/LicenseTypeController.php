<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\LicenseTypeTransformer;
use App\Http\Controllers\Controller;
use App\Models\License\LicenseType;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class LicenseTypeController extends Controller
{
    /** @var LicenseType */
    private $licenseTypeRepository;

    public function __construct(LicenseType $licenseTypeRepository)
    {
        $this->licenseTypeRepository = $licenseTypeRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $typeQuery = $this->parseRequest($request);

        $paginator = $typeQuery
            ->paginate($request->get('_perPage', $this->licenseTypeRepository->getPerPage()))
            ->appends($request->except('page'));

        $licenseTypes = $paginator->items();

        $data = fractal()
            ->collection($licenseTypes, new LicenseTypeTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $licenseType = $this->licenseTypeRepository->findOrFail($id);

        $data = fractal()
            ->item($licenseType, new LicenseTypeTransformer(true))
            ->parseIncludes(['examples'])
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseRequest(Request $request)
    {
        $typeQuery = $this->licenseTypeRepository
            ->addSelect(['license_types.*']);

        // handle sort parameters
        $sortField = $request->query->get('_sortField', 'id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        $typeQuery->orderBy($sortField, $sortDir);

        // filter by name
        if ($request->query->has('name')) {
            $name = $request->query->get('name');
            $typeQuery->where('name', 'like', '%' . $name . '%');
        }

        // filter by state
        if ($request->query->has('state')) {
            $state = $request->query->getInt('state');
            $typeQuery->hasState($state);
        }

        // filter by ids
        if ($request->query->has('license_type_id')) {
            $license_type_ids = $request->query->get('license_type_id');
            $typeQuery->whereIn('license_types.id', $license_type_ids);
        }

        return $typeQuery;
    }
}
