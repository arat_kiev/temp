<?php

namespace App\Api1\Controllers;

use App\Http\Controllers\Controller;
use App\Models\LandingPages\LandingPage;
use Illuminate\Http\Request;
use App\Api1\Transformers\LandingPageTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class LandingPageController extends Controller
{
    /** @var LandingPage */
    private $lpRepository;

    public function __construct(LandingPage $lpRepository)
    {
        $this->lpRepository = $lpRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = $this->lpRepository->with('translations')->paginate();
        // $lp = array_filter($paginator->items(), function($item) {
        //     return $item->slug;
        // });
        $lp = $paginator->items();

        $data = fractal()
            ->collection($lp, new LandingPageTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lp = $this->lpRepository->with('translations')->findOrFail($id);

        if (!$lp->slug) {
            abort(404);
        }

        $data = fractal()
            ->item($lp, new LandingPageTransformer(true))
            ->parseIncludes('area')
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}