<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\QuotaUnitTransformer;
use App\Http\Controllers\Controller;
use App\Models\Ticket\QuotaUnit;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class QuotaUnitController extends Controller
{
    /** @var \App\Models\Ticket\QuotaUnit */
    private $quotaRepository;

    /**
     * @param \App\Models\Ticket\QuotaUnit $quotaRepository
     */
    public function __construct(QuotaUnit $quotaRepository)
    {
        $this->quotaRepository = $quotaRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = $this->quotaRepository->paginate();
        $quotaUnits = $paginator->items();

        $data = fractal()
            ->collection($quotaUnits, new QuotaUnitTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        throw new MethodNotAllowedHttpException(['GET']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $quotaUnit = $this->quotaRepository->findOrFail($id);

        $data = fractal()
            ->item($quotaUnit, new QuotaUnitTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        throw new MethodNotAllowedHttpException(['GET']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        throw new MethodNotAllowedHttpException(['GET']);
    }
}
