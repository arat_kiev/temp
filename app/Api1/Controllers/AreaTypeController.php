<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\AreaTypeTransformer;
use App\Http\Controllers\Controller;
use App\Models\Area\AreaType;
use Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class AreaTypeController extends Controller
{
    /** @var \App\Models\Area\AreaType */
    private $areaTypeRepository;

    /**
     * @param \App\Models\Area\AreaType $areaTypeRepository
     */
    public function __construct(AreaType $areaTypeRepository)
    {
        $this->areaTypeRepository = $areaTypeRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $typeQuery = $this->parseRequest($request);
        $cacheKey = $this->getIndexCacheKey($request);

        $areaTypes = Cache::remember($cacheKey, 60, function () use ($typeQuery) {
            return $typeQuery->get();
        });

        $data = fractal()
            ->collection($areaTypes, new AreaTypeTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Generate cache key according to request params
     *
     * @param Request $request
     * @return string
     */
    private function getIndexCacheKey(Request $request)
    {
        $cacheHash = md5(json_encode([
            $request->query->get('_sortField', 'id'),
            $request->query->get('_sortDir', 'asc'),
            $request->query->get('name', ''),
            $request->query->get('area_type_id', []),
        ]));

        return implode('_', [
            'area_types',
            App::getLocale(),
            $cacheHash,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $areaType = $this->areaTypeRepository->findOrFail($id);

        $data = fractal()
            ->item($areaType, new AreaTypeTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseRequest(Request $request)
    {
        $typeQuery = $this->areaTypeRepository
            ->join('area_type_translations', 'area_types.id', '=', 'area_type_translations.area_type_id')
            ->addSelect(['area_types.*', 'area_type_translations.name']);

        // handle sort parameters
        $sortField = $request->query->get('_sortField', 'id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        $typeQuery->orderBy($sortField, $sortDir);

        // filter by name
        if ($request->query->has('name')) {
            $name = $request->query->get('name');
            $typeQuery->whereHas('translations', function ($query) use ($name) {
                $query->where('area_type_translations.name', 'like', '%' . $name . '%');
            });
        }

        // filter by ids
        if ($request->query->has('area_type_id') && is_array($request->query->get('area_type_id'))) {
            $type_ids = $request->query->get('area_type_id');
            $typeQuery->whereIn('area_types.id', $type_ids);
        }

        return $typeQuery;
    }
}
