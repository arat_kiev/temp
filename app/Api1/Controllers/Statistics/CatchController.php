<?php

namespace App\Api1\Controllers\Statistics;

use App;
use Auth;
use Cache;
use App\Http\Controllers\Controller;
use App\Api1\Requests\Statistics\CatchRequest;

use App\Models\Haul;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CatchController extends Controller
{
    /** @var Haul */
    private $haulRepository;

    /**
     * @param Haul $haulRepository
     */
    public function __construct(Haul $haulRepository)
    {
        $this->haulRepository = $haulRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  CatchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(CatchRequest $request)
    {
        $haulQuery = $this->parseRequest($request);
        $cacheKey = $this->getIndexCacheKey($request);

        try {
            $from = Carbon::createFromFormat('Y-m-d', $request->query->get('from'));
            $till = Carbon::createFromFormat('Y-m-d', $request->query->get('till'));
        } catch (\InvalidArgumentException $e) {
            throw new BadRequestHttpException();
        }

        $hauls = Cache::remember($cacheKey, 60, function () use ($haulQuery) {
            return $haulQuery->get();
        });

        /** @var App\Managers\CatchStatisticsManager $statsManager */
        $statsManager = app('App\Managers\CatchStatisticsManager', [$from, $till]);
        $data = [];

        if ($request->query->get('hour')) {
            $data = $statsManager->countPerHourOfDay($hauls);
        } else {
            switch ($request->query->get('granularity')) {
                case 'DAY':
                    $data = $statsManager->countPerDays($hauls);
                    break;
                case 'WEEK':
                    $data = $statsManager->countPerWeeks($hauls);
                    break;
                case 'MONTH':
                    $data = $statsManager->countPerMonths($hauls);
                    break;
                case 'SEASON':
                    $data = $statsManager->countPerSeasons($hauls);
                    break;
                default:
                    break;
            }
        }

        return response()->json($data);
    }

    /**
     * Generate cache key according to request params
     *
     * @param CatchRequest $request
     * @return string
     */
    private function getIndexCacheKey(CatchRequest $request)
    {
        $cacheHash = md5(json_encode([
            $request->query->get('_sortField', 'id'),
            $request->query->get('_sortDir', 'asc'),
            $request->query->get('from', ''),
            $request->query->get('till', ''),
            $request->query->get('areas', []),
            $request->query->get('techniques', []),
            $request->query->get('users', []),
            $request->query->get('fishes', []),
            $request->query->get('teams', []),
        ]));

        return implode('_', [
            'catch_statistics',
            App::getLocale(),
            $cacheHash,
        ]);
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param CatchRequest $request
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseRequest(CatchRequest $request)
    {
        $from = $request->query->get('from');
        $till = $request->query->get('till');

        $haulQuery = $this->haulRepository
            ->setEagerLoads([])
            ->addSelect(['hauls.*', 'hauls.id as id'])
            ->whereBetween('catch_date', array($from, $till))
            ->isEmptyReport(false);

        // handle sort parameters
        $sortField = $request->query->get('_sortField', 'id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        $haulQuery->orderBy($sortField, $sortDir);

        // filter by area ids
        if ($request->query->has('areas')) {
            $areaIds = $request->query->get('areas');
            $haulQuery->whereIn('hauls.area_id', $areaIds);
        }

        // filter by technique ids
        if ($request->query->has('techniques')) {
            $techniqueIds = $request->query->get('techniques');
            $haulQuery->whereIn('hauls.technique_id', $techniqueIds);
        }

        // filter by user ids
        if ($request->query->has('users')) {
            $userIds = $request->query->get('users');
            $haulQuery->whereIn('hauls.user_id', $userIds);
        }

        // filter by fish ids
        if ($request->query->has('fishes')) {
            $fishIds = $request->query->get('fishes');
            $haulQuery->whereIn('hauls.fish_id', $fishIds);
        }

        // filter by team ids
        /* TODO: Implement after "Fishing teams" will be implemented
        if ($request->query->has('teams')) {
            $teamIds = $request->query->get('teams');
            $haulQuery->hasTeam($teamIds);
        }
        */

        return $haulQuery;
    }
}
