<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\RoleTransformer;
use App\Http\Controllers\Controller;
use App\Models\Authorization\Role;
use Auth;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class RoleController extends Controller
{
    /** @var Role */
    private $roleRepository;

    /**
     * @param Role $roleRepository
     */
    public function __construct(Role $roleRepository)
    {
        $this->roleRepository = $roleRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!Auth::user()->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        }

        $roleQuery = $this->parseRequest($request);

        $paginator = $roleQuery
            ->paginate($request->get('_perPage', $this->roleRepository->getPerPage()))
            ->appends($request->except('page'));

        $roles = $paginator->items();

        $data = fractal()
            ->collection($roles, new RoleTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $includes = ['permissions'];

        $role = $this->roleRepository->with($includes)->findOrFail($id);

        $data = fractal()
            ->item($role, new RoleTransformer())
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function parseRequest(Request $request)
    {
        $roleQuery = $this->roleRepository
            ->addSelect(['roles.*', 'roles.id as id']);

        if ($request->query->has('search')) {
            $search = $request->query->get('search');
            $roleQuery->where('name', 'like', '%' . $search . '%');
        }

        // filter by ids
        if ($request->query->has('role_id')) {
            $role_ids = $request->query->get('role_id');
            $roleQuery->whereIn('roles.id', $role_ids);
        }

        return $roleQuery;
    }
}
