<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\StateTransformer;
use App\Http\Controllers\Controller;
use App\Models\Location\State;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class StateController extends Controller
{
    /** @var \App\Models\Location\State */
    private $stateRepository;

    /**
     * @param \App\Models\Location\State $stateRepository
     */
    public function __construct(State $stateRepository)
    {
        $this->stateRepository = $stateRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = $this->parseRequest($request);

        $paginator = $query->with(['regions'])
            ->paginate($request->get('_perPage', $this->stateRepository->getPerPage()))
            ->appends($request->except('page'));

        $states = $paginator->items();
        $detailed = $request->query->getBoolean('detailed');

        $data = fractal()
            ->collection($states, new StateTransformer($detailed))
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $includes = ['regions'];

        $state = $this->stateRepository->with($includes)->findOrFail($id);

        $data = fractal()
            ->item($state, new StateTransformer(true))
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseRequest(Request $request)
    {
        $query = $this->stateRepository
            ->addSelect(['states.*', 'states.id as id']);;

        // handle sort parameters
        if ($request->query->has('_sortField') && $request->query->has('_sortDir')) {
            $sortField = $request->query->get('_sortField', 'id');
            $sortDir = $request->query->get('_sortDir', 'asc');

            $query->orderBy($sortField, $sortDir);
        }

        // Filter by name
        if ($request->query->has('name') && is_string($request->query->get('name'))
            || $request->query->has('search') && is_string($request->query->get('search'))) {
            $name = $request->query->get('name', $request->query->get('search', ''));
            $query->whereTranslationLike('name', "%$name%");
        }

        // filter by ids
        if ($request->query->has('ids') && is_array($request->query->get('ids'))) {
            $ids = $request->query->get('ids', []);
            $query->whereIn('id', $ids);
        }

        return $query;
    }
}
