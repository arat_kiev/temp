<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\CityTransformer;
use App\Http\Controllers\Controller;
use App\Models\Location\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /** @var \App\Models\Location\City */
    private $cityRepository;

    /**
     * @param \App\Models\Location\City $cityRepository
     */
    public function __construct(City $cityRepository)
    {
        $this->cityRepository = $cityRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $city = $this->cityRepository->findOrFail($id);

        $data = fractal()
            ->item($city, new CityTransformer(true))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
