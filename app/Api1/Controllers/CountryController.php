<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\CountryTransformer;
use App\Http\Controllers\Controller;
use App\Models\Location\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /** @var \App\Models\Location\Country */
    private $countryRepository;

    /**
     * @param \App\Models\Location\Country $countryRepository
     */
    public function __construct(Country $countryRepository)
    {
        $this->countryRepository = $countryRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = $this->countryRepository->all();

        $data = fractal()
            ->collection($countries, new CountryTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $includes = ['states'];

        $country = $this->countryRepository->with($includes)->findOrFail($id);

        $data = fractal()
            ->item($country, new CountryTransformer(true))
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
