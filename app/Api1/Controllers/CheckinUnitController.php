<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\CheckinUnitTransformer;
use App\Http\Controllers\Controller;
use App\Models\Ticket\CheckinUnit;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class CheckinUnitController extends Controller
{
    /** @var \App\Models\Ticket\CheckinUnit */
    private $checkinRepository;

    /**
     * @param \App\Models\Ticket\CheckinUnit $checkinRepository
     */
    public function __construct(CheckinUnit $checkinRepository)
    {
        $this->checkinRepository = $checkinRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = $this->checkinRepository->paginate();
        $checkinUnits = $paginator->items();

        $data = fractal()
            ->collection($checkinUnits, new CheckinUnitTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        throw new MethodNotAllowedHttpException(['GET']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $checkinUnit = $this->checkinRepository->findOrFail($id);

        $data = fractal()
            ->item($checkinUnit, new CheckinUnitTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        throw new MethodNotAllowedHttpException(['GET']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        throw new MethodNotAllowedHttpException(['GET']);
    }
}
