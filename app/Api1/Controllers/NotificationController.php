<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\NotificationTransformer;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class NotificationController extends Controller
{
    /** @var Notification */
    private $notifRepository;

    /** @var int archive notifications expiration time, in days */
    private $ttlArchive = 7;

    public function __construct(Notification $notifRepository)
    {
        $this->notifRepository = $notifRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the new notifications.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $notifQuery = $this->parseRequest($request)->read();

        $paginator = $notifQuery
            ->paginate($request->get('_perPage', $this->notifRepository->getPerPage()))
            ->appends($request->except('page'));

        $notifs = $paginator->items();

        $data = fractal()
            ->collection($notifs, new NotificationTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->addMeta(['links' => ['archive' => route('v1.index.archive.notifs')]])
            ->toArray();

        // mark retrieved notifications as `read`
        $this->notifRepository->whereIn('id', NotificationTransformer::$read)->update(['read_at' => Carbon::now()]);

        return response()->json($data);
    }

    /**
     * Display a listing of the old notifications.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexArchive(Request $request)
    {
        // removing archived notifications which are older than $this->ttlArchive days
        $this->notifRepository->where('read_at', '<', Carbon::now()->subDays($this->ttlArchive))->delete();
        $notifQuery = $this->parseRequest($request)->read(true);

        $paginator = $notifQuery
            ->paginate($request->get('_perPage', $this->notifRepository->getPerPage()))
            ->appends($request->except('page'));

        $notifs = $paginator->items();

        $data = fractal()
            ->collection($notifs, new NotificationTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->addMeta(['links' => ['unread' => route('v1.index.notifs')]])
            ->toArray();

        return response()->json($data);
    }

    private function parseRequest(Request $request)
    {
        $notifQuery = $this->notifRepository->where('user_id', Auth::id());

        // Filter by notification type
        if ($request->query->has('type')) {
            $types = (array)$request->query->get('type');
            $notifQuery->hasTypes($types);
        }

        return $notifQuery;
    }
}
