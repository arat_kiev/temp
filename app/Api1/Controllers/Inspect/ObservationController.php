<?php

namespace App\Api1\Controllers\Inspect;

use App\Admin\Controllers\Inspect\BaseInspectController as BaseController;
use App\Api1\Requests\ObservationRequest;
use App\Models\Inspection\Observation;
use App\Models\User;
use App\Models\Inspection\InspectionTour;
use App\Api1\Transformers\Inspection\ObservationTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class ObservationController extends BaseController
{
    /**
     * @var User
     */
    private $inspector;

    /**
     * ObservationController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->inspector = $this->getInspector();
    }

    /**
     * Get inspector's observations
     *
     * @param ObservationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getObservations(ObservationRequest $request)
    {
        $observationPaginator = $this->parseRequest($request, true);

        $data = fractal()
            ->collection($observationPaginator->items(), new ObservationTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($observationPaginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Create observation for inspection
     *
     * @param ObservationRequest $request
     * @param $tourId
     * @return \Illuminate\Http\JsonResponse
     */
    public function newObservation(ObservationRequest $request, $tourId)
    {
        $inspectionTour = InspectionTour::findOrFail($tourId);

        $observation = Observation::create($request->all());
        $inspectionTour->observations()->save($observation);

        $data = fractal()
            ->item($observation, new ObservationTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update inspection observation
     *
     * @param ObservationRequest $request
     * @param $observationId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateObservation(ObservationRequest $request, $observationId)
    {
        $observation = Observation::findOrFail($observationId);
        $observation->update($request->all());

        $data = fractal()
            ->item($observation, new ObservationTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Parse observations request
     * Requested date params: `from` (date format Y-m-d H:i:s), `till` (date format Y-m-d H:i:s)
     *
     * @param ObservationRequest $request
     * @param bool $paginated
     * @return mixed
     */
    public function parseRequest(ObservationRequest $request, bool $paginated = false)
    {
        $query = $this->inspector->observations()->orderBy('id', 'desc')->where(function ($query) use ($request) {
            if ($request->has('from') && $request->has('till')) {
                $query->whereBetween('observations.created_at', [$request->from, $request->till]);
            }
        });

        $query = $paginated ? $query->paginate($request->perPage) : $query->get();

        return $query;
    }
}