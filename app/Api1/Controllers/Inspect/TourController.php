<?php

namespace App\Api1\Controllers\Inspect;

use App\Admin\Controllers\Inspect\BaseInspectController as BaseController;
use App\Api1\Requests\InspectionTourRequest;
use App\Api1\Transformers\Inspection\InspectionTourTransformer;
use App\Models\Inspection\InspectionTour;
use Carbon\Carbon;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class TourController extends BaseController
{
    /**
     * @var User
     */
    private $inspector;

    /**
     * @var
     */
    private $inspections;

    /**
     * ObservationController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->inspector = $this->getInspector();
        $this->inspections = $this->inspector->ticketInspections();
    }

    /**
     * Get Inspector's tours
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTours()
    {
        $inspectionTours = $this->inspector->inspectionTours()
            ->orderBy('id', 'desc')
            ->paginate();

        $data = fractal()
            ->collection($inspectionTours, new InspectionTourTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($inspectionTours))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Set new inspection tour
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function newTour()
    {
        $inspectionTour = InspectionTour::create(['inspector_id' => $this->inspector->id]);

        $data = fractal()
            ->item($inspectionTour, new InspectionTourTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Close inspection tour
     *
     * @param InspectionTourRequest $request
     * @param $tourId
     * @return \Illuminate\Http\JsonResponse
     */
    public function endTour($tourId)
    {
        $inspectionTour = InspectionTour::findOrFail($tourId);
        $inspectionTour->update(['updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);

        $data = fractal()
            ->item($inspectionTour, new InspectionTourTransformer())
            ->toArray();

        return response()->json($data);
    }
}