<?php

namespace App\Api1\Controllers\Inspect;

use App\Admin\Controllers\Inspect\BaseInspectController as BaseController;
use App\Api1\Requests\AreaInspectionRequest;
use App\Api1\Transformers\AreaTransformer;
use App\Models\User;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class AreaController extends BaseController
{
    /**
     * @var User
     */
    private $inspector;

    /**
     * @var
     */
    private $areas;

    /**
     * InspectorController constructor.
     *
     * @param User $userRepository
     */
    public function __construct()
    {
        parent::__construct();

        $this->inspector = $this->getInspector();
        $this->areas = $this->inspector->inspectedAreas();
    }

    /**
     * Get inspector's inspections
     *
     * @param TicketInspectionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAreas(AreaInspectionRequest $request)
    {
        $areasPaginator = $this->parseAreasRequest($request, true);

        $data = fractal()
            ->collection($areasPaginator->items(), new AreaTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($areasPaginator))
            ->toArray();

        return response()->json($data);
    }


    /**
     * Get inspector's areas
     *
     * @param AreaInspectionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInspectionAreas(AreaInspectionRequest $request)
    {
        $perPage = $request->has('perPage') ? $request->perPage : 15;
        $areasRequest = $this->inspector->inspectedAreas()->orderBy('id', 'desc')->paginate($perPage);

        $data = fractal()
            ->collection($areasRequest, new AreaTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($areasRequest))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Parse areas request
     * Requested date params: `from` (date format Y-m-d H:i:s), `till` (date format Y-m-d H:i:s)
     *
     * @param AreaInspectionRequest $request
     * @param bool $paginated
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Eloquent\Relations\HasMany[]
     */
    public function parseAreasRequest(AreaInspectionRequest $request, bool $paginated = false)
    {
        $query = $this->areas->orderBy('id', 'desc')->where(function ($query) use ($request) {
            if ($request->has('name')) {
                $query->where('name', 'LIKE', '%' . $request->name . '%');
            }
        });

        $perPage = $request->has('perPage') ? $request->perPage : 15;

        $query = $paginated ? $query->paginate($perPage) : $query->get();

        return $query;
    }
}
