<?php

namespace App\Api1\Controllers\Inspect;

use App\Admin\Controllers\Inspect\BaseInspectController as BaseController;
use App\Api1\Requests\TicketInspectionRequest;
use App\Models\User;
use App\Api1\Transformers\Inspection\TicketInspectionTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class InspectionController extends BaseController
{
    /**
     * @var User
     */
    private $inspector;

    /**
     * @var
     */
    private $inspections;

    /**
     * InspectorController constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->inspector = $this->getInspector();
        $this->inspections = $this->inspector->ticketInspections();
    }

    /**
     * Get inspector's inspections
     *
     * @param TicketInspectionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInspections(TicketInspectionRequest $request)
    {
        $inspectionsPaginator = $this->parseRequest($request, true);

        $data = fractal()
            ->collection($inspectionsPaginator->items(), new TicketInspectionTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($inspectionsPaginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Parse inspections request
     * Requested date params: `from` (date format Y-m-d H:i:s), `till` (date format Y-m-d H:i:s)
     *
     * @param TicketInspectionRequest $request
     * @param bool $paginated
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Eloquent\Relations\HasMany[]
     */
    public function parseRequest(TicketInspectionRequest $request, bool $paginated = false)
    {
        $query = $this->inspections->orderBy('id', 'desc')->where(function ($query) use ($request) {
            if ($request->has('from') && $request->has('till')) {
                $query->whereBetween('created_at', [$request->from, $request->till]);
            }
        });

        $query = $paginated ? $query->paginate($request->perPage) : $query->get();

        return $query;
    }
}
