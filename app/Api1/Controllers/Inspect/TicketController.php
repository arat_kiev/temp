<?php

namespace App\Api1\Controllers\Inspect;

use App\Admin\Controllers\Inspect\BaseInspectController as BaseController;
use App\Api1\Requests\TicketInspectionRequest;
use App\Api1\Transformers\AreaTransformer;
use App\Api1\Transformers\TicketTransformer;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketInspection;
use App\Api1\Transformers\Inspection\InspectionTourTransformer;
use App\Api1\Transformers\Inspection\TicketInspectionTransformer;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class TicketController extends BaseController
{
    private $ticketRepository;

    private $inspector;

    /**
     * TicketController constructor.
     *
     * @param Ticket $ticketRepository
     */
    public function __construct(Ticket $ticketRepository)
    {
        parent::__construct();
        $this->ticketRepository = $ticketRepository;
        $this->inspector = $this->getInspector();

        if (is_null($this->inspector)) {
            throw new AccessDeniedHttpException();
        }
    }

    /**
     * Start new Inspection
     *
     * @param Request $request
     * @param $ticketId
     * @return \Illuminate\Http\JsonResponse
     */
    public function newInspection(TicketInspectionRequest $request, $ticketId)
    {
        $ticket = $this->getTicket($ticketId);

        $inspection = TicketInspection::create([
            'ticket_id' => $ticket->id,
            'tour_id' => $request->tour_id,
            'gallery_id' => $request->gallery_id,
            'status' => $request->status,
            'comment' => $request->comment,
            'location' => $request->location
        ]);

        $data = fractal()
            ->item($inspection, new TicketInspectionTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update ticket inspection
     *
     * @param TicketInspectionRequest $request
     * @param $inspectionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateInspection(TicketInspectionRequest $request, $inspectionId)
    {
        $ticketInspection = TicketInspection::findOrFail($inspectionId);

        $ticketInspection->update([
            'gallery_id' => $request->gallery_id,
            'status' => $request->status,
            'comment' => $request->comment,
            'location' => $request->location
        ]);

        $data = fractal()
            ->item($ticketInspection, new TicketInspectionTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Get Inspections
     *
     * @param $ticketId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllInspections($ticketId)
    {
        $ticket = $this->getTicket($ticketId);

        $data = fractal()
            ->collection($ticket->inspections, new InspectionTourTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Get inspection
     *
     * @param $ticketId
     * @param $inspectionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInspection($ticketId, $inspectionId)
    {
        $ticket = $this->getTicket($ticketId);

        $inspection = $ticket->inspections->reject(function ($inspection) use ($inspectionId) {
            return $inspection->id === $inspectionId;
        });

        $data = fractal()
            ->collection($inspection, new InspectionTourTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Get ticket
     *
     * @param $ticketId
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private function getTicket($ticketId)
    {
        return $this->ticketRepository->findOrFail($ticketId);
    }

    /**
     * Get inspected Area tickets
     *
     * @param TicketInspectionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInspectionAreasTickets(TicketInspectionRequest $request)
    {
        $areaPaginator = $this->parseRequest($request);
        $perPage = $request->perPage ? $request->perPage : 15;
        $ticketsPaginator = $areaPaginator->tickets()->inspectionTickets($request->valid_from, $request->valid_to)->paginate($perPage);

        $data = fractal()
            ->collection($ticketsPaginator, new TicketTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($ticketsPaginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Parse request
     *
     * @param TicketInspectionRequest $request
     * @param bool $paginated
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function parseRequest(TicketInspectionRequest $request, bool $paginated = false)
    {
        $query = $this->inspector->inspectedAreas()->where(function ($query) use ($request) {
            $query->where('id', $request->area_id);
        });

        $query = $query->first();

        return $query;
    }
}
