<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\AreaRatingCategoryTransformer;
use App\Http\Controllers\Controller;
use App\Models\RatingCategory;
use Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class AreaRatingCategoryController extends Controller
{
    /** @var RatingCategory */
    private $categoryRepository;

    /**
     * @param RatingCategory $categoryRepository
     */
    public function __construct(RatingCategory $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cacheKey = $this->getIndexCacheKey();
        $catQuery = $this->categoryRepository->listsTranslations('name');

        $categories = Cache::remember($cacheKey, 60, function () use ($catQuery) {
            return $catQuery->get();
        });

        $data = fractal()
            ->collection($categories, new AreaRatingCategoryTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Generate cache key according to request params
     *
     * @param Request $request
     * @return string
     */
    private function getIndexCacheKey()
    {
        return implode('_', [
            'area_rating_category',
            App::getLocale(),
        ]);
    }
}
