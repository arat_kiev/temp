<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\ManagerTransformer;
use App\Api1\Requests\StoreManagerRequest;
use App\Http\Controllers\Controller;
use App\Models\Contact\Manager;
use Carbon\Carbon;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use View;

class ManagerController extends Controller
{
    /** @var Manager */
    private $managerRepository;

    /**
     * @param Manager $managerRepository
     */
    public function __construct(Manager $managerRepository)
    {
        $this->managerRepository = $managerRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $managerQuery = $this->parseRequest($request);

        $paginator = $managerQuery->with('logo')
            ->paginate($request->get('_perPage', $this->managerRepository->getPerPage()))
            ->appends($request->except('page'));

        $managers = $paginator->items();

        $data = fractal()
            ->collection($managers, new ManagerTransformer())
            ->includeLogo()
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreManagerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreManagerRequest $request)
    {
        $client = config('app.client')->name;
        $defaultClient = 'bissanzeiger';
        $data = array_merge(
            $request->all(),
            [
                'templates' => [
                    'pdf' => [
                        'ticket'    => View::exists($client.'.pdf.ticket')
                            ? $client.'.pdf.ticket'
                            : $defaultClient.'.pdf.ticket',
                        'product'   => View::exists($client.'.pdf.product')
                            ? $client.'.pdf.product'
                            : $defaultClient.'.pdf.product',
                        'invoice'   => View::exists($client.'.pdf.invoice')
                            ? $client.'.pdf.invoice'
                            : $defaultClient.'.pdf.invoice',
                    ],
                ],
            ]
        );

        $this->managerRepository->saveAsDraft($data);

        return response()->json([], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $includes = ['logo', 'organizations', 'products', 'signature'];
        $manager = $this->managerRepository->with($includes)->findOrFail($id);

        $data = fractal()
            ->item($manager, new ManagerTransformer(true))
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function parseRequest(Request $request)
    {
        $managerQuery = $this->managerRepository
            ->addSelect(['managers.*', 'managers.id as id']);

        if ($request->query->has('search') && is_string($request->query->get('search'))) {
            $search = $request->query->get('search');
            $managerQuery->where('name', 'like', '%' . $search . '%');
        }

        // filter by ids
        if ($request->query->has('manager_id') && is_array($request->query->get('manager_id'))) {
            $managerIds = $request->query->get('manager_id');
            $managerQuery->whereIn('managers.id', $managerIds);
        }

        // filter by country id
        if ($request->query->has('country')) {
            $countryId = $request->query->getInt('country');
            $managerQuery->whereHas('country', function ($query) use ($countryId) {
                $query->where('id', $countryId);
            });
        }

        // filter by state ids
        if ($request->query->has('states') && is_array($request->query->get('states'))) {
            $states = $request->query->get('states');
            $managerQuery->whereHas('country.states', function ($query) use ($states) {
                $query->whereIn('id', $states);
            });
        }

        // filter by ticket ids
        if ($request->query->has('tickets') && is_array($request->query->get('tickets'))) {
            $tickets = $request->query->get('tickets');
            $managerQuery->whereHas('areas.ticketTypes.tickets', function ($query) use ($tickets) {
                $query->whereIn('id', $tickets);
            });
        }

        // filter by active ticket prices
        if ($request->query->has('has_ticket_prices')) {
            $hasTicketPrices = $request->query->getBoolean('has_ticket_prices');
            $managerQuery->join('areas', 'areas.manager_id', '=', 'managers.id')
                ->leftJoin('ticket_types', 'ticket_types.area_id', '=', 'areas.id')
                ->leftJoin('ticket_prices', 'ticket_prices.ticket_type_id', '=', 'ticket_types.id');

            $now = Carbon::now()->toDateString();
            if ($hasTicketPrices) {
                $managerQuery->whereRaw('? BETWEEN ticket_prices.visible_from AND ticket_prices.valid_to', [$now]);
            } else {
                $managerQuery->whereRaw('? NOT BETWEEN ticket_prices.visible_from AND ticket_prices.valid_to', [$now]);
            }
            $managerQuery->groupBy('managers.id');
        }

        // handle sort parameters
        if ($request->query->has('_sortField') && $request->query->has('_sortDir')) {
            $sortField = $request->query->get('_sortField', 'id');
            $sortDir = $request->query->get('_sortDir', 'asc');

            $managerQuery->orderBy($sortField, $sortDir);
        }

        return $managerQuery;
    }
}
