<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\ParentAreasTransformer;
use App\Http\Controllers\Controller;
use App\Models\Area\AreaParent;
use Auth;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ParentAreasController extends Controller
{
    /** @var AreaParent */
    private $areaRepository;

    /**
     * @param AreaParent $areaRepository
     */
    public function __construct(AreaParent $areaRepository)
    {
        $this->areaRepository = $areaRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!Auth::user()->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        }

        $areaQuery = $this->parseRequest($request);

        $paginator = $areaQuery->with('type')
            ->paginate($request->get('_perPage', $this->areaRepository->getPerPage()))
            ->appends($request->except('page'));

        $areas = $paginator->items();

        $data = fractal()
            ->collection($areas, new ParentAreasTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws AccessDeniedHttpException
     */
    public function show($id)
    {
        if (!Auth::user()->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        }

        /** @var AreaParent $area */
        $area = $this->areaRepository->findOrFail($id);

        $data = fractal()
            ->item($area, new ParentAreasTransformer(true))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseRequest(Request $request)
    {
        $areaQuery = $this->areaRepository;

        // handle sort parameters
        $sortField = $request->query->get('_sortField', 'id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        $areaQuery->orderBy($sortField, $sortDir);

        // filter by name
        if ($request->query->has('name')) {
            $name = $request->query->get('name');
            $areaQuery->whereHas('translations', function ($query) use ($name) {
                $query->where('parent_area_translations.name', 'like', '%' . $name . '%');
            });
        }

        // filter by ids
        if ($request->query->has('parent_id')) {
            $ids = $request->query->get('parent_id');
            $areaQuery->whereIn('parent_areas.id', $ids);
        }

        return $areaQuery;
    }
}
