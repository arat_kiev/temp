<?php

namespace App\Api1\Controllers;

use App\Api1\Requests\BasketRequest;
use App\Api1\Transformers\BasketTransformer;
use App\Events\ProductSaleCreated;
use App\Events\TicketCreated;
use App\Exceptions\NotEnoughBalanceException;
use App\Http\Controllers\Controller;

use App\Models\Product\ProductPrice;
use App\Models\User;
use Carbon\Carbon;
use Event;
use Illuminate\Http\Request;
use App\Managers\ProductSaleManager;
use App\Managers\TicketManager;
use App\Models\Ticket\TicketPrice;
use Gloudemans\Shoppingcart\CartItem;
use Gloudemans\Shoppingcart\Exceptions\InvalidRowIDException;
use Cart;

class BasketController extends Controller
{
    /**
     * Constructor. Restrict access to authenticated users
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');

        $this->middleware(function (Request $request, $next) {
            Cart::restore($request->user()->id);
            $response = $next($request);
            Cart::store($request->user()->id);
            return $response;
        });
    }

    /**
     * Get a list of basket items
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        // return transformed cartdata
        $data = fractal()
            ->item(Cart::instance(), new BasketTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Add item to basket
     *
     * @param BasketRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function add(BasketRequest $request)
    {
        // Add item to cart
        switch ($request->type) {
            case 'ticket':
                $this->addTicket($request);
                break;
            case 'product':
                $this->addProduct($request);
                break;
        }

        // return transformed cartdata
        $data = fractal()
            ->item(Cart::instance(), new BasketTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update quantity
     *
     * @param BasketRequest $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(BasketRequest $request, $id)
    {
        try {
            $item = Cart::get($id);
            if (starts_with($item->id, 'ticket')) {
                if ($request->get('qty', 0) > 0) {
                    return response('ticket qty cannot be changed', 400);
                }

                if ($request->get('qty', 0) == 0) {
                    Cart::content()->filter(function ($item) use ($id) {
                          return $item->options['depends'] === $id;
                    })
                    ->pluck('rowId')
                    ->each(function ($rowId) {
                        Cart::remove($rowId);
                    });
                }
            }

            if ($request->has('datetime')) {
                Cart::update($id, ['options' => $request->only('datetime')]);
            }

            Cart::update($id, $request->get('qty', 1));
        } catch (InvalidRowIDException $e) {}

        // return transformed cartdata
        $data = fractal()
            ->item(Cart::instance(), new BasketTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Empty the basket
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function clear()
    {
        // Empty basket
        Cart::destroy();

        // return transformed cartdata
        $data = fractal()
            ->item(Cart::instance(), new BasketTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * @param Request $request
     * @param TicketManager $tm
     * @param ProductSaleManager $pm
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function checkout(Request $request, TicketManager $tm, ProductSaleManager $pm)
    {
        /** @var User $user */
        $user = $request->user();
        $errors = [];

        if (!$user->hasEnoughCredit(Cart::total())) {
            $errors['basket'] = 'Not enough balance';
        }

        // Check all items
        foreach (Cart::content() as $item) {
            /** @var CartItem $item */
            try {
                switch ($item->model->getBuyableType()) {
                    case 'ticket':
                        $tm->canBuyTicket($item->model, $user, [
                            'group' => $item->options->group,
                            'startDate' => new Carbon($item->options->datetime),
                        ]);
                        break;
                    case 'product':
                        $pm->canbuyProduct($item->model->product, $user, array_merge([
                            'quantity' => $item->qty,
                        ], $item->options->toArray()));
                        break;
                }
            }
            catch (\Exception $e) {
                $errors[$item->rowId] = $e->getMessage();
            }
        }

        if (!empty($errors)) {
            return response()->json(compact('errors'), 400);
        }

        // buy all items
        foreach (Cart::content() as $item) {
            /** @var CartItem $item */
            try {
                switch ($item->model->getBuyableType()) {
                    case 'ticket':
                        $ticket = $tm->buyTicket($item->model, $user, new Carbon($item->options->datetime));
                        Event::fire(new TicketCreated($ticket, config('app.client')));
                        $ticket->group_data = $item->options->group;
                        $ticket->save();
                        break;
                    case 'product':
                        $product = $pm->buyProduct($item->model->product, $user, array_merge([
                            'quantity' => $item->qty,
                        ], $item->options->toArray()));
                        Event::fire(new ProductSaleCreated($product, config('app.client')));
                        $product->save();
                        break;
                }
                Cart::remove($item->rowId);
            }
            catch (\Exception $e) {
                $errors[$item->rowId] = $e->getMessage();
            }
        }

        if (!empty($errors)) {
            return response()->json(compact('errors'), 400);
        }

        return $this->clear();
    }

    protected function addTicket(BasketRequest $request)
    {
        $ticketPrice = TicketPrice::findOrFail($request->id);

        $datetime = $request->request->get('datetime');
        $group_data = collect(array_values($request->request->get('group', [])));
        $group = $group_data->map(function ($p) {
            return [
                'first_name' => $p['first_name'],
                'last_name' =>$p['last_name'],
                'birthday' => Carbon::parse($p['birthday']),
            ];
        });

        Cart::add($ticketPrice, 1, compact('datetime', 'group'));
    }

    protected function addProduct(BasketRequest $request)
    {
        $productPrice = ProductPrice::findOrFail($request->id);

        $options = $productPrice->timeslots()->exists() ?
            $request->only('depends', 'timeslot_dates') :
            $request->only('depends');

        Cart::add($productPrice, $request->qty, $options);
    }
}