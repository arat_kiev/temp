<?php

namespace App\Api1\Controllers\Area;

use App\Api1\Transformers\Picture\PictureTransformer;
use App\Http\Controllers\Controller;
use App\Models\Area\Area;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class PictureController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $areaId
     * @return \Illuminate\Http\Response
     */
    public function index($areaId)
    {
        $area = Area::with('gallery.pictures.author')->findOrFail($areaId);

        if ($area->gallery) {
            $paginator = $area->gallery->pictures()->orderBy('pivot_priority', 'desc')->latest()->paginate();
            $pictures = $paginator->items();
        } else {
            $pictures = [];
            $paginator = new LengthAwarePaginator($pictures, 0, 15);
        }

        $data = fractal()
            ->collection($pictures, new PictureTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param int $areaId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $areaId)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $areaId
     * @param int $galleryId
     * @return \Illuminate\Http\Response
     */
    public function show($areaId, $galleryId)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param int $areaId
     * @param int $galleryId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $areaId, $galleryId)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $areaId
     * @param int $galleryId
     * @return \Illuminate\Http\Response
     */
    public function destroy($areaId, $galleryId)
    {
        //
    }
}
