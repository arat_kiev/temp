<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\CatchStatsTransformer;
use App\Api1\Transformers\ProductSalesStatsTransformer;
use App\Api1\Transformers\TicketSalesStatsTransformer;
use App\Http\Controllers\Controller;
use App\Managers\TicketManager;
use App\Models\Area\Area;
use App\Models\Product\ProductSale;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StatsController extends Controller
{

    public function __construct()
    {
        $this->middleware(['jwt.auth', 'area.manager']);
    }

    /**
     * External API for managers to query their sales data
     *
     * @param Request $request
     * @param int $areaId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSales(Request $request, $areaId)
    {
        try {
            $from = Carbon::parse($request->query->get('from', '1970-01-01'));
            $till = Carbon::parse($request->query->get('till', Carbon::now()))->endOfDay();
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Invalid time range or data',
            ], 400);
        }

        $area = Area::findOrFail($areaId);

        $tickets = $this->fetchTicketsByArea($area, $from, $till);
        $ticketTransformer = new TicketSalesStatsTransformer();
        $ticketData = fractal()->collection($tickets->get(), $ticketTransformer)->toArray();

        $productSales = $this->fetchProductSalesByArea($area, $from, $till);
        $productSalesTransformer = new ProductSalesStatsTransformer();
        $productSaleData = fractal()->collection($productSales->get(), $productSalesTransformer)->toArray();

        $data['data'] = array_merge($ticketData['data'], $productSaleData['data']);

        $data['meta'] = [
            'from'          => $from->toDateString(),
            'till'          => $till->toDateString(),
            'count_total'   => count($data['data']),
            'sum-gross'     => $this->sumStringsAsFloat(
                $ticketTransformer::getGross(),
                $productSalesTransformer::getGross()
            ),
            'sum-net'       => $this->sumStringsAsFloat(
                $ticketTransformer::getPriceNet(),
                $productSalesTransformer::getPriceNet()
            ),
            'sum-vat'       => $this->sumStringsAsFloat(
                $ticketTransformer::getVatValue(),
                $productSalesTransformer::getVatValue()
            ),
        ];

        return response()->json($data);
    }
    
    private function fetchTicketsByArea(Area $area, Carbon $from, Carbon $till)
    {
        $eager = [
            'type', 'price', 'user.country.translations',
            'resellerTicket.reseller.country.translations',
            'resellerTicket.country.translations', 'type.area',
        ];
        
        return $area->tickets()
            ->with($eager)
            ->withTrashed()
            ->whereBetween('tickets.created_at', [$from, $till]);
    }
    
    private function fetchProductSalesByArea(Area $area, Carbon $from, Carbon $till)
    {
        $eager = [
            'price', 'reseller', 'reseller.country', 'product', 'product.managers', 'product.areas'
        ];

        $saleIds = [];
        $area->products()->with('sales')->each(function ($product) use (&$saleIds) {
            $ids = $product->sales()->pluck('id')->toArray();
            $saleIds = array_merge($saleIds, $ids);
        });

        return ProductSale::whereIn('id', $saleIds)
            ->with($eager)
            ->whereBetween('created_at', [$from, $till])
            ->withTrashed();
    }

    private function sumStringsAsFloat()
    {
        $args = func_get_args();

        $floatedArgs = array_map(function ($value) {
            return is_string($value) ? floatval(str_replace(',', '.', $value)) : floatval($value);
        }, $args);

        return number_format((float) array_sum($floatedArgs), 2, ',', '');
    }

    /**
     * External API for managers to query their hauls data
     *
     * @param Request $request
     * @param int $area_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHauls(Request $request, $area_id)
    {
        try {
            $from = Carbon::parse($request->query->get('from', '1970-01-01'));
            $till = Carbon::parse($request->query->get('till', '2037-12-31'));
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Invalid time range or data',
            ], 400);
        }

        $area = Area::findOrFail($area_id);

        $eager = ['user.country.translations'];

        $query = $area->hauls()->with($eager)->whereBetween('hauls.created_at', [$from, $till]);

        $transformer = new CatchStatsTransformer();
        $data = fractal()->collection($query->get(), $transformer)->toArray();

        $data['meta'] = [
            'from'          => $from->toDateString(),
            'till'          => $till->toDateString(),
            'count_total'   => $transformer::getCount(),
        ];

        return response()->json($data);
    }
}
