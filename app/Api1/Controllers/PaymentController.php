<?php

namespace App\Api1\Controllers;

use App\Api1\Requests\PaymentInitRequest;
use App\Api1\Requests\PaymentResponseRequest;
use App\Exceptions\ProfileNotCompleteException;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\User;
use Auth;
use Guzzle;
use Illuminate\Http\Request;
use Uuid;

class PaymentController extends Controller
{
    /** @var Payment */
    private $paymentRepository;

    public function __construct(Payment $paymentRepository)
    {
        $this->paymentRepository = $paymentRepository;

        $this->middleware('jwt.auth', ['only' => ['init']]);
    }

    /**
     * Start new payment process and get url to payment form
     *
     * @param PaymentInitRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function init(PaymentInitRequest $request)
    {
        $user = Auth::user();

        if (!$user->complete) {
            throw new ProfileNotCompleteException();
        }

        $payment = new Payment([
            'uuid' => Uuid::generate(4)->string,
            'amount' => (string)($request->get('amount') * 100),
            'success_url' => $request->get('success_url'),
            'failure_url' => $request->get('failure_url'),
        ]);
        $payment->user()->associate($user);
        $payment->save();

        $parameters = [
            'REQUEST.VERSION' => '1.0',
            'SECURITY.SENDER' => config('services.heidelpay.sender'),
            'USER.LOGIN' => config('services.heidelpay.login'),
            'USER.PWD' => config('services.heidelpay.password'),
            'TRANSACTION.MODE' => config('services.heidelpay.mode'),
            'TRANSACTION.CHANNEL' => config('services.heidelpay.channel'),
            'IDENTIFICATION.TRANSACTIONID' => $payment->uuid,
            'IDENTIFICATION.SHOPPERID' => $user->id,
            'PAYMENT.CODE' => $request->get('method') . '.DB',
            'PRESENTATION.AMOUNT' => number_format($request->get('amount'), 2, '.', ''),
            'PRESENTATION.CURRENCY' => 'EUR',
            'PRESENTATION.USAGE' => trans('payment.credit.charge'),
            'CRITERION.SECRET' => $payment->secure_hash,
            'NAME.GIVEN' => $user->first_name,
            'NAME.FAMILY' => $user->last_name,
            'ADDRESS.STREET' => $user->street,
            'ADDRESS.ZIP' => $user->post_code,
            'ADDRESS.CITY' => $user->city,
            'ADDRESS.COUNTRY' => strtoupper($user->country->country_code),
            'ACCOUNT.COUNTRY' => strtoupper($user->country->country_code),
            'CONTACT.EMAIL' => $user->email,
            'FRONTEND.MODE' => 'DEFAULT',
            'FRONTEND.ENABLED' => 'true',
            'FRONTEND.POPUP' => 'false',
            'FRONTEND.NEXT_TARGET' => $request->get('redirect_target'),
            'FRONTEND.REDIRECT_TIME' => '0',
            'FRONTEND.LANGUAGE_SELECTOR' => 'false',
            'FRONTEND.LANGUAGE' => 'de',
            'FRONTEND.CSS_PATH' => url('css/hco.css'),
            'FRONTEND.RESPONSE_URL' => route('v1.payment.response'),
        ];

        $response = Guzzle::post(config('services.heidelpay.url'), [
            'form_params' => $parameters,
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded;charset=UTF-8',
            ],
        ]);

        // get response body, parse the post data and extract redirect url
        if ($response->getStatusCode() !== 200) {
            return response()->json(['error' => 'Payment gateway error'], 500);
        }

        $body = preg_replace('/\s+/', '', $response->getBody());
        parse_str($body, $resData);

        if (isset($resData) && isset($resData['PROCESSING_RESULT']) && $resData['PROCESSING_RESULT'] === 'ACK') {
            $redirect_url = $resData['FRONTEND_REDIRECT_URL'];
        } else {
            return response()->json(['error' => 'Payment gateway error'], 500);
        }

        return response()->json(['data' => compact('redirect_url')]);
    }

    /**
     * Gets the response from the payment provider
     *
     * @param PaymentResponseRequest|Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(PaymentResponseRequest $request)
    {
        $procResult = $request->get('PROCESSING.RESULT', $request->get('PROCESSING_RESULT', 'NOK'));
        $uuid = $request->get('IDENTIFICATION.TRANSACTIONID', $request->get('IDENTIFICATION_TRANSACTIONID', ''));
        $secret = $request->get('CRITERION.SECRET', $request->get('CRITERION_SECRET', ''));
        $currency = $request->get('PRESENTATION.CURRENCY', $request->get('PRESENTATION_CURRENCY', ''));

        // Check request status
        /** @var Payment $payment */
        if (!$payment = $this->paymentRepository->where('uuid', $uuid)->first()) {
            return response(config('app.url'), 200);
        }

        if ($procResult !== 'ACK' || $payment->completed || !$payment->checkSecureHash($secret) || $currency !== 'EUR') {
            return response($payment->failure_url);
        }

        // Perform balance update
        $amount = $request->get('PRESENTATION.AMOUNT', $request->get('PRESENTATION_AMOUNT', 0.0));
        $amount = str_replace(',', '.', $amount);
        $amount = ((float)$amount) * 100;

        /** @var User $user */
        $user = $payment->user;
        $user->addCredit($amount);
        $user->save();

        info('Credit added to user', ['payment' => $payment->id, 'user' => $user->id, 'amount' => $amount]);

        $payment->completed = true;
        $payment->save();

        return response($payment->success_url);
    }
}
