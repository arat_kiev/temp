<?php

namespace App\Api1\Controllers;

use App\Api1\Requests\AreaRatingRequest;
use App\Http\Controllers\Controller;
use App\Models\Area\Area;
use App\Models\Area\AreaRating;
use Auth;

class AreaRatingController extends Controller
{
    /** @var \App\Models\Area\AreaRating */
    private $areaRatingRepository;

    /**
     * @param AreaRating $areaRatingRepository
     * @internal param \App\Models\Area\AreaType $areaTypeRepository
     */
    public function __construct(AreaRating $areaRatingRepository)
    {
        $this->areaRatingRepository = $areaRatingRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param                                            $areaId
     * @param AreaRatingRequest|\Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AreaRatingRequest $request, $areaId)
    {
        $area = Area::findOrFail($areaId);
        $params = $request->all();
        $areaRatingObj = $this->areaRatingRepository;

        foreach ($params['subcategories'] as $category) {
            $areaRatingObj::updateOrCreate(
                [
                    'area_id' => $area->id,
                    'user_id' => Auth::user()->id,
                    'rating_category_id' => $category['id'],
                ],
                [
                    'value' => $category['value'],
                ]
            );
        }

        return response()->json(['Status' => 1], 201);
    }
}
