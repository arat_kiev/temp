<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\ResellerTransformer;
use App\Http\Controllers\Controller;
use App\Models\Contact\Reseller;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class ResellerController extends Controller
{
    /** @var Reseller */
    private $resellerRepository;

    /**
     * @param \App\Models\Contact\Reseller $resellerRepository
     */
    public function __construct(Reseller $resellerRepository)
    {
        $this->resellerRepository = $resellerRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = $this->resellerRepository->paginate();
        $resellers = $paginator->items();

        $data = fractal()
            ->collection($resellers, new ResellerTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reseller = $this->resellerRepository->findOrFail($id);

        $data = fractal()
            ->item($reseller, new ResellerTransformer())
            ->includeAreas()
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
