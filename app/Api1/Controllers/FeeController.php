<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\FeeTransformer;
use App\Models\Product\Fee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class FeeController extends Controller
{
    /** @var Fee */
    private $feeRepository;

    /**
     * @param Fee $feeRepository
     */
    public function __construct(Fee $feeRepository)
    {
        $this->feeRepository = $feeRepository;

        $this->middleware('jwt.auth', ['only' => ['show', 'store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $feeQuery = $this->parseRequest($request);

        $paginator = $feeQuery->with(['category'])
            ->paginate($request->get('_perPage', $this->feeRepository->getPerPage()))
            ->appends($request->except('page'));

        $fees = $paginator->items();

        $data = fractal()
            ->collection($fees, new FeeTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $includes = ['country', 'category'];

        /** @var Fee $fee */
        $fee = $this->feeRepository->with($includes)->findOrFail($id);

        $data = fractal()
            ->item($fee, new FeeTransformer(true))
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseRequest(Request $request)
    {
        $feeQuery = $this->feeRepository
            ->addSelect(['fees.*', 'fees.id as id']);

        // handle sort parameters
        if ($request->query->has('_sortField') && $request->query->has('_sortDir')) {
            $sortField = $request->query->get('_sortField', 'id');
            $sortDir = $request->query->get('_sortDir', 'asc');

            $feeQuery->orderBy($sortField, $sortDir);
        }

        // Filter by name
        if ($request->query->has('name') && is_string($request->query->get('name'))
            || $request->query->has('search') && is_string($request->query->get('search'))) {
            $name = $request->query->get('name', $request->query->get('search', ''));
            $feeQuery->whereTranslationLike('name', "%$name%");
        }

        if ($request->query->has('fee_ids') && is_array($request->query->get('fee_ids'))) {
            $ids = $request->query->get('fee_ids');
            $feeQuery->whereIn('id', $ids);
        }

        return $feeQuery;
    }
}
