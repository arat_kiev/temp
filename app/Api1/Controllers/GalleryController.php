<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\GalleryTransformer;
use App\Http\Controllers\Controller;
use App\Models\Gallery;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class GalleryController extends Controller
{
    /** @var Gallery */
    private $galleryRepository;

    /**
     * @param Gallery $galleryRepository
     */
    public function __construct(Gallery $galleryRepository)
    {
        $this->galleryRepository = $galleryRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = $this->galleryRepository->paginate();
        $galleries = $paginator->items();

        $data = fractal()
            ->collection($galleries, new GalleryTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gallery = $this->galleryRepository->findOrFail($id);

        $data = fractal()
            ->item($gallery, new GalleryTransformer())
            ->includePictures()
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
