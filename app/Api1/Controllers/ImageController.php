<?php

namespace App\Api1\Controllers;

use App\Api1\Requests\ImageStringUploadRequest;
use App\Api1\Requests\ImageUploadRequest;
use App\Api1\Transformers\Picture\PictureTransformer;
use App\Http\Controllers\Controller;
use App\Models\Picture;
use Auth;
use Intervention\Image\ImageManagerStatic;
use Symfony\Component\HttpFoundation\File\File;

class ImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Upload image and get data back
     *
     * @param ImageUploadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(ImageUploadRequest $request)
    {
        $picture = new Picture([
            'file' => $request->file('file'),
        ]);
        $picture->author()->associate(Auth::user());
        $picture->save();

        $data = fractal()
            ->item($picture, new PictureTransformer())
            ->toArray();

        return response()->json($data, 201);
    }

    public function uploadBase64(ImageStringUploadRequest $request)
    {
        $image = ImageManagerStatic::make($request->get('image'));
        $tempName = tempnam(sys_get_temp_dir(), 'IMG');
        $image->save($tempName);

        $picture = new Picture([
            'file' => new File($tempName),
        ]);
        $picture->author()->associate(Auth::user());
        $picture->save();

        $data = fractal()
            ->item($picture, new PictureTransformer())
            ->toArray();

        return response()->json($data, 201);
    }
}
