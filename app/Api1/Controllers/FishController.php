<?php

namespace App\Api1\Controllers;

use App\Api1\Requests\StoreFishRequest;
use App\Api1\Transformers\FishTransformer;
use App\Http\Controllers\Controller;
use App\Models\Meta\Fish;
use App\Models\Picture;
use Auth;
use Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class FishController extends Controller
{
    /** @var \App\Models\Meta\Fish */
    private $fishRepository;

    /**
     * @param \App\Models\Meta\Fish $fishRepository
     */
    public function __construct(Fish $fishRepository)
    {
        $this->fishRepository = $fishRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fishQuery = $this->parseRequest($request);
        $cacheKey = $this->getIndexCacheKey($request);

        $fishes = Cache::remember($cacheKey, 60, function () use ($fishQuery) {
            return $fishQuery->get();
        });

        $data = fractal()
            ->collection($fishes, new FishTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Generate cache key according to request params
     *
     * @param Request $request
     * @return string
     */
    private function getIndexCacheKey(Request $request)
    {
        $cacheHash = md5(json_encode([
            $request->query->get('_sortField', 'id'),
            $request->query->get('_sortDir', 'asc'),
            $request->query->get('name', ''),
            $request->query->get('fish_id', []),
        ]));

        return implode('_', [
            'fishes',
            App::getLocale(),
            $cacheHash,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFishRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFishRequest $request)
    {
        $fish = $this->fishRepository->saveAsDraft(array_merge($request->all(), ['picture_id' => $request->get('picture')]));

        $data = fractal()
            ->item($fish, new FishTransformer(true))
            ->toArray();

        return response()->json($data, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fish = $this->fishRepository->findOrFail($id);

        $data = fractal()
            ->item($fish, new FishTransformer(true))
            ->parseIncludes(['haul_pictures', 'fish_category'])
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreFishRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreFishRequest $request, $id)
    {
        /** @var Fish $fish */
        $fish = $this->fishRepository->includeDraft()->findOrFail($id);
        $fish->update(array_merge($request->all(), ['picture_id' => $request->get('picture')]));

        $data = fractal()
            ->item($fish, new FishTransformer(true))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseRequest(Request $request)
    {
        $fishQuery = $this->fishRepository
            ->join('fish_translations', function($join) {
                $join->on('fishes.id', '=', 'fish_translations.fish_id')
                    ->where('fish_translations.locale', '=', App::getLocale());
            })
            ->addSelect(['fishes.*', 'fish_translations.name']);

        // handle sort parameters
        $sortField = $request->query->get('_sortField', 'id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        $fishQuery->orderBy($sortField, $sortDir);

        // filter by name
        if ($request->query->has('name')) {
            $name = $request->query->get('name');
            $fishQuery->whereHas('translations', function ($query) use ($name) {
                $query->where('fish_translations.name', 'like', '%' . $name . '%');
            });
        }

        // filter by area
        if ($request->query->has('areas') && is_array($request->query->get('areas', []))) {
            $areas = $request->query->get('areas');
            $fishQuery->whereHas('areas', function ($query) use ($areas) {
                $query->whereIn('id', $areas);
            });
        }

        // filter by ids
        if ($request->query->has('fish_id') && is_array($request->query->get('fish_id'))) {
            $fish_ids = $request->query->get('fish_id');
            $fishQuery->whereIn('fishes.id', $fish_ids);
        }

        return $fishQuery;
    }
}
