<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\NewsTransformer;
use App\Http\Controllers\Controller;
use App\Models\Organization\News;
use App\Models\Organization\Organization;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NewsController extends Controller
{
    /** @var Organization */
    private $organizationRepository;

    /** @var News */
    private $newsRepository;

    /**
     * @param Organization $organizationRepository
     */
    public function __construct(Organization $organizationRepository, News $newsRepository)
    {
        $this->organizationRepository = $organizationRepository;
        $this->newsRepository = $newsRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $organizationId)
    {
        $user = Auth::user();

        $organization = $this->organizationRepository->findOrFail($organizationId);
        $newsQuery = $organization->news()->active(Carbon::now());

        if (!$user || !$user->isMemberOf($organization)) {
            $newsQuery->public(true);
        }

        $paginator = $newsQuery->with('author')
            ->paginate($request->get('_perPage', $this->organizationRepository->getPerPage()))
            ->appends($request->except('page'));

        $news = $paginator->items();

        $data = fractal()
            ->collection($news, new NewsTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param $organizationId
     * @param $newsId
     * @return \Illuminate\Http\Response
     */
    public function show($organizationId, $newsId)
    {
        $user = Auth::user();

        $organization = $this->organizationRepository->findOrFail($organizationId);
        $news = $this->newsRepository->findOrFail($newsId);

        if (!$news->public) {
            if (!$user || !$user->isMemberOf($organization)) {
                throw new AccessDeniedHttpException('not public');
            }
        } elseif ($news->getStatus() !== News::ACTIVE) {
            throw new NotFoundHttpException();
        }

        $data = fractal()
            ->item($news, new NewsTransformer())
            ->toArray();

        return response()->json($data);
    }
}
