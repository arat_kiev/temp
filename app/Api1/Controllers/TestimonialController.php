<?php

namespace App\Api1\Controllers;

use App\Api1\Requests\StoreTestimonialRequest;
use App\Api1\Transformers\TestimonialTransformer;
use App\Http\Controllers\Controller;
use App\Models\Picture;
use App\Models\Testimonial;
use Auth;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class TestimonialController extends Controller
{
    /** @var Testimonial */
    private $testimonialRepository;

    /**
     * @param Testimonial $testimonialRepository
     */
    public function __construct(Testimonial $testimonialRepository)
    {
        $this->testimonialRepository = $testimonialRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = $this->testimonialRepository->paginate();
        $testimonials = $paginator->items();

        $data = fractal()
            ->collection($testimonials, new TestimonialTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTestimonialRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTestimonialRequest $request)
    {
        /** @var Testimonial $testimonial */
        $testimonial = $this->testimonialRepository->create($request->all());

        if ($request->request->has('picture')) {
            $picture = Picture::findOrFail($request->request->getInt('picture'));

            if ($picture->author->id !== Auth::user()->id) {
                return response()->json(['error' => 'Picture is not allowed'], 400);
            }

            $testimonial->picture()->associate($picture);
        }

        $testimonial->save();

        $data = fractal()
            ->item($testimonial, new TestimonialTransformer())
            ->toArray();

        return response()->json($data, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $testimonial = $this->testimonialRepository->findOrFail($id);

        $data = fractal()
            ->item($testimonial, new TestimonialTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreTestimonialRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTestimonialRequest $request, $id)
    {
        /** @var Testimonial $testimonial */
        $testimonial = $this->testimonialRepository->findOrFail($id);

        if ($request->request->has('picture')) {
            $picture = Picture::findOrFail($request->request->getInt('picture'));

            if ($picture->author->id !== Auth::user()->id) {
                return response()->json(['error' => 'Picture is not allowed'], 400);
            }

            $testimonial->picture()->associate($picture);
        }

        $testimonial->update($request->all());

        $data = fractal()
            ->item($testimonial, new TestimonialTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
