<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\ProductCategoryTransformer;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class ProductCategoryController extends Controller
{
    /** @var ProductCategory */
    private $repository;

    /**
     * @param ProductCategory $repository
     */
    public function __construct(ProductCategory $repository)
    {
        $this->repository = $repository;

        $this->middleware('jwt.auth', ['only' => ['show', 'store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $productQuery = $this->parseRequest($request);

        $paginator = $productQuery
            ->paginate($request->get('_perPage', $this->repository->getPerPage()))
            ->appends($request->except('page'));

        $products = $paginator->items();

        $data = fractal()
            ->collection($products, new ProductCategoryTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $productsPage = $request->query->getInt('product_page') ?: 1;
        $includes = ['products'];

        /** @var ProductCategory $category */
        $category = $this->repository->with($includes)->findOrFail($id);

        $data = fractal()
            ->item($category, new ProductCategoryTransformer(true, $productsPage))
            ->parseIncludes($includes)
            ->includeImage()
            ->toArray();

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseRequest(Request $request)
    {
        $categoryQuery = $this->repository;

        // handle sort parameters
        if ($request->query->has('_sortField') && $request->query->has('_sortDir')) {
            $sortField = $request->query->get('_sortField', 'id');
            $sortDir = $request->query->get('_sortDir', 'asc');

            $categoryQuery->orderBy($sortField, $sortDir);
        }

        // Filter by name
        if ($request->query->has('name') && is_string($request->query->get('name'))
            || $request->query->has('search') && is_string($request->query->get('search'))) {
            $name = $request->query->get('name', $request->query->get('search', ''));
            $categoryQuery->whereTranslationLike('name', "%$name%");
        }

        // Filter by Ids
        if ($request->query->has('ids') && is_array($request->query->get('ids', []))) {
            $ids = $request->query->get('ids', []);
            $categoryQuery->whereIn('id', $ids);
        }

        // Filter by managers
        if ($request->query->has('manager') && $request->query->getInt('manager')) {
            $managerId = $request->query->getInt('manager');
            $categoryQuery->whereHas('products', function ($productQuery) use ($managerId) {
                $productQuery->whereHas('managers', function ($managers) use ($managerId) {
                    $managers->whereId($managerId);
                });
            });
        }

        return $categoryQuery;
    }
}
