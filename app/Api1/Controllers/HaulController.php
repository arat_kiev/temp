<?php

namespace App\Api1\Controllers;

use App\Api1\Requests\HaulReportRequest;
use App\Api1\Requests\UpdateHaulRequest;
use App\Api1\Transformers\HaulTransformer;
use App\Http\Controllers\Controller;
use App\Models\Haul;
use App\Models\HaulFlags;
use App\Models\HaulVotes;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Api1\Requests\TicketInspectionHaulRequest;
use App\Models\Inspection\TicketInspectionHaul;
use App\Api1\Transformers\Inspection\TicketInspectionHaulsTransformer;

class HaulController extends Controller
{
    /** @var Haul */
    private $haulRepository;

    public function __construct(Haul $haulRepository)
    {
        $this->haulRepository = $haulRepository;

        $this->middleware('jwt.auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $includes = ['user', 'area', 'fish', 'picture'];

        $haulsQuery = $this->parseRequest($request);
        $paginator = $haulsQuery->with($includes)
            ->paginate($request->get('_perPage', $this->haulRepository->getPerPage()))
            ->appends($request->except('page'));
        $hauls = $paginator->items();

        $data = fractal()
            ->collection($hauls, new HaulTransformer())
            ->parseIncludes($includes)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $includes = ['user', 'area', 'fish', 'picture'];

        $haul = $this->haulRepository->with($includes)->findOrFail($id);

        if (!$haul->public && $haul->user_id !== Auth::id()) {
            throw new AccessDeniedHttpException('You are not permitted to view the requested resource');
        }

        $data = fractal()
            ->item($haul, new HaulTransformer())
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateHaulRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateHaulRequest $request, $id)
    {
        $user = Auth::user();
        $haul = Haul::findOrFail($id);

        $this->checkUserIsHaulOwner($user, $haul);

        if (!$request->request->has('ticket_number')) {
            $request->request->set('ticket_number', '');
        }

        $includes = ['area', 'fish', 'technique', 'picture'];

        $data = fractal()
            ->item($haul, new HaulTransformer())
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $haul = Haul::findOrFail($id);

        $this->checkUserIsHaulOwner($user, $haul);

        if ($this->isModificationAvailable($haul)) {
            $haul->delete();
        } else {
            throw new AccessDeniedHttpException();
        }
    }

    public function check($haulId)
    {
        $user = Auth::user();
        $haul = Haul::findOrFail($haulId);

        $this->checkUserIsHaulOwner($user, $haul);

        return response()->json([
            'isEditable' => $this->isModificationAvailable($haul),
        ]);
    }

    private function parseRequest(Request $request)
    {
        $haulsQuery = $this->haulRepository
            ->where('public', 1)
            ->where('status', 'accepted')
            ->whereNotNull('picture_id')
            ->whereNotNull('fish_id');

        // handle sort parameters
        $sortField = $request->query->get('_sortField', 'id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        if (in_array($sortField, ['id', 'catch_date', 'created_at']) &&
            in_array($sortDir, ['asc', 'desc'])
        ) {
            $haulsQuery->orderBy($sortField, $sortDir);
        }

        if ($request->query->has('area')) {
            $area = (array)$request->query->get('area');
            $haulsQuery->whereIn('area_id', $area);
        }

        if ($request->query->has('user')) {
            $user = (array)$request->query->get('user');
            $haulsQuery->whereIn('user_id', $user);
        }

        if ($request->query->has('fish')) {
            $fish = (array)$request->query->get('fish');
            $haulsQuery->whereIn('fish_id', $fish);
        }

        if ($request->query->has('tech')) {
            $tech = (array)$request->query->get('tech');
            $haulsQuery->whereIn('technique_id', $tech);
        }

        if ($request->query->has('catch_from')) {
            $catch_from = $request->query->get('catch_from');
            $haulsQuery->where('catch_date', '>=', $catch_from);
        }

        if ($request->query->has('catch_till')) {
            $catch_till = $request->query->get('catch_till');
            $haulsQuery->where('catch_date', '<=', $catch_till);
        }

        if ($request->query->has('city')) {
            $cities = (array)$request->query->get('city');
            $haulsQuery->belongsToCities((array)$cities);
        }

        if ($request->query->has('region')) {
            $regions = (array)$request->query->get('region');
            $haulsQuery->belongsToRegions((array)$regions);
        }

        if ($request->query->has('state')) {
            $states = (array)$request->query->get('state');
            $haulsQuery->belongsToStates((array)$states);
        }

        if ($request->query->has('country')) {
            $countries = (array)$request->query->get('country');
            $haulsQuery->belongsToCountries((array)$countries);
        }

        return $haulsQuery;
    }

    /**
     * Vote for haul (like/dislike)
     *
     * @param $id
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function vote($id, $type)
    {
        $vote = $this->fetchVote($type);

        $haul = $this->haulRepository->findOrFail($id);

        $this->saveVoteInDatabase($id, $vote);

        $this->checkHaulDislikesToReport($haul);

        $data = fractal()
            ->item($haul, new HaulTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Fetch vote value from Human-readable vote
     *
     * @param $type
     * @return int
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function fetchVote($type)
    {
        switch ($type) {
            case 'like':
                return 1;
            case 'dislike':
                return -1;
            default:
                abort(404);
        }
    }

    private function saveVoteInDatabase($haulId, $vote = 0)
    {
        // Check if user already voted for this haul
        if ($haulVote = HaulVotes::findVote($haulId, Auth::user()->id)) {
            if ($haulVote->vote == $vote) {
                $haulVote->delete();    // unvote
            } else {
                $haulVote->update(['vote' => $vote]);
            }
        } else {
            HaulVotes::create([
                'haul_id'   => $haulId,
                'user_id'   => Auth::user()->id,
                'vote'      => $vote,
            ]);
        }
    }

    private function checkHaulDislikesToReport(Haul $haul)
    {
        // If haul has 10 and more dislikes it place once to Reports
        if ($haul->dislikes() >= 10 && !HaulFlags::where('user_id', '=', null)->first()) {
            HaulFlags::create([
                'haul_id'       => $haul->id,
                'user_id'       => null,
                'description'   => 'System Report',
            ]);
            $haul->update(['status' => 'review']);
        }
    }

    /**
     * Report a haul
     *
     * @param HaulReportRequest $request
     * @param                   $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function report(HaulReportRequest $request, $id)
    {
        $haul = $this->haulRepository->findOrFail($id);

        // Check if user already reported this haul
        if ($haulReport = HaulFlags::findReport($id, Auth::user()->id)) {
            abort(403, 'User already reported this haul.');
        } else {
            HaulFlags::create([
                'haul_id' => $id,
                'user_id' => Auth::user()->id,
                'description' => $request->get('description', ''),
            ]);
            $haul->update(['status' => 'review']);
        }

        return response()->json(['Status' => 1]);
    }

    private function isModificationAvailable(Haul $haul) : bool
    {
        $now = Carbon::now();

        return $now->diffInWeeks($haul->created_at) < 4;
    }

    private function checkUserIsHaulOwner(User $user, Haul $haul)
    {
        if (($user->id !== $haul->user->id) || $user->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        }
    }

    /**
     * Set inspection haul
     *
     * @param TicketInspectionHaulRequest $request
     * @param $ticketId
     * @param $inspectionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeHauls(TicketInspectionHaulRequest $request, $ticketId, $inspectionId)
    {
        $ticket = $this->getTicket($ticketId);

        $inspection = $ticket->inspections->reject(function ($inspection) use ($inspectionId) {
            return $inspection->id === $inspectionId;
        });

        $haul = TicketInspectionHaul::create([
            'inspection_id' => $inspection->id,
            'fish_id' => $request->fish_id,
            'length' => $request->length,
            'weight' => $request->weight,
            'comment' => $request->comment,
        ]);

        $inspection->hauls()->associate($haul);

        $data = fractal()
            ->item($haul, new TicketInspectionHaulsTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Get inspection ticket hauls
     *
     * @param $ticketId
     * @param $inspectionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHauls($ticketId, $inspectionId)
    {
        $ticket = $this->getTicket($ticketId);

        $inspection = $ticket->inspections->reject(function ($inspection) use ($inspectionId) {
            return $inspection->id === $inspectionId;
        });

        $hauls = !empty($inspection->first()) ? $hauls = $inspection->first()->hauls : $hauls = collect([]);

        $data = fractal()
            ->collection($hauls, new InspectionTourTransformer())
            ->toArray();

        return response()->json($data);
    }
}
