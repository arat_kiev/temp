<?php

namespace App\Api1\Controllers;

use App\Api1\Requests\StoreTechniqueRequest;
use App\Api1\Transformers\TechniqueTransformer;
use App\Http\Controllers\Controller;
use App\Models\Meta\Technique;
use Cache;
use Illuminate\Http\Request;
use App;

class TechniqueController extends Controller
{
    /** @var Technique */
    private $techniqueRepository;

    /**
     * @param Technique $techniqueRepository
     */
    public function __construct(Technique $techniqueRepository)
    {
        $this->techniqueRepository = $techniqueRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $techQuery = $this->parseRequest($request);
        $cacheKey = $this->getIndexCacheKey($request);

        $techniques = Cache::remember($cacheKey, 60, function () use ($techQuery) {
            return $techQuery->get();
        });

        $data = fractal()
            ->collection($techniques, new TechniqueTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Generate cache key according to request params
     *
     * @param Request $request
     * @return string
     */
    private function getIndexCacheKey(Request $request)
    {
        $cacheHash = md5(json_encode([
            $request->query->get('_sortField', 'id'),
            $request->query->get('_sortDir', 'asc'),
            $request->query->get('name', ''),
            $request->query->get('tech_id', []),
        ]));

        return implode('_', [
            'techniques',
            App::getLocale(),
            $cacheHash,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTechniqueRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTechniqueRequest $request)
    {
        $technique = $this->techniqueRepository->saveAsDraft($request->all());

        $data = fractal()
            ->item($technique, new TechniqueTransformer(true))
            ->toArray();

        return response()->json($data, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $technique = $this->techniqueRepository->findOrFail($id);

        $data = fractal()
            ->item($technique, new TechniqueTransformer(true))
            ->includeGallery()
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreTechniqueRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTechniqueRequest $request, $id)
    {
        /** @var Technique $technique */
        $technique = $this->techniqueRepository->includeDraft()->findOrFail($id);

        $technique->update($request->all());

        $data = fractal()
            ->item($technique, new TechniqueTransformer(true))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //d(^_*)b
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseRequest(Request $request)
    {
        $techQuery = $this->techniqueRepository
            ->join('technique_translations', function($join) {
                $join->on('techniques.id', '=', 'technique_translations.technique_id')
                    ->where('technique_translations.locale', '=', App::getLocale());
            })
            ->addSelect(['techniques.*', 'technique_translations.name']);

        // handle sort parameters
        $sortField = $request->query->get('_sortField', 'id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        $techQuery->orderBy($sortField, $sortDir);

        // filter by name
        if ($request->query->has('name')) {
            $name = $request->query->get('name');
            $techQuery->whereHas('translations', function ($query) use ($name) {
                $query->where('technique_translations.name', 'like', '%' . $name . '%');
            });
        }

        // filter by ids
        if ($request->query->has('tech_id') && is_array($request->query->get('tech_id'))) {
            $tech_ids = $request->query->get('tech_id');
            $techQuery->whereIn('techniques.id', $tech_ids);
        }

        return $techQuery;
    }
}
