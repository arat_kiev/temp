<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\LicenseTransformer;
use App\Http\Controllers\Controller;
use App\Models\License\License;
use Auth;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class LicenseController extends Controller
{
    /** @var License */
    private $licenseRepository;

    public function __construct(License $licenseRepository)
    {
        $this->licenseRepository = $licenseRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        }

        $includes = ['user'];

        $paginator = $this->licenseRepository->with($includes)->paginate();
        $licenses = $paginator->items();

        $data = fractal()
            ->collection($licenses, new LicenseTransformer())
            ->parseIncludes($includes)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();

        if (!$user->licenses->contains($id) && !$user->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        }

        $includes = ['user', 'type', 'attachments'];

        $license = $this->licenseRepository->with($includes)->findOrFail($id);

        $data = fractal()
            ->item($license, new LicenseTransformer())
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
