<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\FishCategoryTransformer;
use App\Http\Controllers\Controller;
use App\Models\Meta\FishCategory;
use Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class FishCategoryController extends Controller
{
    /** @var \App\Models\Meta\Fish */
    private $fishCategoryRepository;

    /**
     * @param \App\Models\Meta\FishCategory $fishCategoryRepository
     */
    public function __construct(FishCategory $fishCategoryRepository)
    {
        $this->fishCategoryRepository = $fishCategoryRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fishCategoriesQuery = $this->parseRequest($request);
        $cacheKey = $this->getIndexCacheKey($request);

        $fishCategories = Cache::remember($cacheKey, 60, function () use ($fishCategoriesQuery) {
            return $fishCategoriesQuery->get();
        });

        $data = fractal()
            ->collection($fishCategories, new FishCategoryTransformer())
            ->includeFishes()
            ->toArray();

        return response()->json($data);
    }

    /**
     * Generate cache key according to request params
     *
     * @param Request $request
     * @return string
     */
    private function getIndexCacheKey(Request $request)
    {
        $cacheHash = md5(json_encode([
            $request->query->get('_sortField', 'id'),
            $request->query->get('_sortDir', 'asc'),
            $request->query->get('name', ''),
            $request->query->get('fish_category_ids', []),
        ]));

        return implode('_', [
            'fish_categories',
            App::getLocale(),
            $cacheHash,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fish = $this->fishCategoryRepository->findOrFail($id);

        $data = fractal()
            ->item($fish, new FishCategoryTransformer(true))
            ->parseIncludes('haul_pictures')
            ->includeFishes()
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseRequest(Request $request)
    {
        $fishCategoryQuery = $this->fishCategoryRepository
            ->join(
                'fish_category_translations',
                'fish_categories.id',
                '=',
                'fish_category_translations.fish_category_id'
            )->addSelect([
                'fish_categories.*',
                'fish_category_translations.name',
                'fish_category_translations.description'
            ])->where('fish_category_translations.locale', '=', App::getLocale());

        // handle sort parameters
        $sortField = $request->query->get('_sortField', 'id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        $fishCategoryQuery->orderBy($sortField, $sortDir);

        // filter by name
        if ($request->query->has('name')) {
            $name = $request->query->get('name');
            $fishCategoryQuery->whereHas('translations', function ($query) use ($name) {
                $query->where('fish_category_translations.name', 'like', '%' . $name . '%')
                    ->where('fish_category_translations.locale', '=', App::getLocale());
            });
        }

        // filter by ids
        if ($request->query->has('fish_category_ids') && is_array($request->query->get('fish_category_ids'))) {
            $fish_category_ids = $request->query->get('fish_category_ids');
            $fishCategoryQuery->whereIn('fish_categories.id', $fish_category_ids);
        }

        return $fishCategoryQuery;
    }
}
