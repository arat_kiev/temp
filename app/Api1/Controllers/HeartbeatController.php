<?php

namespace App\Api1\Controllers;

use App\Http\Controllers\Controller;

class HeartbeatController extends Controller
{
    public function ping()
    {
        return response()->json(['message' => 'pong']);
    }
}