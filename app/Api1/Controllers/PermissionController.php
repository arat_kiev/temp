<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\PermissionTransformer;
use App\Http\Controllers\Controller;
use App\Models\Authorization\Permission;
use App\Models\Authorization\Role;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class PermissionController extends Controller
{
    /** @var \App\Models\Meta\Fish */
    private $permissionRepository;

    /**
     * @param Role $roleRepository
     */
    public function __construct(Permission $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = $this->permissionRepository->paginate();
        $permissions = $paginator->items();

        $data = fractal()
            ->collection($permissions, new PermissionTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $includes = ['roles'];

        $permission = $this->permissionRepository->with($includes)->findOrFail($id);

        $data = fractal()
            ->item($permission, new PermissionTransformer())
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
