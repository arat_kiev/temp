<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\TicketTypeTransformer;
use App\Http\Controllers\Controller;
use App\Models\Ticket\TicketType;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class TicketTypeController extends Controller
{
    /** @var \App\Models\Ticket\TicketType */
    private $ticketTypeRepository;

    /**
     * @param \App\Models\Ticket\TicketType $ticketTypeRepository
     */
    public function __construct(TicketType $ticketTypeRepository)
    {
        $this->ticketTypeRepository = $ticketTypeRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $typeQuery = $this->parseRequest($request);

        $paginator = $typeQuery
            ->paginate($request->get('_perPage', $this->ticketTypeRepository->getPerPage()))
            ->appends($request->except('page'));

        $ticketTypes = $paginator->items();

        $data = fractal()
            ->collection($ticketTypes, new TicketTypeTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticketType = $this->ticketTypeRepository->findOrFail($id);

        $data = fractal()
            ->item($ticketType, new TicketTypeTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseRequest(Request $request)
    {
        $typeQuery = $this->ticketTypeRepository
            ->join('ticket_type_translations', 'ticket_types.id', '=', 'ticket_type_translations.ticket_type_id')
            ->addSelect(['ticket_types.*', 'ticket_type_translations.name']);

        // handle sort parameters
        $sortField = $request->query->get('_sortField', 'id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        $typeQuery->orderBy($sortField, $sortDir);

        // filter by name
        if ($request->query->has('name') && is_string($request->query->get('name'))
            || $request->query->has('search') && is_string($request->query->get('search'))) {
            $name = $request->query->get('name', $request->query->get('search', ''));
            $typeQuery->whereHas('translations', function ($query) use ($name) {
                $query->where('ticket_type_translations.name', 'like', '%' . $name . '%');
            });
        }

        // filter by ids
        if ($request->query->has('ticket_type_id')) {
            $ticket_type_ids = $request->query->get('ticket_type_id');
            $typeQuery->whereIn('ticket_types.id', $ticket_type_ids);
        }

        return $typeQuery;
    }
}
