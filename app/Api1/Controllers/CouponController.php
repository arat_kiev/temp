<?php

namespace App\Api1\Controllers;

use App\Api1\Requests\CouponRequest;
use App\Api1\Requests\TransferCouponRequest;
use App\Api1\Transformers\CouponTransformer;
use App\Events\CouponUsed;
use App\Exceptions\ApplicationException;
use App\Exceptions\NotEnoughBalanceException;
use App\Models\Promo\Coupon;
use App\Models\Promo\Promotion;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use CouponManager;
use DB;
use Event;

class CouponController extends Controller
{
    /** @var \App\Models\Promo\Coupon */
    private $couponRepository;

    /**
     * @param \App\Models\Promo\Coupon $couponRepository
     */
    public function __construct(Coupon $couponRepository)
    {
        $this->couponRepository = $couponRepository;

        $this->middleware('jwt.auth', [
            'only' => ['store', 'useCoupon', 'storeTransfer', 'useTransfer', 'update', 'destroy']
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $couponQuery = $this->parseRequest($request);

        $paginator = $couponQuery
            ->paginate($request->get('_perPage', $this->couponRepository->getPerPage()))
            ->appends($request->except('page'));

        $coupons = $paginator->items();

        $data = fractal()
            ->collection($coupons, new CouponTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Api1\Requests\CouponRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CouponRequest $request)
    {
        $promo = Promotion::where('is_active', true)->findOrFail($request->request->get('promo'));

        switch ($promo->type) {
            case 'SIGNUP':
            case 'SALES':
                $this->validateCouponPermissions($promo);
                break;
            case 'INVITE':
                break;
            default:
                throw new ApplicationException('Wrong promotion type');
        }

        $coupon = $promo->coupons()->create([
            'code'          => CouponManager::generateCode(),
            'usage_bonus'   => $promo->usage_bonus,
            'is_public'     => true,
        ]);

        $data = fractal()
            ->item($coupon, new CouponTransformer(true))
            ->includePromotion()
            ->toArray();

        return response()->json($data);
    }

    private function validateCouponPermissions(Promotion $promotion)
    {
        if (!auth()->user()->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        } elseif ($promotion->coupons()->count()) {
            throw new ApplicationException('Coupon for this promotion already has been created');
        }
    }
    
    public function useCoupon(CouponRequest $request)
    {
        $coupon = Coupon::where('code', $request->request->get('code'))->first();
        $promo = $coupon->promotion;

        if (!$promo) {
            throw new ApplicationException('Missing promotion for this coupon');
        } elseif (($promo->valid_till && $promo->valid_till->lt(Carbon::now()))
            || !$promo->is_active) {
            throw new ApplicationException('Coupon can not be used anymore');
        } elseif (in_array(auth()->id(), $coupon->usedBy()->pluck('user_id')->toArray())) {
            throw new ApplicationException('You already used this coupon');
        }

        switch ($promo->type) {
            case 'SIGNUP':
                $this->useSignUpCoupon($coupon);
                break;
            case 'SALE':
                $this->useSalesCoupon($coupon);
                break;
            case 'INVITE':
                $this->useInviteCoupon($coupon);
                break;
            default:
                throw new ApplicationException('Wrong promotion type');
        }

        return response()->json(['success' => true]);
    }

    private function useSignUpCoupon(Coupon $coupon)
    {
        $receiver = auth()->user();

        if (!$receiver->created_at->between(Carbon::now()->subWeek(), Carbon::now())) {
            throw new ApplicationException('You are no longer permitted to use this coupon');
        }
        
        $receiver->usedCoupons()->each(function ($coupon) {
            if ($coupon->promotion && $coupon->promotion === 'INVITE') {
                throw new ApplicationException(
                    'You are not permitted to used this coupon. You already used invitation coupon'
                );
            } elseif ($coupon->promotion && $coupon->promotion === 'SIGNUP') {
                throw new ApplicationException(
                    'You are not permitted to used this coupon. You already used Sign Up coupon'
                );
            }
        });

        DB::transaction(function () use ($coupon, $receiver) {
            $coupon->usedBy()->attach($receiver);
            $receiver->balance += $coupon->usage_bonus_origin;
            $receiver->save();
        });

        Event::fire(new CouponUsed($receiver, $coupon, config('app.client')));
    }

    private function useSalesCoupon(Coupon $coupon)
    {
        $receiver = auth()->user();

        if (in_array($coupon->id, $receiver->usedCoupons()->pluck('coupon_id')->toArray())) {
            throw new ApplicationException('You are not permitted to used this coupon. You already used it');
        }

        $promo = $coupon->promotion;
        $receiverProductSales = $this->fetchUserProductSales($promo, $receiver);
        $receiverTickets = $this->fetchUserTickets($promo, $receiver);

        if (!$receiverTickets->count() || !$receiverProductSales->count()) {
            throw new ApplicationException('You are not permitted to used this coupon. First make some sale');
        }

        DB::transaction(function () use ($coupon, $receiver) {
            $coupon->usedBy()->attach($receiver);
            $receiver->balance += $coupon->usage_bonus_origin;
            $receiver->save();
        });

        Event::fire(new CouponUsed($receiver, $coupon, config('app.client')));
    }

    private function useInviteCoupon(Coupon $coupon)
    {
        $receiver = auth()->user();

        if (!$receiver->created_at->between(Carbon::now()->subWeek(), Carbon::now())) {
            throw new ApplicationException('You are no longer permitted to use this coupon');
        }

        $receiver->usedCoupons()->each(function ($coupon) {
            if ($coupon->promotion && $coupon->promotion === 'INVITE') {
                throw new ApplicationException(
                    'You are not permitted to used this coupon. You already used invitation coupon'
                );
            } elseif ($coupon->promotion && $coupon->promotion === 'SIGNUP') {
                throw new ApplicationException(
                    'You are not permitted to used this coupon. You already used Sign Up coupon'
                );
            }
        });

        DB::transaction(function () use ($coupon, $receiver) {
            $coupon->usedBy()->attach($receiver);

            $receiver->balance += $coupon->usage_bonus_origin;
            $receiver->save();

            $inviter = $coupon->createdBy;
            $inviter->balance += $coupon->promotion->owner_bonus_origin;
            $inviter->save();
        });

        Event::fire(new CouponUsed($receiver, $coupon, config('app.client')));
    }

    private function fetchUserTickets(Promotion $promo, User $user)
    {
        $promoTicketTypes = $promo->ticketTypes()->pluck('id')->toArray();

        $userTickets = $user->tickets()->where('created_at', '>', $promo->valid_from->timestamp);

        if ($promo->valid_till) {
            $userTickets->where('created_at', '<', $promo->valid_till->timestamp);
        }
        if ($promoTicketTypes) {
            $userTickets->whereHas('type', function ($type) use ($promoTicketTypes) {
                $type->whereIn('id', $promoTicketTypes);
            });
        }

        return $userTickets;
    }

    private function fetchUserProductSales(Promotion $promo, User $user)
    {
        $promoProducts = $promo->products()->pluck('id')->toArray();

        $userProductSales = $user->productSales()->where('created_at', '>', $promo->valid_from->timestamp);
        if ($promo->valid_till) {
            $userProductSales->where('created_at', '<', $promo->valid_till->timestamp);
        }
        if ($promoProducts) {
            $userProductSales->whereHas('product', function ($product) use ($promoProducts) {
                $product->whereIn('id', $promoProducts);
            });
        }

        return $userProductSales;
    }

    /**
     * Store a newly created resource in storage with type TRANSFER.
     *
     * @param  \App\Api1\Requests\TransferCouponRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function storeTransfer(TransferCouponRequest $request)
    {
        $amount = $request->request->get('amount');

        if (auth()->user()->balance < $amount * 100) {
            throw new NotEnoughBalanceException(auth()->user()->balance - $amount);
        }

        $receiver = $request->request->has('email')
            ? User::where('email', $request->request->get('email'))->first()
            : User::where('fisher_id', $request->request->get('fisher_id'))->first();

        $coupon = Coupon::create([
            'code'          => CouponManager::generateCode(),
            'usage_bonus'   => $amount,
            'is_public'     => false,
            'created_for'   => $receiver->id,
        ]);

        $data = fractal()
            ->item($coupon, new CouponTransformer(true))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Use coupon with type TRANSFER.
     *
     * @param  \App\Api1\Requests\TransferCouponRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function useTransfer(TransferCouponRequest $request)
    {
        $coupon = Coupon::where('code', $request->request->get('code'))->first();

        if (auth()->id() !== $coupon->created_for) {
            throw new AccessDeniedHttpException();
        } elseif ($coupon->usedBy()->count()) {
            throw new ApplicationException('This coupon already used');
        } elseif ($coupon->createdBy->balance < $coupon->usage_bonus_origin) {
            throw new ApplicationException(
                'Coupon cannot be used right now. User who created coupon has not enough money'
            );
        }

        DB::transaction(function () use ($coupon) {
            $coupon->usedBy()->attach(auth()->id());

            $receiver = auth()->user();
            $receiver->balance += $coupon->usage_bonus_origin;
            $receiver->save();

            $creator = $coupon->createdBy;
            $creator->balance -= $coupon->usage_bonus_origin;
            $creator->save();
        });

        return response()->json(['success' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $includes = ['promotion', 'createdBy'];

        if (auth()->check() && auth()->user()->hasRole('superadmin')) {
            $this->couponRepository = $this->couponRepository->withTrashed();
        } else {
            $this->couponRepository = $this->couponRepository->where(function ($query) {
                $query->where('is_public', true);

                if (auth()->check()) {
                    $query->orWhere('created_by', auth()->user()->id)
                        ->orWhere('created_for', auth()->user()->id);
                }
            });
        }

        $coupon = $this->couponRepository->with($includes)->findOrFail($id);

        $data = fractal()
            ->item($coupon, new CouponTransformer(true))
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coupon = $this->couponRepository->findOrFail($id);

        if (!auth()->user()->hasRole('superadmin') && auth()->user()->id !== $coupon->created_by) {
            throw new AccessDeniedHttpException('You are not allowed for this action');
        } elseif (!auth()->user()->hasRole('superadmin') && $coupon->usedBy()->count()) {
            throw new AccessDeniedHttpException('Some users already used this coupon.');
        }

        $coupon->delete();

        return response()->json(['status' => 'success']);
    }

    private function parseRequest(Request $request)
    {
        $couponQuery = $this->couponRepository;

        if (auth()->check() && !auth()->user()->hasRole('superadmin')) {
            $couponQuery = $couponQuery
                ->where(function ($query) {
                    $query->where('is_public', true)
                        ->orWhere('created_by', auth()->user()->id)
                        ->orWhere('created_for', auth()->user()->id);
                });
        } elseif (auth()->check() && auth()->user()->hasRole('superadmin')) {
            $couponQuery = $couponQuery->withTrashed();
        } else {
            $couponQuery = $couponQuery->where('is_public', true);
        }

        if ($request->query->has('promo')) {
            $promo = $request->query->getInt('promo');
            $couponQuery->where('promo_id', $promo);
        }

        if ($request->query->has('type')
            && in_array($request->query->get('type'), array_keys(Promotion::AVAILABLE_TYPES))) {
            $couponQuery->whereHas('promotion', function ($promo) use ($request) {
                $promo->where('type', $request->query->get('type'));
            });
        } elseif (auth()->check() && $request->query->has('type') && $request->query->get('type') == 'TRANSFER') {
            $couponQuery->where('is_public', false);
        }

        if (auth()->check() && $request->query->getBoolean('only_available')) {
            $couponQuery = $couponQuery->whereNotIn(
                'id',
                auth()->user()->usedCoupons()->select('id')->pluck('id')->toArray()
            );
            $couponQuery = $couponQuery->where(function ($query) {
                $query = $query->where('created_for', auth()->user()->id);

                if (auth()->user()->created_at
                    && auth()->user()->created_at->between(Carbon::now()->subWeek(), Carbon::now())) {
                    $isLookForSignUpCoupons = auth()->user()->usedCoupons()->whereHas('promotion', function ($promo) {
                        $promo->where('type', 'SIGNUP')
                            ->orWhere('type', 'INVITE');
                    })->get()->isEmpty();

                    if ($isLookForSignUpCoupons) {
                        $query = $query->orWhereHas('promotion', function ($promo) {
                            $promo->where('type', 'SIGNUP')
                                ->orWhere('type', 'INVITE')
                                ->where('valid_from', '<=', auth()->user()->created_at)
                                ->where(function ($sub) {
                                    $sub->where('valid_till', '>=', auth()->user()->created_at)
                                        ->orWhereNull('valid_till');
                                });
                        });
                    }
                }

                $userTicketTypeIds = auth()->user()->tickets()->select('ticket_type_id')->pluck('ticket_type_id')->toArray();
                $userProductIds = auth()->user()->productSales()->select('product_id')->pluck('product_id')->toArray();

                if ($userTicketTypeIds || $userProductIds) {
                    $query = $query->orWhere(function ($sub) use ($userTicketTypeIds, $userProductIds) {
                        $sub->doesntHave('promotion')
                            ->orWhereHas('promotion', function ($promo) use ($userTicketTypeIds, $userProductIds) {
                                $promo
                                    ->whereHas('ticketTypes', function ($ticketType) use ($userTicketTypeIds) {
                                        $ticketType->whereIn('id', $userTicketTypeIds);
                                    })
                                    ->orWhereHas('products', function ($product) use ($userProductIds) {
                                        $product->whereIn('id', $userProductIds);
                                    })
                                ;
                            });
                    });
                }
            });
        }

        return $couponQuery;
    }
}
