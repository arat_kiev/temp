<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use DB;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class InspectorController extends Controller
{
    /** @var User */
    private $userRepository;

    /**
     * @param User $userRepository
     */
    public function __construct(User $userRepository)
    {
        $this->userRepository = $userRepository;

        $this->middleware('jwt.auth');

        if (!Auth::user()->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $inspectorQuery = $this->parseRequest($request);

        $paginator = $inspectorQuery
            ->paginate($request->get('_perPage', $this->userRepository->getPerPage()))
            ->appends($request->except('page'));

        $inspectors = $paginator->items();

        $data = fractal()
            ->collection($inspectors, new UserTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inspector = $this->userRepository
            ->hasRole('ticket_inspector')
            ->findOrFail($id);

        $data = fractal()
            ->item($inspector, new UserTransformer(true))
            ->toArray();

        return response()->json($data);
    }

    public function newInspection()
    {
        
    }

    private function parseRequest(Request $request)
    {
        $userQuery = $this->userRepository
            ->join('user_roles', 'user_roles.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'user_roles.role_id')
            ->rightJoin('area_inspectors', 'area_inspectors.user_id', '=', 'users.id')
            ->addSelect(['users.*', 'users.id as id', 'users.name as name'])
            ->where('roles.name', '=', 'ticket_inspector');

        if ($request->query->has('search') && !is_array($request->query->get('search'))) {
            $search = $request->query->get('search');
            $userQuery
                ->where('users.name', 'like', '%' . $search . '%')
                ->orWhere(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'like', '%' . $search . '%');
        }

        // filter by ids
        if ($request->query->has('ids') && is_array($request->query->get('ids'))) {
            $userIds = $request->query->get('ids');
            $userQuery->whereIn('users.id', $userIds);
        }

        return $userQuery->distinct();
    }
}
