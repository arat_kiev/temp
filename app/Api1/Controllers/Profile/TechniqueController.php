<?php

namespace App\Api1\Controllers\Profile;

use App\Api1\Transformers\TechniqueTransformer;
use App\Http\Controllers\Controller;
use App\Models\Meta\Technique;
use Auth;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class TechniqueController extends Controller
{

    private $techniquesRepository;

    public function __construct(Technique $techniquesRepository)
    {
        $this->techniquesRepository = $techniquesRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     * @param $user_id
     * @return mixed
     */
    public function index($user_id)
    {
        $paginator = Auth::user()->techniques()->paginate();
        $techniques = $paginator->items();

        $data = fractal()
            ->collection($techniques, new TechniqueTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $new_values = (array)$request->input('id');

        Auth::user()->techniques()->sync($new_values, false);

        return response()->json();
    }

    /**
     * Display the specified resource.
     * @param $user_id
     * @param $id
     * @return mixed
     */
    public function show($user_id, $id)
    {
        if (Auth::user()->techniques()->where('id', $id)->exists()) {
            $techniques = Auth::user()->techniques()->where('id', $id)->first();
            $data = fractal()->item($techniques, new TechniqueTransformer(true))->toArray();
            return response()->json($data);
        }
        abort(400, 'Invalid input data');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $user_id
     * @return mixed
     */
    public function update(Request $request, $user_id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param $user_id
     * @param $id
     * @return mixed
     */
    public function destroy($user_id, $id)
    {
        if (Auth::user()->techniques()->where('id', $id)->exists()) {
            Auth::user()->techniques()->detach($id);
            return response()->json();
        }
        abort(400, 'Invalid input data');
    }
}
