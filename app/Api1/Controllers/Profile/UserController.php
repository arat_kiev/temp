<?php

namespace App\Api1\Controllers\Profile;

use App\Api1\Transformers\AreaTransformer;
use App\Api1\Transformers\HaulTransformer;
use App\Api1\Transformers\UserFollowsTransformer;
use App\Api1\Transformers\UserProfileTransformer;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\TransformerAbstract;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserController extends Controller
{

    private $userRepository;
    private $publicAccess = [
        'areas' => 'favorite_areas_public',
        'followers' => 'followers_public',
    ];

    /**
     * @param \App\Models\User $userRepository
     */
    public function __construct(Request $request, User $userRepository)
    {
        $this->middleware(function ($request, $next) use ($userRepository) {
            $this->userRepository = $userRepository;
            list(, $action) = explode('@', $request->route()->getActionName());
            $user_id = $request->route('user_id');

            // Validate user
            $user = $this->userRepository->findOrFail($user_id);

            // Validate public access to specific action
            if (isset($this->publicAccess[$action]) && !User::find($user_id)->{$this->publicAccess[$action]}) {
                throw new AccessDeniedHttpException('Access to this resource was restricted by the owner');
            }

            return $next($request);
        });
    }


    /**
     * Return user's public profile
     * @param $user_id
     * @return mixed
     */
    public function profile($user_id)
    {
        $user = $this->userRepository->findOrFail($user_id);
        $data = fractal()->item($user, new UserProfileTransformer())->toArray();
        return response()->json($data);
    }

    /**
     * Get user's areas
     * @param Request $request
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function areas(Request $request, $user_id)
    {
        $query = User::find($user_id)->favorites();
        return $this->transformQuery($query, new AreaTransformer(true));
    }

    /**
     * Get user's hauls
     * @param Request $request
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function hauls(Request $request, $user_id)
    {
        $query = User::find($user_id)->hauls()->where('public', 1);
        return $this->transformQuery($query, new HaulTransformer());
    }

    /**
     * Get user's followers
     * @param Request $request
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function followers(Request $request, $user_id)
    {
        $query = User::find($user_id)->follows();
        return $this->transformQuery($query, new UserFollowsTransformer());
    }

    /**
     * Create paginated response
     * @param $query
     * @param TransformerAbstract $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    private function transformQuery($query, TransformerAbstract $transformer)
    {
        $paginator = $query->paginate();
        $items = $paginator->items();

        $data = fractal()
            ->collection($items, $transformer)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }
}
