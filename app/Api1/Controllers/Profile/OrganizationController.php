<?php

namespace App\Api1\Controllers\Profile;

use App\Api1\Transformers\OrganizationTransformer;
use App\Http\Controllers\Controller;
use App\Models\Organization\Organization;
use App\Models\User;
use Illuminate\Http\Response;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class OrganizationController extends Controller
{
    /** @var User */
    private $userRepository;

    public function __construct(User $userRepository)
    {
        $this->userRepository = $userRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $userId
     * @return Response
     */
    public function index($userId)
    {
        $user = $this->userRepository->findOrFail($userId);
        $this->checkPermissions($user);

        $includes = ['news', 'memberAreas'];

        $paginator = $user->organizations()->paginate();
        $organizations = $paginator->items();

        $data = fractal()
            ->collection($organizations, new OrganizationTransformer())
            ->parseIncludes($includes)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param $userId
     * @param $organizationId
     * @return Response
     * @internal param int $id
     */
    public function show($userId, $organizationId)
    {
        $user = $this->userRepository->findOrFail($userId);
        $this->checkPermissions($user);
        $organization = Organization::findOrFail($organizationId);
        $includes = ['news', 'memberAreas'];

        $checkRelation = $user->whereHas('organizations', function ($query) use ($organizationId) {
            $query->where('id', $organizationId);
        })->count();

        if (!$checkRelation) {
            throw new AccessDeniedHttpException('User does not belong to this organization');
        }

        $data = fractal()
            ->item($organization, new OrganizationTransformer())
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data);
    }

    private function checkPermissions(User $user)
    {
        if (\Auth::user()->id !== $user->id) {
            throw new AccessDeniedHttpException();
        }
    }
}