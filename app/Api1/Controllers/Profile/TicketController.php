<?php

namespace App\Api1\Controllers\Profile;

use App\Api1\Requests\StoreCheckinRequest;
use App\Api1\Requests\StoreTicketRequest;
use App\Api1\Transformers\CheckinTransformer;
use App\Api1\Transformers\TicketTransformer;
use App\Events\TicketCreated;
use App\Exceptions\InvalidGroupCountException;
use App\Http\Controllers\Controller;
use App\Managers\TicketManager;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketPrice;
use App\Models\Ticket\ResellerTicket;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Config;
use Event;
use iio\libmergepdf\Merger;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use SnappyPDF;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Api1\Requests\ClaimTicketRequest;

class TicketController extends Controller
{
    /** @var int */
    private static $mainClient = 2;

    /** @var User */
    private $userRepository;

    /** @var Ticket */
    private $ticketRepository;

    public function __construct(User $userRepository, Ticket $ticketRepository)
    {
        $this->userRepository = $userRepository;
        $this->ticketRepository = $ticketRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $userId)
    {
        $user = Auth::user();

        if ($user->id != $userId) {
            throw new AccessDeniedHttpException('');
        }

        $includes = ['user', 'type', 'price', 'hauls'];

        $user = $this->userRepository->findOrFail($userId);

        $paginator = $this->parseRequest($request, $user)->with($includes)->paginate();
        $tickets = $paginator->items();

        $data = fractal()
            ->collection($tickets, new TicketTransformer())
            ->parseIncludes($includes)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTicketRequest $request
     * @param TicketManager $ticketManager
     * @param int $userId
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTicketRequest $request, TicketManager $ticketManager, $userId)
    {
        $includes = ['user', 'type', 'price'];

        $user = Auth::user();

        if ($user->id !== (int)$userId) {
            throw new AccessDeniedHttpException();
        }

        $date = $request->request->get('date');
        $startDate = Carbon::parse($date);

        $priceId = $request->request->getInt('price_id');
        /** @var TicketPrice $price */
        $price = TicketPrice::find($priceId);

        // process group ticket data
        $group_data = collect(array_values($request->request->get('group', [])));
        $group_data = $group_data->map(function ($p) {
            return [
                'first_name' => $p['first_name'],
                'last_name' =>$p['last_name'],
                'birthday' => Carbon::parse($p['birthday']),
            ];
        });

        // check for valid count (equal to price setting)
        if ($price->group_count > 1) {
            if (count($group_data) !== $price->group_count - 1) {
                throw new InvalidGroupCountException();
            }
        }

        $ticket = $ticketManager->buyTicket($price, $user, $startDate);

        // Add the group data to the ticket
        $ticket->group_data = $group_data;
        $ticket->save();

        Event::fire(new TicketCreated($ticket, config('app.client')));

        $data = fractal()
            ->item($ticket, new TicketTransformer(true))
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data, 201);
    }

    /**
     * @param StoreTicketRequest $request
     * @param TicketManager      $ticketManager
     * @param                    $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkDateIntersection(StoreTicketRequest $request, TicketManager $ticketManager, $userId)
    {
        $user = Auth::user();

        if ($user->id !== (int) $userId) {
            throw new AccessDeniedHttpException();
        }

        $date = $request->request->get('date');
        $startDate = Carbon::parse($date);

        $priceId = $request->request->getInt('price_id');
        $price = TicketPrice::find($priceId);

        $ticketManager->checkUserTicketsIntersection($price, $user, $startDate);

        return response()->json(['Status' => 1], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param $userId
     * @param $ticketId
     * @return \Illuminate\Http\Response
     */
    public function show($userId, $ticketId)
    {
        $includes = ['user', 'type', 'price', 'hauls', 'checkins', 'signature'];

        $ticket = $this->ticketRepository->findOrFail($ticketId);

        $user = Auth::user();

        if ($user->id != $userId || !$ticket->user || $ticket->user->id != $userId) {
            throw new AccessDeniedHttpException('');
        }

        $data = fractal()
            ->item($ticket, new TicketTransformer(true))
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data, 200, [], JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        throw new AccessDeniedHttpException();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        throw new AccessDeniedHttpException();
    }

    /**
     * Get the tickets checkins
     *
     * @param int $userId
     * @param int $ticketId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCheckins($userId, $ticketId)
    {
        $ticket = $this->ticketRepository->findOrFail($ticketId);
        $user = Auth::user();

        if (Auth::user()->id !== $user->id || $ticket->user->id !== $user->id) {
            throw new AccessDeniedHttpException();
        }

        $data = fractal()
            ->collection($ticket->checkins, new CheckinTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Add checkin to ticket
     *
     * @param int $userId
     * @param int $ticketId
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCheckins(StoreCheckinRequest $request, $userId, $ticketId)
    {
        /** @var Ticket $ticket */
        $ticket = $this->ticketRepository->findOrFail($ticketId);
        $user = Auth::user();

        if (Auth::user()->id !== $user->id || $ticket->user->id !== $user->id) {
            throw new AccessDeniedHttpException();
        }

        $checkin = $ticket->checkins()->create($request->only('from', 'till'));
        $checkin->fishingMethods()->attach(['fishing_method_id' => $request->fishing_method_id]);

        $data = fractal()
            ->item($checkin, new CheckinTransformer())
            ->toArray();

        return response()->json($data);
    }

    public function updateCheckins(StoreCheckinRequest $request, $userId, $ticketId, $checkinId)
    {
        /** @var Ticket $ticket */
        $ticket = $this->ticketRepository->findOrFail($ticketId);
        $user = Auth::user();

        if (Auth::user()->id !== $user->id || $ticket->user->id !== $user->id) {
            throw new AccessDeniedHttpException();
        }

        $checkin = $ticket->checkins()->findOrFail($checkinId);

        if ($request->request->has('from')) {
            $checkin->from = $request->request->get('from');
        }

        if ($request->request->has('till')) {
            $checkin->till = $request->request->get('till');
        }

        if ($request->request->has('fishing_method_id')) {
            $checkin->fishingMethods()->attach(['fishing_method_id' => $request->fishing_method_id]);
        }

        $checkin->save();

        $data = fractal()
            ->item($checkin, new CheckinTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @param User    $user
     * @return \Illuminate\Database\Query\Builder ;
     */
    private function parseRequest(Request $request, User $user)
    {
        $ticketQuery = $user->tickets()
            ->withoutStorno()
            ->addSelect(['tickets.*']);

        if (config('app.client')->id !== self::$mainClient) {
            $ticketQuery->where('tickets.client_id', '=', config('app.client')->id);
        }

        // handle sort parameters
        $sortField = $request->query->get('_sortField', 'id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        $ticketQuery->orderBy($sortField, $sortDir);

        // filter by active ticket
        if ($request->query->has('active')) {
            $active = $request->query->getBoolean('active');
            $ticketQuery->active($active);
        }

        // filter by ids
        if ($request->query->has('ticket_id')) {
            $ticket_ids = $request->query->get('ticket_id');
            $ticketQuery->whereIn('tickets.id', $ticket_ids);
        }

        return $ticketQuery;
    }

    public function pdf($userId, $ticketId)
    {
        $user = Auth::user();

        if ($user->id != $userId) {
            throw new AccessDeniedHttpException('');
        }
        // Generate ticket, merge and download
        $pdf = $this->generatePdf($ticketId);

        return response()->make($pdf)->withHeaders([
            'Content-Type'          => 'application/pdf',
            'Content-Disposition'   => 'inline; ticket.pdf',
        ]);
    }

    private function generatePdf($ticketId)
    {
        $ticket = Ticket::findOrFail($ticketId);

        // Generate PDF
        $pdf = SnappyPDF::loadFile(route('pdf.ticket', ['ticket' => $ticketId, 'demo' => false]));
        $pdf->setOptions([
            'custom-header' => [
                'X-Api-Key'     => Config::get('app.client')->api_key,
            ],
            'margin-left'   => 5,
            'margin-top'    => 5,
            'margin-right'  => 5,
            'margin-bottom' => 5,
        ]);

        $merger = new Merger();
        $merger->addRaw($pdf->output());

        if ($rule = $ticket->type->ruleWithFallback) {
            foreach ($rule->files as $file) {
                if (ends_with($file->fileWithFullPath, ['.pdf'])) {
                    $merger->addFile($file->fileWithFullPath);
                }
            }
        }

        return $merger->merge();
    }

    /**
     * Attach reseller ticket to user
     *
     * @param ClaimTicketRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function attachTickets(ClaimTicketRequest $request)
    {
        $userId = auth()->user()->id;
        $ticketId = explode('-', $request->ticket_number);
        $ticketId = $ticketId[2];
        $resellerTicket = ResellerTicket::where('fisher_id', $request->fisher_id)
            ->where('ticket_id', $ticketId)
            ->first();

        if (!empty($resellerTicket)) {
            $ticket = $resellerTicket->baseTicket;

            if (is_null($ticket->user_id)) {
                $ticket->user_id = $userId;
                $ticket->save();
            }

            return response()->json([], 200);
        }

        return response()->json(['error' => trans('tickets.errors.attach.not_found')], 400);
    }
}
