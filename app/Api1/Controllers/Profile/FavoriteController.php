<?php

namespace App\Api1\Controllers\Profile;

use App\Api1\Requests\StoreFavoriteRequest;
use App\Api1\Transformers\AreaTransformer;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class FavoriteController extends Controller
{
    /** @var User */
    private $userRepository;

    public function __construct(User $userRepository)
    {
        $this->userRepository = $userRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $userId
     * @return Response
     */
    public function index($userId)
    {
        $user = $this->userRepository->findOrFail($userId);

        $paginator = $user->favorites()->paginate();
        $favorites = $paginator->items();

        $data = fractal()
            ->collection($favorites, new AreaTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFavoriteRequest $request
     * @param $userId
     * @return Response
     */
    public function store(StoreFavoriteRequest $request, $userId)
    {
        if (!($user = Auth::user()) || $user->id != $userId) {
            throw new AccessDeniedHttpException();
        }

        $areaId = $request->request->get('area_id');

        /** @var $user User */
        $user->favorites()->detach($areaId);
        $user->favorites()->attach($areaId);
        $user->save();

        return response()->json();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        // NOPE
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $userId
     * @param $areaId
     * @return Response
     */
    public function destroy($userId, $areaId)
    {
        if (!($user = Auth::user()) || $user->id != $userId) {
            throw new AccessDeniedHttpException();
        }

        /** @var $user User */
        $user->favorites()->detach($areaId);
        $user->save();

        return response()->json();
    }
}
