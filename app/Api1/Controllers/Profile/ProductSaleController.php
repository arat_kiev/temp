<?php

namespace App\Api1\Controllers\Profile;

use App\Api1\Requests\ProductSaleRequest;
use App\Api1\Transformers\ProductSaleTransformer;
use App\Http\Controllers\Controller;
use App\Managers\ProductSaleManager;
use App\Models\Product\Product;
use App\Models\Product\ProductSale;
use App\Models\User;
use Auth;
use Config;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use SnappyPDF;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ProductSaleController extends Controller
{
    /** @var User */
    private $userRepository;

    /** @var ProductSale */
    private $saleRepository;

    public function __construct(User $userRepository, ProductSale $saleRepository)
    {
        $this->userRepository = $userRepository;
        $this->saleRepository = $saleRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param         $userId
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $userId)
    {
        $includes = ['product', 'user', 'reseller', 'price'];

        $user = $this->userRepository->findOrFail($userId);

        $paginator = $this->parseRequest($request, $user)->with($includes)->paginate();
        $sales = $paginator->items();

        $data = fractal()
            ->collection($sales, new ProductSaleTransformer())
            ->parseIncludes($includes)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductSaleRequest $request
     * @param ProductSaleManager $saleManager
     * @param int $userId
     * @return \Illuminate\Http\Response
     */
    public function store(ProductSaleRequest $request, ProductSaleManager $saleManager, $userId)
    {
        $includes = ['product', 'user', 'reseller', 'price'];

        $user = Auth::user();

        if ($user->id !== (int) $userId) {
            throw new AccessDeniedHttpException();
        }

        $productId = $request->request->getInt('product_id');
        $product = Product::find($productId);

        $sale = $saleManager->buyProduct($product, $user, ['quantity' => 1]);

        $data = fractal()
            ->item($sale, new ProductSaleTransformer(true))
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param $userId
     * @param $saleId
     * @return \Illuminate\Http\Response
     */
    public function show($userId, $saleId)
    {
        $includes = ['product', 'user', 'reseller', 'price'];

        $sale = $this->saleRepository->findOrFail($saleId);
        $user = Auth::user();

        if ($user->id != $userId || $sale->user->id != $userId) {
            throw new AccessDeniedHttpException();
        }

        $data = fractal()
            ->item($sale, new ProductSaleTransformer(true))
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data, 200, [], JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        throw new AccessDeniedHttpException();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        throw new AccessDeniedHttpException();
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @param User    $user
     * @return \Illuminate\Database\Query\Builder ;
     */
    private function parseRequest(Request $request, User $user)
    {
        $query = $user->productSales();

        $query->where('client_id', config('app.client')->id);

        // handle sort parameters
        $sortField = $request->query->get('_sortField', 'id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        $query->orderBy($sortField, $sortDir);

        // filter by ids
        if ($request->query->has('ids') && is_array($request->query->get('ids'))) {
            $ids = $request->query->get('ids');
            $query->whereIn('id', $ids);
        }

        return $query;
    }

    /**
     * @param $userId
     * @param $productSaleId
     * @return $this
     */
    public function pdf($userId, $productSaleId)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->id != $userId) {
            throw new AccessDeniedHttpException('');
        }
        // Generate ticket, merge and download
        $pdf = $this->generatePdf($productSaleId);
        return response()->make($pdf)->withHeaders([
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; product.pdf',
        ]);
    }

    /**
     * @param $productSaleId
     * @return string
     */
    private function generatePdf($productSaleId)
    {
        // Generate PDF
        $pdf = SnappyPDF::loadFile(route('pdf.product', ['product' => $productSaleId, 'demo' => false]));
        $pdf->setOptions([
            'custom-header' => [
                'X-Api-Key' => Config::get('app.client')->api_key,
            ],
            'margin-left' => 5,
            'margin-top'    => 5,
            'margin-right'  => 5,
            'margin-bottom' => 5,
        ]);

        return $pdf->output();
    }
}
