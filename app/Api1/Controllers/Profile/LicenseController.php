<?php

namespace App\Api1\Controllers\Profile;

use App\Api1\Requests\StoreLicenseRequest;
use App\Api1\Transformers\LicenseTransformer;
use App\Events\LicenseChanged;
use App\Exceptions\ProfileNotCompleteException;
use App\Http\Controllers\Controller;
use App\Models\License\License;
use App\Models\License\LicenseType;
use App\Models\Picture;
use App\Models\User;
use Auth;
use Event;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class LicenseController extends Controller
{
    /** @var User */
    private $userRepository;

    /** @var License */
    private $licenseRepository;

    public function __construct(User $userRepository, License $licenseRepository)
    {
        $this->userRepository = $userRepository;
        $this->licenseRepository = $licenseRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $userId
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $userId)
    {
        $user = $this->userRepository->findOrFail($userId);

        $paginator = $request->query->has('valid')
            ? $user->licenses()->valid($request->query->getBoolean('valid'))->paginate()
            : $user->licenses()->paginate();

        $licenses = $paginator->items();

        $data = fractal()
            ->collection($licenses, new LicenseTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLicenseRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLicenseRequest $request)
    {
        if (!Auth::user()->complete) {
            throw new ProfileNotCompleteException();
        }

        /** @var LicenseType $type */
        $typeId = $request->request->get('license_type_id');
        $type = LicenseType::findOrFail($typeId);

        $license = new License();
        $license->user()->associate(Auth::user());
        $license->type()->associate($type);

        $fields = [];

        if ($request->request->has('fields')) {
            foreach ($request->request->get('fields') as $field => $value) {
                if (isset($type->fields[$field])) {
                    switch ($type->fields[$field]) {
                        case 'from':
                            $license->valid_from = $value;
                            $fields[$field] = $value;
                            break;
                        case 'till':
                            $license->valid_to = $value;
                            $fields[$field] = $value;
                            break;
                        default:
                            $fields[$field] = $value;
                            break;
                    }
                }
            }
        }

        $license->fields = $fields;
        $license->save();

        if ($request->request->has('attachments')) {
            foreach ($request->request->get('attachments') as $attachment) {
                $picture = Picture::findOrFail($attachment);

                if (Auth::user()->id !== $picture->author_id) {
                    return response()->json(['error' => 'One or more attachments do not belong to this user.'], 422);
                }

                $license->attachments()->attach($attachment);
            }
        }

        Event::fire(new LicenseChanged($license, config('app.client')));

        $data = fractal()
            ->item($license, new LicenseTransformer())
            ->parseIncludes(['user', 'type', 'attachments'])
            ->toArray();

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // NOPE
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
