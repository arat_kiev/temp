<?php

namespace App\Api1\Controllers\Profile;

use App\Api1\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class UserFollowsController extends Controller
{

    private $userRepository;
    private $followType;

    public function __construct(User $userRepository)
    {
        $this->followType = \Illuminate\Support\Facades\Request::segment(4);
        $this->userRepository = $userRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $userQuery = Auth::user()->{$this->followType}();

        $paginator = $userQuery
            ->paginate($request->get('_perPage', $this->userRepository->getPerPage()))
            ->appends($request->except('page'));

        $data = fractal()
            ->collection($paginator->items(), new UserTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $user_id = $request->input('user_id');
        if (!User::find($user_id)->followed()->wherePivot('user_id', '=', Auth::id())->first()) {
            Auth::user()->{$this->followType}()->attach($user_id);
            return response()->json(array(), 201);
        }
        return response()->json(['error' => 'User has already been `followed` or doesn\'t exist'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param $user_id
     * @param $follower_id
     * @return mixed
     */
    public function show($user_id, $follower_id)
    {
        $user = Auth::user()->{$this->followType}()->where('id', $follower_id)->firstOrFail();

        $data = fractal()
            ->item($user, new UserTransformer(true))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage
     */
    public function update()
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user_id
     * @param $followed_user_id
     * @return mixed
     */
    public function destroy($user_id, $followed_user_id)
    {
        if (Auth::user()->follows()->wherePivot('follower_id', '=', $followed_user_id)->first()) {
            Auth::user()->{$this->followType}()->detach($followed_user_id);
            return response()->json(array());
        }
        return response()->json(['error' => 'User hasn\'t been `followed`'], 500);
    }
}
