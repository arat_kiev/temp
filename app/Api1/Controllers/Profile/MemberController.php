<?php

namespace App\Api1\Controllers\Profile;

use App\Api1\Requests\StoreFavoriteRequest;
use App\Api1\Transformers\AreaTransformer;
use App\Http\Controllers\Controller;
use App\Models\Area\Area;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class MemberController extends Controller
{
    /** @var User */
    private $userRepository;

    public function __construct(User $userRepository)
    {
        $this->userRepository = $userRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $userId
     * @return Response
     */
    public function index($userId)
    {
        $user = $this->userRepository->findOrFail($userId);

        $paginator = Area::isMember($user)->paginate();
        $areas = $paginator->items();

        $data = fractal()
            ->collection($areas, new AreaTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFavoriteRequest $request
     * @param $userId
     * @return Response
     */
    public function store(StoreFavoriteRequest $request, $userId)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $userId
     * @param $areaId
     * @return Response
     */
    public function destroy($userId, $areaId)
    {
        //
    }
}
