<?php

namespace App\Api1\Controllers\Profile;

use App\Api1\Transformers\FishTransformer;
use App\Http\Controllers\Controller;
use App\Models\Meta\Fish;
use Auth;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class FishController extends Controller
{

    private $fishRepository;

    public function __construct(Fish $fishRepository)
    {
        $this->fishRepository = $fishRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     * @param $user_id
     * @return mixed
     */
    public function index($user_id)
    {
        $paginator = Auth::user()->fishes()->paginate();
        $fish = $paginator->items();

        $data = fractal()
            ->collection($fish, new FishTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $new_values = (array)$request->input('id');

        Auth::user()->fishes()->sync($new_values, false);

        return response()->json();
    }

    /**
     * Display the specified resource.
     * @param $user_id
     * @param $id
     * @return mixed
     */
    public function show($user_id, $id)
    {
        if (Auth::user()->fish()->where('id', $id)->exists()) {
            $fish = Auth::user()->fish()->where('id', $id)->first();
            $data = fractal()->item($fish, new FishTransformer(true))->toArray();
            return response()->json($data);
        }
        abort(400, 'Invalid input data');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $user_id
     * @return mixed
     */
    public function update(Request $request, $user_id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param $user_id
     * @param $id
     * @return mixed
     */
    public function destroy($user_id, $id)
    {
        if (Auth::user()->fish()->where('id', $id)->exists()) {
            Auth::user()->fish()->detach($id);
            return response()->json();
        }
        abort(400, 'Invalid input data');
    }
}
