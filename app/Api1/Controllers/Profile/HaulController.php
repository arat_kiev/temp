<?php

namespace App\Api1\Controllers\Profile;

use App\Api1\Requests\StoreHaulRequest;
use App\Api1\Requests\UpdateHaulRequest;
use App\Api1\Transformers\HaulTransformer;
use App\Exceptions\ModelException;
use App\Http\Controllers\Controller;
use App\Models\Area\Area;
use App\Models\Haul;
use App\Models\Ticket\Ticket;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class HaulController extends Controller
{
    /** @var User */
    private $userRepository;

    /** @var Haul */
    private $haulRepository;

    public function __construct(User $userRepository, Haul $haulRepository)
    {
        $this->userRepository = $userRepository;
        $this->haulRepository = $haulRepository;

        $this->middleware('jwt.auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param $userId
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $userId)
    {
        $includes = ['area', 'fish', 'technique', 'picture'];

        $user = $this->userRepository->findOrFail($userId);

        $haulsQuery = $this->parseRequest($request, $user);
        $paginator = $haulsQuery->with($includes)
            ->paginate($request->get('_perPage', $this->haulRepository->getPerPage()))
            ->appends($request->except('page'));
        $hauls = $paginator->items();

        $data = fractal()
            ->collection($hauls, new HaulTransformer())
            ->parseIncludes($includes)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreHaulRequest $request
     * @return \Illuminate\Http\Response
     * @throws ModelException
     */
    public function store(StoreHaulRequest $request, $userId)
    {
        $user = $this->checkAllowedUser($userId);
        $areaId = $request->request->getInt('area_id');
        $ticketId = $request->request->getInt('ticket_id');

        if (!$this->checkIfAllowToCreateEmptyHaul($request, $user)) {
            throw new ModelException('Leermeldung ist bereits vorhanden');
        }

        if (!$areaId || !($area = Area::find($areaId))) {
            throw new ModelException('Konnte das Gewässer nicht finden');
        }

        if (!$ticketId || !($ticket = Ticket::find($ticketId))) {
            throw new ModelException('Konnte die Angelkarte nicht finden');
        }

        if ($ticket->type->area->id !== $areaId && !$ticket->type->additionalAreas->has($areaId)) {
            throw new ModelException('Angelkarte ist nicht für dieses Gewässer');
        }

        if (!$request->request->has('fish_id')) {
            $request->request->set('user_comment', 'Leermeldung');
        }

        if (!$request->request->has('ticket_number')) {
            $request->request->set('ticket_number', '');
        }

        /** @var $user User */
        $newHaul = $user->hauls()->create($request->request->all());

        $includes = ['area', 'fish', 'technique', 'picture'];

        $data = fractal()
            ->item($newHaul, new HaulTransformer())
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data);
    }

    private function checkIfAllowToCreateEmptyHaul(StoreHaulRequest $request, $user)
    {
        if ($request->get('fish_id', null)) {
            return true;
        }

        $haulCount = $user->hauls()
            ->where(function ($query) use ($request) {
                if ($request->get('ticket_id', null)) {
                    $query->where('ticket_id', $request->get('ticket_id', null));
                } else {
                    $query->where('ticket_number', $request->get('ticket_number', null));
                }
            })
            ->where('catch_date', $request->get('catch_date', date('Y-m-d')))
            ->count();

        return $haulCount ? false : true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateHaulRequest $request
     * @param $userId
     * @param $haulId
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(UpdateHaulRequest $request, $userId, $haulId)
    {
        $user = $this->checkAllowedUser($userId);

        if (!$request->request->has('ticket_number')) {
            $request->request->set('ticket_number', '');
        }

        /** @var Haul $haul */
        $haul = Haul::findOrFail($haulId);
        $this->checkUserIsHaulOwner($user, $haul);

        $haul->update($request->request->all());

        $includes = ['area', 'fish', 'technique', 'picture'];

        $haul = $haul->fresh($includes);

        $data = fractal()
            ->item($haul, new HaulTransformer())
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $userId
     * @param $haulId
     * @return \Illuminate\Http\Response
     */
    public function destroy($userId, $haulId)
    {
        $user = $this->checkAllowedUser($userId);
        $haul = Haul::findOrFail($haulId);

        $this->checkUserIsHaulOwner($user, $haul);

        if ($this->isModificationAvailable($haul)) {
            $haul->delete();
        } else {
            throw new AccessDeniedHttpException();
        }
    }

    public function check($userId, $haulId)
    {
        $user = $this->checkAllowedUser($userId);
        $haul = Haul::findOrFail($haulId);

        $this->checkUserIsHaulOwner($user, $haul);

        return response()->json([
            'isEditable' => $this->isModificationAvailable($haul),
        ]);
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Database\Query\Builder ;
     */
    private function parseRequest(Request $request, User $user)
    {
        $haulQuery = $user->hauls()
            ->where('status', 'accepted')
            ->whereNotNull('fish_id')
            ->whereHas('area.clients', function ($query) {
                $query->where('id', config('app.client')->id);
            });

        // handle sort parameters
        $sortField = $request->query->get('_sortField', 'id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        if (in_array($sortField, ['id', 'catch_date', 'created_at']) &&
            in_array($sortDir, ['asc', 'desc'])
        ) {
            $haulQuery->orderBy($sortField, $sortDir);
        }
        // filter hauls by catch date
        if ($request->query->has('catch_from')) {
            $catch_from = $request->query->get('catch_from');
            $haulQuery->where('catch_date', '>=', $catch_from);
        }

        if ($request->query->has('catch_till')) {
            $catch_till = $request->query->get('catch_till');
            $haulQuery->where('catch_date', '<=', $catch_till);
        }

        //filter hauls by size
        if ($request->query->has('min_length')) {
            $min_length = $request->query->get('min_length');
            $haulQuery->where('size_value', '>=', $min_length)
                ->where('size_type', '=', 'CM');
        }

        if ($request->query->has('max_length')) {
            $max_length = $request->query->get('max_length');
            $haulQuery->where('size_value', '<=', $max_length)
                ->where('size_type', '=', 'CM');
        }

        // filter if fish is empty
        if ($request->query->has('fish')) {
            $fish = $request->query->getBoolean('fish');
            $haulQuery->hasFish($fish);
        }
        // filter by certain fishes
        if ($request->query->has('fishes')) {
            $fishes = $request->query->get('fishes');
            $haulQuery->hasFishes($fishes);
        }

        // filter if haul is taken
        if ($request->query->has('taken')) {
            $taken = $request->query->getBoolean('taken');
            $haulQuery->isTaken($taken);
        }

        // if has picture search parameter
        if ($request->query->has('picture')) {
            $picture = $request->query->getBoolean('picture');
            $haulQuery->hasPicture($picture);
        }

        // filter by certain techniques
        if ($request->query->has('tech')) {
            $techniques = $request->query->get('tech');
            $haulQuery->hasTechnique($techniques);
        }

        // filter by certain area
        if ($request->query->has('area')) {
            $area = $request->query->get('area');
            $haulQuery->hasArea($area);
        }

        // get only public records for hauls non owners
        if ((!Auth::check() || $user->id != Auth::id()) && !Auth::user()->hasRole('superadmin')) {
            $haulQuery->where('public', 1);
        }



        return $haulQuery;
    }

    private function isModificationAvailable(Haul $haul) : bool
    {
        $now = Carbon::now();

        return $now->diffInWeeks($haul->created_at) < 4;
    }

    private function checkAllowedUser($userId)
    {
        if (!($user = Auth::user()) || $user->id != $userId) {
            throw new AccessDeniedHttpException();
        }

        return $user;
    }

    private function checkUserIsHaulOwner(User $user, Haul $haul)
    {
        if (($user->id !== $haul->user->id) && !$user->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        }
    }
}
