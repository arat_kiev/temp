<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\ProductTransformer;
use App\Models\Product\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class ProductController extends Controller
{
    /** @var Product */
    private $productRepository;

    /**
     * @param Product $productRepository
     */
    public function __construct(Product $productRepository)
    {
        $this->productRepository = $productRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $productQuery = $this->parseRequest($request);

        $paginator = $productQuery->with(['managers', 'areas', 'prices', 'productStocks'])
            ->paginate($request->get('_perPage', $this->productRepository->getPerPage()))
            ->appends($request->except('page'));

        $products = $paginator->items();

        $data = fractal()
            ->collection($products, new ProductTransformer())
            ->includeProductStocks()
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $includes = ['prices', 'category', 'productStocks'];

        /** @var Product $product */
        $product = $this->productRepository->with($includes)->findOrFail($id);

        $data = fractal()
            ->item($product, new ProductTransformer(true))
            ->parseIncludes(array_merge($includes, ['image', 'related_products']))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseRequest(Request $request)
    {
        $productQuery = $this->productRepository
            ->doesntHave('managers');

        // handle sort parameters
        if ($request->query->has('_sortField') && $request->query->has('_sortDir')) {
            $sortField = $request->query->get('_sortField', 'id');
            $sortDir = $request->query->get('_sortDir', 'asc');

            $productQuery->orderBy($sortField, $sortDir);
        }

        // Filter by name
        if ($request->query->has('name') && is_string($request->query->get('name'))
            || $request->query->has('search') && is_string($request->query->get('search'))) {
            $name = $request->query->get('name', $request->query->get('search', ''));
            $productQuery->whereTranslationLike('name', "%$name%");
        }

        // Filter by featured_from
        if ($request->query->has('featured_from')) {
            $featuredFrom = $request->query->get('featured_from');
            $productQuery->where('featured_from', '>=', $featuredFrom);
        }

        // Filter by featured_till
        if ($request->query->has('featured_till')) {
            $featuredTill = $request->query->get('featured_till');
            $productQuery->where('featured_till', '<=', $featuredTill);
        }

        // Filter by categories
        if ($request->query->has('categories') && is_array($request->query->get('categories'))) {
            $categories = $request->query->get('categories');
            $productQuery->hasCategories($categories);
        }

        // Filter by downloadable
        if ($request->query->has('downloadable')) {
            if ($request->query->getBoolean('downloadable')) {
                $productQuery->whereNotNull('attachment');
            } else {
                $productQuery->whereNull('attachment');
            }
        }

        // Filter by "in stock" flag
        if ($request->query->has('in_stock')) {
            if ($request->query->getBoolean('in_stock')) {
                $productQuery->where(function ($query) {
                    $query
                        ->whereHas('productStocks', function ($stock) {
                            $stock->where('quantity', '>=', 0);
                        }); //TODO: include products without Stock
                });
            } else {
                $productQuery->whereHas('productStocks', function ($stock) {
                    $stock->where('quantity', 0);
                });
            }
        }

        // Filter by price.value
        if ($request->query->has('price_values') && is_array($request->query->get('price_values'))) {
            $prices = $request->query->get('price_values');
            $productQuery->hasPriceValues($prices);
        }

        // Filter by Ids
        if ($request->query->has('ids') && is_array($request->query->get('ids', []))) {
            $ids = $request->query->get('ids', []);
            $productQuery->whereIn('id', $ids);
        }

        // Filter by managers
        if ($request->query->has('manager')) {
            $managerId = $request->query->getInt('manager');
            $productQuery->whereHas('managers', function ($managers) use ($managerId) {
                $managers->whereId($managerId);
            });
        }

        // Filter by active prices
        if ($request->query->getBoolean('only_active', true)) {
            $productQuery->hasActivePrice();
        }

        $productQuery->groupBy('products.id');

        return $productQuery;
    }
}
