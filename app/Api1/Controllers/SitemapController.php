<?php

namespace App\Api1\Controllers;

use App\Api1\Sitemaps\AreaSitemap;
use App\Api1\Sitemaps\CountrySitemap;
use App\Api1\Sitemaps\GroupAreaSitemap;
use App\Api1\Sitemaps\RegionSitemap;
use App\Api1\Sitemaps\StateSitemap;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SitemapController extends Controller
{
    const MAX_PER_PAGE = 15;

    public static $types = [
        'areas' => AreaSitemap::class,
        'group-areas' => GroupAreaSitemap::class,
        'landing-pages' => [
            'countries' => CountrySitemap::class,
            'states' => StateSitemap::class,
            'regions' => RegionSitemap::class,
        ],
    ];

    public function show(Request $request, $type, $subType = null)
    {
        if (!array_key_exists($type, self::$types)) {
            throw new BadRequestHttpException();
        }

        if (is_array(self::$types[$type])) {
            if (!array_key_exists($subType, self::$types[$type])) {
                throw new BadRequestHttpException();
            }

            $sitemap = new self::$types[$type][$subType];
        } else {
            $sitemap = new self::$types[$type];
        }

        $params = $request->except('page');

        $query = $sitemap->getQuery($params);
        $paginator = $query
            ->paginate($request->get('_perPage', self::MAX_PER_PAGE))
            ->appends($params);

        $items = $paginator->items();

        $data = fractal()
            ->collection($items, $sitemap->getTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

}