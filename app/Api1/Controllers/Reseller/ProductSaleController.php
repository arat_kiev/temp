<?php

namespace App\Api1\Controllers\Reseller;

use App\Api1\Requests\ResellerProductSaleRequest;
use App\Api1\Transformers\ResellerProductSaleTransformer;
use App\Events\ProductSaleCreated;
use App\Http\Controllers\Controller;
use App\Managers\ProductSaleManager;
use App\Models\Contact\Reseller;
use App\Models\Product\Product;
use App\Models\Product\ProductSale;
use Auth;
use Carbon\Carbon;
use Config;
use Event;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use SnappyPDF;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ProductSaleController extends Controller
{
    /** @var Reseller */
    private $resellerRepository;

    /** @var ProductSale */
    private $saleRepository;

    /**
     * @param Reseller $resellerRepository
     */
    public function __construct(Reseller $resellerRepository, ProductSale $saleRepository)
    {
        $this->resellerRepository = $resellerRepository;

        $this->saleRepository = $saleRepository;

        $this->middleware('jwt.auth', ['except']);
    }

    /**
     * @param ResellerProductSaleRequest $request
     * @param $resellerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ResellerProductSaleRequest $request, $resellerId)
    {

        try {
            $from = Carbon::parse($request->query->get('from', '1970-01-01'));
            $till = Carbon::parse($request->query->get('till', Carbon::now()))->endOfDay();
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Invalid time range or data',
            ], 400);
        }

        $includes = ['product', 'user', 'reseller', 'price', 'timeslotDates'];

        $reseller = $this->resellerRepository->findOrFail($resellerId);
        $salesQuery = $this->parseRequest($request, $reseller);
        $totalSum = $this->totalSalesSum($salesQuery) / 100;
        $paginator = $salesQuery->with($includes)->paginate($request->get('_perPage', $this->resellerRepository->getPerPage()));

        $sales = $paginator->items();

        $data = fractal()
            ->collection($sales, new ResellerProductSaleTransformer())
            ->parseIncludes($includes)
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->addMeta([
                'from' => $from->toDateString(),
                'till' => $till->toDateString(),
                'total_sum' => $totalSum,
                'page_sum' => collect($sales)->sum(function ($sale) {
                        return $sale->price->value_origin;
                    }) / 100,
            ])
            ->toArray();

        return response()->json($data);
    }

    public function show($resellerId, $saleId)
    {
        /** @var \App\Models\User $user */
        $user = Auth::user();

        if (!$user->resellers->contains('id', $resellerId)) {
            throw new AccessDeniedHttpException('Not admin of this reseller');
        }

        $includes = ['product', 'user', 'reseller', 'price'];

        $sale = $this->saleRepository->findOrFail($saleId);

        if($sale->reseller_id != $resellerId)
        {
            throw new AccessDeniedHttpException('Not admin of this sale');
        }

        $data = fractal()
            ->item($sale, new ResellerProductSaleTransformer(true))
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data, 200, [], JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR);
    }

    public function destroy(ResellerProductSaleRequest $request, $resellerId, $saleId, ProductSaleManager $productSaleManager)
    {
        /** @var \App\Models\User $user */
        $user = Auth::user();

        if (!$user->resellers->contains('id', $resellerId)) {
            throw new AccessDeniedHttpException('Not admin of this reseller');
        }

        $sale = $this->saleRepository->findOrFail($saleId);

        if($sale->reseller_id != $resellerId)
        {
            throw new AccessDeniedHttpException('Not admin of this sale');
        }

        $productSaleManager->createStornoSale($sale, $request->request->get('reason'));

        return response()->json('', 204);


    }
    /**
     * Store a newly created resource in storage
     *
     * @param ResellerProductSaleRequest $request
     * @param ProductSaleManager         $saleManager
     * @param                            $resellerId
     * @return \Illuminate\Http\Response
     * @throws AccessDeniedHttpException
     */
    public function store(ResellerProductSaleRequest $request, ProductSaleManager $saleManager, $resellerId)
    {
        /** @var \App\Models\User $user */
        $user = Auth::user();

        if (!$user->resellers->contains('id', $resellerId)) {
            throw new AccessDeniedHttpException('Not admin of this reseller');
        }

        /** @var Reseller $reseller */
        $reseller = $this->resellerRepository->findOrFail($resellerId);

        // parse input parameters
        $product = Product::findOrFail($request->request->getInt('product_id'));
        $userData = $request->except(['product_id']);

        // create reseller product sale
        $productSale = $saleManager->buyResellerProductSale($product, $reseller, $userData);

        Event::fire(new ProductSaleCreated($productSale, config('app.client')));

        $data = fractal()
            ->item($productSale, new ResellerProductSaleTransformer())
            ->toArray();

        return response()->json($data, 201);
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param ResellerProductSaleRequest $request
     * @param Reseller $reseller
     * @return \Illuminate\Database\Query\Builder ;
     */
    private function parseRequest(Request $request,Reseller $reseller)
    {
        $query = $reseller->productSales();

        // handle sort parameters
        $sortField = $request->query->get('_sortField', 'product_prices.id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        $query->orderBy($sortField, $sortDir);

        // filter by ids
        if ($request->query->has('ids') && is_array($request->query->get('ids'))) {
            $ids = $request->query->get('ids');
            $query->whereIn('id', $ids);
        }

        /* search first or lastname
        * TODO: remove after frontend update */

        if ($request->query->has('search')) {
            $search = $request->query->get('search');
            $query->Where('first_name', 'like', '%' . $search . '%')
                ->orWhere('last_name', 'like', '%' . $search . '%');
        }

        // filter by first name or last name
        if ($request->query->has('firstName') || $request->query->has('lastName')) {
            $firstName = $request->query->get('firstName');
            $lastName = $request->query->get('lastName');

            $query->where(function ($query) use ($firstName, $lastName) {
                $query->where('first_name', 'like', '%' . $firstName . '%')
                    ->where('last_name', 'like', '%' . $lastName . '%');
            });
        }

        // filter from date
        if ($request->query->has('from')) {
            $from = $request->query->get('from');
            $query->where('created_at', '>', $from);
        }

        // filter till date
        if ($request->query->has('till')) {
            $till = $request->query->get('till');
            $query->where('created_at', '>', $till);
        }

        return $query;
    }

    /**
     * @param $resellerId
     * @param $productSaleId
     * @return $this
     */
    public function pdf($resellerId, $productSaleId)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->hasRole('superadmin') && !$user->resellers->contains('id', $resellerId)) {
            throw new AccessDeniedHttpException('No admin of this reseller');
        }

        // Generate ticket, merge and download
        $pdf = $this->generatePdf($resellerId, $productSaleId);

        return response()->make($pdf)->withHeaders([
            'Content-Type'          => 'application/pdf',
            'Content-Disposition'   => 'inline; product.pdf',
        ]);
    }

    /**
     * @param $query
     * @return mixed
     */
    private function totalSalesSum($query)
    {
        return $query->join('product_prices', 'product_prices.id', '=', 'product_sales.price_id')
            ->sum('value');
    }
    /**
     * @param $resellerId
     * @param $productSaleId
     * @return mixed
     */
    private function generatePdf($resellerId, $productSaleId)
    {
        // Generate PDF
        $pdf = SnappyPDF::loadFile(route('pdf.product', ['product' => $productSaleId, 'demo' => false]));
        $pdf->setOptions([
            'custom-header' => [
                'X-Api-Key'     => Config::get('app.client')->api_key,
            ],
            'margin-left'   => 5,
            'margin-top'    => 5,
            'margin-right'  => 5,
            'margin-bottom' => 5,
        ]);

        info('reseller pdf generated', [
            'product'    => $productSaleId,
            'reseller'  => $resellerId,
        ]);

        return $pdf->output();
    }
}