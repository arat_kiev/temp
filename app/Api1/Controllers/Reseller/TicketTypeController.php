<?php

namespace App\Api1\Controllers\Reseller;

use App\Api1\Transformers\Reseller\TicketTypeTransformer;
use App\Http\Controllers\Controller;
use App\Models\Contact\Reseller;
use App\Models\Ticket\TicketType;
use Carbon\Carbon;
use HttpInvalidParamException;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TicketTypeController extends Controller
{
    /** @var Reseller */
    private $resellerRepository;

    /** @var TicketType */
    private $ticketTypeRepository;

    /**
     * @param Reseller $resellerRepository
     * @param TicketType $ticketTypeRepository
     */
    public function __construct(Reseller $resellerRepository, TicketType $ticketTypeRepository)
    {
        $this->resellerRepository = $resellerRepository;
        $this->ticketTypeRepository = $ticketTypeRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param $resellerId
     * @return \Illuminate\Http\Response
     * @throws HttpInvalidParamException
     */
    public function index(Request $request, $resellerId)
    {
        if ($request->user()->resellers()->first()->id !== (int)$resellerId) {
            throw new AccessDeniedHttpException('Not an admin of this reseller');
        }

        if (!$areaId = $request->query->getInt('area_id')) {
            throw new BadRequestHttpException('missing area_id');
        }

        $reseller = $this->resellerRepository->findOrFail($resellerId);

        $ticketTypes = $this->ticketTypeRepository->newQuery()
            ->with([
                'quotaUnit',
                'checkinUnit',
                'requiredLicenseTypes',
                'optionalLicenseTypes',
                'category.translations',
            ])
            ->selectRaw('ticket_types.*, COUNT(ticket_prices.id) as ticket_prices_count')
            ->join('ticket_prices', 'ticket_types.id', '=', 'ticket_prices.ticket_type_id')
            ->join('reseller_ticket_prices', 'ticket_prices.id', '=', 'reseller_ticket_prices.ticket_price_id')
            ->where('reseller_ticket_prices.reseller_id', '=', $reseller->id)
            ->where('ticket_types.area_id', '=', $areaId)
            ->whereRaw('? BETWEEN visible_from AND valid_to', [Carbon::now()->toDateString()])
            ->groupBy('ticket_types.id')
            ->get();

        $ticketTypes->each(function (&$type) {
            $transformer = new TicketTypeTransformer();
            $period = -1;

            switch ($type->category->id) {
                case 1:
                    $period = ((float)$type->duration) / 24;
                    break;
                case 2:
                    $period = $type->duration;
                    break;
                case 3:
                    $period = $type->duration * 7;
                    break;
                case 4:
                    $period = $type->duration * 30;
                    break;
                case 5:
                    $period = 100;
            }

            if ($period <= 3) {
                $type->duration_category = 0;
            } elseif ($period <= 14) {
                $type->duration_category = 1;
            } elseif ($period <= 60) {
                $type->duration_category = 2;
            } else {
                $type->duration_category = 3;
            }
        });

        $data = fractal()
            ->collection($ticketTypes, new TicketTypeTransformer())
            ->toArray();

        return response()->json($data);
    }
}
