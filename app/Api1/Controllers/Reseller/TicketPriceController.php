<?php

namespace App\Api1\Controllers\Reseller;

use App;
use App\Api1\Transformers\TicketPriceTransformer;
use Cache;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact\Reseller;
use App\Models\Ticket\TicketPrice;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class TicketPriceController extends Controller
{
    /** @var Reseller */
    private $resellerRepository;

    /** @var TicketPrice */
    private $ticketPriceRepository;

    public function __construct(Reseller $resellerRepository, TicketPrice $ticketPriceRepository)
    {
        $this->resellerRepository = $resellerRepository;
        $this->ticketPriceRepository = $ticketPriceRepository;

        $this->middleware('jwt.auth');
    }

    public function index(Request $request, $resellerId)
    {
        if ($request->user()->resellers()->first()->id !== (int)$resellerId) {
            throw new AccessDeniedHttpException('Not an admin of this reseller');
        }

        $reseller = $this->resellerRepository->findOrFail($resellerId);

        $cacheKey = $this->getIndexCacheKey($request, $resellerId);

        $data = Cache::remember($cacheKey, 60, function () use ($reseller, $request) {
            $ticketPriceQuery = $this->parseRequest($request, $reseller);

            if ($ticketTypeId = $request->query->getInt('ticket_type_id')) {
                $ticketPriceQuery->where('ticket_prices.ticket_type_id', '=', $ticketTypeId);
            }

            $ticketPrices = $ticketPriceQuery->get();

            return fractal()
                ->collection($ticketPrices, new TicketPriceTransformer())
                ->toArray();
        });

        return response()->json($data);
    }

    /**
     * Generate cache key according to request params
     *
     * @param Request $request
     * @return string
     */
    private function getIndexCacheKey(Request $request, $resellerId)
    {
        $cacheHash = md5(json_encode([
            $request->query->get('ticket_type_id'),
            $request->query->get('orderBy'),
            $request->query->get('orderDir', 'asc'),
            $resellerId,
        ]));

        return implode('_', [
            'reseller_ticket_prices',
            App::getLocale(),
            $cacheHash,
        ]);
    }

    private function parseRequest(Request $request, Reseller $reseller)
    {
        $query = $reseller->ticketPrices()->active()
            ->with(['ticketType.area', 'weekdays', 'lockPeriods']);

        if ($request->query->has('orderBy')) {
            $orderDir = $request->query->get('orderDir', 'asc');

            switch ($request->query->get('orderBy')) {
                case 'area_name':
                    $query->addSelect([
                        'ticket_prices.*',
                        'areas.name as area_name',
                    ])
                        ->join('ticket_types as ticketTypes', 'ticketTypes.id', '=', 'ticket_prices.ticket_type_id')
                        ->join('areas', 'areas.id', '=', 'ticketTypes.area_id')
                        ->orderBy('area_name', $orderDir)
                        ->orderBy('value', $orderDir);
                case 'price':
                    $query->orderBy('value', $orderDir);
            }
        }

        return $query;
    }

}