<?php

namespace App\Api1\Controllers\Reseller;

use App\Api1\Transformers\ProductTransformer;
use App\Models\Area\Area;
use App\Models\Contact\Reseller;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ProductController extends Controller
{
    /** @var Reseller */
    private $resellerRepository;

    /** @var Area */
    private $areaRepository;

    public function __construct(Reseller $resellerRepository, Area $areaRepository)
    {
        $this->resellerRepository = $resellerRepository;
        $this->areaRepository = $areaRepository;

        $this->middleware('jwt.auth');
    }

    public function index(Request $request, $resellerId)
    {
        if ($request->user()->resellers()->first()->id !== (int)$resellerId) {
            throw new AccessDeniedHttpException('Not an admin of this reseller');
        }

        if (!$areaId = $request->query->getInt('area_id')) {
            throw new BadRequestHttpException('missing area_id');
        }

        $area = $this->areaRepository->findOrFail($areaId);

        $data = fractal()
            ->collection($area->products, new ProductTransformer())
            ->toArray();

        return response()->json($data);
    }
}