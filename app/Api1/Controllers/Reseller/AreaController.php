<?php

namespace App\Api1\Controllers\Reseller;

use App\Api1\Transformers\Reseller\AreaTransformer;
use App\Http\Controllers\Controller;
use App\Models\Area\Area;
use App\Models\Contact\Reseller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AreaController extends Controller
{
    /** @var Reseller */
    private $resellerRepository;

    /** @var  Area */
    private $areaRepository;

    /**
     * @param Reseller $resellerRepository
     * @param Area $areaRepository
     */
    public function __construct(Reseller $resellerRepository, Area $areaRepository)
    {
        $this->resellerRepository = $resellerRepository;
        $this->areaRepository = $areaRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $resellerId)
    {
        if ($request->user()->resellers()->first()->id !== (int)$resellerId) {
            throw new AccessDeniedHttpException('Not an admin of this reseller');
        }

        $reseller = $this->resellerRepository->findOrFail($resellerId);

        $areas = $this->areaRepository->newQuery()->setEagerLoads([])
            ->select(['areas.*', 'managers.name as manager', 'areas.id as id'])
            ->join('managers', 'areas.manager_id', '=', 'managers.id')
            ->join('ticket_types', 'areas.id', '=', 'ticket_types.area_id')
            ->join('ticket_prices', 'ticket_types.id', '=', 'ticket_prices.ticket_type_id')
            ->join('reseller_ticket_prices', 'ticket_prices.id', '=', 'reseller_ticket_prices.ticket_price_id')
            ->where('reseller_ticket_prices.reseller_id', '=', $reseller->id)
            ->whereRaw('? BETWEEN visible_from AND valid_to', [Carbon::now()->toDateString()])
            ->groupBy('areas.id')
            ->orderBy('areas.name')
            ->get();

        $data = fractal()
            ->collection($areas, new AreaTransformer())
            ->toArray();

        return response()->json($data);
    }
}
