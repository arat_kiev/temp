<?php

namespace App\Api1\Controllers\Reseller;

use App\Managers\TicketManager;
use App\Models\Contact\Reseller;
use App\Models\Ticket\TicketPrice;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TicketDateController extends Controller
{
    /** @var Reseller */
    private $resellerRepository;

    /** @var TicketPrice */
    private $ticketPriceRepository;

    public function __construct(Reseller $resellerRepository, TicketPrice $ticketPriceRepository)
    {
        $this->resellerRepository = $resellerRepository;
        $this->ticketPriceRepository = $ticketPriceRepository;

        $this->middleware('jwt.auth');
    }

    public function index(Request $request, TicketManager $ticketManager, $resellerId)
    {
        if ($request->user()->resellers()->first()->id !== (int)$resellerId) {
            throw new AccessDeniedHttpException('Not an admin of this reseller');
        }

        if (!$ticketPriceId = $request->query->getInt('ticket_price_id')) {
            throw new BadRequestHttpException('missing ticket_type_id');
        }

        $ticketPrice = $this->ticketPriceRepository->findOrFail($ticketPriceId);

        $list = $ticketManager->getValidDates($ticketPrice);
        $from = $ticketPrice->valid_from->toW3cString();
        $to = $ticketPrice->valid_to->toW3cString();

        return response()->json(compact('from', 'to', 'list'));
    }
}