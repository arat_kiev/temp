<?php

namespace App\Api1\Controllers\Reseller;

use App\Api1\Transformers\InvoiceTransformer;
use App\Http\Controllers\Controller;
use App\Models\Contact\Reseller;
use App\Models\Ticket\Invoice;
use App\Models\User;
use Auth;
use Config;
use iio\libmergepdf\Merger;
use SnappyPDF;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class InvoiceController extends Controller
{
    /** @var Reseller */
    private $resellerRepository;

    /**
     * @param Reseller $resellerRepository
     */
    public function __construct(Reseller $resellerRepository)
    {
        $this->resellerRepository = $resellerRepository;

        $this->middleware('jwt.auth', ['except']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($resellerId)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->resellers->contains('id', $resellerId)) {
            throw new AccessDeniedHttpException('No admin of this reseller');
        }

        $reseller = $this->resellerRepository->findOrFail($resellerId);
        $invoices = $reseller->invoices;

        $data = fractal()
            ->collection($invoices, new InvoiceTransformer())
            ->toArray();

        return response()->json($data);
    }

    public function show($resellerId, $invoiceId)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->resellers->contains('id', $resellerId)) {
            throw new AccessDeniedHttpException('No admin of this reseller');
        }

        $invoice = Invoice::findOrFail($invoiceId);

        if ($invoice->reseller_id != $resellerId) {
            throw new AccessDeniedHttpException('Ticket not from this reseller');
        }

        $includes = ['manager', 'reseller', 'tickets'];

        $data = fractal()
            ->item($invoice, new InvoiceTransformer())
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data);
    }

    public function pdf($resellerId, $invoiceId)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->hasRole('superadmin') && !$user->resellers->contains('id', $resellerId)) {
            throw new AccessDeniedHttpException('No admin of this reseller');
        }

        $invoice = Invoice::findOrFail($invoiceId);

        if ($invoice->reseller_id != $resellerId) {
            throw new AccessDeniedHttpException('Invoice not from this reseller');
        }

        // Generate invoice, merge and download
        $pdf = $this->generatePdf($resellerId, $invoiceId);

        return response()->make($pdf)->withHeaders([
            'Content-Type'          => 'application/pdf',
            'Content-Disposition'   => 'inline; ticket.pdf',
        ]);
    }

    private function generatePdf($resellerId, $invoiceId)
    {
        Invoice::findOrFail($invoiceId);

        // Generate PDF
        $pdf = SnappyPDF::loadFile(route('pdf.invoice', ['invoice' => $invoiceId]));
        $pdf->setOptions([
            'custom-header' => [
                'X-Api-Key'     => Config::get('app.client')->api_key,
            ],
            'margin-left'   => 5,
            'margin-top'    => 5,
            'margin-right'  => 5,
            'margin-bottom' => 5,
        ]);

        info('reseller pdf generated', [
            'invoice'   => $invoiceId,
            'reseller'  => $resellerId,
        ]);

        $merger = new Merger();
        $merger->addRaw($pdf->output());

        return $merger->merge();
    }
}