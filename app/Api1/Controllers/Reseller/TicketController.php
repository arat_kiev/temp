<?php

namespace App\Api1\Controllers\Reseller;

use App\Api1\Requests\ResellerTicketRequest;
use App\Api1\Transformers\ResellerTicketTransformer;
use App\Api1\Transformers\MemberTransformer;
use App\Api1\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use App\Managers\TicketManager;
use App\Models\Contact\Reseller;
use App\Models\Contact\Manager;
use App\Models\Ticket\ResellerTicket;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketPrice;
use App\Models\Organization\Member;
use App\Models\User;
use App\Blacklist\Blacklist;
use Auth;
use Carbon\Carbon;
use Config;
use DB;
use iio\libmergepdf\Merger;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use SnappyPDF;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class TicketController
 * @package App\Api1\Controllers\Reseller
 */
class TicketController extends Controller
{
    /** @var Reseller */
    private $resellerRepository;

    /**
     * @param Reseller $resellerRepository
     */
    public function __construct(Reseller $resellerRepository)
    {
        $this->resellerRepository = $resellerRepository;

        $this->middleware('jwt.auth', ['except']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param ResellerTicketRequest $request
     * @param $resellerId
     * @return \Illuminate\Http\Response
     */
    public function index(ResellerTicketRequest $request, $resellerId)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->resellers->contains('id', $resellerId)) {
            throw new AccessDeniedHttpException('No admin of this reseller');
        }

        try {
            $from = Carbon::parse($request->query->get('from', '1970-01-01'));
            $till = Carbon::parse($request->query->get('till', Carbon::now()))->endOfDay();
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Invalid time range or data',
            ], 400);
        }

        $reseller = $this->resellerRepository->findOrFail($resellerId);
        $ticketQuery = $this->parseRequest($request, $reseller)->whereHas('baseTicket');
        $totalSum = $this->totalTicketSum($ticketQuery) / 100;
        $paginator = $ticketQuery->paginate($request->get('_perPage', $this->resellerRepository->getPerPage()));
        $tickets = $paginator->items();

        $data = fractal()
            ->collection($tickets, new ResellerTicketTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->addMeta([
                'from' => $from->toDateString(),
                'till' => $till->toDateString(),
                'total_sum' => $totalSum,
                'page_sum' => collect($tickets)->sum(function ($ticket) {
                        return $ticket->baseTicket->price->value;
                    }) / 100,
            ])
            ->toArray();

        return response()->json($data);
    }

    public function show($resellerId, $ticketId)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->resellers->contains('id', $resellerId)) {
            throw new AccessDeniedHttpException('No admin of this reseller');
        }

        $ticket = ResellerTicket::findOrFail($ticketId);

        if ($ticket->reseller_id != $resellerId) {
            throw new AccessDeniedHttpException('Ticket not from this reseller');
        }

        $data = fractal()
            ->item($ticket, new ResellerTicketTransformer())
            ->includeTicket()
            ->toArray();

        return response()->json($data);
    }

    public function destroy(ResellerTicketRequest $request, $resellerId, $ticketId, TicketManager $ticketManager)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->resellers->contains('id', $resellerId)) {
            throw new AccessDeniedHttpException('No admin of this reseller');
        }

        $ticket = ResellerTicket::findOrFail($ticketId);

        if ($ticket->reseller_id != $resellerId) {
            throw new AccessDeniedHttpException('Ticket not from this reseller');
        }

        $ticketManager->createStornoTicket($ticket->baseTicket, $request->request->get('reason'));

        return response()->json('', 204);

    }

    /**
     * Retrieve User by fisher_id
     *
     * @param int $fisherId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserByFisherId($resellerId, $fisherId)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->resellers->contains('id', $resellerId)) {
            throw new AccessDeniedHttpException('No admin of this reseller');
        }

        $fisherId = strtoupper($fisherId);

        $user = User::where('fisher_id', $fisherId)->first();
        $member = Member::where('fisher_id', $fisherId)->first();
        $resellerTicket = ResellerTicket::where('fisher_id', $fisherId)->orderBy('ticket_id', 'desc')->first();

        if ($user) {
            $data = fractal()
                ->item($user, new UserTransformer(true))
                ->toArray();
            return response()->json($data);
        }
        if ($resellerTicket) {
            $data = fractal()
                ->item($resellerTicket, new ResellerTicketTransformer())
                ->toArray();
            return response()->json($data);
        }

        if ($member) {
            $data = fractal()
                ->item($member, new MemberTransformer())
                ->toArray();
            return response()->json($data);
        }

        return response()->json([]);
    }

    /**
     * @param $query
     * @return mixed
     */
    private function totalTicketSum($query)
    {
        return $query->join('tickets', 'tickets.id', '=', 'reseller_tickets.ticket_id')
            ->join('ticket_prices', 'ticket_prices.id', '=', 'tickets.ticket_price_id')
            ->sum('value');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ResellerTicketRequest $request
     * @param TicketManager $ticketManager
     * @param $resellerId
     * @param Blacklist $blacklist
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ResellerTicketRequest $request, TicketManager $ticketManager, $resellerId, Blacklist $blacklist)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->resellers->contains('id', $resellerId)) {
            throw new AccessDeniedHttpException('Not admin of this reseller');
        }

        // is user in blacklist
        if($blocked = $blacklist->isUserOnBlacklist($request->all())) {
            $manager = Manager::find($blocked->manager_id);
                return response()->json(['error' => "<b>Achtung:</b> Die eingegeben Daten erscheinen auf der Sperrliste des Bewirtschafters {$manager->name}! Bitte überprüfen und ggf. Bewirtschafter unter Telefon: " . ($manager->phone ? $manager->phone : '-') ." kontaktieren!"], 422);
        }

        try {
            return DB::transaction(function () use ($request, $ticketManager, $resellerId) {
                /** @var Reseller $reseller */
                $reseller = $this->resellerRepository->findOrFail($resellerId);

                // parse input parameters
                $fisherId = $request->request->get('fisher_id');
                $ticketPriceId = $request->request->getInt('ticket_price_id');
                $ticketPrice = TicketPrice::findOrFail($ticketPriceId);
                $startDate = Carbon::parse($request->request->get('date'));
                $userData = $request->except(['ticket_price_id', 'date']);

                if ($fisherId) {
                    //remove fisher_code from old ticket/s
                    ResellerTicket::where('fisher_id', $fisherId)->update(['fisher_id' => null]);
                } else {
                    $fisherId = User::uniqueFisherId();
                }

                // create reseller ticket
                $resellerTicket = $ticketManager->createResellerTicket($ticketPrice, $reseller, $userData, $startDate);
                $resellerTicket->fisher_id = $fisherId;
                $resellerTicket->save();

                // store additional users (group tickets)
                $ticket = $resellerTicket->baseTicket;
                $group_data = collect(array_values($request->request->get('group', [])));
                $ticket->group_data = $group_data->map(function ($p) {
                    return [
                        'first_name' => $p['first_name'],
                        'last_name' => $p['last_name'],
                        'birthday' => Carbon::createFromDate($p['birthday_year'], $p['birthday_month'], $p['birthday_day']),
                    ];
                });

                $ticket->save();

                $data = fractal()
                    ->item($resellerTicket, new ResellerTicketTransformer())
                    ->toArray();

                return response()->json($data, 201);
            });
        } catch (\Exception $e) {
            return response()->json(['error' => 'Datenbankproblem'], 400);
        }
    }

    public function pdf($resellerId, $ticketId)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->hasRole('superadmin') && !$user->resellers->contains('id', $resellerId)) {
            throw new AccessDeniedHttpException('No admin of this reseller');
        }

        $ticket = ResellerTicket::findOrFail($ticketId);

        if ($ticket->reseller_id != $resellerId) {
            throw new AccessDeniedHttpException('Ticket not from this reseller');
        }

        // Generate ticket, merge and download
        $pdf = $this->generatePdf($resellerId, $ticketId);

        return response()->make($pdf)->withHeaders([
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; ticket.pdf',
        ]);
    }

    private function generatePdf($resellerId, $ticketId)
    {
        $ticket = Ticket::findOrFail($ticketId);

        // Generate PDF
        $pdf = SnappyPDF::loadFile(route('pdf.ticket', ['ticket' => $ticketId, 'demo' => false]));
        $pdf->setOptions([
            'custom-header' => [
                'X-Api-Key' => Config::get('app.client')->api_key,
            ],
            'margin-left' => 5,
            'margin-top' => 5,
            'margin-right' => 5,
            'margin-bottom' => 5,
        ]);

        info('reseller pdf generated', [
            'ticket' => $ticketId,
            'reseller' => $resellerId,
        ]);

        $merger = new Merger();
        $merger->addRaw($pdf->output());

        if ($rule = $ticket->type->ruleWithFallback) {
            foreach ($rule->files as $file) {
                if (ends_with($file->fileWithFullPath, ['.pdf'])) {
                    $merger->addFile($file->fileWithFullPath);
                }
            }
        }

        return $merger->merge();
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param ResellerTicketRequest $request
     * @param Reseller $reseller
     * @return \Illuminate\Database\Query\Builder ;
     */
    private function parseRequest(ResellerTicketRequest $request, Reseller $reseller)
    {
        $query = $reseller->tickets();

        // handle sort parameters
        $sortField = $request->query->get('_sortField', 'ticket_id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        $query->orderBy($sortField, $sortDir);

        // filter by ids
        if ($request->query->has('ids') && is_array($request->query->get('ids'))) {
            $ids = $request->query->get('ids');
            $query->whereIn('id', $ids);
        }

        /* search first or lastname
        * TODO: remove after frontend update */

        if ($request->query->has('search')) {
            $search = $request->query->get('search');
            $query->where('first_name', 'like', '%' . $search . '%')
                ->orWhere('last_name', 'like', '%' . $search . '%');
        }

        // filter by first name or last name
        if ($request->query->has('firstName') || $request->query->has('lastName')) {
            $firstName = $request->query->get('firstName');
            $lastName = $request->query->get('lastName');

            $query->where(function ($query) use ($firstName, $lastName) {
                $query->where('first_name', 'like', '%' . $firstName . '%')
                    ->where('last_name', 'like', '%' . $lastName . '%');
            });
        }


        //filter by area
        if ($request->query->has('area')) {
            $area = $request->query->get('area');
            $query->whereHas('baseTicket', function ($baseTicket) use ($area) {
                $baseTicket->join('ticket_types', 'ticket_types.id', '=', 'tickets.ticket_type_id')
                    ->where('ticket_types.area_id','=',$area);
            });
        }

        // filter from date
        if ($request->query->has('from')) {
            $from = $request->query->get('from');
            $query->whereHas('baseTicket', function ($baseTicket) use ($from) {
                $baseTicket->where('valid_from', '>=', $from);
            });
        }

        // filter till date
        if ($request->query->has('till')) {
            $till = $request->query->get('till');
            $query->whereHas('baseTicket', function ($baseTicket) use ($till) {
                $baseTicket->where('valid_to', '<=', $till);
            });
        }

        return $query;
    }
}
