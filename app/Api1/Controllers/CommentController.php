<?php

namespace App\Api1\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\CommentFlags;
use App\Api1\Transformers\CommentTransformer;
use Illuminate\Database\Eloquent\Relations\Relation;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Api1\Requests\CommentRequest;
use App\Api1\Requests\CommentReportRequest;

class CommentController extends Controller
{
    private $availableEntities = [
        'areas',
        'hauls',
        'techniques',
        'fishes',
        'users',
        'points_of_interest',
    ];

    /** @var Comment */
    private $commentRepository;

    /**
     * @param Comment $commentRepository
     */
    public function __construct(Comment $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * Get comments per each entity
     *
     * @param CommentRequest $request
     * @param string         $entity
     * @param integer        $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(CommentRequest $request, $entity, $id)
    {
        $morphMap = Relation::morphMap();
        if (!in_array($entity, $this->availableEntities) || !in_array($entity, array_keys($morphMap))) {
            abort(404);
        }
        $comments = $this->commentRepository
            ->where('status', '=', $request->get('status', 'accepted'))
            ->where('commentable_id', '=', $id)
            ->where('commentable_type', '=', $entity);

        // handle sort parameters
        if ($request->query->has('_sortField') && $request->query->has('_sortDir')) {
            $sortField = $request->query->get('_sortField', 'created_at');
            $sortDir = $request->query->get('_sortDir', 'asc');

            $comments->orderBy($sortField, $sortDir);
        }

        $paginator = $comments
            ->paginate($request->get('_perPage', $this->commentRepository->getPerPage()))
            ->appends($request->except('page'));

        $comments = $paginator->items();

        $data = fractal()
            ->collection($comments, new CommentTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Add new comment about specific entity
     *
     * @param CommentRequest $request
     * @param string         $entity
     * @param integer        $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(CommentRequest $request, $entity, $id)
    {
        $morphMap = Relation::morphMap();
        if (!in_array($entity, $this->availableEntities) || !in_array($entity, array_keys($morphMap))) {
            abort(404);
        }
        $stripped = preg_replace('/(<.*?>)|(&.*?;)/', '', $request->body);

        $element = $morphMap[$entity]::findOrFail($id);
        $comment = new Comment(['body'  => $stripped]);
        $comment->user()->associate(Auth::user()->id);
        $element->comments()->save($comment);
        $comment->pictures()->attach($request->get('pictures', []));

        $data = fractal()
            ->item($comment, new CommentTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Report a comment
     *
     * @param CommentReportRequest  $request
     * @param                       $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function report(CommentReportRequest $request, $id)
    {
        $comment = $this->commentRepository->findOrFail($id);

        // Check if user already reported this comment
        if (CommentFlags::findReport($id, Auth::user()->id)) {
            abort(403, 'User already reported this comment.');
        }

        $flag = new CommentFlags(['description' => $request->get('description', '')]);
        $flag->user()->associate(Auth::user()->id);
        $flag->comment()->associate($id);
        $flag->save();
        $comment->update(['status' => 'review']);

        return response()->json(['Status' => 1]);
    }
}
