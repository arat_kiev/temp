<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\TicketPriceTransformer;
use App\Http\Controllers\Controller;
use App\Managers\TicketManager;
use App\Models\Ticket\TicketPrice;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class TicketPriceController extends Controller
{
    /** @var \App\Models\Ticket\TicketPrice */
    private $ticketPriceRepository;

    /**
     * @param \App\Models\Ticket\TicketPrice $ticketPriceRepository
     */
    public function __construct(TicketPrice $ticketPriceRepository)
    {
        $this->ticketPriceRepository = $ticketPriceRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $priceQuery = $this->parseRequest($request);

        $paginator = $priceQuery
            ->paginate($request->get('_perPage', $this->ticketPriceRepository->getPerPage()))
            ->appends($request->except('page'));

        $ticketPrices = $paginator->items();

        $data = fractal()
            ->collection($ticketPrices, new TicketPriceTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticketPrice = $this->ticketPriceRepository->findOrFail($id);

        $data = fractal()
            ->item($ticketPrice, new TicketPriceTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseRequest(Request $request)
    {
        $typeQuery = $this->ticketPriceRepository
            ->join('ticket_price_translations', 'ticket_prices.id', '=', 'ticket_price_translations.ticket_price_id')
            ->addSelect(['ticket_prices.*', 'ticket_price_translations.name']);

        // handle sort parameters
        $sortField = $request->query->get('_sortField', 'id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        $typeQuery->orderBy($sortField, $sortDir);

        // filter by name
        if ($request->query->has('name')) {
            $name = $request->query->get('name');
            $typeQuery->whereHas('translations', function ($query) use ($name) {
                $query->where('ticket_price_translations.name', 'like', '%' . $name . '%');
            });
        }

        // filter by ids
        if ($request->query->has('ticket_type_id')) {
            $ticket_type_ids = $request->query->get('ticket_type_id');
            $typeQuery->whereIn('area_types.id', $ticket_type_ids);
        }

        return $typeQuery;
    }

    /**
     * Get available dates
     *
     * @param int $id
     * @param TicketManager $ticketManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDates($id, TicketManager $ticketManager)
    {
        $price = $this->ticketPriceRepository->findOrFail($id);

        return response()->json($ticketManager->getValidDates($price));
    }
}
