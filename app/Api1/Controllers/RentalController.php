<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\RentalTransformer;
use App\Api1\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use App\Models\Contact\Rental;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use DB;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class RentalController extends Controller
{
    /** @var Rental */
    private $rentalRepository;

    /**
     * @param Rental $rentalRepository
     */
    public function __construct(Rental $rentalRepository)
    {
        $this->rentalRepository = $rentalRepository;

        $this->middleware('jwt.auth');

        if (!Auth::user()->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $rentalQuery = $this->parseRequest($request);

        $paginator = $rentalQuery
            ->paginate($request->get('_perPage', $this->rentalRepository->getPerPage()))
            ->appends($request->except('page'));

        $rentals = $paginator->items();

        $data = fractal()
            ->collection($rentals, new RentalTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rental = $this->rentalRepository->findOrFail($id);

        $data = fractal()
            ->item($rental, new RentalTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function parseRequest(Request $request)
    {
        $rentalQuery = $this->rentalRepository
            ->addSelect(['rentals.*', 'rentals.id as id']);

        if ($request->query->has('search')) {
            $search = $request->query->get('search');
            $rentalQuery->where('name', 'like', '%' . $search . '%');
        }

        // filter by ids
        if ($request->query->has('rental_id')) {
            $rental_ids = $request->query->get('rental_id');
            $rentalQuery->whereIn('rentals.id', $rental_ids);
        }

        return $rentalQuery;
    }
}
