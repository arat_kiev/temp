<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\PointOfInterestTransformer;
use App\Api1\Requests\StorePoiRequest;
use App\Http\Controllers\Controller;
use App\Models\PointOfInterest\PointOfInterest;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class PointsOfInterestController extends Controller
{
    private $pointsOfInterestRepository;

    public function __construct(PointOfInterest $pointOfInterestRepository)
    {
        $this->pointsOfInterestRepository = $pointOfInterestRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paginator = $this->pointsOfInterestRepository
            ->paginate($request->get('_perPage', $this->pointsOfInterestRepository->getPerPage()))
            ->appends($request->except('page'));

        $data = fractal()
            ->collection($paginator->items(), new PointOfInterestTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function show($id)
    {
        $poi = $this->pointsOfInterestRepository->findOrFail($id);

        $data = fractal()
            ->item($poi, new PointOfInterestTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     * @param StorePoiRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StorePoiRequest $request)
    {
        $this->pointsOfInterestRepository->saveAsDraft($request->all());

        return response()->json([], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
