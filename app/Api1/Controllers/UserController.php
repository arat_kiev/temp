<?php

namespace App\Api1\Controllers;

use App\Api1\Requests\StoreUserRequest;
use App\Api1\Transformers\UserTransformer;
use App\Exceptions\ActiveTicketsException;
use App\Http\Controllers\Controller;
use App\Managers\UserManager;
use App\Models\Picture;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserController extends Controller
{
    /** @var \App\Models\User */
    private $userRepository;

    /**
     * @param \App\Models\User $userRepository
     */
    public function __construct(User $userRepository)
    {
        $this->userRepository = $userRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if (!Auth::user()->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        }

        $userQuery = $this->parseRequest($request);

        $paginator = $userQuery
            ->paginate($request->get('_perPage', $this->userRepository->getPerPage()))
            ->appends($request->except('page'));

        $users = $paginator->items();

        $data = fractal()
            ->collection($users, new UserTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // NOT NEEDED --> Registered users
        throw new AccessDeniedHttpException();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     * @throws AccessDeniedHttpException
     */
    public function show($id)
    {
        if ($id !== 'self' && !Auth::user()->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        } elseif ($id === 'self') {
            $id = Auth::user()->id;
        }

        $user = $this->userRepository->findOrFail($id);

        $data = fractal()
            ->item($user, new UserTransformer(true))
            ->includeProfile()
            ->includeRoles()
            ->includeResellers()
            ->includeOrganizations()
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreUserRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(StoreUserRequest $request, $id)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($id !== 'self' && !$user->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        } elseif ($id === 'self') {
            $id = $user->id;
        }

        if ($user->newsletter == $request->request->getBoolean('newsletter')) {
            if ($user->id === $id && $user->tickets()->active()->count()) {
                throw new ActiveTicketsException('Forbidden, tickets are active');
            }
        }

        $user = $this->userRepository->findOrFail($id);
        $this->updateUserData($user, $request);

        $data = fractal()
            ->item($user, new UserTransformer(true))
            ->includeRoles()
            ->toArray();

        return response()->json($data, 200);
    }

    private function updateUserData(User $user, StoreUserRequest $request)
    {
        $addressBefore = [
            'post_code'     => $user->post_code,
            'country_id'    => $user->country_id,
        ];
        $user->update($request->all());

        $addressAfter = [
            'post_code'     => $user->post_code,
            'country_id'    => $user->country_id,
        ];

        if (array_diff_assoc($addressBefore, $addressAfter)) {
            $state = (new UserManager())->fetchState($user);
            $user->state()->associate($state);
            $user->save();
        }
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $user = Auth::user();

        if ($id !== 'self' && !$user->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        } elseif ($id === 'self') {
            $id = $user->id;
        }

        $user->delete();

        return response()->json();
    }

    /**
     * Add avatarPicture|heroPicture id
     * @param $picture_id
     * @param $type
     * @return mixed
     */
    public function addHeroAvatarPicture($picture_id, $type)
    {
        if (Picture::where('id', $picture_id)->exists()) {
            Auth::user()->{$type . '_picture_id'} = $picture_id;
            Auth::user()->save();
            return response()->json();
        }
        abort(400, 'Invalid input data');
    }

    /**
     * Remove avatarPicture|heroPicture id
     * @param $type
     * @return mixed
     */
    public function rmHeroAvatarPicture($type)
    {
        Auth::user()->{$type . '_picture_id'} = null;
        Auth::user()->save();
        return response()->json();
    }

    private function parseRequest(Request $request)
    {
        $userQuery = $this->userRepository
            ->addSelect(['users.*', 'users.id as id']);

        if ($request->query->has('search')) {
            $search = $request->query->get('search');
            $userQuery->where('name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%');
        }

        // filter by ids
        if ($request->query->has('user_id')) {
            $user_ids = $request->query->get('user_id');
            $userQuery->whereIn('users.id', $user_ids);
        }
        
        if ($request->query->has('roles') && is_array($request->query->get('roles'))) {
            $roles = $request->query->get('roles');
            $userQuery->role($roles);
        }

        return $userQuery;
    }
}
