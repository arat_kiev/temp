<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\TicketCategoryTransformer;
use App\Http\Controllers\Controller;
use App\Models\Ticket\TicketCategory;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class TicketCategoryController extends Controller
{
    /** @var \App\Models\Ticket\TicketCategory */
    private $ticketCategoryRepository;

    /**
     * @param \App\Models\Ticket\TicketCategory $ticketCategoryRepository
     */
    public function __construct(TicketCategory $ticketCategoryRepository)
    {
        $this->ticketCategoryRepository = $ticketCategoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = $this->ticketCategoryRepository->paginate();
        $ticketCategories = $paginator->items();

        $data = fractal()
            ->collection($ticketCategories, new TicketCategoryTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        throw new MethodNotAllowedHttpException(['GET']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticketCategory = $this->ticketCategoryRepository->findOrFail($id);

        $data = fractal()
            ->item($ticketCategory, new TicketCategoryTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        throw new MethodNotAllowedHttpException(['GET']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        throw new MethodNotAllowedHttpException(['GET']);
    }
}
