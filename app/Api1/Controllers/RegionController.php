<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\RegionTransformer;
use App\Http\Controllers\Controller;
use App\Models\Location\Region;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    /** @var \App\Models\Location\Region */
    private $regionRepository;

    /**
     * @param \App\Models\Location\Region $regionRepository
     */
    public function __construct(Region $regionRepository)
    {
        $this->regionRepository = $regionRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $includes = ['cities'];

        $region = $this->regionRepository->with($includes)->findOrFail($id);

        $data = fractal()
            ->item($region, new RegionTransformer(true))
            ->parseIncludes($includes)
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
