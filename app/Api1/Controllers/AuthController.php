<?php

namespace App\Api1\Controllers;

use App\Api1\Requests\ChangePasswordRequest;
use App\Api1\Requests\CheckOldPasswordRequest;
use App\Api1\Requests\ResetPasswordRequest;
use App\Api1\Requests\SavePasswordRequest;
use App\Api1\Requests\SignupRequest;
use App\Api1\Requests\SocialLoginRequest;
use App\Api1\Transformers\UserTransformer;
use App\Events\PasswordReset as PasswordResetEvent;
use App\Events\UserActivated;
use App\Events\UserRegistered;
use App\Events\UserChangeEmailEvent;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\PasswordReset;
use App\Models\User;
use Event;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use JWTAuth;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    protected $auth_drivers = ['facebook', 'google'];

    public function __construct()
    {
        $this->middleware('jwt.auth', ['only' => ['change']]);
    }

    /**
     * Refresh an expired token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        if (!$token = JWTAuth::getToken()) {
            return response()->json(['error' => trans('auth.failed')], 401);
        }

        $token = JWTAuth::refresh($token);

        return response()->json(compact('token'));
    }

    /**
     * Reset the given user's password.
     *
     * @param User $user
     * @param string $password
     */
    protected function resetPassword($user, $password)
    {
        $user->password = $password;
        $user->save();
    }

    /**
     * Change password
     *
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function change(ChangePasswordRequest $request)
    {
        $user = \Auth::user();
        $credentials = [
            'email' => $user->email,
            'password' => $request->get('old_password')
        ];

        if (!\Auth::attempt($credentials, false, false)) {
            return response()->json(['error' => trans('auth.failed')], 401);
        }

        $user = \Auth::user();
        $pass = $request->request->get('password');
        $this->resetPassword($user, $pass);

        $data = fractal()
            ->item($user, new UserTransformer(true))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Change user email
     *
     * @param Request $request
     */
    public function postChangeEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users'
        ]);

        event(new UserChangeEmailEvent(\Auth::user(), \Auth::user()->signupClient, $request));
    }

    /**
     * Check old password value in DB
     *
     * @param CheckOldPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function oldPasswordCheck(CheckOldPasswordRequest $request)
    {

        $user = \Auth::user();
        $credentials = [
            'email' => $user->email,
            'password' => $request->get('old_password')
        ];

        if (!\Auth::attempt($credentials, false, false)) {
            return response()->json(['error' => trans('auth.failed')], 401);
        } else {
            return response()->json();
        }
    }

    /**
     * Reset password
     *
     * @param ResetPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(ResetPasswordRequest $request)
    {
        $email = trim($request->request->get('email'));
        $user = User::where('email', '=', $email)->first();
        $token = \Uuid::generate(4);

        if (!$user) {
            return response()->json();
        }

        if (PasswordReset::where('emails', '=', $email)->count() >= 3) {
            return response()->json(['error' => 'Too many resets'], 400);
        }

        PasswordReset::create([
            'emails' => $email,
            'token' => $token,
        ]);

        Event::fire(new PasswordResetEvent($user, $token, config('app.client')));

        return response()->json();
    }

    /**
     * Change password after reset
     *
     * @param string $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getChangePassword($token)
    {
        if (!$reset = PasswordReset::where('token', '=', $token)->first()) {
            return view('bissanzeiger.profile.invalid');
        }

        return view('bissanzeiger.profile.new_password', compact('token'));
    }

    /**
     * Change password after reset
     *
     * @param SavePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postChangePassword(SavePasswordRequest $request, $token)
    {
        if (!$reset = PasswordReset::where('token', '=', $token)->first()) {
            return view('bissanzeiger.profile.invalid');
        }

        $user = User::where('email', '=', $reset->emails)->first();
        $this->resetPassword($user, $request->request->get('password'));

        PasswordReset::where('emails', '=', $reset->emails)->delete();

        return view('bissanzeiger.profile.success');
    }

    /**
     * Login with email and password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function signin(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            $seconds = $this->limiter()->availableIn(
                $this->throttleKey($request)
            );

            return response()->json(['error' => trans('auth.throttle', compact('seconds'))], 423);
        }

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                $this->incrementLoginAttempts($request);

                return response()->json(['error' => trans('auth.failed')], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => trans('auth.token_error')], 500);
        }

        return response()->json(compact('token'));
    }

    /**
     * Create account with email and password
     *
     * @param SignupRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signup(SignupRequest $request)
    {
        $credentials = $request->only(['name', 'email', 'password', 'fisher_id']);

        try {
            $user = User::create($credentials);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }

        Event::fire(new UserRegistered($user, config('app.client')));

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('token'));
    }

    /**
     * Activate account with activation code
     *
     * @param Request $request
     * @param $activation_code
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function activate(Request $request, $activation_code)
    {
        if (!($user = User::where('activation_code', '=', $activation_code)->first())) {
            return response('Aktivierungscode ist ungültig oder Konto ist bereits aktiviert.');
        }

        /** @var User $user */
        $user->active = true;
        $user->activation_code = null;
        $user->save();

        Event::fire(new UserActivated($user));

        /** @var Client $client */
        $client = $user->signupClient;

        $data = [
            'baseUrl' => $client->base_url
        ];

        if (array_key_exists('activate', $client->templates)) {
            return view($client->templates['activate'], $data);
        } else {
            return view('bissanzeiger.profile.activated', $data);
        }
    }

    /**
     * Generate oAuth authorization url for requested provider
     *
     * @param string $auth_service
     * @return \Illuminate\Http\JsonResponse
     */
    public function socialAuthorize($auth_service)
    {
        if (!$provider = $this->createProvider($auth_service)) {
            abort(400);
        }

        return response()->json(['uri' => $provider->getAuthorizationUrl()]);
    }

    /**
     * Receive the oAuth authorization code grant and create/login user
     *
     * @param SocialLoginRequest $request
     * @param string $auth_service
     * @return \Illuminate\Http\JsonResponse
     */
    public function socialLogin(SocialLoginRequest $request, $auth_service)
    {
        try {
            $provider = $this->createProvider($auth_service);
            $token = $provider->getAccessToken('authorization_code', ['code' => $request['code']]);
            $owner = $provider->getResourceOwner($token);
        } catch (IdentityProviderException $e) {
            abort(400);
        }
        $user = $this->findOrCreateUser($auth_service, $owner);
        $token = JWTAuth::fromUser($user);
        return response()->json(compact('token'));
    }

    /**
     * Find existing or create a new user
     *
     * @param string $auth_service
     * @param $owner
     * @return static
     */
    private function findOrCreateUser($auth_service, $owner)
    {
        try {
            $user = User::firstOrCreate([
                'name' => $owner->getName(),
                'first_name' => $owner->getFirstName() ?: null,
                'last_name' => $owner->getLastName() ?: null,
                //'birthday' =>  //TODO add birthday
                'email' => $owner->getEmail(),
                'active' => true,
                "${auth_service}_id" => $owner->getId(),
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Can\'t save profile'], 500);
        }
        return $user;
    }

    /**
     * Get the provider object for the given auth service
     *
     * @param $auth_service
     * @return AbstractProvider|null
     */
    private function createProvider($auth_service)
    {
        if (in_array($auth_service, $this->auth_drivers)) {
            $providerClass = config("services.$auth_service.provider");
            $provider = new $providerClass([
                'clientId' => config("services.$auth_service.client_id"),
                'clientSecret' => config("services.$auth_service.client_secret"),
                'redirectUri' => config("services.$auth_service.redirect"),
            ]);
            return $provider;
        }
        abort(404);
    }
}
