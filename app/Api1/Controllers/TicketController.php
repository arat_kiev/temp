<?php

namespace App\Api1\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Ticket\ResellerTicket;
use App\Models\Ticket\Ticket;
use Illuminate\Http\Request;
use App\Api1\Requests\TicketsRequest;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class TicketController extends Controller
{
    /** @var Ticket */
    private $ticketRepository;

    public function __construct(Ticket $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;

        $this->middleware('jwt.auth');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        throw new AccessDeniedHttpException();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        throw new AccessDeniedHttpException();
    }

}
