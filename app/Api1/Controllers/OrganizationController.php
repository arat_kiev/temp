<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\OrganizationTransformer;
use App\Http\Controllers\Controller;
use App\Models\Organization\Organization;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class OrganizationController extends Controller
{
    /** @var Organization */
    private $organizationRepository;

    /**
     * @param Organization $organizationRepository
     */
    public function __construct(Organization $organizationRepository)
    {
        $this->organizationRepository = $organizationRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $organizationQuery = $this->parseRequest($request);

        $paginator = $organizationQuery->with('logo')
            ->paginate($request->get('_perPage', $this->organizationRepository->getPerPage()))
            ->appends($request->except('page'));

        $organizations = $paginator->items();

        $data = fractal()
            ->collection($organizations, new OrganizationTransformer())
            ->includeLogo()
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $organization = $this->organizationRepository->findOrFail($id);

        $data = fractal()
            ->item($organization, new OrganizationTransformer())
            ->includeLogo()
            ->toArray();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function parseRequest(Request $request)
    {
        $organizationQuery = $this->organizationRepository
            ->addSelect(['organizations.*', 'organizations.id as id']);

        if ($request->query->has('search')) {
            $search = $request->query->get('search');
            $organizationQuery->where('name', 'like', '%' . $search . '%');
        }

        // filter by ids
        if ($request->query->has('organization_id')) {
            $organization_ids = $request->query->get('organization_id');
            $organizationQuery->whereIn('organizations.id', $organization_ids);
        }

        return $organizationQuery;
    }
}
