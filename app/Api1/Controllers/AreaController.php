<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\AreaTransformer;
use App\Api1\Transformers\Picture\PictureTransformer;
use App\Api1\Requests\StoreAreaRequest;
use App\Api1\Transformers\SimpleAreaTransformer;
use App\Http\Controllers\Controller;
use App\Models\Area\Area;
use App\Models\User;
use Auth;
use GeoJson\Feature\Feature;
use GeoJson\Feature\FeatureCollection;
use GeoJson\Geometry\Point;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Slugify;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AreaController extends Controller
{
    /** @var Area */
    private $areaRepository;

    /**
     * @param Area $areaRepository
     */
    public function __construct(Area $areaRepository)
    {
        $this->areaRepository = $areaRepository;

        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $areaQuery = $this->parseRequest($request);

        $paginator = $areaQuery->with(['manager', 'type', 'techniques', 'fishes', 'meta', 'partOf', 'ticketActivePrices'])
            ->paginate($request->get('_perPage', $this->areaRepository->getPerPage()), ['areas.id', 'distance'])
            ->appends($request->except('page'));

        $areas = $paginator->items();

        $data = fractal()
            ->collection($areas, new AreaTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function simpleIndex(Request $request)
    {
        $areaQuery = $this->parseRequest($request);

        $paginator = $areaQuery->with(['gallery.pictures'])
            ->paginate($request->get('_perPage', $this->areaRepository->getPerPage()))
            ->appends($request->except('page'));

        $areas = $paginator->items();

        $data = fractal()
            ->collection($areas, new SimpleAreaTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }
    /**
     * Store a newly created resource in storage.
     * @param StoreAreaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreAreaRequest $request)
    {
        $this->areaRepository->saveAsDraft($request->all());

        return response()->json([], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws NotFoundHttpException
     */
    public function show($id)
    {
        /** @var User $user */
        $user = Auth::user();

        $includes = [
            'affiliates',
            'fishes',
            'techniques',
            'manager',
            'locations',
            'ticketTypes.prices',
            'additionalTicketTypes.prices',
            'clients',
            'hauls',
            'products',
            'inspectors',
        ];

        /** @var Area $area */
        $area = $this->areaRepository->with($includes)->findOrFail($id);

        if (!($user && Area::isMember($user)->find($id) || $area->public)) {
            abort(404);
        }

        $data = fractal()
            ->item($area, new AreaTransformer(true))
            ->parseIncludes($includes)
            ->includePicture()
            ->includeHeroImage()
            ->includeNearbyAreas()
            ->includeAdditionalManagers()
            ->toArray();

        app('App\Managers\TicketManager')->filterPrices($data, Auth::user());

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    /**
     * Get areas as geoJson for map display
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function map(Request $request)
    {
        $areaQuery = $this->parseRequest($request);
        $areas = $areaQuery->with(['locations', 'ticketTypes'])->get();

        $features = [];

        /** @var Area $area */
        foreach ($areas as $area) {
            foreach ($area->locations as $location) {
                $features[] = new Feature(new Point([
                    $location->longitude,
                    $location->latitude,
                ]), [
                    'tickets' => (boolean)$area->approved,
                    'lease' => (boolean)$area->lease,
                    'id' => (int)$area->id,
                    'name' => $area->name,
                    'slug' => $area->slug,
                    'image' => $area->picture ? fractal()->item($area->picture, new PictureTransformer())->toArray()['files'] : null,
                ]);
            }
        }

        $featureCollection = new FeatureCollection($features);

        return response()->json($featureCollection);
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseRequest(Request $request)
    {
        $areaQuery = $this->areaRepository
            ->addSelect(['areas.*', 'areas.id as id']);

        // show only public areas
        $areaQuery->isPublic(true);

        // handle bounding box parameter (for map)
        if ($request->query->has('bbox')) {
            list ($west, $south, $east, $north) = explode(',', $request->query->get('bbox'));
            $areaQuery->inBoundingBox($north, $east, $south, $west);
        }

        if ($request->query->has('brad')) {
            list ($lng, $lat, $radius) = explode(',', $request->query->get('brad'));
            $areaQuery->inRadius((float)$lng, (float)$lat, (float)$radius);
        }

        // handle sort parameters
        if ($request->query->has('_sortField') && $request->query->has('_sortDir')) {
            $sortField = $request->query->get('_sortField', 'id');
            $sortDir = $request->query->get('_sortDir', 'asc');

            $areaQuery->orderBy($sortField, $sortDir);
        }

        // Filter by manager
        if ($request->query->has('manager')) {
            $manager = $request->query->getInt('manager');
            $areaQuery->hasManager($manager);
        }

        // Filter by area type
        if ($request->query->has('areaType')) {
            $areaType = $request->query->getInt('areaType');
            $areaQuery->hasAreaType($areaType);
        }

        // if has tickets available
        if ($request->query->has('tickets')) {
            $tickets = $request->query->getBoolean('tickets');
            $areaQuery->hasTickets($tickets);
        }

        if ($request->query->has('category')) {
            $categories = $request->query->get('category');
            $areaQuery->hasTicketCategories($categories);
        }

        // if is lease area
        if ($request->query->has('lease')) {
            $lease = $request->query->getBoolean('lease');
            $areaQuery->isLease($lease);
        }

        // only areas where user has required licenses
        if ($request->query->has('has_licenses') && Auth::user()) {
            /** @var User $user */
            $user = Auth::user();
            $areaQuery->userHasLicenses($user);
        }

        // if has region/state/country search parameter
        if ($request->query->has('city')) {
            $city = $request->query->getInt('city');
            $areaQuery->hasCity($city);
        } elseif ($request->query->has('region')) {
            $region = $request->query->getInt('region');
            $areaQuery->hasRegion($region);
        } elseif ($request->query->has('state')) {
            $state = $request->query->getInt('state');
            $areaQuery->hasState($state);
        } elseif ($request->query->has('country')) {
            $country = $request->query->getInt('country');
            $areaQuery->hasCountry($country);
        }

        // if has area type search parameter
        if ($request->query->has('type')) {
            $types = $request->query->get('type');
            $areaQuery->hasTypes($types);
        }

        // if has fishes search parameter
        if ($request->query->has('fish')) {
            $fishes = $request->query->get('fish');
            $areaQuery->hasFishes($fishes);
        }

        // if has techniques search parameter
        if ($request->query->has('tech')) {
            $techniques = $request->query->get('tech');
            $areaQuery->hasTechniques($techniques);
        }

        // if has full text search parameter
        if ($request->query->has('search')) {
            $searchTokens = $this->genSearchParams($request->query->get('search'));
            $areaQuery->hasTags($searchTokens);
        }

        // if has rods_max search parameter
        if ($request->query->has('rods')) {
            $roadsNumber = $request->query->getInt('rods');
            $areaQuery->hasRods($roadsNumber);
        }

        // if has member_only search parameter
        if ($request->query->has('member')) {
            $member = $request->query->getBoolean('member');
            $areaQuery->hasMember($member);
        }

        // if has boat search parameter
        if ($request->query->has('boat')) {
            $boat = $request->query->getBoolean('boat');
            $areaQuery->hasBoat($boat);
        }

        // if has phone_ticket search parameter
        if ($request->query->has('phone_ticket')) {
            $phone_ticket = $request->query->getBoolean('phone_ticket');
            $areaQuery->hasPhoneTicket($phone_ticket);
        }

        // if has nightfishing search parameter
        if ($request->query->has('night')) {
            $night = $request->query->getBoolean('night');
            $areaQuery->hasNight($night);
        }

        // filter by landing_pages selected areas related to
        if ($request->query->has('part_of') && is_array($request->query->get('part_of'))) {
            $lpIds = $request->query->get('part_of');
            $areaQuery->partOf($lpIds);
        }

        // filter by ids
        if ($request->query->has('area_id') && is_array($request->query->get('area_id'))) {
            $area_ids = $request->query->get('area_id');
            $areaQuery->whereIn('areas.id', $area_ids);
        }

        return $areaQuery;
    }

    /**
     * Create array of tokens from search string
     *
     * @param string $searchString
     * @return array
     */
    private function genSearchParams($searchString)
    {
        $tokens = [];
        foreach (explode(' ', $searchString) as $token) {
            $token = Slugify::slugify($token);
            foreach (explode('-', $token) as $partToken) {
                if (strlen($partToken) > 2) {
                    $tokens[] = $partToken;
                }
            }
        }
        return $tokens;
    }
}
