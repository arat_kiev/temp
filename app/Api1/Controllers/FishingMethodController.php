<?php

namespace App\Api1\Controllers;

use App\Api1\Transformers\FishingMethodTransformer;
use App\Http\Controllers\Controller;
use App\Models\Meta\FishingMethod;
use Cache;
use Illuminate\Http\Request;
use App;

class FishingMethodController extends Controller
{
    /** @var FishingMethod */
    private $fishingMethodRepository;

    /**
     * @param FishingMethod $fishingMethodRepository
     */
    public function __construct(FishingMethod $fishingMethodRepository)
    {
        $this->fishingMethodRepository = $fishingMethodRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Propaganistas\LaravelCacheKeywords\Exceptions\ReservedCacheKeyPatternException
     */
    public function index(Request $request)
    {
        $fishingMethodQuery = $this->parseRequest($request);
        $cacheKey = $this->getIndexCacheKey($request);

        $methods = Cache::remember($cacheKey, 60, function () use ($fishingMethodQuery) {
            return $fishingMethodQuery->get();
        });

        $data = fractal()
            ->collection($methods, new FishingMethodTransformer())
            ->toArray();

        return response()->json($data);
    }

    /**
     * Generate cache key according to request params
     *
     * @param Request $request
     * @return string
     */
    private function getIndexCacheKey(Request $request)
    {
        $cacheHash = md5(json_encode([
            $request->query->get('_sortField', 'id'),
            $request->query->get('_sortDir', 'asc'),
            $request->query->get('name', ''),
            $request->query->get('fishing_method_id', []),
        ]));

        return implode('_', [
            'fishing_methods',
            App::getLocale(),
            $cacheHash,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     */
    public function show($id)
    {
      //
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update()
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        //d(^_*)b
    }

    /**
     * Parse the request (sort, filter, search, ...) and build query
     *
     * @param Request $request
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseRequest(Request $request)
    {
        $fishingMethodQuery = $this->fishingMethodRepository
            ->join('fishing_methods_translations', function($join) {
                $join->on('fishing_methods.id', '=', 'fishing_methods_translations.fishing_method_id')
                    ->where('fishing_methods_translations.locale', '=', App::getLocale());
            })
            ->addSelect(['fishing_methods.*', 'fishing_methods_translations.name']);

        $sortField = $request->query->get('_sortField', 'id');
        $sortDir = $request->query->get('_sortDir', 'asc');

        $fishingMethodQuery->orderBy($sortField, $sortDir);

        return $fishingMethodQuery;
    }
}
