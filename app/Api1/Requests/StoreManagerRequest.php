<?php

namespace App\Api1\Requests;

class StoreManagerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'street' => 'required|string',
            'city' => 'required|string',
            'area_code' => 'required|string',

            'country_id' => 'required|exists:countries,id',
            'email' => 'required|email',

            'parent_id' => 'exists:managers,id',
            'logo_id' => 'exists:pictures,id',
            'rule_id' => 'exists:rules,id',

            'account_number' => 'string',
            'short_code' => 'string',
            'person' => 'string',
            'phone' => 'string',
            'website' => 'url',

        ];
    }
}
