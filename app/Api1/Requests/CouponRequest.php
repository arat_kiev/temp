<?php

namespace App\Api1\Requests;

class CouponRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'promo'     => 'required|exists:promotions,id',
                ];
            case 'PUT':
                return [
                    'code'      => 'required|exists:coupons,code',
                ];
            default:
                return [];
        }
    }
}
