<?php

namespace App\Api1\Requests;

class BasketRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                return [
                    'id' => 'required|integer',
                    'type' => 'required|in:ticket,product',
                    'qty' => 'required_if:type,product|integer|min:1|max:1000',
                    'datetime' => 'required_if:type,ticket|date_format:Y-m-d H:i',
                    'depends' => 'string',
                    'group'     => 'array',
                    'group.*.first_name'    => 'string',
                    'group.*.last_name'     => 'string',
                    'group.*.birthday'      => 'date',
                    'timeslot_dates' => 'array',
                    'timeslot_dates.*' => 'integer',
                ];

            case 'PATCH':
                return [
                    'qty' => 'integer|min:0|max:1000',
                    'datetime' => 'date_format:Y-m-d H:i',
                ];

            default:
                return [];
        }
    }
}
