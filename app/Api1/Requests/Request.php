<?php

namespace App\Api1\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{

    /**
     * Get the validator instance for the request and
     * add attach callbacks to be run after validation
     * is completed.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function($validator){
            // Call the after method of the FormRequest
            !method_exists($this, 'after') ?: $this->after($validator);
        });
    }

}
