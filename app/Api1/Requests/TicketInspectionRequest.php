<?php

namespace App\Api1\Requests;


class TicketInspectionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
                return [
                    'perPage' => 'integer',
                    'page' => 'integer',
                    'from' => 'date|date_format:Y-m-d H:i',
                    'till' => 'date|date_format:Y-m-d H:i',
                    'valid_from' => 'date|date_format:Y-m-d',
                    'valid_to' => 'date|date_format:Y-m-d',
                    'area_id' => 'required|integer'
                ];
            case 'POST':
                return [
                    'tour_id' => 'required|integer',
                    'gallery_id' => 'integer',
                    'location' => 'json',
                    'status' => 'required|in:OK,NOT_OK',
                    'comment' => 'alpha_spaces'
                ];
            case 'PATCH':
                return [
                    'gallery_id' => 'integer',
                    'location' => 'json',
                    'status' => 'required|in:OK,NOT_OK',
                    'comment' => 'alpha_spaces'
                ];

            default:
                return [];
        }
    }
}