<?php

namespace App\Api1\Requests;


class TicketInspectionHaulRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'fish_id' => 'required|integer',
                    'length' => 'required|integer',
                    'weight' => 'required|integer',
                    'comment' => 'alpha_dash'
                ];
            default:
                return [];
        }
    }
}