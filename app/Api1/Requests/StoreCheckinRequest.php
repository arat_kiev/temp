<?php

namespace App\Api1\Requests;

use Auth;

class StoreCheckinRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() !== null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => 'date_format:"Y-m-d H:i:s"|before:tomorrow',
            'till' => 'date_format:"Y-m-d H:i:s"|before:tomorrow',
            'fishing_method_id' => 'integer'
        ];
    }
}
