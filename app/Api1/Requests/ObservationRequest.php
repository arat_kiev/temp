<?php

namespace App\Api1\Requests;


class ObservationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
                return [
                    'perPage' => 'integer',
                    'page' => 'integer',
                    'from' => 'date|date_format:Y-m-d H:i:s',
                    'till' => 'date|date_format:Y-m-d H:i:s'
                ];
            case 'POST':
                return [
                    'observation_category_id' => 'required|integer',
                    'gallery_id' => 'integer',
                    'comment' => 'required',
                    'location' => 'json'
                ];
            case 'PATCH':
                return [
                    'observation_category_id' => 'required|integer',
                    'gallery_id' => 'integer',
                    'comment' => 'required',
                    'location' => 'json'
                ];

            default:
                return [];
        }
    }
}