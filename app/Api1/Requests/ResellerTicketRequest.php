<?php

namespace App\Api1\Requests;

use Auth;

class ResellerTicketRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'ticket_price_id' => 'required|exists:ticket_prices,id',
                    'date' => 'date',
                    'first_name' => 'required|string',
                    'last_name' => 'required|string',
                    'street' => 'required|string',
                    'post_code' => 'required|string',
                    'city' => 'required|string',
                    'country_id' => 'required|integer|exists:countries,id',
                    'birthday' => 'required|date',
                    'phone' => 'string',
                    'email' => 'string',
                    'licenses' => 'sometimes|array',
                    'authorization_id' => 'string',
                    'issuing_authority' => 'string',
                    'fisher_id' => 'regex:/^\w{3}\-\d{3}\-\w{3}$/',
                    'group' => 'array',
                    'group.*.first_name' => 'string',
                    'group.*.last_name' => 'string',
                    'group.*.birthday_day' => 'integer|min:1|max:31',
                    'group.*.birthday_month' => 'integer|min:1|max:12',
                    'group.*.birthday_year' => 'integer|min:1900|max:2050',
                ];
            case 'DELETE':
                return [
                    'reason' => 'required|string|min:4',
                ];
            default:
                return [];
        }
    }
}
