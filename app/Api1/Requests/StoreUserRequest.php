<?php

namespace App\Api1\Requests;

class StoreUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'string',
            'last_name' => 'string',
            'street' => 'string',
            'post_code' => 'string',
            'city' => 'string',
            'country_id' => 'integer|exists:countries,id',
            'birthday' => 'date',
            'phone' => 'string',
            'newsletter' => 'boolean'
        ];
    }
}
