<?php

namespace App\Api1\Requests;

use Auth;

class StoreHaulRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() !== null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = Auth::check() ? Auth::user()->id : null;

        return [
            'area_id'       => 'integer|exists:areas,id',
            'area_name'     => 'required_without:area_id|max:255',
            'fish_id'       => 'nullable|integer|required_if:taken,true,1',
            'technique_id'  => 'nullable|integer|exists:techniques,id',
            'ticket_id'     => 'nullable|integer|exists:tickets,id,user_id,'.$userId,
            'picture_id'    => 'nullable|integer|exists:pictures,id,author_id,'.$userId,
            'public'        => 'required|boolean',
            'taken'         => 'required|boolean',
            'size_value'    => 'nullable|numeric|required_with:fish_id',
            'size_type'     => 'nullable|string|in:KG,CM|required_with:fish_id',
            'weather'       => 'string|in:sunny,cloudy,rain,snow',
            'temperature'   => 'integer|between:-10,40',
            'user_comment'  => 'string',
            'catch_date'    => 'required|date_format:"Y-m-d"|before:tomorrow',
            'catch_time'    => 'nullable|date_format:"H:i"',
            'ticket_number' => 'string',
        ];
    }
}
