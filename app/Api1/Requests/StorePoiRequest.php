<?php

namespace App\Api1\Requests;

class StorePoiRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email',

            'country_id' => 'required|exists:countries,id',
            'picture_id' => 'exists:pictures,id',

            'street' => 'string',
            'post_code' => 'string',
            'city' => 'string',
            'phone' => 'string',
            'website' => 'url',

            'featured_from' => 'date_format:"Y-m-d H:i:s"',
            'featured_till' => 'date_format:"Y-m-d H:i:s"',
        ];
    }

    /**
     * Attach callbacks to be run after validation is completed.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return callback
     */
    public function after($validator)
    {
        if ((isset($this->featured_from) && isset($this->featured_till)) && strtotime($this->featured_from) > strtotime($this->featured_till)) {
            $validator->errors()->add('featured_from', 'featured_from should be less than featured_till');
        }
    }
}
