<?php

namespace App\Api1\Requests\Statistics;

use Auth;
use App\Api1\Requests\Request;

class CatchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() !== null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from'          => 'required|date_format:Y-m-d',
            'till'          => 'required|date_format:Y-m-d',
            'granularity'   => 'required_without:hour|in:DAY,WEEK,MONTH,SEASON',
            'hour'          => 'sometimes|required|boolean|in:true,1,yes',

            'areas'         => 'sometimes|required|array',
            'areas.*'       => 'sometimes|required|integer|exists:areas,id',
            'fishes'        => 'sometimes|required|array',
            'fishes.*'      => 'sometimes|required|integer|exists:fishes,id',
            'techniques'    => 'sometimes|required|array',
            'techniques.*'  => 'sometimes|required|integer|exists:techniques,id',
            'users'         => 'sometimes|required|array',
            'users.*'       => 'sometimes|required|integer|exists:users,id',
            'teams'         => 'sometimes|required|array',
            'teams.*'       => 'sometimes|required|integer|exists:teams,id',
        ];
    }
}
