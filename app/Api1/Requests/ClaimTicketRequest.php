<?php

namespace App\Api1\Requests;

class ClaimTicketRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user() !== null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fisher_id' => 'required|string',
            'ticket_number' => 'required|string',
        ];
    }
}
