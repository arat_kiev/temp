<?php

namespace App\Api1\Requests;

use App\Models\Picture;
use Auth;

class StoreFishRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'description' => 'string',
        ];
    }

    /**
     * Attach callbacks to be run after validation is completed.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return callback
     */
    public function after($validator)
    {
        if (isset($this->picture)) {
            Picture::where([['id', $this->picture], ['author_id', Auth::id()]])->exists() ?: $validator->errors()->add('picture', 'Picture is not allowed');
        }
    }
}
