<?php

namespace App\Api1\Requests;

use Auth;

class SignupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:users,name',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|max:64',
            'fisher_id' => array('regex:/^\w{3}\-\d{3}\-\w{3}$/', 'exists:reseller_tickets,fisher_id', 'unique:users,fisher_id')
        ];
    }
}
