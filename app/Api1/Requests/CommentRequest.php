<?php

namespace App\Api1\Requests;

use Auth;

class CommentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'GET':
                return [
                    'status'        => 'sometimes|string|in:accepted,rejected,review',
                    '_sortField'    => 'in:id,created_at',
                    '_sortDir'      => 'in:asc,desc',
                ];
            case 'POST':
                return [
                    'body'          => 'required|string|max:1024',
                    'pictures'      => 'sometimes|array|max:10',
                    'pictures.*'    => 'integer|exists:pictures,id,author_id,'.Auth::user()->id,
                ];
            case 'DELETE':
            case 'PUT':
            case 'PATCH':
            default:
                return [];
        }
    }
}
