<?php

namespace App\Api1\Requests;

use Auth;

class ProductSaleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (bool) Auth::user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'product_id'  => 'required|exists:products,id',
                ];
            default:
                return [];
        }
    }
}
