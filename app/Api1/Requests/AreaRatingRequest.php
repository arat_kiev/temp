<?php

namespace App\Api1\Requests;

use Auth;

class AreaRatingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
            case 'PUT':
            case 'PATCH':
                return [
                    'subcategories'         => 'required|array',
                    'subcategories.*.id'    => 'required|exists:rating_categories,id',
                    'subcategories.*.value' => 'required|numeric|min:0|max:10',
                ];
            default:
                return [];
        }
    }

    public function messages()
    {
        return [
            'subcategories.*.id.required'   => 'Das Identifizierungszeichen der Subkategorie muss ausgefüllt sein.',
            'subcategories.*.id.exists'     => 'Der gewählte Wert für das Identifizierungszeichen der Subkategorie'
                                               . ' ist ungültig.',
            'subcategories.*.value.required'    => 'Der Wert muss ausgefüllt sein.',
            'subcategories.*.value.numeric'     => 'Der Wert muss eine Zahl sein.',
            'subcategories.*.value.min'         => 'Der Wert muss mindestens :min sein.',
            'subcategories.*.value.max'         => 'Der Wert darf maximal :max sein.',
        ];
    }
}
