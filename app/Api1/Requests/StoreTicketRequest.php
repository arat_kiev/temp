<?php

namespace App\Api1\Requests;

use Auth;

class StoreTicketRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (bool) Auth::user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price_id'  => 'required|exists:ticket_prices,id',
            'date'      => 'date',
            'group'     => 'array',
            'group.*.first_name'    => 'string',
            'group.*.last_name'     => 'string',
            'group.*.birthday'      => 'date',
        ];
    }
}
