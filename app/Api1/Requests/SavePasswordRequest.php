<?php

namespace App\Api1\Requests;

class SavePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|confirmed|min:8|max:64',
        ];
    }

    public function messages()
    {
        return [
            'password.required' => 'Kein Passwort angegeben.',
            'password.confirmed' => 'Eingegebene Passwörter müssen identisch sein.',
            'password.min' => 'Passwort muss mindestens 8 Zeichen haben.',
            'password.max' => 'Passwort darf höchstens 64 Zeichen haben.',
        ];
    }
}
