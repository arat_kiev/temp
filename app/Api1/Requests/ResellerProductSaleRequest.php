<?php

namespace App\Api1\Requests;

use Auth;

class ResellerProductSaleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'product_id'    => 'required|exists:products,id',
                    'first_name'    => 'required|string',
                    'last_name'     => 'required|string',
                    'street'        => 'required|string',
                    'post_code'     => 'required|string',
                    'city'          => 'required|string',
                    'country_id'    => 'required|integer|exists:countries,id',
                    'birthday'      => 'required|date',
                    'email'         => 'string',
                    'fisher_id'     => 'regex:/^\w{3}\-\d{3}\-\w{3}$/',
                ];
            case 'DELETE':
                return [
                    'reason' => 'required|string|min:4',
                ];
            default:
                return [];
        }
    }
}
