<?php

namespace App\Api1\Requests;

class PaymentInitRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'method' => 'required|string|in:OT,VA,CC',
            'amount' => 'required|numeric|min:5|max:2500',
            'redirect_target' => 'required|string',
            'success_url' => 'required|string',
            'failure_url' => 'required|string',
        ];
    }
}
