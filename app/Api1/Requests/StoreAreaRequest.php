<?php

namespace App\Api1\Requests;

class StoreAreaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'manager_id' => 'required|exists:managers,id',

            'parent_area_id' => 'exists:areas,id',
            'type_id' => 'exists:area_types,id',
            'gallery_id' => 'exists:galleries,id',
            'rule_id' => 'exists:rules,id',

            'rods_max' => 'integer|max:255',
            'lease' => 'sometimes|accepted',
            'member_only' => 'sometimes|accepted',
            'boat' => 'sometimes|accepted',
            'phone_ticket' => 'sometimes|accepted',
            'nightfishing' => 'sometimes|accepted',

            'account_number' => 'string',
        ];
    }
}
