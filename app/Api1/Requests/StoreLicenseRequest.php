<?php

namespace App\Api1\Requests;

use Auth;

class StoreLicenseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = Auth::check() ? Auth::user()->id : null;

        return [
            'license_type_id' => 'required|exists:license_types,id',
            'attachments' => 'sometimes|required|exists:pictures,id,author_id,'.$userId,
            'fields' => 'sometimes|required',
        ];
    }
}
