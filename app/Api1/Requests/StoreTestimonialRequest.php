<?php

namespace App\Api1\Requests;

use Auth;

class StoreTestimonialRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->hasRole('superadmin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'person_name' => 'string|required|max:255',
            'person_description' => 'string',
            'text' => 'string|required',
            'position' => 'integer|min:0|max:100000',
            'manager_id' => 'exists:managers,id'
        ];
    }
}
