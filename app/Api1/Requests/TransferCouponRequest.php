<?php

namespace App\Api1\Requests;

class TransferCouponRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'amount'    => 'required|numeric|min:1|max:2500',
                    'email'     => 'email|exists:users,email',
                    'fisher_id' => 'required_without:email|exists:users,fisher_id',
                ];
            case 'PUT':
                return [
                    'code'      => 'required|exists:coupons,code',
                ];
            default:
                return [];
        }
    }
}
