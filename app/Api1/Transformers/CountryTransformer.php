<?php

namespace App\Api1\Transformers;

use App\Models\Location\Country;
use League\Fractal\TransformerAbstract;

class CountryTransformer extends TransformerAbstract
{
    protected $detailed;

    protected $availableIncludes = ['states'];

    public function __construct($detailed = false)
    {
        $this->detailed = $detailed;
    }

    public function transform(Country $country)
    {
        $data = [
            'id' => (int)$country->id,
            'name' => $country->name,
            'slug' => $country->slug,
            'country_code' => $country->country_code,
            'links' => [
                'self' => route('v1.countries.show', ['id' => $country->id]),
            ],
        ];

        if ($country->has('states.regions.cities.areas.hauls')) {
            $data['links']['hauls'] = route('v1.hauls.index', ['country' => $country->id]);
        }

        if ($this->detailed) {
            $data['description'] = $country->description;
            $data['links']['approved'] = route('v1.areas.index', ['country' => $country->id, 'tickets' => true]);
        }

        return $data;
    }

    public function includeStates(Country $country)
    {
        return $this->collection($country->states, new StateTransformer());
    }
}
