<?php

namespace App\Api1\Transformers;

use App\Models\Authorization\Role;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['permissions'];

    public function transform(Role $role)
    {
        $data = [
            'id' => (int)$role->id,
            'name' => $role->name,
            'description' => $role->description,
            'links' => [
                'self' => route('v1.roles.show', ['id' => $role->id]),
            ],
        ];

        return $data;
    }

    public function includePermissions(Role $role)
    {
        return $this->collection($role->permissions, new PermissionTransformer());
    }
}
