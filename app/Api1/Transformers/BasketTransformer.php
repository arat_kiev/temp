<?php

namespace App\Api1\Transformers;

use App\Models\Area\AreaRating;
use Gloudemans\Shoppingcart\Cart;
use League\Fractal\TransformerAbstract;

class BasketTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['items'];

    protected $defaultIncludes = ['items'];

    public function transform(Cart $cart)
    {
        return [
            'count' => $cart->count(),
            'total' => $cart->subtotal(),
        ];
    }

    public function includeItems(Cart $cart)
    {
        return $this->collection($cart->content(), new BasketItemTransformer());
    }
}
