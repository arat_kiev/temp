<?php

namespace App\Api1\Transformers;

use App\Models\Ticket\LockPeriod;
use League\Fractal\TransformerAbstract;

class LockPeriodTransformer extends TransformerAbstract
{
    public function transform(LockPeriod $lockPeriod)
    {
        $data = [
            'lock_from' => $lockPeriod->lock_from->toIso8601String(),
            'lock_till' => $lockPeriod->lock_till->toIso8601String(),
        ];

        return $data;
    }
}
