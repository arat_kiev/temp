<?php

namespace App\Api1\Transformers;

use App\Models\Promo\Coupon;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class CouponTransformer extends TransformerAbstract
{
    private $detailed;

    protected $defaultIncludes = ['createdBy', 'createdFor'];

    protected $availableIncludes = ['createdBy', 'createdFor', 'promotion'];

    public function __construct(bool $detailed = false)
    {
        $this->detailed = $detailed;
    }

    public function transform(Coupon $coupon)
    {
        $data = [
            'id'            => $coupon->id,
            'type'          => $coupon->promotion ? $coupon->promotion->type : 'TRANSFER',
            'code'          => $coupon->code,
            'usageBonus'    => $coupon->usage_bonus,
            'isPublic'      => $coupon->is_public,
            'isActive'      => $coupon->promotion
                ? $coupon->promotion->is_active
                    && (!$coupon->promotion->valid_till || Carbon::now()->lt($coupon->promotion->valid_till))
                : !$coupon->deleted_at && !$coupon->usedBy()->count(),
            'isDeleted'     => (bool) $coupon->deleted_at,
            'links'         => [
                'self'  => route('v1.coupons.show', ['id' => $coupon->id]),
                'usage' => $coupon->promotion ? route('v1.coupons.use') : route('v1.coupons.transfer.use'),
            ],
        ];
        
        return $data;
    }
    
    public function includeCreatedBy(Coupon $coupon)
    {
        return $coupon->createdBy ? $this->item($coupon->createdBy, new UserTransformer()) : null;
    }

    public function includeCreatedFor(Coupon $coupon)
    {
        return $coupon->createdFor ? $this->item($coupon->createdFor, new UserTransformer()) : null;
    }

    public function includePromotion(Coupon $coupon)
    {
        return $coupon->promotion ? $this->item($coupon->promotion, new PromotionTransformer()) : null;
    }
}
