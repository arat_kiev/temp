<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\PictureTransformer;
use App\Models\Organization\News;
use League\Fractal\TransformerAbstract;

class NewsTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['image', 'author'];

    protected $defaultIncludes = ['image', 'author'];

    public function transform(News $news)
    {
        return [
            'id' => (int)$news->id,
            'title' => $news->title,
            'content' => $news->content,
            'published_on' => $news->published_on->format('Y-m-d H:i'),
            'links' => [
                'self' => route('v1.organizations.news.show', [
                    'organizationId' => $news->organization->id,
                    'newsId' => $news->id
                ]),
                'gallery' => route('v1.galleries.show', ['id' => $news->gallery->id]),
            ],
        ];
    }

    public function includeImage(News $news)
    {
        $image = $news->gallery->pictures()->first();

        return $image ? $this->item($image, new PictureTransformer()) : null;
    }

    public function includeAuthor(News $news)
    {
        return $this->item($news->author, new UserTransformer());
    }
}
