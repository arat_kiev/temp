<?php

namespace App\Api1\Transformers;

use App\Models\Area\AreaRating;
use League\Fractal\TransformerAbstract;

class AreaRatingTransformer extends TransformerAbstract
{
    public function transform(AreaRating $rating)
    {
        $data = [
            'id' => (int)$rating->id,
            'area_id' => (int)$rating->area_id,
            'user_id' => (int)$rating->user_id,
            'rating_cat_id' => (int)$rating->rating_category_id,
            'value' => (real)$rating->value,
        ];

        return $data;
    }
}
