<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\AvatarPictureTransformer;
use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserFollowsTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'avatar',
    ];

    protected $defaultIncludes = [
        'avatar',
    ];

    public function transform(User $user)
    {
        $data = [
            'id' => (int)$user->id,
            'name' => $user->name,
        ];

        return $data;
    }

    public function includeAvatar(User $user)
    {
        return $user->avatarPicture ? $this->item($user->avatarPicture, new AvatarPictureTransformer()) : null;
    }
}
