<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\LogoPictureTransformer;
use App\Api1\Transformers\Picture\PictureTransformer;
use App\Models\Contact\Manager;
use League\Fractal\TransformerAbstract;

class ManagerTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'logo',
        'signature',
        'organizations',
        'related_areas',
        'products',
    ];

    protected $defaultIncludes = ['logo', 'signature', 'related_areas'];

    protected $detailed;

    protected $area_id;

    public function __construct($detailed = false, $area_id = null)
    {
        $this->detailed = $detailed;
        $this->area_id = $area_id;
    }

    public function transform(Manager $manager)
    {
        $data = [
            'id'            => (int) $manager->id,
            'name'          => $manager->name,
            'street'        => $manager->street,
            'area_code'     => $manager->area_code,
            'city'          => $manager->city,
            'country'       => $manager->country ? $manager->country->name : null,
            'country_id'    => $manager->country ? $manager->country->id : null,
            'country_code'  => $manager->country ? $manager->country->country_code : null,
            'person'        => $manager->person,
            'phone'         => $manager->phone,
            'email'         => $manager->email,
            'website'       => $manager->website,
            'slug'          => $manager->slug,
            'config'        => $manager->config,
            'areas_count'   => $manager->areas()->count(),
            'links'         => [
                'self'  => route('v1.managers.show', ['id' => $manager->id]),
            ],
        ];

        if ($this->detailed) {
            $data['parent_id'] = (int) $manager->parent_id ?: null;
            $data['children'] = $manager->children->pluck('id')->toArray();
            $data['child_areas'] = $manager->childrenAreas()->pluck('areas.id')->toArray();
        }

        return $data;
    }

    public function includeLogo(Manager $manager)
    {
        return $manager->logo ? $this->item($manager->logo, new LogoPictureTransformer()) : null;
    }

    public function includeOrganizations(Manager $manager)
    {
        return $this->collection($manager->organizations, new OrganizationTransformer());
    }

    public function includeProducts(Manager $manager)
    {
        return $this->collection($manager->products, new ProductTransformer());
    }

    public function includeRelatedAreas(Manager $manager)
    {
        $areas = $manager->areas()->isPublic()->take(5);

        if ($this->area_id !== null) {
            $areas->where('id', '<>', $this->area_id);
        }

        return $this->collection($areas->get(), new SimpleAreaTransformer());
    }

    public function includeSignature(Manager $manager)
    {
        return $manager->signature ? $this->item($manager->signature, new PictureTransformer()) : null;
    }
}
