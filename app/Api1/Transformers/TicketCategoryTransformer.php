<?php

namespace App\Api1\Transformers;

use App\Models\Ticket\TicketCategory;
use League\Fractal\TransformerAbstract;

class TicketCategoryTransformer extends TransformerAbstract
{
    public function transform(TicketCategory $ticketCategory)
    {
        $data = [
            'id' => (int)$ticketCategory->id,
            'type' => $ticketCategory->type,
            'name' => $ticketCategory->name,
        ];

        return $data;
    }
}
