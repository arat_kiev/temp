<?php

namespace App\Api1\Transformers;

use App\Models\Notification;
use App\Models\User;
use League\Fractal\TransformerAbstract;
use Carbon\Carbon;

class NotificationTransformer extends TransformerAbstract
{

    public static $read = array();

    protected $availableIncludes = [
        'object',
        'subject'
    ];

    protected $defaultIncludes = [
        'object',
        'subject'
    ];

    public function transform(Notification $notification)
    {
        // collect returned notification's ids
        self::$read[] = $notification->id;

        $data = [
            'id' => (int) $notification->id,
            'type' => $notification->type,
            'created_at' => $notification->created_at->toDateTimeString(),
            'read_at' => $notification->read_at ? $notification->read_at->toDateTimeString(): null,
        ];

        return $data;
    }

    public function includeObject(Notification $notification)
    {
        if ($notification->object_id && $notification->object_type) {
            $transformer = 'App\Api1\Transformers\\' . class_basename($notification->object_type) . 'Transformer';
                return $this->item((new $notification->object_type)::find($notification->object_id), new $transformer());
        }
        return null;
    }

    public function includeSubject(Notification $notification)
    {
        return $notification->fromUser ? $this->item($notification->fromUser, new UserTransformer()) : null;
    }
}
