<?php

namespace App\Api1\Transformers;

use App\Models\Area\Rule;
use League\Fractal\TransformerAbstract;

class RuleTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['files'];

    protected $defaultIncludes = ['files'];

    public function transform(Rule $rule)
    {
        $data = [
            'id' => (int)$rule->id,
            'text' => $rule->text,
        ];

        return $data;
    }

    public function includeFiles(Rule $rule)
    {
        return $this->collection($rule->files()->public()->get(), new PdfTransformer());
    }
}
