<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\PictureTransformer;
use App\Models\Product\Product;
use App\Models\Product\ProductStock;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Auth;

class ProductTransformer extends TransformerAbstract
{
    private $detailed;

    protected $availableIncludes = [
        'category',
        'active_price',
        'related_products',
        'prices',
        'managers',
        'areas',
        'productStocks',
        'image',
    ];

    protected $defaultIncludes = ['category', 'active_price', 'image', 'productStocks'];

    public function __construct($detailed = false)
    {
        $this->detailed = $detailed;
    }

    public function transform(Product $product)
    {
        $allowDownload = Auth::user() && Auth::user()->productSales()->where('product_id', $product->id)->count();

        $data = [
            'id'            => $product->id,
            'name'          => $product->name,
            'short_description' => $product->short_description,
            'long_description'  => $product->long_description,
            'slug'          => $product->slug,
            'featured_from' => $product->featured_from ? $product->featured_from->toDateString() : '',
            'featured_till' => $product->featured_till ? $product->featured_till->toDateString() : '',
            'category_id'   => $product->category_id,
            'template'      => $product->template,
            'in_stock'       => $product->in_stock,
            'hidden_info'    => $this->allowHiddenInfo($product) ? nl2br(e($product->hidden_info)) : null,
            'total_quantity' => $product->productStocks->reduce(function (int $carry, ProductStock $item) {
                return $carry + $item->quantity;
            }, 0),
            'links'         => [
                'self'          => route('v1.products.show', ['id' => $product->id]),
            ],
        ];

        if ($this->detailed && $allowDownload) {
            // Default templates available: original, download
            $data['links']['attachment'] = $product->attachment
                ? route('imagecache', ['template' => 'download', 'filename' => $product->attachment])
                : null;
        }

        if ($this->detailed && $product->gallery) {
            $data['links']['gallery'] = route('v1.galleries.show', ['id' => $product->gallery->id]);
        }

        return $data;
    }

    public function includeCategory(Product $product)
    {
        return $this->item($product->category, new ProductCategoryTransformer());
    }

    public function includeActivePrice(Product $product)
    {
        $activePrice = $product->prices()
            ->where('visible_from', '<=', Carbon::now())
            ->where('valid_till', '>=', Carbon::now())
            ->first();

        return $activePrice ? $this->item($activePrice, new ProductPriceTransformer()) : null;
    }

    public function includeRelatedProducts(Product $product)
    {
        $relatedProducts = Product::where('category_id', $product->category_id)
            ->where('id', '!=', $product->id)
            ->take(5)
            ->get();

        return $relatedProducts ? $this->collection($relatedProducts, new self()) : null;
    }

    public function includePrices(Product $product)
    {
        return $this->collection($product->prices, new ProductPriceTransformer());
    }

    public function includeManagers(Product $product)
    {
        return $this->collection($product->managers, new ManagerTransformer());
    }

    public function includeAreas(Product $product)
    {
        return $this->collection($product->areas, new AreaTransformer());
    }

    public function includeProductStocks(Product $product)
    {
        return $this->collection($product->productStocks, new ProductStockTransformer());
    }

    public function includeImage(Product $product)
    {
        $image = $product->gallery ? $product->gallery->pictures()->first() : null;

        return $image ? $this->item($image, new PictureTransformer()) : null;
    }

    private function allowHiddenInfo($product)
    {
        if (!Auth::user()) {
            return false;
        }

        if (Auth::user()->productSales()->where('product_id', $product->id)->count() === 0) {
            return false;
        }

        if (Auth::user()->productSales()->where('product_id', $product->id)->whereHas('timeslotDates', function($query) {
            $query->whereDate('date', Carbon::now()->toDateString());
        })->count() === 0) {
            return false;
        }

        return true;
    }
}