<?php

namespace App\Api1\Transformers\Inspection;

use League\Fractal\TransformerAbstract;
use App\Models\Inspection\InspectionTour;
use App\Api1\Transformers\UserTransformer;

class InspectionTourTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['inspector'];

    protected $inspectionTour;

    public function __construct(InspectionTour $inspectionTour = null)
    {
        $this->inspectionTour = $inspectionTour;
    }

    public function transform(InspectionTour $inspectionTour)
    {
        $data = [
            'id'        => (int) $inspectionTour->id,
            'observations' => fractal()
                ->collection($inspectionTour->observations, new ObservationTransformer())
                ->toArray(),
            'inspector' => [
                'id' => (int) $inspectionTour->inspector_id,
                'inspectorInfo' => fractal()
                                    ->item($inspectionTour->inspector, new UserTransformer())
                                    ->toArray()
            ],
        ];

        return $data;
    }
}
