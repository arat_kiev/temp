<?php

namespace App\Api1\Transformers\Inspection;

use League\Fractal\TransformerAbstract;
use App\Models\Ticket\TicketInspection;
use App\Api1\Transformers\UserTransformer;

class TicketInspectionTransformer extends TransformerAbstract
{
    protected $ticketInspection;

    public function __construct(TicketInspection $ticketInspection = null)
    {
        $this->ticketInspection = $ticketInspection;
    }

    public function transform(TicketInspection $ticketInspection)
    {
        $data = [
            'id'        => (int) $ticketInspection->id,
            'inspection_tour' => $ticketInspection->inspectionTour,
            'inspector' => fractal()
                ->item($ticketInspection->inspectionTour->inspector, new UserTransformer())
                ->toArray()
        ];

        return $data;
    }
}
