<?php

namespace App\Api1\Transformers\Inspection;

use App\Api1\Transformers\FishTransformer;
use App\Models\Inspection\TicketInspectionHaul;
use League\Fractal\TransformerAbstract;

class TicketInspectionHaulsTransformer extends TransformerAbstract
{
    protected $ticketInspectionHaul;

    public function __construct(TicketInspectionHaul $ticketInspectionHaul = null)
    {
        $this->ticketInspectionHaul = $ticketInspectionHaul;
    }

    public function transform(TicketInspectionHaul $ticketInspectionHaul)
    {
        $data = [
            'id'   => (int) $ticketInspectionHaul->id,
            'fish' => fractal()
                ->item($ticketInspectionHaul->fish, new FishTransformer())
                ->toArray(),
        ];

        return $data;
    }
}
