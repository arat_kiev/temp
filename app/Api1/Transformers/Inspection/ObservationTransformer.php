<?php

namespace App\Api1\Transformers\Inspection;

use League\Fractal\TransformerAbstract;
use App\Models\Inspection\Observation;
use App\Api1\Transformers\UserTransformer;

class ObservationTransformer extends TransformerAbstract
{
    protected $observation;

    public function __construct(Observation $observation = null)
    {
        $this->observation = $observation;
    }

    public function transform(Observation $observation)
    {
        $data = [
            'id'        => (int) $observation->id,
            'category' => $observation->category->translate(app()->getLocale()),
            'gallery' => $observation->gallery->pictures,
            'comment' => $observation->comment,
            'location' => $observation->location
        ];

        return $data;
    }
}
