<?php

namespace App\Api1\Transformers;

use App\Models\Meta\Technique;
use App\Api1\Transformers\Picture\PictureTransformer;
use League\Fractal\TransformerAbstract;

class TechniqueTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['picture', 'gallery'];

    protected $defaultIncludes = ['picture'];

    protected $detailed;

    public function __construct($detailed = false)
    {
        $this->detailed = $detailed;
    }

    public function transform(Technique $technique)
    {
        $data = [
            'id' => (int)$technique->id,
            'name' => $technique->name,
            'slug' => $technique->slug,
            'links' => [
                'self' => route('v1.techniques.show', ['id' => $technique->id]),
                'comments' => route('v1.comments.index', ['entity' => 'techniques', 'id' => $technique->id]),
            ],
        ];

        if ($this->detailed) {
            $data['description'] = $technique->description;
        }

        return $data;
    }

    public function includePicture(Technique $technique)
    {
        return $technique->picture ? $this->item($technique->picture, new PictureTransformer()) : null;
    }

    public function includeGallery(Technique $technique)
    {
        $pictures = [];

        if ($technique->gallery) {
            foreach ($technique->gallery->pictures as $picture) {
                $pictures[] = $picture;
            }
        }

        return $pictures ? $this->collection($pictures, new HaulPictureTransformer()) : null;
    }
}
