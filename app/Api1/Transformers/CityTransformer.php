<?php

namespace App\Api1\Transformers;

use App\Models\Location\City;
use League\Fractal\TransformerAbstract;

class CityTransformer extends TransformerAbstract
{
    protected $detailed;

    public function __construct($detailed = false)
    {
        $this->detailed = $detailed;
    }

    public function transform(City $city)
    {
        $data = [
            'id' => (int)$city->id,
            'name' => $city->name,
            'slug' => $city->slug . '-' . $city->post_code,
            'post_code' => $city->post_code,
            'links' => [
                'self' => route('v1.cities.show', ['id' => $city->id]),
                'areas' => route('v1.areas.index', ['city' => $city->id]),
            ],
        ];

        if ($city->has('areas.hauls')) {
            $data['links']['hauls'] = route('v1.hauls.index', ['city' => $city->id]);
        }

        if ($this->detailed) {
            $data['region_id'] = $city->region->id;
            $data['region'] = $city->region->name;
            $data['state_id'] = $city->region->state->id;
            $data['state'] = $city->region->state->name;
            $data['country_id'] = $city->region->state->country->id;
            $data['country'] = $city->region->state->country->name;
            $data['country_code'] = $city->region->state->country->country_code;
            $data['description'] = $city->description;
        }

        return $data;
    }
}
