<?php

namespace App\Api1\Transformers;

use App\Models\Product\TimeDiscount;
use League\Fractal\TransformerAbstract;

class TimeDiscountTransformer extends TransformerAbstract
{
    public function transform(TimeDiscount $discount)
    {
        return [
            'id' =>         (int)$discount->id,
            'count' =>      (int)$discount->count,
            'discount' =>   (int)$discount->discount,
            'inclusive' =>  (bool)$discount->inclusive,
        ];
    }
}