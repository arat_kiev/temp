<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\PictureTransformer;
use App\Models\PointOfInterest\PointOfInterest;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class PointOfInterestTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'picture',
        'opening_hours',
        'coordinates'
    ];

    protected $defaultIncludes = [
        'picture',
        'opening_hours',
        'coordinates'
    ];

    public function transform(PointOfInterest $poi)
    {
        $data = [
            'id'            => (int) $poi->id,
            'name'          => $poi->name,
            'street'        => $poi->street,
            'post_code'     => $poi->post_code,
            'city'          => $poi->city,
            'country_id'    => $poi->country ? (int) $poi->country->id : null,
            'country'       => $poi->country ? $poi->country->name : null,
            'phone'         => $poi->phone,
            'email'         => $poi->email,
            'website'       => $poi->website,
            'description'   => $poi->description,
            'open_now'      => $this->openNow($poi),
            'links'         => [
                'self'          => route('v1.poi.show', ['id' => $poi->id]),
                'comments'      => route('v1.comments.index', ['entity' => 'points_of_interest', 'id' => $poi->id]),
            ],
        ];

        return $data;
    }

    public function includePicture(PointOfInterest $poi)
    {
        return $poi->picture ? $this->item($poi->picture, new PictureTransformer()) : null;
    }

    public function includeOpeningHours(PointOfInterest $poi)
    {
        $poi_oh = $poi->opening_hours()->where('poi_id', $poi->id)->get();
        return $poi_oh ? $this->collection($poi_oh, new PointOfInterestOpeningHourTransformer(), 'periods') : null;
    }

    public function includeCoordinates(PointOfInterest $poi)
    {
        return $poi->locations->first() ? $this->item($poi->locations->first(), new GeolocationTransformer()) : null;
    }

    /**
     * check open_now status
     * @return bool
     */
    private function openNow(PointOfInterest $poi)
    {
        $now = Carbon::now();

        // Get opening hours for today of this poi
        $openingHoursToday = $poi->opening_hours()->where('day', $now->format('D'))->get();

        // Check each period for current time intersecting
        foreach ($openingHoursToday as $openingHour) {
            $openFrom = $now->copy()->setTimeFromTimeString($openingHour->opens);
            $closesAt = $now->copy()->setTimeFromTimeString($openingHour->closes);

            if ($now->gte($openFrom) && $now->lte($closesAt)) {
                return true;
            }
        }

        return false;
    }
}
