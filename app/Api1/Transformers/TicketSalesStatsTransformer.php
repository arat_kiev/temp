<?php

namespace App\Api1\Transformers;

use App\Managers\TicketManager;
use App\Models\Ticket\Ticket;
use League\Fractal\TransformerAbstract;

class TicketSalesStatsTransformer extends TransformerAbstract
{
    static private $vat;
    static private $price = [];

    /** @var TicketManager */
    private $ticketManager;

    protected $availableIncludes = ['user', 'checkins'];
    protected $defaultIncludes = ['user', 'checkins'];

    public function __construct()
    {
        $this->ticketManager = new TicketManager();
    }

    /**
     * get VAT by country
     * @return mixed
     */
    public static function getVat()
    {
        if (!isset(self::$vat)) {
            self::$vat = config('app.manager')->country->vat;
        }
        return self::$vat;
    }

    /**
     * Count tickets
     * @return int
     */
    public static function getCount()
    {
        return empty(self::$price) ? 0 : count(current(self::$price));
    }

    /**
     * Convert values to strings to represent money format, e.g '100' -> '100,00', '100.55' => '100,55'
     * @param $value
     * @return string
     */
    private static function money($value)
    {
        return number_format((float)$value, 2, ',', '');
    }

    /**
     * Magic method for retrieving static properties
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        return isset(self::$price[$name]) ? end(self::$price[$name]) : null;
    }

    /**
     * Magic method for counting static properties. Example: to get vat_value $name should be getVatValue
     * @param $name
     * @param $args
     * @return null|number
     */
    public static function __callStatic($name, $args)
    {
        $name_parts = array_map("strtolower", preg_split('/(?=[A-Z])/', substr($name, 3), -1, PREG_SPLIT_NO_EMPTY));
        $name = count($name_parts) == 1 ? current($name_parts) : implode('_', $name_parts);
        return isset(self::$price[$name]) ? self::money(array_sum(self::$price[$name])) : null;
    }

    private function negativeIf($value, $when)
    {
        return $when ? -$value : $value;
    }

    public function transform(Ticket $ticket)
    {
        self::$price['gross'][] = $this->negativeIf($ticket->price->value / 100, $ticket->storno_id);
        self::$price['vat_percent'][] = $ticket->resellerTicket
            ? $ticket->resellerTicket->reseller->country->vat
            : $this->getVat();
        self::$price['price_net'][] = $this->gross / (1 + ($this->vat_percent / 100));
        self::$price['vat_value'][] = $this->gross - $this->price_net;

        $manager = $ticket->type->area->manager;
        $reseller = $ticket->resellerTicket ? $ticket->resellerTicket->reseller : null;

        $data = [
            'id'                => (int) $ticket->id,
            'sale_type'         => 'ticket',
            'manager-ticket-id' => (int) $ticket->manager_ticket_id,
            'ticket-id'         => $this->ticketManager->getIdentifier($ticket),
            'created_at'        => $ticket->created_at->toDateTimeString(),
            'valid_from'        => $ticket->valid_from->toDateTimeString(),
            'valid_to'          => $ticket->valid_to->toDateTimeString(),
            'duration'          => $ticket->checkins->sum('duration_in_minutes'),

            'ticket_type_id'                => $ticket->type->id,
            'ticket_type_account_number'    => $ticket->type->account_number,
            'ticket_price_id'               => $ticket->price->id,
            'ticket_price'                  => $ticket->price->name,
            'ticket_price_type'             => $ticket->price->type,

            'ticket_price_gross'    => $this->money($this->gross),
            'ticket_vat_percent'    => $this->vat_percent . '%',
            'ticket_vat_value'      => $this->money($this->vat_value),
            'ticket_price_net'      => $this->money($this->price_net),

            'storno_id'             => $ticket->storno_id,
            'storno_reason'         => $ticket->storno_reason,

            'area'                  => $ticket->type->area->name,
            'area_account_number'   => $ticket->type->area->account_number,

            'reseller'                  => $ticket->resellerTicket
                ? $ticket->resellerTicket->reseller->name
                : 'online',
            'reseller_id'               => $ticket->resellerTicket
                ? $ticket->resellerTicket->reseller->id
                : null,
            'reseller_account_number'   => $ticket->resellerTicket
                ? $manager->resellers()->find($reseller->id)->pivot->account_number
                : null,
        ];

        return $data;
    }

    public function includeUser(Ticket $ticket)
    {
        return $ticket->user
            ? $this->item($ticket->user, new UserTransformer(true, ['links', 'balance', 'active']))
            // Ticket was bought via reseller, user's data from reseller's ticket
            : $this->item($ticket->resellerTicket, new ResellerUserTransformer());
    }

    public function includeCheckins(Ticket $ticket)
    {
        return $this->collection($ticket->checkins, new CheckinTransformer());
    }
}
