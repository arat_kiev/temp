<?php

namespace App\Api1\Transformers;

use App\Models\Ticket\QuotaUnit;
use League\Fractal\TransformerAbstract;

class QuotaUnitTransformer extends TransformerAbstract
{
    public function transform(QuotaUnit $quotaUnit)
    {
        $data = [
            'id' => (int)$quotaUnit->id,
            'type' => $quotaUnit->type,
            'name' => $quotaUnit->name,
        ];

        return $data;
    }
}
