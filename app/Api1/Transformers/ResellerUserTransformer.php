<?php

namespace App\Api1\Transformers;

use App\Models\Ticket\ResellerTicket;
use League\Fractal\TransformerAbstract;

class ResellerUserTransformer extends TransformerAbstract
{

    public function transform(ResellerTicket $resellerUser)
    {
        $data = [
            'first_name' => $resellerUser->first_name,
            'last_name' => $resellerUser->last_name,
            'street' => $resellerUser->street,
            'post_code' => $resellerUser->post_code,
            'city' => $resellerUser->city,
            'country' => $resellerUser->country ? $resellerUser->country->name : null,
            'birthday' => $resellerUser->birthday ? $resellerUser->birthday->toIso8601String() : null,
            'phone' => $resellerUser->phone,
            'email' => $resellerUser->email,
        ];
        return $data;
    }
}
