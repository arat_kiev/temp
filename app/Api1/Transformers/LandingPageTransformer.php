<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\PictureTransformer;
use App\Models\LandingPages\LandingPage;
use League\Fractal\TransformerAbstract;

class LandingPageTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['area', 'picture'];

    protected $defaultIncludes = ['picture'];

    protected $detailed;

    public function __construct($detailed = false)
    {
        $this->detailed = $detailed;
    }

    public function transform(LandingPage $lp)
    {
        $data = [
            'id'    => (int) $lp->id,
            'slug'  => $lp->slug,
            'links' => [
                'self' => route('v1.lp.show', ['id' => $lp->id]),
            ],
        ];

        if ($this->detailed) {
            $data['title'] = $lp->title;
            $data['headline'] = $lp->headline ?: '';
            $data['punchline'] = $lp->punchline ?: '';
            $data['description'] = $lp->description ?: '';
            if ($lp->areas()->count() > 15) {
                $data['links']['nextPage'] = route('v1.areas.index', ['part_of' => $lp->id, 'page' => 2]);
            }
        }

        return $data;
    }

    public function includeArea(LandingPage $lp)
    {
        return $this->collection($lp->areas()->limit(15)->get(), new AreaTransformer());
    }

    public function includePicture(LandingPage $lp)
    {
        return $lp->picture ? $this->item($lp->picture, new PictureTransformer()) : null;
    }
}