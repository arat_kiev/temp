<?php

namespace App\Api1\Transformers;

use App\Models\Ticket\TicketDay;
use League\Fractal\TransformerAbstract;

class TicketDayTransformer extends TransformerAbstract
{
    public function transform(TicketDay $ticketDay)
    {
        $data = [
            'day' => $ticketDay->weekday,
        ];

        return $data;
    }
}
