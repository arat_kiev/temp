<?php

namespace App\Api1\Transformers;

use App\Models\Picture;
use League\Fractal\TransformerAbstract;

class HaulPictureTransformer extends TransformerAbstract
{
    protected $square;

    public function __construct($square = false)
    {
        $this->square = $square;
    }

    public function transform(Picture $picture)
    {
        $tpl = $this->square ? 'sq' : 're';

        $data = [
            'haul_id' => (int)$picture->haul_id,
            'picture_id' => (int)$picture->id,
            'name' => $picture->name,
            'description' => $picture->description,
            'uploaded_by' => $picture->author ? $picture->author->name : null,
            'author_name' => $picture->author
                ? $picture->author->first_name . ' ' . $picture->author->last_name
                : null,
            'files' => [
                'small' => route('imagecache', ['template' => 's' . $tpl, 'filename' => $picture->file]),
                'medium' => route('imagecache', ['template' => 'm' . $tpl, 'filename' => $picture->file]),
                'large' => route('imagecache', ['template' => 'l' . $tpl, 'filename' => $picture->file]),
                'original' => route('imagecache', ['template' => 'org', 'filename' => $picture->file]),
            ],
            'links' => [
                'self' => route('v1.pictures.show', ['id' => $picture->id]),
            ],
        ];

        return $data;
    }
}
