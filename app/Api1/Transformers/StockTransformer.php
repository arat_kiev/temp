<?php

namespace App\Api1\Transformers;

use App\Models\Stock;
use League\Fractal\TransformerAbstract;

class StockTransformer extends TransformerAbstract
{
    private $detailed;

    protected $availableIncludes = ['products'];

    protected $defaultIncludes = [];

    public function __construct(bool $detailed = false)
    {
        $this->detailed = $detailed;
    }

    public function transform(Stock $stock) : array
    {
        $data = [
            'id'            => $stock->id,
            'name'          => $stock->name,
            'countryId'     => $stock->country_id,
            'countryName'   => $stock->country ? $stock->country->name : null,
            'city'          => $stock->city,
            'street'        => $stock->street,
            'building'      => $stock->building,
            'address'       => $stock->address,
            'restInfo'      => $stock->rest_info,
            'links'         => [
                'self'  => route('v1.stocks.show', ['id' => $stock->id]),
            ],
        ];

        if ($this->detailed) {
            $data += [
                'emails'    => $stock->emails,
                'phones'    => $stock->phones,
            ];
        }

        return $data;
    }
    
    public function includeProducts(Stock $stock)
    {
        return $this->collection($stock->products, new ProductTransformer());
    }
}
