<?php

namespace App\Api1\Transformers;

use App\Models\Location\Region;
use League\Fractal\TransformerAbstract;

class RegionTransformer extends TransformerAbstract
{
    protected $detailed;

    protected $availableIncludes = ['cities'];

    public function __construct($detailed = false)
    {
        $this->detailed = $detailed;
    }

    public function transform(Region $region)
    {
        $data = [
            'id' => (int)$region->id,
            'name' => $region->name,
            'slug' => $region->slug,
            'links' => [
                'self' => route('v1.regions.show', ['id' => $region->id]),
            ],
        ];

        if ($region->has('cities.areas.hauls')) {
            $data['links']['hauls'] = route('v1.hauls.index', ['region' => $region->id]);
        }

        if ($this->detailed) {
            $data['state_id'] = $region->state->id;
            $data['state'] = $region->state->name;
            $data['country_id'] = $region->state->country->id;
            $data['country'] = $region->state->country->name;
            $data['country_code'] = $region->state->country->country_code;
            $data['description'] = $region->description;
            $data['links']['approved'] = route('v1.areas.index', ['region' => $region->id, 'tickets' => true]);
        }

        return $data;
    }

    public function includeCities(Region $region)
    {
        return $this->collection($region->cities, new CityTransformer());
    }
}
