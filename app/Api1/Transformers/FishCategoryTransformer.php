<?php

namespace App\Api1\Transformers;

use App\Models\Meta\FishCategory;
use League\Fractal\TransformerAbstract;

class FishCategoryTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['fishes'];

    protected $defaultIncludes = [];

    protected $detailed;

    public function __construct($detailed = false)
    {
        $this->detailed = $detailed;
    }

    public function transform(FishCategory $fishCategory)
    {
        $data = [
            'id' => (int)$fishCategory->id,
            'name' => $fishCategory->name,
            'slug' => $fishCategory->slug,
            'links' => [
                'self' => route('v1.fishes.show', ['id' => $fishCategory->id]),
            ],
        ];

        if ($this->detailed) {
            $data['description'] = $fishCategory->description;
        }

        return $data;
    }

    public function includeFishes(FishCategory $fishCategory)
    {
        return $fishCategory->fishes ? $this->collection($fishCategory->fishes, new FishTransformer()) : null;
    }
}
