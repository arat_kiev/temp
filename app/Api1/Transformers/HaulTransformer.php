<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\PictureTransformer;
use App\Models\Haul;
use App\Models\HaulVotes;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;

class HaulTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['user', 'area', 'fish', 'technique', 'picture'];

    protected $defaultIncludes = ['user', 'fish'];

    public function transform(Haul $haul)
    {
        $data = [
            'id'            => (int) $haul->id,
            'ticket_id'     => $haul->ticket_id ? $haul->ticket_id : $haul->ticket_number,
            'area_name'     => $haul->area ? $haul->area->name : $haul->area_name,
            'taken'         => (boolean) $haul->taken,
            'vote'          => (int) $haul->votes(),
            'userVote'      => $this->fetchUserVote($haul),
            'checkin_id'    => $haul->checkin_id,
            //'likes'         => (int) $haul->likes(),
            //'dislikes'      => (int) $haul->dislikes(),
            'count'         => $haul->count,
            'size_value'    => $haul->size_value,
            'size_type'     => $haul->size_type,
            'weather'       => $haul->weather,
            'temperature'   => $haul->temperature,
            'user_comment'  => $haul->user_comment,
            'catch_date'    => $haul->catch_date->toDateString(),
            'catch_time'    => $haul->catch_time ? $haul->catch_time : null,
            'links'         => [
                'self'          => route('v1.hauls.show', ['id' => $haul->id]),
                'comments'      => route('v1.comments.index', ['entity' => 'hauls', 'id' => $haul->id]),
            ],
        ];

        return $data;
    }

    private function fetchUserVote(Haul $haul)
    {
        return Auth::user()
            ? ($userVote = HaulVotes::findVote($haul->id, Auth::user()->id))
                ? $userVote->vote
                : 0
            : 0;
    }

    public function includeUser(Haul $haul)
    {
        return $haul->user ? $this->item($haul->user, new UserTransformer()) : null;
    }

    public function includeArea(Haul $haul)
    {
        return $haul->area ? $this->item($haul->area, new AreaTransformer()) : null;
    }

    public function includeFish(Haul $haul)
    {
        return $haul->fish ? $this->item($haul->fish, new FishTransformer()) : null;
    }

    public function includeTechnique(Haul $haul)
    {
        return $haul->technique ? $this->item($haul->technique, new TechniqueTransformer()) : null;
    }

    public function includePicture(Haul $haul)
    {
        return $haul->picture ? $this->item($haul->picture, new PictureTransformer()) : null;
    }
}
