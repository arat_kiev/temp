<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\LogoPictureTransformer;
use App\Models\Organization\Organization;
use League\Fractal\TransformerAbstract;

class OrganizationTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'logo', 'news', 'memberAreas'
    ];

    public function transform(Organization $organization)
    {
        $data = [
            'id' => (int)$organization->id,
            'name' => $organization->name,
            'street' => $organization->street,
            'area_code' => $organization->area_code,
            'city' => $organization->city,
            'country' => $organization->country ? $organization->country->name : null,
            'country_id' => $organization->country ? $organization->country->id : null,
            'country_code' => $organization->country ? $organization->country->country_code : null,
            'person' => $organization->person,
            'phone' => $organization->phone,
            'email' => $organization->email,
            'website' => $organization->website,
            'slug' => $organization->slug,
            'links' => [
                'self' => route('v1.organizations.show', ['id' => $organization->id]),
                'news' => route('v1.organizations.news.index', ['id' => $organization->id]),
            ],
        ];

        return $data;
    }

    public function includeLogo(Organization $organization)
    {
        return $organization->logo ? $this->item($organization->logo, new LogoPictureTransformer()) : null;
    }

    public function includeManager(Organization $organization)
    {
        return $organization->manager ? $this->item($organization->manager, new ManagerTransformer()) : null;
    }

    public function includeNews(Organization $organization)
    {
        return $organization->news
            ? $this->collection(
                $organization->news()->orderBy('published_on', 'desc')->limit(5)->get(),
                new NewsTransformer()
            )
            : null;
    }

    public function includeMemberAreas(Organization $organization)
    {
        $selection = collect();

        $organization->ticketPrices->each(function ($ticketPrice) use ($selection) {
            $selection->push($ticketPrice->ticketType->area);
        });

        return $selection->count()
            ? $this->collection($selection->splice(5)->all(), new AreaTransformer())
            : null;
    }
}
