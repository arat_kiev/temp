<?php

namespace App\Api1\Transformers;

use App\Models\Location\State;
use League\Fractal\TransformerAbstract;

class StateTransformer extends TransformerAbstract
{
    protected $detailed;

    protected $availableIncludes = ['regions'];

    public function __construct($detailed = false)
    {
        $this->detailed = $detailed;
    }

    public function transform(State $state)
    {
        $data = [
            'id' => (int)$state->id,
            'name' => $state->name,
            'slug' => $state->slug,
            'links' => [
                'self' => route('v1.states.show', ['id' => $state->id]),
            ],
        ];

        if ($state->has('regions.cities.areas.hauls')) {
            $data['links']['hauls'] = route('v1.hauls.index', ['state' => $state->id]);
        }

        if ($this->detailed) {
            $data['country_id'] = $state->country->id;
            $data['country'] = $state->country->name;
            $data['country_code'] = $state->country->country_code;
            $data['description'] = $state->description;
            $data['links']['approved'] = route('v1.areas.index', ['state' => $state->id, 'tickets' => true]);
        }

        return $data;
    }

    public function includeRegions(State $state)
    {
        return $this->collection($state->regions, new RegionTransformer());
    }
}
