<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\PictureTransformer;
use App\Models\License\License;
use League\Fractal\TransformerAbstract;

class LicenseTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['user', 'type', 'attachments'];

    protected $defaultIncludes = ['type', 'attachments'];

    public function transform(License $license)
    {
        $data = [
            'id' => (int)$license->id,
            'name' => $license->type->name,
            'fields' => $license->fields,
            'status' => $license->status,
            'status_text' => $license->status_text,
            'valid_from' => $license->valid_from ? $license->valid_from->toDateString() : null,
            'valid_to' => $license->valid_to  ? $license->valid_to->toDateString() : null,
            'links' => [
                'self' => route('v1.licenses.show', ['id' => $license->id]),
            ],
        ];

        return $data;
    }

    public function includeUser(License $license)
    {
        return $this->item($license->user, new UserTransformer());
    }

    public function includeType(License $license)
    {
        return $this->item($license->type, new LicenseTypeTransformer());
    }

    public function includeAttachments(License $license)
    {
        return $this->collection($license->attachments, new PictureTransformer());
    }
}
