<?php

namespace App\Api1\Transformers\Picture;

use App\Models\Picture;
use League\Fractal\TransformerAbstract;

class PictureTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'files',
        'links',
    ];

    protected $defaultIncludes = [
        'files',
        'links',

    ];
    // Default values
    protected $shape = 're';
    protected $size = ['s', 'm', 'l'];

    private $size_name = ['small' => 's', 'medium' => 'm', 'large' => 'l'];
    private $available_shapes = ['re', 'sq'];


    public function __construct($shape = false, $size = false)
    {
        $this->shape = $shape ?: $this->shape;
        $this->size = $size ?: $this->size;
    }

    public function transform(Picture $picture)
    {
        $data = [
            'id' => (int)$picture->id,
            'name' => $picture->name,
            'description' => $picture->description,
            'user_supplied' => !is_null($picture->author),
        ];

        return $data;
    }

    public function includeFiles(Picture $picture)
    {
        // Get type by class name
        $type_name = preg_split('/(?=[A-Z])/', class_basename(get_class($this)))[1];
        $type = ($type_name == 'Picture') ? '' : $type_name;
        // Validating size and shape
        $this->size = array_intersect($this->size_name, is_array($this->size) ? $this->size : (array)$this->size);
        $this->shape = is_string($this->shape) ? current(array_intersect($this->available_shapes, (array)$this->shape)) : 're';

        return $this->item($picture, function ($picture) use ($type) {

            $files = ['original' => route('imagecache', ['template' => 'org', 'filename' => $picture->file])];
            // Creating template name from size.shape.type e.g. `m.sq.Avatar`
            foreach ($this->size as $name => $size) {
                $files[$name] = route('imagecache', ['template' => $size . $this->shape . $type, 'filename' => $picture->file]);
            }
            return $files;
        });
    }

    public function includeLinks(Picture $picture)
    {
        return $this->item($picture, function ($picture) {
            return [
                'self' => route('v1.pictures.show', ['id' => $picture->id]),
            ];
        });
    }
}
