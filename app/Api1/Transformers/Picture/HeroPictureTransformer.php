<?php

namespace App\Api1\Transformers\Picture;

use App\Models\Picture;

class HeroPictureTransformer extends PictureTransformer
{

    public function transform(Picture $picture)
    {
        return parent::transform($picture);
    }
}
