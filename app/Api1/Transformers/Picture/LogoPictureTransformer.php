<?php

namespace App\Api1\Transformers\Picture;

use App\Models\Picture;

class LogoPictureTransformer extends PictureTransformer
{
    // for `logo` available only `size` change
    protected $shape = '';

    public function __construct($size = false)
    {
        $this->size = $size ?: $this->size;
    }

    public function transform(Picture $picture)
    {
        $data = [
            'id' => (int)$picture->id,
        ];

        return $data;
    }
}
