<?php

namespace App\Api1\Transformers\Picture;

use App\Models\Picture;

class AvatarPictureTransformer extends PictureTransformer
{
    // for `avatar` available only `shape` change
    protected $size = 's';

    public function __construct($shape = false)
    {
        $this->shape = $shape ?: $this->shape;
    }

    public function transform(Picture $picture)
    {
        return parent::transform($picture);
    }
}
