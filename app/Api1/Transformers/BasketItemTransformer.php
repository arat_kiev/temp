<?php

namespace App\Api1\Transformers;

use Gloudemans\Shoppingcart\CartItem;
use League\Fractal\TransformerAbstract;

class BasketItemTransformer extends TransformerAbstract
{
    public function transform(CartItem $item)
    {
        $data = [
            'rowId' => $item->rowId,
            'type' => $item->model->getBuyableType(),
            'id' => $item->id,
            'name' => $item->name,
            'qty' => $item->qty,
            'price' => $item->price(),
            'subtotal' => $item->subtotal(),
            'options' => $item->options,
        ];

        switch ($item->model->getBuyableType()) {
            case 'ticket':
                $data['ticketType'] = fractal()->item($item->model->ticketType, new TicketTypeTransformer())->toArray();
                $data['area'] = fractal()->item($item->model->ticketType->area, new AreaTransformer())->toArray();
                break;
            case 'product':
                $data['product'] = fractal()->item($item->model->product, new ProductTransformer())->toArray();

                break;
        }

        return $data;
    }
}