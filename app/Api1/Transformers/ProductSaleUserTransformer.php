<?php

namespace App\Api1\Transformers;

use App\Models\Product\ProductSale;
use League\Fractal\TransformerAbstract;

class ProductSaleUserTransformer extends TransformerAbstract
{

    public function transform(ProductSale $sale)
    {
        return [
            'first_name'    => $sale->first_name,
            'last_name'     => $sale->last_name,
            'street'        => $sale->street,
            'post_code'     => $sale->post_code,
            'city'          => $sale->city,
            'country'       => $sale->country ? $sale->country->name : null,
            'email'         => $sale->email,
        ];
    }
}
