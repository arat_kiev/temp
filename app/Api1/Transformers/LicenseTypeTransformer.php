<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\LogoPictureTransformer;
use App\Models\License\LicenseType;
use League\Fractal\TransformerAbstract;

class LicenseTypeTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['examples'];

    public function transform(LicenseType $licenseType)
    {
        return [
            'id' => (int)$licenseType->id,
            'name' => $licenseType->name,
            'states' => $licenseType->states,
            'links' => [
                'self' => route('v1.licensetypes.show', ['id' => $licenseType->id]),
            ],
            'fields' => $licenseType->fields,
            'attachments' => $licenseType->attachments,
        ];
    }

    public function includeExamples(LicenseType $licenseType)
    {
        return $this->collection($licenseType->examples, new LogoPictureTransformer());
    }
}
