<?php

namespace App\Api1\Transformers;

use App\Models\Promo\Promotion;
use League\Fractal\TransformerAbstract;

class PromotionTransformer extends TransformerAbstract
{
    private $detailed;

    protected $defaultIncludes = ['createdBy'];

    protected $availableIncludes = ['createdBy', 'promotion'];

    public function __construct(bool $detailed = false)
    {
        $this->detailed = $detailed;
    }

    public function transform(Promotion $promo)
    {
        $data = [
            'id'            => $promo->id,
            'type'          => $promo->type,
            'usageBonus'    => $promo->usage_bonus,
            'ownerBonus'    => $promo->owner_bonus,
            'validFrom'     => $promo->valid_from->toIso8601String(),
            'validTill'     => $promo->valid_till->toIso8601String(),
            'isActive'      => $promo->is_active,
            'links'         => [
                'self' => route('v1.promotions.show', ['id' => $promo->id]),
            ],
        ];

        return $data;
    }

    public function includeCreatedBy(Promotion $promo)
    {
        return $promo->createdBy ? $this->item($promo->createdBy, new UserTransformer()) : null;
    }

    public function includeCoupons(Promotion $promo)
    {
        return $this->collection($promo->coupons, new CouponTransformer());
    }
}
