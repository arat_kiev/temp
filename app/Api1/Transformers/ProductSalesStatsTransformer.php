<?php

namespace App\Api1\Transformers;

use App\Managers\ProductSaleManager;
use App\Models\Product\ProductSale;
use League\Fractal\TransformerAbstract;

class ProductSalesStatsTransformer extends TransformerAbstract
{
    static private $vat;
    static private $price = [];

    protected $availableIncludes = ['user'];
    protected $defaultIncludes = ['user'];

    private $saleManager;

    public function __construct()
    {
        $this->saleManager = new ProductSaleManager();
    }

    /**
     * Magic method for retrieving static properties
     * 
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        return isset(self::$price[$name]) ? end(self::$price[$name]) : null;
    }

    /**
     * Magic method for counting static properties. Example: to get vat_value $name should be getVatValue
     * 
     * @param $name
     * @param $args
     * 
     * @return null|number
     */
    public static function __callStatic($name, $args)
    {
        $nameParts = array_map(
            "strtolower",
            preg_split('/(?=[A-Z])/', substr($name, 3), -1, PREG_SPLIT_NO_EMPTY)
        );
        $name = count($nameParts) == 1 ? current($nameParts) : implode('_', $nameParts);
        
        return isset(self::$price[$name])
            ? self::money(array_sum(self::$price[$name]))
            : null;
    }

    /**
     * Get manager VAT by country
     * 
     * @return mixed
     */
    public static function getVat()
    {
        if (!isset(self::$vat)) {
            self::$vat = config('app.manager')->country->vat;
        }
        
        return self::$vat;
    }

    /**
     * Count tickets
     * 
     * @return int
     */
    public static function getCount()
    {
        return empty(self::$price) ? 0 : count(current(self::$price));
    }

    /**
     * Convert values to strings to represent money format, e.g '100' -> '100,00', '100.55' => '100,55'
     * 
     * @param $value
     * @return string
     */
    private static function money($value)
    {
        return number_format((float) $value, 2, ',', '');
    }

    private function negativeIf($value, $when)
    {
        return $when ? -$value : $value;
    }

    public function transform(ProductSale $sale)
    {
        self::$price['gross'][] = $this->negativeIf(
            ($sale->price->value_origin / 100),
            $sale->storno_id
        );
        self::$price['vat_percent'][] = $sale->reseller
            ? $sale->reseller->country->vat
            : $this->getVat();
        self::$price['price_net'][] = $this->gross / (1 + ($this->vat_percent / 100));
        self::$price['vat_value'][] = $this->gross - $this->price_net;

        $managers = $sale->product->managers;
        $reseller = $sale->reseller ?? null;

        $data = [
            'id'                => (int) $sale->id,
            'sale_type'         => 'product',
            'ticket-id'           => $this->saleManager->getIdentifier($sale),
            'product_type_id'        => $sale->product->id,
            'ticket_type_account_number'    => $sale->product->account_number,
            'created_at'         => $sale->created_at->toDateTimeString(),
            'featured_from'     => $sale->product->featured_from
                ? $sale->product->featured_from->toDateTimeString()
                : null,
            'featured_till'     => $sale->product->featured_till
                ? $sale->product->featured_till->toDateTimeString()
                : null,

            'ticket_price_gross'       => $this->money($this->gross),
            'ticket_vat_percent'       => $this->vat_percent . '%',
            'ticket_vat_value'         => $this->money($this->vat_value),
            'ticket_price_net'         => $this->money($this->price_net),

            'storno_id'         => $sale->storno_id,
            'storno_reason'     => $sale->storno_reason,

            'areas'             => $sale->product->areas()->pluck('account_number', 'name')->toArray(),

            'reseller'          => $reseller ? $sale->reseller->name : 'online',
            'reseller_id'       => $reseller ? $sale->reseller->id : null,
            'reseller_account_number'   => $reseller
                ? $managers
                    ->reduce(function ($carry, $manager) use ($reseller) {
                        $accountNumber = $manager->resellers()->find($reseller->id)
                            ? $manager->resellers()->find($reseller->id)->pivot->account_number
                            : null;
                        return $accountNumber ? $carry + [$accountNumber] : $carry;
                    }, [])
                : null,
        ];

        return $data;
    }

    public function includeUser(ProductSale $sale)
    {
        return $sale->user
            ? $this->item($sale->user, new UserTransformer(true, ['links', 'balance', 'active']))
            // Product was bought via reseller, user's data from sale
            : $this->item($sale, new ProductSaleUserTransformer());
    }
}
