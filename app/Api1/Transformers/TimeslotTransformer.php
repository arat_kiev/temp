<?php

namespace App\Api1\Transformers;

use App\Models\Product\Timeslot;
use League\Fractal\TransformerAbstract;

class TimeslotTransformer extends TransformerAbstract
{
    public function transform(Timeslot $timeslot)
    {
        return [
            'id' => $timeslot->id,
            'product_price_id' => $timeslot->product_price_id,
            'from' => $timeslot->from->format('H:i'),
            'till' => $timeslot->till->format('H:i'),
        ];
    }
}