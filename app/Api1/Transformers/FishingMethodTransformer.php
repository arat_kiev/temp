<?php

namespace App\Api1\Transformers;

use App\Models\Meta\FishingMethod;
use League\Fractal\TransformerAbstract;

class FishingMethodTransformer extends TransformerAbstract
{
    public function transform(FishingMethod $fishingMethod)
    {
        $data = [
            'id'        => (int) $fishingMethod->id,
            'name'      => $fishingMethod->name,
            'description' => $fishingMethod->description,
         ];

        return $data;
    }
}
