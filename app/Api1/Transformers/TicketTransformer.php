<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\PictureTransformer;
use App\Models\Meta\FishingMethod;
use App\Models\Ticket\Ticket;
use League\Fractal\TransformerAbstract;

class TicketTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['user', 'type', 'price', 'hauls', 'checkins', 'signature'];

    protected $detailed;

    public function __construct($detailed = false)
    {
        $this->detailed = $detailed;
    }

    public function transform(Ticket $ticket)
    {
        $data = [
            'id'            => (int) $ticket->id,
            'ticket_number' => app('App\Managers\TicketManager')->getIdentifier($ticket),
            'area'          => $ticket->type->area->name,
            'active'        => (boolean) $ticket->active,
            'valid_from'    => $ticket->valid_from->toIso8601String(),
            'valid_to'      => $ticket->valid_to->toIso8601String(),
            'created_at'    => $ticket->created_at->toIso8601String(),
            'user_id'       => $ticket->user ? $ticket->user->id : null,
            'user'       => $ticket->user
                ? fractal()->item($ticket->user, new UserTransformer(true))->toArray()
                : null,
            'type_id'       => $ticket->type->id,
            'price_id'      => $ticket->price->id,
            'phone_ticket'  => (boolean)$ticket->type->area->phone_ticket,
            'area_id'       => $ticket->type->area->id,
            'additional_areas'      => $ticket->type->additional_areas,
            'fishes'                => $ticket->type->area->fishes,
            'techniques'            => $ticket->type->area->techniques,
            'fishingMethods'        => fractal()->collection(FishingMethod::all(), new FishingMethodTransformer)->toArray(),
            'isCatchlogRequired'    => (boolean) $ticket->type->is_catchlog_required,
            'hasCatchlogUploaded'   => (boolean) $ticket->hauls()->count(),
            'links'         => [
                'self' => $ticket->user
                    ? route('v1.users.tickets.show', ['user' => $ticket->user->id, 'ticket' => $ticket->id])
                    : null,
            ],
        ];

        if ($ticket->type->group) {
            $data['group_data'] = $ticket->group_data;
        }

        if ($this->detailed) {
            $data['security'] = [
                'qrcode' => app('App\Managers\TicketManager')->getQrCodeData($ticket),
                'vfcode' => app('App\Managers\TicketManager')->getControlCode($ticket),
            ];
        }

        return $data;
    }

    public function includeCheckins(Ticket $ticket)
    {
        return $this->collection($ticket->checkins, new CheckinTransformer());
    }

    public function includeUser(Ticket $ticket)
    {
        return $this->item($ticket->user, new UserTransformer());
    }

    public function includeType(Ticket $ticket)
    {
        return $this->item($ticket->type, new TicketTypeTransformer());
    }

    public function includePrice(Ticket $ticket)
    {
        return $this->item($ticket->price, new TicketPriceTransformer());
    }

    public function includeHauls(Ticket $ticket)
    {
        return $this->collection($ticket->hauls, new HaulTransformer());
    }

    public function includeSignature(Ticket $ticket)
    {
        $manager = $ticket->type->area->manager;
        return $manager->signature ? $this->item($manager->signature, new PictureTransformer()) : null;
    }
}
