<?php

namespace App\Api1\Transformers;

use App\Models\Ticket\Invoice;
use League\Fractal\TransformerAbstract;

class InvoiceTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['manager', 'reseller', 'tickets'];

    protected $defaultIncludes = [];

    public function transform(Invoice $invoice)
    {
        $data = [
            'id'            => (int) $invoice->id,
            'manager_id'    => $invoice->manager_id,
            'reseller_id'   => $invoice->reseller_id,
            'date_from'     => $invoice->date_from->format('d.m.y'),
            'date_till'     => $invoice->date_till->format('d.m.y'),
            'created_by'    => $invoice->createdBy ? $invoice->createdBy->name : '',
            'updated_by'    => $invoice->updatedBy ? $invoice->updatedBy->name : '',
            'deleted_by'    => $invoice->deletedBy ? $invoice->deletedBy->name : '',
            'links'         => [
                'self'  => route('v1.resellers.invoices.show', [
                    'reseller'  => $invoice->reseller->id,
                    'invoice'   => $invoice->id
                ]),
                'pdf'   => route('v1.resellers.invoices.pdf', [
                    'reseller'  => $invoice->reseller->id,
                    'invoice'   => $invoice->id
                ]),
            ],
        ];

        return $data;
    }

    public function includeManager(Invoice $invoice)
    {
        return $this->item($invoice->manager, new ManagerTransformer());
    }

    public function includeReseller(Invoice $invoice)
    {
        return $this->item($invoice->reseller, new ResellerTransformer());
    }

    public function includeTickets(Invoice $invoice)
    {
        return $this->collection($invoice->tickets, new TicketTransformer());
    }
}