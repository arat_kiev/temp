<?php

namespace App\Api1\Transformers;

use App\Models\Product\FeeCategory;
use League\Fractal\TransformerAbstract;

class FeeCategoryTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['fees'];

    protected $defaultIncludes = [];

    public function transform(FeeCategory $feeCategory)
    {
        $data = [
            'id'    => $feeCategory->id,
            'name'  => $feeCategory->name,
        ];

        return $data;
    }

    public function includeFees(FeeCategory $feeCategory)
    {
        return $this->collection($feeCategory->fees, new FeeTransformer());
    }
}
