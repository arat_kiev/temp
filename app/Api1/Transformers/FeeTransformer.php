<?php

namespace App\Api1\Transformers;

use App\Models\Product\Fee;
use League\Fractal\TransformerAbstract;

class FeeTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['country', 'category'];

    protected $defaultIncludes = ['category'];

    public function transform(Fee $fee)
    {
        $data = [
            'id'    => $fee->id,
            'name'  => $fee->name,
            'type'  => $fee->type,
            'value' => $fee->value,
            'value_with_mark'   => $fee->value_with_mark,
            'country_name'      => $fee->country->name,
            'category_name'     => $fee->category->name,
        ];

        return $data;
    }

    public function includeCountry(Fee $fee)
    {
        return $this->item($fee->country, new CountryTransformer());
    }

    public function includeCategory(Fee $fee)
    {
        return $this->item($fee->category, new FeeCategoryTransformer());
    }
}
