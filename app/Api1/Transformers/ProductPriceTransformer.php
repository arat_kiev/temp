<?php

namespace App\Api1\Transformers;

use App\Managers\ProductSaleManager;
use Auth;
use App\Models\Product\ProductPrice;
use League\Fractal\TransformerAbstract;

class ProductPriceTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['fees', 'discounts', 'timeslots', 'timeslot_dates'];

    protected $defaultIncludes = ['fees', 'userFees', 'discounts', 'timeslots', 'timeslot_dates'];

    public function transform(ProductPrice $price)
    {
        return [
            'id'            => $price->id,
            'value'         => $price->value,
            'total_price'   => $this->getTotalPrice($price),
            'vat'           => $price->vat,
            'visible_from'  => $price->visible_from ? $price->visible_from->toDateString() : '',
            'valid_from'    => $price->valid_from ? $price->valid_from->toDateString() : '',
            'valid_till'    => $price->valid_till ? $price->valid_till->toDateString() : '',
            'bookable'      => $price->timeslots()->exists(),
        ];
    }

    public function includeDiscounts(ProductPrice $price)
    {
        return $this->collection($price->timeDiscounts->sortBy('count'), new TimeDiscountTransformer());
    }

    public function includeTimeslots(ProductPrice $price)
    {
        return $this->collection($price->timeslots, new TimeslotTransformer());
    }

    public function includeTimeslotDates(ProductPrice $price)
    {
        if ($price->timeslotDates()->count() === 0) {
            return null;
        }

        $slots = $price->timeslotDates()->withCount('productSales')->get();

        return $this->item($slots, new TimeslotDateTransformer());
    }

    public function includeFees(ProductPrice $price)
    {
        return $this->collection($price->fees, new FeeTransformer());
    }

    public function includeUserFees(ProductPrice $price)
    {
        if (Auth::user() && Auth::user()->complete) {
            return $this->collection(
                $price->fees()->whereHas('country', function ($query) {
                    $query->whereId(Auth::user()->country->id);
                })->get(),
                new FeeTransformer()
            );
        }

        return $this->collection($price->fees, new FeeTransformer());
    }

    private function getTotalPrice(ProductPrice $price)
    {
        if (Auth::user() && Auth::user()->complete) {
            $price = $price->value_origin + (new ProductSaleManager())->fetchFeeValues($price, Auth::user());
            return number_format($price / 100.0, 2, ',', '.');
        }

        return $price->value;
    }

}
