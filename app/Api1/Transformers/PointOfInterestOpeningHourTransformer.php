<?php

namespace App\Api1\Transformers;

use App\Models\PointOfInterest\PointOfInterestOpeningHour;
use League\Fractal\TransformerAbstract;

class PointOfInterestOpeningHourTransformer extends TransformerAbstract
{

    public function transform(PointOfInterestOpeningHour $poi_oh)
    {
        $day = date('w', strtotime($poi_oh->day));
        $periods = [
            'close' => [
                'day' => (int)$day,
                'time' => $poi_oh->closes
            ],
            'open' => [
                'day' => (int)$day,
                'time' => $poi_oh->opens
            ]

        ];
        return $periods;
    }
}
