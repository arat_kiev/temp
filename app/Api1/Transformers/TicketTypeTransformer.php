<?php

namespace App\Api1\Transformers;

use App\Models\Ticket\TicketType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class TicketTypeTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['prices', 'rule', 'required_licenses', 'optional_licenses'];

    protected $defaultIncludes = ['prices', 'rule', 'required_licenses', 'optional_licenses'];

    /**
     * @param TicketType $ticketType
     * @return array
     */
    public function transform(TicketType $ticketType) : array
    {
        $data = [
            'id' => (int)$ticketType->id,
            'name' => $ticketType->name,
            'area_id' => $ticketType->area->id,
            'category' => $ticketType->category->type,
            'category_id' => $ticketType->category->id,
            'duration' => $ticketType->duration,
            'group' => (boolean)$ticketType->group,
            'day_starts_at_sunrise' => $ticketType->day_starts_at_sunrise,
            'day_starts_at_sunset' => $ticketType->day_starts_at_sunset,
            'day_starts_at_time' => $ticketType->day_starts_at_time,
            'day_ends_at_sunrise' => $ticketType->day_ends_at_sunrise,
            'day_ends_at_sunset' => $ticketType->day_ends_at_sunset,
            'day_ends_at_time' => $ticketType->day_ends_at_time,
            'checkin_id' => $ticketType->checkinUnit ? $ticketType->checkinUnit->id : null,
            'checkin_max' => $ticketType->checkin_max,
            'quota_id' => $ticketType->quotaUnit ? $ticketType->quotaUnit->id : null,
            'quota_max' => $ticketType->quota_max,
            'fish_per_day' => $ticketType->fish_per_day,
            'fishing_days' => $ticketType->fishing_days,
            'online_only' => $ticketType->onlineOnly,
            'missedHaulForTickets' => $this->fetchTicketMissedHauls($ticketType),
            'cluster_ticket' => $this->clusterTickets($ticketType),
            'additional_areas' => $this->fetchAdditionalAreas($ticketType)
        ];

        return $data;
    }

    /**
     * @param TicketType $ticketType
     * @return Collection
     */
    public function includePrices(TicketType $ticketType) : Collection
    {
        return $this->collection($ticketType->prices()->active()->get(), new TicketPriceTransformer());
    }

    /**
     * @param TicketType $ticketType
     * @return \League\Fractal\Resource\Item|null
     */
    public function includeRule(TicketType $ticketType)
    {
        return $ticketType->ruleWithFallback ? $this->item($ticketType->ruleWithFallback, new RuleTransformer()) : null;
    }

    /**
     * @param TicketType $ticketType
     * @return Collection
     */
    public function includeRequiredLicenses(TicketType $ticketType) : Collection
    {
        return $this->collection($ticketType->requiredLicenseTypes, new LicenseTypeTransformer());
    }

    /**
     * @param TicketType $ticketType
     * @return Collection
     */
    public function includeOptionalLicenses(TicketType $ticketType) : Collection
    {
        return $this->collection($ticketType->optionalLicenseTypes, new LicenseTypeTransformer());
    }

    /**
     * @param TicketType $ticketType
     * @return array
     */
    private function fetchTicketMissedHauls(TicketType $ticketType) : array
    {
        return Auth::user()
            ? Auth::user()->tickets()
                ->doesntHave('hauls')
                ->whereHas('type', function ($query) use ($ticketType) {
                    return $query->where('id', $ticketType->id);
                })
                ->where('valid_to','<', Carbon::now()->format('Y-m-d'))
                ->get()
                ->pluck('id')
                ->toArray()
            : [];
    }

    /**
     * Get additional areas param for ticket type
     *
     * @param TicketType $ticketType
     * @return bool
     */
    private function clusterTickets(TicketType $ticketType) : bool
    {
        return $ticketType->additionalAreas->count() > 0;
    }

    /**
     * Get additional areas for ticket type
     *
     * @param TicketType $ticketType
     * @return array
     */
    private function fetchAdditionalAreas(TicketType $ticketType) : array
    {
        return $ticketType->additionalAreas->toArray();
    }
}
