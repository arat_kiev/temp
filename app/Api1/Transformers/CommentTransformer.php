<?php

namespace App\Api1\Transformers;

use App\Models\Comment;
use App\Api1\Transformers\Picture\PictureTransformer;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['user', 'pictures'];

    protected $defaultIncludes = ['user', 'pictures'];

    public function transform(Comment $comment)
    {
        return [
            'id'            => $comment->id,
            'body'          => $comment->body,
            'created_at'    => $comment->created_at ? $comment->created_at->toDateTimeString() : null,
        ];
    }

    public function includeUser(Comment $comment)
    {
        return $this->item($comment->user, new UserTransformer());
    }

    public function includePictures(Comment $comment)
    {
        return $comment->pictures ? $this->collection($comment->pictures, new PictureTransformer()) : null;
    }
}
