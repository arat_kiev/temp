<?php

namespace App\Api1\Transformers;

use App\Models\RatingCategory;
use League\Fractal\TransformerAbstract;

class AreaRatingCategoryTransformer extends TransformerAbstract
{
    public function transform(RatingCategory $category)
    {
        return [
            'id' => (int)$category->id,
            'name' => $category->name,
        ];
    }
}
