<?php

namespace App\Api1\Transformers;

use App\Models\Authorization\Permission;
use League\Fractal\TransformerAbstract;

class PermissionTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['roles'];

    public function transform(Permission $permission)
    {
        $data = [
            'id' => (int)$permission->id,
            'name' => $permission->name,
            'description' => $permission->description,
            'links' => [
                'self' => route('v1.permissions.show', ['id' => $permission->id]),
            ],
        ];

        return $data;
    }

    public function includeRoles(Permission $permission)
    {
        return $this->collection($permission->roles, new RoleTransformer());
    }
}
