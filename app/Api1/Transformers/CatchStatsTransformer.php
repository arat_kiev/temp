<?php

namespace App\Api1\Transformers;

use App\Models\Haul;
use League\Fractal\TransformerAbstract;

class CatchStatsTransformer extends TransformerAbstract
{
    static private $counter = 0;

    protected $availableIncludes = ['user'];

    protected $defaultIncludes = ['user'];

    /**
     * Count hauls
     * @return int
     */
    public static function getCount()
    {
        return self::$counter;
    }

    public function transform(Haul $haul)
    {
        $data = [
            'id'            => (int) $haul->id,
            'taken'         => (bool) $haul->taken,
            'count'         => $haul->count ?: 1,
            'size_value'    => $haul->length->value,
            'size_type'     => $haul->length->type,
            'weight_value'  => $haul->weight->value,
            'weight_type'   => $haul->weight->type,
            'user_comment'  => $haul->user_comment,
            'catch_date'    => $haul->catch_date->toDateString(),
            'catch_time'    => $haul->catch_time,
            'ticket_id'     => $haul->ticket_id,

            'area'          => [
                'id'                    => $haul->area_id,
                'area_account_number'   => $haul->area ? $haul->area->account_number : null,
                'name'                  => $haul->area ? $haul->area->name : $haul->area_name,
                'manager_id'            => $haul->area ? $haul->area->manager_id : null,
            ],

            'fish'          => [
                'id'    => $haul->fish ? $haul->fish->id : null,
                'name'  => $haul->fish ? $haul->fish->name : null,
            ],
        ];

        self::$counter++;

        return $data;
    }

    public function includeUser(Haul $haul)
    {
        return $haul->user
            ? $this->item($haul->user, new UserTransformer(true, ['links', 'balance', 'active']))
            : $this->item($haul->ticket->resellerticket, new ResellerUserTransformer());
    }
}
