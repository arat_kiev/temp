<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\LogoPictureTransformer;
use App\Models\Contact\Rental;
use App\Models\Organization\Organization;
use League\Fractal\TransformerAbstract;

class RentalTransformer extends TransformerAbstract
{
    public function transform(Rental $rental)
    {
        $data = [
            'id' => (int)$rental->id,
            'name' => $rental->name,
            'street' => $rental->street,
            'area_code' => $rental->area_code,
            'city' => $rental->city,
            'country' => $rental->country ? $rental->country->name : null,
            'country_id' => $rental->country ? $rental->country->id : null,
            'country_code' => $rental->country ? $rental->country->country_code : null,
            'person' => $rental->person,
            'phone' => $rental->phone,
            'email' => $rental->email,
            'website' => $rental->website,
            'links' => [
                'self' => route('v1.rentals.show', ['id' => $rental->id]),
            ],
        ];

        return $data;
    }
}
