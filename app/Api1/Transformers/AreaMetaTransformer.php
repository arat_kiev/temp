<?php

namespace App\Api1\Transformers;

use App\Models\Area\AreaMeta;
use League\Fractal\TransformerAbstract;

class AreaMetaTransformer extends TransformerAbstract
{
    public function transform(AreaMeta $meta)
    {
        $data = [
            'type_id' => (int)$meta->id,
            'type' => $meta->type,
            'name' => $meta->name,
            'value' => $meta->pivot->value,
        ];

        return $data;
    }
}
