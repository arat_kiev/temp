<?php

namespace App\Api1\Transformers;

use App\Models\Picture;
use League\Fractal\TransformerAbstract;

class PdfTransformer extends TransformerAbstract
{
    public function transform(Picture $picture)
    {
        $data = [
            'name' => $picture->name,
            'description' => $picture->description,
            'file' => route('fileload', ['file' => $picture->filePathWithName]),
        ];

        return $data;
    }
}
