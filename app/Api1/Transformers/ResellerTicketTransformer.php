<?php

namespace App\Api1\Transformers;

use App\Models\Ticket\ResellerTicket;
use League\Fractal\TransformerAbstract;

class ResellerTicketTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['ticket'];

    protected $defaultIncludes = [];

    protected $detailed;

    public function __construct($detailed = false)
    {
        $this->detailed = $detailed;
    }

    public function transform(ResellerTicket $ticket)
    {
        $data = [
            'id' => (int)$ticket->baseTicket->id,
            'fisher_id' => $ticket->fisher_id,
            'first_name' => $ticket->first_name,
            'last_name' => $ticket->last_name,
            'street' => $ticket->street,
            'post_code' => $ticket->post_code,
            'city' => $ticket->city,
            'country' => $ticket->country ? $ticket->country->name : null,
            'country_id' => $ticket->country ? $ticket->country->id : null,
            'birthday' => $ticket->birthday->toIso8601String(),
            'email' => $ticket->email,
            'phone' => $ticket->phone,
            'authorization_id' => $ticket->authorization_id,
            'issuing_authority' => $ticket->issuing_authority,
            'licenses' => $ticket->licenses,
            'price' => $ticket->baseTicket ? $ticket->baseTicket->price : null,
            'type' => $ticket->baseTicket ? $ticket->baseTicket->type : null,
            'created_at' => $ticket->baseTicket->created_at->toIso8601String(),
            'valid_from' =>$ticket->baseTicket->valid_from->toIso8601String(),
            'valid_to' => $ticket->baseTicket->valid_to->toIso8601String(),
            'area' => $ticket->baseTicket->type->area->name,
            'links' => [
                'self' => route('v1.resellers.tickets.show', ['reseller' => $ticket->reseller->id, 'ticket' => $ticket->baseTicket->id]),
                'baseTicket' => route('v1.tickets.show', ['ticket' => $ticket->baseTicket->id]),
                'pdf' => route('v1.resellers.tickets.pdf', ['reseller' => $ticket->reseller->id, 'ticket' => $ticket->baseTicket->id]),
            ],
        ];

        return $data;
    }

    public function includeTicket(ResellerTicket $ticket)
    {
        return $ticket->baseTicket ? $this->item($ticket->baseTicket, new TicketTransformer()) : null;
    }
}
