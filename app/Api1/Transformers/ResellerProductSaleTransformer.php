<?php

namespace App\Api1\Transformers;

use App\Managers\ProductSaleManager;
use App\Models\Product\ProductSale;
use League\Fractal\TransformerAbstract;

class ResellerProductSaleTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['product', 'user', 'reseller', 'price', 'timeslot_dates'];

    protected $defaultIncludes = ['timeslot_dates'];

    protected $detailed;

    private $saleManager;

    public function __construct($detailed = false)
    {
        $this->detailed = $detailed;
        $this->saleManager = app(ProductSaleManager::class);
    }

    public function transform(ProductSale $sale)
    {
        $data = [
            'id'            => (int) $sale->id,
            'sale_number'   => $this->saleManager->getIdentifier($sale),
            'status'        => $sale->status,
            'reseller_id'   => $sale->reseller ? $sale->reseller->id : null,
            'user_id'       => $sale->user ? $sale->user->id : null,
            'email'         => $sale->user ? $sale->user->email : $sale->email,
            'first_name'    => $sale->user ? $sale->user->first_name : $sale->first_name,
            'last_name'     => $sale->user ? $sale->user->last_name : $sale->last_name,
            'street'        => $sale->user ? $sale->user->street : $sale->street,
            'post_code'     => $sale->user ? $sale->user->post_code : $sale->post_code,
            'city'          => $sale->user ? $sale->user->city : $sale->city,
            'country_id'    => $sale->user ? $sale->user->country_id : $sale->country_id,
            'country'       => $sale->user ? $sale->user->country->name : $sale->country->name,
            'created_at'    => $sale->created_at->toIso8601String(),
            'links'         => [
                'self' => $sale->reseller
                    ? route('v1.resellers.productsales.show', ['reseller' => $sale->reseller->id, 'saleId' => $sale->id])
                    : null,
                'attachment'    => $sale->product->attachment
                    ? route('imagecache', ['template' => 'download', 'filename' => $sale->product->attachment])
                    : null,
                'pdf' => $sale->reseller
                    ? route('v1.resellers.productsales.pdf', ['reseller' => $sale->reseller->id, 'product' => $sale->id])
                    : null,
            ],
        ];

        if ($this->detailed) {
            $data['security'] = [
                'qrcode' => $this->saleManager->getQrCodeData($sale),
                'vfcode' => $this->saleManager->getControlCode($sale),
            ];
        }

        return $data;
    }

    public function includeTimeslotDates(ProductSale $sale)
    {
        if ($sale->timeslotDates()->count() === 0) {
            return $this->null();
        }
        $slots = $sale->timeslotDates()->get();

        return $this->item($slots, new TimeslotDateTransformer(false));
    }

    public function includeProduct(ProductSale $sale)
    {
        return $this->item($sale->product, new ProductTransformer());
    }

    public function includeUser(ProductSale $sale)
    {
        return $sale->user ? $this->item($sale->user, new UserTransformer()) : null;
    }

    public function includeReseller(ProductSale $sale)
    {
        return $sale->reseller ? $this->item($sale->reseller, new ResellerTransformer()) : null;
    }

    public function includePrice(ProductSale $sale)
    {
        return $this->item($sale->price, new ProductPriceTransformer());
    }
}
