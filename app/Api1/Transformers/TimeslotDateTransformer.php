<?php

namespace App\Api1\Transformers;

use App\Models\Product\Timeslot;
use League\Fractal\TransformerAbstract;

class TimeslotDateTransformer extends TransformerAbstract
{
    private $withAvailable;

    public function __construct($withAvailable = true)
    {
        $this->withAvailable = $withAvailable;
    }

    public function transform($timeslots)
    {
        $data = [];

        $timeslots
            ->groupBy(function ($slot) {
                return $slot->date->format('Y-m-d');
            })
            ->each(function ($slots, $date) use (&$data) {
                foreach ($slots as $slot) {
                    $entry = [
                        'id' => $slot->id,
                        'timeslot_id' => $slot->timeslot_id,
                    ];

                    if ($this->withAvailable) {
                        $entry['available'] = $slot->available;
                    }

                    $data[$date][] = $entry;
                }
            });

        return $data;
    }
}