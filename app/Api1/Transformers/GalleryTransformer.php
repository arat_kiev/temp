<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\PictureTransformer;
use App\Models\Gallery;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\TransformerAbstract;

class GalleryTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'pictures',
    ];

    public function transform(Gallery $gallery)
    {
        $data = [
            'id' => (int)$gallery->id,
            'name' => $gallery->name,
            'description' => $gallery->description,
            'links' => [
                'self' => route('v1.galleries.show', ['id' => $gallery->id]),
            ],
        ];

        return $data;
    }

    public function includePictures(Gallery $gallery)
    {
        $paginator = $gallery->pictures()->paginate();
        $pictures = $paginator->items();

        $data = fractal()
            ->collection($pictures, new PictureTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->getResource();

        return $data;
    }
}
