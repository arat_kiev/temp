<?php
declare(strict_types=1);

namespace App\Api1\Transformers;

use App\Models\Notification;
use App\Models\User;
use Carbon\Carbon;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['profile', 'roles', 'resellers', 'organizations'];

    protected $defaultIncludes = ['profile'];

    protected $detailed;
    protected $exclude;

    public function __construct($detailed = false, $exclude = null)
    {
        $this->detailed = $detailed;
        $this->exclude = is_string($exclude)
            ? array_map('trim', explode(',', $exclude))
            : (is_array($exclude)
                ? $exclude
                : []);
    }

    public function transform(User $user) : array
    {
        $data = [
            'id' => (int)$user->id,
            'email' => $user->email,
            'name' => $user->name,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'links' => [
                'self' => route('v1.users.show', ['id' => $user->id]),
                'licenses' => route('v1.users.licenses.index', ['userId' => $user->id]),
                'tickets' => route('v1.users.tickets.index', ['userId' => $user->id]),
                'hauls' => route('v1.users.hauls.index', ['userId' => $user->id]),
                'favorite_areas' => route('v1.users.favorites.index', ['userId' => $user->id]),
                'payment' => route('v1.payment.init'),
                'friends_invitation' => '',
                'comments' => route('v1.comments.index', ['entity' => 'users', 'id' => $user->id]),
            ],
        ];

        if ($this->detailed) {
            $data['fisher_id'] = (int)$user->fisher_id;
            $data['email'] = $user->email;
            $data['newsletter'] = (bool)$user->newsletter;
            $data['street'] = $user->street;
            $data['post_code'] = $user->post_code;
            $data['city'] = with($user->city);
            $data['state'] = $user->state ? $user->state->name : null;
            $data['state_id'] = $user->state ? $user->state->id : null;
            $data['country'] = $user->country ? $user->country->name : null;
            $data['country_id'] = $user->country ? $user->country->id : null;
            $data['fisher_id'] = $user->fisher_id;
            $data['birthday'] = $user->birthday ? $user->birthday->toIso8601String() : null;
            $data['phone'] = $user->phone;
            $data['balance'] = number_format($user->balance / 100.0, 2, ',', '.');

            $data['active'] = empty($user->activation_code);
            $data['complete'] = $user->complete;
            $data['ticketsWithoutCatch'] = $this->fetchTicketsWithoutCatch($user);
            $data['unreadNotifications'] = $this->fetchUnreadNotificationsCount($user);
        }

        return array_diff_key($data, array_flip($this->exclude));
    }

    public function includeProfile(User $user) : Item
    {
        return $this->item($user, new UserProfileTransformer());
    }

    public function includeRoles(User $user) : Collection
    {
        return $this->collection($user->roles, new RoleTransformer());
    }

    public function includeResellers(User $user) : Collection
    {
        return $this->collection($user->resellers, new ResellerTransformer());
    }

    public function includeOrganizations(User $user) : Collection
    {
        return $this->collection($user->organizations, new OrganizationTransformer());
    }

    private function fetchTicketsWithoutCatch(User $user) : array
    {
        return $user->tickets()
            ->withoutStorno()
            ->requiredCatchlog(true)
            ->doesntHave('hauls')
            ->where('valid_to', '<', Carbon::now())
            ->pluck('id')
            ->toArray();
    }

    private function fetchUnreadNotificationsCount(User $user)
    {
        return Notification::where('user_id', $user->id)
            ->where('read_at', null)
            ->count();
    }
}
