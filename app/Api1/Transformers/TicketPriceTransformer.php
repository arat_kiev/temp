<?php

namespace App\Api1\Transformers;

use CommissionManager;
use App\Models\Ticket\TicketPrice;
use League\Fractal\TransformerAbstract;

class TicketPriceTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['weekdays', 'lockPeriods'];

    protected $defaultIncludes = ['weekdays', 'lockPeriods'];

    public function transform(TicketPrice $ticketPrice)
    {
        $data = [
            'id'                => $ticketPrice->id,
            'area_id'           => $ticketPrice->ticketType->area->id,
            'area'              => $ticketPrice->ticketType->area->name,
            'ticket_type_id'    => $ticketPrice->ticketType->id,
            'ticket_type'       => $ticketPrice->ticketType->name,
            'type'              => $ticketPrice->type,
            'value'             => number_format($ticketPrice->value / 100.0, 2, ',', '.'),
            'age'               => $ticketPrice->age,
            'group_count'       => $ticketPrice->ticketType->group ? $ticketPrice->group_count : null,
            'name'              => $ticketPrice->nameWithYear,
            'valid_from'        => $ticketPrice->valid_from->toDateString(),
            'valid_to'          => $ticketPrice->valid_to->toDateString(),
        ];

        return $data;
    }

    public function includeWeekdays(TicketPrice $ticketPrice)
    {
        return $this->collection($ticketPrice->weekdays, new TicketDayTransformer());
    }

    public function includeLockPeriods(TicketPrice $ticketPrice)
    {
        return $this->collection($ticketPrice->lockPeriods, new LockPeriodTransformer());
    }
}
