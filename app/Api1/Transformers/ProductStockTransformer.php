<?php

namespace App\Api1\Transformers;

use App\Models\Product\Product;
use App\Models\Product\ProductStock;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Auth;

class ProductStockTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['product', 'stock', 'notificatableUsers'];

    protected $defaultIncludes = ['stock'];

    public function transform(ProductStock $productStock)
    {
        $data = [
            'id'                    => $productStock->id,
            'quantity'              => $productStock->quantity,
            'unit'                  => $productStock->unit,
            'humanReadableUnit'     => $productStock->human_readable_unit,
            'deliveryTime'          => $productStock->delivery_time,
            'deliveryTimeInDays'    => $productStock->delivery_time_in_days,
            'notificationQuantity'  => $productStock->notification_quantity,
            'links'                 => [
                'self'  => route('v1.product_stocks.show', ['id' => $productStock->id]),
            ],
        ];

        return $data;
    }

    public function includeProduct(ProductStock $productStock)
    {
        return $this->item($productStock->product, new ProductTransformer());
    }
    
    public function includeStock(ProductStock $productStock)
    {
        return $productStock->stock ? $this->item($productStock->stock, new StockTransformer()) : null;
    }

    public function includeNotificatableUsers(ProductStock $productStock)
    {
        return $this->collection($productStock->notificatableUsers, new UserTransformer());
    }
}