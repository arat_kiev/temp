<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\HeroPictureTransformer;
use App\Api1\Transformers\Picture\PictureTransformer;
use App\Exceptions\AreaHasNoLocationsException;
use App\Managers\AreaManager;
use App\Models\Area\Area;
use App\Models\Area\AreaRating;
use App\Models\RatingCategory;
use Cache;
use DB;
use League\Fractal\TransformerAbstract;

/**
 * Class AreaTransformer
 * @package App\Api1\Transformers
 */
class AreaTransformer extends TransformerAbstract
{
    const NEAR_DISTANCE = 15;
    const NEAR_COUNT_MAX = 5;

    protected $availableIncludes = [
        'affiliates',
        'fishes',
        'techniques',
        'manager',
        'locations',
        'ticketTypes',
        'additionalTicketTypes',
        'picture',
        'heroImage',
        'poi-groups',
        'meta',
        'part_of',
        'nearbyAreas',
        'hauls',
        'lowest_price',
        'products',
        'additionalProducts',
        'additionalManagers',
        'inspectors',
    ];

    protected $defaultIncludes = [
        'picture',
        'meta',
        'part_of',
        'poi-groups',
        'lowest_price',
    ];

    protected $detailed;

    /**
     * AreaTransformer constructor.
     * @param bool $detailed
     */
    public function __construct($detailed = false)
    {
        $this->detailed = $detailed;
    }

    /**
     * @param Area $area
     * @return array
     */
    public function transform(Area $area)
    {
        $city = $area->cities()->first();

        $data = [
            'id' => (int)$area->id,
            'public' => (boolean)$area->public,
            'approved' => (boolean)$area->approved,
            'lease' => (boolean)$area->lease,
            'name' => $area->name,
            'slug' => $area->slug,
            'type' => $area->type->name,
            'tickets' => fractal()->collection($area->tickets, new TicketTransformer())->toArray(),
            'season_begin' => $area->season_begin ? $area->season_begin->toDateString() : null,
            'season_end' => $area->season_end ? $area->season_end->toDateString() : null,
            'type_id' => $area->type->id,
            'distance' => $area->distance ?? null,
            'parent_area_id' => $area->parent ? $area->parent->id : null,
            'parent_area' => $area->parent ? $area->parent->name : null,
            'city' => $city ? $city->name : null,
            'rods_max' => (int)$area->rods_max,
            'member_only' => (boolean)$area->member_only,
            'boat' => (boolean)$area->boat,
            'phone_ticket' => (boolean)$area->phone_ticket,
            'nightfishing' => (boolean)$area->nightfishing,
            'city_id' => $city ? $city->id : null,
            'region' => $city ? $city->region->name : null,
            'region_id' => $city ? $city->region->id : null,
            'state' => $city ? $city->region->state->name : null,
            'state_id' => $city ? $city->region->state->id : null,
            'country' => $city ? $city->region->state->country->name : null,
            'country_id' => $city ? $city->region->state->country->id : null,
            'country_code' => $city ? $city->region->state->country->country_code : null,
            'manager_id' => $area->manager_id,
            'manager_name' => $area->manager_name ? $area->manager_name : null,
            'additional_managers' => $area->additionalManagers()->wherePivot('public', true)->exists(),
            'fish_ids' => $area->fishes->pluck('id')->toArray(),
            'fish_names' => $area->fishes->pluck('name')->toArray(),
            'technique_ids' => $area->techniques->pluck('id')->toArray(),
            'technique_names' => $area->techniques->pluck('name')->toArray(),
            'links' => [
                'self' => route('v1.areas.show', ['id' => $area->id]),
                'comments' => route('v1.comments.index', ['entity' => 'areas', 'id' => $area->id]),
            ],
        ];

        if ($area->has('hauls')) {
            $data['links']['hauls'] = route('v1.hauls.index', ['area' => $area->id]);
        }

        if ($this->detailed) {
            if ($user = \Auth::user()) {
                $data['favorite'] = $user->favorites()->get()->contains($area->id);
            }

            $data['description'] = $area->description;
            $data['borders'] = $area->borders;
            $data['links']['gallery'] = route('v1.areas.pictures.index', ['id' => $area->id]);
            $data['map_image'] = AreaManager::getStaticMap($area);
            $data['geojson'] = $area->polyfield ? json_decode($area->polyfield) : [];
            $data['rating'] = $this->getRating($area) ?: null;
        }

        return $data;
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAffiliates(Area $area)
    {
        return $this->collection($area->affiliates, new AffiliateTransformer());
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Item|null
     */
    public function includePicture(Area $area)
    {
        return $area->picture ? $this->item($area->picture, new PictureTransformer()) : null;
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Item|null
     */
    public function includeHeroImage(Area $area)
    {
        return $area->heroImage ? $this->item($area->heroImage, new HeroPictureTransformer()) : null;
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Collection
     */
    public function includeFishes(Area $area)
    {
        return $this->collection($area->fishes, new FishTransformer(false, $area));
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTechniques(Area $area)
    {
        return $this->collection($area->techniques, new TechniqueTransformer());
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Item
     */
    public function includeManager(Area $area)
    {
        return $this->item($area->manager, new ManagerTransformer(false, $area->id));
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Collection
     */
    public function includeProducts(Area $area)
    {
        return $this->collection($area->products()->hasActivePrice()->get(), new ProductTransformer());
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Collection
     */
    public function includeLocations(Area $area)
    {
        return $this->collection($area->locations, new GeolocationTransformer());
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTicketTypes(Area $area)
    {
        return $this->collection(
            $area->ticketTypes()
                ->hasActivePrices()
                ->sortByPrice()
                ->get(),
            new TicketTypeTransformer()
        );
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAdditionalTicketTypes(Area $area)
    {
        return $this->collection(
            $area->additionalTicketTypes()
                ->hasActivePrices()
                ->sortByPrice()
                ->get(),
            new TicketTypeTransformer()
        );
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Collection|null
     */
    public function includeMeta(Area $area)
    {
        return $area->meta ? $this->collection($area->meta, new AreaMetaTransformer()) : null;
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Collection|null
     */
    public function includePartOf(Area $area)
    {
        return $area->partOf ? $this->collection($area->partOf, new LandingPageTransformer()) : null;
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Collection|null
     */
    public function includeHauls(Area $area)
    {
        return $area->publicHauls ? $this->collection($area->publicHauls, new HaulTransformer()) : null;
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Item|null
     */
    public function includeLowestPrice(Area $area)
    {
        $price = $area->ticketActivePrices()->where('type', 'DEFAULT')->orderBy('value')->first();
        $additionalPrice = $area->additionalTicketActivePrices()->where('type', 'DEFAULT')->orderBy('value')->first();

        if ($price && $additionalPrice) {
            if ($price->value > $additionalPrice->value) {
                $price = $additionalPrice;
            }
        } else {
            if ($additionalPrice) {
                $price = $additionalPrice;
            }
        }

        return $price ? $this->item($price, new TicketPriceTransformer()) : null;
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Collection|null
     */
    public function includePoiGroups(Area $area)
    {
        // Get pois and categories related to areas
        $pois = $area->poi()->with('categories')->get();
        $categoriesWithPoi = [];
        foreach ($pois as $poi) {
            foreach ($poi->categories as $poiCat) {
                $categoriesWithPoi[$poiCat->id][] = $poi;
            }
        }
        // sort and preparing for iteration
        ksort($categoriesWithPoi);
        $categoriesWithPoi = array_map(function ($k, $v) {
            return array($k => $v);
        }, array_keys($categoriesWithPoi), $categoriesWithPoi);

        return $categoriesWithPoi
            ? $this->collection($categoriesWithPoi, new PointOfInterestCategoryTransformer())
            : null;
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Collection|null
     */
    public function includeNearbyAreas(Area $area)
    {
        try {
            $nearbyAreas = $area->getNearbyAreasQuery(self::NEAR_DISTANCE)
                ->isPublic()
                ->setEagerLoads([])
                ->limit(self::NEAR_COUNT_MAX)->get();
        } catch (AreaHasNoLocationsException $e) {
            $nearbyAreas = [];
        }

        return $nearbyAreas ? $this->collection($nearbyAreas, new SimpleAreaTransformer()) : null;
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Collection|null
     */
    public function includeInspectors(Area $area)
    {
        $inspectors = $area->inspectors()->where('is_public', true)->get();

        return $inspectors ? $this->collection($inspectors, new UserTransformer()) : null;
    }

    /**
     * @param Area $area
     * @return \League\Fractal\Resource\Collection|null
     */
    public function includeAdditionalManagers(Area $area)
    {
        $additionalManagers = $area->additionalManagers()->wherePivot('public', true)->get();

        return $additionalManagers ? $this->collection($additionalManagers, new ManagerTransformer(false, $area->id)) : null;
    }

    /**
     * @param Area $area
     * @return mixed
     */
    private function getRating(Area $area)
    {
        return Cache::remember('rating_area_' . $area->id, 60, function () use ($area) {
            $rating_cats = RatingCategory::listsTranslations('name')->get()->map(function ($cat) {
                return [$cat->name => 0];
            });

            $cat_ratings = AreaRating::with('category')
                ->addSelect(['area_ratings.*', DB::raw('avg(value) as avg_value'), DB::raw('count(id) as total_ratings_count')])
                ->where('area_id', '=', $area->id)
                ->groupBy('rating_category_id')
                ->get();


            $combined_ratings = collect([
                'total' => (int)ceil($cat_ratings->avg('avg_value')),
                'rating_count' => $cat_ratings->sum('total_ratings_count'),
                'sub' => $cat_ratings->map(function ($rating) {
                    return [$rating->category->name => (int)ceil($rating->avg_value)];
                })->union($rating_cats),
            ]);

            return $combined_ratings->toArray();
        });
    }
}
