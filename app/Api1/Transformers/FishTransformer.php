<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\PictureTransformer;
use App\Models\Area\Area;
use App\Models\Location\State;
use App\Models\Meta\Fish;
use League\Fractal\TransformerAbstract;

class FishTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['picture', 'restrictions', 'haul_pictures', 'fish_category'];

    protected $defaultIncludes = ['picture', 'restrictions'];

    protected $detailed;

    protected $area;

    public function __construct($detailed = false, Area $area = null)
    {
        $this->detailed = $detailed;
        $this->area = $area;
    }

    public function transform(Fish $fish)
    {
        $data = [
            'id'        => (int) $fish->id,
            'name'      => $fish->name,
            'latin'     => $fish->latin,
            'slug'      => $fish->slug,
            'min_size'  => $fish->min_size,
            'max_size'  => $fish->max_size,
            'min_weight'  => $fish->min_weight,
            'max_weight'  => $fish->max_weight,
            'links'     => [
                'self'      => route('v1.fishes.show', ['id' => $fish->id]),
                'comments'  => route('v1.comments.index', ['entity' => 'fishes', 'id' => $fish->id]),
            ],
        ];

        if ($this->detailed) {
            $data['description'] = $fish->description;
        }

        return $data;
    }

    public function includePicture(Fish $fish)
    {
        return $fish->picture ? $this->item($fish->picture, new PictureTransformer()) : null;
    }

    public function includeFishCategory(Fish $fish)
    {
        return $fish->category ? $this->item($fish->category, new FishCategoryTransformer()) : null;
    }

    public function includeHaulPictures(Fish $fish)
    {
        $haulPictures = [];
        foreach ($fish->hauls as $haul) {
            if ($haul->picture) {
                $haul->picture->haul_id = $haul->id;
                $haulPictures[] = $haul->picture;
            }
        }

        return $haulPictures ? $this->collection($haulPictures, new HaulPictureTransformer()) : null;
    }

    public function includeRestrictions(Fish $fish)
    {
        $fields = [
            'closed_from' => null,
            'closed_till' => null,
            'min_length' => null,
            'max_length' => null,
            'source' => null,
        ];

        $useStateFallback = true;

        if ($fish->pivot) {
            foreach ($fields as $key => $val) {
                if ($fish->pivot->$key !== null) {
                    $fields[$key] = $fish->pivot->$key;
                    $useStateFallback = false;
                }
            }

            if (!$useStateFallback) {
                $fields['source'] = 'area';
            }

            if ($useStateFallback && $this->area) {
                if ($city = $this->area->cities()->first()) {
                    /** @var State $state */
                    $state = $city->region->state;

                    if ($stateFish = $state->fishes()->find($fish->id)) {
                        foreach ($fields as $key => $val) {
                            if ($stateFish->pivot->$key !== null) {
                                $fields[$key] = $stateFish->pivot->$key;
                                $fields['source'] = 'state';
                            }
                        }
                    }
                }
            }

            return $this->item($fields, new FishRestrictionTransformer());
        }

        return null;
    }
}
