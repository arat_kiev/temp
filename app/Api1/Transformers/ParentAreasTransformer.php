<?php

namespace App\Api1\Transformers;

use App\Models\Area\AreaParent;
use League\Fractal\TransformerAbstract;

class ParentAreasTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'child_areas',
    ];

    protected $defaultIncludes = [
        'child_areas',
    ];

    public function transform(AreaParent $area)
    {
        return [
            'id' => (int)$area->id,
            'name' => $area->name ?: '',
            'description' => $area->description ?: '',
        ];
    }

    public function includeChildAreas(AreaParent $area)
    {
        return $area->areas ? $this->collection($area->areas, new AreaTransformer()) : null;
    }
}
