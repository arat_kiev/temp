<?php

namespace App\Api1\Transformers;

use App\Models\Contact\Reseller;
use League\Fractal\TransformerAbstract;

class ResellerTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'areas'
    ];

    public function transform(Reseller $reseller)
    {
        $data = [
            'id'        => (int) $reseller->id,
            'name'      => $reseller->name,
            'street'    => $reseller->street,
            'area_code' => $reseller->area_code,
            'city'      => $reseller->city,
            'person'    => $reseller->person,
            'phone'     => $reseller->phone,
            'email'     => $reseller->email,
            'website'   => $reseller->website,
            'slug'      => $reseller->slug,
            'links'     => [
                'self'      => route('v1.resellers.show', ['id' => $reseller->id]),
            ],
        ];

        return $data;
    }

    public function includeAreas(Reseller $reseller)
    {
        return $this->collection($reseller->areas, new AreaTransformer());
    }
}
