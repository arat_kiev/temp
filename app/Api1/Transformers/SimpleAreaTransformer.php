<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\PictureTransformer;
use App\Models\Area\Area;
use League\Fractal\TransformerAbstract;

class SimpleAreaTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'picture',
    ];

    protected $defaultIncludes = [
        'picture',
    ];

    public function transform(Area $area)
    {
        return [
            'id' => (int)$area->id,
            'name' => $area->name,
            'slug' => $area->slug,
            'links' => [
                'self' => route('v1.areas.show', ['id' => $area->id]),
            ],
        ];
    }

    public function includePicture(Area $area)
    {
        return $area->picture ? $this->item($area->picture, new PictureTransformer()) : null;
    }
}
