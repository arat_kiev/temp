<?php

namespace App\Api1\Transformers;

use App\Models\Location\Geolocation;
use League\Fractal\TransformerAbstract;

class GeolocationTransformer extends TransformerAbstract
{
    public function transform(Geolocation $location)
    {
        $data = [
            'longitude' => $location->longitude,
            'latitude' => $location->latitude,
            'name' => $location->name,
        ];

        return $data;
    }
}
