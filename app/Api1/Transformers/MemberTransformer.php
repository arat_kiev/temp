<?php

namespace App\Api1\Transformers;

use App\Models\Organization\Member;
use League\Fractal\TransformerAbstract;

class MemberTransformer extends TransformerAbstract
{

    public function transform(Member $member)
    {
        $data = [
            'id' => (int)$member->id,
            'fisher_id' => $member->fisher_id,
            'first_name' => $member->first_name,
            'last_name' => $member->last_name,
            'street' => $member->street,
            'post_code' => $member->post_code,
            'city' => $member->city,
            'country' => $member->country ? $member->country->name : null,
            'country_id' => $member->country ? $member->country->id : null,
            'birthday' => $member->birthday ? $member->birthday->toIso8601String() : null,
            'email' => $member->email,
            'phone' => $member->phone,
        ];

        return $data;
    }
}
