<?php

namespace App\Api1\Transformers;

use App\Models\Affiliate;
use League\Fractal\TransformerAbstract;

class AffiliateTransformer extends TransformerAbstract
{
    public function transform(Affiliate $affiliate)
    {
        $data = [
            'id'            => $affiliate->id,
            'name'          => $affiliate->name,
            'description'   => $affiliate->description,
            'price'         =>  number_format($affiliate->price / 100.0, 2, ",", ".").' €',
            'link'          => $affiliate->link,
            'image'         => $affiliate->image_link
        ];

        return $data;
    }
}