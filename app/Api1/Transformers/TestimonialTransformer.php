<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\PictureTransformer;
use App\Models\Testimonial;
use League\Fractal\TransformerAbstract;

class TestimonialTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['manager', 'picture'];

    protected $defaultIncludes = ['manager', 'picture'];

    public function transform(Testimonial $testimonial)
    {
        $data = [
            'id' => (int)$testimonial->id,
            'person_name' => $testimonial->person_name,
            'person_description' => $testimonial->person_description,
            'text' => $testimonial->text,
            'position' => $testimonial->position,
            'links' => [
                'self' => route('v1.testimonials.show', ['id' => $testimonial->id]),
            ],
        ];

        return $data;
    }

    public function includePicture(Testimonial $testimonial)
    {
        return $testimonial->picture ? $this->item($testimonial->picture, new PictureTransformer('sq')) : null;
    }

    public function includeManager(Testimonial $testimonial)
    {
        return $testimonial->manager ? $this->item($testimonial->manager, new ManagerTransformer()) : null;
    }
}
