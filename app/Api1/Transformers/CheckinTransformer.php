<?php

namespace App\Api1\Transformers;

use App\Models\Ticket\Checkin;
use League\Fractal\TransformerAbstract;

class CheckinTransformer extends TransformerAbstract
{
    public function transform(Checkin $checkin)
    {
        return [
            'id' => $checkin->id,
            'ticket_id' => $checkin->ticket->id,
            'from' => $checkin->from ? $checkin->from->toIso8601String() : '',
            'till' => $checkin->till ? $checkin->till->toIso8601String() : '',
            'method' => $checkin->fishingMethods->first() ? [
                'id' => $checkin->fishingMethods->first()->id,
                'name' => $checkin->fishingMethods->first()->translate(app()->getLocale(), true)->name,
            ] : null
        ];
    }
}
