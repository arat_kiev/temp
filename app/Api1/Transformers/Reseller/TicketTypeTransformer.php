<?php

namespace App\Api1\Transformers\Reseller;

use App\Api1\Transformers\LicenseTypeTransformer;
use App\Models\Ticket\TicketType;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class TicketTypeTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['required_licenses', 'optional_licenses'];

    protected $defaultIncludes = ['required_licenses', 'optional_licenses'];

    public function transform(TicketType $ticketType) : array
    {
        $data = [
            'id' => (int)$ticketType->id,
            'name' => $ticketType->name,
            'area_id' => $ticketType->area->id,
            'prices_count' => $ticketType->ticket_prices_count,
            'category' => $ticketType->category->type,
            'category_id' => $ticketType->category->id,
            'duration' => $ticketType->duration,
            'duration_category' => $ticketType->duration_category,
            'group' => (boolean)$ticketType->group,
            'day_starts_at_sunrise' => $ticketType->day_starts_at_sunrise,
            'day_starts_at_sunset' => $ticketType->day_starts_at_sunset,
            'day_starts_at_time' => $ticketType->day_starts_at_time,
            'day_ends_at_sunrise' => $ticketType->day_ends_at_sunrise,
            'day_ends_at_sunset' => $ticketType->day_ends_at_sunset,
            'day_ends_at_time' => $ticketType->day_ends_at_time,
            'checkin_id' => $ticketType->checkinUnit ? $ticketType->checkinUnit->id : null,
            'checkin_max' => $ticketType->checkin_max,
            'quota_id' => $ticketType->quotaUnit ? $ticketType->quotaUnit->id : null,
            'quota_max' => $ticketType->quota_max,
            'fish_per_day' => $ticketType->fish_per_day,
            'fishing_days' => $ticketType->fishing_days,
            'online_only' => $ticketType->onlineOnly,
        ];

        return $data;
    }

    public function includeRequiredLicenses(TicketType $ticketType) : Collection
    {
        return $this->collection($ticketType->requiredLicenseTypes, new LicenseTypeTransformer());
    }

    public function includeOptionalLicenses(TicketType $ticketType) : Collection
    {
        return $this->collection($ticketType->optionalLicenseTypes, new LicenseTypeTransformer());
    }
}
