<?php

namespace App\Api1\Transformers\Reseller;

use App\Models\Area\Area;
use League\Fractal\TransformerAbstract;

class AreaTransformer extends TransformerAbstract
{
    public function transform(Area $area)
    {
        return [
            'id' => (int)$area->id,
            'name' => $area->name,
            'manager' => $area->manager,
        ];
    }
}
