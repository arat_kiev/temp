<?php

namespace App\Api1\Transformers;

use App\Models\Ticket\CheckinUnit;
use League\Fractal\TransformerAbstract;

class CheckinUnitTransformer extends TransformerAbstract
{
    public function transform(CheckinUnit $checkinUnit)
    {
        $data = [
            'id' => (int)$checkinUnit->id,
            'type' => $checkinUnit->type,
            'name' => $checkinUnit->name,
        ];

        return $data;
    }
}
