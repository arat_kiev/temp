<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\PictureTransformer;
use App\Models\Product\ProductCategory;
use League\Fractal\TransformerAbstract;

class ProductCategoryTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['products', 'picture'];

    protected $defaultIncludes = ['picture'];

    protected $detailed;
    protected $productsPage;
    
    const PRODUCTS_PER_PAGE = 5;

    public function __construct($detailed = false, int $productsPage = 1)
    {
        $this->detailed = $detailed;
        $this->productsPage = $productsPage;
    }

    public function transform(ProductCategory $category)
    {
        $data = [
            'id'    => $category->id,
            'name'  => $category->name,
            'slug' => $category->slug,
            'links'         => [
                'self'          => route('v1.product_categories.show', ['id' => $category->id]),
            ],
        ];

        if ($this->detailed) {
            $data['short_description'] = $category->short_description;
            $data['long_description'] = $category->long_description;
        }

        return $data;
    }

    public function includeProducts(ProductCategory $category)
    {
        $products = $category->products()
            ->skip(self::PRODUCTS_PER_PAGE * ($this->productsPage - 1))
            ->take(self::PRODUCTS_PER_PAGE)
            ->get();

        return $this->collection($products, new ProductTransformer());
    }

    public function includePicture(ProductCategory $category)
    {
        return $category->picture ? $this->item($category->picture, new PictureTransformer()) : null;
    }
}