<?php

namespace App\Api1\Transformers;

use App\Models\PointOfInterest\PointOfInterestCategory;
use League\Fractal\Manager;
use League\Fractal\Serializer\ArraySerializer;
use League\Fractal\TransformerAbstract;

class PointOfInterestCategoryTransformer extends TransformerAbstract
{

    public function transform(array $category_with_poi)
    {
        $manager = (new Manager())->setSerializer(new ArraySerializer());
        $category = PointOfInterestCategory::find(key($category_with_poi));
        foreach (current($category_with_poi) as $poi) {
            $pois[] = $manager->createData($this->item($poi, new PointOfInterestTransformer()))->toArray();
        }
        return [
            'id' => $category->id,
            'name' => $category->name,
            'points' => $pois
        ];
    }
}
