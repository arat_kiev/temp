<?php

namespace App\Api1\Transformers;

use App\Api1\Transformers\Picture\AvatarPictureTransformer;
use App\Api1\Transformers\Picture\HeroPictureTransformer;
use App\Managers\UserManager;
use App\Models\Location\State;
use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserProfileTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'fishes',
        'techniques',
        'avatar',
        'hero-image',
        'state',
    ];

    protected $defaultIncludes = [
        'fishes',
        'techniques',
        'avatar',
        'hero-image',
        'state',
    ];

    public function transform(User $user)
    {
        $data = [
            'id' => (int)$user->id,
            'name' => $user->name_public ? $user->name : null,
            'about' => $user->about_me,
            'links' => [
                'hauls' => route('v1.users.profile.hauls', ['user_id' => $user->id]),
                'favorite_areas' => route('v1.users.profile.favorites', ['user_id' => $user->id]),
                'followers' => route('v1.users.profile.followers', ['user_id' => $user->id]),
            ],
        ];

        return $data;
    }

    public function includeFishes(User $user)
    {
        return $this->collection($user->fishes, new FishTransformer());
    }

    public function includeTechniques(User $user)
    {
        return $this->collection($user->techniques, new TechniqueTransformer());
    }

    public function includeAvatar(User $user)
    {
        return $user->avatarPicture ? $this->item($user->avatarPicture, new AvatarPictureTransformer('sq')) : null;
    }

    public function includeHeroImage(User $user)
    {
        return $user->heroPicture ? $this->item($user->heroPicture, new HeroPictureTransformer()) : null;
    }

    public function includeState(User $user)
    {
        $state = $user->state ?: (new UserManager())->fetchState($user);

        return $state ? $this->item($state, new StateTransformer()) : null;
    }
}
