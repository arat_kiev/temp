<?php

namespace App\Api1\Transformers;

use App\Models\Area\AreaType;
use League\Fractal\TransformerAbstract;

class AreaTypeTransformer extends TransformerAbstract
{
    public function transform(AreaType $type)
    {
        $data = [
            'id' => (int)$type->id,
            'name' => $type->name,
            'description' => $type->description,
            'links' => [
                'self' => route('v1.areatypes.index', ['areaType' => $type->id]),
            ],
        ];

        return $data;
    }
}
