<?php

namespace App\Api1\Sitemaps;

use App\Models\Area\AreaParent;
use League\Fractal\TransformerAbstract;

class GroupAreaSitemap implements SitemapInterface
{
    public function getQuery($options)
    {
        return AreaParent::query();
    }

    public function getTransformer()
    {
        return new class extends TransformerAbstract {
            public function transform(AreaParent $areaParent) {
                return [
                    'id' => $areaParent->id,
                    'slug' => $areaParent->slug,
                ];
            }
        };
    }
}