<?php

namespace App\Api1\Sitemaps;

use App\Models\Location\State;
use League\Fractal\TransformerAbstract;

class StateSitemap implements SitemapInterface
{
    public function getQuery($options)
    {
        $query = State::query();

        if (isset($options['country'])) {
            $query->where('country_id', '=', $options['country']);
        }

        return $query;
    }

    public function getTransformer()
    {
        return new class extends TransformerAbstract {
            public function transform(State $state) {
                return [
                    'id' => $state->id,
                    'slug' => $state->slug,
                    'links' => [
                        'country' => route('v1.sitemaps.show', [
                            'type' => 'landing-pages',
                            'subType' => 'countries',
                        ]),
                        'regions' => route('v1.sitemaps.show', [
                            'type' => 'landing-pages',
                            'subType' => 'regions',
                            'state' => $state->id,
                        ]),
                    ],
                ];
            }
        };
    }
}