<?php

namespace App\Api1\Sitemaps;

use App\Models\Location\Country;
use League\Fractal\TransformerAbstract;

class CountrySitemap implements SitemapInterface
{
    public function getQuery($options = [])
    {
        return Country::query()
            ->select('countries.*')
            ->join('states', 'states.country_id', '=', 'countries.id')
            ->join('regions', 'regions.state_id', '=', 'states.id')
            ->join('cities', 'cities.region_id', '=', 'regions.id')
            ->join('area_cities', 'area_cities.city_id', '=', 'cities.id')
            ->join('areas', 'areas.id', '=', 'area_cities.area_id')
            ->groupBy('countries.id');
    }

    public function getTransformer()
    {
        return new class extends TransformerAbstract {
            public function transform(Country $country) {
                return [
                    'id' => $country->id,
                    'slug' => $country->slug,
                    'links' => [
                        'states' => route('v1.sitemaps.show', [
                            'type' => 'landing-pages',
                            'subType' => 'states',
                            'country' => $country->id,
                        ]),
                    ],
                ];
            }
        };
    }
}