<?php

namespace App\Api1\Sitemaps;

use App\Models\Location\Region;
use League\Fractal\TransformerAbstract;

class RegionSitemap implements SitemapInterface
{
    public function getQuery($options)
    {
        $query = Region::query();

        if (isset($options['state'])) {
            $query->where('state_id', '=', $options['state']);
        }

        return $query;
    }

    public function getTransformer()
    {
        return new class extends TransformerAbstract {
            public function transform(Region $region) {
                return [
                    'id' => $region->id,
                    'slug' => $region->slug,
                    'links' => [
                        'state' => route('v1.sitemaps.show', [
                            'type' => 'landing-pages',
                            'subType' => 'states',
                        ]),
                    ],
                ];
            }
        };
    }
}