<?php

namespace App\Api1\Sitemaps;

interface SitemapInterface
{
    public function getQuery($options);
    public function getTransformer();
}