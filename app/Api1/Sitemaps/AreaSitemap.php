<?php

namespace App\Api1\Sitemaps;

use App\Models\Area\Area;
use League\Fractal\TransformerAbstract;

class AreaSitemap implements SitemapInterface
{
    public function getQuery($options)
    {
        return Area::isPublic();
    }

    public function getTransformer()
    {
        return new class extends TransformerAbstract {
            public function transform(Area $area) {
                return [
                    'id' => $area->id,
                    'slug' => $area->slug,
                ];
            }
        };
    }
}