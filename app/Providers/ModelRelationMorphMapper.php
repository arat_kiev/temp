<?php

namespace App\Providers;

use App\Models\Area\Area;
use App\Models\Client;
use App\Models\Contact\Manager;
use App\Models\Contact\Rental;
use App\Models\Contact\Reseller;
use App\Models\Haul;
use App\Models\Location\City;
use App\Models\Location\Country;
use App\Models\Location\State;
use App\Models\Meta\Fish;
use App\Models\Meta\Technique;
use App\Models\PointOfInterest\PointOfInterest;
use App\Models\Product\Product;
use App\Models\Ticket\TicketType;
use App\Models\Product\ProductPrice;
use App\Models\Ticket\TicketPrice;
use App\Models\User;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class ModelRelationMorphMapper extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'areas'                 => Area::class,
            'cities'                => City::class,
            'countries'             => Country::class,
            'states'                => State::class,
            'hauls'                 => Haul::class,
            'techniques'            => Technique::class,
            'fishes'                => Fish::class,
            'users'                 => User::class,
            'points_of_interest'    => PointOfInterest::class,
            'managers'              => Manager::class,
            'ticket_types'          => TicketType::class,
            'products'              => Product::class,
            'resellers'             => Reseller::class,
            'clients'               => Client::class,
            'ticket_prices'         => TicketPrice::class,
            'product_prices'        => ProductPrice::class,
            'rentals'               => Rental::class,
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
