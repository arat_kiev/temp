<?php

namespace App\Providers;

use Cocur\Slugify\Slugify;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class SlugifyServiceProvider extends LaravelServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('slugify', function () {
            return new Slugify();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return string[]
     */
    public function provides()
    {
        return ['slugify'];
    }
}
