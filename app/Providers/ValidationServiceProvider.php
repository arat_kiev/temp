<?php

namespace App\Providers;

use Intervention\Image\ImageManagerStatic;
use Validator;
use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('pdf_version', function($attr, $value, $params, $validator) {
            $sign = $params[0] ?? null;
            $version = $params[1] ?? null;

            if (!preg_match('/^\d{1,3}\.\d{1,3}$/', $version)) {
                return false;
            }

            $pdfVersion = (float) $this->getPdfVersionFromStreamFile($value);
            $compareVersion = (float) $version;
            switch ($sign) {
                case '<':
                    return $pdfVersion < $compareVersion;
                case '<=':
                    return $pdfVersion <= $compareVersion;
                case '>':
                    return $pdfVersion > $compareVersion;
                case '>=':
                    return $pdfVersion >= $compareVersion;
                case '=':
                    return $pdfVersion == $compareVersion;
                case '!=':
                case '<>':
                    return $pdfVersion != $compareVersion;
                default:
                    return false;
            }
        });

        Validator::extend('image_base64', function($attr, $value, $params, $validator) {
             try {
                 $image = ImageManagerStatic::make($value);
                 return in_array($image->mime(), ['image/png', 'image/jpeg', 'image/jpg']);
             } catch (\Exception $e) {
                 return false;
             }
        });

        Validator::extend('present_if', 'App\\Managers\\PresentValidationManager@validatePresentIf');
        Validator::extendImplicit('present_if', 'App\\Managers\\PresentValidationManager@validatePresentIf');
        Validator::extend('present_unless', 'App\\Managers\\PresentValidationManager@validatePresentUnless');
        Validator::extendImplicit('present_unless', 'App\\Managers\\PresentValidationManager@validatePresentUnless');
        Validator::extend('present_with', 'App\\Managers\\PresentValidationManager@validatePresentWith');
        Validator::extendImplicit('present_with', 'App\\Managers\\PresentValidationManager@validatePresentWith');
        Validator::extend('present_with_all', 'App\\Managers\\PresentValidationManager@validatePresentWithAll');
        Validator::extendImplicit('present_with_all', 'App\\Managers\\PresentValidationManager@validatePresentWithAll');
        Validator::extend('present_without', 'App\\Managers\\PresentValidationManager@validatePresentWithOut');
        Validator::extendImplicit('present_without', 'App\\Managers\\PresentValidationManager@validatePresentWithOut');
        Validator::extend('present_without_all', 'App\\Managers\\PresentValidationManager@validatePresentWithOut');
        Validator::extendImplicit('present_without_all', 'App\\Managers\\PresentValidationManager@validatePresentWithOut');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function getPdfVersionFromStreamFile($file)
    {
        if ($fp = @fopen($file->getPathname(), 'rb')) {
            preg_match('/PDF-(\d+\.\d+)/', fread($fp, 10), $matches);
            fclose($fp);
            return isset($matches[1]) ? $matches[1] : 0;
        }

        return 0;
    }
}
