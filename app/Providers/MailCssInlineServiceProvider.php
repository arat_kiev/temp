<?php

namespace App\Providers;

use App\Plugins\CssInlinerPlugin;
use Illuminate\Support\ServiceProvider;

class MailCssInlineServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['mailer']->getSwiftMailer()->registerPlugin(new CssInlinerPlugin());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
