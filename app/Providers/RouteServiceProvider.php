<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
         parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        Route::group(['namespace' => $this->namespace], function () {
            $this->mapDefaultRoutes();
            $this->mapApiRoutes();
            $this->mapAdminRoutes();
            $this->mapPosRoutes();

            if (app()->environment() === 'local') {
                $this->mapDevRoutes();
            }
        });
    }

    protected function mapAdminRoutes()
    {
        Route::group(['domain' => config('app.sub_domains.admin').'.'.config('app.domain')], function () {
            require(base_path('routes/admin.php'));
        });
    }

    protected function mapApiRoutes()
    {
        Route::group(['domain' => config('app.sub_domains.api').'.'.config('app.domain')], function () {
            require(base_path('routes/api1.php'));
        });
    }

    protected function mapPosRoutes()
    {
        $subdomains = [
            config('app.sub_domains.pos'), 'rv', 'edersee', 'terminal', 'demo',
        ];

        Route::pattern('pos', sprintf('(%s)', join('|', $subdomains)));
        Route::group(['domain' => '{pos}.'.config('app.domain')], function () {
            require(base_path('routes/pos.php'));
        });
    }

    protected function mapDevRoutes()
    {
        Route::group([
            'domain' =>config('app.sub_domains.stats').'.'.config('app.domain')
        ], function () {
            require base_path('routes/dev.php');
        });
    }

    protected function mapDefaultRoutes()
    {
        Route::group([], function () {
            require base_path('routes/default.php');
        });
    }
}
