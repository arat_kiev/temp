<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Cache;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*', function ($view) {
            if ($this->app->environment() === 'local') {
                $assetVersion = crc32(str_random(32));
            } else {
                if (Cache::has('asset_version')) {
                    $assetVersion = Cache::get('asset_version');
                } else {
                    $assetVersion = crc32(str_random(32));
                    Cache::forever('asset_version', $assetVersion);
                }
            }

            $view->with('av', $assetVersion);
        });
    }

    /**
     * Register the service provider
     *
     * @return void
     */
    public function register()
    {

    }
}