<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \App\Events\UserRegistered::class => [
            \App\Listeners\WelcomeMessage::class,
        ],
        \App\Events\TicketCreated::class => [
            \App\Listeners\TicketMessage::class,
        ],
        \App\Events\TicketStorned::class => [
            \App\Listeners\TicketStornoMessage::class,
        ],
        \App\Events\TicketPriceCreated::class => [
            \App\Listeners\AssignDefaultCommissionsToTicketPrice::class,
        ],
        \App\Events\ProductSaleStorned::class => [
            \App\Listeners\ProductSaleStornedMessage::class,
        ],
        \App\Events\ProductSaleStatusChanged::class => [
            \App\Listeners\ProductSaleStatusChangedMessage::class,
        ],
        \App\Events\ProductSaleCreated::class => [
            \App\Listeners\ProductSaleCreatedMessage::class,
        ],
        \App\Events\ProductEndsInStock::class => [
            \App\Listeners\ProductEndsMessage::class,
        ],
        \App\Events\AreaChanged::class => [
            \App\Listeners\AreaTags::class,
            \App\Listeners\AssignCities::class,
        ],
        \App\Events\LicenseChanged::class => [
            \App\Listeners\LicenseMessage::class,
        ],
        \App\Events\PasswordReset::class => [
            \App\Listeners\PasswordMessage::class,
        ],
        \App\Events\AreaCreated::class => [
            \App\Listeners\AreaMessage::class,
        ],
        \App\Events\UserActivated::class => [
            \App\Listeners\ConnectUser::class,
        ],
        \App\Events\MemberChanged::class => [
            \App\Listeners\ConnectMember::class,
        ],
        \App\Events\CouponUsed::class => [
            \App\Listeners\CouponUsedMessage::class,
        ],
        \App\Events\UserChangeEmailEvent::class => [
            \App\Listeners\UserChangeEmailListener::class,
        ],
    ];

    protected $subscribe = [
        \App\Listeners\JobEventListener::class,
    ];

    /**
     * Register any other events for your application.
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen('tymon.jwt.expired', function () {
            return response()->json(['error' => 'Token expired'], 401);
        });

        Event::listen('tymon.jwt.invalid', function () {
            return response()->json(['error' => 'Token invalid'], 401);
        });

        Event::listen('tymon.jwt.absent', function () {
            return response()->json(['error' => 'Token required'], 401);
        });
    }
}
