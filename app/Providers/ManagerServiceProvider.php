<?php

namespace App\Providers;

use App\Managers\AreaManager;
use App\Managers\CommissionManager;
use App\Managers\DiscountManager;
use App\Managers\TicketManager;
use App\Managers\UserManager;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ManagerServiceProvider extends LaravelServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(AreaManager::class, function () {
            return new AreaManager();
        });

        $this->app->singleton(TicketManager::class, function () {
            return new TicketManager();
        });

        $this->app->singleton(CommissionManager::class, function () {
            return new CommissionManager();
        });

        $this->app->singleton(UserManager::class, function () {
            return new UserManager();
        });

        $this->app->singleton(DiscountManager::class, function () {
            return new DiscountManager();
        });
    }
}
