<?php

namespace App\Providers;

use App\Managers\CouponManager;
use App\Managers\CommissionManager;
use App\Managers\ZipManager;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\ServiceProvider;
use View;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('zipManager', ZipManager::class);
        $this->app->bind('couponManager', CouponManager::class);
        $this->app->bind('commissionManager', CommissionManager::class);

        // Share current Blade template name
        View::composer('*', function ($view) {
            View::share('currentTemplateName', $view->getName());
        });

        Validator::extend('alpha_spaces', function ($attribute, $value) {
            return preg_match('/^[\pL\s]+$/u', $value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Only activate these service providers in development
        if ($this->app->environment() === 'local') {
            $this->app->register(IdeHelperServiceProvider::class);
        }

        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
