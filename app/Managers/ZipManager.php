<?php

namespace App\Managers;

use Illuminate\Http\UploadedFile;
use ZipArchive;
use File as Filesystem;
use Exception;

class ZipManager
{
    const DEFAULT_ATTACHMENT_EXTENSION = 'zip';
    
    public function zipFiles(array $files, $fileName)
    {
        $filePath = implode(DIRECTORY_SEPARATOR, [
            config('filesystems.disks.attachment_uploads.root'),
            $this->storagePath($fileName),
        ]);
        $fileFullPath = implode(DIRECTORY_SEPARATOR, [$filePath, $fileName]);

        $this->createFolderInStorage($filePath);

        $zip = new ZipArchive();
        $this->createZipArchive($zip, $fileFullPath);
        $filesAdded = $this->addFilesToZip($files, $zip);
        $zip->close();

        return $filesAdded;
    }

    private function createFolderInStorage(string $filePath)
    {
        if (!Filesystem::exists($filePath)) {
            Filesystem::makeDirectory($filePath, 0775, true);
        }
    }

    private function createZipArchive(ZipArchive $zip, string $fileFullPath)
    {
        if ($zip->open($fileFullPath, ZipArchive::CREATE) !== true) {
            throw new Exception("Unable to create file $fileFullPath");
        }
    }

    private function addFilesToZip(array $files, ZipArchive $zip)
    {
        $filesAdded = 0;

        foreach ($files as $file) {
            if ($file instanceof UploadedFile) {
                if ($zip->addFile($file->path(), $file->getClientOriginalName()) !== true) {
                    throw new Exception("Error archiving file $file->getClientOriginalName()");
                }
                $filesAdded++;
            }
        }

        return $filesAdded;
    }

    /**
     * Get random filename for generated file
     *
     * @return string
     */
    public function storageName()
    {
        return sha1(uniqid(mt_rand(), true)) . '.' . self::DEFAULT_ATTACHMENT_EXTENSION;
    }

    /**
     * Get the absolute path for the specified filename
     *
     * @param string $fileName
     * @return string
     */
    public function storagePath($fileName)
    {
        return implode(DIRECTORY_SEPARATOR, [
            substr($fileName, 0, 2),
            substr($fileName, 2, 2),
        ]);
    }

    public function getFileFullPath($fileName)
    {
        return implode(DIRECTORY_SEPARATOR, [
            config('filesystems.disks.attachment_uploads.root'),
            $fileName,
        ]);
    }
}
