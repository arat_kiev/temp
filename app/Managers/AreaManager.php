<?php

namespace App\Managers;

use App\Models\Area\Area;
use App\Models\Area\AreaTag;
use App\Models\Location\City;
use App\Models\Location\Geolocation;
use Illuminate\Support\Collection;
use Slugify;

class AreaManager
{
    const MIN_SEARCH_RADIUS = 5.0;
    const MAX_SEARCH_RADIUS = 100.0;

    private static $citiesFound;

    public static function generateSearchTags(Area $area, $customTags, $clearCustomTags = true)
    {
        $tags = [];

        // Tag area name
        $tags = array_add($tags, $area->slug, AreaTag::SUPER);
        foreach (explode('-', $area->slug) as $token) {
            if (strlen($token) > 2) {
                $tags = array_add($tags, $token, AreaTag::SUPER);
            }
        }

        $tags = array_add($tags, $area->manager->slug, AreaTag::SUPER);
        foreach (explode('-', $area->manager->slug) as $token) {
            if (strlen($token) > 2) {
                $tags = array_add($tags, $token, AreaTag::SUPER);
            }
        }

        // Tag location
        if ($city = $area->cities()->first()) {
            $tags = array_add($tags, $city->post_code, AreaTag::SUPER);
            
            foreach (config('app.locales') as $locale) {
                foreach (explode('-', $city->getSlug($locale)) as $token) {
                    if (strlen($token) > 2) {
                        $tags = array_add($tags, $token, AreaTag::SUPER);
                    }
                }
            }

            foreach (config('app.locales') as $locale) {
                foreach (explode('-', $city->region->getSlug($locale)) as $token) {
                    if (strlen($token) > 2) {
                        $tags = array_add($tags, $token, AreaTag::HEAVY);
                    }
                }
            }

            foreach (config('app.locales') as $locale) {
                foreach (explode('-', $city->region->state->getSlug($locale)) as $token) {
                    if (strlen($token) > 2) {
                        $tags = array_add($tags, $token, AreaTag::MIDDLE);
                    }
                }
            }

            foreach (config('app.locales') as $locale) {
                foreach (explode('-', $city->region->state->country->getSlug($locale)) as $token) {
                    if (strlen($token) > 2) {
                        $tags = array_add($tags, $token, AreaTag::LIGHT);
                    }
                }
            }
        }

        // Tag area target fishes
        foreach ($area->fishes as $fish) {
            foreach (config('app.locales') as $locale) {
                foreach (explode('-', $fish->getSlug($locale)) as $token) {
                    if (strlen($token) > 2) {
                        $tags = array_add($tags, $token, AreaTag::FEATHER);
                    }
                }
            }
        }

        // Tag area catch techniques
        foreach ($area->techniques as $technique) {
            foreach (config('app.locales') as $locale) {
                foreach (explode('-', $technique->getSlug($locale)) as $token) {
                    if (strlen($token) > 2) {
                        $tags = array_add($tags, $token, AreaTag::FEATHER);
                    }
                }
            }
        }
        
        $customTags = collect($customTags);

        $arr = collect(array_where($tags, function ($key, $value) {
            return $key !== null && $key !== '';
        }));

        if ($clearCustomTags) {
            $area->tags()->delete();
        } else {
            $area->tags()->where('custom', false)->delete();
        }

        $arr->each(function ($item, $key) use ($area) {
            $area->tags()->create([
                'tag' => $key,
                'weight' => $item,
            ]);
        });

        $customTags->each(function ($item) use ($area) {
            $area->tags()->create([
                'tag' => Slugify::slugify($item),
                'weight' => AreaTag::SUPER,
                'custom' => true,
            ]);
        });
    }
    
    public static function assignCities(Area $area, $primCityId)
    {
        info('job: assignCities', ['area' => $area->id]);

        $searchRadius = 15.0;

        $locations = $area->locations;

        if ($locations->isEmpty()) {
            $area->cities()->detach();
            return false;
        }

        self::$citiesFound = new Collection();

        do {
            $searchRadius += self::MIN_SEARCH_RADIUS;

            $locations->each(function ($location) use ($searchRadius) {
                self::getCitiesInRadius($location, $searchRadius);
            });
        } while (self::$citiesFound->isEmpty() && $searchRadius < self::MAX_SEARCH_RADIUS);

        info('job: assignCities', ['area' => $area->id, 'search_radius' => $searchRadius]);

        $area->cities()->detach();
        $area->cities()->attach(self::$citiesFound->pluck('id')->toArray());
        $area->cities()->updateExistingPivot($primCityId, ['primary' => true]);

        return true;
    }

    private static function getCitiesInRadius(Geolocation $location, $radius)
    {
        list($south, $west, $north, $east) = $location->getBoundingBox($radius);
        
        $cities = City::inBoundingBox($north, $east, $south, $west)->get();

        self::$citiesFound = self::$citiesFound->merge($cities);
    }

    public static function getStaticMap(Area $area)
    {
        if ($area->locations()->count() === 0) {
            return null;
        }

        /** @var GeoLocation $location */
        $location = $area->locations()->first();
        $locString = $location->lonLatString;

        $accessToken = config('services.mapbox.access_token');
        $basePath = config('services.mapbox.base_path');
        $imagePath = $basePath.'/'.$locString.',12/400x250.png?access_token='.$accessToken;

        return $imagePath;
    }
}
