<?php

namespace App\Managers;

use App\Models\Ticket\Invoice;
use QrCode;

class InvoiceManager
{
    public function getInvoiceNumber(Invoice $invoice)
    {
        return sprintf(Invoice::IDENT_FORMAT, $invoice->created_at->format('Y'), $invoice->invoice_id);
    }

    public function getTotalAmount(Invoice $invoice)
    {
        $totalPrice = 0;

        foreach ($invoice->tickets as $ticket) {
            $totalPrice += $ticket->price->value;
        }

        return number_format($totalPrice / 100.0, 2, ',', '.');
    }

    public function getTotalVatAmount(Invoice $invoice)
    {
        $totalVat = 0;
        $vatPercentage = $invoice->manager->country->vat;

        foreach ($invoice->tickets as $ticket) {
            $price = $ticket->price->value;
            $totalVat += round(($price / (100 + $vatPercentage)) * ($vatPercentage / 100), 2);
        }

        return $totalVat;
    }

    public function getTotalNetAmount(Invoice $invoice)
    {
        $totalVat = 0;
        $vatPercentage = $invoice->manager->country->vat;

        foreach ($invoice->tickets as $ticket) {
            $price = $ticket->price->value;
            $totalVat += round(($price / (100 + $vatPercentage)) * 100, 2);
        }

        return $totalVat;
    }

    public function getTotalPrice(Invoice $invoice, $commission_included = false)
    {
        $reducer = function ($valueField) use ($commission_included) {
            return function ($carry, $item) use ($valueField, $commission_included) {
                return $carry + ($commission_included ?
                        $item->price->{$valueField} - $item->commission_value_origin :
                        $item->price->{$valueField}
                    );
            };
        };

        return $invoice->tickets->reduce($reducer('value'), 0)
            + $invoice->hfTickets->reduce($reducer('value'), 0)
            + $invoice->productSales->reduce($reducer('value_origin'), 0)
            + $invoice->hfProductSales->reduce($reducer('value_origin'), 0);
    }

    public function getCommissionNet(Invoice $invoice)
    {
        $vatPercentage = $invoice->manager->country->vat;
        $reducer = function ($carry, $item) use ($vatPercentage) {
            return $carry + ($item->commission_value_origin - ($item->commission_value_origin / (100 + $vatPercentage) * $vatPercentage)
            );
        };

        return $invoice->tickets->reduce($reducer, 0)
            + $invoice->hfTickets->reduce($reducer, 0)
            + $invoice->productSales->reduce($reducer, 0)
            + $invoice->hfProductSales->reduce($reducer, 0);
    }

    public function getCommissionGross(Invoice $invoice)
    {
        return $invoice->tickets->sum('commission_value_origin')
               + $invoice->hfTickets->sum('commission_value_origin')
               + $invoice->productSales->sum('commission_value_origin')
               + $invoice->hfProductSales->sum('commission_value_origin');
    }

    public function getCommissionVatAmount(Invoice $invoice)
    {
        $commissionGrossAmount = $this->getCommissionGross($invoice);
        $commissionNetAmount = $this->getCommissionNet($invoice);

        $commissionVatAmount = $commissionGrossAmount - $commissionNetAmount;

        return $commissionVatAmount;
    }

    /**
     * Get the tickets qrcode svg
     *
     * @return string
     */
    public function getQrCode(Invoice $invoice)
    {
        return QrCode::format('png')->size(300)->margin(0)->errorCorrection('L')
            ->generate($this->getQrCodeData($invoice));
    }

    /**
     * Get the tickets qrcode data string
     *
     * @param Invoice $invoice
     * @return string
     */
    public function getQrCodeData(Invoice $invoice)
    {
        return implode('|', [
            'manager:'.$invoice->manager->name,
            'reseller:'.($invoice->reseller ? $invoice->reseller->name : ''),
            'brutto:'.$this->getTotalAmount($invoice),
            'netto:'.$this->getTotalNetAmount($invoice),
            'vat:'.$this->getTotalVatAmount($invoice),
        ]);
    }
}