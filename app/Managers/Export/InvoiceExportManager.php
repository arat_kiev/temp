<?php

namespace App\Managers\Export;

use App\Managers\TicketManager;
use App\Models\Area\Area;
use App\Models\Contact\Manager;
use App\Models\Contact\Reseller;
use App\Models\Ticket\Ticket;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Validator;
use PHPExcel;
use PHPExcel_Worksheet as Worksheet;
use PHPExcel_Writer_CSV as WriterCSV;
use PHPExcel_Writer_Excel5 as WriterExcel;
use PHPExcel_Shared_Date as ExcelDate;
use PHPExcel_Style_NumberFormat as ExcelNumberFormat;

class InvoiceExportManager extends BaseExportManager
{
    private $availableFormats = ['xls', 'csv'];

    private $columns = [
        'Bewirtschafter-Nr',    // 0    - A
        'Hejfish-Nr',           // 1    - B
        'Gewässer',             // 2    - C
        'Verkäufer',            // 3    - D
        'Bewirtschafter',       // 4    - E
        'Vorname',              // 5    - F
        'Nachname',             // 6    - G
        'Adresse',              // 7    - H
        'PLZ',                  // 8    - I
        'Ort/Stadt',            // 9    - J
        'Land',                 // 10   - K
        'Geburtstag',           // 11   - L
        'Kaufdatum',            // 12   - M
        'Gültig von',           // 13   - N
        'Gültig bis',           // 14   - O
        'Karten-Typ',           // 15   - P
        'Preis-Typ',            // 16   - Q
        'MwSt.-Satz',           // 17   - R
        'Netto-Betrag',         // 18   - S
        'MwSt.-Betrag',         // 19   - T
        'Brutto-Betrag',        // 20   - U
        'Provision netto',      // 21   - V
        'MwSt.-Provision',      // 22   - W
        'Provision brutto',     // 23   - X
    ];

    protected function validateParams($format, array $filters)
    {
        if (!in_array($format, $this->availableFormats)) {
            throw new \Exception('Invalid format parameter');
        }

        if (!isset($filters['from']) || !isset($filters['till']) || !isset($filters['manager'])) {
            throw new \Exception('Missing required filter params');
        }
    }

    protected function generate(Builder $query, $format, array $filters)
    {
        /** @var TicketManager $ticketManager */
        $ticketManager = app()->make('App\Managers\TicketManager');
        $manager = Manager::find($filters['manager']);
        $invoiceNumber = $filters['invoiceNumber'];

        $tmpFile = tempnam(sys_get_temp_dir(), $invoiceNumber . '-' . $manager->name . '-') . '.' . $format;
        $file = new PHPExcel();
        $worksheet = $file->getSheet();

        $this->drawFilterParamsTable($worksheet, $filters);

        $row = 6;
        // Draw table header
        $worksheet->fromArray($this->columns, '', 'A' . $row);
        $row++;

        $firstDataRow = $row;

        $query->chunk(25, function ($tickets) use ($worksheet, &$row, $ticketManager) {
            $this->drawTableData($worksheet, $tickets, $ticketManager, $row);
        });

        if ($firstDataRow < $row) {
            $this->drawTableFooter($worksheet, $firstDataRow, $row);
            $this->stylingColumns($worksheet, $firstDataRow, $row);
        }

        // Space out columns
        foreach (range('A', 'W') as $columnID) {
            $worksheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $worksheet->setTitle('Verkäufe');
        $worksheet->setSelectedCells();

        switch ($format) {
            case 'xls':
                $excelWriter = new WriterExcel($file);
                $excelWriter->save($tmpFile);
                break;
            case 'csv':
                $csvWriter = new WriterCSV($file);
                $csvWriter->save($tmpFile);
                break;
        }

        return $tmpFile;
    }

    private function drawFilterParamsTable(Worksheet $worksheet, array $filters)
    {
        $from = Carbon::createFromFormat('Y-m-d', $filters['from']);
        $till = Carbon::createFromFormat('Y-m-d', $filters['till']);
        $manager = Manager::find($filters['manager']);

        $worksheet->mergeCellsByColumnAndRow(1, 1, 4, 1);
        $worksheet->setCellValueByColumnAndRow(0, 1, 'Bewirtschafter:');
        $worksheet->setCellValueByColumnAndRow(1, 1, $manager->name);

        $worksheet->setCellValueByColumnAndRow(0, 2, 'Zeitraum von:');
        $cell = $worksheet->setCellValueByColumnAndRow(1, 2, \PHPExcel_Shared_Date::PHPToExcel($from), true);
        $cell->getStyle()->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

        $worksheet->setCellValueByColumnAndRow(0, 3, 'Zeitraum bis:');
        $cell = $worksheet->setCellValueByColumnAndRow(1, 3, \PHPExcel_Shared_Date::PHPToExcel($till), true);
        $cell->getStyle()->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
    }

    private function drawTableData(Worksheet $worksheet, Collection $tickets, TicketManager $ticketManager, &$row)
    {
        /**
         * @var int    $row
         * @var Ticket $ticket
         */
        foreach ($tickets as $ticket) {
            $user = $ticket->user ?: $ticket->resellerTicket;

            $managerTicketIdentifier = $ticket->data_type === 'ticket'
                ? $ticketManager->getManagerIdentifier($ticket)
                : '';

            $worksheet->setCellValueByColumnAndRow(0, $row, $managerTicketIdentifier);
            $worksheet->setCellValueByColumnAndRow(1, $row, $ticketManager->getIdentifier($ticket));
            $worksheet->setCellValueByColumnAndRow(2, $row, $ticket->type->area->name);
            $reseller = ($ticket->data_type === 'ticket' && $ticket->user && ($ticket->client && $ticket->client->name !== 'pos'))
                ? 'online'
                : ($ticket->resellerTicket && $ticket->resellerTicket->reseller
                    ? $ticket->resellerTicket->reseller->name
                    : '-');
            $worksheet->setCellValueByColumnAndRow(3, $row, $reseller);

            $manager = $ticket->type->area->manager;
            $manager_name = $manager->name;
            $worksheet->setCellValueByColumnAndRow(4, $row, $manager_name);

            $worksheet->setCellValueByColumnAndRow(5, $row, $user->first_name);
            $worksheet->setCellValueByColumnAndRow(6, $row, $user->last_name);
            $worksheet->setCellValueByColumnAndRow(7, $row, $user->street);
            $worksheet->setCellValueByColumnAndRow(8, $row, $user->post_code);
            $worksheet->setCellValueByColumnAndRow(9, $row, $user->city);
            $worksheet->setCellValueByColumnAndRow(10, $row, $user->country->name);

            $worksheet->setCellValueByColumnAndRow(11, $row, ExcelDate::PHPToExcel($user->birthday), true);
            $worksheet->setCellValueByColumnAndRow(12, $row, ExcelDate::PHPToExcel($ticket->created_at), true);
            $worksheet->setCellValueByColumnAndRow(13, $row, $ticket->valid_from ? ExcelDate::PHPToExcel($ticket->valid_from) : '', true);
            $worksheet->setCellValueByColumnAndRow(14, $row, $ticket->valid_to ? ExcelDate::PHPToExcel($ticket->valid_to) : '', true);

            $worksheet->setCellValueByColumnAndRow(15, $row, $ticket->type->name);
            $worksheet->setCellValueByColumnAndRow(16, $row, $ticket->price->name);

            $vatPercent = $manager->uid && $ticket->price->show_vat ? $ticketManager->getVatPercent($ticket) : 0;
            $worksheet->setCellValueByColumnAndRow(17, $row, $vatPercent / 100, true);

            $worksheet->setCellValueByColumnAndRow(18, $row, sprintf('=ROUND(U%d/(1+R%d),2)', $row, $row));
            $worksheet->setCellValueByColumnAndRow(19, $row, sprintf('=U%d-S%d', $row, $row));

            $ticketPrice = number_format(((float) $ticket->price->value) / 100, 2, '.', '');
            $worksheet->setCellValueByColumnAndRow(20, $row, $ticketPrice, true);

            $vatCommissionPercent = $ticketManager->getVatPercent($ticket);

            $worksheet->setCellValueByColumnAndRow(21, $row, (float) $ticket->commission_value_origin / (100 + $vatCommissionPercent), true);
            $worksheet->setCellValueByColumnAndRow(22, $row, (float) $ticket->commission_value_origin / (100 + $vatCommissionPercent) * ($vatCommissionPercent / 100), true);
            $worksheet->setCellValueByColumnAndRow(23, $row, sprintf('=SUM(V%d:W%d)', $row, $row));

            $row++;
        }
    }

    private function drawTableFooter(Worksheet $worksheet, $firstDataRow, $row)
    {
        $worksheet->setCellValueByColumnAndRow(17, $row, 'Gesamt');

        // Columns "Netto-Betrag", "MwSt.-Betrag", "Brutto-Betrag", "Provision", "Gesamtpreis"
        $worksheet->setCellValueByColumnAndRow(18, $row, sprintf('=SUM(S%d:S%d)', $firstDataRow, $row - 1));
        $worksheet->setCellValueByColumnAndRow(19, $row, sprintf('=SUM(T%d:T%d)', $firstDataRow, $row - 1));
        $worksheet->setCellValueByColumnAndRow(20, $row, sprintf('=SUM(U%d:U%d)', $firstDataRow, $row - 1));
        $worksheet->setCellValueByColumnAndRow(21, $row, sprintf('=SUM(V%d:V%d)', $firstDataRow, $row - 1));
        $worksheet->setCellValueByColumnAndRow(22, $row, sprintf('=SUM(W%d:W%d)', $firstDataRow, $row - 1));
        $worksheet->setCellValueByColumnAndRow(23, $row, sprintf('=SUM(X%d:X%d)', $firstDataRow, $row - 1));
    }

    private function stylingColumns(Worksheet $worksheet, $firstDataRow, $row)
    {
        // Column "Geburtstag"
        $worksheet->getStyle(sprintf('L%d:L%d', $firstDataRow, $row - 1))
            ->getNumberFormat()
            ->setFormatCode(ExcelNumberFormat::FORMAT_DATE_DDMMYYYY);
        // Column "Kaufdatum"
        $worksheet->getStyle(sprintf('M%d:M%d', $firstDataRow, $row - 1))
            ->getNumberFormat()
            ->setFormatCode(ExcelNumberFormat::FORMAT_DATE_XLSX22);
        // Columns "Gültig von", "Gültig bis"
        $worksheet->getStyle(sprintf('N%d:O%d', $firstDataRow, $row - 1))
            ->getNumberFormat()
            ->setFormatCode(ExcelNumberFormat::FORMAT_DATE_DDMMYYYY);
        // Column "MwSt.-Satz"
        $worksheet->getStyle(sprintf('R%d:R%d', $firstDataRow, $row - 1))
            ->getNumberFormat()
            ->setFormatCode(ExcelNumberFormat::FORMAT_PERCENTAGE);
        // Columns "Netto-Betrag", "MwSt.-Betrag", "Brutto-Betrag", "Provision", "Gesamtpreis"
        $worksheet->getStyle(sprintf('S%d:X%d', $firstDataRow, $row))
            ->getNumberFormat()
            ->setFormatCode('"€ "#,##0.00_-');
    }
}
