<?php

namespace App\Managers\Export;

use Illuminate\Database\Eloquent\Builder;

abstract class BaseExportManager
{
    abstract protected function validateParams($format, array $filters);

    abstract protected function generate(Builder $query, $format, array $filters);
    
    final public function run(Builder $query, $format, array $filters)
    {
        $this->validateParams($format, $filters);
        return $this->generate($query, $format, $filters);
    }
}
