<?php

namespace App\Managers\Export;

use App\Managers\ProductSaleManager;
use App\Models\Contact\Reseller;
use App\Models\Product\ProductSale;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Validator;
use PHPExcel;
use PHPExcel_Worksheet as Worksheet;
use PHPExcel_Writer_CSV as WriterCSV;
use PHPExcel_Writer_Excel5 as WriterExcel;
use PHPExcel_Shared_Date as ExcelDate;
use PHPExcel_Style_NumberFormat as ExcelNumberFormat;

class ProductSaleExportManager extends BaseExportManager
{
    private $availableFormats = ['xls', 'csv'];

    private $columns = [
        'Gewässer',         // 0    - A
        'Verkäufe-Nr',      // 1    - B
        'Produktkonto-Nr.', // 2    - C
        'Verkäufer',        // 3    - D
        'Vorname',          // 4    - E
        'Nachname',         // 5    - F
        'Adresse',          // 6    - G
        'PLZ',              // 7    - H
        'Ort/Stadt',        // 8    - I
        'Land',             // 9    - J
        'Geburtstag',       // 10    - K
        'Kaufdatum',        // 11    - L
        'Gültig von',       // 12   - M
        'Gültig bis',       // 13   - N
        'MwSt.-Satz',       // 14   - O
        'Netto-Betrag',     // 15   - P
        'MwSt.-Betrag',     // 16   - Q
        'Brutto-Betrag',    // 17   - R
    ];

    protected function validateParams($format, array $filters)
    {
        if (!in_array($format, $this->availableFormats)) {
            throw new \Exception('Invalid format parameter');
        }

        if (!isset($filters['from']) || !isset($filters['till']) || !isset($filters['reseller'])) {
            throw new \Exception('Missing required filter params');
        }

        $checkReseller = $filters['reseller'] > 0 ? true : false;
        $validator = Validator::make($filters, [
            'from'      => 'required|date_format:Y-m-d',
            'till'      => 'required|date_format:Y-m-d',
            'reseller'  => 'required|integer' . ($checkReseller ? '|exists:resellers,id' : ''),
        ]);

        if ($validator->fails()) {
            throw new \Exception(json_encode($validator->errors()));
        }
    }

    protected function generate(Builder $query, $format, array $filters)
    {
        /** @var ProductSaleManager $ticketManager */
        $saleManager = app()->make(ProductSaleManager::class);

        $tmpFile = tempnam(sys_get_temp_dir(), 'Produktverkäufe' . date('-d_m_Y-')) . '.' . $format;
        $file = new PHPExcel();
        $worksheet = $file->getSheet();

        $this->drawFilterParamsTable($worksheet, $filters);

        $row = 5;
        // Draw table header
        $worksheet->fromArray($this->columns, '', 'A' . $row);
        $row++;

        $firstDataRow = $row;

        $query->chunk(25, function ($sales) use ($worksheet, &$row, $saleManager) {
            $this->drawTableData($worksheet, $sales, $saleManager, $row);
        });


        if ($firstDataRow < $row) {
            $this->drawTableFooter($worksheet, $firstDataRow, $row);
            $this->stylingColumns($worksheet, $firstDataRow, $row);
        }

        // Space out columns
        foreach (range('A', 'T') as $columnID) {
            $worksheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $worksheet->setTitle('Verkäufe');
        $worksheet->setSelectedCells();

        switch ($format) {
            case 'xls':
                $excelWriter = new WriterExcel($file);
                $excelWriter->save($tmpFile);
                break;
            case 'csv':
                $csvWriter = new WriterCSV($file);
                $csvWriter->save($tmpFile);
                break;
        }

        return $tmpFile;
    }

    private function drawFilterParamsTable(Worksheet $worksheet, array $filters)
    {
        $reseller = $this->fetchResellerName($filters['reseller']);

        $from = Carbon::createFromFormat('Y-m-d', $filters['from']);
        $till = Carbon::createFromFormat('Y-m-d', $filters['till']);

        $worksheet->setCellValueByColumnAndRow(0, 1, 'Zeitraum von:');
        $cell = $worksheet->setCellValueByColumnAndRow(1, 1, \PHPExcel_Shared_Date::PHPToExcel($from), true);
        $cell->getStyle()->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

        $worksheet->setCellValueByColumnAndRow(0, 2, 'Zeitraum bis:');
        $cell = $worksheet->setCellValueByColumnAndRow(1, 2, \PHPExcel_Shared_Date::PHPToExcel($till), true);
        $cell->getStyle()->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

        $worksheet->setCellValueByColumnAndRow(0, 3, 'Verkäufer:');
        $worksheet->setCellValueByColumnAndRow(1, 3, $reseller);
    }

    private function fetchResellerName($resellerId)
    {
        switch ($resellerId) {
            case -1:
                return 'Alle';
            case 0:
                return 'Online';
            default:
                return Reseller::find($resellerId)->name;
        }
    }

    private function drawTableData(Worksheet $worksheet, Collection $sales, ProductSaleManager $saleManager, &$row)
    {
        /**
         * @var int         $row
         * @var ProductSale $sale
         */
        foreach ($sales as $sale) {
            $userData = $sale->user ?: $sale;

            $worksheet->setCellValueByColumnAndRow(0, $row, $sale->product->areas->pluck('name')->first());
            $worksheet->setCellValueByColumnAndRow(1, $row, $saleManager->getIdentifier($sale));
            $worksheet->setCellValueByColumnAndRow(2, $row, $sale->product->account_number);

            $reseller = $sale->reseller ? $sale->reseller->name : 'online';
            $worksheet->setCellValueByColumnAndRow(3, $row, $reseller);

            $worksheet->setCellValueByColumnAndRow(4, $row, $userData->first_name);
            $worksheet->setCellValueByColumnAndRow(5, $row, $userData->last_name);
            $worksheet->setCellValueByColumnAndRow(6, $row, $userData->street);
            $worksheet->setCellValueByColumnAndRow(7, $row, $userData->post_code);
            $worksheet->setCellValueByColumnAndRow(8, $row, $userData->city);
            $worksheet->setCellValueByColumnAndRow(9, $row, $userData->country->name);

            $birthDate = $userData->birthday
                ? ExcelDate::PHPToExcel($userData->birthday)
                : '';
            $featuredFrom = $sale->product->featured_from
                ? ExcelDate::PHPToExcel($sale->product->featured_from)
                : '';
            $featuredTill = $sale->product->featured_till
                ? ExcelDate::PHPToExcel($sale->product->featured_till)
                : '';
            $worksheet->setCellValueByColumnAndRow(10, $row, $birthDate, true);
            $worksheet->setCellValueByColumnAndRow(11, $row, ExcelDate::PHPToExcel($sale->created_at), true);
            $worksheet->setCellValueByColumnAndRow(12, $row, $featuredFrom);
            $worksheet->setCellValueByColumnAndRow(13, $row, $featuredTill);

            $vatPercent = $saleManager->getVatPercent($sale);
            $worksheet->setCellValueByColumnAndRow(14, $row, $vatPercent / 100, true);
            $worksheet->setCellValueByColumnAndRow(15, $row, sprintf('=ROUND(R%d/(1+O%d),2)', $row, $row));
            $worksheet->setCellValueByColumnAndRow(16, $row, sprintf('=R%d-P%d', $row, $row));
            $worksheet->setCellValueByColumnAndRow(17, $row, (float) $sale->price->value_origin / 100, true);
            $row++;
        }
    }

    private function drawTableFooter(Worksheet $worksheet, $firstDataRow, $row)
    {
        $worksheet->setCellValueByColumnAndRow(13, $row, 'Gesamt');

        // Columns "Netto-Betrag", "MwSt.-Betrag", "Brutto-Betrag", "Provision", "Gebühr", "Gesamtpreis"
        $worksheet->setCellValueByColumnAndRow(14, $row, sprintf('=SUM(O%d:O%d)', $firstDataRow, $row - 1));
        $worksheet->setCellValueByColumnAndRow(15, $row, sprintf('=SUM(P%d:P%d)', $firstDataRow, $row - 1));
        $worksheet->setCellValueByColumnAndRow(16, $row, sprintf('=SUM(Q%d:Q%d)', $firstDataRow, $row - 1));
        $worksheet->setCellValueByColumnAndRow(17, $row, sprintf('=SUM(R%d:R%d)', $firstDataRow, $row - 1));
    }

    private function stylingColumns(Worksheet $worksheet, $firstDataRow, $row)
    {
        // Column "Geburtstag"
        $worksheet->getStyle(sprintf('J%d:J%d', $firstDataRow, $row - 1))
            ->getNumberFormat()
            ->setFormatCode(ExcelNumberFormat::FORMAT_DATE_DDMMYYYY);
        // Column "Kaufdatum"
        $worksheet->getStyle(sprintf('K%d:K%d', $firstDataRow, $row - 1))
            ->getNumberFormat()
            ->setFormatCode(ExcelNumberFormat::FORMAT_DATE_XLSX22);
        // Columns "Gültig von", "Gültig bis"
        $worksheet->getStyle(sprintf('L%d:M%d', $firstDataRow, $row - 1))
            ->getNumberFormat()
            ->setFormatCode(ExcelNumberFormat::FORMAT_DATE_DDMMYYYY);
        // Column "MwSt.-Satz"
        $worksheet->getStyle(sprintf('N%d:N%d', $firstDataRow, $row - 1))
            ->getNumberFormat()
            ->setFormatCode(ExcelNumberFormat::FORMAT_PERCENTAGE);
        // Columns "Netto-Betrag", "MwSt.-Betrag", "Brutto-Betrag", "Provision", "Gebühr", "Gesamtpreis"
        $worksheet->getStyle(sprintf('O%d:T%d', $firstDataRow, $row))
            ->getNumberFormat()
            ->setFormatCode('"€ "#,##0.00_-');
    }
}
