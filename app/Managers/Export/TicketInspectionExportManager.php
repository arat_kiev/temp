<?php

namespace App\Managers\Export;

use App\Managers\TicketManager;
use App\Models\Area\Area;
use App\Models\Ticket\TicketInspection;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Validator;
use PHPExcel;
use PHPExcel_Worksheet as Worksheet;
use PHPExcel_Writer_CSV as WriterCSV;
use PHPExcel_Writer_Excel5 as WriterExcel;
use PHPExcel_Shared_Date as ExcelDate;

class TicketInspectionExportManager extends BaseExportManager
{
    private $availableFormats = ['xls', 'csv'];

    private $columns = [
        'Bewirtschafter-Nr',    // 0    - A
        'Hejfish-Nr',      // 1    - B
        'Gewässer',             // 2    - C
        'Bewirtschafter',       // 3    - D
        'Angler-Name',          // 4    - E
        'Angler-Fischer-ID',    // 5    - F
        'Fänge',                // 6    - G
        'Status',               // 7    - H
        'Kommentar',            // 8    - I
        'Kontrolliert am',      // 9    - J
    ];

    const DATETIME_FORMAT = 'dd.mm.yy hh:mm';
    
    protected function validateParams($format, array $filters)
    {
        if (!in_array($format, $this->availableFormats)) {
            throw new \Exception('Invalid format parameter');
        }

        if (!isset($filters['from']) || !isset($filters['till'])
            || !isset($filters['area']) || !isset($filters['inspector'])) {
            throw new \Exception('Missing required filter params');
        }

        $checkArea = $filters['area'] > 0 ? true : false;
        $checkInspector = $filters['area'] > 0 ? true : false;
        $validator = Validator::make($filters, [
            'from'      => 'required|date_format:Y-m-d',
            'till'      => 'required|date_format:Y-m-d',
            'area'      => 'required|integer' . ($checkArea ? '|exists:areas,id' : ''),
            'inspector' => 'required|integer' . ($checkInspector ? '|exists:users,id' : ''),
        ]);

        if ($validator->fails()) {
            throw new \Exception(json_encode($validator->errors()));
        }
    }

    protected function generate(Builder $query, $format, array $filters)
    {
        /** @var TicketManager $ticketManager */
        $ticketManager = app()->make('App\Managers\TicketManager');

        $tmpFile = tempnam(sys_get_temp_dir(), 'export') . date('_-_d_m_Y') . '.' . $format;
        $file = new PHPExcel();
        $worksheet = $file->getSheet();

        $this->drawFilterParamsTable($worksheet, $filters);

        $row = 6;
        // Draw table header
        $worksheet->fromArray($this->columns, '', 'A' . $row);
        $row++;

        $firstDataRow = $row;

        $query->chunk(25, function ($tickets) use ($worksheet, &$row, $ticketManager) {
            $this->drawTableData($worksheet, $tickets, $row, $ticketManager);
        });

        if ($firstDataRow < $row) {
            $this->stylingColumns($worksheet, $firstDataRow, $row);
        }

        // Space out columns
        foreach (range('A', 'J') as $columnID) {
            $worksheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $worksheet->setTitle('Angelkarte Kontrolle');
        $worksheet->setSelectedCells();

        switch ($format) {
            case 'xls':
                $excelWriter = new WriterExcel($file);
                $excelWriter->save($tmpFile);
                break;
            case 'csv':
                $csvWriter = new WriterCSV($file);
                $csvWriter->save($tmpFile);
                break;
        }

        return $tmpFile;
    }

    private function drawFilterParamsTable(Worksheet $worksheet, array $filters)
    {
        $area = $this->fetchAreaName($filters['area']);
        $inspector = $this->fetchInspectorName($filters['inspector']);

        $from = Carbon::createFromFormat('Y-m-d', $filters['from']);
        $till = Carbon::createFromFormat('Y-m-d', $filters['till']);

        $worksheet->setCellValueByColumnAndRow(0, 1, 'Zeitraum von:');
        $cell = $worksheet->setCellValueByColumnAndRow(1, 1, \PHPExcel_Shared_Date::PHPToExcel($from), true);
        $cell->getStyle()->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

        $worksheet->setCellValueByColumnAndRow(0, 2, 'Zeitraum bis:');
        $cell = $worksheet->setCellValueByColumnAndRow(1, 2, \PHPExcel_Shared_Date::PHPToExcel($till), true);
        $cell->getStyle()->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);

        $worksheet->setCellValueByColumnAndRow(0, 3, 'Gewässer:');
        $worksheet->setCellValueByColumnAndRow(1, 3, $area);

        $worksheet->setCellValueByColumnAndRow(0, 4, 'Kontrolleur:');
        $worksheet->setCellValueByColumnAndRow(1, 4, $inspector);
    }

    private function fetchAreaName($areaId)
    {
        switch ($areaId) {
            case 0:
                return 'Alle';
            default:
                return Area::find($areaId)->name;
        }
    }

    private function fetchInspectorName($inspectorId)
    {
        switch ($inspectorId) {
            case 0:
                return 'Alle';
            default:
                return User::find($inspectorId)->full_name;
        }
    }

    private function drawTableData(Worksheet $worksheet, Collection $inspections, &$row, TicketManager $ticketManager)
    {
        /**
         * @var int                 $row
         * @var TicketInspection    $inspection
         */
        foreach ($inspections as $inspection) {
            $ticket = $inspection->ticket;
            $user = $inspection->ticket->user ?: $inspection->ticket->resellerTicket;

            $worksheet->setCellValueByColumnAndRow(0, $row, $ticketManager->getManagerIdentifier($ticket));
            $worksheet->setCellValueByColumnAndRow(1, $row, $ticketManager->getIdentifier($ticket));
            $worksheet->setCellValueByColumnAndRow(2, $row, $ticket->type->area->name);

            $manager = $ticket->type->area->manager->name;
            $worksheet->setCellValueByColumnAndRow(3, $row, $manager);

            $worksheet->setCellValueByColumnAndRow(4, $row, $user->full_name);
            $worksheet->setCellValueByColumnAndRow(5, $row, $user->fisher_id);
            $worksheet->setCellValueByColumnAndRow(6, $row, $ticket->hauls()->count());
            $worksheet->setCellValueByColumnAndRow(7, $row, $inspection->status);
            $worksheet->setCellValueByColumnAndRow(8, $row, $inspection->comment);
            $worksheet->setCellValueByColumnAndRow(9, $row, ExcelDate::PHPToExcel($inspection->created_at), true);

            $row++;
        }
    }

    private function stylingColumns(Worksheet $worksheet, $firstDataRow, $row)
    {
        // Column "Kontrolliert am"
        $worksheet->getStyle(sprintf('J%d:J%d', $firstDataRow, $row - 1))
            ->getNumberFormat()
            ->setFormatCode(self::DATETIME_FORMAT);
    }
}