<?php

namespace App\Managers\Export;

use App\Managers\TicketManager;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use PHPExcel;
use PHPExcel_Worksheet as Worksheet;
use PHPExcel_Writer_CSV as WriterCSV;
use PHPExcel_Writer_Excel5 as WriterExcel;
use PHPExcel_Style_NumberFormat as ExcelNumberFormat;

class HaulExportManager extends BaseExportManager
{
    const DATE_FORMAT = 'dd.mm.yyyy';
    
    private $availableFormats = ['xls', 'csv'];

    private $columns = [
        'ID',               // 0    - A
        'Fischart',         // 1    - B
        'Anzahl',           // 2    - C
        'Größe/Gewicht',    // 3    - D
        'Einheit',          // 4    - E
        'Entnommen',        // 5    - F
        'Fischer',          // 6    - G
        'Angelkarten-Nr.',  // 7    - H
        'Gewässer',         // 8    - I
        'Fangdatum',        // 9   - J
        'Fangzeit',         // 10   - K
        'Anmerkungen',      // 11   - L
    ];

    protected function validateParams($format, array $filters = [])
    {
        if (!in_array($format, $this->availableFormats)) {
            throw new \Exception('Invalid format parameter');
        }
        if (!isset($filters['from']) || !isset($filters['till'])) {
            throw new \Exception('Missing required filter params');
        }
    }

    protected function generate(Builder $query, $format, array $filters)
    {
        $from = $filters['from'];
        $till = $filters['till'];
        $tmpFile = tempnam(sys_get_temp_dir(), 'Fangliste_' . $from . '-' . $till).'.'.$format;
        $file = new PHPExcel();
        $worksheet = $file->getSheet();

        $row = 1;
        // Draw table header
        $worksheet->fromArray($this->columns, '', 'A' . $row);
        $row++;

        $query->chunk(25, function ($hauls) use ($worksheet, &$row) {
            $this->drawTableData($worksheet, $hauls, $row);
        });

        // Space out columns
        foreach (range('A', 'J') as $columnID) {
            $worksheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $worksheet->setTitle('Fangstatistik');

        switch ($format) {
            case 'xls':
                $excelWriter = new WriterExcel($file);
                $excelWriter->save($tmpFile);
                break;

            case 'csv':
                $csvWriter = new WriterCSV($file);
                $csvWriter->save($tmpFile);
                break;

            default:
                throw new \Exception('Invalid format parameter');
        }

        return $tmpFile;
    }

    private function drawTableData(Worksheet $worksheet, Collection $hauls, &$row)
    {
        foreach ($hauls as $haul) {
            $dateOfCatch = $haul->catch_date->format('d.m.Y');
            $timeOfCatch = $haul->catch_time ?: '';
            $count = $haul->count ?: 1;
            $taken = $haul->taken ? 'Entnommen' : 'Nicht entnommen';

            if ($haul->ticket) {
                $user = $haul->ticket->resellerTicket ? $haul->ticket->resellerTicket->full_name : $haul->ticket->user->full_name;
            } else {
                $user = $haul->user ? $haul->user->full_name : 'Unbekannt';
            }


            if ($haul->fish && $haul->size_value != 0) {
                $fishType = $haul->fish->name;
                $sizeValue = $haul->size_value ?: '';
                $sizeType = $haul->size_value ? $haul->size_type : '';
            } else {
                $fishType = 'Leermeldung';
                $sizeValue = $sizeType = '';
            }

            $worksheet->setCellValueByColumnAndRow(0, $row, $haul->id);
            $worksheet->setCellValueByColumnAndRow(1, $row, $fishType);
            $worksheet->setCellValueByColumnAndRow(2, $row, $count);
            $sizeCell = $worksheet->setCellValueByColumnAndRow(3, $row, $sizeValue);
            $worksheet->setCellValueByColumnAndRow(4, $row, $sizeType);
            $worksheet->setCellValueByColumnAndRow(5, $row, $taken);
            $worksheet->setCellValueByColumnAndRow(6, $row, $user);
            $ticketNumber = $haul->ticket_number ?:
                ($haul->ticket ? (new TicketManager())->getIdentifier($haul->ticket) : null);
            $worksheet->setCellValueByColumnAndRow(7, $row, $ticketNumber);
            $worksheet->setCellValueByColumnAndRow(8, $row, ($haul->area ? $haul->area->name : $haul->area_name));
            $dateCell = $worksheet->setCellValueByColumnAndRow(9, $row, $dateOfCatch);
            $timeCell = $worksheet->setCellValueByColumnAndRow(10, $row, $timeOfCatch);
            $worksheet->setCellValueByColumnAndRow(11, $row, $haul->user_comment);

            $sizeCell->getStyle()->getNumberFormat()->setFormatCode(ExcelNumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $dateCell->getStyle()->getNumberFormat()->setFormatCode(self::DATE_FORMAT);
            $timeCell->getStyle()->getNumberFormat()->setFormatCode(ExcelNumberFormat::FORMAT_DATE_TIME3);

            $row++;
        }
    }
}
