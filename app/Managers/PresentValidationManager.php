<?php

namespace App\Managers;

use Illuminate\Support\Arr;
use Countable;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\File\File;

class PresentValidationManager
{
    /**
     * Validate that an attribute exists (even if not filled) when another attribute has a given value.
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  mixed   $parameters
     * @param  mixed   $validator
     * @return bool
     */
    protected function validatePresentIf($attribute, $value, $parameters, $validator)
    {
        $this->requireParameterCount(2, $parameters, 'present_if');

        $other = Arr::get($validator->getData(), $parameters[0]);

        $values = array_slice($parameters, 1);

        if (is_bool($other)) {
            $values = $this->convertValuesToBoolean($values);
        }

        if (in_array($other, $values)) {
            return $this->validateRequired($attribute, $value);
        }

        return true;
    }

    /**
     * Validate that an attribute exists (even if not filled) when another attribute does not have a given value.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  mixed  $parameters
     * @param  mixed   $validator
     * @return bool
     */
    protected function validatePresentUnless($attribute, $value, $parameters, $validator)
    {
        $this->requireParameterCount(2, $parameters, 'present_unless');

        $data = Arr::get($validator->getData(), $parameters[0]);

        $values = array_slice($parameters, 1);

        if (!in_array($data, $values)) {
            return $this->validateRequired($attribute, $value);
        }

        return true;
    }

    /**
     * Validate that an attribute exists (even if not filled) when any other attribute exists.
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  mixed   $parameters
     * @return bool
     */
    public function validatePresentWith($attribute, $value, $parameters, $validator)
    {
        if (!$this->allFailingRequired($parameters, $validator)) {
            return $this->validatePresent($attribute, $validator);
        }

        return true;
    }

    /**
     * Validate that an attribute exists (even if not filled) when all other attributes exists.
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  mixed   $parameters
     * @return bool
     */
    public function validatePresentWithAll($attribute, $value, $parameters, $validator)
    {
        if (!$this->anyFailingRequired($parameters, $validator)) {
            return $this->validateRequired($attribute, $value);
        }

        return true;
    }

    /**
     * Validate that an attribute exists (even if not filled) when another attribute does not.
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  mixed   $parameters
     * @return bool
     */
    public function validatePresentWithOut($attribute, $value, $parameters, $validator)
    {
        if ($this->anyFailingRequired($parameters, $validator)) {
            return $this->validateRequired($attribute, $value);
        }

        return true;
    }

    /**
     * Validate that an attribute exists (even if not filled) when all other attributes do not.
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  mixed   $parameters
     * @return bool
     */
    public function validatePresentWithOutAll($attribute, $value, $parameters, $validator)
    {
        if ($this->allFailingRequired($parameters, $validator)) {
            return $this->validateRequired($attribute, $value);
        }

        return true;
    }



    /**
     * Validate that an attribute exists even if not filled.
     *
     * @param  string  $attribute
     * @param  mixed   $validator
     * @return bool
     */
    protected function validatePresent($attribute, $validator)
    {
        return Arr::has($validator->getData(), $attribute);
    }

    /**
     * Convert the given values to boolean if they are string "true" / "false".
     *
     * @param  array  $values
     * @return array
     */
    protected function convertValuesToBoolean($values)
    {
        return array_map(function ($value) {
            if ($value === 'true') {
                return true;
            } elseif ($value === 'false') {
                return false;
            }

            return $value;
        }, $values);
    }

    /**
     * Determine if all of the given attributes fail the required test.
     *
     * @param  array  $attributes
     * @param  mixed   $validator
     * @return bool
     */
    protected function allFailingRequired(array $attributes, $validator)
    {
        foreach ($attributes as $key) {
            if ($this->validateRequired($key, $this->getValue($key, $validator))) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determine if any of the given attributes fail the required test.
     *
     * @param  array  $attributes
     * @param  mixed   $validator
     * @return bool
     */
    protected function anyFailingRequired(array $attributes, $validator)
    {
        foreach ($attributes as $key) {
            if (!$this->validateRequired($key, $validator->getValue($key))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get the value of a given attribute.
     *
     * @param  string  $attribute
     * @param  mixed   $validator
     * @return mixed
     */
    protected function getValue($attribute, $validator)
    {
        return Arr::get($validator->getData(), $attribute);
    }

    /**
     * Validate that a required attribute exists.
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @return bool
     */
    protected function validateRequired($attribute, $value)
    {
        if (is_null($value)) {
            return false;
        } elseif (is_string($value) && trim($value) === '') {
            return false;
        } elseif ((is_array($value) || $value instanceof Countable) && count($value) < 1) {
            return false;
        } elseif ($value instanceof File) {
            return (string) $value->getPath() != '';
        }

        return true;
    }

    /**
     * Require a certain number of parameters to be present.
     *
     * @param  int    $count
     * @param  array  $parameters
     * @param  string  $rule
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    protected function requireParameterCount($count, $parameters, $rule)
    {
        if (count($parameters) < $count) {
            throw new InvalidArgumentException("Validation rule $rule requires at least $count parameters.");
        }
    }
}
