<?php

namespace App\Managers;

use App\Events\PasswordReset as PasswordResetEvent;
use App\Models\Client;
use App\Models\Location\State;
use App\Models\Organization\Member;
use App\Models\PasswordReset;
use App\Models\User;
use Event;

class UserManager
{
    public static function assignUserToMember(User $user)
    {
        info('job: assignUserToMember', ['user' => $user->id]);

        if ($member = Member::where('email', $user->email)->first()) {
            self::connectUserAndMember($user, $member);
        }
    }

    public static function assignMemberToUser(Member $member)
    {
        info('job: assignMemberToUser', ['member' => $member->id]);

        if ($user = User::where('email', $member->email)->first()) {
            self::connectUserAndMember($user, $member);
        }
    }

    private static function connectUserAndMember($user, $member)
    {
        $member->user()->associate($user);
        $member->save();
    }

    public function resetPassword(User $user, Client $client)
    {
        $token = \Uuid::generate(4);

        PasswordReset::create([
            'emails'    => $user->email,
            'token'     => $token,
        ]);

        // Event::fire(new PasswordResetEvent($user, $token, $client));
    }
    
    public function prePasswordResetCheck(User $user)
    {
        if (PasswordReset::where('emails', '=', $user->email)->count() >= PasswordReset::MAX_ALLOWED_ATTEMPTS) {
            return false;
        }

        return true;
    }
    
    public function fetchState(User $user)
    {
        return $user->post_code && $user->country
            ? State::where('country_id', $user->country->id)
                ->whereHas('regions', function ($regions) use ($user) {
                    $regions->whereHas('cities', function ($cities) use ($user) {
                        $cities->where('post_code', $user->post_code);
                    });
                })
                ->first()
            : null;
    }
}
