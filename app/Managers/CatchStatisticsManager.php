<?php

namespace App\Managers;

use Carbon\Carbon;
use \DateTime;
use \DateInterval;
use \DatePeriod;
use Illuminate\Support\Collection;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CatchStatisticsManager
{
    /** @var Carbon */
    private static $from;


    private static $till;

    /**
     * CatchStatisticsManager constructor.
     *
     * @param $from
     * @param $till
     */
    public function __construct(Carbon $from, Carbon $till)
    {
        self::$from = $from;
        self::$till = $till;
    }

    /**
     * Return statistics per hour of day
     *
     * @param Collection $hauls
     * @return array
     */
    public static function countPerHourOfDay(Collection $hauls)
    {
        $hourRanges = self::getHoursOfDayRanges();

        foreach ($hauls as $haul) {
            foreach ($hourRanges as $index => $range) {
                // Check if Catch time pass to our Hour range
                if (strtotime($range['start']) <= strtotime($haul->catch_time)
                        && strtotime($range['end']) >= strtotime($haul->catch_time)) {
                    $hourRanges[$index]['count'] += 1;
                }
            }
        }

        return [
            'data'          => $hourRanges,
            'totalCount'    => count($hauls),
        ];
    }

    /**
     * Return statistics per days
     *
     * @param Collection $hauls
     * @return array
     */
    public static function countPerDays(Collection $hauls)
    {
        $dayRanges = self::getDayRanges();

        foreach ($hauls as $haul) {
            foreach ($dayRanges as $index => $range) {
                // Check if Catch day same as day in range
                if (date_format($haul->catch_date, "Y-m-d") == $range['date']) {
                    $dayRanges[$index]['count'] += 1;
                }
            }
        }

        return [
            'data'          => $dayRanges,
            'totalCount'    => count($hauls),
        ];
    }

    /**
     * Return statistics per weeks
     *
     * @param Collection $hauls
     * @return array
     */
    public static function countPerWeeks(Collection $hauls)
    {
        $weekRanges = self::getWeekRanges();

        foreach ($hauls as $haul) {
            foreach ($weekRanges as $index => $range) {
                // Check if Catch date pass to our Week range
                if (new DateTime($range['start']) <= new DateTime($haul->catch_date)
                    && new DateTime($range['end']) >= new DateTime($haul->catch_date)) {
                    $weekRanges[$index]['count'] += 1;
                }
            }
        }

        return [
            'data'          => $weekRanges,
            'totalCount'    => count($hauls),
        ];
    }

    /**
     * Return statistics per months
     *
     * @param Collection $hauls
     * @return array
     */
    public static function countPerMonths(Collection $hauls)
    {
        $monthRanges = self::getMonthRanges();

        foreach ($hauls as $haul) {
            foreach ($monthRanges as $index => $range) {
                // Check if Catch date pass to our Month range
                if (new DateTime($range['start']) <= new DateTime($haul->catch_date)
                    && new DateTime($range['end']) >= new DateTime($haul->catch_date)) {
                    $monthRanges[$index]['count'] += 1;
                }
            }
        }

        return [
            'data'          => $monthRanges,
            'totalCount'    => count($hauls),
        ];
    }

    /**
     * Return statistics per seasons
     *
     * @param Collection $hauls
     * @return array
     */
    public static function countPerSeasons(Collection $hauls)
    {
        $seasonRanges = self::getSeasonRanges();

        foreach ($hauls as $haul) {
            foreach ($seasonRanges as $index => $range) {
                // Check if Catch date pass to our Season range
                if (new DateTime($range['start']) <= new DateTime($haul->catch_date)
                    && new DateTime($range['end']) >= new DateTime($haul->catch_date)) {
                    $seasonRanges[$index]['count'] += 1;
                }
            }
        }

        return [
            'data'          => $seasonRanges,
            'totalCount'    => count($hauls),
        ];
    }

    /**
     * Return range per days
     *
     * @return array
     */
    private static function getHoursOfDayRanges()
    {
        $ranges = [];
        for ($i = 0; $i < 24; $i++) {
            $ranges[] = [
                'start' => sprintf("%02d", $i) . ':00:00',
                'end'   => sprintf("%02d", $i) . ':59:59',
                'count' => 0,
            ];
        }

        return $ranges;
    }

    /**
     * Return range per days
     *
     * @return array
     */
    private static function getDayRanges()
    {
        $start = self::$from->startOfDay();
        $end = self::$till->endOfDay();
        $interval = new \DateInterval('P1D');
        $dateRange = new \DatePeriod($start, $interval, $end);

        $days = [];
        foreach ($dateRange as $index => $date) {
            $days[$index] = [
                'date'  => $date->format('Y-m-d'),
                'count' => 0,
            ];
        }

        return $days;
    }

    /**
     * Return range per weeks
     *
     * @return array
     */
    private static function getWeekRanges()
    {
        $start = self::$from->startOfDay();
        $end = self::$till->endOfDay();
        $interval = new \DateInterval('P1D');
        $dateRange = new \DatePeriod($start, $interval, $end);

        $weekNumber = 0;
        $weeks = [];
        foreach ($dateRange as $date) {
            $weeks[$weekNumber][] = $date->format('Y-m-d');
            if ($date->format('w') === '0') {
                $weekNumber++;
            }
        }

        return array_map(function ($week) {
            return [
                'start' => reset($week),
                'end'   => end($week),
                'count' => 0,
            ];
        }, $weeks);
    }

    /**
     * Return range per months
     *
     * @return array
     */
    private static function getMonthRanges()
    {
        $start = self::$from->startOfDay();
        $end = self::$till->endOfDay();
        $interval = new DateInterval('P1D');
        $dateRange = new DatePeriod($start, $interval, $end);

        $monthNumber = 0;
        $months = [];
        foreach ($dateRange as $date) {
            if ($date->format('d') === '01') {
                $monthNumber++;
            }
            $months[$monthNumber][] = $date->format('Y-m-d');
        }

        return array_map(function ($month) {
            return [
                'start' => reset($month),
                'end'   => end($month),
                'count' => 0,
            ];
        }, $months);
    }

    /**
     * Return range per seasons
     *
     * @return array
     */
    private static function getSeasonRanges()
    {
        $start = self::$from->startOfDay();
        $end = self::$till->endOfDay();
        $interval = new DateInterval('P1D');
        $dateRange = new DatePeriod($start, $interval, $end);

        $yearNumber = 0;
        $years = [];
        foreach ($dateRange as $date) {
            if ($date->format('d') === '01' && $date->format('m') === '01') {
                $yearNumber++;
            }
            $years[$yearNumber][] = $date->format('Y-m-d');
        }

        return array_map(function ($year) {
            return [
                'start' => reset($year),
                'end'   => end($year),
                'count' => 0,
            ];
        }, $years);
    }
}
