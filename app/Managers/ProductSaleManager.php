<?php

namespace App\Managers;

use App\Events\ProductEndsInStock;
use App\Events\ProductSaleStatusChanged;
use App\Exceptions\NotEnoughBalanceException;
use App\Exceptions\ProductMaxQuantityException;
use App\Exceptions\ProductNotAvailableException;
use App\Exceptions\ProductTimeslotMissingException;
use App\Exceptions\ProductTimeslotNotAvailableException;
use App\Exceptions\ProfileNotCompleteException;
use App\Models\Contact\Reseller;
use App\Models\Product\Product;
use App\Models\Product\ProductPrice;
use App\Models\Product\ProductSale;
use App\Models\User;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use QrCode;
use Event;
use DB;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use CommissionManager;

class ProductSaleManager
{
    public function getUserName(ProductSale $sale)
    {
        if ($sale->user) {
            $firstName = $this->getUserFirstName($sale);
            $lastName = $this->getUserLastName($sale);

            if ($firstName && $lastName) {
                return join(' ', [$firstName, $lastName]);
            }
        } elseif ($sale->reseller) {
            return $sale->reseller->name;
        }

        return 'Unbekannt';
    }

    public function getUserFirstName(ProductSale $sale)
    {
        if ($sale->user) {
            return $sale->user->first_name;
        } else {
            return null;
        }
    }

    public function getUserLastName(ProductSale $sale)
    {
        if ($sale->user) {
            return $sale->user->last_name;
        } else {
            return null;
        }
    }

    public function getUserBirthday(ProductSale $sale)
    {
        if ($sale->user) {
            return $sale->user->birthday->format('d.m.Y');
        } else {
            return 'Unbekannt';
        }
    }

    /**
     * Get the product's identifier string
     *
     * @param ProductSale $sale
     * @return string
     */
    public function getIdentifier(ProductSale $sale)
    {
        // Get the last 2 digits of the year
        $year = $sale->created_at->year - 2000;

        return sprintf(ProductSale::IDENT_FORMAT, $year, $sale->id);
    }

    /**
     * Get the product's control code
     *
     * @param ProductSale $sale
     * @return number|string
     */
    public function getControlCode(ProductSale $sale)
    {
        $num = $sale->product->created_at->format('jm');

        $num -= $sale->product->created_at->year;
        $num = abs($num);

        $num = str_pad($num, 4, '0', STR_PAD_LEFT);
        $num = strrev($num);

        return $num;
    }

    /**
     * Get the product's vat percentage (from country)
     *
     * @param ProductSale $sale
     * @return integer
     */
    public function getVatPercent(ProductSale $sale)
    {
        return $sale->price->vat;
    }

    /**
     * Get the product's vat amount
     *
     * @param ProductSale $sale
     * @return float
     */
    public function getVatAmount(ProductSale $sale, $vat = null)
    {
        $vat = $vat ?: $this->getVatPercent($sale);
        $val = $this->getTotalAmountOrigin($sale);

        return round(($val / (100 + $vat)) * ($vat / 100), 2);
    }

    public function getVatAmountFormatted(ProductSale $sale, $vat = null)
    {
        return number_format($this->getVatAmount($sale, $vat),2, ',', '.');
    }

    /**
     * Gets the total amount for sale including fees
     *
     * @param ProductSale $sale
     * @return mixed
     */
    public function getTotalAmount(ProductSale $sale)
    {
        $sum = $this->getTotalAmountOrigin($sale) / 100;

        return number_format($sum, 2, ',', '.');
    }

    private function getTotalAmountOrigin(ProductSale $sale)
    {
        $sum = $sale->price->value_origin * $sale->quantity;
        // TODO: commissions

        foreach ($sale->fees as $fee) {
            $sum += $fee->value_origin;
        }
        $sum = $sale->storno_id ? -$sum : $sum;

        return $sum;
    }

    /**
     * Get the product's QR code
     *
     * @param ProductSale $sale
     * @return string
     */
    public function getQrCode(ProductSale $sale)
    {
        return $this->getQrCodeFromString($this->getQrCodeData($sale));
    }

    /**
     * Get qr code from string
     *
     * @param string $string
     * @return string
     */
    public function getQrCodeFromString($string)
    {
        return QrCode::format('svg')
            ->size(300)
            ->margin(0)
            ->errorCorrection('L')
            ->generate($string);
    }

    /**
     * Get the product's QR code data string
     *
     * @param ProductSale $sale
     * @return string
     */
    public function getQrCodeData(ProductSale $sale)
    {
        return implode('|', [
            'dat:' . $sale->created_at->format('d.m.Y-H:i'),
            'id:' . $this->getIdentifier($sale),
            'prod:' . $sale->product->name,
            'name:' . $this->getUserName($sale),
            'bday:' . $this->getUserBirthday($sale),
            'status:'.($sale->storno_id ? 'storno' : 'active'),
        ]);
    }

    public function createStornoSale(ProductSale $sale, $reason)
    {
        DB::transaction(function () use ($sale, $reason) {
            $stornoSale = $sale->replicate(['offisy_id', 'offisy_code', 'offisy_tags']);

            ProductSale::$stornoReason = $reason;
            $sale->delete();

            // Create Storno sale
            $stornoSale->storno_id = $sale->id;
            $stornoSale->save();

            // Money back to user balance
            if ($sale->user && $sale->price && !$sale->reseller) {
                $amount = $sale->price->value_origin;
                $amount += $this->fetchFeeValues($sale->price, $sale->user);
                $sale->user->addCredit($amount);
            }
        });
    }

    public function canBuyProduct(Product $product, User $user, $options)
    {
        // TODO: refactor to pipeline (\Illuminate\Pipeline\Pipeline)
        if (!$user->complete) {
            throw new ProfileNotCompleteException();
        }

        if (!$product->price) {
            throw new AccessDeniedHttpException('There is no active price for selected product');
        }

        if ($options['quantity'] > $product->max_quantity && $product->max_quantity !== 0) {
            throw new ProductMaxQuantityException('Maximum quantity exceeded');
        }

        if ($product->productStocks()->count() && $product->productStocks->sum('quantity') < $options['quantity']) {
            throw new ProductNotAvailableException($options['quantity'] - $product->productStocks->sum('quantity'));
        }

        if ($product->price->timeslots()->count() > 0) {
            if (isset($options['timeslot_dates']) && count($options['timeslot_dates']) > 0) {
                $query = $product->price->timeslotDates()
                    ->whereIn('timeslot_dates.id', $options['timeslot_dates'])
                    ->withCount('productSales');

                if ($query->count() !== count($options['timeslot_dates'])) {
                    throw new ProductTimeslotNotAvailableException("Timeslot unavailable");
                }

                $query->each(function ($slot) {
                    if ($slot->pool - $slot->product_sales_count <= 0) {
                        throw new ProductTimeslotNotAvailableException("Timeslot unavailable");
                    }
                });
            } else {
                throw new ProductTimeslotMissingException("No timeslots selected");
            }
        }

        if ($product->price->timeslotDates()->exists()) {
            $dm = app(DiscountManager::class);
            $total = $dm->discountedPrice($product->price, count($options['timeslot_dates']));
        } else {
            $priceValue = $product->price->value_origin;
            $feesValue = $this->fetchFeeValues($product->price, $user);
            $total = ($priceValue + $feesValue) * $options['quantity'];
        }

        if (!$user->hasEnoughCredit($total)) {
            throw new NotEnoughBalanceException($user->balance - $total);
        }
    }

    /**
     * Buy product by user
     *
     * @param Product $product
     * @param User $user
     * @param $options
     * @return ProductSale
     * @throws \Exception
     * @throws \Throwable
     */
    public function buyProduct(Product $product, User $user, $options)
    {
        // TODO: refactor to pipeline (\Illuminate\Pipeline\Pipeline)

        $quantity  = $options['quantity'] ?: 1;

        if (!$user->complete) {
            throw new ProfileNotCompleteException();
        }

        if (!$product->price) {
            throw new AccessDeniedHttpException('There is no active price for selected product');
        }

        if ($quantity > $product->max_quantity && $product->max_quantity !== 0) {
            throw new ProductMaxQuantityException('Maximum quantity exceeded');
        }

        if ($product->productStocks()->count() && $product->productStocks->sum('quantity') < $quantity) {
            throw new ProductNotAvailableException($quantity - $product->productStocks->sum('quantity'));
        }

        if ($product->price->timeslots()->count() > 0) {
            if (isset($options['timeslot_dates']) && count($options['timeslot_dates']) > 0) {
                $query = $product->price->timeslotDates()
                    ->whereIn('timeslot_dates.id', $options['timeslot_dates'])
                    ->withCount('productSales');

                if ($query->count() !== count($options['timeslot_dates'])) {
                    throw new ProductTimeslotNotAvailableException("Timeslot unavailable");
                }

                $query->each(function ($slot) {
                    if ($slot->pool - $slot->product_sales_count <= 0) {
                        throw new ProductTimeslotNotAvailableException("Timeslot unavailable");
                    }
                });
            } else {
                throw new ProductTimeslotMissingException("No timeslots selected");
            }
        }

        /** @var ProductSale $sale */
        $sale = DB::transaction(function () use ($product, $user, $quantity, $options) {
            if ($product->price->timeslotDates()->exists()) {
                $dm = app(DiscountManager::class);
                $total = $dm->discountedPrice($product->price, count($options['timeslot_dates']));
            } else {
                $priceValue = $product->price->value_origin;
                $feesValue = $this->fetchFeeValues($product->price, $user);
                $total = ($priceValue + $feesValue) * $quantity;
            }

            $user->useCredit($total);

            $sale = $this->createProductSale($product, $user, $quantity);

            // Connect the timeslots to this sale

            if ($product->price->timeslots()->count() > 0) {
                $sale->timeslotDates()->sync($options['timeslot_dates']);
            }

            return $sale;
        });

        // Fire email to product purchaser email
        Event::fire(new ProductSaleStatusChanged($sale, ProductSale::DEFAULT_STATUS));

        return $sale;
    }
    
    public function fetchFeeValues(ProductPrice $price, User $user)
    {
        return $price->fees()
            ->whereHas('country', function ($query) use ($user) {
                $query->whereId($user->country->id);
            })
            ->get()
            ->reduce(function ($carry, $item) use ($price) {
                $res = $carry + (
                    $item->type === 'relative'
                        // divide per 100% and 100 for convert DB value to float price
                        ? $price->value_origin * ($item->value_origin / 10000)
                        : $item->value_origin
                    );
                return $res;
            }, 0);
    }

    /**
     * Create product sale for product bought online
     *
     * @param Product   $product
     * @param User      $user
     *
     * @return ProductSale
     */
    protected function createProductSale(Product $product, User $user, $quantity = 1, $isOnline = true)
    {
        $sale = new ProductSale([
            'status'        => ProductSale::DEFAULT_STATUS,
            'email'         => $user->email,
            'first_name'    => $user->first_name,
            'last_name'     => $user->last_name,
            'street'        => $user->street,
            'post_code'     => $user->post_code,
            'city'          => $user->city,
            'quantity'      => $quantity,
            'commission_value'  => (CommissionManager::fetchProductPriceCommission($product->price, $isOnline)) * $quantity,
        ]);

        $sale->product()->associate($product);
        $sale->price()->associate($product->price);
        $sale->user()->associate($user);
        $sale->client()->associate(config('app.client'));
        $sale->country()->associate($user->country);

        $sale->save();

        return $sale;
    }

    /**
     * Buy product sale by reseller
     *
     * @param Product   $product
     * @param Reseller  $reseller
     * @param array     $userData
     * @throws AccessDeniedHttpException
     *
     * @return ProductSale
     */
    public function buyResellerProductSale(Product $product, Reseller $reseller, array $userData)
    {
        $quantity  = $userData['quantity'] ?: 1;

        if (!$product->price) {
            throw new AccessDeniedHttpException('There is no active price for selected product');
        }

        if ($quantity > $product->max_quantity && $product->max_quantity !== 0) {
            throw new ProductMaxQuantityException('Maximum quantity exceeded');
        }

        if ($product->productStocks()->count() && $product->productStocks->sum('quantity') < $quantity) {
            throw new ProductNotAvailableException($quantity - $product->productStocks->sum('quantity'));
        }

        if ($product->price->timeslots()->count() > 0) {
            if (isset($userData['timeslot_dates']) && count($userData['timeslot_dates']) > 0) {
                $query = $product->price->timeslotDates()
                    ->whereIn('timeslot_dates.id', $userData['timeslot_dates'])
                    ->withCount('productSales');

                if ($query->count() !== count($userData['timeslot_dates'])) {
                    throw new ProductTimeslotNotAvailableException("Timeslot unavailable");
                }

                $query->each(function ($slot) {
                    if ($slot->pool - $slot->product_sales_count <= 0) {
                        throw new ProductTimeslotNotAvailableException("Timeslot unavailable");
                    }
                });
            } else {
                throw new ProductTimeslotMissingException("No timeslots selected");
            }
        }

        /** @var ProductSale $sale */
        $sale = DB::transaction(function () use ($product, $userData, $quantity, $reseller) {
            $sale = $this->createProductSaleByUserData($product, $userData, $quantity);
            // Connect the timeslots to this sale
            if ($product->price->timeslots()->count() > 0) {
                $sale->timeslotDates()->sync($userData['timeslot_dates']);
            }

            $sale->reseller()->associate($reseller);
            return $sale;
        });

        $sale->save();

        // Fire email to product purchaser email
        Event::fire(new ProductSaleStatusChanged($sale, ProductSale::DEFAULT_STATUS));

        return $sale;
    }

    /**
     * Create product sale for product bought via Point Of Sale (reseller)
     *
     * @param Product   $product
     * @param array     $userData
     *
     * @return ProductSale
     */
    protected function createProductSaleByUserData(Product $product, array $userData, $quantity)
    {
        $sale = new ProductSale([
            'status'        => ProductSale::DEFAULT_STATUS,
            'email'         => $userData['email'],
            'first_name'    => $userData['first_name'],
            'last_name'     => $userData['last_name'],
            'street'        => $userData['street'],
            'post_code'     => $userData['post_code'],
            'city'          => $userData['city'],
            'quantity'      => $quantity,
            'commission_value'  => CommissionManager::fetchProductPriceCommission($product->price, false) * 100,
        ]);

        $sale->product()->associate($product);
        $sale->price()->associate($product->price);
        $sale->client()->associate(config('app.client'));
        $sale->country()->associate($userData['country_id']);

        $sale->save();

        return $sale;
    }

    public function shipProduct(Request $request, ProductSale $sale)
    {
        $productStocks = $sale->product->productStocks;
        
        if (!$productStocks->count()) {
            return;
        }
        
        $stockId = $request->request->getInt('stock');

        if (!$stockId) {
            $responseData = ['notification' => 'danger', 'message' => 'Kein Lager angegeben'];
            throw new HttpResponseException(response()->json($responseData));
        } elseif (!in_array($stockId, $productStocks->pluck('stock_id')->toArray())) {
            $responseData = ['notification' => 'danger', 'message' => 'Falsches Lager'];
            throw new HttpResponseException(response()->json($responseData));
        }

        $stock = $productStocks->where('stock_id', $stockId)->first();
        $stock->quantity -= $sale->quantity;
        $stock->save();

        if ($stock->quantity <= $stock->notification_quantity) {
            Event::fire(new ProductEndsInStock($stock));
        }
    }
}
