<?php

namespace App\Managers;

use App\Models\Commission;
use App\Models\Product\ProductPrice;
use App\Models\Ticket\TicketPrice;
use Carbon\Carbon;

class CommissionManager
{
    public function sumStringsAsFloat(): float
    {
        $args = func_get_args();

        $floatedArgs = array_map(function ($value) {
            return is_string($value) ? floatval(str_replace(',', '.', $value)) : floatval($value);
        }, $args);

        return (float)array_sum($floatedArgs);
    }

    public function formatFloat(float $value): string
    {
        return number_format($value, 2, ',', '.');
    }

    public function addDefaultCommissions(TicketPrice $ticketPrice)
    {
        $this->addDefaultResellerCommission($ticketPrice);
        $this->addDefaultOnlineCommission($ticketPrice);
    }

    private function addDefaultOnlineCommission(TicketPrice $ticketPrice)
    {
        $managerId = $ticketPrice->ticketType->area->manager->id;

        $commissionTicketPrice = TicketPrice::with('commissions')
            ->whereHas('commissions', function ($query) {
                $query->where('is_online', true)
                    ->whereNull('valid_till');
            })
            ->whereHas('ticketType.area.manager', function ($query) use ($managerId) {
                $query->whereId($managerId);
            })
            ->orderBy('created_at', 'desc')
            ->first();

        $commission = $commissionTicketPrice
            ? $commissionTicketPrice->commissions()
                ->where('is_online', true)
                ->whereNull('valid_till')
                ->orderBy('created_at', 'desc')
                ->first()
            : (object) Commission::DEFAULT_COMMISSIONS['online'];

        $ticketPrice->commissions()->create([
            'commission_type'   => $commission->commission_type,
            'commission_value'  => $commission->commission_value,
            'min_value'         => $commission->min_value,
            'is_online'         => true,
            'valid_from'        => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

    private function addDefaultResellerCommission(TicketPrice $ticketPrice)
    {
        $managerId = $ticketPrice->ticketType->area->manager->id;

        $commissionTicketPrice = TicketPrice::with('commissions')
            ->whereHas('commissions', function ($query) {
                $query->where('is_online', false)
                    ->whereNull('valid_till');
            })
            ->whereHas('ticketType.area.manager', function ($query) use ($managerId) {
                $query->whereId($managerId);
            })
            ->orderBy('created_at', 'desc')
            ->first();

        $commission = $commissionTicketPrice
            ? $commissionTicketPrice->commissions()
                ->where('is_online', false)
                ->whereNull('valid_till')
                ->orderBy('created_at', 'desc')
                ->first()
            : (object) Commission::DEFAULT_COMMISSIONS['reseller'];

        $ticketPrice->commissions()->create([
            'commission_type'   => $commission->commission_type,
            'commission_value'  => $commission->commission_value,
            'min_value'         => $commission->min_value,
            'is_online'         => false,
            'valid_from'        => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

    public function fetchTicketPriceCommission(TicketPrice $ticketPrice, $isOnline = true): float
    {
        $commission = $ticketPrice->commissionsWithFallback($isOnline)
            ?: ($isOnline
                ? (object)Commission::DEFAULT_COMMISSIONS['online']
                : (object)Commission::DEFAULT_COMMISSIONS['reseller']);

        $commission_online = $ticketPrice->commissionsWithFallback(true);

        $vatPercentage = $ticketPrice->ticketType->area->manager->country->vat;

        $commission_percent = $commission->commission_value_origin / 100;

        $commission_included = $ticketPrice->ticketType->area->manager->commission_included;

        if ($commission->commission_type === 'ABSOLUTE'
            && ($commission->commission_value_origin > $commission->min_value_origin)) {
            return $commission->commission_value_origin / 100.0;
        }

        if (($commission_percent * $ticketPrice->value / 100) > $commission->min_value_origin) {

            if ($commission_included) {
                return $commission_percent * ($ticketPrice->value / ((100.0 + $commission_online->commission_value_origin) / 100.0)) / 1000.0;
            }

            return $commission_percent  * $ticketPrice->value / 10000.0;
        }

        return $commission->min_value_origin / 1.2 * (1 +  $vatPercentage / 100) / 100.0;
    }

    public function fetchTotalTicketPriceValue(TicketPrice $ticketPrice, $isOnline = true): float
    {
        return $this->sumStringsAsFloat(
            $ticketPrice->value / 100,
            $this->fetchTicketPriceCommission($ticketPrice, $isOnline)
        );
    }

    public function fetchProductPriceCommission(ProductPrice $productPrice, $isOnline = true): float
    {
        $commission = $productPrice->commissionsWithFallback($isOnline)
            ?: ($isOnline
                ? (object)Commission::DEFAULT_COMMISSIONS['online']
                : (object)Commission::DEFAULT_COMMISSIONS['reseller']);

        if ($commission->commission_type === 'ABSOLUTE'
            && ($commission->commission_value_origin > $commission->min_value_origin)) {
            return $commission->commission_value_origin;
        }
        if (($commission->commission_value_origin * $productPrice->value_origin / 10000) > $commission->min_value_origin) {
            return $commission->commission_value_origin * $productPrice->value_origin / 10000;
        }

        return $commission->min_value_origin;

    }

    public function fetchTotalProductPriceValue(ProductPrice $productPrice, $isOnline = true): float
    {
        return $this->sumStringsAsFloat(
            $productPrice->value_origin / 100,
            $this->fetchProductPriceCommission($productPrice, $isOnline) / 100
        );
    }
}
