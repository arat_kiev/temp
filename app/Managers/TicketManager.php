<?php

namespace App\Managers;

use App\CashRegister\Offisy\Client as OffisyClient;
use App\Exceptions\ActiveTicketsException;
use App\Exceptions\InvalidGroupCountException;
use App\Exceptions\ModelException;
use App\Exceptions\NotEnoughBalanceException;
use App\Exceptions\ProfileNotCompleteException;
use App\Exceptions\QuotaDepletedException;
use App\Exceptions\TicketDateNotValidException;
use App\Exceptions\TicketLicenseMissingException;
use App\Models\Area\Area;
use App\Models\Contact\Manager;
use App\Models\Contact\Reseller;
use App\Models\License\License;
use App\Models\Ticket\ResellerTicket;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketPrice;
use App\Models\User;
use Carbon\Carbon;
use DB;
use QrCode;

class TicketManager
{
    private static $weekDays = [
        0 => 'SUN',
        1 => 'MON',
        2 => 'TUE',
        3 => 'WED',
        4 => 'THU',
        5 => 'FRI',
        6 => 'SAT',
    ];

    const MAX_FISH_PER_DAY = 6;
    const MAX_FISHING_DAYS = 152;

    /** @var CommissionManager */
    private $commissionManager;

    public function __construct()
    {
        $this->commissionManager = app('App\Managers\CommissionManager');
    }

    /**
     * @param TicketPrice $price
     * @param Carbon $date
     * @return bool
     */
    public function isValidDate(TicketPrice $price, Carbon $date)
    {
        $weekDays = collect($price->weekdays);
        $lockPeriods = collect($price->lockPeriods);

        if (Carbon::now()->lt($price->visible_from)) {
            return false;
        }

        if (!$date->between($price->valid_from, $price->valid_to)) {
            return false;
        }

        // check for valid weekday
        if ($weekDays->contains('weekday', static::$weekDays[$date->dayOfWeek])) {
            return false;
        }

        // check for each lock period
        foreach ($lockPeriods as $period) {
            if ($date->between($period->lock_from, $period->lock_till)) {
                return false;
            }
        }

        if (!$this->hasQuotaRemaining($price, $date)) {
            return false;
        }

        return true;
    }

    /**
     * @param TicketPrice $price
     * @return array
     */
    public function getValidDates(TicketPrice $price)
    {
        $start = $price->valid_from;
        $end = $price->valid_to;

        $validDates = [];

        /** @var Carbon $curDate */
        for ($curDate = $start; $curDate->lte($end); $curDate->addDay()) {
            if ($this->isValidDate($price, $curDate) && $this->hasQuotaRemaining($price, $curDate)) {
                $validDates[$curDate->year][$curDate->month][$curDate->day] = [
                    'type' => $price->ticketType->quotaUnit ? $price->ticketType->quotaUnit->name : null,
                    'max' => $price->ticketType->quota_max,
                    'left' => $this->getQuotaRemaining($price, $curDate),
                ];
            }
        }

        return $validDates;
    }

    /**
     * @param TicketPrice $price
     * @param User $user
     * @return bool
     */
    public function hasValidLicenses(TicketPrice $price, User $user)
    {
        $requiredLicenses = $price->ticketType->requiredLicenseTypes;
        $optionalLicenses = $price->ticketType->optionalLicenseTypes;
        $userLicenses = $user->licenses()->valid()->get();

        $hasLicenses = false;

        if ($optionalLicenses->isEmpty()) {
            $hasLicenses = true;
        } else {
            foreach ($optionalLicenses as $license) {
                if ($userLicenses->contains(function($value) use ($license) {
                    return $value->license_type_id === $license->id;
                })
                ) {
                    $hasLicenses = true;
                    break;
                }
            }
        }

        if (!$requiredLicenses->isEmpty()) {
            foreach ($requiredLicenses as $license) {
                if (!$userLicenses->contains(function ($value) use ($license) {
                    return $value->license_type_id === $license->id;
                })
                ) {
                    $hasLicenses = false;
                }
            }
        }

        return $hasLicenses;
    }

    /**
     * @param TicketPrice $price
     * @param User $user
     * @return bool
     */
    public function hasEnoughBalance(TicketPrice $price, User $user)
    {
        return $this->missingBalance($price, $user) <= 0;
    }

    /**
     * @param TicketPrice $price
     * @param User $user
     * @return integer
     */
    public function missingBalance(TicketPrice $price, User $user)
    {
        return $price->value - $user->balance;
    }

    /**
     * @param TicketPrice $price
     * @param Carbon $startDate
     * @return Carbon|null
     */
    public function validFrom(TicketPrice $price, Carbon $startDate)
    {
        $date = null;

        switch ($price->ticketType->category->type) {
            case 'HOUR':
                $date = $startDate->copy();
                break;
            case 'DAY':
                $date = $startDate->copy()->setTime(0, 0);
                break;
            case 'WEEK':
                $date = $startDate->copy()->startOfWeek();
                break;
            case 'MONTH':
                $date = $startDate->copy()->startOfMonth();
                break;
            case 'SEASON':
                $date = $price->valid_from->copy();
                break;
            case 'NOTIME':
                $date = $price->ticketType->created_at->copy();
        }

        return $date;
    }

    /**
     * @param TicketPrice $price
     * @param Carbon $startDate
     * @return Carbon|null
     */
    public function validTo(TicketPrice $price, Carbon $startDate)
    {
        $date = null;
        $duration = $price->ticketType->duration;

        switch ($price->ticketType->category->type) {
            case 'HOUR':
                $date = $startDate->copy()->addHours($duration);
                break;
            case 'DAY':
                $date = $startDate->copy()->setTime(23, 59)->addDays($duration - 1);
                break;
            case 'WEEK':
                $date = $startDate->copy()->endOfWeek()->addWeeks($duration - 1);
                break;
            case 'MONTH':
                $date = $startDate->copy()->endOfMonth()->addMonths($duration - 1);
                break;
            case 'SEASON':
                $date = $price->valid_to;
                break;
            case 'NOTIME':
                $date = $startDate->createFromDate(2100, 12, 31);
        }

        return $date;
    }

    public function getUserName(Ticket $ticket)
    {
        $first_name = $this->getUserFirstName($ticket);
        $last_name = $this->getUserLastName($ticket);

        if (!empty($first_name) && !empty($last_name)) {
            return join(' ', [$first_name, $last_name]);
        }

        return 'Unbekannt';
    }

    public function getUserFirstName(Ticket $ticket)
    {
        if ($ticket->user) {
            return $ticket->user->first_name;
        } elseif ($ticket->resellerTicket) {
            return $ticket->resellerTicket->first_name;
        } else {
            return null;
        }
    }

    public function getUserLastName(Ticket $ticket)
    {
        if ($ticket->user) {
            return $ticket->user->last_name;
        } elseif ($ticket->resellerTicket) {
            return $ticket->resellerTicket->last_name;
        } else {
            return null;
        }
    }

    public function getUserBirthday(Ticket $ticket)
    {
        if ($ticket->user) {
            return $ticket->user->birthday->format('d.m.Y');
        } elseif ($ticket->resellerTicket) {
            return $ticket->resellerTicket->birthday->format('d.m.Y');
        } else {
            return 'Unbekannt';
        }
    }

    /**
     * Get the tickets identifier string
     *
     * @return string
     */
    public function getIdentifier(Ticket $ticket)
    {
        // Get the last 2 digits of the year
        $year = $ticket->valid_from->year - 2000;

        return sprintf(Ticket::IDENT_FORMAT, $year, $ticket->id);
    }

    public function getManagerIdentifier(Ticket $ticket)
    {
        $year = $ticket->valid_from->year - 2000;

        $short = $ticket->type->area->manager->short_code ?: 'FISH';

        return sprintf(Ticket::IDENT_FORMAT_MANAGER, $short, $year, $ticket->manager_ticket_id);
    }

    /**
     * Get the tickets qrcode
     *
     * @return string
     */
    public function getQrCode(Ticket $ticket)
    {
        return $this->getQrCodeFromString($this->getQrCodeData($ticket));
    }

    /**
     * Get qr code from string
     *
     * @param string $string
     * @return string
     */
    public function getQrCodeFromString($string)
    {
        return QrCode::format('svg')
            ->encoding('UTF-8')
            ->size(300)
            ->margin(0)
            ->errorCorrection('L')
            ->generate($string);
    }

    /**
     * Get the tickets qrcode data string
     *
     * @param Ticket $ticket
     * @return string
     */
    public function getQrCodeData(Ticket $ticket)
    {
        return implode('|', [
            'dat:' . $ticket->created_at->format('d.m.Y-H:i'),
            'id:' . $this->getIdentifier($ticket),
            'rev:' . $ticket->type->area->name,
            'typ:' . $ticket->type->name,
            'val:' . $this->getValidString($ticket),
            'name:' . $this->getUserName($ticket),
            'bday:' . $this->getUserBirthday($ticket),
            'status:'.($ticket->storno_id ? 'storno' : 'active'),
        ]);
    }

    /**
     * Get the tickets control code
     *
     * @param Ticket $ticket
     * @return number|string
     */
    public function getControlCode(Ticket $ticket)
    {
        $num = $ticket->created_at->format('jm');

        $num -= $ticket->created_at->year;
        $num = abs($num);

        $num = str_pad($num, 4, '0', STR_PAD_LEFT);
        $num = strrev($num);

        return $num;
    }

    /**
     * Get the tickets readable date/time range
     *
     * @return string
     */
    public function getValidString(Ticket $ticket)
    {
        if ($ticket->type->category->type === 'HOUR') {
            $validString = $ticket->valid_from->formatLocalized('%e. %B %Y - %H:%M') . ' - ' . $ticket->valid_to->formatLocalized('%e. %B %Y - %H:%M');
        } elseif ($ticket->type->category->type === 'DAY' && $ticket->type->duration === 1) {
            $validString = $ticket->valid_from->formatLocalized('%e. %B %Y');
        } else {
            $validString = $ticket->valid_from->formatLocalized('%e. %B %Y') . ' - ' . $ticket->valid_to->formatLocalized('%e. %B %Y');
        }

        if (!ends_with(setlocale(LC_ALL, "0"), '.utf8')) {
            $validString = utf8_encode($validString);
        }

        return $validString;
    }

    /**
     * Get the tickets vat percentage (from country)
     *
     * @param Ticket $ticket
     * @return integer
     */
    public function getVatPercent(Ticket $ticket)
    {
        return $ticket->type->area->manager->country->vat;
    }

    /**
     * Get ticket price
     *
     * @param Ticket $ticket
     * @return string
     */
    public function getTicketPrice(Ticket $ticket)
    {
        $cm = $this->commissionManager;
        $commission_included = $ticket->type->area->manager->commission_included;

        $value = $ticket->storno_id ? -$ticket->price->value : $ticket->price->value;

        if($commission_included && !$ticket->user) {
            $value = $ticket->price->value - $cm->fetchTicketPriceCommission($ticket->price) * 100;
            $value = $ticket->storno_id ? $value * -1 : $value;
        }
        return number_format($value / 100.0, 2, ',', '.');
    }

    /**
     * Get the tickets vat amount
     *
     * @param Ticket $ticket
     * @return float
     */
    public function getVatAmount(Ticket $ticket, $vat = null)
    {
        $vat = $vat ?: $this->getVatPercent($ticket);
        $val = $ticket->storno_id ? -$ticket->price->value : $ticket->price->value;

        return round(($val / (100 + $vat)) * ($vat / 100), 2);
    }

    public function getVatAmountFormatted(Ticket $ticket, $vat = null)
    {
        return number_format($this->getVatAmount($ticket, $vat), 2, ',', '.');
    }

    public function getNettoPrice(Ticket $ticket)
    {
        $vat = $this->getVatPercent($ticket);
        $value = $ticket->storno_id ? -$ticket->price->value : $ticket->price->value;

        return round($value / (100.0 + $vat), 2);
    }

    /**
     * Get maximum fish per day for catchlog
     *
     * @param Ticket $ticket
     * @return int
     */
    public function getMaxFishPerDay(Ticket $ticket)
    {
        return $ticket->type->fish_per_day <= self::MAX_FISH_PER_DAY
            ? $ticket->type->fish_per_day
            : self::MAX_FISH_PER_DAY;
    }

    /**
     * Get maximum fishing days for catchlog
     *
     * @param Ticket $ticket
     * @return int
     */
    public function getMaxFishingDays(Ticket $ticket)
    {
        return $ticket->type->fishing_days <= self::MAX_FISHING_DAYS
            ? $ticket->type->fishing_days
            : self::MAX_FISHING_DAYS;
    }

    /**
     * Check if catchlog should be displayed (on ticket)
     *
     * @return bool
     */
    public function shouldDisplayCatchlog(Ticket $ticket)
    {
        return $ticket->type->category->type === 'HOUR' || ($ticket->type->category->type === 'DAY' && $ticket->type->duration < 2);
    }

    /**
     * @param TicketPrice $price
     * @param User        $user
     * @param Carbon      $startDate
     * @param bool        $isOnline
     * @return Ticket
     */
    protected function createTicket(TicketPrice $price, $user, Carbon $startDate, $isOnline = true)
    {
        $ticket = new Ticket([
            'valid_from'        => $this->validFrom($price, $startDate),
            'valid_to'          => $this->validTo($price, $startDate),
            'commission_value'  => $this->commissionManager->fetchTicketPriceCommission($price, $isOnline) * 100,
        ]);

        /** @var Manager $manager */
        $manager = $price->ticketType->area->manager;
        $ticket->manager_ticket_id = $manager->getNextTicketId($startDate->year);

        $ticket->type()->associate($price->ticketType);
        $ticket->price()->associate($price);
        $ticket->user()->associate($user);
        $ticket->client()->associate(config('app.client'));

        $ticket->save();

        return $ticket;
    }

    /**
     * Buy ticket by reseller
     *
     * @param TicketPrice $price
     * @param Reseller $reseller
     * @param array|string[] $userData
     * @param Carbon $startDate
     * @return ResellerTicket
     */
    public function createResellerTicket(TicketPrice $price, Reseller $reseller, $userData, Carbon $startDate)
    {
        // Check date
        if (!$this->isValidDate($price, $startDate)) {
            throw new TicketDateNotValidException();
        }

        // Quota check
        if (!$this->hasQuotaRemaining($price, $startDate)) {
            throw new QuotaDepletedException();
        }

        // Create ticket
        $ticket = $this->createTicket($price, null, $startDate, false);

        // Create "wrapper" ticket and associate with original
        $resellerTicket = new ResellerTicket($userData);
        $resellerTicket->baseTicket()->associate($ticket);

        $reseller = $price->resellers()->find($reseller->id);

        // Set reseller commission
        $resellerTicket->commission_type = $reseller->pivot->commission_type;
        $resellerTicket->commission_value = $reseller->pivot->commission_value;

        // Set reseller and save ticket to database
        $resellerTicket->reseller()->associate($reseller);

        // Send RKSV if enabled
        $manager = $ticket->type->area->manager;
        if ($manager->rksv_enabled) {
            $this->sendRKSV($resellerTicket);
        }

        // Save and return
        $resellerTicket->save();

        return $resellerTicket;
    }

    public function createStornoTicket(Ticket $ticket, $reason)
    {
        DB::transaction(function () use ($ticket, $reason) {
            $stornoTicket = $ticket->replicate(['offisy_id', 'offisy_code', 'offisy_tags']);

            Ticket::$stornoReason = $reason;
            $ticket->delete();

            // Create Storno ticket
            $stornoTicket->storno_id = $ticket->id;
            $stornoTicket->storno_reason = $reason;
            $stornoTicket->save();

            // storno reseller ticket
            if ($oldResellerTicket = $ticket->resellerTicket) {
                $resellerTicket = $oldResellerTicket->replicate(['ticket_id']);
                $resellerTicket->baseTicket()->associate($stornoTicket);
                $resellerTicket->save();

                // Send RKSV if enabled
                $manager = $ticket->type->area->manager;
                if ($manager->rksv_enabled && $ticket->offisy_code) {
                    $this->sendRKSV($resellerTicket);
                }
            }

            // Money back to user balance
            if ($ticket->user && $ticket->price && !$ticket->resellerTicket) {
                $amount = $ticket->price->value;
                $ticket->user->addCredit($amount);
            }
        });
    }

    private function sendRKSV(ResellerTicket $resellerTicket)
    {
        /** @var Ticket $ticket */
        $ticket = $resellerTicket->baseTicket;
        $manager = $ticket->type->area->manager;

        try {
            $client = new OffisyClient();

            // Get previously failed tickets and resend them to rksv
            $this->getFailedResellerTickets($manager->id)->each(function (ResellerTicket $failedTicket) use ($client) {
                $receipt = $client->createReceipt(
                    $this->formatReceiptOptions($failedTicket)
                )->toArray();

                $ticket = $failedTicket->baseTicket;
                $ticket->offisy_id = $receipt['id'];
                $ticket->offisy_code = $receipt['attributes']['signatures'][0]['code'];
                $ticket->offisy_tags = $receipt['attributes']['signatures'][0]['tags'];
                $ticket->save();
            });

            $receipt = $client->createReceipt(
                $this->formatReceiptOptions($resellerTicket)
            )->toArray();

            $ticket->offisy_id = $receipt['id'];
            $ticket->offisy_code = $receipt['attributes']['signatures'][0]['code'];
            $ticket->offisy_tags = $receipt['attributes']['signatures'][0]['tags'];
        } catch (\Exception $exception) {
            info('rksv-error', compact('exception'));
            $ticket->offisy_code = base64_encode('Sicherheitseinrichtung ausgefallen');
            $ticket->offisy_tags = [
                [
                    "Name" => "Info",
                    "Label" => "",
                    "Value" => "Sicherheitseinrichtung ausgefallen",
                ],
            ];
        } finally {
            $ticket->save();
        }
    }

    private function formatReceiptOptions(ResellerTicket $resellerTicket)
    {
        $ticket = $resellerTicket->baseTicket;
        $manager = $ticket->type->area->manager;

        return [
            'ticket_identifier' => $this->getIdentifier($ticket),
            'ticket_type_id' => $ticket->type->id,
            'ticket_type_name' => $ticket->type->name,
            'ticket_price_id' => $ticket->price->id,
            'ticket_price_name' => $ticket->price->name,
            'area_name' => $ticket->type->area->name,
            'vat_id' => $this->getOffisyVatId($ticket),
            'ticket_price_net' => $ticket->storno_id ? -$this->getNettoPrice($ticket) : $this->getNettoPrice($ticket),
            'company_id' => $manager->offisy_id,
        ];
    }

    private function getOffisyVatId(Ticket $ticket)
    {
        $country = $ticket->type->area->manager->country;

        return $ticket->price->show_vat ?
            $country->offisy_vat_id :
            $country->offisy_no_vat_id;
    }

    private function getFailedResellerTickets($manager_id)
    {
        return ResellerTicket::select('reseller_tickets.*')
            ->join('tickets', 'tickets.id', '=', 'reseller_tickets.ticket_id')
            ->join('ticket_types', 'ticket_types.id', '=', 'tickets.ticket_type_id')
            ->join('areas', 'areas.id', '=', 'ticket_types.area_id')
            ->where('areas.manager_id', '=', $manager_id)
            ->whereNull('tickets.offisy_id')
            ->whereNotNull('tickets.offisy_code')
            ->orderBy('reseller_tickets.ticket_id')
            ->get();
    }

    public function canBuyTicket(TicketPrice $price, User $user, $options)
    {
        if (!$user->complete) {
            throw new ProfileNotCompleteException();
        }

        // Check license
        if (!$this->hasValidLicenses($price, $user)) {
            throw new TicketLicenseMissingException();
        }

        // Check date
        if (!$this->isValidDate($price, $options['startDate'])) {
            throw new TicketDateNotValidException();
        }

        // Quota check
        if (!$this->hasQuotaRemaining($price, $options['startDate'])) {
            throw new QuotaDepletedException();
        }

        // check for valid count (equal to price setting)
        if ($price->group_count > 1) {
            if (count($options['group']) !== $price->group_count - 1) {
                throw new InvalidGroupCountException();
            }
        }

        $priceValue = $price->value;
        if (!$user->hasEnoughCredit($priceValue)) {
            throw new NotEnoughBalanceException($user->balance - $priceValue);
        }
    }

    /**
     * Buy ticket by user
     *
     * @param TicketPrice $price
     * @param User $user
     * @param Carbon $startDate
     * @return Ticket
     * @throws NotEnoughBalanceException
     * @throws ProfileNotCompleteException
     * @throws TicketDateNotValidException
     * @throws TicketLicenseMissingException
     * @throws QuotaDepletedException
     * @throws ModelException
     */
    public function buyTicket(TicketPrice $price, User $user, Carbon $startDate)
    {
        // TODO: refactor to pipeline (\Illuminate\Pipeline\Pipeline)

        if (!$user->complete) {
            throw new ProfileNotCompleteException();
        }

        // Check license
        if (!$this->hasValidLicenses($price, $user)) {
            throw new TicketLicenseMissingException();
        }

        // Check date
        if (!$this->isValidDate($price, $startDate)) {
            throw new TicketDateNotValidException();
        }

        // Quota check
        if (!$this->hasQuotaRemaining($price, $startDate)) {
            throw new QuotaDepletedException();
        }

        $priceValue = $price->value;
        $user->useCredit($priceValue);

        $ticket = $this->createTicket($price, $user, $startDate);

        return $ticket;
    }

    /**
     * Check if user has date intersections in tickets
     *
     * @param TicketPrice $price
     * @param User        $user
     * @param Carbon      $startDate
     * @throws ActiveTicketsException
     */
    public function checkUserTicketsIntersection(TicketPrice $price, User $user, Carbon $startDate)
    {
        $from = $this->validFrom($price, $startDate);
        $till = $this->validTo($price, $startDate);

        /** @var Ticket $ticket */
        foreach ($this->getUserTicketsForArea($user, $price->ticketType->area) as $ticket) {
            if ($this->hasDateIntervalIntersection($from, $till, $ticket->valid_from, $ticket->valid_to)) {
                throw new ActiveTicketsException();
            } elseif ($ticket->type->is_catchlog_required && !$ticket->hauls()->count()) {
                throw new ModelException('No catch for ticket with id ' . $ticket->id);
            }
        }
    }

    /**
     * Check if two date ranges intersect
     *
     * @param Carbon $from_a
     * @param Carbon $till_a
     * @param Carbon $from_b
     * @param Carbon $till_b
     * @return boolean
     */
    public function hasDateIntervalIntersection($from_a, $till_a, $from_b, $till_b)
    {
        return $from_a->lte($till_b) && $till_a->gte($from_b);
    }

    /**
     * Get the a list of the users tickets with specified ticket type
     *
     * @param User $user
     * @param Area $area
     * @return array|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getUserTicketsForArea($user, $area)
    {
        return Ticket::withoutStorno()
            ->select('tickets.*', 'hauls.id as haul_id')
            ->join('ticket_types', 'tickets.ticket_type_id', '=', 'ticket_types.id')
            ->leftJoin('hauls', 'hauls.ticket_id', '=', 'tickets.id')
            ->where('tickets.user_id', '=', $user->id)
            ->where('ticket_types.area_id', '=', $area->id)
            ->groupBy('tickets.id')
            ->get();
    }

    /**
     * Remaining quota count
     *
     * @param TicketPrice $price
     * @param Carbon $startDate
     * @return int
     */
    public function getQuotaRemaining(TicketPrice $price, Carbon $startDate)
    {
        if (!($quotaUnit = $price->ticketType->quotaUnit)) {
            return -1;
        }

        $from = $startDate->copy()->setTime(0, 0);
        $till = $startDate->copy()->setTime(23, 59);

        switch ($quotaUnit->type) {
            case 'DAILY':
                break;
            case 'WEEKLY':
                $from->startOfWeek();
                $till->endOfWeek();
                break;
            case 'MONTHLY':
                $from->startOfMonth();
                $till->endOfMonth();
                break;
            case 'SEASON':
                $from = $price->valid_from;
                $till = $price->valid_to;
                break;
        }

        $quotaCur = $price->ticketType->tickets()
            ->withoutStorno()
            ->whereBetween('valid_from', [$from, $till])->count();

        return $price->ticketType->quota_max - $quotaCur;
    }

    /**
     * Check if has remaining quota
     *
     * @param TicketPrice $price
     * @param Carbon $startDate
     * @return bool
     */
    public function hasQuotaRemaining(TicketPrice $price, Carbon $startDate)
    {
        return $price->ticketType->quotaUnit ? ($this->getQuotaRemaining($price, $startDate)) > 0 : true;
    }

    public function filterPrices(array &$areaData, $user)
    {
        if (config('app.client')->name === 'pos') {
            return;
        }

        foreach ($areaData['ticketTypes']['data'] as $index => &$type) {
            $filteredPrices = [];

            foreach ($type['prices']['data'] as $price) {
                switch ($price['type']) {
                    case 'MEMBER':
                        if ($this->checkMembership($user, $price['id'])) {
                            array_push($filteredPrices, $price);
                        }
                        break;
                    case 'JUNIOR':
                        if ($this->checkAge($user, $price['age'])) {
                            array_push($filteredPrices, $price);
                        }
                        break;
                    case 'SENIOR':
                        if ($this->checkAge($user, $price['age'], false)) {
                            array_push($filteredPrices, $price);
                        }
                        break;
                    default:
                        array_push($filteredPrices, $price);
                }
            }

            if (count($filteredPrices) > 0) {
                $type['prices']['data'] = $filteredPrices;
            } else {
                unset($areaData['ticketTypes']['data'][$index]);
            }
        }

        $areaData['ticketTypes']['data'] = array_values($areaData['ticketTypes']['data']);
    }

    /**
     * Get german card id (29/39) of ticket holder, if has one
     *
     * @param Ticket $ticket
     * @return string
     */
    public function getGermanCardId(Ticket $ticket)
    {
        $fieldName = 'Fischereischein-Nummer';
        $licenses = $ticket->user->licenses;

        /** @var License $license */
        foreach ($licenses as $license) {
            if ($license->type->id === 29) {
                return $license->fields[$fieldName];
            } elseif ($license->type->id === 39) {
                return $license->fields[$fieldName] . ' (Gast)';
            }
        }

        return '';
    }

    /**
     * Get issuing authority field (29/39) of ticket holder, if has one
     *
     * @param Ticket $ticket
     * @return string
     */
    public function getIssuingAuthority(Ticket $ticket)
    {
        $licenses = $ticket->user->licenses;

        /** @var License $license */
        foreach ($licenses as $license) {
            if ($license->type->id === 29) {
                foreach ($license->fields as $field => $value) {
                    if (starts_with($field, 'Ausstellende')) {
                        return $value;
                    }
                }
            } elseif ($license->type->id === 39) {
                foreach ($license->fields as $field => $value) {
                    if (starts_with($field, 'Ausstellende')) {
                        return $value . ' (Gast)';
                    }
                }
            }
        }

        return '';
    }

    private function checkMembership($user, $priceId)
    {
        if (!$user) {
            return false;
        }

        if (!$price = TicketPrice::find($priceId)) {
            return false;
        }

        $priceOrgs = $price->organizations()->pluck('organizations.id')->toArray();
        $userOrgs = $user->memberships()->pluck('organization_id')->toArray();

        return count(array_intersect($userOrgs, $priceOrgs)) > 0;
    }

    private function checkAge($user, $age, $junior = true)
    {
        if (!$user || !$user->birthday) {
            return true;
        }

        if ($junior) {
            return $user->birthday->age <= $age;
        } else {
            return $user->birthday->age >= $age;
        }
    }
}
