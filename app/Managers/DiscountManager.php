<?php

namespace App\Managers;

use App\Models\Product\ProductPrice;
use App\Models\Product\TimeDiscount;
use Illuminate\Support\Collection;

class DiscountManager
{
    public function discountedPrice(ProductPrice $price, int $timeCount)
    {
        $dailyPrices = array_fill(0, $timeCount, $price->value_origin);
        /** @var Collection|TimeDiscount[] $timeDiscounts */
        $timeDiscounts = $price->timeDiscounts()
            ->where('count', '<=', $timeCount)
            ->orderBy('count', 'desc')
            ->get();

        $minCount = $timeDiscounts->min('count');
        $lastCount = 0;

        foreach ($timeDiscounts as $index => $timeDiscount) {
            $rangeStart = $timeDiscount->inclusive ? 0 : $minCount - 1;
            $rangeEnd = $index === 0 ? count($dailyPrices) - 1 : $lastCount - 2;

            foreach (range($rangeStart, $rangeEnd) as $index) {
                $dailyPrices[$index] = $price->value_origin - $timeDiscount->discount;
            }

            if ($timeDiscount->inclusive) {
                break;
            }

            $lastCount = $timeDiscount->count;
        }

        return array_sum($dailyPrices);
    }
}