<?php

namespace App\Managers;

use App\Models\Promo\Coupon;

class CouponManager
{
    public function generateCode()
    {
        do {
            $code = str_random_formatted(12, 3);
        } while (Coupon::where('code', $code)->exists());

        return $code;
    }
}
