<?php

namespace App\CashRegister\Offisy;

use App\CashRegister\Offisy\Exceptions\AuthFailedException;
use App\CashRegister\Offisy\Exceptions\BadArgumentException;
use App\CashRegister\Offisy\Exceptions\RequestFailedException;
use Carbon\Carbon;
use Config;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Http\Adapter\Guzzle6\Client as AdapterClient;
use Namshi\JOSE\SimpleJWS;
use Validator;
use WoohooLabs\Yang\JsonApi\Client\JsonApiClient;
use WoohooLabs\Yang\JsonApi\Request\JsonApiRequestBuilder;
use WoohooLabs\Yang\JsonApi\Response\JsonApiResponse;
use WoohooLabs\Yang\JsonApi\Schema\ResourceObject;

class Client
{
    private static $vatCountries = ['AT', 'DE'];

    /**
     * Client for making 'normal' requests
     *
     * @var GuzzleClient
     */
    private $guzzleClient;

    /**
     * Client for making JSON:API requests
     *
     * @var JsonApiClient
     */
    private $apiClient;

    /**
     * Current auth token
     *
     * @var string
     */
    private $authToken;

    /**
     * Constructor
     *
     * Setting up http client and getting auth session token
     */
    public function __construct()
    {
        $this->guzzleClient = $client = new GuzzleClient();
        $this->apiClient = new JsonApiClient(new AdapterClient($client));
        $this->authToken = $this->getAuthToken();
    }

    /**
     * Get a new session token from API
     *
     * @return string
     * @throws AuthFailedException
     */
    public function getAuthToken(): string
    {
        $loginToken = $this->generateLoginToken();

        $authResponse = $this->guzzleClient->request('POST', Config::get('services.offisy.uri.auth'), [
            'form_params' => [
                'grant_type' => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
                'assertion' => $loginToken,
            ],
        ]);

        if (!($authResponse->getStatusCode() === 200)) {
            throw new AuthFailedException();
        }

        $response = json_decode($authResponse->getBody()->getContents());
        return $response->access_token;
    }

    /**
     * Generate token for API login
     *
     * @return string
     */
    private function generateLoginToken(): string
    {
        $key = openssl_pkey_get_private('file://' . Config::get('services.offisy.pem'));

        $jws = new SimpleJWS(['alg' => 'RS256']);
        $jws->setPayload([
            'iss' => Config::get('services.offisy.iss'),
            'sub' => Config::get('services.offisy.sub'),
            'aud' => Config::get('services.offisy.aud'),
            'exp' => Carbon::now()
                ->addMinutes(Config::get('services.offisy.ttl'))
                ->timestamp,
        ]);
        $jws->sign($key);

        return $jws->getTokenString();
    }

    /**
     * Send an api request and transform result as json api
     *
     * @param string $method
     * @param string $path
     * @param array|null $data
     * @return JsonApiResponse
     */
    private function sendJsonApiRequest(string $method, string $path, array $data = null): JsonApiResponse
    {
        $builder = $this->getRequestBuilder()
            ->setMethod($method)
            ->setUriPath($path);

        if ($data) {
            $builder->setJsonApiBody(['data' => $data]);
        }

        return $this->apiClient->sendRequest($builder->getRequest());
    }

    /**
     * Send an api request with no result transformation
     *
     * @param string $method
     * @param string $path
     * @param array $data
     * @return GuzzleResponse
     */
    private function sendRequest(string $method, string $path, array $data = null): GuzzleResponse
    {
        $builder = $this->getRequestBuilder()
            ->setMethod($method)
            ->setUriPath($path);

        if ($data) {
            $builder->setJsonApiBody(['data' => $data]);
        }

        return $this->guzzleClient->send($builder->getRequest());
    }

    /**
     * Create a new request builder with
     *
     * @return JsonApiRequestBuilder
     */
    private function getRequestBuilder(): JsonApiRequestBuilder
    {
        return (new JsonApiRequestBuilder(new GuzzleRequest('', '')))
            ->setUri(Config::get('services.offisy.uri.api'))
            ->setHeader('Content-Type', 'application/vnd.api+json')
            ->setHeader('Authorization', 'Bearer ' . $this->authToken);
    }

    /**
     * Get the available VATs of the country
     *
     * @param string $country
     * @return null|ResourceObject|ResourceObject[]
     * @throws BadArgumentException
     * @throws RequestFailedException
     */
    public function getCountryTaxes($country)
    {
        if (Validator::make(compact('country'), [
            'country' => 'required|in:' . join(',', self::$vatCountries),
        ])->fails()
        ) {
            throw new BadArgumentException();
        };

        $response = $this->sendJsonApiRequest('GET', "/vats/{$country}");

        if (!$response->isSuccessfulDocument([200])) {
            throw new RequestFailedException('FAILED_FETCH_VATS', $response->getStatusCode());
        }

        return $response->document()->primaryResources();
    }

    /**
     * Create a new company
     * wit attributes:
     * - name
     * - street
     * - zip
     * - city
     * - country (alpha-2)
     *
     * @param array $attributes
     * @return ResourceObject
     * @throws BadArgumentException
     * @throws RequestFailedException
     */
    public function createCompany(array $attributes)
    {
        if (Validator::make($attributes, [
            'name' => 'required|string',
            'street' => 'required|string',
            'zip' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|string|in:AT,DE',
        ])->fails()
        ) {
            throw new BadArgumentException();
        };

        $response = $this->sendJsonApiRequest('POST', '/companies', [
            'type' => 'companies',
            'attributes' => $attributes,
        ]);

        if (!$response->isSuccessfulDocument([201])) {
            throw new RequestFailedException('FAILED_CREATE_COMPANY', $response->getStatusCode());
        }

        return $response->document()->primaryResource();
    }

    /**
     * Create a offisy user for the specified company
     *
     * @param array $attributes
     * @return null|ResourceObject
     * @throws BadArgumentException
     * @throws RequestFailedException
     */
    public function createUserForCompany(array $attributes)
    {
        if (Validator::make($attributes, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string',
            'title' => 'string',
            'description' => 'string',
            'company_id' => 'required|string',
        ])->fails()
        ) {
            throw new BadArgumentException();
        }

        $response = $this->sendJsonApiRequest('POST', '/users', [
            'type' => 'users',
            'attributes' => collect($attributes)
                ->except('company_id')
                ->toArray(),
            'relationships' => [
                'companies' => [
                    'data' => [
                        'type' => 'companies',
                        'id' => $attributes['company_id'],
                    ],
                ],
            ],
        ]);

        if (!$response->isSuccessfulDocument([201])) {
            throw new RequestFailedException('FAILED_CREATE_USER', $response->getStatusCode());
        }

        return $response->document()->primaryResource();
    }

    /**
     * Get the companies users
     *
     * @param string $company_id
     * @return ResourceObject[]
     * @throws RequestFailedException
     */
    public function getUsersForCompany($company_id)
    {
        $response = $this->sendJsonApiRequest('GET', "/companies/{$company_id}/users");

        if (!$response->isSuccessfulDocument([200])) {
            throw new RequestFailedException('FAILED_FETCH_COMPANY_USERS', $response->getStatusCode());
        }

        return $response->document()->primaryResources();
    }

    /**
     * Create a new receipt
     * wit attributes:
     * - ticket_identifier
     * - ticket_type_id
     * - ticket_type_name
     * - ticket_price_id
     * - ticket_price_name
     * - area_name
     * - vat_id
     * - ticket_price_net
     * - company_id
     *
     * @param array $attributes
     * @return mixed
     * @throws BadArgumentException
     * @throws RequestFailedException
     */
    public function createReceipt(array $attributes)
    {
        if (Validator::make($attributes, [
            'ticket_identifier' => 'required|string',
            'ticket_type_id' => 'required|integer',
            'ticket_type_name' => 'required|string',
            'ticket_price_id' => 'required|integer',
            'ticket_price_name' => 'required|string',
            'area_name' => 'required|string',
            'vat_id' => 'required|string',
            'ticket_price_net' => 'required|numeric',
            'company_id' => 'required|string',
            'storno_reason' => 'string',
        ])->fails()
        ) {
            throw new BadArgumentException();
        }

        $response = $this->sendJsonApiRequest('POST', '/receipts', [
            'type' => 'receipts',
            'attributes' => [
                'type' => 'cash',
                'payment_type' => 'cash',
                'internal_id' => $attributes['ticket_identifier'],
                'storno_reason' => $attributes['storno_reason'] ?? '',
                'positions' => [
                    [
                        'name' =>
                            $attributes['area_name'] . ' - ' .
                            $attributes['ticket_type_name'] .
                            ' (' . $attributes['ticket_price_name'] . ')',
                        'quantity' => 1,
                        'net_per_item' => $attributes['ticket_price_net'],
                        'vat' => $attributes['vat_id'] ?? '',
                        'internal_number' =>
                            'tt' . $attributes['ticket_type_id'] .
                            '-tp' . $attributes['ticket_price_id'],
                    ],
                ],
            ],
            'relationships' => [
                'companies' => [
                    'data' => [
                        'type' => 'companies',
                        'id' => $attributes['company_id'],
                    ],
                ],
            ],
        ]);

        if (!$response->isSuccessfulDocument([201])) {
            throw new RequestFailedException('FAILED_CREATE_RECEIPT', $response->getStatusCode());
        }

        return $response->document()->primaryResource();
    }

    /**
     * Get an existing receipt
     *
     * @param string $receipt_id
     * @return mixed
     * @throws RequestFailedException
     */
    public function getStoredReceipt($receipt_id)
    {
        $response = $this->sendJsonApiRequest('GET', "/receipts/{$receipt_id}");

        if (!$response->isSuccessfulDocument([200])) {
            throw new RequestFailedException('FAILED_GET_STORED_RECEIPT', $response->getStatusCode());
        }

        return $response->document()->primaryResource();
    }

    /**
     * Ping offisy API / HVM status
     *
     * @return bool
     */
    public function ping()
    {
        $response = $this->sendRequest('GET', '/ping');
        $status = json_decode($response->getBody()->getContents());

        return $status;
    }
}