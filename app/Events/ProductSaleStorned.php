<?php

namespace App\Events;

use App\Models\Product\ProductSale;
use Illuminate\Queue\SerializesModels;

class ProductSaleStorned extends Event
{
    use SerializesModels;

    public $productSale;
    public $stornoReason;

    /**
     * Create a new event instance
     * 
     * @param ProductSale $sale
     * @param             $stornoReason
     */
    public function __construct(ProductSale $sale, $stornoReason)
    {
        $this->productSale = $sale;
        $this->stornoReason = $stornoReason;
    }
}
