<?php

namespace App\Events;

use App\Models\Ticket\Ticket;

class TicketStorned extends Event
{
    public $ticket;

    /**
     * Create a new event instance.
     * @param $ticket
     */
    public function __construct(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }
}
