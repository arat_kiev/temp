<?php

namespace App\Events;

use App\Models\Client;
use App\Models\Promo\Coupon;
use App\Models\User;
use Illuminate\Queue\SerializesModels;

class CouponUsed extends Event
{
    use SerializesModels;

    /** @var User */
    public $user;

    /** @var Coupon */
    public $coupon;

    /** @var Client */
    public $client;

    /**
     * Create a new event instance.
     * @param User   $user
     * @param Coupon $coupon
     * @param Client $client
     */
    public function __construct(User $user, Coupon $coupon, Client $client)
    {
        $this->user = $user;
        $this->coupon = $coupon;
        $this->client = $client;
    }
}
