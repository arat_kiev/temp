<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Client;
class UserChangeEmailEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $request;


    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Client $client
     * @param Request $request
     */
    public function __construct(User $user, Client $client, Request $request)
    {
        $this->user = $user;
        $this->client = $client;
        $this->request = $request;
    }
}
