<?php

namespace App\Events;

use App\Models\Client;
use App\Models\License\License;
use Illuminate\Queue\SerializesModels;

class LicenseChanged extends Event
{
    use SerializesModels;

    /** @var License */
    public $license;

    /** @var Client */
    public $client;

    /**
     * Create a new event instance.
     *
     * @param License $license
     * @param Client $client
     */
    public function __construct(License $license, Client $client)
    {
        $this->license = $license;
        $this->client = $client;
    }
}
