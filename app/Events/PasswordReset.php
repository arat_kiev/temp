<?php

namespace App\Events;

use App\Models\Client;
use App\Models\User;
use Illuminate\Queue\SerializesModels;

class PasswordReset extends Event
{
    use SerializesModels;

    /** @var User */
    public $user;

    /** @var Client */
    public $client;

    /** @var string */
    public $token;

    /**
     * Create a new event instance.
     * @param User $user
     * @param string $token
     * @param Client $client
     */
    public function __construct(User $user, $token, Client $client)
    {
        $this->user = $user;
        $this->client = $client;
        $this->token = $token;
    }
}
