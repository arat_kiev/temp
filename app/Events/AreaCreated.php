<?php

namespace App\Events;

use App\Models\Area\Area;
use App\Models\User;
use Illuminate\Queue\SerializesModels;

class AreaCreated extends Event
{
    use SerializesModels;

    /** @var Area */
    public $area;

    /** @var User */
    public $user;

    /**
     * Create a new event instance.
     * @param Area $area
     * @param User $user
     */
    public function __construct(Area $area, User $user)
    {
        $this->area = $area;
        $this->user = $user;
    }
}
