<?php

namespace App\Events;

use App\Models\User;
use App\Models\Client;
use Illuminate\Queue\SerializesModels;

class UserRegistered extends Event
{
    use SerializesModels;

    /** @var User */
    public $user;

    /** @var Client */
    public $client;

    /**
     * Create a new event instance.
     * @param User $user
     * @param Client $client
     */
    public function __construct(User $user, Client $client)
    {
        $this->user = $user;
        $this->client = $client;
    }
}
