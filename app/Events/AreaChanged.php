<?php

namespace App\Events;

use App\Listeners\AreaTags;
use App\Models\Area\Area;
use App\Models\Location\City;
use Illuminate\Queue\SerializesModels;

class AreaChanged extends Event
{
    use SerializesModels;

    /** @var Area */
    public $area;

    /** @var int */
    public $primCityId;

    /** @var array|AreaTags[] */
    public $tags;

    /**
     * Create a new event instance.
     * @param Area $area
     * @param int $primCityId
     * @param array $tags
     */
    public function __construct(Area $area, $primCityId, $tags)
    {
        $this->area = $area;
        $this->primCityId = $primCityId;
        $this->tags = $tags;
    }
}
