<?php

namespace App\Events;

use App\Models\Product\ProductSale;

class ProductSaleStatusChanged extends Event
{
    public $productSale;
    public $status;

    /**
     * Create a new event instance
     *
     * @param ProductSale $sale
     * @param             $status
     */
    public function __construct(ProductSale $sale, $status)
    {
        $this->productSale = $sale;
        $this->status = $status;
    }
}
