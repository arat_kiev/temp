<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class TicketPriceCreated extends Event
{
    use SerializesModels;

    public $ticketPrice;

    /**
     * Create a new event instance.
     * @param $ticketPrice
     */
    public function __construct($ticketPrice)
    {
        $this->ticketPrice = $ticketPrice;
    }
}
