<?php

namespace App\Events;

use App\Models\Client;
use App\Models\Product\ProductSale;
use Illuminate\Queue\SerializesModels;

class ProductSaleCreated extends Event
{
    use SerializesModels;

    /** @var ProductSale */
    public $sale;

    /** @var Client */
    public $client;

    /**
     * Create a new event instance.
     * @param $sale
     * @param Client $client
     */
    public function __construct(ProductSale $sale, Client $client)
    {
        $this->sale = $sale;
        $this->client = $client;
    }
}
