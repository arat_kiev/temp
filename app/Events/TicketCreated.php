<?php

namespace App\Events;

use App\Models\Client;
use Illuminate\Queue\SerializesModels;

class TicketCreated extends Event
{
    use SerializesModels;

    public $ticket;

    /** @var Client */
    public $client;

    /**
     * Create a new event instance.
     * @param $ticket
     * @param Client $client
     */
    public function __construct($ticket, Client $client)
    {
        $this->ticket = $ticket;
        $this->client = $client;
    }
}
