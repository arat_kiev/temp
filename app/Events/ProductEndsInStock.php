<?php

namespace App\Events;

use App\Models\Product\ProductStock;
use Illuminate\Queue\SerializesModels;

class ProductEndsInStock extends Event
{
    use SerializesModels;

    /** @var ProductStock */
    public $stock;

    /**
     * Create a new event instance.
     * @param ProductStock $stock
     */
    public function __construct(ProductStock $stock)
    {
        $this->stock = $stock;
    }
}
