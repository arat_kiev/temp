<?php

namespace App\Blacklist;

use Carbon\Carbon;
use DB;

class Blacklist
{
    private $blacklistRepository;

    /**
     * Blacklist constructor.
     * @param BlacklistModel $blacklistRepository
     */
    public function __construct(BlacklistModel $blacklistRepository)
    {
        $this->blacklistRepository = $blacklistRepository;
    }


    /**
     * Update / create a record
     *
     * @param $user
     * @return bool
     */
    public function InsertIntoBlacklist(array $user)
    {
        !empty($user['id'])
            ? $this->blacklistRepository->findOrFail($user['id'])->update($user)
            : $this->blacklistRepository->create($user);
            return true;
    }

    /**
     * Remove record by id
     *
     * @param $record_id
     * @return bool
     */
    public function RemoveFromBlacklist($record_id)
    {
        $this->blacklistRepository->where('id', $record_id)->delete();
            return true;
    }

    /**
     * Check record existence by first_name, last_name and birthday
     *
     * @param array $user
     * @param bool $banned_till
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function isUserOnBlacklist(array $user, $banned_till = true)
    {
        $banned_till = $banned_till ? '(`banned_till` IS NULL OR `banned_till` > NOW())' : 1;

        $blocked = $this->blacklistRepository
                    ->where('first_name', $user['first_name'])
                    ->where('last_name', $user['last_name'])
                    ->where('birthday', Carbon::parse($user['birthday'])->toDateString())
                    ->whereRaw($banned_till)
                    ->first();
            return $blocked;
    }

}
