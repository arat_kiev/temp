<?php

namespace App\Blacklist;

use App\Models\BaseModel;
use App\Models\User;
use App\Models\Contact\Manager;
use App\Models\Location\Country;
use Carbon\Carbon;

class BlacklistModel extends BaseModel
{
    protected $table = 'blacklist';

    public $timestamps  = false;

    protected $fillable = ['manager_id', 'user_id', 'first_name', 'last_name', 'birthday', 'country_id', 'street', 'post_code', 'city', 'country_id', 'reason', 'banned_till'];

    /**
     * Get related manager
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo(Manager::class);
    }

    /**
     * Get related user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get related country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Setter for banned_till attribute
     *
     * @param $value
     */
    public function setBannedTillAttribute($value)
    {
        $this->attributes['banned_till']  = !empty($value)
            ? Carbon::parse($value . ' 23:59:59')->toDateTimeString()
            : null;
    }

    /**
     * Setter for birthday attribute
     *
     * @param $value
     */
    public function setBirthdayAttribute($value)
    {
        if($value) {
            $this->attributes['birthday'] = Carbon::parse($value)->toDateString();
        }
    }

    /**
     * Setter for country_id attribute
     *
     * @param $value
     */
    public function setCountryIdAttribute($value)
    {
        $this->attributes['country_id'] = empty($value) ? null : $value;
    }

}