<?php

namespace App\Blacklist;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class BlacklistServiceProvider extends LaravelServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Blacklist::class, function () {
            return new Blacklist();
        });
    }

}
