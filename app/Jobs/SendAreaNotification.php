<?php

namespace App\Jobs;

use App\Events\AreaCreated;
use App\Models\Area\Area;
use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class SendAreaNotification extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var User */
    private $user;

    /** @var Area */
    private $area;

    /**
     * Create a new job instance.
     *
     * @param AreaCreated $event
     */
    public function __construct(AreaCreated $event)
    {
        parent::__construct();

        $this->user = $event->user;
        $this->area = $event->area;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $mailer = app('mailer');

        // Send mail
        $mailer->send('system.email.new_area', ['area' => $this->area, 'user' => $this->user], function ($message) {
            $message
                ->from('no-reply@hejfish.com', 'hejfish')
                ->to('info@hejfish.com', 'hejfish')
                ->subject('Neues Gewässer erstellt');
        });

        Log::info('area notification e-mail sent', [
            'area' => $this->area->id,
            'user' => $this->user->id,
        ]);
    }
}
