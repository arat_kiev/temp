<?php

namespace App\Jobs;

use App\Events\AreaChanged;
use App\Managers\AreaManager;
use App\Models\Area\Area;
use App\Models\Area\AreaTag;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateAreaTags extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var Area */
    private $area;

    /** @var array|AreaTag[] */
    private $tags;

    /** @var bool */
    private $clearCustomTags;

    /**
     * Create a new job instance.
     * @param AreaChanged $event
     */
    public function __construct(AreaChanged $event, $clearCustomTags = true)
    {
        parent::__construct();

        $this->area = $event->area;
        $this->tags = $event->tags;
        $this->clearCustomTags = $clearCustomTags;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        AreaManager::generateSearchTags($this->area, $this->tags, $this->clearCustomTags);
    }
}
