<?php

namespace App\Jobs;

use App\Events\ProductEndsInStock;
use App\Models\Product\ProductStock;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Message;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendProductEndsInStockEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var ProductStock */
    private $productStock;

    /**
     * Create a new job instance.
     * @param ProductEndsInStock $event
     */
    public function __construct(ProductEndsInStock $event)
    {
        parent::__construct();

        $this->productStock = $event->stock;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $mailer = app('mailer');

        info('product ends in stock created', [
            'product'   => $this->productStock->product->id,
            'stock'     => $this->productStock->stock->id,
        ]);
        
        // Get template
        $template = 'bissanzeiger.email.product_ends';
        $templateData = [
            'productId'     => $this->productStock->product->id,
            'productName'   => $this->productStock->product->name,
            'stockId'       => $this->productStock->stock->id,
            'stockName'     => $this->productStock->stock->name,
        ];

        // Send notification email
        $mailer->send($template, $templateData, function ($message) use ($templateData) {
            $receivers = $this->productStock->notificatableUsers->pluck('email')->toArray();

            /** @var Message $message */
            $message
                ->to($receivers)
                ->from(env('EMAIL_NOREPLY', 'no-reply@hejfish.com'), 'hejfish')
                ->subject(trans("email.product.ends.subject", $templateData));
        });

        info('product ends in stock notified', [
            'product'   => $this->productStock->product->id,
            'stock'     => $this->productStock->stock->id,
        ]);
    }
}
