<?php

namespace App\Jobs;

use App\Events\AreaChanged;
use App\Managers\AreaManager;
use App\Models\Area\Area;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AssignCitiesToArea extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var Area */
    private $area;

    /** @var int */
    private $primCityId;

    /**
     * Create a new job instance.
     * @param AreaChanged $event
     */
    public function __construct(AreaChanged $event)
    {
        parent::__construct();

        $this->area = $event->area;
        $this->primCityId = $event->primCityId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        info('handler: area-changed', ['area' => $this->area->id]);
        AreaManager::assignCities($this->area, $this->primCityId);
    }
}
