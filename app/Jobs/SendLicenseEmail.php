<?php

namespace App\Jobs;

use App\Events\LicenseChanged;
use App\Models\Client;
use App\Models\License\License;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class SendLicenseEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var License */
    private $license;

    /** @var Client */
    private $client;

    /**
     * Create a new job instance.
     *
     * @param LicenseChanged $event
     */
    public function __construct(LicenseChanged $event)
    {
        parent::__construct();

        $this->license = $event->license;
        $this->client = $event->client;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $mailer = app('mailer');

        Log::info('license e-mail start', [
            'license' => $this->license,
            'user' => $this->license->user,
            'status' => $this->license->status
        ]);

        if ($this->license->status === 'PENDING') {
            $template = 'license_pending';
            $receiver = 'info@hejfish.com';
            $receiver_name = 'Bissanzeiger';
        } else {
            $template = $this->license->status === 'ACCEPTED' ? 'license_accepted' : 'license_rejected';
            $receiver = $this->license->user->email;
            $receiver_name = $this->license->user->full_name;
        }

        // Get client template
        $template = $this->client->templates['email'][$template];

        // Send mail
        $mailer->send($template, ['license' => $this->license], function ($message) use ($receiver, $receiver_name) {
            $message
                ->from('no-reply@hejfish.com', 'hejfish')
                ->to($receiver, $receiver_name)
                ->subject(trans('email.license.'.strtolower($this->license->status).'.subject'));

            if ($this->license->status === 'PENDING') {
                foreach ($this->license->attachments as $attachment) {
                    $message->attach($attachment->fileWithFullPath);
                }
            }
        });

        Log::info('license e-mail sent', [
            'license' => $this->license,
            'user' => $this->license->user,
            'status' => $this->license->status
        ]);
    }
}
