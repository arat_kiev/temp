<?php

namespace App\Jobs;

use App\Events\TicketCreated;
use App\Models\Client;
use App\Models\Ticket\Ticket;
use App\Managers\TicketManager;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Message;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SnappyPDF;
use App;

class SendTicketEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var Ticket */
    private $ticket;

    /** @var Client */
    private $client;

    /**
     * Create a new job instance.
     * @param TicketCreated $event
     */
    public function __construct(TicketCreated $event)
    {
        parent::__construct();

        $this->ticket = $event->ticket;
        $this->client = $event->client;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $mailer = app('mailer');

        // Generate PDF
        $pdf = SnappyPDF::loadFile(route("pdf.ticket", ['ticket' => $this->ticket->id]));
        $pdf->setOptions([
            'custom-header' => [
                'X-Api-Key'     => $this->client->api_key,
            ],
            'margin-left'   => 5,
            'margin-top'    => 5,
            'margin-right'  => 5,
            'margin-bottom' => 5,
        ]);

        // Get client template
        $template = isset($this->client->templates['email']['ticket'])
            ? $this->client->templates['email']['ticket']
            : 'bissanzeiger.email.ticket';

        // Send email with ticket attached
        $mailer->send($template, ['user' => $this->ticket->user], function ($message) use ($pdf) {
            /** @var Message $message */
            $message
                ->to($this->ticket->user->email, $this->ticket->user->full_name)
                ->from('no-reply@hejfish.com', 'hejfish')
                ->subject(trans("email.ticket.subject"));

            $message->attachData(
                $pdf->output(),
                trans("email.ticket.attach").'.pdf',
                ['mime' => 'application/pdf']
            );

            $this->attachRules($message);

            $this->attachStateRules($message);
        });

        info('invoice e-mail sent', [
            'type'      => $this->ticket->type->id,
            'ticket'    => $this->ticket->id,
            'user'      => $this->ticket->user->id,
        ]);
    }
    
    private function attachRules($message)
    {
        if ($this->ticket->type->ruleWithFallback) {
            foreach ($this->ticket->type->ruleWithFallback->files as $i => $attachment) {
                \Log::debug('e-mail: attaching file: '.$attachment);
                $fileName = $attachment->name ?: 'Bestimmungen-'.$i;
                $message->attach($attachment->fileWithFullPath, [
                    'as' => $fileName.'.'.\File::extension($attachment->file)
                ]);
            }
        }
    }

    private function attachStateRules($message)
    {
        $this->ticket->type->area->states->each(function ($state) use ($message) {
            if ($state->rule) {
                foreach ($state->rule->files as $i => $attachment) {
                    \Log::debug('e-mail: attaching file: '.$attachment);
                    $fileName = $attachment->name ?: 'Bestimmungen-'.$i;
                    $message->attach($attachment->fileWithFullPath, [
                        'as' => $fileName.'.'.\File::extension($attachment->file)
                    ]);
                }
            }
        });
    }
}
