<?php

namespace App\Jobs;

use App\Events\ProductSaleStatusChanged;
use App\Models\Product\ProductSale;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class SendProductSaleStatusChangedEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var ProductSale */
    private $productSale;
    private $status;

    /**
     * Create a new job instance
     *
     * @param ProductSaleStatusChanged $event
     */
    public function __construct(ProductSaleStatusChanged $event)
    {
        parent::__construct();

        $this->productSale = $event->productSale;
        $this->status = $event->status;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $mailer = app('mailer');

        Log::info('product_sale_status_changed e-mail start', [
            'sale'      => $this->productSale,
            'status'    => $this->status,
        ]);

        $template = 'system.email.product_sale.status';
        $mailData = [
            'sale'      => $this->productSale,
            'status'    => $this->status,
        ];

        // Send mail
        $mailer->send($template, $mailData, function ($message) {
            $sender = env('EMAIL_NOREPLY', 'no-reply@hejfish.com');
            $receiver = $this->productSale->user
                ? $this->productSale->user->email
                : $this->productSale->email;
            $receiverName = $this->productSale->user
                ? $this->productSale->user->first_name . ' ' . $this->productSale->user->last_name
                : $this->productSale->first_name . ' ' . $this->productSale->last_name;

            $message
                ->from($sender, 'hejfish | Komm Ans Wasser')
                ->to($receiver, $receiverName)
                ->subject('hejfish | Update zu Deiner Bestellung #' . $this->productSale->id . ': Neuer Status');
        });

        Log::info('product_sale_status_changed e-mail sent', [
            'sale'      => $this->productSale,
            'status'    => $this->status,
        ]);
    }
}


