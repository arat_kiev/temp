<?php

namespace App\Jobs;

use App\Events\UserActivated;
use App\Managers\UserManager;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ConnectUserToMember extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $user;

    /**
     * Create new job instance
     *
     * @param UserActivated $event
     */
    public function __construct(UserActivated $event)
    {
        $this->user = $event->user;
    }

    public function handle()
    {
        UserManager::assignUserToMember($this->user);
    }
}
