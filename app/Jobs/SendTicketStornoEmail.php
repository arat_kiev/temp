<?php

namespace App\Jobs;

use App\Events\TicketStorned;
use App\Models\Ticket\Ticket;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Message;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendTicketStornoEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var Ticket */
    private $ticket;

    /**
     * Create a new job instance.
     * @param TicketStorned $event
     */
    public function __construct(TicketStorned $event)
    {
        parent::__construct();

        $this->ticket = $event->ticket;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        if ($emails = $this->ticket->type->area->manager->stornoNotifications()->pluck('email')->toArray()) {
            $this->sendNotificationEmails($emails);
        }
    }

    private function sendNotificationEmails(array $emails)
    {
        $mailer = app('mailer');

        $template = 'bissanzeiger.email.ticket.storno';

        // Send email with ticket attached
        $mailer->send($template, ['ticket' => $this->ticket], function ($message) use ($emails) {
            /** @var Message $message */
            $message
                ->to($emails)
                ->from(config('mail.no_reply_address'), 'hejfish')
                ->subject(trans('email.ticket.storno.subject', ['ticketId' => $this->ticket->id]));
        });

        info('ticket storno e-mail sent', [
            'ticket'    => $this->ticket->id,
            'user'      => $this->ticket->user ? $this->ticket->user->id : null,
        ]);
    }
}
