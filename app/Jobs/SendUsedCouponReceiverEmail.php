<?php

namespace App\Jobs;

use App\Events\CouponUsed;
use App\Models\Client;
use App\Models\User;
use App\Models\Promo\Coupon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Message;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendUsedCouponReceiverEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue,
        SerializesModels;

    /** @var User */
    private $user;

    /** @var Coupon */
    private $coupon;

    /** @var Client */
    private $client;

    /**
     * Create a new job instance.
     * @param CouponUsed $event
     */
    public function __construct(CouponUsed $event)
    {
        parent::__construct();

        $this->user = $event->user;
        $this->coupon = $event->coupon;
        $this->client = $event->client;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $mailer = app('mailer');

        info('coupon used', [
            'user'    => $this->user->id,
            'coupon'  => $this->coupon->id,
            'client'  => $this->client->id,
        ]);
        // Get client template
        $template = 'bissanzeiger.email.coupons.used.receiver';
        $templateData = ['code' => $this->coupon->code, 'bonus' => $this->coupon->usage_bonus];

        // Send email with ticket attached
        $mailer->send($template, $templateData, function ($message) {
            /** @var Message $message */
            $message
                ->to($this->user->email, $this->user->full_name)
                ->from('no-reply@hejfish.com', 'hejfish')
                ->subject(trans("email.coupons.used.receiver.subject"));
        });

        info('coupon used email sent', [
            'user'    => $this->user->id,
            'coupon'  => $this->coupon->id,
            'client'  => $this->client->id,
        ]);
    }
}
