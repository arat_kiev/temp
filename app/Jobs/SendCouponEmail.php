<?php

namespace App\Jobs;

use App\Models\User;
use App\Models\Promo\Coupon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Message;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendCouponEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue,
        SerializesModels;

    /** @var User */
    private $user;

    /** @var Coupon */
    private $coupon;

    /**
     * Create a new job instance.
     * @param User   $user
     * @param Coupon $coupon
     */
    public function __construct(User $user, Coupon $coupon)
    {
        parent::__construct();

        $this->user = $user;
        $this->coupon = $coupon;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $mailer = app('mailer');

        info('coupon sending', [
            'user'    => $this->user->id,
            'coupon'  => $this->coupon->id,
        ]);
        // Get client template
        $template = 'bissanzeiger.email.coupons.coupon';
        $templateData = ['code' => $this->coupon->code, 'bonus' => $this->coupon->usage_bonus];

        // Send email with ticket attached
        $mailer->send($template, $templateData, function ($message) {
            /** @var Message $message */
            $message
                ->to($this->user->email, $this->user->full_name)
                ->from('no-reply@hejfish.com', 'hejfish')
                ->subject(trans("email.coupon.subject"));
        });

        info('coupon sent', [
            'user'    => $this->user->id,
            'coupon'  => $this->coupon->id,
        ]);
    }
}
