<?php

namespace App\Jobs;

use App\Events\ProductSaleCreated;
use App\Models\Client;
use App\Models\Product\ProductSale;
use App\Managers\ProductSaleManager;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Message;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SnappyPDF;
use Log;

class SendProductSaleEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var ProductSale */
    private $sale;

    /** @var Client */
    private $client;

    /**
     * Create a new job instance.
     * @param ProductSaleCreated $event
     */
    public function __construct(ProductSaleCreated $event)
    {
        parent::__construct();

        $this->sale = $event->sale;
        $this->client = $event->client;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $mailer = app('mailer');
        // Generate PDF
        $pdf = SnappyPDF::loadFile(route("pdf.product", ['product' => $this->sale->id]));
        $pdf->setOptions([
            'custom-header' => [
                'X-Api-Key'     => $this->client->api_key,
            ],
            'margin-left'   => 5,
            'margin-top'    => 5,
            'margin-right'  => 5,
            'margin-bottom' => 5,
        ]);

        info('product sale created', [
            'product'   => $this->sale->id,
            'user'      => $this->sale->user ? $this->sale->user->id : null,
        ]);
        // Get client template
        $template = isset($this->client->templates['email']['product'])
            ? $this->client->templates['email']['product']
            : 'bissanzeiger.email.product';
        $templateData = ['code' => app(ProductSaleManager::class)->getIdentifier($this->sale)];

        // Send email with ticket attached
        $mailer->send($template, $templateData, function ($message) use ($templateData, $pdf) {
            $receiver = $this->sale->user ? $this->sale->user->email : $this->sale->email;
            $receiverName = $this->sale->user
                ? $this->sale->user->full_name
                : $this->sale->first_name . ' ' . $this->sale->last_name;

            /** @var Message $message */
            $message
                ->to($receiver, $receiverName)
                ->from(env('EMAIL_NOREPLY', 'no-reply@hejfish.com'), 'hejfish')
                ->subject(trans(
                    "email.product.subject", 
                    ['code' => $templateData['code']]
                ));

            if ($this->sale->product->attachment) {
                $fileName = $this->sale->product->attachment_name;
                Log::debug('e-mail: attaching file: ' . $fileName);
                $message->attach($this->sale->product->attachment_full_path, ['as' => $fileName]);
            }
            $message->attachData(
                $pdf->output(),
                trans("email.product.attach").'.pdf',
                ['mime' => 'application/pdf']
            );

            if ($this->sale->product->gallery && $this->sale->product->gallery->pictures()->count()) {
                foreach ($this->sale->product->gallery->pictures as $picture) {
                    $fileName = $picture->file_name;
                    Log::debug('e-mail: attaching file: ' . $fileName);
                    $message->attach($picture->file_with_full_path, ['as' => $picture->file]);
                }
            }
        });

        info('product sale sent', [
            'product'   => $this->sale->id,
            'user'      => $this->sale->user ? $this->sale->user->id : null,
        ]);
    }
}
