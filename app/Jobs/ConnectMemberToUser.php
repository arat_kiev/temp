<?php

namespace App\Jobs;

use App\Events\MemberChanged;
use App\Managers\UserManager;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ConnectMemberToUser extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $member;

    /**
     * Create new job instance
     *
     * @param MemberChanged $event
     */
    public function __construct(MemberChanged $event)
    {
        $this->member = $event->member;
    }

    public function handle()
    {
        UserManager::assignMemberToUser($this->member);
    }
}
