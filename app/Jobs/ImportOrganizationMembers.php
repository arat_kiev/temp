<?php

namespace App\Jobs;

use App\Events\MemberChanged;
use App\Models\Organization\CsvData;
use App\Models\Organization\Organization;
use Event;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportOrganizationMembers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var Organization */
    private $organization;

    /** @var int */
    private $csvFileId;

    /** @var array */
    private $headerFields;

    /**
     * Create a new job instance.
     *
     * @param Organization $organization
     * @param int $csvFileId
     */
    public function __construct(Organization $organization, $csvFileId, $headerFields = [])
    {
        $this->organization = $organization;
        $this->csvFileId = $csvFileId;
        $this->headerFields = $headerFields;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        info('# import of organization started');
        /** @var \Illuminate\Support\Collection $existingMemberIds */
        $existingMemberIds = $this->organization->members()->pluck('member_id');

        $csvImport = CsvData::find($this->csvFileId);
        $csvData = json_decode($csvImport->csv_data, true);

        $progress = 0;
        $lastSavePercent = 0;
        $total = count($csvData);

        info('  - importing ' . $total . ' entries total into organization "' . $this->organization->name . '"');

        foreach ($csvData as $rowId => $row) {
            $memberData = [];

            if (!$csvImport->csv_header) {
                foreach ($this->headerFields as $fieldKey => $field) {
                    $memberData[config('csv-import.db_fields')[$field]] = $row[$fieldKey];
                }
            } else {
                $memberData = $row;
            }

            if (isset($memberData['member_id']) && $existingMemberIds->contains($memberData['member_id'])) {
                $member = $this->organization->members()->where('member_id', $memberData['member_id'])->first();
                $member->update($memberData);
            } else {
                $member = $this->organization->members()->create($memberData);
            }

            Event::fire(new MemberChanged($member));

            if (isset($memberData['member_id'])) {
                $existingMemberIds->push($memberData['member_id']);
            }

            $progress++;
            $progressPercent = ($progress / $total) * 100.0;

            if ($progressPercent - $lastSavePercent >= 10.0) {
                $csvImport->update(['progress' => $progressPercent]);
                $lastSavePercent = $progressPercent;
            }
        }

        $csvImport->update(['progress' => 100.0]);
        info('# import of organization finished');
    }
}
