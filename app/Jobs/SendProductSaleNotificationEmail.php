<?php

namespace App\Jobs;

use App\Events\ProductSaleCreated;
use App\Models\Product\ProductSale;
use App\Models\User;
use App\Models\Client;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendProductSaleNotificationEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var Client */
    private $client;

    /** @var ProductSale */
    private $sale;

    /**
     * Create a new job instance.
     *
     * @param ProductSaleCreated $event
     */
    public function __construct(ProductSaleCreated $event)
    {
        parent::__construct();

        $this->sale = $event->sale;
        $this->client = $event->client;
    }

    /**
     * Execute the job
     */
    public function handle()
    {
        $mailer = app('mailer');

        // Get client template
        $template = isset($this->client->templates['email']['notify_users_product'])
            ? $this->client->templates['email']['notify_users_product']
            : 'bissanzeiger.email.product_notify';

        $emails = env('EMAIL_INFO', 'info@hejfish.com');
        $templateData = ['sale' => $this->sale];

        // Send mail
        if ($emails) {
            $mailer->send($template, $templateData, function ($message) use ($emails) {
                $message
                    ->from('no-reply@hejfish.com', 'hejfish')
                    ->to($emails)
                    ->subject('Produkte: ' . $this->sale->product->name . ' wurde bestellt.');
            });
        }
    }
}
