<?php

namespace App\Jobs;

use App\Events\PasswordReset;
use App\Models\Client;
use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class SendPasswordEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var User */
    private $user;

    /** @var Client */
    private $client;

    /** @var string */
    private $token;

    /**
     * Create a new job instance.
     * @param PasswordReset $event
     */
    public function __construct(PasswordReset $event)
    {
        parent::__construct();

        $this->user = $event->user;
        $this->client = $event->client;
        $this->token = $event->token->string;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $mailer = app('mailer');

        // Get client template
        $template = $this->client->templates['email']['password_reset'];

        // Send mail
        $mailer->send($template, ['user' => $this->user, 'token' => $this->token], function ($message) {
            $message
                ->from('no-reply@hejfish.com', 'hejfish')
                ->to($this->user->email)
                ->subject(trans('email.password.subject'));
        });

        Log::info('password reset e-mail sent', [
            'user' => $this->user->id,
            'client' => $this->client->id,
            'token' => $this->token,
        ]);
    }
}
