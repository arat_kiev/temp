<?php

namespace App\Jobs;

use App\Events\TicketPriceCreated;
use App\Managers\CommissionManager;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddDefaultCommissions extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $ticketPrice;

    /**
     * Create a new job instance.
     * @param TicketPriceCreated $event
     */
    public function __construct(TicketPriceCreated $event)
    {
        parent::__construct();

        $this->ticketPrice = $event->ticketPrice;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $commission = new CommissionManager();
        $commission->addDefaultCommissions($this->ticketPrice);
    }
}
