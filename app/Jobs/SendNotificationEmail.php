<?php

namespace App\Jobs;

use App\Events\TicketCreated;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketType;
use App\Models\User;
use App\Models\Client;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNotificationEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var Client */
    private $client;

    /** @var Ticket */
    private $ticket;

    /**
     * Create a new job instance.
     *
     * @param TicketCreated $event
     */
    public function __construct(TicketCreated $event)
    {
        parent::__construct();

        $this->ticket = $event->ticket;
        $this->client = $event->client;
    }

    /**
     * Execute the job
     */
    public function handle()
    {
        $mailer = app('mailer');

        // Get client template
        $template = $this->client->templates['email']['notify_users'];

        $emails = $this->ticket->type->productBasedNotifications()->pluck('email')->toArray();
        $templateData = ['ticket' => $this->ticket, 'user' => $this->ticket->user];

        // Send mail
        if ($emails) {
            $mailer->send($template, $templateData, function ($message) use ($emails) {
                $message
                    ->from('no-reply@hejfish.com', 'hejfish')
                    ->to($emails)
                    ->subject(trans('Produkt: ' . $this->ticket->type->name . ' wurde gekauft.'));
            });
        }
    }
}
