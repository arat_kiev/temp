<?php

namespace App\Jobs;

use App\Events\UserRegistered;
use App\Models\User;
use App\Models\Client;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class SendWelcomeEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var User */
    private $user;

    /** @var Client */
    private $client;

    /**
     * Create a new job instance.
     *
     * @param UserRegistered $event
     */
    public function __construct(UserRegistered $event)
    {
        parent::__construct();

        $this->user = $event->user;
        $this->client = $event->client;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $mailer = app('mailer');

        // Get client template
        $template = $this->client->templates['email']['welcome'];

        // Send mail
        $mailer->send($template, ['user' => $this->user], function ($message) {
            $message
                ->from('no-reply@hejfish.com', 'hejfish')
                ->to($this->user->email, $this->user->name)
                ->subject(trans('email.welcome.subject'));
        });

        Log::info('welcome e-mail sent', [
            'client' => $this->client->id,
            'user' => $this->user->id,
        ]);
    }
}
