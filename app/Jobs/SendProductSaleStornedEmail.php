<?php

namespace App\Jobs;

use App\Events\ProductSaleStorned;
use App\Models\Product\ProductSale;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class SendProductSaleStornedEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var ProductSale */
    private $productSale;
    private $stornoReason;

    /**
     * Create a new job instance
     *
     * @param ProductSaleStorned $event
     */
    public function __construct(ProductSaleStorned $event)
    {
        parent::__construct();

        $this->productSale = $event->productSale;
        $this->stornoReason = $event->stornoReason;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $mailer = app('mailer');

        Log::info('product_sale_storned e-mail start', [
            'sale'      => $this->productSale,
            'reason'    => $this->stornoReason,
        ]);

        $template = 'system.email.product_sale.storned';
        $mailData = [
            'sale'      => $this->productSale,
            'reason'    => $this->stornoReason
        ];

        // Send mail
        $mailer->send($template, $mailData, function ($message) {
            $sender = env('EMAIL_NOREPLY', 'no-reply@hejfish.com');
            $receiver = env('EMAIL_INFO', 'info@hejfish.com');
            $receiverName = 'hejfish Info';

            $message
                ->from($sender, 'hejfish')
                ->to($receiver, $receiverName)
                ->subject('Abfrage um Produkte Verkäufe #' . $this->productSale->id . ' zu stornieren');
        });

        Log::info('product_sale_storned e-mail sent', [
            'sale'      => $this->productSale,
            'reason'    => $this->stornoReason,
        ]);
    }
}
