<?php

namespace App\Image\Templates\Logo;

use Intervention\Image\Constraint;
use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class SmallLogo implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->orientate()->resize(80, 80, function (Constraint $constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
    }
}
