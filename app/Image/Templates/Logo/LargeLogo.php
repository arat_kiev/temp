<?php

namespace App\Image\Templates\Logo;

use Intervention\Image\Constraint;
use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class LargeLogo implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->orientate()->resize(1400, 1400, function (Constraint $constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
    }
}
