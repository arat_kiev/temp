<?php

namespace App\Image\Templates\Logo;

use Intervention\Image\Constraint;
use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class MediumLogo implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->orientate()->resize(160, 160, function (Constraint $constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
    }
}
