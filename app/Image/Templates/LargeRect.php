<?php

namespace App\Image\Templates;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class LargeRect implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->orientate()->fit(800, 600);
    }
}
