<?php

namespace App\Image\Templates\Avatar;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class SmallRect implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->orientate()->fit(120, 100);
    }
}
