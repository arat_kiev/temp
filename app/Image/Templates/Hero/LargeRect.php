<?php

namespace App\Image\Templates\Hero;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class LargeRect implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->orientate()->fit(1920, 360);
    }
}
