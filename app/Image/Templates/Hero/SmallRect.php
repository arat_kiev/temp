<?php

namespace App\Image\Templates\Hero;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class SmallRect implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->orientate()->fit(360, 68);
    }
}
