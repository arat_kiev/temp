<?php

namespace App\Admin\Forms;

use App\Models\Meta\FishCategory;
use App\Models\Promo\Promotion;
use Kris\LaravelFormBuilder\Form;

class PromotionForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('type', 'select', [
                'label'     => 'Typ',
                'choices'   => Promotion::AVAILABLE_TYPES,
                'selected'  => $this->model->type,
            ])
            ->add('valid_from', 'text', [
                'label' => 'Gültig von',
                'value' => $this->model->valid_from
                    ? $this->model->valid_from->format('d.m.Y H:i')
                    : null,
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy hh:mm',
                    'data-provide' => 'datetimepicker',
                ],
            ])
            ->add('valid_till', 'text', [
                'label' => 'Gültig bis',
                'value' => $this->model->valid_till
                    ? $this->model->valid_till->format('d.m.Y H:i')
                    : null,
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy hh:mm',
                    'data-provide' => 'datetimepicker',
                ],
            ])
            ->add('usage_bonus', 'number', [
                'label' => 'Nutzung Bonus (in €)',
                'value' => $this->model->usage_bonus
                    ? number_format($this->model->usage_bonus_origin / 100, 2, '.', '')
                    : '',
                'attr'  => [
                    'min'   => 0,
                    'max'   => '9999.99',
                    'step'  => '0.01',
                ],
            ])
            ->add('owner_bonus', 'number', [
                'label' => 'Gutschein Besitzer Bonus (in €)',
                'value' => $this->model->owner_bonus
                    ? number_format($this->model->owner_bonus_origin / 100, 2, '.', '')
                    : '',
                'attr'  => [
                    'min'   => 0,
                    'max'   => '9999.99',
                    'step'  => '0.01',
                ],
            ])
            ->add('is_active', 'checkbox', [
                'label' => 'Aktiv',
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
