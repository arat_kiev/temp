<?php

namespace App\Admin\Forms;

use App\Models\Area\AreaType;
use Kris\LaravelFormBuilder\Form;

class ParentAreasForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('type', 'entity', [
                'label'         => 'Gewässertyp',
                'class'         => AreaType::class,
                'property'      => 'name',
                'query_builder' => function (AreaType $type) {
                    return $type->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->type ? $this->model->type->id : null;
                },
                'attr'          => [
                    'id'               => 'types',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
