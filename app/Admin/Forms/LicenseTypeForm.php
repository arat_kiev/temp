<?php

namespace App\Admin\Forms;

use App\Models\License\LicenseType;
use Kris\LaravelFormBuilder\Form;

class LicenseTypeForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'label' => 'Titel',
                'attr'  => [
                    'required'      => true,
                ],
            ])
            ->add('fields', 'collection', [
                'label'     => 'Felder',
                'type'      => 'form',
                'data'      => $this->model->fields
                    ? collect(array_map(function ($type, $titel) {
                        return [
                            'name'       => $this->fetchHumanReadableFieldType($type),
                            'field_type' => $type,
                            'field_name' => $titel,
                            'disabled'   => (bool) $this->model->licenses()->count(),
                        ];
                    }, $this->model->fields, array_keys($this->model->fields)))
                    : null,
                'prototype' => true,
                'empty_row' => false,
                'options'   => [
                    'class'     => 'App\Admin\Forms\LicenseTypeFieldForm',
                    'template'  => 'admin.fields.license_type_fields',
                    'label'     => false,
                ],
            ])
            ->add('attachments', 'select', [
                'label'     => 'Anhänge Titel',
                'choices'   => $this->model->attachments
                    ? array_combine($this->model->attachments, $this->model->attachments)
                    : [],
                'selected'  => $this->model->attachments ?? [],
                'attr'      => [
                    'id'                => 'attachments',
                    'multiple'          => true,
                    'data-placeholder'  => 'Anhänge hinzufügen',
                ]
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ]);

        if ($this->model->licenses()->count()) {
            $this->modify('fields', 'collection', [
                'attr' => [
                    'disabled' => true,
                ],
            ]);
            $this->modify('attachments', 'select', [
                'attr' => [
                    'disabled' => true,
                ],
            ]);
        }
    }

    private function fetchHumanReadableFieldType($type)
    {
        $types = array_flip(LicenseType::ALLOWED_FIELD_TYPES);

        return $types[$type] ?? '';
    }
}
