<?php

namespace App\Admin\Forms;

use App\Models\Product\Product;
use App\Models\Product\ProductStock;
use App\Models\Stock;
use Kris\LaravelFormBuilder\Form;

class ProductStockForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('product', 'entity', [
                'label'         => 'Produkt',
                'class'         => Product::class,
                'property'      => 'name',
                'query_builder' => function (Product $product) {
                    return $product->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->product
                        ? $this->model->product->id
                        : ($this->data['productId'] ?? null);
                },
                'attr'          => [
                    'id'               => 'products',
                    'required'         => true,
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('stock', 'entity', [
                'label'         => 'Lager',
                'class'         => Stock::class,
                'property'      => 'name',
                'query_builder' => function (Stock $stock) {
                    return $stock->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->stock
                        ? $this->model->stock->id
                        : ($this->data['stockId'] ?? null);
                },
                'attr'          => [
                    'id'               => 'stocks',
                    'required'         => true,
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('quantity', 'number', [
                'label'         => 'Lagermenge',
                'attr'          => [
                    'min'           => 0,
                    'step'          => 1,
                    'required'      => true,
                ],
            ])
            ->add('delivery_time', 'number', [
                'label'         => 'Lieferzeit',
                'attr'          => [
                    'min'           => 1,
                    'step'          => 1,
                    'required'      => true,
                ],
            ])
            ->add('notification_quantity', 'number', [
                'label'         => 'Benachrichtigungsmenge',
                'attr'          => [
                    'min'   => 0,
                    'step'  => 1,
                ],
            ])
            ->add('unit', 'select', [
                'label'         => 'Einheit',
                'choices'       => ProductStock::AVAILABLE_UNITS,
                'selected'      => $this->model->unit,
                'attr'          => [
                    'id'                => 'units',
                    'data-placeholder'  => 'Einheit wählen',
                ]
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ]
            ]);

        if ($this->model->id) {
            $this->modify('product', 'entity', [
                'attr' => [
                    'disabled' => true,
                ],
            ]);
        }
    }
}
