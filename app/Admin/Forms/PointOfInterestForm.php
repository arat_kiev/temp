<?php

namespace App\Admin\Forms;

use App\Models\Area\Area;
use App\Models\Location\Country;
use App\Models\PointOfInterest\PointOfInterestCategory;
use App\Models\PointOfInterest\PointOfInterestOpeningHour;
use Kris\LaravelFormBuilder\Form;

class PointOfInterestForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'label' => 'Name',
            ])
            ->add('street', 'text', [
                'label' => 'Straße',
            ])
            ->add('post_code', 'text', [
                'label' => 'Postleitzahl',
            ])
            ->add('city', 'text', [
                'label' => 'Stadt',
            ])
            ->add('country', 'entity', [
                'label'         => 'Land',
                'class'         => Country::class,
                'property'      => 'name',
                'query_builder' => function (Country $country) {
                    return $country->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->country ? $this->model->country->id : null;
                },
                'attr'          => [
                    'id'               => 'countries',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('picture', 'file', [
                'label' => 'Neues Bild wählen',
                'value' => '',
            ])
            ->add('email', 'text', [
                'label' => 'Email',
            ])
            ->add('phone', 'text', [
                'label' => 'Telefon',
            ])
            ->add('website', 'text', [
                'label' => 'Website',
            ])
            ->add('opening_hours', 'collection', [
                'label'     => false,
                'class'     => PointOfInterestOpeningHour::class,
                'type'      => 'form',
                'prototype' => true,
                'empty_row' => false,
                //'property' => 'opening_hours',
                //'data' => $this->model->opening_hours,
                'options'   => [
                    'class'     => 'App\Admin\Forms\PointOfInterestOpeningHourForm',
                    'template'  => 'admin.fields.opening_hours',
                    'label'     => false,
                ],
            ])
            ->add('categories', 'entity', [
                'label'         => 'Kategorien',
                'class'         => PointOfInterestCategory::class,
                'property'      => 'name',
                'multiple'      => true,
                'query_builder' => function (PointOfInterestCategory $category) {
                    return $category->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->categories->pluck('id')->toArray();
                },
                'attr'          => [
                    'id'               => 'categories',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('areas', 'entity', [
                'label'         => 'Gewässer',
                'class'         => Area::class,
                'property'      => 'name',
                'multiple'      => true,
                'query_builder' => function (Area $area) {
                    return $area->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->areas->pluck('id')->toArray();
                },
                'attr'          => [
                    'id'               => 'areas',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
