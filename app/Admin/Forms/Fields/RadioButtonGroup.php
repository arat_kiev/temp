<?php

namespace App\Admin\Forms\Fields;

use Kris\LaravelFormBuilder\Fields\FormField;

class RadioButtonGroup extends FormField
{
    protected function getTemplate()
    {
        // At first it tries to load config variable,
        // and if fails falls back to loading view
        // resources/views/fields/datetime.blade.php
        //todo make template
        return 'admin.fields.radiobutton-group';
    }

    public function render(array $options = [], $showLabel = true, $showField = true, $showError = true)
    {
        return parent::render($options, $showLabel, $showField, $showError);
    }
}