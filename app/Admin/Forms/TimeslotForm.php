<?php

namespace App\Admin\Forms;

use App\Models\License\LicenseType;
use App\Models\Ticket\CheckinUnit;
use App\Models\Ticket\QuotaUnit;
use App\Models\Ticket\TicketCategory;
use App\Models\Ticket\TicketDay;
use Kris\LaravelFormBuilder\Form;

class TimeslotForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('id', 'hidden', [
                'label' => false,
                'default_value' => "0",
            ])
            ->add('from', 'time', [
                'label' => false,
                'attr' => [
                    'placeholder' => 'hh:mm',
                ],
            ])
            ->add('till', 'time', [
                'label' => false,
                'attr' => [
                    'placeholder' => 'hh:mm',
                ],
            ])
            ->add('default_pool', 'number', [
                'label' => false,
                'default_value' => "1",
            ]);
    }
}
