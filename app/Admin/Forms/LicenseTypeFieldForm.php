<?php

namespace App\Admin\Forms;

use Kris\LaravelFormBuilder\Form;

class LicenseTypeFieldForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('field_type', 'hidden', [
                'default_value' => '__FIELD_TYPE__',
                'attr'          => [
                    'class'         => 'field-id',
                    'required'      => true,
                ],
            ])
            ->add('name', 'hidden', [
                'default_value' => '__FIELD__',
            ])
            ->add('field_name', 'text', [
                'label'         => 'Feld-Titel',
                'attr'          => [
                    'placeholder'   => 'z.B. Gültig bis',
                    'required'      => true,
                ],
            ])
            ->add('button_remove', 'button', [
                'label'         => '&times;',
                'attr'          => [
                    'class' => 'btn btn-danger field-btn-remove',
                ],
            ]);
    }
}
