<?php

namespace App\Admin\Forms;

use App\Models\License\LicenseType;
use App\Models\Ticket\CheckinUnit;
use App\Models\Ticket\QuotaUnit;
use App\Models\Ticket\TicketCategory;
use Kris\LaravelFormBuilder\Form;
use Carbon\Carbon;

class TicketTypeForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'label' => 'Name',
            ])
            ->add('is_catchlog_required', 'checkbox', [
                'label' => 'Fang ist erforderlich',
            ])
            ->add('category', 'entity', [
                'class'         => TicketCategory::class,
                'property'      => 'name',
                'query_builder' => function (TicketCategory $category) {
                    return $category->listsTranslations('name');
                },
                'selected'      => function () {
                    return $this->model->category ? $this->model->category->id : 0;
                },
                'label'         => 'Kategorie',
            ])
            ->add('duration', 'number', [
                'label' => 'Dauer',
                'value' => $this->model->duration ?: '',
                'attr'  => [
                    'min'   => 1,
                    'max'   => 300,
                ],
            ])
            ->add('group', 'checkbox', [
                'label' => 'Gruppenticket',
            ])
            ->add('day_starts_at_sunrise', 'checkbox', [
                'label' => 'Sonnenaufgang',
            ])
            ->add('day_starts_at_sunset', 'checkbox', [
                'label' => 'Sonnenuntergang',
            ])
            ->add('day_starts_at_time', 'time', [
                'label' => 'Uhrzeit',
                'value' => $this->model->day_starts_at_time
                    ? Carbon::parse($this->model->day_starts_at_time)->format('H:i')
                    : Carbon::now()->startOfDay()->format('H:i'),
                'attr'  => [
                    'placeholder'   => 'hh:mm',
                ],
            ])
            ->add('day_ends_at_sunrise', 'checkbox', [
                'label' => 'Sonnenaufgang',
            ])
            ->add('day_ends_at_sunset', 'checkbox', [
                'label' => 'Sonnenuntergang',
            ])
            ->add('day_ends_at_time', 'time', [
                'label' => 'Uhrzeit',
                'value' => $this->model->day_ends_at_time
                    ? Carbon::parse($this->model->day_ends_at_time)->format('H:i')
                    : Carbon::now()->startOfDay()->format('H:i'),
                'attr' => [
                    'placeholder'   => 'hh:mm',
                ],
            ])
            // Angeltag (Checkins)
            ->add('checkin_id', 'entity', [
                'label'         => 'Zeitraum',
                'class'         => CheckinUnit::class,
                'property'      => 'name',
                'query_builder' => function (CheckinUnit $checkinUnit) {
                    return $checkinUnit->listsTranslations('name');
                },
                'selected'      => function () {
                    return $this->model->checkinUnit ? $this->model->checkinUnit->id : null;
                },
                'empty_value'   => 'Kein Limit',
            ])
            ->add('checkin_max', 'number', [
                'label'     => 'Anzahl',
                'value'     => $this->model->checkin_max ?: '',
                'attr'      => [
                    'min' => 1,
                    'max' => 1000,
                ],
            ])
            // Kontingent (Quotas)
            ->add('quota_id', 'entity', [
                'label'         => 'Zeitraum',
                'class'         => QuotaUnit::class,
                'property'      => 'name',
                'query_builder' => function (QuotaUnit $quotaUnit) {
                    return $quotaUnit->listsTranslations('name');
                },
                'selected'      => function () {
                    return $this->model->quotaUnit ? $this->model->quotaUnit->id : null;
                },
                'empty_value'   => 'Kein Limit',
            ])
            ->add('quota_max', 'number', [
                'label'     => 'Anzahl',
                'value'     => $this->model->quota_max ?: '',
                'attr'      => [
                    'min' => 1,
                    'max' => 10000,
                ],
            ])
            // Berechtigungen (required / optional)
            ->add('required_licenses', 'entity', [
                'label'         => 'Berechtigungen (benötigt alle)',
                'class'         => LicenseType::class,
                'multiple'      => true,
                'property'      => 'name',
                'query_builder' => function (LicenseType $licenseType) {
                    return $licenseType->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->requiredLicenseTypes()->pluck('id')->toArray();
                },
                'attr'          => [
                    'id'                => 'required_licenses',
                    'data-placeholder'  => 'Wählen Sie aus ...'
                ],
            ])
            ->add('optional_licenses', 'entity', [
                'label'         => 'Berechtigungen (benötigt mindestens eine)',
                'class'         => LicenseType::class,
                'multiple'      => true,
                'property'      => 'name',
                'query_builder' => function (LicenseType $licenseType) {
                    return $licenseType->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->optionalLicenseTypes()->pluck('id')->toArray();
                },
                'attr'          => [
                    'id'                => 'optional_licenses',
                    'data-placeholder'  => 'Wählen Sie aus ...',
                ],
            ])
            // Rules
            ->add('rule_text', 'textarea', [
                'label' => 'Text',
                'value' => $this->model->rule ? $this->model->rule->text : '',
                'attr'  => [
                    'id'    => 'rule_text',
                ],
            ])
            ->add('rule_files_public', 'file', [
                'label' => 'Öffentliche Datei(en) (pdf)',
                'type'  => 'file',
                'attr'  => [
                    'multiple'  => true,
                    'class'     => 'form-control rule_files_public',
                ],
            ])
            ->add('rule_files', 'file', [
                'label' => 'Nicht öffentliche Datei(en) (pdf/zip)',
                'type'  => 'file',
                'attr'  => [
                    'multiple'  => true,
                    'class'     => 'form-control rule_files',
                ],
            ])
            // Ausfang (Fische/Tag, Maximale Angeltage/für Fangstatistik)
            ->add('fish_per_day', 'number', [
                'label'         => 'Fische/Tag',
                'value'         => $this->model->fish_per_day ?: 0,
                'help_block'    => [
                    'text'  => 'Maximal erlaubter Ausfang pro Tag.',
                ],
                'attr'          => [
                    'min'   => 0,
                    'max'   => 10,
                ],
            ])
            ->add('fishing_days', 'number', [
                'label'         => 'Max. Angeltage',
                'value'         => $this->model->fishing_days ?: '',
                'help_block'    => [
                    'text'  => 'Wird als Tabelle an die Angelkarte angehängt.',
                ],
                'attr'          => [
                    'min'   => 2,
                    'max'   => 144,
                ],
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ],
            ]);

        if ($this->model->prices()->count() > 0) {
            $this->modify('group', 'checkbox', [
                'attr' => [
                    'disabled',
                ],
            ]);
        }
    }
}
