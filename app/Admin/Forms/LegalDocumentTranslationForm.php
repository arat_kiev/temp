<?php

namespace App\Admin\Forms;

use Kris\LaravelFormBuilder\Form;

class LegalDocumentTranslationForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('id', 'hidden')
            ->add('locale', 'choice', [
                'label' => 'Schema/Locale',
                'attr' => [
                    'placeholder' => 'Bitte auswählen',
                ],
                'choices' => $this->getData('locales'),
            ])
            ->add('name', 'text', [
                'label' => 'Name',
            ])
            ->add('content', 'textarea', [
                'label' => 'Inhalt',
                'attr' => [
                    'class' => 'translations-rich-text',
                ],
            ])
            ->add('file', 'file', [
                'label' => 'PDF-Dokument',
            ]);
    }
}