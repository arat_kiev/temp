<?php

namespace App\Admin\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Models\Location\Country;
use App\Models\Picture;

class OrganizationForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'label' => 'Name',
            ])
            ->add('street', 'text', [
                'label' => 'Adresse',
            ])
            ->add('area_code', 'text', [
                'label' => 'Postleitzahl',
            ])
            ->add('city', 'text', [
                'label' => 'Ort/Stadt',
            ])
            ->add('person', 'text', [
                'label' => 'Ansprechperson',
            ])
            ->add('phone', 'text', [
                'label' => 'Telefon-Nr.',
            ])
            ->add('email', 'text', [
                'label' => 'E-Mail-Adresse',
            ])
            ->add('website', 'text', [
                'label' => 'Webseite',
            ])
            ->add('logo', 'file', [
                'label' => 'Neues Logo wählen',
            ])
            ->add('country', 'entity', [
                'label' => 'Land',
                'class' => 'App\Models\Location\Country',
                'property' => 'name',
                'property_key' => 'id',
                'query_builder' => function (Country $country) {
                    return $country->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected' => function () {
                    return ($this->model && $this->model->country) ? $this->model->country->id : 0;
                },
                'empty_value' => ' ',
                'attr' => [
                    'data-placeholder' => 'Bitte wählen',
                ],
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
