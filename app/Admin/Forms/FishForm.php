<?php

namespace App\Admin\Forms;

use App\Models\Meta\FishCategory;
use Kris\LaravelFormBuilder\Form;

class FishForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('latin', 'text', [
                'label' => 'Latin-Name',
            ])
            ->add('picture', 'file', [
                'label' => 'Neues Bild wählen',
                'value' => '',
            ])
            ->add('min_size', 'number', [
                'label' => 'Min. Länge',
                'value' => $this->model->min_size ? number_format($this->model->min_size, 2, '.', '') : '',
                'attr'  => [
                    'min'   => '0',
                    'max'   => '999.99',
                    'step'  => '0.01',
                ],
            ])
            ->add('max_size', 'number', [
                'label' => 'Max. Länge',
                'value' => $this->model->max_size ? number_format($this->model->max_size, 2, '.', '') : '',
                'attr'  => [
                    'min'   => '0',
                    'max'   => '999.99',
                    'step'  => '0.01',
                ],
            ])
            ->add('min_weight', 'number', [
                'label' => 'Min. Gewicht in Gramm',
                'value' => $this->model->min_weight ? $this->model->min_weight : '',
                'attr'  => [
                    'min'   => '0',
                    'max'   => '999999',
                    'step'  => '1',
                ],
            ])
            ->add('max_weight', 'number', [
                'label' => 'Max. Gewicht in Gramm',
                'value' => $this->model->max_weight ? $this->model->max_weight : '',
                'attr'  => [
                    'min'   => '0',
                    'max'   => '999999',
                    'step'  => '1',
                ],
            ])
            ->add('category', 'entity', [
                'label'         => 'Kategorie',
                'class'         => FishCategory::class,
                'property'      => 'name',
                'query_builder' => function (FishCategory $category) {
                    return $category->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->category ? $this->model->category->id : null;
                },
                'attr'          => [
                    'id'               => 'categories',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
