<?php

namespace App\Admin\Forms;

use App\Managers\TicketManager;
use App\Models\Area\Area;
use App\Models\Haul;
use App\Models\Meta\Fish;
use App\Models\Meta\Technique;
use App\Models\Ticket\Ticket;
use App\Models\User;
use Carbon\Carbon;
use Auth;
use Kris\LaravelFormBuilder\Form;

class HaulForm extends Form
{
    public function buildForm()
    {
        $area = isset($this->data['areaId'])
            ? Area::where('id', $this->data['areaId'])->first()
            : Area::where('id', $this->model->area->id)->first();
        $ticket = isset($this->data['ticketId'])
            ? Ticket::find($this->data['ticketId'])
            : $this->model->ticket;

        $this
            ->add('area_name', 'text', [
                'label'         => 'Gewässer-Name',
            ])
            ->add('ticket_number', 'text', [
                'label'         => 'Angelkarten-Nummer',
            ])
            ->add('taken', 'radiobutton-group', [
               'label' => 'Entnomen',
               'options' => [
                   [
                       'name' => 'taken',
                       'class' => 'info',
                       'label' => 'Entnommen',
                       'value' => 1
                   ],
                   [
                       'name' => 'taken',
                       'class' => 'info',
                       'label' => 'Nicht entnommen',
                       'value' => 0
                   ]
               ]
            ])
            ->add('ticket', 'entity', [
                'class'         => Ticket::class,
                'label'         => 'Angelkarte',
                'property'      => 'ticket_number',
                'query_builder' => function (Ticket $ticket) {
                    $ticket = $this->data['user']
                        ? $ticket->where('user_id', $this->data['user']->id)
                        : $ticket;

                    return $ticket->select('id', 'valid_from')->orderBy('id', 'asc')->get()->map(function ($item) {
                        return collect($item)->merge(['ticket_number' => (new TicketManager())->getIdentifier($item)]);
                    });
                },
                'selected'      => function () {
                    return $this->model->ticket
                        ? $this->model->ticket->id
                        : ($this->data['ticketId'] ?? null);
                },
                'empty_value'   => '-- Keine --',
                'attr'          => [
                    'id'               => 'tickets',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ]);

        $areas = [];

        if ($ticket && $ticket->type->additionalAreas) {
            $areas = $ticket->type->additionalAreas->count() > 0
                ? $ticket->type->additionalAreas->pluck('name', 'id')->toArray()
                : $area->pluck('name', 'id')->toArray();
        }
        $selectedArea = isset($area->id)
            ? $area->id
            : key($areas);

            $this->add('area', 'select', [
                'choices'     => $areas,
                'selected'    => $selectedArea,
                'label'       => 'Gewässer',
                'empty_value' => '-- Keine --',
                'attr'        => [
                    'id'               => 'areas',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ]);

            $this->add('fish', 'entity', [
                'class'         => Fish::class,
                'label'         => 'Fischart',
                'property'      => 'name',
                'query_builder' => function (Fish $fish) {
                    return $fish->listsTranslations('name')->orderBy('id', 'asc');
                },
                'selected'      => function () {
                    return $this->model->fish ? $this->model->fish->id : null;
                },
                'empty_value'   => '-- Keine --',
                'attr'          => [
                    'id'               => 'fishes',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('public', 'checkbox', [
                'label'         => 'Öffentlich',
            ])
//            ->add('taken', 'checkbox', [
//                'label'         => 'Entnommen',
//            ])
            ->add('count', 'number', [
                'label'         => 'Anzahl',
                'value'         => $this->model->count ?: 1,
            ])
            ->add('size_value', 'number', [
                'label'         => 'Größe',
                'value'         => $this->model->size_value
                    ? number_format((float) $this->model->size_value, 2, '.', '')
                    : '',
                'attr'          => [
                    'min'   => 0,
                    'max'   => '999.99',
                    'step'  => '0.01',
                ],
            ])
//            ->add('size_type', 'select', [
//                'label'         => 'Einheit',
//                'choices'       => Haul::AVAILABLE_SIZE_TYPES,
//                'selected'      => $this->model->size_type ?: 'CM',
//            ])
            ->add('size_type', 'radiobutton-group', [
                'label' => 'Einheit',
                'options' => [
                    [
                        'name' => 'size_type',
                        'class' => 'info',
                        'label' => 'CM',
                        'value' => 'CM'
                    ],
                    [
                        'name' => 'size_type',
                        'class' => 'info',
                        'label' => 'KG',
                        'value' => 'KG'
                    ]
                ]
            ])
            ->add('catch_date', 'text', [
                'label'         => 'Fangdatum',
                'value'         => $this->model->catch_date
                    ? $this->model->catch_date->format('d.m.Y')
                    : (isset($this->data['ticketId'])
                       && $this->data['ticketId']
                       && ($ticket = Ticket::whereId($this->data['ticketId'])->first())
                        ? $ticket->valid_from->format('d.m.Y')
                        : null),
                'attr'          => [
                    'placeholder'   => 'dd.mm.yyyy',
                    'data-provide'  => 'datepicker',
                ],
            ])
            ->add('catch_time', 'time', [
                'label'         => 'Fangzeit',
                'value'         => $this->model->catch_time
                    ? Carbon::parse($this->model->catch_time)->format('H:i')
                    : (isset($this->data['ticketId'])
                       && $this->data['ticketId']
                       && ($ticket = Ticket::whereId($this->data['ticketId'])->first())
                        ? $ticket->valid_from->format('H:i')
                        : null),
                'attr'          => [
                    'placeholder'   => 'hh:mm',
                ],
            ])
            ->add('technique', 'entity', [
                'class'         => Technique::class,
                'label'         => 'Technik',
                'property'      => 'name',
                'query_builder' => function (Technique $technique) {
                    return $technique->listsTranslations('name')->orderBy('id', 'asc');
                },
                'selected'      => function () {
                    return $this->model->technique ? $this->model->technique->id : null;
                },
                'empty_value'   => '-- Keine --',
                'attr'          => [
                    'id'               => 'techniques',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('weather', 'select', [
                'label'     => 'Wetter',
                'choices'   => array_flip(Haul::AVAILABLE_WEATHER),
                'selected'  => $this->model->weather ?: null,
            ])
            ->add('temperature', 'text', [
                'label'         => 'Temperatur',
            ])
            ->add('user_comment', 'textarea', [
                'label'         => 'Kommentar',
                'attr'          => [
                    'id'    => 'user_comment',
                ],
            ])
            ->add('picture', 'file', [
                'label' => 'Neues Bild wählen',
            ])
            ->add('submit', 'submit', [
                'label'         => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'          => [
                    'class' => 'btn btn-primary',
                ]
            ]);

        if (!$this->model->id) {
            $this
                ->add('user', 'entity', [
                    'class'         => User::class,
                    'label'         => 'Angler',
                    'property'      => 'full_user_name',
                    'query_builder' => function (User $user) {
                        return $user
                            ->selectRaw('id, IF(first_name IS NOT NULL && last_name IS NOT NULL, '
                                        . 'CONCAT_WS(" ",id,first_name,last_name), '
                                        . 'CONCAT("Unbekannt #", id)) AS full_user_name')
                            ->orderBy('last_name', 'asc');
                    },
                    'selected'      => function () {
                        return $this->model->user
                            ? $this->model->user->id
                            : ($this->data['userId'] ?? null);
                    },
                    'attr'          => [
                        'id'               => 'users',
                        'data-placeholder' => 'Wählen Sie aus ...'
                    ],
                ]);
        }

        if ($this->model->id && $this->model->ticket) {
            $this->remove('ticket_number');
        }

        if ($this->model->id && $this->model->area) {
            $this->remove('area_name');
        }
    }
}
