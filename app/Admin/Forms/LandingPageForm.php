<?php

namespace App\Admin\Forms;

use App\Models\Area\Area;
use App\Models\Contact\Manager;
use App\Models\LandingPages\LandingPage;
use App\Models\Area\AreaType;
use App\Models\Meta\Fish;
use App\Models\Meta\Technique;
use App\Models\Ticket\TicketCategory;
use Kris\LaravelFormBuilder\Form;

class LandingPageForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('type', 'select', [
                'label'     => 'Typ',
                'choices'   => LandingPage::getAvailableTypes(true),
                'selected'  => $this->model->type ?: null,
            ])
            ->add('areas', 'entity', [
                'label'         => 'Gewässer (Min. 3)',
                'class'         => Area::class,
                'property'      => 'name',
                'multiple'      => true,
                'query_builder' => function (Area $area) {
                    return $area->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->areas()->pluck('id')->toArray();
                },
                'attr'          => [
                    'id'                => 'areas',
                    'data-placeholder'  => 'Wählen Sie aus ...'
                ],
            ])
            ->add('picture', 'file', [
                'label' => 'Neues Bild wählen',
                'value' => ''
            ])


            ->add('search[area_types]', 'entity', [
                'label' => 'Gewässer-Typ',
                'class' => AreaType::class,
                'multiple' => true,
                'property' => 'name',
                'query_builder' => function (AreaType $type) {
                    return $type->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected' => null,
                'attr' => [
                    'id'                => 'areaTypes',
                    'data-placeholder'  => 'Wählen Sie aus ...'
                ],
            ])
            ->add('search[fishes]', 'entity', [
                'label' => 'Zielfische',
                'class' => Fish::class,
                'multiple' => true,
                'property' => 'name',
                'query_builder' => function (Fish $fish) {
                    return $fish->listsTranslations('name')->orderBy('name');
                },
                'selected' => null,
                'attr' => [
                    'id'                => 'fishes',
                    'data-placeholder'  => 'Wählen Sie aus ...'
                ],
            ])
            ->add('search[techniques]', 'entity', [
                'label' => 'Angeltechniken',
                'class' => Technique::class,
                'multiple' => true,
                'property' => 'name',
                'query_builder' => function (Technique $tech) {
                    return $tech->listsTranslations('name')->orderBy('name');
                },
                'selected' => null,
                'attr' => [
                    'id' => 'techniques',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('search[manager]', 'entity', [
                'label' => 'Bewirtschafter',
                'class' => Manager::class,
                'multiple' => true,
                'property' => 'name',
                'query_builder' => function (Manager $manager) {
                    return $manager->orderBy('name');
                },
                'selected' => null,
                'attr' => [
                    'id' => 'managers',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('search[is_ticket]', 'checkbox', [
                'label' => 'Angelkarten',
            ])
            ->add('search[ticket_categories]', 'entity', [
                'label' => 'Angelkartentyp',
                'class' => TicketCategory::class,
                'multiple' => true,
                'property' => 'name',
                'query_builder' => function (TicketCategory $category) {
                    return $category->listsTranslations('name')->orderBy('name');
                },
                'selected' => null,
                'attr' => [
                    'id' => 'categories',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('search[lease]', 'checkbox', [
                'label' => 'Pachtgewässer (lease)',
            ])
            ->add('search[city]', 'checkbox', [
                'label' => 'Stadt',
            ])
            ->add('search[region]', 'checkbox', [
                'label' => 'Region',
            ])
            ->add('search[state]', 'checkbox', [
                'label' => 'Bundesland (state)',
            ])
            ->add('search[country]', 'checkbox', [
                'label' => 'Land',
            ])
            ->add('search[member_only]', 'checkbox', [
                'label' => 'Nur für Mitglieder',
            ])
            ->add('search[boat]', 'checkbox', [
                'label' => 'Boot',
            ])
            ->add('search[phone_ticket]', 'checkbox', [
                'label' => 'Handy Angelkarte',
            ])
            ->add('search[nightfishing]', 'checkbox', [
                'label' => 'Nachtangeln',
            ])

            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
