<?php

namespace App\Admin\Forms;

use App\Models\License\LicenseType;
use App\Models\Ticket\CheckinUnit;
use App\Models\Ticket\QuotaUnit;
use App\Models\Ticket\TicketCategory;
use App\Models\Ticket\TicketDay;
use Kris\LaravelFormBuilder\Form;

class LockPeriodForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('lock_from_string', 'text', [
                'label' => false,
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy',
                    'data-provide' => 'datepicker',
                ],
            ])
            ->add('lock_till_string', 'text', [
                'label' => false,
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy',
                    'data-provide' => 'datepicker',
                ],
            ]);
    }
}
