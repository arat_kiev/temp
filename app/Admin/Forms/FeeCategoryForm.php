<?php

namespace App\Admin\Forms;

use Kris\LaravelFormBuilder\Form;

class FeeCategoryForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
