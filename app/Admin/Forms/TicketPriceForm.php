<?php

namespace App\Admin\Forms;

use App\Models\License\LicenseType;
use App\Models\Ticket\CheckinUnit;
use App\Models\Ticket\QuotaUnit;
use App\Models\Ticket\TicketCategory;
use App\Models\Ticket\TicketDay;
use Carbon\Carbon;
use Kris\LaravelFormBuilder\Form;

class TicketPriceForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'label' => 'Name',
                'help_block' => [
                    'text' => 'Hinweis: Jahreszahl wird automatisch an Namen angehängt.',
                ],
            ])
            ->add('type', 'select', [
                'label' => 'Typ',
                'choices' => [
                    'DEFAULT' => 'Standard',
                    'JUNIOR' => 'Jugend',
                    'SENIOR' => 'Senioren',
                    'MEMBER' => 'Mitglied',
                ]
            ])
            ->add('value', 'text', [
                'label' => 'Preis (in €)',
                'value' => number_format($this->model->value / 100.0, 2, ',', ''),
            ])
            ->add('show_vat', 'checkbox', [
                'label' => 'MwSt auf Angelkarte anzeigen',
                'checked' => $this->model->id ? $this->model->show_vat : true,
            ])
            ->add('age', 'number', [
                'label' => 'Alter (inkl.)',
                'attr' => [
                    'min' => 1,
                    'max' => 300,
                ],
            ])
            ->add('visible_from', 'text', [
                'label' => 'Kaufbar ab',
                'value' => $this->model->visible_from
                    ? $this->model->visible_from->format('d.m.Y')
                    : null,
                'default_value' => Carbon::now()->format('d.m.Y'),
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy',
                    'data-provide' => 'datepicker',
                ],
            ])
            ->add('valid_from', 'text', [
                'label' => 'Saison von',
                'value' => $this->model->valid_from
                    ? $this->model->valid_from->format('d.m.Y')
                    : null,
                'default_value' => Carbon::now()->addDay()->format('d.m.Y'),
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy',
                    'data-provide' => 'datepicker',
                ],
            ])
            ->add('valid_to', 'text', [
                'label' => 'Saison bis',
                'value' => $this->model->valid_to
                    ? $this->model->valid_to->format('d.m.Y')
                    : null,
                'default_value' => Carbon::now()->diffInDays(Carbon::now()->endOfYear()) > 90
                    ? Carbon::now()->endOfYear()->format('d.m.Y')
                    : Carbon::now()->addYear()->endOfYear()->format('d.m.Y'),
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy',
                    'data-provide' => 'datepicker',
                ],
            ])
            ->add('weekdays', 'entity', [
                'class' => TicketDay::class,
                'property' => 'weekday',
                'query_builder' => function (TicketDay $ticketDay) {
                    return [
                        'MON' => 'Montag',
                        'TUE' => 'Dienstag',
                        'WED' => 'Mittwoch',
                        'THU' => 'Donnerstag',
                        'FRI' => 'Freitag',
                        'SAT' => 'Samstag',
                        'SUN' => 'Sonntag',
                    ];
                },
                'selected' => function () {
                    return $this->model->weekdays()->pluck('weekday')->toArray();
                },
                'multiple' => true,
                'label' => 'Gesperrte Wochentage',
                'attr' => [
                    'id' => 'weekdays',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('lock_periods', 'collection', [
                'label' => false,
                'wrapper' => false,
                'type' => 'form',
                'prototype' => true,
                'data' => $this->model->lockPeriods,
                'empty_row' => false,
                'options' => [
                    'label' => false,
                    'class' => LockPeriodForm::class,
                    'template' => 'admin.fields.periods',
                ],
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ]);

        if ($this->model->ticketType && $this->model->ticketType->group) {
            $this->add('group_count', 'number', [
                'label' => 'Gruppengröße',
                'default_value' => 1,
                'attr' => [
                    'min' => 1,
                    'max' => 15,
                ],
            ]);
        }
    }
}
