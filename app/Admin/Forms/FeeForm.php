<?php

namespace App\Admin\Forms;

use App\Models\Location\Country;
use App\Models\Product\Fee;
use App\Models\Product\FeeCategory;
use Kris\LaravelFormBuilder\Form;

class FeeForm extends Form
{
    private $disableAttr = [];

    public function buildForm()
    {
        if ($this->model->prices()->count()) {
            $this->disableAttr['disabled'] = true;
        }

        $this
            ->add('value', 'text', [
                'label' => 'Wert',
            ])
            ->add('type', 'select', [
                'label'     => 'Typ',
                'choices'   => Fee::AVAILABLE_TYPES,
                'selected'  => $this->model->type,
            ])
            ->add('category', 'entity', [
                'label'         => 'Kategorie',
                'class'         => FeeCategory::class,
                'property'      => 'name',
                'query_builder' => function (FeeCategory $category) {
                    return $category->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->category
                        ? $this->model->category->id
                        : null;
                },
                'attr'          => [
                    'id'               => 'categories',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('country', 'entity', [
                'label'         => 'Land',
                'class'         => Country::class,
                'property'      => 'name',
                'query_builder' => function (Country $country) {
                    return $country->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->country
                        ? $this->model->country->id
                        : null;
                },
                'attr'          => [
                    'id'               => 'countries',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => array_merge([
                    'class' => 'btn btn-primary',
                ], $this->disableAttr),
            ]);
    }
}
