<?php

namespace App\Admin\Forms;

use App\Models\Location\Country;
use App\Models\Location\State;
use Kris\LaravelFormBuilder\Form;

class RegionForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('country', 'entity', [
                'label'         => 'Land',
                'class'         => Country::class,
                'property'      => 'name',
                'query_builder' => function (Country $country) {
                    return $country->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->state && $this->model->state->country
                        ? $this->model->state->country->id
                        : null;
                },
                'attr'          => [
                    'id'               => 'countries',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('state', 'entity', [
                'label'         => 'Bundesland',
                'class'         => State::class,
                'property'      => 'name',
                'query_builder' => function (State $state) {
                    return $state->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->state ? $this->model->state->id : null;
                },
                'attr'          => [
                    'id'               => 'states',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
