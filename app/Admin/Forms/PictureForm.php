<?php

namespace App\Admin\Forms;

use Kris\LaravelFormBuilder\Form;

class PictureForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'label' => 'Name',
            ])
            ->add('description', 'textarea', [
                'label' => 'Beschreibung',
                'attr' => [
                    'id' => 'description',
                ],
            ])
            ->add('priority', 'select', [
                'label' => 'Priorität',
                'choices' => range(0, 5),
                'selected' => $this->model->pivot->priority,
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
