<?php

namespace App\Admin\Forms;

use App\Models\Location\Country;
use Kris\LaravelFormBuilder\Form;

class StateForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('country', 'entity', [
                'label'         => 'Land',
                'class'         => Country::class,
                'property'      => 'name',
                'query_builder' => function (Country $country) {
                    return $country->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->country ? $this->model->country->id : null;
                },
                'attr'          => [
                    'id'               => 'countries',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('rule_text', 'textarea', [
                'label' => 'Text',
                'value' => $this->model->rule ? $this->model->rule->text : '',
                'attr' => [
                    'id' => 'rule_text',
                ],
            ])
            ->add('rule_files', 'file', [
                'label' => 'Datei(en) (pdf)',
                'type' => 'file',
                'attr' => [
                    'multiple' => true,
                    'class' => 'form-control rule_files',
                ],
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
