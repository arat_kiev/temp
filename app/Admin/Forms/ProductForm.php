<?php

namespace App\Admin\Forms;

use App\Models\Product\ProductCategory;
use Kris\LaravelFormBuilder\Form;

class ProductForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('category', 'entity', [
                'label'         => 'Kategorie',
                'class'         => ProductCategory::class,
                'property'      => 'name',
                'query_builder' => function (ProductCategory $category) {
                    return $category->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->category
                        ? $this->model->category->id
                        : null;
                },
                'attr'          => [
                    'id'               => 'categories',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('featured_from', 'text', [
                'label' => 'Featured von',
                'value' => $this->model->featured_from
                    ? $this->model->featured_from->format('d.m.Y')
                    : null,
                'attr'  => [
                    'placeholder'   => 'dd.mm.yyyy',
                    'data-provide'  => 'datepicker',
                ],
            ])
            ->add('featured_till', 'text', [
                'label' => 'Featured bis',
                'value' => $this->model->featured_till
                    ? $this->model->featured_till->format('d.m.Y')
                    : null,
                'attr'  => [
                    'placeholder'   => 'dd.mm.yyyy',
                    'data-provide'  => 'datepicker',
                ],
            ])
            ->add('account_number', 'text', [
                'label' => 'Kontonummer',
            ])
            ->add('max_quantity', 'number', [
                'label' => 'Maximale Anzahl (im Warenkorb)',
                'help_block' => [
                    'text' => '0 = unbegrenzt',
                ],
                'attr'          => [
                    'min'       => 0,
                    'step'      => 1,
                    'max'       => 1000,
                    'required'  => true,
                ],
            ])
            ->add('template', 'textarea', [
                'label' => 'Beschreibung',
                'attr' => [
                    'id' => 'template',
                ],
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
