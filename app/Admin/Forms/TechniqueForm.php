<?php

namespace App\Admin\Forms;

use Kris\LaravelFormBuilder\Form;

class TechniqueForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('picture', 'file', [
                'label' => 'Neues Bild wählen',
                'value' => '',
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
