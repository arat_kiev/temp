<?php

namespace App\Admin\Forms;

use Kris\LaravelFormBuilder\Form;

class FishingMethodForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('fishingMethodId', 'hidden', [
                'value' => $this->model->id ? $this->model->id : 0,
                'attr' => [
                    'id' => 'fishing-method-id',
                    'data-fishing-method-id' => $this->model->id ? $this->model->id : 0
                ]
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
