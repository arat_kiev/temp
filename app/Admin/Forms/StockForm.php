<?php

namespace App\Admin\Forms;

use App\Models\Location\Country;
use Kris\LaravelFormBuilder\Form;

class StockForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'label'         => 'Name',
            ])
            ->add('country', 'entity', [
                'label'         => 'Land',
                'class'         => Country::class,
                'property'      => 'name',
                'query_builder' => function (Country $country) {
                    return $country->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->country ? $this->model->country->id : null;
                },
                'attr'          => [
                    'id'               => 'countries',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('city', 'text', [
                'label'         => 'Stadt',
            ])
            ->add('street', 'text', [
                'label'         => 'Straße',
            ])
            ->add('building', 'text', [
                'label'         => 'Gebäude',
            ])
            ->add('rest_info', 'text', [
                'label'         => 'Rest Infos',
                'help_block'    => [
                    'text'  => 'z.B. Gebäude, Geschäftsstelle, Büro, Appartement',
                ],
            ])
            ->add('emails', 'select', [
                'label'         => 'Emails',
                'choices'       => $this->model->emails
                    ? array_combine($this->model->emails, $this->model->emails)
                    : [],
                'selected'      => $this->model->emails,
                'attr'          => [
                    'id'                => 'emails',
                    'multiple'          => true,
                    'data-placeholder'  => 'Email hinzufügen',
                ]
            ])
            ->add('phones', 'select', [
                'label'         => 'Telefonnummern',
                'choices'       => $this->model->phones
                    ? array_combine($this->model->phones, $this->model->phones)
                    : [],
                'selected'      => $this->model->phones,
                'attr'          => [
                    'id'                => 'phones',
                    'multiple'          => true,
                    'data-placeholder'  => 'Telefonnummer hinzufügen',
                ]
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
