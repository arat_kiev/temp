<?php

namespace App\Admin\Forms;

use App\Models\License\LicenseType;
use App\Models\Ticket\CheckinUnit;
use App\Models\Ticket\QuotaUnit;
use App\Models\Ticket\TicketCategory;
use App\Models\Ticket\TicketDay;
use Kris\LaravelFormBuilder\Form;

class TimeDiscountForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('id', 'hidden', [
                'label' => false,
                'default_value' => '0',
            ])
            ->add('count', 'number', [
                'label' => false,
                'default_value' => '1',
                'attr' => [
                    'min' => '1',
                ],
            ])
            ->add('discount', 'text', [
                'label' => false,
                'default_value' => 0,
            ])
            ->add('inclusive', 'choice', [
                'label' => false,
                'choices' => [
                    '1' => 'Ja',
                    '0' => 'Nein',
                ],
                'attr' => [
                    'style' => 'width: 80px;'
                ],
            ]);
    }
}
