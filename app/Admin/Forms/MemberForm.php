<?php

namespace App\Admin\Forms;

use App\Models\Location\Country;
use Kris\LaravelFormBuilder\Form;

class MemberForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('first_name', 'text', [
                'label' => 'Name',
            ])
            ->add('last_name', 'text', [
                'label' => 'Nachname',
            ])
            ->add('function', 'text', [
                'label' => 'Funktion',
            ])
            ->add('member_id', 'text', [
                'label' => 'Mitglieds-Nr.',
            ])
            ->add('since', 'text', [
                'label' => 'Mitglied seit',
                'value' => $this->model->since ? $this->model->since->format('d.m.Y') : null,
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy',
                    'data-provide' => 'datepicker',
                ],
            ])
            ->add('email', 'text', [
                'label' => 'E-Mail-Adresse',
            ])
            ->add('gender', 'select', [
                'label' => 'Geschlecht',
                'choices' => [
                    null => 'Keine Angabe',
                    'MALE' => 'Männlich',
                    'FEMALE' => 'Weiblich',
                ],
            ])
            ->add('birthday', 'text', [
                'label' => 'Geburtsdatum',
                'value' => $this->model->birthday ? $this->model->birthday->format('d.m.Y') : null,
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy',
                    'data-provide' => 'datepicker',
                ],
            ])
            ->add('street', 'text', [
                'label' => 'Straße',
            ])
            ->add('area_code', 'text', [
                'label' => 'Postleitzahl',
            ])
            ->add('city', 'text', [
                'label' => 'Ort/Stadt',
            ])
            ->add('phone', 'text', [
                'label' => 'Telefon',
            ])
            ->add('leave_date', 'text', [
                'label' => 'Ausgetreten am',
                'value' => $this->model->leave_date ? $this->model->leave_date->format('d.m.Y') : null,
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy',
                    'data-provide' => 'datepicker',
                ],
            ])
            ->add('country', 'entity', [
                'label' => 'Land',
                'class' => 'App\Models\Location\Country',
                'property' => 'name',
                'property_key' => 'id',
                'query_builder' => function (Country $country) {
                    return $country->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected' => function () {
                    return $this->model->country ? $this->model->country->id : 0;
                },
                'empty_value' => ' ',
                'attr' => [
                    'data-placeholder' => 'Bitte wählen',
                ],
            ])
            ->add('comment', 'textarea', [
                'label' => 'Kommentar',
                'attr' => [
                    'id' => 'comment',
                ],
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr' => [
                    'class' => 'btn btn-default',
                    'value' => 'save',
                    'name' => 'action'
                ]
            ])
            ->add('submit_exit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern und Schließen',
                'attr' => [
                    'class' => 'btn btn-primary member_save_close',
                    'value' => 'save_and_close',
                    'name' => 'action'
                ]
            ]);
    }
}
