<?php

namespace App\Admin\Forms;

use App\Models\Ticket\TicketInspection;
use Kris\LaravelFormBuilder\Form;

class TicketInspectionForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('status', 'choice', [
                'label'     => false,
                'choices'   => array_flip(TicketInspection::AVAILABLE_STATUSES),
                'selected'  => $this->model->status ?: null,
                'expanded'  => true,
                'default_value'     => 'OK',
                'choice_options'    => [
                     'wrapper'  => [
                         'class' => 'choice-wrapper '
                                    . (!$this->model->status || $this->model->status == 'OK')
                                        ? 'funkyradio-success'
                                        : 'funkyradio-danger'
                     ],
                ],
                'attr'      => [
                    'required'  => true,
                ],
            ])
            ->add('comment', 'text', [
                'label'     => 'Anmerkung',
                'value'     => $this->model->comment,
                'attr'      => [
                    'id'        => 'comment',
                ],
            ])
            ->add('latitude', 'text', [
                'label' => false,
                'attr'  => ['id' => 'latitude', 'style' => 'display:none'],
            ])
            ->add('longitude', 'text', [
                'label' => false,
                'attr'  => ['id' => 'longitude', 'style' => 'display:none'],
            ])
            ->add('accuracy', 'text', [
                'label' => false,
                'attr'  => ['id' => 'accuracy', 'style' => 'display:none'],
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Kontrolle eintragen',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ]);
    }
}
