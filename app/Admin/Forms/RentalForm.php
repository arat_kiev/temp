<?php

namespace App\Admin\Forms;

use App\Models\Location\Country;
use Kris\LaravelFormBuilder\Form;

class RentalForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'label' => 'Name',
            ])
            ->add('street', 'text', [
                'label' => 'Adresse',
            ])
            ->add('area_code', 'text', [
                'label' => 'Postleitzahl',
            ])
            ->add('city', 'text', [
                'label' => 'Ort/Stadt',
            ])
            ->add('country', 'entity', [
                'label' => 'Land',
                'class' => Country::class,
                'property' => 'name',
                'query_builder' => function (Country $country) {
                    return $country->join('country_translations as ct', 'ct.country_id', '=', 'countries.id')
                        ->where('locale', app()->getLocale())
                        ->groupBy('countries.id')
                        ->orderBy('ct.name', 'asc')
                        ->with('translations')
                        ->select('countries.id as id', 'ct.name as name');
                },
                'selected' => function () {
                    return $this->model->country ? $this->model->country->id : 0;
                },
                'empty_value' => ' ',
                'attr' => [
                    'data-placeholder' => 'Bitte wählen',
                ],
            ])
            ->add('person', 'text', [
                'label' => 'Ansprechperson',
            ])
            ->add('phone', 'text', [
                'label' => 'Telefon-Nr.',
            ])
            ->add('email', 'email', [
                'label' => 'E-Mail-Adresse',
            ])
            ->add('website', 'text', [
                'label' => 'Webseite',
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
