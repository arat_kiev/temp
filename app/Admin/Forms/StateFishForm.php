<?php

namespace App\Admin\Forms;

use App\Models\Meta\Fish;
use Kris\LaravelFormBuilder\Form;

class StateFishForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('fish', 'entity', [
                'label'         => 'Fisch',
                'class'         => Fish::class,
                'property'      => 'name',
                'query_builder' => function (Fish $country) {
                    return $country->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->fish ? $this->model->fish->id : null;
                },
                'attr'          => [
                    'id'               => 'fishes',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('closed_from', 'text', [
                'label' => 'Schonzeit von',
                'value' => $this->model->closed_from
                    ? $this->model->closed_from->format('d.m.Y')
                    : null,
                'attr'  => [
                    'placeholder'   => 'dd.mm.yyyy',
                    'data-provide'  => 'datepicker',
                ],
            ])
            ->add('closed_till', 'text', [
                'label' => 'Schonzeit bis',
                'value' => $this->model->closed_till
                    ? $this->model->closed_till->format('d.m.Y')
                    : null,
                'attr'  => [
                    'placeholder'   => 'dd.mm.yyyy',
                    'data-provide'  => 'datepicker',
                ],
            ])
            ->add('min_length', 'number', [
                'label' => 'Mindestmaß',
            ])
            ->add('max_length', 'number', [
                'label' => 'Maximalmaß',
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
