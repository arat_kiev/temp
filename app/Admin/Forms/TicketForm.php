<?php

namespace App\Admin\Forms;

use App\Models\Client;
use App\Models\Ticket\Checkin;
use App\Models\Ticket\TicketPrice;
use App\Models\Ticket\TicketType;
use Kris\LaravelFormBuilder\Form;

class TicketForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('type', 'text', [
                'label' => 'Typ',
                'value' => $this->model->type
                    ? $this->model->type->id
                    : 0,
            ])
            ->add('price', 'entity', [
                'class'         => TicketPrice::class,
                'property'      => 'described_name',
                'query_builder' => function (TicketPrice $prices) {
                    return $prices->listsTranslations('name')->selectDescribedName();
                },
                'selected'      => function () {
                    return $this->model->price ? $this->model->price->id : 0;
                },
                'label'         => 'Preis',
            ])
            ->add('client', 'entity', [
                'class'         => Client::class,
                'property'      => 'name',
                'query_builder' => function (Client $clients) {
                    return $clients->orderBy('name', 'asc');
                },
                'selected'      => function () {
                    return $this->model->client ? $this->model->client->id : 0;
                },
                'label'         => 'Client',
            ])
            ->add('valid_from', 'text', [
                'label' => 'Gültig von',
                'value' => $this->model->valid_from
                    ? $this->model->valid_from->format('d.m.Y H:i')
                    : null,
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy hh:mm',
                    'data-provide' => 'datetimepicker',
                ],
            ])
            ->add('valid_to', 'text', [
                'label' => 'Gültig bis',
                'value' => $this->model->valid_to
                    ? $this->model->valid_to->format('d.m.Y H:i')
                    : null,
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy hh:mm',
                    'data-provide' => 'datetimepicker',
                ],
            ])
            ->add('checkins', 'collection', [
                'label'     => false,
                'type'      => 'form',
                'prototype' => true,
                'empty_row' => false,
                'data' => $this->model->checkins,
                'options'   => [
                    'class'     => 'App\Admin\Forms\CheckinForm',
                    'template'  => 'admin.fields.checkins',
                    'label'     => false,
                ],
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ],
            ]);
    }
}
