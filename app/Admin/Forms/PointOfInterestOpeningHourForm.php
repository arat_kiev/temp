<?php

namespace App\Admin\Forms;

use Kris\LaravelFormBuilder\Form;

class PointOfInterestOpeningHourForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('day', 'hidden', [
                'default_value' => '__OPENING_HOUR_DAY__',
                'attr'  => [
                    'class' => 'opening_hour-id',
                ],
            ])
            ->add('opens', 'text', [
                'label' => 'Öffnet',
                // Wrapper hidden, because important to manually include span inside this wrapper
                // 'wrapper' => ['class' => 'input-group date'],
                'attr'  => [
                    'placeholder'   => 'hh:mm',
                ],
            ])
            ->add('closes', 'text', [
                'label' => 'Schließt',
                // Wrapper hidden, because important to manually include span inside this wrapper 
                // 'wrapper' => ['class' => 'input-group date'],
                'attr' => [
                    'placeholder'   => 'hh:mm',
                ],
            ])
            ->add('button_remove', 'button', [
                'label' => '&times;',
                'attr'  => [
                    'class' => 'btn btn-danger fishes-btn-remove',
                ],
            ]);
    }
}
