<?php

namespace App\Admin\Forms;

use App\Models\Contact\Manager;
use Carbon\Carbon;
use Kris\LaravelFormBuilder\Form;

class SuperInvoiceForm extends Form
{

    public function buildForm()
    {
        $manager = $this->data['manager'];
        $lastInvoice = $manager ? $manager->invoices()->orderBy('created_at', 'desc')->first() : null;

        $from = $lastInvoice ? $lastInvoice->date_till->copy()->addDay() : Carbon::parse('first day of this month');
        $till = Carbon::today();

        $this
            ->add('date_from', 'text', [
                'label' => 'Datum von',
                'value' => $from->format('d.m.Y'),
            ])
            ->add('date_till', 'text', [
                'label' => 'Datum bis',
                'value' => $till->format('d.m.Y'),
            ])
            ->add('manager', 'entity', [
                'label'         => 'Bewirtschafter',
                'class'         => Manager::class,
                'property'      => 'name',
                'query_builder' => function (Manager $managers) {
                    return $managers->select('id', 'name')->orderBy('name');
                },
                'selected'      => function () {
                    return $this->model->manager
                        ? $this->model->manager->id
                        : ($this->data['manager'] ? $this->data['manager']->id : 0);
                },
                'empty_value'   => 'Bitte wählen',
                'attr'          => [
                    'data-placeholder' => 'Bitte wählen',
                ],
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
