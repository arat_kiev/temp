<?php

namespace App\Admin\Forms;

use App\Models\Location\Country;
use Kris\LaravelFormBuilder\Form;

class UserForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('id', 'hidden')
            ->add('first_name', 'text', [
                'label' => 'Vorname',
            ])
            ->add('last_name', 'text', [
                'label' => 'Nachname',
            ])
            ->add('street', 'text', [
                'label' => 'Adresse',
            ])
            ->add('post_code', 'text', [
                'label' => 'Postleitzahl',
            ])
            ->add('city', 'text', [
                'label' => 'Ort/Stadt',
            ])
            ->add('country', 'entity', [
                'label' => 'Land',
                'class' => Country::class,
                'property' => 'name',
                'query_builder' => function (Country $country) {
                    return $country->join('country_translations as ct', 'ct.country_id', '=', 'countries.id')
                        ->where('locale', app()->getLocale())
                        ->groupBy('countries.id')
                        ->orderBy('ct.name', 'asc')
                        ->with('translations')
                        ->select('countries.id as id', 'ct.name as name');
                },
                'selected' => function () {
                    return $this->model->country ? $this->model->country->id : 0;
                },
                'empty_value' => ' ',
                'attr' => [
                    'data-placeholder' => 'Bitte wählen',
                ]
            ])
            ->add('birthday', 'text', [
                'label' => 'Geburtstag',
                'value' => $this->model->birthday
                    ? $this->model->birthday->format('d.m.Y')
                    : null,
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy',
                    'data-provide' => 'datepicker',
                ],
            ])
            ->add('phone', 'text', [
                'label' => 'Telefon-Nr.',
            ])
            ->add('email', 'email', [
                'label' => 'E-Mail-Adresse',
            ])
            ->add('balance', 'text', [
                'label' => 'Balance (in €)',
                'value' => number_format($this->model->balance / 100.0, 2, ',', ''),
            ])
            ->add('active', 'checkbox', [
                'label' => 'Aktiv',
            ])
            ->add('newsletter', 'checkbox', [
                'label' => 'Newsletter',
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
