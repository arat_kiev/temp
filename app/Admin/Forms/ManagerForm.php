<?php

namespace App\Admin\Forms;

use App\Models\Location\Country;
use Kris\LaravelFormBuilder\Form;

class ManagerForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'label' => 'Name',
            ])
            ->add('street', 'text', [
                'label' => 'Adresse',
            ])
            ->add('area_code', 'text', [
                'label' => 'Postleitzahl',
            ])
            ->add('city', 'text', [
                'label' => 'Ort/Stadt',
            ])
            ->add('country', 'entity', [
                'label' => 'Land',
                'class' => Country::class,
                'property' => 'name',
                'query_builder' => function (Country $country) {
                    return $country->join('country_translations as ct', 'ct.country_id', '=', 'countries.id')
                        ->where('locale', app()->getLocale())
                        ->groupBy('countries.id')
                        ->orderBy('ct.name', 'asc')
                        ->with('translations')
                        ->select('countries.id as id', 'ct.name as name');
                },
                'selected' => function () {
                    return $this->model->country ? $this->model->country->id : 0;
                },
                'empty_value' => ' ',
                'attr' => [
                    'data-placeholder' => 'Bitte wählen',
                ],
            ])
            ->add('person', 'text', [
                'label' => 'Ansprechperson',
            ])
            ->add('phone', 'text', [
                'label' => 'Telefon-Nr.',
            ])
            ->add('email', 'email', [
                'label' => 'E-Mail-Adresse',
            ])
            ->add('website', 'text', [
                'label' => 'Webseite',
            ])
            ->add('uid', 'text', [
                'label' => 'UID-Nr.',
            ])
            ->add('account_number', 'text', [
                'label' => 'Kurzbezeichnung',
            ])
            ->add('short_code', 'text', [
                'label' => 'Ticket-Kürzel',
                'help_block' => [
                    'text' => 'Kürzel für die fortlaufende Kartennummer. Genau 4 Zeichen, nur Buchstaben.',
                ],
            ])
            ->add('ticket_info', 'textarea', [
                'label' => 'Text auf Angelkarte (rechts)',
                'attr' => [
                    'id' => 'ticket_info',
                ],
            ])
            ->add('rule_text', 'textarea', [
                'label' => 'Text',
                'value' => $this->model->rule ? $this->model->rule->text : '',
                'attr' => [
                    'id' => 'rule_text',
                ],
            ])
            ->add('rule_files_public', 'file', [
                'label' => 'Öffentliche Datei(en) (pdf)',
                'type' => 'file',
                'attr' => [
                    'multiple' => true,
                    'class' => 'form-control rule_files_public'
                ],
            ])
            ->add('rule_files', 'file', [
                'label' => 'Nicht öffentliche Datei(en) (pdf)',
                'type' => 'file',
                'attr' => [
                    'multiple' => true,
                    'class' => 'form-control rule_files'
                ],
            ])
            ->add('logo', 'file', [
                'label' => 'Neues Logo wählen',
            ])
            ->add('signature', 'file', [
                'label' => 'Unterschrift',
            ])
            ->add('storno_emails', 'select', [
                'label'     => 'E-Mail-Adresse für Storno-Benachrichtigungen',
                'choices'   => $this->model->stornoNotifications()->pluck('email', 'email')->toArray(),
                'selected'  => $this->model->stornoNotifications()->pluck('email')->toArray(),
                'attr'      => [
                    'id'                => 'storno-emails',
                    'multiple'          => true,
                    'data-placeholder'  => 'Email hinzufügen',
                ]
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ]);

        if ($this->getFormOption('super')) {
            $this->add('commission_included', 'checkbox', [
                'label' => 'Servicegebühr durch Angler',
            ]);
        }
    }
}
