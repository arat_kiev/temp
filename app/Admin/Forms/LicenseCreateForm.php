<?php

namespace App\Admin\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Models\License\LicenseType;
use App\Models\User;

class LicenseCreateForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('user_id', 'entity', [
                'label' => 'User',
                'class'     => User::class,
                'query_builder' => function () {
                    return User::select('id', 'name')->orderBy('name', 'asc');
                },
                'wrapper' => [
                    'class' => 'form-group col-md-6',
                    'style' => 'padding: 0;'
                ],
                'attr' => [
                    'id' => 'users',
                    'class' => 'form-control',
                    'style'=>'width: 100%;',
                ]
            ])
            ->add('license_type_id', 'entity', [
                'label' => 'Berechtigungstyp',
                'class'     => LicenseType::class,
                'query_builder' => function () {
                    return LicenseType::select('id', 'name')->orderBy('name', 'asc');
                },
                'wrapper' => [
                    'class' => 'form-group col-md-6',
                    'style' => 'padding-right: 0;'
                ],
                'attr' => [
                    'id' => 'licenses',
                    'class' => 'form-control',
                    'style'=>'width: 100%;',
                ]
            ])
        ;
    }
}
