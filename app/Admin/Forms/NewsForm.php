<?php

namespace App\Admin\Forms;

use Kris\LaravelFormBuilder\Form;

class NewsForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('title', 'text', [
                'label' => 'Titel *',
            ])
            ->add('content', 'textarea', [
                'label' => 'Inhalt *',
                'attr' => [
                    'id' => 'content_text',
                ],
            ])
            ->add('public', 'checkbox', [
                'label' => 'Öffentlich',
            ])
            ->add('published_on', 'text', [
                'label' => 'Veröffentlicht',
                'value' => $this->model->published_on
                    ? $this->model->published_on->format('d.m.Y H:i')
                    : null,
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy hh:mm',
                    'id' => 'published_on_dtp',
                ],
            ])
            ->add('unpublished_on', 'text', [
                'label' => 'Gelöscht',
                'value' => $this->model->deleted_at
                    ? $this->model->deleted_at->format('d.m.Y H:i')
                    : null,
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy hh:mm',
                    'id' => 'unpublished_on_dtp',
                ],
            ])
            ->add('submit', 'submit', [
                'label' => 'Speichern',
                'attr' => [
                    'class' => 'btn btn-primary',
                    'value' => 'save',
                    'name' => 'action'
                ]
            ]);
    }
}
