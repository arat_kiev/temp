<?php

namespace App\Admin\Forms;

use App\Models\Contact\Reseller;
use Carbon\Carbon;
use Kris\LaravelFormBuilder\Form;

class InvoiceForm extends Form
{

    public function buildForm()
    {
        $this
            ->add('date_from', 'text', [
                'label' => 'Datum von',
                'value' => Carbon::parse('first day of this month')->format('d.m.Y'),
            ])
            ->add('date_till', 'text', [
                'label' => 'Datum bis',
                'value' => Carbon::today()->format('t.m.Y'),
            ])
            ->add('reseller', 'entity', [
                'label'         => 'Verkäufer',
                'class'         => Reseller::class,
                'property'      => 'name',
                'query_builder' => function (Reseller $reseller) {
                    return $reseller
                        ->join('manager_resellers as mr', 'mr.reseller_id', '=', 'resellers.id')
                        ->join('managers', 'mr.manager_id', '=', 'managers.id')
                        ->where('managers.id', '=', $this->data['manager']->id)
                        ->select('resellers.id', 'resellers.name');
                },
                'selected'      => function () {
                    return $this->model->reseller ? $this->model->reseller->id : 0;
                },
                'empty_value'   => 'Bitte wählen',
                'attr'          => [
                    'data-placeholder' => 'Bitte wählen',
                ],
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
