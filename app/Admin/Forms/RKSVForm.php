<?php

namespace App\Admin\Forms;

use App\Models\Location\Country;
use Kris\LaravelFormBuilder\Form;

class RKSVForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('company_name', 'text', [
                'label' => 'Name',
            ])
            ->add('company_street', 'text', [
                'label' => 'Adresse',
            ])
            ->add('company_plz', 'text', [
                'label' => 'Postleitzahl',
            ])
            ->add('company_city', 'text', [
                'label' => 'Stadt',
            ])
            ->add('company_country', 'select', [
                'label' => 'Land',
                'choices' => [
                    'at' => 'Österreich',
                    'de' => 'Deutschland',
                ],
            ])
            ->add('user_first_name', 'text', [
                'label' => 'Vorname',
            ])
            ->add('user_last_name', 'text', [
                'label' => 'Nachname',
            ])
            ->add('user_email', 'text', [
                'label' => 'E-Mail',
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-check fa-fw"></span> Erstellen',
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
