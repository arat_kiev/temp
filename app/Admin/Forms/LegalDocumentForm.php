<?php

namespace App\Admin\Forms;

use Kris\LaravelFormBuilder\Form;

class LegalDocumentForm extends Form
{
    public function buildForm()
    {
        $locales = $this->model->getLocales();
        $locales = array_combine($locales, $locales);

        $this
            ->add('internal_name', 'text', [
                'label' => 'Interne Bezeichnung',
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ])
            ->add('translations', 'collection', [
                'label' => false,
                'wrapper' => false,
                'type' => 'form',
                'prototype' => true,
                'data' => $this->model->translations,
                'empty_row' => false,
                'options' => [
                    'label' => false,
                    'class' => LegalDocumentTranslationForm::class,
                    'data' => compact('locales'),
                    'template' => 'admin.fields.legaldocs.translations',
                ],
            ]);
    }
}