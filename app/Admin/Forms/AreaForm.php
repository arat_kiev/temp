<?php

namespace App\Admin\Forms;

use App\Models\Area\AreaType;
use App\Models\Meta\Fish;
use App\Models\Meta\Technique;
use App\Models\Contact\Manager;
use App\Models\Ticket\TicketType;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Kris\LaravelFormBuilder\Form;
use Request;
use Auth;

class AreaForm extends Form
{
    public function buildForm()
    {
        $cities = $this->model->cities()->get()
            ->sortBy('post_code')
            ->mapWithKeys(function ($city) {
                return [(string)$city->id => $city->name . ' (' . $city->post_code . ')'];
            });
        $citySelected =  $this->model->cities()->first();

        $this
            ->add('name', 'text', [
                'label' => 'Gewässer-Name',
            ])
            ->add('type', 'entity', [
                'class' => AreaType::class,
                'property' => 'name',
                'query_builder' => function (AreaType $type) {
                    return $type->listsTranslations('name')->orderBy('name', 'asc');
                },
                'selected' => function () {
                    return $this->model->type ? $this->model->type->id : 0;
                },
                'label' => 'Gewässer-Typ',
            ])
            ->add('public', 'checkbox', [
                'label' => 'Online',
            ])
            ->add('lease', 'checkbox', [
                'label' => 'Pachtgewässer',
            ])
            ->add('member_only', 'checkbox', [
                'label' => 'Nur für Mitglieder',
            ])
            ->add('boat', 'checkbox', [
                'label' => 'Boot',
            ])
            ->add('phone_ticket', 'checkbox', [
                'label' => 'Handy Angelkarte',
            ])
            ->add('nightfishing', 'checkbox', [
                'label' => 'Nachtangeln',
            ])
            ->add('rods_max', 'number', [
                'label' => 'Ruten',
            ])
            ->add('season_begin', 'text', [
                'label' => 'Saison-Beginn',
                'value' => $this->model->season_begin
                    ? $this->model->season_begin->format('d.m')
                    : null,
                'default_value' => '',
                'attr' => [
                    'placeholder' => 'Tag.Monat',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'dd.mm',
                    'data-date-min-view-mode' => '0',
                    'data-date-max-view-mode' => '1',
                ],
            ])
            ->add('season_end', 'text', [
                'label' => 'Saison-Ende',
                'value' => $this->model->season_end
                    ? $this->model->season_end->format('d.m')
                    : null,
                'default_value' => '',
                'attr' => [
                    'placeholder' => 'Tag.Monat',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'dd.mm',
                    'data-date-min-view-mode' => '0',
                    'data-date-max-view-mode' => '1',
                ],
            ])
            ->add('description', 'textarea', [
                'label' => 'Beschreibung',
                'attr' => [
                    'id' => 'description',
                ],
            ])
            ->add('borders', 'textarea', [
                'label' => 'Gewässer-Grenzen',
                'attr' => [
                    'id' => 'borders',
                ],
            ])
            ->add('ticket_info', 'textarea', [
                'label' => 'Text auf Angelkarte (links)',
                'attr' => [
                    'id' => 'ticket_info',
                ],
            ])
            ->add('rule_text', 'textarea', [
                'label' => 'Text',
                'value' => $this->model->rule ? $this->model->rule->text : '',
                'attr' => [
                    'id' => 'rule_text',
                ],
            ])
            ->add('rule_files_public', 'file', [
                'label' => 'Öffentliche Datei(en) (pdf)',
                'type' => 'file',
                'attr' => [
                    'multiple' => true,
                    'class' => 'form-control rule_files_public'
                ],
            ])
            ->add('rule_files', 'file', [
                'label' => 'Nicht öffentliche Datei(en) (pdf)',
                'type' => 'file',
                'attr' => [
                    'multiple' => true,
                    'class' => 'form-control rule_files'
                ],
            ])
            ->add('fishes', 'collection', [
                'label' => false,
                'class' => Fish::class,
                'type' => 'form',
                'prototype' => true,
                'empty_row' => false,
                'options' => [
                    'class' => 'App\Admin\Forms\FishRestrictionForm',
                    'template' => 'admin.fields.fish',
                    'label' => false,
                ],
            ])
            ->add('techniques', 'entity', [
                'label' => 'Angeltechniken',
                'class' => Technique::class,
                'multiple' => true,
                'property' => 'name',
                'query_builder' => function (Technique $tech) {
                    return $tech->listsTranslations('name')->orderBy('name');
                },
                'selected' => function () {
                    return $this->model->techniques()->pluck('id')->toArray();
                },
                'attr' => [
                    'id' => 'techniques',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('ticket_types', 'entity', [
                'label' => 'Kartentypen',
                'class' => TicketType::class,
                'multiple' => true,
                'property' => 'name',
                'query_builder' => function (TicketType $ticketType) {
                    return $ticketType->listsTranslations('name')->orderBy('name');
                },
                'selected' => function () {
                    return $this->model->additionalTicketTypes()->pluck('id')->toArray();
                },
                'attr' => [
                    'id' => 'ticketTypes',
                    'data-placeholder' => 'Wählen Sie aus ...'
                ],
            ])
            ->add('locations', 'collection', [
                'label' => false,
                'type' => 'text',
                'property' => 'latLonString',
                'prototype' => true,
                'wrapper' => false,
                'empty_row' => false,
                'options' => [
                    'label' => false,

                    'template' => 'admin.fields.location',
                    'attr' => [
                            'required' => 'required',
                            'placeholder' => 'Bspw. 48.2966549,14.2957787',
                    ],
                ],
            ])
            ->add('cities', 'select', [
                'label' => 'Primärer Ort',
                'choices' => $cities->toArray(),
                'selected' => $citySelected ? $citySelected->id : null,
            ])
            ->add('custom_tags', 'select', [
                'label' => 'Benutzerdefiniert',
                'choices' => $this->model->tags()->where('custom', 1)->pluck('tag', 'tag')->toArray(),
                'selected' => $this->model->tags()->where('custom', 1)->pluck('tag')->toArray(),
                'attr' => [
                    'id' => 'custom-tags',
                    'multiple' => true,
                    'data-placeholder' => 'Tags erstellen',
                ]
            ])
            ->add('polyfield', 'textarea', [
                'label' => 'Gewässergrenzen (geojson)',
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr' => [
                    'class' => 'btn btn-primary',
                ]
            ]);

        if(strpos(Request::path(), 'super') !== false) {
            $this->add('manager', 'entity', [
                'class' => Manager::class,
                'property' => 'name',
                'query_builder' => function (Manager $manager) {
                    return $manager->orderBy('id', 'asc');
                },
                'selected' => function () {
                    return $this->model->manager_id ?: 0;
                },

                'label' => 'Bewirtschafter',
            ]);
        }
    }
}
