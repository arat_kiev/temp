<?php

namespace App\Admin\Forms;

use App\Models\Product\Product;
use App\Models\Product\TimeDiscount;
use App\Models\Product\Timeslot;
use Kris\LaravelFormBuilder\Form;

class ProductPriceForm extends Form
{
    private $disableAttr = [];

    public function buildForm()
    {
        if ($this->model->sales()->count()) {
            $this->disableAttr['disabled'] = true;
        }

        $this
            ->add('value', 'text', [
                'label' => 'Preis (in €)',
                'value' => $this->model->value,
            ])
            ->add('vat', 'text', [
                'label' => 'VAT',
            ])
            ->add('visible_from', 'text', [
                'label' => 'Kaufbar ab',
                'value' => $this->model->visible_from
                    ? $this->model->visible_from->format('d.m.Y')
                    : null,
                'attr'  => [
                    'placeholder'   => 'dd.mm.yyyy',
                    'data-provide'  => 'datepicker',
                ],
            ])
            ->add('valid_from', 'text', [
                'label' => 'Gültig von',
                'value' => $this->model->valid_from
                    ? $this->model->valid_from->format('d.m.Y')
                    : null,
                'attr'  => [
                    'placeholder'   => 'dd.mm.yyyy',
                    'data-provide'  => 'datepicker',
                ],
            ])
            ->add('valid_till', 'text', [
                'label' => 'Gültig bis',
                'value' => $this->model->valid_till
                    ? $this->model->valid_till->format('d.m.Y')
                    : null,
                'attr'  => [
                    'placeholder'   => 'dd.mm.yyyy',
                    'data-provide'  => 'datepicker',
                ],
            ])
            ->add('timeslots', 'collection', [
                'label' => false,
                'class' => Timeslot::class,
                'type' => 'form',
                'prototype' => true,
                'wrapper' => false,
                'empty_row' => false,
                'options' => [
                    'label' => false,
                    'class' => TimeslotForm::class,
                    'template' => 'admin.fields.timeslots',
                    'formOptions' => $this->disableAttr,
                ],
            ])
            ->add('timeDiscounts', 'collection', [
                'label' => false,
                'class' => TimeDiscount::class,
                'type' => 'form',
                'prototype' => true,
                'wrapper' => false,
                'empty_row' => false,
                'options' => [
                    'label' => false,
                    'class' => TimeDiscountForm::class,
                    'template' => 'admin.fields.discounts',
                    'formOptions' => $this->disableAttr,
                ]
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => array_merge([
                    'class'     => 'btn btn-primary',
                ], $this->disableAttr),
            ]);
    }
}
