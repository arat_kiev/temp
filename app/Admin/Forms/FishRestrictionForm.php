<?php

namespace App\Admin\Forms;

use Kris\LaravelFormBuilder\Form;

class FishRestrictionForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('id', 'hidden', [
                'default_value' => '__FISH_ID__',
                'attr' => [
                    'class' => 'fish-id',
                ],
            ])
            ->add('name', 'hidden', [
                'default_value' => '__FISH__',
            ])
            ->add('min_length', 'number', [
                'label' => 'Mindestmaß',
            ])
            ->add('max_length', 'number', [
                'label' => 'Maximalmaß',
            ])
            ->add('closed_from', 'text', [
                'label' => 'Schonzeit von',
                //'value' => $this->model->closed_from ? $this->model->closed_from->format('d.m.Y') : null,
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy',
                    'data-provide' => 'datepicker',
                ],
            ])
            ->add('closed_till', 'text', [
                'label' => 'Schonzeit bis',
                'attr' => [
                    'placeholder' => 'dd.mm.yyyy',
                    'data-provide' => 'datepicker',
                ],
            ])
            ->add('button_remove', 'button', [
                'label' => '&times;',
                'attr' => [
                    'class' => 'btn btn-danger fishes-btn-remove',
                ],
            ]);
    }
}
