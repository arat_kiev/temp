<?php

namespace App\Admin\Forms;

use Kris\LaravelFormBuilder\Form;

class CountryForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('area_code', 'text', [
                'label' => 'Gewässer Code',
            ])
            ->add('country_code', 'text', [
                'label' => 'Landes Code',
            ])
            ->add('vat', 'text', [
                'label' => 'VAT',
            ])
            ->add('submit', 'submit', [
                'label' => '<span class="fa fa-save fa-fw"></span> Speichern',
                'attr'  => [
                    'class' => 'btn btn-primary',
                ]
            ]);
    }
}
