<?php

namespace App\Admin\Forms;

use App\Models\License\LicenseType;
use Carbon\Carbon;
use Kris\LaravelFormBuilder\Form;
use App\Models\Location\Country;

class LicenseForm extends Form
{
    public function buildForm()
    {
        $this
        ->add('email', 'email', [
            'label' => 'E-Mail-Adresse',
            'value' => $this->model->user->email ? $this->model->user->email : null,
            'attr'  => [
                'disabled' => 'disabled',
            ],
        ])
        ->add('first_name', 'text', [
            'label' => 'Vorname',
            'value' => $this->model->user->first_name ? $this->model->user->first_name : null,
        ])
        ->add('last_name', 'text', [
            'label' => 'Nachname',
            'value' => $this->model->user->last_name ? $this->model->user->last_name : null,
        ])
        ->add('street', 'text', [
            'label' => 'Adresse',
            'value' => $this->model->user->street ? $this->model->user->street : null,
        ])
        ->add('post_code', 'text', [
            'label' => 'Postleitzahl',
            'value' => $this->model->user->post_code ? $this->model->user->post_code : null,
        ])
        ->add('city', 'text', [
            'label' => 'Ort/Stadt',
            'value' => $this->model->user->city ? $this->model->user->city : null,
        ])
        ->add('country', 'entity', [
            'label'     => 'Land',
            'class'     => Country::class,
            'property'  => 'name',
            'query_builder' => function (Country $country) {
                return $country->listsTranslations('name');
            },
            'selected'  => function () {
                return $this->model->user->country ? $this->model->user->country->id : 0;
            },
        ])
        ->add('birthday', 'text', [
            'label' => 'Geburtstag',
            'value' => $this->model->user->birthday ? $this->model->user->birthday->format('d.m.Y') : null,
            'attr'  => [
                'placeholder'   => 'dd.mm.yyyy',
                'data-provide'  => 'datepicker',
            ],
        ])
        ->add('phone', 'text', [
            'label' => 'Telefon-Nr.',
            'value' => $this->model->user->phone ? $this->model->user->phone : null,

        ])
        ->add('valid_from', 'text', [
            'label' => 'Gültig von',
            'value' => $this->model->valid_from ? $this->model->valid_from->format('d.m.Y') : null,
            'attr'  => [
                'placeholder'   => 'dd.mm.yyyy',
                'data-provide'  => 'datepicker',
            ],
        ])
        ->add('valid_to', 'text', [
            'label' => 'Gültig bis',
            'value' => $this->model->valid_to ? $this->model->valid_to->format('d.m.Y') : null,
            'attr'  => [
                'placeholder'   => 'dd.mm.yyyy',
                'data-provide'  => 'datepicker',
            ],
        ])
            ->add('status_text', 'textarea', [
                'label' => false,
            ])
            ->add('license_type', 'entity', [
                'label'         => 'Typ',
                'class'         => LicenseType::class,
                'query_builder' => function () {
                    return LicenseType::select('id', 'name')->orderBy('name', 'asc');
                },
                'selected' => function () {
                    return $this->model->type ? $this->model->type->id : 0;
                },
            ]);

        // iterate over custom params
        if ($this->model && is_array($this->model->fields)) {
            foreach ($this->model->fields as $field => $value) {
                if (in_array($this->model->type->fields[$field], ['date', 'from', 'till'])) {
                    $this->add($field, 'text', [
                        'value' => Carbon::parse($value)->format('d.m.Y'),
                        'attr' => [
                            'placeholder' => 'dd.mm.yyyy',
                            'data-provide' => 'datepicker',
                        ],
                    ]);
                } else {
                    $this->add($field, 'text', [
                        'value' => $value,
                    ]);
                }
            }
        }

        // iterate over custom params from license type, that aren't included to license custom params
        foreach ($this->fetchMissedFields() as $field => $type) {
            if (in_array($type, ['date', 'from', 'till'])) {
                $this->add($field, 'text', [
                    'value' => '',
                    'attr' => [
                        'placeholder' => 'dd.mm.yyyy',
                        'data-provide' => 'datepicker',
                    ],
                ]);
            } else {
                $this->add($field, 'text', [
                    'value' => '',
                ]);
            }
        }
    }

    private function fetchMissedFields()
    {
        if (!$this->model) {
            return null;
        }

        $modelFields = $this->model->fields && is_array($this->model->fields)
            ? array_keys($this->model->fields)
            : [];

        return array_filter($this->model->type->fields, function ($field) use ($modelFields) {
            return !in_array($field, $modelFields);
        }, ARRAY_FILTER_USE_KEY);
    }
}
