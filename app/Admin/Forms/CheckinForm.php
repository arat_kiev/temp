<?php

namespace App\Admin\Forms;

use Kris\LaravelFormBuilder\Form;

class CheckinForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('checkin_from', 'text', [
                'label' => 'von',
                'attr'  => [
                    'placeholder'   => 'hh:mm',
                    'data-provide' => 'timepicker',
                ],
            ])
            ->add('checkin_till', 'text', [
                'label' => 'bis',

                'attr' => [
                    'placeholder'   => 'hh:mm',
                    'data-provide' => 'timepicker',

                ],
            ])
            ->add('button_remove', 'button', [
                'label' => '&times;',
                'attr'  => [
                    'class' => 'btn btn-danger fishes-btn-remove',
                ],
            ]);
    }
}
