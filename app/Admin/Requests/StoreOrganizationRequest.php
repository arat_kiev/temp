<?php

namespace App\Admin\Requests;

use App\Models\User;
use Auth;

class StoreOrganizationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var User $user */
        $user = Auth::user();

        return $user->organizations()->first() != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo' => 'mimes:jpeg,jpg,png|max:5000',
            'name' => 'required|string|min:8',
            'street' => 'string',
            'area_code' => 'string',
            'city' => 'string',
            'country' => 'sometimes|exists:countries,id',
            'person' => 'string',
            'phone' => 'string',
            'email' => 'string',
            'website' => 'active_url',
        ];
    }

    public function messages()
    {
        return [
            '*.required' => 'Pflichtfeld',
            'name.min' => 'Name muss mindestens :min Zeichen lang sein.',
        ];
    }
}
