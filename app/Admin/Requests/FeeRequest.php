<?php

namespace App\Admin\Requests;

use App\Models\Product\Fee;
use App\Models\User;
use Auth;

class FeeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() && Auth::user()->hasRole('superadmin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'value'         => 'required|string|regex:/^\d+(,\d{2})?$/',
                    'type'          => 'required|in:'.implode(',', array_keys(Fee::AVAILABLE_TYPES)),
                    'category'      => 'required|exists:fee_categories,id',
                    'country'       => 'required|exists:countries,id',
                    'data'          => 'required|array',
                    'data.*.locale' => 'required|string|max:5',
                    'data.*.name'   => 'required|string|max:255',
                ];
            case 'GET':
            case 'DELETE':
            case 'PUT':
            case 'PATCH':
            default:
                return [];
        }
    }
}
