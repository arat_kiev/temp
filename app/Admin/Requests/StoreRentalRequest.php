<?php

namespace App\Admin\Requests;

use App\Models\User;
use Auth;

class StoreRentalRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:8',
            'street' => 'required|string',
            'area_code' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|exists:countries,id',
            'person' => 'string',
            'phone' => 'string',
            'email' => 'string',
            'website' => 'active_url',
        ];
    }

    public function messages()
    {
        return [
            '*.required' => 'Pflichtfeld',
            'name.min' => 'Name muss mindestens :min Zeichen lang sein.',
        ];
    }
}
