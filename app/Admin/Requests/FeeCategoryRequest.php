<?php

namespace App\Admin\Requests;

use Auth;

class FeeCategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() && Auth::user()->hasRole('superadmin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                return [
                    'data'          => 'required|array',
                    'data.*.locale'             => 'required|string|max:5',
                    'data.*.name'               => 'required|string|max:255',
                    'data.*.short_description'  => 'sometimes|string',
                    'data.*.long_description'   => 'sometimes|string',
                ];
            case 'GET':
            case 'DELETE':
            case 'PUT':
            case 'PATCH':
            default:
                return [];
        }
    }
}
