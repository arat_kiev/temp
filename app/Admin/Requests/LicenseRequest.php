<?php

namespace App\Admin\Requests;

use App\Models\User;
use Auth;

class LicenseRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST':
                return [
                    'action'        => 'required|in:save,accept,reject',
                    'license_type'  => 'required|exists:license_types,id',
                    'valid_from'    => 'date_format:"d.m.Y"',
                    'valid_to'      => 'date_format:"d.m.Y"',
                    'status_text'   => 'string',
                ];
            default:
                return [];
        }
    }
}
