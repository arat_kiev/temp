<?php

namespace App\Admin\Requests;
use App\Models\Ticket\Checkin;
use Auth;

class CheckinRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var \App\Models\User $user */
        $user = Auth::user();

        return $user && $user->managers()->first() != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': // create
                return [
                    'checkins'     => 'array|min:1',
                    'checkins.*'   => 'array',
                    'checkins.*.id'    => 'required|integer',
                    'checkins.*.from'    => 'required|date|date_format:Y-m-d H:i:s',
                    'checkins.*.till'    => 'required|date|date_format:Y-m-d H:i:s',
                ];
            case 'PATCH': // update
                return [
                    'checkins'     => 'array|min:1',
                    'checkins.*'   => 'array',
                    'checkins.*.id'    => 'sometimes|integer',
                    'checkins.*.from'    => 'required|date|date_format:Y-m-d H:i:s',
                    'checkins.*.till'    => 'required|date|date_format:Y-m-d H:i:s',
                ];
            case 'GET':
            case 'DELETE':
                return [
                    'checkins'     => 'array|min:1',
                    'checkins.*'   => 'array',
                    'checkins.*.id'    => 'required|integer',
                ];
            default:
                return [];
        }
    }
}
