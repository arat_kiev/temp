<?php

namespace App\Admin\Requests;

use Illuminate\Validation\Validator;

class StoreTicketTypeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rule_text' => 'string',
            'rule_files' => 'sometimes|array',
            'rule_files.*' => 'mimes:pdf,zip|max:5000',
            'rule_files_public' => 'sometimes|array',
            'rule_files_public.*' => 'mimes:pdf|max:5000',
            'rule_files_name' => 'sometimes|array',
            'rule_public_files_name' => 'sometimes|array',
            'name' => 'required|string|min:8',
            'is_catchlog_required' => 'boolean',
            'group' => 'boolean',
            'category' => 'required|integer|exists:ticket_categories,id',
            'duration' => 'required|integer|between:1,300',
            'day_starts_at_sunrise' => 'boolean|required_without_all:day_starts_at_time,day_starts_at_sunset',
            'day_starts_at_sunset' => 'boolean|required_without_all:day_starts_at_time,day_starts_at_sunrise',
            'day_starts_at_time' => 'date_format:H:i|required_without_all:day_starts_at_sunrise,day_starts_at_sunset',
            'day_ends_at_sunrise' => 'boolean|required_without_all:day_ends_at_time,day_ends_at_sunset',
            'day_ends_at_sunset' => 'boolean|required_without_all:day_ends_at_time,day_ends_at_sunrise',
            'day_ends_at_time' => 'date_format:H:i|required_without_all:day_ends_at_sunrise,day_ends_at_sunset',
            'checkin_id' => 'integer|exists:checkin_units,id',
            'checkin_max' => 'integer|between:1,1000|required_with:checkin_id',
            'quota_id' => 'integer|exists:quota_units,id',
            'quota_max' => 'integer|between:1,10000|required_with:quota_id',
            'fish_per_day' => 'required|integer|between:0,10',
            'fishing_days' => 'integer|between:2,144',
        ];
    }

    public function messages()
    {
        return [
            '*.required' => 'Pflichtfeld',
            'name.min' => 'Name muss mindestens :min Zeichen lang sein.',
            'rule_files.*.max' => 'Maximale Dateigröße: 5 MByte.',
            'rule_files.*.mimes' => 'Datei(en) müssen im PDF oder ZIP Format sein.',
            'rule_files_public.*.max' => 'Maximale Dateigröße: 5 MByte.',
            'rule_files_public.*.mimes' => 'Datei(en) müssen im PDF-Format sein.',
            '*.required_without_all' => 'Sie müssen eine dieser Optionen auswählen.'
        ];
    }

    protected function getValidatorInstance()
    {
        /** @var Validator $v */
        $v = parent::getValidatorInstance();

        $v->sometimes(['fishing_days'], 'required', function ($input) {
            return !($input->category == 1 || $input->category == 2 && $input->duration == 1);
        });

        return $v;
    }
}
