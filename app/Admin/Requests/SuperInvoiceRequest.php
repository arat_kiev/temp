<?php

namespace App\Admin\Requests;

use Auth;

class SuperInvoiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                return [
                    'date_from' => 'date|date_format:d.m.Y|before:date_till',
                    'date_till' => 'date|date_format:d.m.Y|after:date_from',
                    'manager'   => 'required|exists:managers,id',
                ];
            case 'GET':
            case 'DELETE':
            case 'PUT':
            case 'PATCH':
            default:
                return [];
        }
    }
}
