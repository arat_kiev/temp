<?php

namespace App\Admin\Requests;

use App\Models\Haul;
use App\Models\Ticket\Ticket;
use Auth;

class HaulRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var \App\Models\User $user */
        $user = Auth::user();

        return $user && $user->managers()->first() != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': // create
                return [
                    'user'          => 'required|integer|exists:users,id',
                    'area'          => 'integer|exists:areas,id',
                    'area_name'     => 'required_without:area|string|max:255',
                    'fish'          => 'integer|exists:fishes,id',
                    'technique'     => 'integer|exists:techniques,id',
                    'count'         => 'integer',
                    'ticket'        => 'integer|exists:tickets,id',
                    'ticket_number' => 'required_without:ticket|string|regex:'.Ticket::IDENT_REGEX,
                    'size_value'    => 'required|regex:/^\d*(\.\d{1,2})?$/"',
                    'size_type'     => 'in:'.implode(',', Haul::AVAILABLE_SIZE_TYPES),
                    'catch_date'    => 'required|date|date_format:d.m.Y',
                    'catch_time'    => 'date_format:H:i',
                    'weather'       => 'in:'.implode(',', Haul::AVAILABLE_WEATHER),
                    'temperature'   => 'integer',
                    'user_comment'  => 'string',
                    'public'        => 'boolean',
                    'taken'         => 'boolean',
                    'picture'       => 'mimes:jpeg,jpg,png|max:5000',
                ];
            case 'PUT': // update
                return [
                    'area'          => 'integer|exists:areas,id',
                    'area_name'     => 'required_without:area|string|max:255',
                    'fish'          => 'integer|exists:fishes,id',
                    'count'         => 'integer',
                    'technique'     => 'integer|exists:techniques,id',
                    'ticket'        => 'integer|exists:tickets,id',
                    'ticket_number' => 'required_without:ticket|string|regex:'.Ticket::IDENT_REGEX,
                    'size_value'    => 'required|regex:/^\d*(\.\d{1,2})?$/"',
                    'size_type'     => 'in:'.implode(',', Haul::AVAILABLE_SIZE_TYPES),
                    'catch_date'    => 'required|date|date_format:d.m.Y',
                    'catch_time'    => 'date_format:H:i',
                    'weather'       => 'in:'.implode(',', Haul::AVAILABLE_WEATHER),
                    'temperature'   => 'integer',
                    'user_comment'  => 'string',
                    'public'        => 'boolean',
                    'taken'         => 'boolean',
                    'picture'       => 'mimes:jpeg,jpg,png|max:5000',
                ];
            case 'GET':
            case 'DELETE':
            case 'PATCH':
            default:
                return [];
        }
    }
}
