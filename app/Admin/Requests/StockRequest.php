<?php

namespace App\Admin\Requests;

class StockRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->hasRole('superadmin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'name'      => 'required|max:255',
                    'country'   => 'required|exists:countries,id',
                    'city'      => 'required|string|max:64',
                    'street'    => 'required|string|max:64',
                    'building'  => 'string|max:64',
                    'rest_info' => 'string|max:255',
                    'emails'    => 'array',
                    'emails.*'  => 'email',
                    'phones'    => 'array',
                    'phones.*'  => 'string|max:20',
                ];
            case 'GET':
            case 'DELETE':
            case 'PUT':
            case 'PATCH':
            default:
                return [];
        }
    }
}
