<?php

namespace App\Admin\Requests;

use App\Models\User;
use Auth;

class StoreManagerRequest extends Request
{
    protected $dontFlash = ['rule_files_public', 'rule_files'];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var User $user */
        $user = Auth::user();

        return $user->managers()->first() != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rule_text' => 'string',
            'rule_files' => 'sometimes|array',
            'rule_files.*' => 'mimes:pdf|max:5000|pdf_version:<,1.5',
            'rule_files_public' => 'sometimes|array',
            'rule_files_public.*' => 'mimes:pdf|max:5000|pdf_version:<,1.5',
            'rule_files_name' => 'sometimes|array',
            'rule_public_files_name' => 'sometimes|array',
            'logo' => 'mimes:jpeg,jpg,png|max:5000',
            'signature' => 'mimes:jpeg,jpg,png|max:5000',
            'name' => 'required|string|min:8',
            'street' => 'required|string',
            'area_code' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|exists:countries,id',
            'person' => 'string',
            'phone' => 'string',
            'email' => 'string',
            'website' => 'active_url',
            'commission_included' => 'sometimes|required|boolean',
            'uid' => 'string',
            'ticket_info' => 'string',
            'account_number' => 'string',
            'short_code' => 'sometimes|string|alpha|size:4',
            'templates' => 'sometimes|array',
            'storno_emails' => 'sometimes|array',
            'storno_emails.*' => 'string|email',
        ];
    }

    public function messages()
    {
        return [
            '*.required' => 'Pflichtfeld',
            'short_code.*' => ' ',
            'name.min' => 'Name muss mindestens :min Zeichen lang sein.',
            'rule_files.*.max' => 'Maximale Dateigröße: 5 MByte.',
            'rule_files.*.mimes' => 'Datei(en) müssen im PDF-Format sein.',
            'rule_files_public.*.max' => 'Maximale Dateigröße: 5 MByte.',
            'rule_files_public.*.mimes' => 'Datei(en) müssen im PDF-Format sein.',
            'storno_emails.array' => 'E-Mail-Adresse für Storno-Benachrichtigungen müssen ein Array sein.',
        ];
    }
}
