<?php

namespace App\Admin\Requests;

use App\Models\Promo\Promotion;

class PromotionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->hasRole('superadmin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                return [
                    'type'          => 'required|in:'.implode(',', array_keys(Promotion::AVAILABLE_TYPES)),
                    'valid_from'    => 'required|date|date_format:d.m.Y H:i',
                    'valid_till'    => 'date|date_format:d.m.Y H:i|after:valid_from',
                    'usage_bonus'   => 'required|string|regex:/^\d+\.\d{2}$/|min:1',
                    'owner_bonus'   => 'required_if:type,INVITE|string|regex:/^\d+\.\d{2}$/|min:1',
                    'is_active'     => 'sometimes|required|boolean',
                    'data'                  => 'required|array',
                    'data.*.locale'         => 'required|string|max:5',
                    'data.*.name'           => 'required|string|max:255',
                    'data.*.description'    => 'sometimes|string',
                ];
            case 'GET':
            case 'DELETE':
            case 'PUT':
            case 'PATCH':
            default:
                return [];
        }
    }
}
