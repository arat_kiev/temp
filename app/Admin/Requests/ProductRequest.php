<?php

namespace App\Admin\Requests;

use App\Models\User;
use Auth;

class ProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var User $user */
        $user = Auth::user();

        return $user->managers()->first() != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                return [
                    'category'      => 'required|exists:product_categories,id',
                    'featured_from' => 'date|date_format:d.m.Y|before:featured_till',
                    'featured_till' => 'date|date_format:d.m.Y|after:featured_from',
                    'account_number' => 'string',
                    'template'      => 'string',
                    'max_quantity'  => 'required|integer|min:0|max:1000',
                    'data'          => 'required|array',
                    'data.*.locale'         => 'required|string|max:5',
                    'data.*.name'           => 'required|string|max:255',
                    'data.*.short_description'  => 'sometimes|string',
                    'data.*.long_description'   => 'sometimes|string',
                ];
            case 'GET':
            case 'DELETE':
            case 'PUT':
            case 'PATCH':
            default:
                return [];
        }
    }
}
