<?php

namespace App\Admin\Requests;

use App\Models\User;
use Auth;

class AssignOrganizationsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'organizations' => 'sometimes|required|array|',
            'organizations.*' => 'integer|exists:organizations,id',
        ];
    }

    public function messages()
    {
        return [
            '*.required' => 'Pflichtfeld',
        ];
    }
}
