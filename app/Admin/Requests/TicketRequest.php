<?php

namespace App\Admin\Requests;

use App\Models\Contact\Reseller;

class TicketRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $checkReseller = ($this->query->getInt('resellerId', -1) > 0
            && $this->query->getInt('resellerId', -1) !== Reseller::OFFLINE_TICKETS_PARAM)
            ? true : false;

        switch ($this->method()) {
            case 'GET':
                return [
                    'from'          => 'date_format:Y-m-d',
                    'till'          => 'date_format:Y-m-d',
                    'resellerId'    => 'integer' . ($checkReseller ? '|exists:resellers,id' : ''),
                    'columns'       => 'array',
                    'order'         => 'array',
                    'start'         => 'integer',
                    'length'        => 'integer',
                    'search'        => 'sometimes|array',
                    'search.value'  => 'sometimes|string',
                ];
            case 'POST':
                return [
                    'type'          => 'required|exists:ticket_types,id',
                    'price'         => 'required|exists:ticket_prices,id',
                    'client'        => 'required|exists:clients,id',
                    'valid_from'    => 'required|date_format:d.m.Y H:i',
                    'valid_to'      => 'required|date_format:d.m.Y H:i',
                ];
            default:
                return [];
        }
    }
}
