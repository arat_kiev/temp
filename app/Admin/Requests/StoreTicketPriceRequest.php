<?php

namespace App\Admin\Requests;

use App\Models\User;
use Auth;

class StoreTicketPriceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:5',
            'type' => 'required|string|in:DEFAULT,JUNIOR,SENIOR,MEMBER',
            'value' => 'required|string|regex:/^\d+,\d{2}$/',
            'show_vat' => 'boolean',
            'group_count' => 'integer|min:1|max:15',
            'age' => 'integer|between:1,100|required_if:type,JUNIOR,SENIOR',
            'visible_from' => 'required|date_format:d.m.Y',
            'valid_from' => 'required|date_format:d.m.Y',
            'valid_to' => 'required|date_format:d.m.Y',
            'lock_periods.*.lock_from_string' => 'sometimes|required|date_format:d.m.Y',
            'lock_periods.*.lock_till_string' => 'sometimes|required|date_format:d.m.Y',
        ];
    }

    public function messages()
    {
        return [
            '*.required' => 'Pflichtfeld',
            '*.date_format' => 'Datumsformat: dd.mm.yyyy',
            'value.regex' => 'Preisformat: 123,00',
            'age.required_if' => 'Pflichfeld, wenn Jugend/Senior Preis',
        ];
    }
}
