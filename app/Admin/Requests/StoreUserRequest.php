<?php

namespace App\Admin\Requests;

class StoreUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = $this->get('id');

        switch ($this->method()) {
            case 'GET':
                return [
                    'email' => 'required|email|unique:users,email,' . $userId,
                    'first_name' => 'string',
                    'last_name' => 'string',
                    'street' => 'string',
                    'post_code' => 'string',
                    'city' => 'string',
                    'country' => 'integer|exists:countries,id',
                    'birthday' => 'date',
                    'phone' => 'string',
                    'active' => 'boolean',
                    'newsletter' => 'boolean',
                    'balance' => 'string|regex:/^\d+,\d{2}$/',
                ];
            case 'DELETE':
                return [];
            default:
                return [];
        }

    }

    public function messages()
    {
        return [
            '*.required'    => 'Pflichtfeld',
            'balance.regex' => 'Balanceformat: 123,00',
        ];
    }
}
