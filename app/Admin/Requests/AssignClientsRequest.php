<?php

namespace App\Admin\Requests;

use App\Models\User;
use Auth;

class AssignClientsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->hasRole('superadmin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'clients' => 'sometimes|required|array|',
            'clients.*' => 'integer|exists:clients,id',
        ];
    }

    public function messages()
    {
        return [
            '*.required' => 'Pflichtfeld',
        ];
    }
}
