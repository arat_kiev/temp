<?php

namespace App\Admin\Requests;

class UpdateTimeslotDatesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'month'  => 'required|date_format:m',
            'year' => 'required|date_format:Y',
            'dates' => 'required|array',
            'dates.*' => 'array',
            'dates.*.*' => 'boolean',
        ];
    }
}
