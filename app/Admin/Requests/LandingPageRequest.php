<?php

namespace App\Admin\Requests;
use App\Models\LandingPages\LandingPage;

class LandingPageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                // $slugExtraRule = '|unique:landingpage_translations,slug';
                return [
                    'type'          => 'required|string|in:' . implode(',', LandingPage::getAvailableTypes()),
                    'areas'         => 'required_without:search|array|min:3|exists:areas,id',
                    'picture'       => 'sometimes|file|image|mimes:jpeg,bmp,png',
                    'pic_changed'   => 'required|in:0,1',
                    'data'          => 'required|array',
                    'data.*.locale'         => 'required|string|max:5',
                    'data.*.slug'           => 'required|string|regex:/^[a-z0-9](-?[a-z0-9]+)*$/',
                    'data.*.title'          => 'required|string|max:255',
                    'data.*.headline'       => 'sometimes|string',
                    'data.*.punchline'      => 'sometimes|string',
                    'data.*.description'    => 'sometimes|string',
                    // Search areas by the following params
                    'search'        => 'sometimes|required|array',
                    'search.area_types'     => 'sometimes|array|exists:area_types,id',
                    'search.fishes'         => 'sometimes|array|exists:fishes,id',
                    'search.techniques'     => 'sometimes|array|exists:techniques,id',
                    'search.manager'        => 'sometimes|integer|exists:managers,id',
                    'search.is_ticket'      => 'sometimes|integer|in:0,1',
                    'search.ticket_categories'  => 'sometimes|array|exists:ticket_categories,id',
                    'search.lease'          => 'sometimes|integer|in:0,1',
                    'search.city'           => 'sometimes|integer|in:0,1',
                    'search.region'         => 'sometimes|integer|in:0,1',
                    'search.state'          => 'sometimes|integer|in:0,1',
                    'search.country'        => 'sometimes|integer|in:0,1',
                    'search.member_only'    => 'sometimes|integer|in:0,1',
                    'search.boat'           => 'sometimes|integer|in:0,1',
                    'search.phone_ticket'   => 'sometimes|integer|in:0,1',
                    'search.nightfishing'   => 'sometimes|integer|in:0,1',
                ];
            case 'GET':
                return [
                    'columns'       => 'array',
                    'order'         => 'array',
                    'start'         => 'integer',
                    'length'        => 'integer',
                    'search'        => 'sometimes|array',
                    'search.value'  => 'sometimes|string',
                ];
            default:
                return [];
        }
    }
}
