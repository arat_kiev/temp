<?php

namespace App\Admin\Requests;

use App\Models\User;
use Auth;

class ProductPriceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var User $user */
        $user = Auth::user();

        return $user->managers()->first() != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                return [
                    'value'                     => 'required|string|regex:/^\d+,\d{2}$/',
                    'vat'                       => 'required|integer',
                    'visible_from'              => 'date|date_format:d.m.Y',
                    'valid_from'                => 'required|date|date_format:d.m.Y|before:valid_till',
                    'valid_till'                => 'required|date|date_format:d.m.Y|after:valid_from',
                    'timeslots'                 => 'array',
                    'timeslots.*.from'          => 'required|date_format:H:i',
                    'timeslots.*.till'          => 'required|date_format:H:i',
                    'timeslots.*.default_pool'  => 'required|integer|min:1',
                    'timeDiscounts'             => 'array',
                    'timeDiscounts.*.id'        => 'required|integer|min:0',
                    'timeDiscounts.*.count'     => 'required|integer|min:1',
                    'timeDiscounts.*.inclusive' => 'boolean',
                ];
            case 'GET':
            case 'DELETE':
            case 'PUT':
            case 'PATCH':
            default:
                return [];
        }
    }

    public function messages()
    {
        return [
            'timeslots.*.default_pool.min' => 'Zeitfenster-Menge muss mindestens :min sein.',
            'timeslots.*.from.before' => 'Die erste Uhrzeit muss vor der zweiten liegen.',
            'timeslots.*.*.required' => 'Es fehlen Zeitfenster Eingaben.',
        ];
    }
}
