<?php

namespace App\Admin\Requests;

use App\Models\User;
use Auth;

class StoreMemberRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var User $user */
        $user = Auth::user();

        return $user->organizations()->first() != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'string',
            'last_name' => 'string',
            'function' => 'string',
            'member_id' => 'string',
            'since' => 'date_format:d.m.Y',
            'email' => 'string|email',
            'gender' => 'string|in:MALE,FEMALE',
            'birthday' => 'date_format:d.m.Y',
            'street' => 'string',
            'phone' => 'string',
            'leave_date' => 'date_format:d.m.Y',
            'comment' => 'string',
        ];
    }

    public function messages()
    {
        return [
            '*.required' => 'Pflichtfeld',
        ];
    }
}
