<?php

namespace App\Admin\Requests;

use Auth;

class ProductCategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var \App\Models\User $user */
        $user = Auth::user();

        return $user->managers()->first() != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                return [
                    'picture'       => 'sometimes|file|image|mimes:jpeg,bmp,png',
                    'data'          => 'required|array',
                    'data.*.locale'             => 'required|string|max:5',
                    'data.*.name'               => 'required|string|max:255',
                    'data.*.short_description'  => 'sometimes|string',
                    'data.*.long_description'   => 'sometimes|string',
                ];
            case 'GET':
            case 'DELETE':
            case 'PUT':
            case 'PATCH':
            default:
                return [];
        }
    }
}
