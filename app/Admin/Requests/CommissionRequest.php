<?php

namespace App\Admin\Requests;

use App\Models\User;
use Auth;

class CommissionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return array_merge(
                    $this->getManagerUpdateRules(),
                    $this->getAreaUpdateRules(),
                    $this->getTicketTypeUpdateRules(),
                    $this->getTicketPriceUpdateRules(),
                    $this->getProductUpdateRules(),
                    $this->getProductPriceUpdateRules()
                );
            case 'GET':
            case 'DELETE':
            case 'PUT':
            case 'PATCH':
            default:
                return [];
        }
    }

    private function getManagerUpdateRules() : array
    {
        return [
            'managers'                                          => 'array',
            'managers.*.id'                                     => 'required|integer|exists:managers,id',
            'managers.*.commissions'                            => 'required|array',
            'managers.*.commissions.online'                     => 'array',
            'managers.*.commissions.online.commission_value'    => [
                'required_with:managers.*.commissions.online',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'managers.*.commissions.online.commission_type'     => 'string|in:RELATIVE,ABSOLUTE',
            'managers.*.commissions.online.min_value'           => [
                'present_with:managers.*.commissions.online',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'managers.*.commissions.reseller'                   => 'array',
            'managers.*.commissions.reseller.commission_value'  => [
                'required_with:managers.*.commissions.reseller',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'managers.*.commissions.reseller.commission_type'   => 'string|in:RELATIVE,ABSOLUTE',
            'managers.*.commissions.reseller.min_value'         => [
                'present_with:managers.*.commissions.reseller',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
        ];
    }

    private function getAreaUpdateRules() : array
    {
        return [
            'areas'                                             => 'array',
            'areas.*.id'                                        => 'required|integer|exists:areas,id',
            'areas.*.commissions'                               => 'required|array',
            'areas.*.commissions.online'                        => 'array',
            'areas.*.commissions.online.commission_value'       => [
                'required_with:areas.*.commissions.online',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'areas.*.commissions.online.commission_type'        => 'string|in:RELATIVE,ABSOLUTE',
            'areas.*.commissions.online.min_value'              => [
                'present_with:areas.*.commissions.online',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'areas.*.commissions.reseller'                      => 'array',
            'areas.*.commissions.reseller.commission_value'     => [
                'required_with:areas.*.commissions.reseller',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'areas.*.commissions.reseller.commission_type'      => 'string|in:RELATIVE,ABSOLUTE',
            'areas.*.commissions.reseller.min_value'            => [
                'present_with:areas.*.commissions.reseller',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
        ];
    }

    private function getTicketTypeUpdateRules() : array
    {
        return [
            'ticket_types'                                          => 'array',
            'ticket_types.*.id'                                     => 'required|integer|exists:ticket_types,id',
            'ticket_types.*.commissions'                            => 'required|array',
            'ticket_types.*.commissions.online'                     => 'array',
            'ticket_types.*.commissions.online.commission_value'    => [
                'required_with:ticket_types.*.commissions.online',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'ticket_types.*.commissions.online.commission_type'     => 'string|in:RELATIVE,ABSOLUTE',
            'ticket_types.*.commissions.online.min_value'           => [
                'present_with:ticket_types.*.commissions.online',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'ticket_types.*.commissions.reseller'                   => 'array',
            'ticket_types.*.commissions.reseller.commission_value'  => [
                'required_with:ticket_types.*.commissions.reseller',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'ticket_types.*.commissions.reseller.commission_type'   => 'string|in:RELATIVE,ABSOLUTE',
            'ticket_types.*.commissions.reseller.min_value'         => [
                'present_with:ticket_types.*.commissions.reseller',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
        ];
    }

    private function getTicketPriceUpdateRules() : array
    {
        return [
            'ticket_prices'                                         => 'array',
            'ticket_prices.*.id'                                    => 'required|integer|exists:ticket_prices,id',
            'ticket_prices.*.commissions'                           => 'required|array',
            'ticket_prices.*.commissions.online'                    => 'array',
            'ticket_prices.*.commissions.online.commission_value'   => [
                'required_with:ticket_prices.*.commissions.online',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'ticket_prices.*.commissions.online.commission_type'    => 'string|in:RELATIVE,ABSOLUTE',
            'ticket_prices.*.commissions.online.min_value'          => [
                'present_with:ticket_prices.*.commissions.online',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'ticket_prices.*.commissions.reseller'                  => 'array',
            'ticket_prices.*.commissions.reseller.commission_value' => [
                'required_with:ticket_prices.*.commissions.reseller',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'ticket_prices.*.commissions.reseller.commission_type'  => 'string|in:RELATIVE,ABSOLUTE',
            'ticket_prices.*.commissions.reseller.min_value'        => [
                'present_with:ticket_prices.*.commissions.reseller',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
        ];
    }

    private function getProductUpdateRules() : array
    {
        return [
            'products'                                          => 'array',
            'products.*.id'                                     => 'required|integer|exists:products,id',
            'products.*.commissions'                            => 'required|array',
            'products.*.commissions.online'                     => 'array',
            'products.*.commissions.online.commission_value'    => [
                'required_with:products.*.commissions.online',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'products.*.commissions.online.commission_type'     => 'string|in:RELATIVE,ABSOLUTE',
            'products.*.commissions.online.min_value'           => [
                'present_with:products.*.commissions.online',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'products.*.commissions.reseller'                   => 'array',
            'products.*.commissions.reseller.commission_value'  => [
                'required_with:products.*.commissions.reseller',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'products.*.commissions.reseller.commission_type'   => 'string|in:RELATIVE,ABSOLUTE',
            'products.*.commissions.reseller.min_value'         => [
                'present_with:products.*.commissions.reseller',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
        ];
    }

    private function getProductPriceUpdateRules() : array
    {
        return [
            'product_prices'                                            => 'array',
            'product_prices.*.id'                                       => 'required|integer|exists:product_prices,id',
            'product_prices.*.commissions'                              => 'required|array',
            'product_prices.*.commissions.online'                       => 'array',
            'product_prices.*.commissions.online.commission_value'      => [
                'required_with:product_prices.*.commissions.online',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'product_prices.*.commissions.online.commission_type'       => 'string|in:RELATIVE,ABSOLUTE',
            'product_prices.*.commissions.online.min_value'             => [
                'present_with:product_prices.*.commissions.online',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'product_prices.*.commissions.reseller'                     => 'array',
            'product_prices.*.commissions.reseller.commission_value'    => [
                'required_with:product_prices.*.commissions.reseller',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
            'product_prices.*.commissions.reseller.commission_type'     => 'string|in:RELATIVE,ABSOLUTE',
            'product_prices.*.commissions.reseller.min_value'           => [
                'present_with:product_prices.*.commissions.reseller',
                'string',
                'regex:/^\d+(,|\.)?\d{0,2}$/'
            ],
        ];
    }
}
