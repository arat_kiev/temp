<?php

namespace App\Admin\Requests;

use App\Models\Haul;
use App\Models\Product\ProductStock;
use App\Models\Ticket\Ticket;

class ProductStockRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->hasRole('superadmin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': // create
                return [
                    'product'               => 'required|integer|exists:products,id',
                    'stock'                 => 'required|integer|exists:stocks,id',
                    'quantity'              => 'required|numeric|min:0',
                    'unit'                  => 'string|in:'.implode(',', array_keys(ProductStock::AVAILABLE_UNITS)),
                    'delivery_time'         => 'required|numeric|min:1',
                    'notification_quantity' => 'numeric|min:0',
                ];
            case 'PUT': // update
                return [
                    'stock'                 => 'required|integer|exists:stocks,id',
                    'quantity'              => 'required|numeric|min:0',
                    'unit'                  => 'string|in:'.implode(',', array_keys(ProductStock::AVAILABLE_UNITS)),
                    'delivery_time'         => 'required|numeric|min:1',
                    'notification_quantity' => 'numeric|min:0',
                ];
            case 'GET':
            case 'DELETE':
            case 'PATCH':
            default:
                return [];
        }
    }
}
