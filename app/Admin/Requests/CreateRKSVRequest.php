<?php

namespace App\Admin\Requests;

use Auth;

class CreateRKSVRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->hasRole('superadmin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required|string',
            'company_street' => 'required|string',
            'company_plz' => 'required|string',
            'company_city' => 'required|string',
            'company_country' => 'required|string|in:at,de',
            'user_first_name' => 'required|string',
            'user_last_name' => 'required|string',
            'user_email' => 'required|email',
        ];
    }

    public function messages()
    {
        return [
            '*.required' => 'Pflichtfeld',
        ];
    }
}
