<?php

namespace App\Admin\Requests;

use App\Models\User;
use Auth;

class StoreResellerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|string|min:8',
            'street'    => 'string',
            'area_code' => 'string',
            'city'      => 'string',
            'country'   => 'required|exists:countries,id',
            'person'    => 'string',
            'phone'     => 'string',
            'email'     => 'string',
            'website'   => 'active_url',
            'uid'       => 'string',
        ];
    }

    public function messages()
    {
        return [
            '*.required'    => 'Pflichtfeld',
            'name.min'      => 'Name muss mindestens :min Zeichen lang sein.',
        ];
    }
}
