<?php

namespace App\Admin\Requests;

use App\Models\User;
use Auth;

class StorePictureRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'sometimes|string|min:5',
            'description'   => 'string',
            'priority'      => 'integer|between:0,5',
        ];
    }

    public function messages()
    {
        return [
            '*.required'        => 'Pflichtfeld',
            'name.min'          => 'Name muss mindestens :min Zeichen lang sein.',
            'priority.between'  => 'Priority muss zwischen :min und :max liegen.',
        ];
    }
}
