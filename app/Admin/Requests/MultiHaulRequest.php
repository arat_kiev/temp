<?php

namespace App\Admin\Requests;

class MultiHaulRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->managers()->first() != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'ticket'    => 'required|integer|exists:tickets,id',
                    'catch_date'    => 'required|date|date_format:d.m.Y',
                    'catch_time'    => 'date_format:H:i',
                    'fish'          => 'integer|exists:fishes,id',
                    'taken'         => 'boolean',
                    'size_value'    => 'required|numeric',
                    'size_type'     => 'string',
                    'user_comment'  => 'string',
                    'duration'      => 'integer|min:1|max:23',
                    'fishing_method_id' => 'integer',
                ];
            default:
                return [];
        }
    }
}
