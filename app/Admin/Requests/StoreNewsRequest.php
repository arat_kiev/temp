<?php

namespace App\Admin\Requests;

use Auth;

class StoreNewsRequest extends Request
{
    public function authorize()
    {
        $user = Auth::user();

        return $user->organizations()->first() != null;
    }

    public function rules()
    {
        return [
            'title' => 'required|string',
            'content' => 'required|string',
            'public' => 'sometimes|required|boolean',
            'published_on' => 'date_format:d.m.Y H:i',
            'unpublished_on' => 'date_format:d.m.Y H:i',
        ];
    }

    public function messages()
    {
        return [
            '*.required' => 'Pflichtfeld',
        ];
    }
}
