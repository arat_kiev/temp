<?php

namespace App\Admin\Requests\Reports;

use Auth;
use App\Admin\Requests\Request;

class CommentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                return [
                    'action' => 'required|in:accepted,rejected',
                ];
            case 'GET':
                return [
                    'status' => 'sometimes|in:all,accepted,rejected,review',
                ];
            case 'DELETE':
            case 'PUT':
            case 'PATCH':
            default:
                return [];
        }
    }
}
