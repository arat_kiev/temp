<?php

namespace App\Admin\Requests;

use App\Models\Ticket\TicketInspection;

class TicketInspectionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check()
               && (auth()->user()->hasRole('ticket_inspector') || auth()->user()->inspectedAreas()->first());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'status'    => 'required|string|in:'.implode(',', TicketInspection::AVAILABLE_STATUSES),
                    'comment'   => 'required_unless:status,OK|string',
                    'latitude'  => 'string',
                    'longitude' => 'required_with:latitude|string',
                    'accuracy'  => 'string',
                ];
            default:
                return [];
        }
    }
    public function messages()
    {
        return [
            'comment.required_unless'   => 'Bitte einen Grund eingeben',
        ];
    }
}
