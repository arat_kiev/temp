<?php

namespace App\Admin\Requests;

use App\Models\Contact\Manager;
use Auth;

class StoreAdditionalManagerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var User $user */
        $user = Auth::user();

        return $user->managers()->first() != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'percentage' => 'required|numeric',
            'public' => 'sometimes|boolean',
            'manager_id' => 'required|integer|exists:managers,id',
        ];
    }

    public function messages()
    {
        return [
            '*.required' => 'Pflichtfeld',
        ];
    }
}
