<?php

namespace App\Admin\Requests;

use App\Models\User;
use Auth;

class StoreAreaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var User $user */
        $user = Auth::user();

        return $user->managers()->first() != null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rule_text' => 'string',
            'rule_files' => 'sometimes|array',
            'rule_files.*' => 'mimes:pdf|max:5000|pdf_version:<,1.5',
            'rule_files_public' => 'sometimes|array',
            'rule_files_public.*' => 'mimes:pdf|max:5000|pdf_version:<,1.5',
            'public' => 'sometimes|required|boolean',
            'lease' => 'sometimes|required|boolean',
            'name' => 'required|string|min:2',
            'type' => 'required|integer|exists:area_types,id',
            'season_begin' => 'date_format:d.m',
            'season_end' => 'date_format:d.m',
            'description' => 'string',
            'borders' => 'string',
            'ticket_info' => 'string',
            'fishes' => 'sometimes|required',
            'fishes.*' => 'required|array',
            'fishes.*.id' => 'integer|exists:fishes,id',
            'fishes.*.min_length' => 'integer',
            'fishes.*.max_length' => 'integer',
            'fishes.*.closed_from' => 'date_format:d.m.Y',
            'fishes.*.closed_till' => 'date_format:d.m.Y',
            'techniques' => 'sometimes|required|array',
            'ticket_types' => 'array',
            'techniques.*' => 'integer|exists:techniques,id',
            'locations' => 'required|array',
            'locations.*' => 'required|regex:/^-?\d+\.\d+,\s?-?\d+\.\d+$/',
            'polyfield' => 'string',
        ];
    }

    public function messages()
    {
        return [
            'name.required'             => 'Pflichtfeld',
            'name.min'                  => 'Name muss mindestens :min Zeichen lang sein.',
            'locations.*.regex'         => 'Format is ungültig.',
            'locations.*.required'      => 'Dieses Feld darf nicht leer sein.',
            'rule_files.*.max'          => 'Maximale Dateigröße: 5 MByte.',
            'rule_files.*.mimes'        => 'Datei(en) müssen im PDF-Format sein.',
            'rule_files_public.*.max'   => 'Maximale Dateigröße: 5 MByte.',
            'rule_files_public.*.mimes' => 'Datei(en) müssen im PDF-Format sein.',
        ];
    }
}
