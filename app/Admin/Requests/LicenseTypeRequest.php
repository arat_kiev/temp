<?php

namespace App\Admin\Requests;

use App\Models\License\LicenseType;
use App\Models\User;
use Auth;

class LicenseTypeRequest extends Request
{
    public function authorize()
    {
        return auth()->check() && auth()->user()->hasRole('superadmin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'name'          => 'required|string|min:4',
                    'fields'        => 'sometimes|required',
                    'fishes.*'      => 'array',
                    'fishes.*.field_type' => 'string|in:'.implode(',', LicenseType::ALLOWED_FIELD_TYPES),
                    'fishes.*.field_name' => 'sometimes|required|string|max:255',
                    'attachments'   => 'array',
                    'attachments.*' => 'sometimes|required|string|max:255',
                ];
            default:
                return [];
        }
    }
}
