<?php

namespace App\Admin\Requests\Content;

use App\Admin\Requests\Request;

class LegalDocumentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'internal_name' => 'required|string|min:5|max:255',
            'translations' => 'required|array',
            'translations.*.locale' => 'required|distinct|string|regex:/[a-z]{2}_[A-Z]{2}/|size:5',
            'translations.*.name' => 'required|string|min:5|max:255',
            'translations.*.content' => 'sometimes|string',
            'translations.*.file' => 'sometimes|mimes:pdf',
        ];
    }

    public function messages()
    {
        return [
            'internal_name.min' => 'Name muss mindestens :min Zeichen lang sein.',
            'internal_name.max' => 'Name darf maximal :max Zeichen lang sein.',
            'translations.required' => 'Übersetzungen sind verpflichtend.',
            'translations.*.locale.distinct' => 'Schema muss einzigartig sein.',
            'translations.*.locale.size' => 'Schema muss exact :size Zeichen lang sein.',
            'translations.*.locale.regex' => 'Schema hat ein ungültiges Format.',
            'translations.*.name.min' => 'Name muss mindestens :min Zeichen lang sein.',
            'translations.*.name.max' => 'Name darf maximal :max Zeichen lang sein.',
            'translations.*.file.mimes' => 'Datei muss vom Typ PDF sein',
            '*.required' => 'Pflichtfeld',
        ];
    }
}
