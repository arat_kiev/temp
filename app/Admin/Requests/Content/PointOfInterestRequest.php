<?php

namespace App\Admin\Requests\Content;

use App\Admin\Requests\Request;

class PointOfInterestRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                return [
                    'name'                  => 'required|string|max:255',
                    'street'                => 'string|max:255',
                    'post_code'             => 'string|max:255',
                    'city'                  => 'string|max:255',
                    'email'                 => 'string|max:255',
                    'phone'                 => 'string|max:255',
                    'website'               => 'string|max:255',
                    'country'               => 'sometimes|integer|exists:countries,id',
                    'picture'               => 'sometimes|file|image|mimes:jpeg,bmp,png',
                    'areas'                 => 'sometimes|array',
                    'areas.*'               => 'required|integer|exists:areas,id',
                    'categories'            => 'sometimes|array',
                    'categories.*'          => 'required|integer|exists:point_of_interest_categories,id',
                    'opening_hours'             => 'sometimes|array',
                    'opening_hours.*.day'       => 'required|string|in:MON,TUE,WED,THU,FRI,SAT,SUN',
                    'opening_hours.*.opens'     => 'required|date_format:H:i',
                    'opening_hours.*.closes'    => 'required|date_format:H:i',
                    'data'                  => 'sometimes|array',
                    'data.*.locale'         => 'required|string|max:5',
                    'data.*.description'    => 'sometimes|string',
                ];
            case 'GET':
                return [
                    'columns'       => 'array',
                    'order'         => 'array',
                    'start'         => 'integer',
                    'length'        => 'integer',
                    'search'        => 'sometimes|array',
                    'search.value'  => 'sometimes|string',
                ];
            default:
                return [];
        }
    }
}
