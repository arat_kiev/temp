<?php

namespace App\Admin\Requests\Content;

use App\Admin\Requests\Request;

class StateFishRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                return [
                    'fish'          => 'required|integer|exists:fishes,id',
                    'closed_from'   => 'date|date_format:d.m.Y',
                    'closed_till'   => 'date|date_format:d.m.Y',
                    'min_length'    => 'integer|min:0',
                    'max_length'    => 'integer|min:0',
                ];
            case 'GET':
                return [
                    'columns'       => 'array',
                    'order'         => 'array',
                    'start'         => 'integer',
                    'length'        => 'integer',
                    'search'        => 'sometimes|array',
                    'search.value'  => 'sometimes|string',
                ];
            default:
                return [];
        }
    }
}
