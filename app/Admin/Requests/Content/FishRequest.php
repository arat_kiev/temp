<?php

namespace App\Admin\Requests\Content;

use App\Admin\Requests\Request;

class FishRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                return [
                    'latin'         => 'string|max:255',
                    'min_size'      => 'numeric|regex:/^\d{1,3}\.?\d{0,2}$/',
                    'max_size'      => 'numeric|regex:/^\d{1,3}\.?\d{0,2}$/',
                    'picture'       => 'sometimes|file|image|mimes:jpeg,bmp,png',
                    'category'      => 'integer|exists:fish_categories,id',
                    'data'          => 'required|array',
                    'data.*.locale'         => 'required|string|max:5',
                    'data.*.name'           => 'required|string|max:255',
                    'data.*.description'    => 'sometimes|string',
                ];
            case 'GET':
                return [
                    'columns'       => 'array',
                    'order'         => 'array',
                    'start'         => 'integer',
                    'length'        => 'integer',
                    'search'        => 'sometimes|array',
                    'search.value'  => 'sometimes|string',
                ];
            default:
                return [];
        }
    }
}
