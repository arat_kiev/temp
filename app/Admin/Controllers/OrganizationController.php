<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\OrganizationForm;
use App\Admin\Requests\StoreOrganizationRequest;
use App\Models\Area\Rule;
use App\Models\Picture;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class OrganizationController extends AdminController
{
    use FormBuilderTrait;

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (!$this->getOrganization()) {
                throw new AccessDeniedHttpException();
            }

            return $next($request);
        });
    }

    public function getIndex()
    {
        $organization = $this->getOrganization();

        $form = $this->form(OrganizationForm::class, [
            'method' => 'POST',
            'url' => route('admin::organization.update'),
            'model' => $organization,
        ]);

        return view('admin.organization.edit', compact('form', 'organization'));
    }

    public function postUpdate(StoreOrganizationRequest $request)
    {
        $organization = $this->getOrganization();

        $form = $this->form(OrganizationForm::class, ['model' => $organization]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $organization->fill($request->all());

        // Country
        $organization->country()->associate($request->request->get('country'));

        // Logo
        if ($request->hasFile('logo')) {
            $logo = Picture::create(['file' => $request->file('logo')]);
            if ($organization->logo) {
                $organization->logo->delete();
            }
            $organization->logo()->associate($logo);
        }

        $organization->save();

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::organization.index');
    }
}
