<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\ManagerForm;
use App\Admin\Requests\StoreManagerRequest;
use App\Models\Area\Rule;
use App\Models\Contact\Manager;
use App\Models\Picture;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use View;
use Slugify;

class ManagerController extends AdminController
{
    use FormBuilderTrait;

    public function getIndex()
    {
        $manager = $this->getManager();

        $form = $this->form(ManagerForm::class, [
            'method' => 'POST',
            'url' => route('admin::manager.update'),
            'model' => $manager,
        ]);

        return view('admin.manager.edit', compact('form', 'manager'));
    }

    public function postUpdate(StoreManagerRequest $request)
    {
        $manager = $this->getManager();

        $form = $this->form(ManagerForm::class, ['model' => $manager]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $except = ['rule_text', 'rule_files_delete', 'rule_files', 'rule_files_public', 'storno_emails'];
        $manager->fill($request->except($except));

        // Emails to be notified on storno email event
        $stornoEmails = $request->request->get('storno_emails', []);
        $manager->stornoNotifications()->delete();

        foreach ($stornoEmails as $email) {
            $manager->stornoNotifications()->create([
                'email' => $email,
            ]);
        }

        // Country
        $manager->country()->associate($request->request->get('country'));

        // Logo
        if ($request->hasFile('logo')) {
            $logo = Picture::create(['file' => $request->file('logo')]);
            if ($manager->logo) {
                $manager->logo->delete();
            }
            $manager->logo()->associate($logo);
        }

        // Signature
        if ($request->hasFile('signature')) {
            $signature = Picture::create(['file' => $request->file('signature')]);
            if ($manager->signature) {
                $manager->signature->delete();
            }
            $manager->signature()->associate($signature);
        }

        // Rules
        $this->updateManagerRules($request, $manager);

        $manager->save();

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::manager.index');
    }

    /**
     * Update ticket type rules (public and private attachments)
     *
     * @param StoreManagerRequest $request
     * @param Manager             $manager
     * @throws \Exception
     */
    private function updateManagerRules(StoreManagerRequest $request, Manager $manager)
    {
        $rule = $manager->rule ?: Rule::create([]);

        $rule->text = $request->request->get('rule_text');

        foreach ($request->request->get('rule_files_delete', []) as $fileId) {
            $rule->files()->detach($fileId);
            // TODO: Delete file from `pictures` table and file system
        }

        // non-public rule files
        $this->processRuleFiles(
            $manager,
            $rule,
            $request->file('rule_files', []),
            $request->get('rule_files_name', []),
            false
        );

        // public rule files
        $this->processRuleFiles(
            $manager,
            $rule,
            $request->file('rule_files_public', []),
            $request->get('rule_public_files_name', []),
            true
        );

        if ($rule->isEmpty()) {
            $rule->delete();
            $manager->rule()->dissociate();
        } else {
            $rule->save();
            $manager->rule()->associate($rule);
        }
    }

    /**
     * Process rule files
     *
     * @param Manager                           $manager
     * @param Rule                              $rule
     * @param \Illuminate\Http\UploadedFile[]   $files
     * @param array[]                           $fileNames
     * @param boolean                           $public
     */
    private function processRuleFiles(Manager $manager, Rule $rule, array $files, array $fileNames, $public)
    {
        foreach ($files as $file) {
            if (!is_null($file)) {
                $currentNewName = isset($fileNames[$file->getClientOriginalName()])
                    ? $fileNames[$file->getClientOriginalName()]
                    : null;

                $name = $currentNewName ?: $manager->name . '-bestimmungen';
                $slug = Slugify::slugify($name, '-');

                $rule->files()->save(new Picture(['file' => $file, 'name' => $slug]), ['public' => $public]);
            }
        }
    }
}
