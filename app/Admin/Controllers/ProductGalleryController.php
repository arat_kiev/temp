<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\PictureForm;
use App\Admin\Requests\StorePictureRequest;
use App\Api1\Requests\ImageUploadRequest;
use App\Models\Gallery;
use App\Models\Picture;
use App\Models\Product\Product;
use Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ProductGalleryController extends AdminController
{
    use FormBuilderTrait;

    public function getList($productId)
    {
        $product = Product::findOrFail($productId);
        $gallery = $product->gallery;

        return view('admin.product.gallery.list', compact('gallery', 'product'));
    }

    public function getEdit($productId, $pictureId)
    {
        $product = Product::findOrFail($productId);

        if (!$product->gallery || !($picture = $product->gallery->pictures()->find(['id' => $pictureId])->first())) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->form(PictureForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::products.gallery.save', [
                'productId' => $productId,
                'pictureId' => $pictureId,
            ]),
            'model'     => $picture,
        ]);

        return view('admin.product.gallery.edit', compact('product', 'picture', 'form'));
    }

    public function postSave(StorePictureRequest $request, $productId, $pictureId)
    {
        $product = Product::findOrFail($productId);

        if (!($gallery = $product->gallery) || !($picture = $gallery->pictures()->find(['id' => $pictureId])->first())) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->form(PictureForm::class, ['model' => $picture]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        if ($request->get('priority') == 5) {
            $product->gallery->pictures()
                ->wherePivot('priority', '=', 5)
                ->each(function (Picture $picture) use ($gallery) {
                    $gallery->pictures()->updateExistingPivot($picture->id, ['priority' => 4], false);
                });
        }

        $gallery->pictures()->updateExistingPivot($picture->id, ['priority' => $request->get('priority')], false);
        $picture->update($request->only(['name', 'description']));

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::products.gallery.list', ['productId' => $productId]);
    }

    public function getDelete($productId, $pictureId)
    {
        /** @var Product $prod */
        $prod = Product::findOrFail($productId);

        if (!($gallery = $prod->gallery) || !($picture = $gallery->pictures()->find(['id' => $pictureId])->first())) {
            throw new AccessDeniedHttpException();
        }

        $picture->delete();

        flash()->success('Bild wurde gelöscht.');

        return redirect()->route('admin::products.gallery.list', ['productId' => $productId]);
    }

    public function postUpload(ImageUploadRequest $request, $productId)
    {
        /** @var Product $product */
        $product = Product::findOrFail($productId);

        $picture = new Picture([
            'file'  => $request->file('file'),
            'name'  => $request->file('file')->getClientOriginalName(),
        ]);
        $picture->author()->associate(Auth::user());

        $gallery = $product->gallery ?: Gallery::create(['name' => $product->name]);
        $gallery->pictures()->save($picture);
        $product->gallery()->associate($gallery);
        $product->save();

        return response()->json();
    }
}
