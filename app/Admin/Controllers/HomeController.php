<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class HomeController extends Controller
{
    public function redirectAdminDashboard()
    {
        return redirect()->route('admin::dashboard');
    }

    public function redirectPosTickets()
    {
        return redirect()->route('pos::tickets.index', ['pos' => Route::input('pos')]);
    }
}