<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\ProductForm;
use App\Admin\Requests\ProductRequest;
use App\Api1\Controllers\AreaController as ApiAreaController;
use App\Api1\Controllers\ProductController as ApiProductController;
use App\Models\Area\Area;
use App\Models\Contact\Manager;
use App\Models\Product\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Datatables;
use Html;
use DB;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ProductController extends AdminController
{
    use FormBuilderTrait;

    public function getIndex(Request $request)
    {
        return view('admin.product.index', $this->indexData($request));
    }

    public function getCreate()
    {
        $product = new Product();

        $form = $this->form(ProductForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::products.store', ['productId' => $product->id]),
            'model'     => $product,
        ]);

        return view('admin.product.edit', compact('product', 'form'));
    }

    public function getEdit($productId)
    {
        $manager = $this->getManager();

        $product = Product::findOrFail($productId);

        if (!$product->managers()->where('id', $manager->id)->count()) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->form(ProductForm::class, [
            'method' => 'POST',
            'url' => route('admin::products.update', ['productId' => $product->id]),
            'model' => $product,
        ]);

        return view('admin.product.edit', compact('product', 'form'));
    }

    /**
     * Get product data
     *
     * @param $productId
     * @return mixed
     */
    public function show($productId)
    {
        return Product::findOrFail($productId)->toArray();
    }

    public function postCreate(ProductRequest $request)
    {
        $product = new Product();

        $form = $this->form(ProductForm::class, ['model' => $product]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $product);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::products.edit', ['productId' => $product->id]);
    }
    
    public function postUpdate(ProductRequest $request, $productId)
    {
        $product = Product::findOrFail($productId);

        $form = $this->form(ProductForm::class, ['model' => $product]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $product);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::products.edit', ['productId' => $productId]);
    }

    private function update(ProductRequest $request, Product $product)
    {
        DB::transaction(function () use ($request, $product) {
            // Product category
            $product->category()->associate($request->request->getInt('category'));

            // General data
            $product->featured_from = $request->request->get('featured_from')
                ? Carbon::createFromFormat('d.m.Y', $request->request->get('featured_from'))->startOfDay()
                : null;
            $product->featured_till = $request->request->get('featured_till')
                ? Carbon::createFromFormat('d.m.Y', $request->request->get('featured_till'))->endOfDay()
                : null;
            $product->account_number = empty($request->request->get('account_number')) ? null :
                $request->request->get('account_number');
            $product->template = $request->request->get('template');

            $product->max_quantity = $request->request->get('max_quantity');

            $this->updateTranslations($request, $product);

            $product->save();

            // Manager
            $managerId = $this->getManager()->id;
            if (!$product->managers()->where('id', $managerId)->count()) {
                $product->managers()->attach($managerId);
            }
        });
    }

    private function updateTranslations(ProductRequest $request, Product $product)
    {
        $newData = $request->request->get('data', []);
        $newLocales = array_map(function ($item) {
            return $item['locale'];
        }, $newData);
        $product->translations()->whereNotIn('locale', $newLocales)->delete();

        foreach ($newData as $newItem) {
            $locale = $newItem['locale'];
            $product->translateOrNew($locale)->name = $newItem['name'];
            $product->translateOrNew($locale)->short_description = trim($newItem['short_description']);
            $product->translateOrNew($locale)->long_description = trim($newItem['long_description']);
        }
    }

    public function getAreas(Request $request, $productId)
    {
        $product = Product::findOrFail($productId);

        $areaIds = $product->areas()->pluck('areas.id')->toArray();
        $request->query->set('area_id', $areaIds);
        $request->query->set('_perPage', count($areaIds));

        return with(new ApiAreaController(new Area()))->index($request);
    }

    public function postAreas(Request $request, $productId)
    {
        $product = Product::findOrFail($productId);

        $areas = $request->request->get('areas', []);
        $product->areas()->sync($areas);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::products.index');
    }

    public function allManagerProducts(Request $request)
    {
        $manager = $this->getManager();
        $request->query->set('manager', $manager->id);

        return with(new ApiProductController(new Product()))->index($request);
    }

    public function uploadAttachment($productId, Request $request)
    {
        $product = Product::findOrFail($productId);

        $product->attachment = $request->file('file');
        $product->save();

        return response()->json(['status' => 1]);
    }

    public function deleteAttachment($productId)
    {
        $product = Product::findOrFail($productId);

        $product->attachment = null;
        $product->save();

        flash()->success('Anhang erfolgreich gelöscht');

        return redirect()->route('admin::products.index');
    }

    public function updateHiddenInfo(Request $request, $productId)
    {
        /** @var Product $product */
        $product = Product::findOrFail($productId);

        if (!$product->managers()->find($this->getManager()->id)) {
            throw new AccessDeniedHttpException('Not a product of this manager');
        }

        $info = trim($request->request->get('info', null));

        if (empty($info)) {
            $info = null;
        }

        $product->hidden_info = $info;
        $product->save();

        return response()->json();
    }
    
    public function anyData(Request $request)
    {
        $products = $this->getManagerProducts($this->getManager(), $request->query->getInt('area', -1))
            ->select(['products.id', 'featured_from', 'featured_till', 'hidden_info', 'gallery_id', 'attachment'])
            ->leftJoin('product_translations', 'product_translations.product_id', '=', 'products.id')
            ->with(['category', 'gallery', 'prices', 'areas'])
            ->groupBy('products.id');

        return Datatables::of($products)
            ->filterColumn('name', function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('product_translations.name', 'like', '%'.$request->search['value'].'%');
                }
            })
            ->editColumn('name', function ($product) {
                return Html::linkRoute(
                    'admin::products.edit',
                    str_limit($product->name, 50),
                    ['productId' => $product->id]
                );
            })
            ->editColumn('featured_from', function ($product) {
                return $product->featured_from ? $product->featured_from->format('d.m.Y') : '';
            })
            ->editColumn('featured_till', function ($product) {
                return $product->featured_till ? $product->featured_till->format('d.m.Y') : '';
            })
            ->addColumn('price', function ($product) {
                $price = $product->prices()
                    ->where('valid_from', '<', Carbon::now())
                    ->where('valid_till', '>', Carbon::now())
                    ->first();
                $routeUrl = $price
                    ? route('admin::products.prices.edit', ['product' => $product->id, 'priceId' => $price->id])
                    : route('admin::products.prices.index', ['product' => $product->id]);

                $pricesRoute = '<a href="'. $routeUrl .'"
                                   class="btn btn-xs btn-default pull-right"
                                   role="button">
                                    <i class="fa fa-edit fa-fw"></i>
                                </a>';

                return $price ? "$price->value € $pricesRoute" : $pricesRoute;
            })
            ->addColumn('gallery', function ($product) {
                $route = route('admin::products.gallery.list', ['productId' => $product->id]);

                return '
                    <a href="' . $route . '" class="btn btn-xs btn-primary">
                        <span class="fa fa-picture-o fa-fw"></span>
                        Bilder <span class="badge">
                            ' . ($product->gallery ? $product->gallery->pictures()->count() : 0) . '
                        </span>
                    </a>';
            })
            ->addColumn('areas', function ($product) {
                return '
                    <button type="button" class="btn btn-xs btn-primary"
                            data-action="' . route('admin::products.areas.index', ['productId' => $product->id]) . '"
                            data-target="#area-modal" data-toggle="modal">
                        <span class="fa fa-map-marker fa-fw"></span>
                        Gewässer <span class="badge">
                            ' . ($product->areas ? $product->areas->count() : 0) . '
                        </span>
                    </button>
                ';
            })
            ->addColumn('info_action', function ($product) {
                return '
                    <button type="button" class="btn btn-xs btn-hidden-info ' . ($product->hidden_info ? 'btn-warning' : 'btn-primary') . '"
                            data-text="' . e($product->hidden_info) . '"
                            data-action="' . route('admin::products.info.update', ['productId' => $product->id]) . '">
                        <span class="fa fa-edit fa-fw"></span>
                        ' . ($product->hidden_info ? 'Bearbeiten' : 'Erstellen') . '
                    </button>
                ';
            })
            ->addColumn('attachment', function ($product) {
                $previewRoute = route('imagecache', ['template' => 'download', 'filename' => $product->attachment]);
                $uploadRoute = route('admin::products.attachment.upload', ['productId' => $product->id]);
                $deleteRoute = route('admin::products.attachment.delete', ['productId' => $product->id]);

                $downloadBtn = '
                    <div class="pull-left">
                        <a href="' . $previewRoute . '"
                           type="button"
                           class="btn btn-xs btn-warning"
                           target="_blank">
                            <span class="fa fa-file-archive-o fa-fw"></span>
                        </a>
                    </div>
                ';
                $deleteBtn = '
                    <form action="' . $deleteRoute . '"
                          method="post"
                          class="pull-left delete-attachment"
                          data-id="' . $product->id . '"
                          style="margin-left:2px">
                        ' . method_field('delete') . '
                        <button type="submit" class="btn btn-xs btn-danger">
                            Löschen<span class="fa fa-trash-o fa-fw"></span>
                        </button>
                    </form>
                ';
                $uploadBtn = '
                    <div class="pull-right">
                        <a href="#"
                           class="btn btn-xs btn-success"
                           data-toggle="modal"
                           data-target="#upload-modal"
                           data-action="'. $uploadRoute .'">
                            <span class="fa fa-upload fa-fw"></span> Hochladen
                        </a>
                    </div>
                ';

                return $product->attachment
                    ? $downloadBtn . $deleteBtn . $uploadBtn
                    : $uploadBtn;
            })
            ->removeColumn('category', 'prices', 'short_description', 'long_description')
            ->blacklist(['price_value', 'gallery'])
            ->make(true);
    }

    private function indexData(Request $request) : array
    {
        $manager = $this->getManager();
        $area = $request->query->getInt('area', -1);
        $products = $this->getManagerProducts($manager, $area);

        $count = $products->count();

        $areaList = collect([
            '-1'    => '---',
            '0'     => 'Alle mit Gewässer',
        ]);

        $areas = $manager->areas()
            ->select(['id', 'name'])
            ->without(['rule', 'gallery.pictures', 'ticketTypes', 'resellers', 'locations'])
            ->each(function ($area) use ($areaList) {
                $areaList[$area->id] = $area->name;
            });

        $areaList->merge($areas);

        return compact('count', 'area', 'areaList');
    }

    private function getManagerProducts(Manager $manager, $area)
    {
        $products = $manager->products();

        if ($area > 0) {
            $products->whereHas('areas', function ($query) use ($area) {
                $query->where('id', $area);
            });
        } elseif ($area === 0) {
            $products->has('areas');
        }

        return $products;
    }
}
