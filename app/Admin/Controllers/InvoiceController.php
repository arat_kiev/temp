<?php

namespace App\Admin\Controllers;

use App\Managers\InvoiceManager;
use App\Managers\ProductSaleManager;
use App\Managers\TicketManager;
use App\Models\Contact\Manager;
use App\Models\Product\ProductSale;
use App\Models\Ticket\Invoice;
use App\Models\Ticket\Ticket;
use App\Admin\Forms\InvoiceForm;
use Illuminate\Http\Request;
use App\Admin\Requests\InvoiceRequest;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Carbon\Carbon;
use Datatables;
use DB;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class InvoiceController extends AdminController
{
    use FormBuilderTrait;

    private $isStorno = false;
    private $productSales = [];

    private $ticketSelect = [
        "id",
        "created_at",
        "valid_from",
        "ticket_type_id",
        "ticket_price_id",
        "'ticket' AS 'data_type'"
    ];
    private $productSaleSelect = [
        "id",
        "created_at",
        "null AS valid_from",
        "null AS ticket_type_id",
        "null AS ticket_price_id",
        "'product' AS 'data_type'"
    ];

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (!$this->getManager()) {
                throw new AccessDeniedHttpException();
            }

            return $next($request);
        });
    }

    public function getIndex(Request $request)
    {
        $manager = $this->getManager();
        $reseller = $request->query->getInt('reseller', -1);

        $resellerList = collect([
            '-1' => 'Alle',
        ]);

        $resellers = $manager->resellers()->each(function ($reseller) use ($resellerList) {
            $resellerList[$reseller->id] = $reseller->name;
        });

        $resellerList->merge($resellers);

        return view('admin.invoices.index', compact('reseller', 'resellerList'));
    }

    public function getCreate()
    {
        $invoice = new Invoice();

        $form = $this->form(InvoiceForm::class, [
            'method' => 'POST',
            'url'    => route('admin::invoices.store'),
            'model'  => $invoice,
            'data'   => ['manager' => $this->getManager()],
        ]);

        return view('admin.invoices.create', compact('form', 'invoice'));
    }

    public function getShow($invoiceId)
    {
        $invoice = Invoice::findOrFail($invoiceId);
        $reseller = $invoice->reseller;

        $from = Carbon::parse($invoice->date_from)->format('d.m.Y');
        $till = Carbon::parse($invoice->date_till)->format('d.m.Y');
        $createdAt = Carbon::parse($invoice->created_at)->format('d.m.Y H:i');

        return view('admin.invoices.show', compact('invoice', 'reseller', 'from', 'till', 'createdAt'));
    }

    public function postCreate(InvoiceRequest $request)
    {
        $invoice = new Invoice();

        $form = $this->form(InvoiceForm::class, ['model' => $invoice, 'data' => ['manager' => $this->getManager()]]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        try {
            DB::beginTransaction();
            $this->create($request, $invoice);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            flash()->error($e->getMessage());
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::invoices.show', ['invoiceId' => $invoice->id]);
    }

    private function create(InvoiceRequest $request, Invoice $invoice)
    {
        $dateFrom = Carbon::parse($request->request->get('date_from') ?: 'first day of this month')->toDateString();
        $dateTill = Carbon::parse($request->request->get('date_till') ?: 'last day of this month')->toDateString();

        $invoice->fill([
            'date_from' => $dateFrom,
            'date_till' => $dateTill,
        ]);
        $invoice->reseller()->associate($request->request->getInt('reseller'));
        $invoice->manager()->associate($this->getManager());
        $invoice->save();

        $tickets = Ticket::whereNull('tickets.invoice_id')
            ->whereNull('tickets.storno_id')
            ->whereDate('tickets.created_at', '>=', $dateFrom)
            ->whereDate('tickets.created_at', '<=', $dateTill)
            ->join('ticket_types', 'tickets.ticket_type_id', '=', 'ticket_types.id')
            ->join('areas', 'ticket_types.area_id', '=', 'areas.id')
            ->join('managers', 'areas.manager_id', '=', 'managers.id')
            ->leftJoin('reseller_tickets', 'tickets.id', '=', 'reseller_tickets.ticket_id')
            ->where('managers.id', '=', $this->getManager()->id)
            ->where('reseller_tickets.reseller_id', '=', $request->request->getInt('reseller'))
            ->select('tickets.*')
            ->get();

        $productSales = ProductSale::whereNull('product_sales.invoice_id')
            ->whereNull('product_sales.storno_id')
            ->whereDate('product_sales.created_at', '>=', $dateFrom)
            ->whereDate('product_sales.created_at', '<=', $dateTill)
            ->join('products', 'product_sales.product_id', '=', 'products.id')
            ->join('has_products', function ($join) {
                $join->on('has_products.product_id', '=', 'products.id');
                $join->where('has_products.related_type', '=', 'managers');
            })
            ->join('managers', 'has_products.related_id', '=', 'managers.id')
            ->where('managers.id', '=', $this->getManager()->id)
            ->where('product_sales.reseller_id', '=', $request->request->getInt('reseller'))
            ->select('product_sales.*')
            ->get();

        if (!$tickets->count() && !$productSales->count()) {
            throw new \Exception('Es gibt keine Angelkarten oder Produkte für die Abrechnung');
        }

        foreach ($tickets as $ticket) {
            $ticket->invoice_id = $invoice->id;
            $ticket->save();
        }

        foreach ($productSales as $productSale) {
            $productSale->invoice_id = $invoice->id;
            $productSale->save();
        }
    }

    public function anyData(Request $request, InvoiceManager $invoiceManager)
    {
        $manager = $this->getManager();
        $reseller = $request->query->getInt('reseller', -1);

        $invoices = $this->getManagerInvoices($manager, $reseller);

        return Datatables::of($invoices)
            ->addColumn('invoice_number', function ($invoice) use ($invoiceManager) {
                return $invoiceManager->getInvoiceNumber($invoice);
            })
            ->addColumn('tickets_count', function ($invoice) {
                return ($invoice->tickets->count() + $invoice->productSales->count()) ?: '<span class="fa fa-times fa-fw"></span>';
            })
            ->addColumn('reseller', function($invoice) {
                return $invoice->reseller
                    ? str_limit($invoice->reseller->name, 30)
                    : null;
            })
            ->addColumn('actions', function($invoice) {
                $showRoute = route('admin::invoices.show', ['invoiceId' => $invoice->id]);
                $removeRoute = route('admin::invoices.storno', ['invoiceId' => $invoice->id]);

                return '
                    <button type="button"
                            class="btn btn-xs btn-primary"
                            data-target="#preview-modal"
                            data-toggle="modal"
                            data-invoice-id="' . $invoice->id . '">
                        <span class="fa fa-eye fa-fw"></span>
                    </button>
                    <a href="'.route('preview.invoice', ['invoice' => $invoice->id, 'format' => 'pdf']).'"
                       type="button"
                       class="btn btn-xs btn-warning"
                       target="_blank">
                        <span class="fa fa-file-pdf-o fa-fw"></span>
                    </a>
                    <a href="' . $showRoute . '" class="btn btn-xs btn-primary">
                        <span class="fa fa-ticket fa-fw"></span>
                        Produkte
                    </a>
                    <button type="button"
                            class="btn btn-xs btn-danger"
                            data-target="#delete-modal"
                            data-toggle="modal"
                            data-action="' . $removeRoute . '">Stornieren
                        <span class="fa fa-trash-o fa-fw"></span>
                    </button>
                ';
            })
            ->make(true);
    }

    private function getManagerInvoices(Manager $manager, $reseller = -1)
    {
        $query = Invoice::select('invoices.*')
            ->join('managers', 'managers.id', '=', 'invoices.manager_id')
            ->where(function($query) use ($manager) {
                $query->where('managers.id', '=', $manager->id)
                    ->orWhere('managers.parent_id', '=', $manager->id);
            })
            ->whereNotNull('invoices.reseller_id');

        if ($this->isStorno) {
            // show storned ticket
            $query->whereNotNull('invoices.storno_id');
            $query->withTrashed();
        } else {
            // show active ticket
            $query->whereNull('invoices.storno_id');
            $query->where('invoices.is_deleted', 0);
        }

        if ($reseller > 0) {
            $query->where('invoices.reseller_id', '=', $reseller);
        }

        return $query;
    }

    public function invoiceData($invoiceId, TicketManager $ticketManager, ProductSaleManager $productSaleManager)
    {
        $invoice = Invoice::findOrFail($invoiceId);
        $tickets = Ticket::selectRaw(implode(', ', $this->ticketSelect))
            ->with(['type.area.manager', 'price', 'user.country', 'resellerTicket.country', 'hauls'])
            ->where('invoice_id', '=', $invoice->id);
        $productSale = ProductSale::selectRaw(implode(',', $this->productSaleSelect))
            ->with('user')
            ->where('invoice_id', '=', $invoice->id);

        $results = $tickets->unionAll($productSale);

        return Datatables::of($results)
            ->addColumn('manager_ticket_id', function ($ticket) use ($ticketManager) {
                if ($ticket->data_type === 'product') {
                    return null;
                }
                return $ticketManager->getManagerIdentifier($ticket);
            })
            ->addColumn('ticket_number', function($ticket) use ($ticketManager, $productSaleManager) {
                if ($ticket->data_type === 'product') {
                    $productSale = $this->fetchProductSale($ticket->id);
                    return $productSaleManager->getIdentifier($productSale);
                }
                return $ticketManager->getIdentifier($ticket);
            })
            ->addColumn('user_full_name', function ($ticket) {
                if ($ticket->data_type === 'product') {
                    $product = $this->fetchProductSale($ticket->id);
                    list($name, $email) = $this->fetchProductSaleUserData($product);
                } else {
                    list($name, $email) = $this->fetchTicketUserData($ticket);
                }

                if (!empty($email)) {
                    $name .= ' <a href="mailto:'.$email.'">
                                    <span class="fa fa-envelope fa-fw"></span>
                               </a>';
                }

                return $name;
            })
            ->addColumn('area_name', function ($ticket) {
                if ($ticket->data_type === 'product') {
                    $productSale = $this->fetchProductSale($ticket->id);
                    $areas = array_filter($productSale->product->areas()->pluck('name')->toArray(), 'strlen');
                    return str_limit(implode($areas, ', '), 45);
                }
                return str_limit($ticket->type->area->name, 30);
            })
            ->addColumn('hauls_count', function ($ticket) {
                return $ticket->hauls->count() > 0
                    ? '<span class="fa fa-check fa-fw"></span>'
                    : '<span class="fa fa-times fa-fw"></span>';
            })
            ->addColumn('type_name', function ($ticket) {
                if ($ticket->data_type === 'product') {
                    return 'Produkte';
                }
                return str_limit($ticket->type->name, 30);
            })
            ->addColumn('valid_from', function ($ticket) use ($ticketManager) {
                if ($ticket->data_type === 'product') {
                    $productSale = $this->fetchProductSale($ticket->id);
                    return $productSale->product->featured_from
                        ? $productSale->product->featured_from->format('d.m.Y')
                        : '';
                }
                $validFrom = $ticketManager->validFrom($ticket->price, $ticket->valid_from);

                return $ticket->price->ticketType->category->type === 'HOUR'
                    ? $validFrom->format('d.m.Y - H:i')
                    : $validFrom->format('d.m.Y');
            })
            ->editColumn('created_at', function ($ticket) {
                return $ticket->created_at->format("d.m.Y - H:i");
            })
            ->addColumn('price_compact', function ($ticket) {
                if ($ticket->data_type === 'product') {
                    $productSale = $this->fetchProductSale($ticket->id);
                    return $productSale->price->value . ' €';
                }
                return $ticket->price->name.' ('.number_format($ticket->price->value / 100.0, 2, ",", ".").' €)';
            })
            ->addColumn('price_brutto', function ($ticket) {
                if ($ticket->data_type === 'product') {
                    $productSale = $this->fetchProductSale($ticket->id);
                    return $productSale->price->value . ' €';
                }
                return number_format($ticket->price->value / 100.0, 2, ",", ".").' €';
            })
            ->addColumn('user', function ($ticket) {
                return $ticket->data_type === 'product'
                    ? $this->fetchProductSale($ticket->id)->user->toArray()
                    : ($ticket->user
                        ? $ticket->user->toArray()
                        : []);
            })
            ->addColumn('price', function ($ticket) {
                return $ticket->data_type === 'product'
                    ? $this->fetchProductSale($ticket->id)->price->toArray()
                    : ($ticket->price
                        ? $ticket->price->toArray()
                        : null);
            })
            ->addColumn('actions', function ($ticket) {
                $removeRoute = $ticket->data_type === 'product'
                    ? route('admin::sales.product.storno', ['saleId' => $ticket->id])
                    : route('admin::sales.ticket.storno', ['ticketId' => $ticket->id]);
                return '
                    <button type="button"
                            class="btn btn-xs btn-danger"
                            data-target="#delete-modal"
                            data-toggle="modal"
                            data-action="' . $removeRoute . '">Stornieren
                        <span class="fa fa-trash-o fa-fw"></span>
                    </button>
                ';
            })
            ->blacklist(['manager_ticket_id', 'user_full_name', 'type_area_name', 'valid_from', 'reseller'])
            ->make(true);
    }

    public function postStornoInvoice($invoiceId, Request $request) {
        /** @var Invoice $invoice */
        if ($invoice = Invoice::find($invoiceId)) {
            try {
                DB::beginTransaction();
                Invoice::$stornoReason = $request->get('reason', '');
                $invoice->delete();

                // create storned invoice
                Invoice::create([
                    'storno_id' => $invoice->id,
                    'date_from' => $invoice->date_from,
                    'date_till' => $invoice->date_till,
                ]);
                DB::commit();
                $result = ['notification' => 'success', 'message' => 'Abrechnung wurde gelöscht'];
            } catch (\Exception $e) {
                DB::rollBack();
                $result = ['notification' => 'danger', 'message' => $e->getMessage()];
            }
        } else {
            $result = ['notification' => 'danger', 'message' => 'Abrechnung wurde nicht gefunden'];
        }

        return response()->json($result);
    }

    private function fetchProductSale($productSaleId)
    {
        if (isset($this->productSales[$productSaleId])) {
            return $this->productSales[$productSaleId];
        }

        $productSale = $this->productSales[$productSaleId] = ProductSale::select([
                'id', 'first_name', 'last_name', 'email', 'user_id', 'reseller_id', 'price_id', 'product_id'
            ])
            ->with(['user', 'reseller', 'price', 'product.areas', 'user.country'])
            ->find($productSaleId);

        return $productSale;
    }

    private function fetchTicketUserData(Ticket $ticket) : array
    {
        $name = $ticket->user
            ? $ticket->user->first_name.' '.$ticket->user->last_name
            : ($ticket->resellerTicket
                ? $ticket->resellerTicket->first_name . ' ' . $ticket->resellerTicket->last_name
                : '');
        $email = $ticket->user
            ? $ticket->user->email
            : ($ticket->resellerTicket
                ? $ticket->resellerTicket->email
                : '');

        return [$name, $email];
    }

    private function fetchProductSaleUserData(ProductSale $productSale) : array
    {
        $name = $productSale->user
            ? $productSale->user->first_name . ' ' . $productSale->user->last_name
            : ($productSale->reseller
                ? $productSale->reseller->name
                : $productSale->first_name . ' ' . $productSale->last_name);
        $email = $productSale->user
            ? $productSale->user->email
            : ($productSale->reseller
                ? $productSale->reseller->email
                : $productSale->email);

        return [$name, $email];
    }
}