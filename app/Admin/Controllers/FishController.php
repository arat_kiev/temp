<?php

namespace App\Admin\Controllers;

use App\Models\Meta\Fish;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class FishController extends AdminController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (!$this->getManager()) {
                throw new AccessDeniedHttpException();
            }

            return $next($request);
        });
    }
    
    public function areaFishes(int $areaId)
    {
        return Fish::whereHas('areas', function ($areas) use ($areaId) {
            $areas->whereId($areaId);
        })->get();
    }
}
