<?php

namespace App\Admin\Controllers;

use App\Events\LicenseChanged;
use App\Models\Client;
use App\Models\License\License;
use App\Models\User;
use Auth;
use Event;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class LicenseController extends AdminController
{
    public function getAccept($licenseId)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        }

        $license = License::findOrFail($licenseId);

        if ($license->status !== 'ACCEPTED') {
            $license->status = 'ACCEPTED';
            $license->save();

            Event::fire(new LicenseChanged($license, Client::where('name', '=', 'bissanzeiger')->first()));

            return response('Berechtigung wurde freigeschalten');
        }

        return response('Berechtigung bereits aktiviert');
    }
}
