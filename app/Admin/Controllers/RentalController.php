<?php

namespace App\Admin\Controllers;

use App\Admin\Requests\CreateRentalReservationRequest;
use App\Models\Product\TimeslotDate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class RentalController extends AdminController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (!$this->getRental()) {
                throw new AccessDeniedHttpException();
            }

            return $next($request);
        });
    }

    public function getIndex(Request $request)
    {
        $products = $this->getProducts();

        try {
            $firstDay = Carbon::createFromFormat('Y-m-d', $request->request->get('date'))->startOfWeek();
        } catch (\Exception $e) {
            $firstDay = Carbon::now()->startOfWeek();
        }

        return view(
            request('view') === 'listing' ?
                'admin.rental.listing' :
                'admin.rental.index',
            compact('products', 'firstDay')
        );
    }

    public function getReservations(Request $request, $timeslotDateId)
    {
        /** @var TimeslotDate $timeslotDate */
        $timeslotDate = TimeslotDate::findOrFail($timeslotDateId);

        if (!$this->getProducts()->where('id', $timeslotDate->timeslot->productPrice->product->id)->first()) {
            throw new AccessDeniedHttpException('not allowed');
        }

        $sales = $timeslotDate->productSales;
        $reservations = $timeslotDate->reservations;

        return view('admin.rental.reservations', compact('sales', 'reservations'));
    }

    public function postManualReservation(CreateRentalReservationRequest $request)
    {
        $timeslotDates = collect($request->request->get('timeslot_dates', []));

        $timeslotDates->each(function ($timeslotDateId) use ($request) {
            if (!($timeslotDate = TimeslotDate::find($timeslotDateId))) {
                return;
            }

            if (!$this->getProducts()->where('id', $timeslotDate->timeslot->productPrice->product->id)->first()) {
                return;
            }

            if (!$timeslotDate->hasReservationsAvailable()) {
                return;
            }

            $timeslotDate->reservations()->create(['notice' => $request->request->get('notice')]);
        });

        return response()->json();
    }

    public function removeManualReservation(Request $request)
    {
        $timeslotDateId = $request->request->getInt('timeslot_date', 0);

        if (!($timeslotDate = TimeslotDate::find($timeslotDateId))) {
            return;
        }

        if (!$this->getProducts()->where('id', $timeslotDate->timeslot->productPrice->product->id)->first()) {
            return;
        }

        $timeslotDate->increment('pool');
    }

    private function getAllAreasFromProducts($products) {
        $areas = array();
        foreach ($products as $product) {
            if($product->areas()->count() > 0) {
                $area = $product->areas()->pluck('name');
                array_push($areas, $area);
            } else {
                array_push($areas, $product->pluck('name'));
            }
        }
        return $areas;
    }

    private function getProducts()
    {
        $products = $this->getRental()->products()->hasActivePrice()->get();

        $items = $products->all();

        $list = $this->getAllAreasFromProducts($products);

        array_multisort($list, SORT_ASC, SORT_NATURAL, $items);

        return collect(array_values($items));
    }
}
