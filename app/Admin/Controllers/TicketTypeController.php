<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\TicketTypeForm;
use App\Traits\ModelQuery;
use Illuminate\Http\Request;
use App\Admin\Requests\StoreTicketTypeRequest;
use App\Models\User;
use App\Models\Area\Area;
use App\Models\Area\Rule;
use App\Models\Picture;
use App\Models\Ticket\TicketType;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Slugify;
use Route;
use Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use View;

class TicketTypeController extends AdminController
{
    use FormBuilderTrait,
        ModelQuery;

    /**
     * @param $areaId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex($areaId)
    {
        $manager = $this->getManager();
        $area = Area::findOrFail($areaId);

        if ($manager->id != $area->manager_id && $manager->id != $area->manager->parent_id) {
            throw new AccessDeniedHttpException();
        }

        $users = array_merge(
            $area->manager->admins()->pluck('id', 'name')->toArray(),
            User::whereHas('roles', function ($query) {
                $query->where('name', 'superadmin');
            })->pluck('id', 'name')->toArray()
        );

        $users = json_encode(array_map(
            function ($k, $v) {
                return ['id' => $v, 'text' => $k];
            },
            array_keys($users),
            $users
        ));

        $additionalAreas = array_merge(
            Auth::check() && Auth::user()->hasRole('superadmin')
                ? Area::where('id', '!=', $areaId)->get()->pluck('id', 'name')->toArray()
                : $area->manager->areas()->where('id', '!=', $areaId)->get()->pluck('id', 'name')->toArray(),
           []
        );

        $additionalAreas = json_encode(array_map(
            function ($k, $v) {
                return ['id' => $v, 'text' => $k];
            },
            array_keys($additionalAreas),
            $additionalAreas
        ));

        return view('admin.tickettypes.index', compact('area', 'users', 'additionalAreas'));
    }

    public function getAdditionalAreas($areaId, $ticketTypeId)
    {
        $area = Area::findOrFail($areaId);

        $areas = array_merge(
            Auth::check() && Auth::user()->hasRole('superadmin')
                ? Area::where('id', '!=', $areaId)->get()->pluck('id', 'name')->toArray()
                : $area->manager->areas()->where('id', '!=', $areaId)->get()->pluck('id', 'name')->toArray(),
            TicketType::find($ticketTypeId)
                ->additionalAreas()
                ->pluck('id', 'name')
                ->toArray()
        );

        $areas = array_map(
            function ($k, $v) {
                return ['id' => $v, 'text' => $k];
            },
            array_keys($areas),
            $areas
        );

        return response()->json($areas);
    }

    /**
     * @param $areaId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate($areaId)
    {
        $manager = $this->getManager();
        $area = $this->findOrAccessDenied($manager->areas(), $areaId);

        $ticketType = new TicketType();

        $form = $form = $this->form(TicketTypeForm::class, [
            'method' => 'POST',
            'url' => route('admin::areas.tickettypes.store', ['areaId' => $areaId]),
            'model' => $ticketType,
        ]);

        return view('admin.tickettypes.edit', compact('area', 'ticketType', 'form'));
    }

    /**
     * @param StoreTicketTypeRequest $request
     * @param $areaId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postStore(StoreTicketTypeRequest $request, $areaId)
    {
        $manager = $this->getManager();
        $area = $this->findOrAccessDenied($manager->areas(), $areaId);

        $ticketType = new TicketType();
        $form = $this->form(TicketTypeForm::class, ['model' => $ticketType]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $ticketType, $area);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::areas.tickettypes.edit', [
            'areaId'        => $area->id,
            'ticketTypeId'  => $ticketType->id,
        ]);
    }

    /**
     * @param $areaId
     * @param $ticketTypeId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($areaId, $ticketTypeId)
    {
        $manager = $this->getManager();

        $area = Area::findOrFail($areaId);

        if ($manager->id != $area->manager_id && $manager->id != $area->manager->parent_id) {
            throw new AccessDeniedHttpException();
        }

        $ticketType = $this->findOrAccessDenied($area->ticketTypes(), $ticketTypeId);

        $form = $this->form(TicketTypeForm::class, [
            'method' => 'POST',
            'url' => route('admin::areas.tickettypes.update', ['areaId' => $areaId, 'ticketTypeId' => $ticketTypeId]),
            'model' => $ticketType,
        ]);

        return view('admin.tickettypes.edit', compact('form', 'area', 'ticketType'));
    }

    /**
     * @param StoreTicketTypeRequest $request
     * @param $areaId
     * @param $ticketTypeId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postUpdate(StoreTicketTypeRequest $request, $areaId, $ticketTypeId)
    {
        $manager = $this->getManager();

        $area = $this->findOrAccessDenied($manager->areas(), $areaId);
        $ticketType = $this->findOrAccessDenied($area->ticketTypes(), $ticketTypeId);

        $form = $this->form(TicketTypeForm::class, ['model' => $ticketType]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $ticketType, $area);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::areas.tickettypes.edit', [
            'areaId'        => $areaId,
            'ticketTypeId'  => $ticketTypeId,
        ]);
    }

    /**
     * @param StoreTicketTypeRequest $request
     * @param TicketType $ticketType
     * @param Area $area
     * @throws \Exception
     */
    private function update(StoreTicketTypeRequest $request, TicketType $ticketType, Area $area)
    {
        $area->approved = true;
        $area->save();

        // Area
        if (!$ticketType->area) {
            $ticketType->area()->associate($area);
        }

        // Standard values
        $ticketType->category()->associate($request->request->getInt('category'));
        $ticketTypeData = $this->parsePostRequestData($request);

        if ($ticketType->prices()->count()) {
            unset($ticketTypeData['group']);
        }

        $ticketType->fill($ticketTypeData);
        $ticketType->save();

        if ($request->request->getInt('checkin_id', 0)) {
            $ticketType->checkinUnit()->associate($request->request->getInt('checkin_id'));
        } else {
            $ticketType->checkinUnit()->dissociate();
        }

        if ($request->request->getInt('quota_id', 0)) {
            $ticketType->quotaUnit()->associate($request->request->getInt('quota_id'));
        } else {
            $ticketType->quotaUnit()->dissociate();
        }

        $ticketType->requiredLicenseTypes()->sync($request->request->get('required_licenses', []));
        $ticketType->optionalLicenseTypes()->sync($request->request->get('optional_licenses', []));

        // Rules
        $this->updateTicketTypeRules($request, $ticketType);

        $ticketType->save();
    }

    /**
     * Return an array of parsed request data to save in DB
     *
     * @param StoreTicketTypeRequest $request
     * @return array
     */
    private function parsePostRequestData(StoreTicketTypeRequest $request)
    {
        return [
            'online_only'   => false,
            'name'          => $request->request->get('name'),
            'is_catchlog_required'  => $request->request->getBoolean('is_catchlog_required', false),
            'duration'      => $request->request->get('duration'),
            'group'         => $request->request->get('group', false),
            'checkin_max'   => $request->request->get('checkin_max', null),
            'quota_max'     => $request->request->get('quota_max', null),

            'day_starts_at_sunrise' => $request->request->getBoolean('day_starts_at_sunrise', false),
            'day_starts_at_sunset'  => $request->request->getBoolean('day_starts_at_sunset', false),
            'day_ends_at_sunrise'   => $request->request->getBoolean('day_ends_at_sunrise', false),
            'day_ends_at_sunset'    => $request->request->getBoolean('day_ends_at_sunset', false),

            'day_starts_at_time'    => $request->request->get('day_starts_at_time', null),
            'day_ends_at_time'      => $request->request->get('day_ends_at_time', null),

            'fish_per_day'  => $request->request->get('fish_per_day'),
            'fishing_days'  => $request->request->get('fishing_days', null),
        ];
    }

    /**
     * Update ticket type rules (public and private attachments)
     *
     * @param StoreTicketTypeRequest $request
     * @param TicketType             $ticketType
     * @throws \Exception
     */
    private function updateTicketTypeRules(StoreTicketTypeRequest $request, TicketType $ticketType)
    {
        $rule = $ticketType->rule ?: Rule::create([]);

        $rule->text = $request->request->get('rule_text');

        foreach ($request->request->get('rule_files_delete', []) as $fileId) {
            $rule->files()->detach($fileId);
        }

        // non-public rule files
        $this->processRuleFiles(
            $ticketType,
            $rule,
            $request->file('rule_files', []),
            $request->get('rule_files_name', []),
            false
        );

        // public rule files
        $this->processRuleFiles(
            $ticketType,
            $rule,
            $request->file('rule_files_public', []),
            $request->get('rule_public_files_name', []),
            true
        );

        if ($rule->isEmpty()) {
            $rule->delete();
            $ticketType->rule()->dissociate();
        } else {
            $rule->save();
            $ticketType->rule()->associate($rule);
        }
    }

    /**
     * Partial edit
     *
     * @param \Illuminate\Http\Request              $request
     * @param                                       $areaId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function partialUpdate(Request $request, $areaId)
    {
        TicketType::find($request->get('id'))
            ->productBasedNotifications()
            ->sync($request->get('users', []));

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::areas.tickettypes.index', $areaId);
    }

    /**
     * Additional areas partial update
     *
     * @param Request $request
     * @param $areaId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function additionalAreasUpdate(Request $request, $areaId)
    {
        TicketType::find($request->get('id'))
            ->additionalAreas()
            ->sync($request->get('additional-areas', []));

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::areas.tickettypes.index', $areaId);
    }

    /**
     * Process rule files
     *
     * @param TicketType                        $ticketType
     * @param Rule                              $rule
     * @param \Illuminate\Http\UploadedFile[]   $files
     * @param array[]                           $fileNames
     * @param boolean                           $public
     */
    private function processRuleFiles(TicketType $ticketType, Rule $rule, array $files, array $fileNames, $public)
    {
        foreach ($files as $file) {
            if (!is_null($file)) {
                $currentNewName = isset($fileNames[$file->getClientOriginalName()])
                    ? $fileNames[$file->getClientOriginalName()]
                    : null;

                $name = $currentNewName ?: $ticketType->name . '-bestimmungen';
                $slug = Slugify::slugify($name, '-');

                $rule->files()->save(new Picture(['file' => $file, 'name' => $slug]), ['public' => $public]);
            }
        }
    }
}
