<?php

namespace App\Admin\Controllers;

use App\Managers\Export\TicketInspectionExportManager as ExportManager;
use App\Models\Ticket\TicketInspection;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Datatables;
use Html;

class TicketInspectionController extends AdminController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (!$this->getManager()) {
                throw new AccessDeniedHttpException();
            }

            return $next($request);
        });
    }

    public function getIndex(Request $request, int $inspectorId)
    {
        if (!$this->getManager()->inspectors()->whereId($inspectorId)->count()) {
            throw new AccessDeniedHttpException('You have no permission to see this inspectors inspections');
        }

        return view('admin.inspection.index', $this->indexData($request, $inspectorId));
    }
    
    public function anyData(Request $request, int $inspectorId)
    {
        if (!$this->getManager()->inspectors()->whereId($inspectorId)->count()) {
            throw new AccessDeniedHttpException('You have no permission to see this inspectors inspections');
        }

        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $area = $request->query->getInt('area', 0);

        $inspections = $this->fetchInspections($inspectorId, $from, $till, $area)
            ->orderBy('created_at', 'desc');

        return Datatables::of($inspections)
            ->editColumn('status', function (TicketInspection $inspection) {
                return $inspection->status == 'OK'
                    ? '<span class="fa fa-fw fa-check"></span>'
                    : '<span class="fa fa-fw fa-times"></span>';
            })
            ->addColumn('fisher', function (TicketInspection $inspection) {
                $userFullName = $inspection->ticket->user
                    ? $inspection->ticket->user->full_name
                    : $inspection->ticket->resellerTicket->full_name;
                $userId = $inspection->ticket->user ? $inspection->ticket->user->id : null;

                switch (true) {
                    case (auth()->user()->hasRole('superadmin') && $userId) :
                        return Html::linkRoute(
                            'admin::super:users.edit',
                            "{$userFullName} ({$userId})",
                            ['userId' => $userId]
                        );
                    case ($userId) :
                        return "{$userFullName} ({$userId})";
                    default :
                        return $userFullName;
                }
            })
            ->addColumn('ticket', function (TicketInspection $inspection) {
                return '
                    <button type="button"
                            class="btn btn-xs btn-primary btn-datatables"
                            data-target="#preview-modal"
                            data-toggle="modal"
                            data-ticket-id="'.$inspection->ticket->id.'">
                        <span class="fa fa-eye fa-fw"></span>
                        Vorschau
                    </button>
                    <a  href="'. route('preview.ticket', ['ticket' => $inspection->ticket->id, 'format' => 'pdf']). '"
                        type="button"
                        class="btn btn-xs btn-warning btn-datatables"
                        target="_blank">
                        <span class="fa fa-file-pdf-o fa-fw"></span>
                        Angelkarte (PDF)
                    </a>
                ';
            })
            ->editColumn('created_at', function (TicketInspection $inspection) {
                return $inspection->created_at->format('d.m.Y H:i');
            })
            ->make(true);
    }

    public function getExport(Request $request, int $inspector, $format = 'xls', ExportManager $exportManager)
    {
        if ($format != 'xls') {
            throw new \Exception('Invalid format parameter');
        } elseif (!$this->getManager()->inspectors()->whereId($inspector)->count()) {
            throw new AccessDeniedHttpException('You have no permission to see this inspectors inspections');
        }

        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $area = $request->query->getInt('area', -1);

        $inspections = $this->fetchInspections($inspector, $from, $till, $area)->orderBy('created_at', 'desc');

        $file = $exportManager->run($inspections, $format, compact('from', 'till', 'area', 'inspector'));

        return response()->download($file);
    }

    private function indexData(Request $request, int $inspectorId)
    {
        $inspector = User::findOrFail($inspectorId);
        $inspectorName = $inspector->full_name ?: $inspector->email;

        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $area = $request->query->getInt('area', 0);

        $count = $this->fetchInspections($inspectorId, $from, $till, $area)->count();

        $areaList = collect([
            '0'     => 'Alle',
        ]);

        $areas = $inspector->inspectedAreas()->each(function ($area) use ($areaList) {
            $areaList[$area->id] = $area->name;
        });

        $areaList->merge($areas);

        return compact('inspectorId', 'inspectorName', 'count', 'from', 'till', 'areaList', 'area');
    }

    private function fetchInspections(int $inspectorId, $from = null, $till = null, int $areaId = 0)
    {
        $inspections = TicketInspection::where('inspector_id', $inspectorId);

        if ($from && $till) {
            try {
                $from = Carbon::createFromFormat('Y-m-d', $from)->startOfDay();
                $till = Carbon::createFromFormat('Y-m-d', $till)->endOfDay();

                $inspections->whereBetween('created_at', [$from, $till]);
            } catch (\InvalidArgumentException $e) {
            }
        }

        if ($areaId > 0) {
            $inspections->whereHas('ticket.type.area', function ($areas) use ($areaId) {
                return $areas->whereId($areaId);
            });
        }

        return $inspections;
    }
}
