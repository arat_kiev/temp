<?php

namespace App\Admin\Controllers;

use App\Models\Meta\LegalDocument;
use Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ManagerDocumentController extends AdminController
{
    private $documentRepository;

    public function __construct(LegalDocument $documentRepository)
    {
        parent::__construct();

        $this->documentRepository = $documentRepository;
    }

    public function getShow($docId)
    {
        $document = $this->documentRepository->findOrFail($docId);

        return view('admin.legal.document', compact('document'));
    }

    public function postUpdate($docId)
    {
        if (!($manager = $this->getManager())) {
            throw new AccessDeniedHttpException();
        }

        $document = $manager->legalDocuments()->unsigned()->findOrFail($docId);

        $document->pivot->signed_at = $document->freshTimestamp();
        $document->pivot->signed_by = Auth::user()->email;
        $document->pivot->save();

        return redirect()->route('admin::home');
    }
}