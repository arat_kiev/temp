<?php

namespace App\Admin\Controllers;

use App\Admin\Requests\CheckinRequest;
use App\Api1\Transformers\TicketTransformer;
use App\Events\TicketStorned;
use App\Managers\Export\TicketExportManager;
use App\Managers\TicketManager;
use App\Models\Contact\Manager;
use App\Models\Ticket\Checkin;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketInspection;
use App\Models\User;
use App\Models\Contact\Reseller;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use App\Blacklist\Blacklist;
use Carbon\Carbon;
use Datatables;
use Illuminate\Http\Request;
use Event;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class SalesController
 * @package App\Admin\Controllers
 */
class SalesController extends AdminController
{
    private $isStorno = false;

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (!$this->getManager()) {
                throw new AccessDeniedHttpException();
            }

            return $next($request);
        });
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        return view('admin.sales.index', $this->indexData($request));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getStornoIndex(Request $request)
    {
        $this->isStorno = true;

        return view('admin.sales.storno.index', $this->indexData($request));
    }

    /**
     * @param Request $request
     * @return array
     */
    private function indexData(Request $request)
    {
        $manager = $this->getManager();

        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $reseller = $request->query->getInt('reseller', -1);
        $area = $request->query->getInt('area', 0);
        $ticketType = $request->query->getInt('ticketType', 0);
        $invoiced = $request->query->getInt('invoiced', -1);
        $has_hauls = $request->query->getInt('has_hauls', -1);

        $query = $this->getManagerTickets($manager, $from, $till, $reseller, $area, $ticketType)
            ->when($invoiced === 0, function (QueryBuilder $query) {
                return $query->whereNull('hf_invoice_id');
            })
            ->when($invoiced === 1, function (QueryBuilder $query) {
                return $query->whereNotNull('hf_invoice_id');
            })
            ->when($has_hauls === 0, function (QueryBuilder $query) {
                return $query->has('hauls', '=', 0);
            })
            ->when($has_hauls === 1, function (QueryBuilder $query) {
                return $query->has('hauls');
            });

        $count = $query->count();
        $total = $query->sum('ticket_prices.value');
        $total = number_format($total / 100.0, 2, ",", ".") . ' €';

        $children = $manager->childrenAreas()->count() > 0;

        $resellerList = collect([
            '-1' => 'Alle',
            Reseller::OFFLINE_TICKETS_PARAM => 'Alle Verkaufsstellen',
            Reseller::ONLINE_TICKETS_PARAM => 'Online',
        ]);

        $areaList = collect([
            '0' => 'Alle',
        ]);

        $ticketTypes = collect([
            '0' => 'Alle',
        ]);

        $manager->resellers()->orderBy('name')->each(function ($reseller) use ($resellerList) {
            $resellerList->put($reseller->id, $reseller->name);
        });

        $manager->areas()->orderBy('name')->each(function ($area) use ($areaList, $ticketTypes) {
            $areaList->put($area->id, $area->name);
            $area->ticketTypes()->each(function ($ticketType) use ($ticketTypes, $area) {
                $ticketTypes->put($ticketType->id, $area->name . " - " .$ticketType->name);
            });
        });

        $manager->additionalAreas()->orderBy('name')->each(function ($area) use ($areaList, $ticketTypes) {
            $areaList->put($area->id, $area->name);
            $area->ticketTypes()->each(function ($ticketType) use ($ticketTypes, $area) {
                $ticketTypes->put($ticketType->id, $area->name . " - " .$ticketType->name);
            });
        });

        return compact('total', 'count', 'children', 'from', 'till', 'reseller', 'resellerList', 'area', 'areaList', 'invoiced', 'has_hauls', 'ticketType', 'ticketTypes');
    }

    /**
     * Get ticket details
     *
     * @param $ticketId
     * @return \Illuminate\Http\JsonResponse
     */
    public function ticketDetails($ticketId)
    {
        $ticket = Ticket::findOrFail($ticketId);

        $data = fractal()
            ->item($ticket, new TicketTransformer());

        return response()->json($data);
    }

    /**
     * @param $ticketId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCheckins($ticketId)
    {
        $ticket = Ticket::findOrFail($ticketId);

        return response()->json($ticket->checkins);
    }

    /**
     * @param $ticketId
     * @param $checkinId
     * @throws \Exception
     */
    public function deleteCheckin($ticketId, $checkinId)
    {
        $manager = $this->getManager();
        $ticket = Ticket::findOrFail($ticketId);
        $checkin = Checkin::findOrFail($checkinId);

        if ($checkin->ticket->id !== $ticket->id) {
            abort(403, 'checkin not in ticket');
        }

        if ($ticket->type->area->manager->id !== $manager->id) {
            abort(403, 'ticket not in area of manager');
        }

        $checkin->delete();
    }

    /**
     * @param CheckinRequest $request
     * @param $ticketId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCheckins(CheckinRequest $request, $ticketId)
    {
        //TODO: check if checkins are overlapping

        $manager = $this->getManager();
        $ticket = Ticket::findOrFail($ticketId);

        if ($ticket->type->area->manager->id !== $manager->id) {
            abort(403, 'ticket not in area of manager');
        }

        foreach ($request->request->get('checkins', []) as $checkins) {
            Checkin::updateOrCreate(
                [
                    'id' => $checkins['id']
                ],
                [
                    'ticket_id' => $ticketId,
                    'from' => $checkins['from'],
                    'till' => $checkins['till'],
                ]
            );
        }

        return response()->json($ticket->checkins);
    }

    /**
     * @param Request $request
     * @param TicketManager $ticketManager
     * @return mixed
     */
    public function anyData(Request $request, TicketManager $ticketManager, Blacklist $blacklist)
    {
        $manager = $this->getManager();

        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $reseller = $request->query->getInt('reseller', -1);
        $area = $request->query->getInt('area', 0);
        $ticketType = $request->query->getInt('ticketType', 0);
        $invoiced = $request->query->getInt('invoiced', -1);
        $has_hauls = $request->query->getInt('has_hauls', -1);

        $tickets = $this->getManagerTickets($manager, $from, $till, $reseller, $area, $ticketType)
            ->leftJoin('users', 'users.id', '=', 'tickets.user_id')
            ->when($invoiced === 0, function (QueryBuilder $query) {
                return $query->whereNull('hf_invoice_id');
            })
            ->when($invoiced === 1, function (QueryBuilder $query) {
                return $query->whereNotNull('hf_invoice_id');
            })
            ->when($has_hauls === 0, function (QueryBuilder $query) {
                return $query->has('hauls', '=', 0);
            })
            ->when($has_hauls === 1, function (QueryBuilder $query) {
                return $query->has('hauls');
            });

        return Datatables::of($tickets)
            ->filterColumn('manager_ticket_id', function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value'] && is_numeric($request->search['value'])) {
                    $instance->where('tickets.id', 'LIKE', $request->search['value'] . "%");
                } elseif (preg_match(Ticket::IDENT_REGEX, $request->search['value'], $matches)) {
                    $ticketId = (int)$matches[2];
                    $instance->where('tickets.id', 'LIKE', $ticketId . "%");
                }
            })
            ->filterColumn('user_full_name', function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    if (is_numeric($request->search['value'])) {
                        $instance->where('users.id', 'LIKE', $request->search['value'] . "%");
                    } elseif (filter_var($request->search['value'], FILTER_VALIDATE_EMAIL)) {
                        $instance->where('users.email', 'LIKE', "%" . $request->search['value'] . "%");
                    } elseif (preg_match(User::FISHER_ID_REGEX, $request->search['value'])) {
                        $instance->where('users.fisher_id', $request->search['value']);
                    } else {
                        $tokens = explode(' ', preg_replace('/\s+/', ' ', $request->search['value']));

                        if (count($tokens) === 1) {
                            $instance->where('users.first_name', 'LIKE', "%{$tokens[0]}%");
                            $instance->orWhere('users.last_name', 'LIKE', "%{$tokens[0]}%");
                            $instance->orWhere('reseller_tickets.first_name', 'LIKE', "%{$tokens[0]}%");
                            $instance->orWhere('reseller_tickets.last_name', 'LIKE', "%{$tokens[0]}%");
                        }

                        if (count($tokens) >= 2) {
                            $instance->orWhere(function ($query) use ($tokens) {
                                $query->where('users.first_name', 'LIKE', "%{$tokens[0]}%");
                                $query->where('users.last_name', 'LIKE', "%{$tokens[1]}%");
                            });

                            $instance->orWhere(function ($query) use ($tokens) {
                                $query->where('users.first_name', 'LIKE', "%{$tokens[1]}%");
                                $query->where('users.last_name', 'LIKE', "%{$tokens[0]}%");
                            });

                            $instance->orWhere(function ($query) use ($tokens) {
                                $query->where('reseller_tickets.first_name', 'LIKE', "%{$tokens[0]}%");
                                $query->where('reseller_tickets.last_name', 'LIKE', "%{$tokens[1]}%");
                            });

                            $instance->orWhere(function ($query) use ($tokens) {
                                $query->where('reseller_tickets.first_name', 'LIKE', "%{$tokens[1]}%");
                                $query->where('reseller_tickets.last_name', 'LIKE', "%{$tokens[0]}%");
                            });
                        }
                    }
                }
            })
            ->editColumn('manager_ticket_id', function ($ticket) use ($ticketManager) {
                return $ticketManager->getManagerIdentifier($ticket);
            })
            ->addColumn('ticket_number', function (Ticket $ticket) use ($ticketManager) {
                return $ticketManager->getIdentifier($ticket);
            })
            ->editColumn('valid_from', function (Ticket $ticket) use ($ticketManager) {
                $validFrom = $ticketManager->validFrom($ticket->price, $ticket->valid_from);

                return $ticket->price->ticketType->category->type === 'HOUR' ?
                    $validFrom->format('d.m.Y - H:i') :
                    $validFrom->format('d.m.Y');
            })
            ->addColumn('from_date', function (Ticket $ticket) use ($ticketManager) {
                return $ticketManager->validFrom($ticket->price, $ticket->valid_from)->format('d.m.Y');
            })
            ->addColumn('from_time', function (Ticket $ticket) use ($ticketManager) {
                return $ticketManager->validFrom($ticket->price, $ticket->valid_from)->format('H:i');
            })
            ->editColumn('valid_to', function (Ticket $ticket) use ($ticketManager) {
                $validFrom = $ticketManager->validTo($ticket->price, $ticket->valid_from);

                return $ticket->price->ticketType->category->type === 'HOUR' ?
                    $validFrom->format('d.m.Y - H:i') :
                    $validFrom->format('d.m.Y');
            })
            ->editColumn('created_at', function (Ticket $ticket) {
                return $ticket->created_at->format("d.m.Y - H:i");
            })
            ->addColumn('price_compact', function (Ticket $ticket) {
                return $ticket->type->name . ' (' . number_format($ticket->price->value / 100.0, 2, ",", ".") . ' €)';
            })
            ->addColumn('additional_manager_share', function (Ticket $ticket) {
                return $ticket->type->area->additionalManagers()->exists()
                    ? '<span class="text-nowrap">' .
                        number_format(
                            $ticket->price->value *
                            (10000 - $ticket->type->area->additionalManagers->sum('pivot.percentage'))
                            / 1000000, 2, ",", ".") .
                    ' €</span>'
                    : '-';
            })
            ->addColumn('price_brutto', function (Ticket $ticket) {
                return number_format($ticket->price->value / 100.0, 2, ",", ".") . ' €';
            })
            ->addColumn('total_price', function ($ticket) {
                return $ticket->total_price . ' €';
            })
            ->editColumn('type.area.name', function ($ticket) {
                return $ticket->type->area->name;
            })
            ->editColumn('user_full_name', function (Ticket $ticket) {
                $name = $ticket->user
                    ? $ticket->user->first_name . ' ' . $ticket->user->last_name
                    : ($ticket->resellerTicket
                        ? $ticket->resellerTicket->first_name . ' ' . $ticket->resellerTicket->last_name
                        : '');
                $email = $ticket->user
                    ? $ticket->user->email
                    : ($ticket->resellerTicket
                        ? $ticket->resellerTicket->email
                        : '');

                if ($email) {
                    $name .= '<a href="mailto:' . $email . '">
                                <span class="fa fa-envelope fa-fw"></span>
                              </a>';
                }

                return $name;
            })
            ->addColumn('hauls_count', function (Ticket $ticket) {
                $haulClass = $ticket->hauls->count() > 0
                    ? 'btn-success'
                    : ($ticket->type->is_catchlog_required
                        ? 'btn-danger'
                        : 'btn-warning');

                $inspectionClass = $ticket->inspections->count() > 0 ? 'btn-success' : 'btn-warning';
                return '
                    <a   type="button"
                         href="' . route('admin::hauls.index', ['ticket' => $ticket->id]) . '"
                         class="btn btn-xs btn-datatables ' . $haulClass . ' ">
                        <span class="fa fa-list fa-2x fa-fw"></span><br/>
                        Fangliste
                        <span class="badge">' . $ticket->hauls->count() . '</span>
                    </a>

                    <button class="btn btn-xs btn-primary btn-datatables haul-edit-btn"
                            data-target="#multi-hauls-modal"
                            data-toggle="modal"
                            data-action="' . route('admin::hauls.multi.create') . '"
                            data-ticket-id="' . $ticket->id . '"
                            data-area-id="' . $ticket->type->area->id . '"
                            data-valid-from-date="' . Carbon::parse($ticket->valid_from)->format('d.m.Y') . '"
                            data-valid-from-time="' . Carbon::parse($ticket->valid_from)->format('H:i') . '">
                        <span class="fa fa-pencil fa-2x fa-fw"></span><br/>
                        Fänge eintragen
                    </button>
                    <button type="button"
                            data-target="#empty-haul-checkin-modal"
                            data-toggle="modal"
                            data-ticket-id="' . $ticket->id . '"
                            data-valid-date="' . Carbon::parse($ticket->valid_from)->format('d.m.Y') . '"
                            data-area-id="' . $ticket->type->area->id . '"
                            class="btn btn-xs btn-primary btn-datatables">
                        <span class="fa fa-ban fa-2x fa-fw"></span><br/>
                        Nichts gefangen
                    </button>
                    <button class="btn btn-xs btn-primary btn-datatables"
                            data-target="#checkin-modal"
                            data-toggle="modal"
                            data-action="' . route('admin::tickets.checkins.update', ['ticketId' => $ticket->id] ) . '"
                            data-ticket-id="' . $ticket->id . '">
                        <span class="fa fa-clock-o fa-fw fa-2x"></span><br/>
                        Angelzeiten
                    </button>
                    <button class="btn btn-xs btn-datatables ' . $inspectionClass . '">
                        <span class="fa fa-check-square-o fa-2x fa-fw"></span><br/>
                        Kontrolliert
                        <span class="badge">' . $ticket->inspections->count() . '</span>
                    </button>';
            })
            ->addColumn('reseller', function (Ticket $ticket) {
                return ($ticket->user && $ticket->client->name !== 'pos')
                    ? 'online'
                    : ($ticket->resellerTicket
                        ? $ticket->resellerTicket->reseller->name
                        : 'System');
            })
            ->addColumn('manager', function (Ticket $ticket) {
                return '<span class="badge"
                    data-toggle="tooltip"
                    data-placement="top"
                    title="' . $ticket->type->area->manager->name . '"
                    >' . $ticket->type->area->manager->account_number . '</span>';
            })
            ->addColumn('actions', function (Ticket $ticket) use ($blacklist) {
                $removeRoute = route('admin::sales.ticket.storno', ['ticketId' => $ticket->id]);
                $addToBlacklist = ($ticket->resellerTicket
                    ? $blacklist->isUserOnBlacklist($ticket->resellerTicket->toArray())
                    : $blacklist->isUserOnBlacklist($ticket->user->toArray()))
                        ? 'data-toggle="tooltip" data-placement="left" title="Bereits auf Sperrliste" style="opacity: 0.6; cursor: default;"'
                        : 'data-target="#add-to-blacklist-modal" data-toggle="modal"';
                return '
                    <button type="button" class="btn btn-xs btn-primary btn-datatables"
                        data-target="#preview-modal" data-toggle="modal"
                        data-ticket-id="' . $ticket->id . '">
                        <span class="fa fa-eye fa-fw"></span>
                        Vorschau
                    </button>
                    <a href="'
                    . route('preview.ticket', ['ticket' => $ticket->id, 'format' => 'pdf'])
                    . '" type="button" class="btn btn-xs btn-warning btn-datatables" target="_blank">
                        <span class="fa fa-file-pdf-o fa-fw"></span>
                        Angelkarte (PDF)
                    </a>
                    <button type="button"
                            class="btn btn-xs btn-danger btn-datatables"
                            data-target="#delete-modal"
                            data-toggle="modal"
                            data-action="' . $removeRoute . '">
                        <span class="fa fa-trash-o fa-fw"></span>
                        Stornieren
                    </button>
                    <button type="button"
                            class="btn btn-xs btn-danger btn-datatables"
                            ' . $addToBlacklist . '>
                        <span class="fa fa-ban fa-fw"></span>
                        Sperren
                    </button>
                ';
            })
            ->editColumn('inspections', function (Ticket $ticket) {
                return $ticket->inspections->map(function (TicketInspection $inspection) {
                    $status = $inspection->status == 'OK'
                        ? '<span class="text-success">
                                <span class="fa fa-fw fa-check"></span> ' . $inspection->human_readable_status . '
                           </span>'
                        : '<span class="text-danger">
                                <span class="fa fa-fw fa-times"></span> ' . $inspection->human_readable_status . '
                           </span>';
                    return [
                        'inspector' => $inspection->inspector->full_name,
                        'created_at' => $inspection->created_at->format('d.m.Y H:i'),
                        'status' => $status,
                        'comment' => '<i>' . $inspection->comment . '</i>',
                    ];
                })->toArray();
            })
            ->addColumn('invoiced', function (Ticket $ticket) {
                return $ticket->hf_invoice_id ?
                    '<span class="fa fa-check fa-fw"></span>' :
                    '<span class="fa fa-times fa-fw"></span>';
            })
            ->blacklist(['user.full_name', 'type.area.name', 'reseller'])
            ->make(true);
    }

    /**
     * @param Request $request
     * @param TicketManager $ticketManager
     * @return mixed
     */
    public function anyStornoData(Request $request, TicketManager $ticketManager)
    {
        $this->isStorno = true;
        $manager = $this->getManager();

        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $reseller = $request->query->getInt('reseller', -1);
        $area = $request->query->getInt('area', 0);

        $tickets = $this->getManagerTickets($manager, $from, $till, $reseller, $area)
            ->leftJoin('users', 'users.id', '=', 'tickets.user_id');

        return Datatables::of($tickets)
            ->filterColumn('manager_ticket_id', function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value'] && is_numeric($request->search['value'])) {
                    $instance->where('tickets.id', 'LIKE', $request->search['value'] . "%");
                } elseif (preg_match(Ticket::IDENT_REGEX, $request->search['value'], $matches)) {
                    $ticketId = (int)$matches[2];
                    $instance->where('tickets.id', 'LIKE', $ticketId . "%");
                }
            })
            ->filterColumn('user_full_name', function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    if (is_numeric($request->search['value'])) {
                        $instance->where('users.id', 'LIKE', $request->search['value'] . "%");
                    } elseif (filter_var($request->search['value'], FILTER_VALIDATE_EMAIL)) {
                        $instance->where('users.email', 'LIKE', "%" . $request->search['value'] . "%");
                    } elseif (preg_match(User::FISHER_ID_REGEX, $request->search['value'])) {
                        $instance->where('users.fisher_id', $request->search['value']);
                    } else {
                        $tokens = explode(' ', preg_replace('/\s+/', ' ', $request->search['value']));

                        if (count($tokens) === 1) {
                            $instance->where('users.first_name', 'LIKE', "%{$tokens[0]}%");
                            $instance->orWhere('users.last_name', 'LIKE', "%{$tokens[0]}%");
                            $instance->orWhere('reseller_tickets.first_name', 'LIKE', "%{$tokens[0]}%");
                            $instance->orWhere('reseller_tickets.last_name', 'LIKE', "%{$tokens[0]}%");
                        }

                        if (count($tokens) >= 2) {
                            $instance->orWhere(function ($query) use ($tokens) {
                                $query->where('users.first_name', 'LIKE', "%{$tokens[0]}%");
                                $query->where('users.last_name', 'LIKE', "%{$tokens[1]}%");
                            });

                            $instance->orWhere(function ($query) use ($tokens) {
                                $query->where('users.first_name', 'LIKE', "%{$tokens[1]}%");
                                $query->where('users.last_name', 'LIKE', "%{$tokens[0]}%");
                            });

                            $instance->orWhere(function ($query) use ($tokens) {
                                $query->where('reseller_tickets.first_name', 'LIKE', "%{$tokens[0]}%");
                                $query->where('reseller_tickets.last_name', 'LIKE', "%{$tokens[1]}%");
                            });

                            $instance->orWhere(function ($query) use ($tokens) {
                                $query->where('reseller_tickets.first_name', 'LIKE', "%{$tokens[1]}%");
                                $query->where('reseller_tickets.last_name', 'LIKE', "%{$tokens[0]}%");
                            });
                        }
                    }
                }
            })
            ->editColumn('manager_ticket_id', function (Ticket $ticket) use ($ticketManager) {
                return $ticketManager->getManagerIdentifier($ticket);
            })
            ->addColumn('ticket_number', function (Ticket $ticket) use ($ticketManager) {
                return $ticketManager->getIdentifier($ticket);
            })
            ->editColumn('valid_from', function (Ticket $ticket) use ($ticketManager) {
                $validFrom = $ticketManager->validFrom($ticket->price, $ticket->valid_from);

                return $ticket->price->ticketType->category->type === 'HOUR'
                    ? $validFrom->format('d.m.Y - H:i')
                    : $validFrom->format('d.m.Y');
            })
            ->editColumn('valid_to', function (Ticket $ticket) use ($ticketManager) {
                $validFrom = $ticketManager->validTo($ticket->price, $ticket->valid_from);

                return $ticket->price->ticketType->category->type === 'HOUR'
                    ? $validFrom->format('d.m.Y - H:i')
                    : $validFrom->format('d.m.Y');
            })
            ->editColumn('storno_date', function (Ticket $ticket) {
                return $ticket->created_at->format("d.m.Y - H:i");
            })
            ->orderColumn('storno_date', 'created_at $1')
            ->addColumn('price_compact', function (Ticket $ticket) {
                return $ticket->price->name . ' (' . number_format($ticket->price->value / 100.0, 2, ",", ".") . ' €)';
            })
            ->addColumn('price_brutto', function (Ticket $ticket) {
                return number_format($ticket->price->value / 100.0, 2, ",", ".") . ' €';
            })
            ->editColumn('type.area.name', function (Ticket $ticket) {
                return $ticket->type->area->name;
            })
            ->addColumn('user_full_name', function (Ticket $ticket) {
                $name = $ticket->user
                    ? $ticket->user->first_name . ' ' . $ticket->user->last_name
                    : ($ticket->resellerTicket
                        ? $ticket->resellerTicket->first_name . ' ' . $ticket->resellerTicket->last_name
                        : '');
                $email = $ticket->user
                    ? $ticket->user->email
                    : ($ticket->resellerTicket
                        ? $ticket->resellerTicket->email
                        : '');

                if ($email) {
                    $name .= '<a href="mailto:' . $email . '">
                                <span class="fa fa-envelope fa-fw"></span>
                              </a>';
                }

                return $name;
            })
            ->addColumn('reseller', function (Ticket $ticket) {
                return ($ticket->user && $ticket->client->name !== 'pos')
                    ? 'online'
                    : ($ticket->resellerTicket
                        ? $ticket->resellerTicket->reseller->name
                        : 'System');
            })
            ->addColumn('manager', function (Ticket $ticket) {
                return '<span class="badge"
                    data-toggle="tooltip"
                    data-placement="top"
                    title="' . $ticket->type->area->manager->name . '"
                    >' . $ticket->type->area->manager->account_number . '</span>';
            })
            ->addColumn('actions', function (Ticket $ticket) {
                return '
                    <button type="button"
                            class="btn btn-xs btn-primary btn-datatables"
                            data-target="#preview-modal"
                            data-toggle="modal"
                            data-ticket-id="' . $ticket->id . '">
                        <span class="fa fa-eye fa-fw"></span>
                        Vorschau
                    </button>
                    <a href="' . route('preview.ticket', ['ticket' => $ticket->id, 'format' => 'pdf']) . '"
                       type="button"
                       class="btn btn-xs btn-warning btn-datatables"
                       target="_blank">
                        <span class="fa fa-file-pdf-o fa-fw"></span>
                        Angelkarte (PDF)
                    </a>
                ';
            })
            ->addColumn('original_actions', function (Ticket $ticket) {
                return '
                    <button type="button"
                            class="btn btn-xs btn-primary btn-datatables"
                            data-target="#preview-modal"
                            data-toggle="modal"
                            data-ticket-id="' . $ticket->storno_id . '">
                        <span class="fa fa-eye fa-fw"></span>
                        Vorschau
                    </button>
                    <a href="' . route('preview.ticket', ['ticket' => $ticket->storno_id, 'format' => 'pdf']) . '"
                       type="button"
                       class="btn btn-xs btn-warning btn-datatables"
                       target="_blank">
                        <span class="fa fa-file-pdf-o fa-fw"></span>
                        Angelkarte (PDF)
                    </a>
                ';
            })
            ->blacklist(['type.area.name', 'reseller'])
            ->make(true);
    }

    /**
     * @param Manager $manager
     * @param null $from
     * @param null $till
     * @param int $reseller
     * @param int $area
     * @param int $ticketType
     * @return Ticket|QueryBuilder|\Illuminate\Database\Query\Builder
     */
    private function getManagerTickets(Manager $manager, $from = null, $till = null, $reseller = -1, $area = 0, $ticketType = 0)
    {
        $with = ['type.area.manager', 'price', 'user.country', 'resellerTicket.country', 'resellerTicket.reseller', 'hauls', 'inspections'];

        $query = Ticket::with($with)
            ->withCount(['hauls', 'inspections'])
            ->select('tickets.*')
            ->join('ticket_prices', 'tickets.ticket_price_id', '=', 'ticket_prices.id')
            ->join('ticket_types', 'tickets.ticket_type_id', '=', 'ticket_types.id')
            ->join('areas', 'ticket_types.area_id', '=', 'areas.id')
            ->join('managers', 'areas.manager_id', '=', 'managers.id')
            ->leftJoin('reseller_tickets', 'tickets.id', '=', 'reseller_tickets.ticket_id')
            ->leftJoin('area_additional_managers AS am', function ($join) use ($manager) {
                $join->on('areas.id', '=', 'am.area_id')
                    ->where('am.manager_id', '=', $manager->id);
            })
            ->where(function ($query) use ($manager) {
                $query->where('managers.id', '=', $manager->id)
                    ->orWhere('managers.parent_id', '=', $manager->id)
                    ->orWhereNotNull('am.manager_id');
            });

        if ($this->isStorno) {
            // show storned ticket
            $query->whereNotNull('storno_id');
            $query->withTrashed();
        } else {
            // show active ticket
            $query->whereNull('storno_id');
            $query->where('is_deleted', 0);
        }

        $from = !is_null($from)
            ? Carbon::createFromFormat('Y-m-d', $from)->startOfDay()
            : Carbon::today()->startOfDay();
        $till = !is_null($till)
            ? Carbon::createFromFormat('Y-m-d', $till)->endOfDay()
            : Carbon::today()->endOfDay();

        if ($reseller >= 0) {
            switch ($reseller) {
                case Reseller::ONLINE_TICKETS_PARAM:
                    $query->whereNotNull('tickets.user_id');
                    break;
                case Reseller::OFFLINE_TICKETS_PARAM:
                    $query->whereNull('tickets.user_id');
                    break;
                default:
                    $query->where('reseller_tickets.reseller_id', '=', $reseller);
            }
        }

        if ($area > 0) {
            $query->where('areas.id', $area);
        }

        if ($ticketType > 0) {
            $query->where('ticket_types.id', $ticketType);
        }

        $query->whereBetween('tickets.created_at', [$from, $till]);

        return $query;
    }

    /**
     * @param Request $request
     * @param string $format
     * @param TicketExportManager $exportManager
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Exception
     */
    public function getExport(Request $request, $format = 'xls', TicketExportManager $exportManager)
    {
        if ($format != 'xls') {
            throw new \Exception('Invalid format parameter');
        }

        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $reseller = $request->query->getInt('reseller', -1);
        $area = $request->query->getInt('area', 0);
        $ticketType = $request->query->getInt('ticketType', 0);
        $invoiced = $request->query->getInt('invoiced', -1);
        $has_hauls = $request->query->getInt('has_hauls', -1);

        $manager = $this->getManager();

        $tickets = $this->getManagerTickets($manager, $from, $till, $reseller, $area, $ticketType)
            ->when($invoiced === 0, function (QueryBuilder $query) {
                return $query->whereNull('hf_invoice_id');
            })
            ->when($invoiced === 1, function (QueryBuilder $query) {
                return $query->whereNotNull('hf_invoice_id');
            })
            ->when($has_hauls === 0, function (QueryBuilder $query) {
                return $query->has('hauls', '=', 0);
            })
            ->when($has_hauls === 1, function (QueryBuilder $query) {
                return $query->has('hauls');
            })
            ->orderBy('tickets.created_at');

        $file = $exportManager->run($tickets, $format, compact('from', 'till', 'reseller', 'area', 'tickeType'));

        return response()->download($file);
    }

    /**
     * @param $ticketId
     * @param Request $request
     * @param TicketManager $ticketManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function softDelete($ticketId, Request $request, TicketManager $ticketManager)
    {
        if (!$ticket = Ticket::find($ticketId)) {
            return response()->json([
                'notification' => 'danger',
                'message' => 'Ticket wurde nicht gefunden'
            ]);
        }

        try {
            $ticketManager->createStornoTicket($ticket, $request->get('reason', ''));
            Event::fire(new TicketStorned($ticket));

            return response()->json([
                'notification' => 'success',
                'message' => 'Ticket wurde gelöscht'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'notification' => 'danger',
                'message' => 'Ticket konnte nicht gelöscht werden'
            ]);
        }
    }
}
