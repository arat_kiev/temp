<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\ProductPriceForm;
use App\Admin\Requests\ProductPriceRequest;
use App\Admin\Requests\UpdateTimeslotDatesRequest;
use App\Models\Commission;
use App\Models\Contact\Manager;
use App\Models\Product\Product;
use App\Models\Product\ProductPrice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Datatables;
use Html;
use DB;
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductPriceController extends AdminController
{
    use FormBuilderTrait;

    public function getIndex(Request $request, $productId)
    {
        /** @var Product $product */
        $product = Product::findOrFail($productId);
        $count = $product->prices()->count();

        return view('admin.productprice.index', compact('product', 'count'));
    }

    public function getCreate(Request $request, $productId)
    {
        $product = Product::findOrFail($productId);
        $productPrice = new ProductPrice();

        $form = $form = $this->form(ProductPriceForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::products.prices.store', [
                'productId' => $productId,
            ]),
            'model'     => $productPrice,
        ]);

        return view('admin.productprice.edit', compact('product', 'productPrice', 'form'));
    }

    public function getEdit($productId, $priceId)
    {
        $manager = $this->getManager();

        $product = Product::findOrFail($productId);
        $productPrice = ProductPrice::findOrFail($priceId);

        $isLegalManager = (bool) $productPrice->whereHas('product.managers', function ($query) use ($manager) {
            $query->where('id', $manager->id);
        })->count();

        if (!$isLegalManager) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->form(ProductPriceForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::products.prices.update', [
                'productId' => $productId,
                'priceId' => $productPrice,
            ]),
            'model'     => $productPrice,
        ]);

        return view('admin.productprice.edit', compact('product', 'productPrice', 'form'));
    }

    public function postCreate(ProductPriceRequest $request, $productId)
    {
        $product = Product::findOrFail($productId);
        $oldPrice = $product->prices()->orderBy('valid_till', 'desc')->first();
        $price = new ProductPrice();
        $price->product()->associate($productId);

        $form = $this->form(ProductPriceForm::class, ['model' => $price]);

        if ($errorMsg = $this->checkFormValidity($form, $price, $request)) {
            flash()->error($errorMsg);
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::transaction(function () use ($request, $price, $oldPrice) {
            $this->update($request, $price);
            $this->addDefaultCommissions($price);

            if ($oldPrice) {
                $oldPrice->valid_till = $price->visible_from->copy()->subDay()->endOfDay();
                $oldPrice->save();
            }
        });

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::products.prices.edit', ['productId' => $productId, 'priceId' => $price]);
    }

    private function addDefaultCommissions(ProductPrice $productPrice)
    {
        $this->addDefaultResellerCommission($productPrice);
        $this->addDefaultOnlineCommission($productPrice);
    }

    private function addDefaultOnlineCommission(ProductPrice $productPrice)
    {
        $commissionPrice = ProductPrice::with('commissions')
            ->whereHas('commissions', function ($query) {
                $query->where('is_online', true)
                    ->whereNull('valid_till');
            })
            ->whereHas('product.managers', function ($query) {
                $query->whereId($this->getManager()->id);
            })
            ->orderBy('created_at', 'desc')
            ->first();

        $commission = $commissionPrice
            ? $commissionPrice->commissions()
                ->where('is_online', true)
                ->whereNull('valid_till')
                ->orderBy('created_at', 'desc')
                ->first()
            : (object) Commission::DEFAULT_COMMISSIONS['online'];

        $productPrice->commissions()->create([
            'commission_type'   => $commission->commission_type,
            'commission_value'  => $commission->commission_value,
            'min_value'         => $commission->min_value,
            'is_online'         => true,
            'valid_from'        => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

    private function addDefaultResellerCommission(ProductPrice $productPrice)
    {
        $commissionPrice = ProductPrice::with('commissions')
            ->whereHas('commissions', function ($query) {
                $query->where('is_online', false)
                    ->whereNull('valid_till');
            })
            ->whereHas('product.managers', function ($query) {
                $query->whereId($this->getManager()->id);
            })
            ->orderBy('created_at', 'desc')
            ->first();

        $commission = $commissionPrice
            ? $commissionPrice->commissions()
                ->where('is_online', false)
                ->whereNull('valid_till')
                ->orderBy('created_at', 'desc')
                ->first()
            : (object) Commission::DEFAULT_COMMISSIONS['reseller'];

        $productPrice->commissions()->create([
            'commission_type'   => $commission->commission_type,
            'commission_value'  => $commission->commission_value,
            'min_value'         => $commission->min_value,
            'is_online'         => false,
            'valid_from'        => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

    public function postUpdate(ProductPriceRequest $request, $productId, $priceId)
    {
        $product = Product::findOrFail($productId);
        $price = ProductPrice::findOrFail($priceId);

        $form = $this->form(ProductPriceForm::class, ['model' => $price]);

        if ($errorMsg = $this->checkFormValidity($form, $price, $request)) {
            flash()->error($errorMsg);
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $price, $product);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::products.prices.edit', [
            'productId' => $productId,
            'priceId' => $priceId,
        ]);
    }

    public function getTimeslotDates(Request $request, $productId, $priceId)
    {
        $month = $request->get('month');
        $year = $request->get('year');

        if (!$month || !$year) {
            throw new NotFoundHttpException();
        }

        $from = Carbon::createFromDate($year, $month, 1)->setTime(0, 0);
        $till = $from->copy()->endOfMonth();

        /** @var Product $product */
        $product = Product::findOrFail($productId);
        /** @var ProductPrice $productPrice */
        $productPrice = ProductPrice::findOrFail($priceId);

        if ($productPrice->product->id !== $product->id) {
            throw new NotFoundHttpException();
        }

        $dates = $productPrice->timeslotDates()
            ->whereBetween('date', [$from, $till])->get()
            ->groupBy(function ($slot) {
                return $slot->date->format('Y-m-d');
            })->transform(function ($date) {
                return $date->pluck('timeslot_id');
            });

        return response()->json($dates);
    }

    public function postTimeslotDates(UpdateTimeslotDatesRequest $request, $productId, $priceId)
    {
        /** @var Product $product */
        $product = Product::findOrFail($productId);
        /** @var ProductPrice $productPrice */
        $productPrice = ProductPrice::findOrFail($priceId);

        if ($productPrice->product->id !== $product->id) {
            throw new NotFoundHttpException();
        }

        if (!$product->managers()->find($this->getManager()->id) &&
            !$request->user()->hasRole('superadmin')) {
            throw new AccessDeniedHttpException();
        }

        foreach ($request->dates as $date => $slots) {
            foreach ($slots as $slot => $enabled) {
                if (!$timeslot = $productPrice->timeslots()->find($slot)) {
                    continue;
                }

                if (!$enabled) {
                    $timeslot->dates()->where('date', '=', $date)->delete();
                    continue;
                }

                /** @var $timeslot \App\Models\Product\Timeslot */
                $timeslot->dates()->updateOrCreate([
                    'date' => $date,
                    'pool' => $timeslot->default_pool,
                ]);
            }
        }

        return response()->json();
    }

    private function checkFormValidity(Form $form, ProductPrice $price, ProductPriceRequest $request)
    {
            if (!$form->isValid()) {
                return 'Es ist ein Fehler aufgetreten';
            }

            if ($price->sales()->count()) {
                return 'Es gibt schon verkaufte Produkte mit diesem Preis';
            }

            return null;
    }
    
    private function update(ProductPriceRequest $request, ProductPrice $productPrice)
    {
        $productPrice->value = $request->request->get('value');
        $productPrice->vat = $request->request->get('vat');
        $productPrice->visible_from = $request->request->get('visible_from', null)
            ? Carbon::createFromFormat('d.m.Y', $request->request->get('visible_from'))->startOfDay()
            : null;
        $productPrice->valid_from = $request->request->get('valid_from', null)
            ? Carbon::createFromFormat('d.m.Y', $request->request->get('valid_from'))->startOfDay()
            : null;
        $productPrice->valid_till = $request->request->get('valid_till', null)
            ? Carbon::createFromFormat('d.m.Y', $request->request->get('valid_till'))->endOfDay()
            : null;

        $this->updateTimeslots(collect($request->request->get('timeslots', [])), $productPrice);
        $this->updateTimeDiscounts(collect($request->request->get('timeDiscounts', [])), $productPrice);

        $productPrice->save();
    }

    private function updateTimeslots($timeslots, ProductPrice $productPrice)
    {
        $productPrice->timeslots()->whereNotIn('id', $timeslots->pluck('id'))->delete();

        $timeslots->each(function ($timeslot) use ($productPrice) {
            $productPrice->timeslots()->updateOrCreate(['id' => $timeslot['id']], $timeslot);
        });
    }

    private function updateTimeDiscounts($timeDiscounts, ProductPrice $productPrice)
    {
        $productPrice->timeDiscounts()->whereNotIn('id', $timeDiscounts->pluck('id'))->delete();

        $timeDiscounts->each(function ($timeDiscount) use ($productPrice) {
            $timeDiscount['discount'] = ((float) str_replace(',', '.', $timeDiscount['discount'])) * 100;
            $productPrice->timeDiscounts()->updateOrCreate(['id' => $timeDiscount['id']], $timeDiscount);
        });
    }

    public function delete($priceId)
    {
        $price = ProductPrice::findOrFail($priceId);

        if ($price->sales()->count()) {
            throw new AccessDeniedHttpException('Preis kann nicht gelöscht werden');
        }

        $price->delete();
        flash()->success('Preis wurde gelöscht');
    }

    public function anyData(Request $request, $productId)
    {
        $prices = $this->getManagerProductPrices($this->getManager(), $request->query->getInt('product', -1))
            ->select(['id', 'value', 'valid_from', 'valid_till', 'product_id'])
            ->with(['product']);

        return Datatables::of($prices)
            ->editColumn('product_name', function ($price) {
                return Html::linkRoute(
                    'admin::products.edit',
                    str_limit($price->product->name, 50),
                    ['productId' => $price->product]
                );
            })
            ->editColumn('valid_from', function ($price) {
                return $price->valid_from ? $price->valid_from->format('d.m.Y') : '';
            })
            ->editColumn('valid_till', function ($price) {
                return $price->valid_till ? $price->valid_till->format('d.m.Y') : '';
            })
            ->editColumn('value', function ($price) {
                return $price->value . ' €';
            })
            ->addColumn('fees', function (ProductPrice $price) {
                $fees = $price->fees()
                    ->listsTranslations('name')
                    ->addSelect(['value', 'type', 'country_id'])
                    ->get()
                    ->pluck('value_with_mark', 'name_with_country')
                    ->toArray();

                return implode("<br>", array_map(function ($fee, $value) {
                    return $fee . ' - ' . $value;
                }, array_keys($fees), $fees));
            })
            ->addColumn('actions', function ($price) use ($productId) {
                $editRoute = route('admin::products.prices.edit', [
                    'productId' => $productId,
                    'priceId' => $price,
                ]);
                $removeRoute = route('admin::products.prices.delete', [
                    'productId' => $productId,
                    'priceId' => $price
                ]);
                $saleExists = (bool) $price->sales()->count();

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        <i class="fa fa-edit fa-fw"></i>
                    </a>
                    <button type="button"
                            data-action="' . $removeRoute . '"
                            data-method="DELETE"
                            class="btn btn-xs btn-danger'.($saleExists ? ' disabled ' : '').'"
                            data-target="#delete-modal"
                            data-toggle="modal"
                            '.($saleExists ? ' disabled ' : '').'>
                        Löschen<span class="fa fa-trash-o fa-fw"></span>
                    </button>
                ';
            })
            ->removeColumn('product')
            ->blacklist(['actions'])
            ->make(true);
    }

    /**
     * @param Manager $manager
     * @param int     $productId
     * @return mixed
     */
    private function getManagerProductPrices(Manager $manager, int $productId)
    {
        return ProductPrice::whereHas('product.managers', function ($query) use ($manager) {
            $query->where('id', $manager->id);
        })->where('product_id', $productId);
    }
}
