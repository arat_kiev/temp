<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\AreaForm;
use App\Admin\Requests\StoreAreaRequest;
use App\Api1\Controllers\AreaController as ApiAreaController;
use App\Events\AreaChanged;
use App\Events\AreaCreated;
use App\Models\Area\Area;
use App\Models\Area\Rule;
use App\Models\Meta\Fish;
use App\Models\Picture;
use App\Models\Ticket\TicketType;
use Auth;
use Carbon\Carbon;
use Datatables;
use Event;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Slugify;
use Html;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AreaController extends AdminController
{
    use FormBuilderTrait;

    public function getIndex()
    {
        return view('admin.area.index', ['children' => false]);
    }

    public function getAffiliated()
    {
        return view('admin.area.index', ['children' => true]);
    }

    public function getCreate()
    {
        $area = new Area();

        $fishes = $this->getFishList();

        $form = $form = $this->form(AreaForm::class, [
            'method' => 'POST',
            'url' => route('admin::areas.store', ['areaId' => $area->id]),
            'model' => $area,
        ]);

        return view('admin.area.edit', compact('area', 'form', 'fishes'));
    }

    public function postStore(StoreAreaRequest $request)
    {
        $area = new Area();

        $form = $this->form(AreaForm::class, ['model' => $area]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $area);

        // send admin email about new area
        Event::fire(new AreaCreated($area, Auth::user()));

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::areas.edit', ['areaId' => $area->id]);
    }

    public function getEdit($areaId)
    {
        $manager = $this->getManager();

        $area = Area::findOrFail($areaId);

        $fishes = $this->getFishList();

        if ($manager->id != $area->manager_id && $manager->id != $area->manager->parent_id) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->form(AreaForm::class, [
            'method' => 'POST',
            'url' => route('admin::areas.update', ['areaId' => $area->id]),
            'model' => $area,
        ]);

        return view('admin.area.edit', compact('area', 'form', 'fishes'));
    }

    public function postUpdate(StoreAreaRequest $request, $areaId)
    {
        $area = Area::findOrFail($areaId);

        $form = $this->form(AreaForm::class, ['model' => $area]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $area);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::areas.edit', ['areaId' => $areaId]);
    }

    private function update(StoreAreaRequest $request, Area $area)
    {
        // Manager
        if (!$area->manager) {
            $area->manager()->associate($this->getManager());
        }

        // Standard values
        $area->public = $request->request->getBoolean('public');
        $area->lease = $request->request->getBoolean('lease');
        $area->name = $request->request->get('name');
        $area->description = $request->request->get('description');
        $area->season_begin = $request->request->get('season_begin');
        $area->season_end = $request->request->get('season_end');
        $area->borders = $request->request->get('borders');
        $area->ticket_info = $request->request->get('ticket_info');
        $area->member_only = $request->request->getBoolean('member_only');
        $area->boat = $request->request->getBoolean('boat');
        $area->phone_ticket = $request->request->getBoolean('phone_ticket');
        $area->nightfishing = $request->request->getBoolean('nightfishing');
        $area->rods_max = $request->request->get('rods_max');
        $area->polyfield = $request->request->get('polyfield');
        $area->save();

        // Custom tags
        $customTags = $request->request->get('custom_tags', []);

        // Relations
        $area->type()->associate($request->request->getInt('type'));
        $area->techniques()->sync($request->request->get('techniques', []));

        //Ticket types
        $area->additionalTicketTypes()->detach();
        foreach ($request->request->get('ticket_types', []) as $ticketTypeId) {
            $ticketType = TicketType::find($ticketTypeId);
            $area->additionalTicketTypes()->save($ticketType);
        }

        // Fishes and restrictions
        $area->fishes()->detach();
        foreach ($request->request->get('fishes', []) as $fish) {
            $area->fishes()->attach($fish['id'], [
                'min_length' => $fish['min_length'] ?: null,
                'max_length' => $fish['max_length'] ?: null,
                'closed_from' => $fish['closed_from'] ? Carbon::createFromFormat('d.m.Y', $fish['closed_from']) : null,
                'closed_till' => $fish['closed_till'] ? Carbon::createFromFormat('d.m.Y', $fish['closed_till']) : null,
            ]);
        }

        // Geolocations
        $area->locations()->delete();
        foreach ($request->request->get('locations', []) as $locString) {
            $location = $area->locations()->create([]);
            $locString = str_replace(' ', '', $locString);
            list($location->latitude, $location->longitude) = explode(',', $locString);
            $location->save();
        }

        // Rules
        $rule = $area->rule ?: Rule::create([]);

        $rule->text = $request->request->get('rule_text');

        foreach ($request->request->get('rule_files_delete', []) as $fileId) {
            $rule->files()->detach($fileId);
        }

        // non-public rule files
        $this->processRuleFiles(
            $area,
            $rule,
            $request->file('rule_files', []),
            $request->get('rule_files_name', []),
            false
        );

        // public rule files
        $this->processRuleFiles(
            $area,
            $rule,
            $request->file('rule_files_public', []),
            $request->get('rule_public_files_name', []),
            true
        );

        if ($rule->isEmpty()) {
            $rule->delete();
            $area->rule()->dissociate();
        } else {
            $rule->save();
            $area->rule()->associate($rule);
        }

        $area->save();

        $primCity = $cityId = $request->request->getInt('cities');
        Event::fire(new AreaChanged($area, $primCity, $customTags));

        /*if ($request->request->has('cities')) {
            ;
            $area->cities()->updateExistingPivot($cityId, ['primary' => true]);
        }*/
    }

    /**
     * Process rule files
     *
     * @param $area Area
     * @param $rule Rule
     * @param $files UploadedFile[]|null
     * @param $fileNames string[]|null
     * @param $public boolean
     */
    private function processRuleFiles($area, $rule, $files, $fileNames, $public)
    {
        foreach ($files as $file) {
            if ($file != null) {
                $currentNewName = $fileNames[$file->getClientOriginalName()];

                $name = !empty($currentNewName) ? $currentNewName : $area->name.'-bestimmungen';
                $slug = Slugify::slugify($name, '-');

                $rule->files()->save(new Picture(['file' => $file, 'name' => $slug]), ['public' => $public]);
            }
        }
    }

    public function anyData(Request $request)
    {
        $manager = $this->getManager();

        $areas = $request->query->has('affiliate') ? $manager->childrenAreas() : $manager->areas();

        $areas->with('resellers', 'manager', 'gallery', 'gallery.pictures', 'ticketTypes')
            ->select(['areas.id as id', 'areas.name as name', 'public', 'manager_id', 'gallery_id']);

        return Datatables::of($areas)
            ->editColumn('name', function ($area) {
                return Html::linkRoute("admin::areas.edit", str_limit($area->name, 50), ["areaId" => $area->id]);
            })
            ->editColumn('public', function ($area) {
                return $area->public
                    ? '<span class="fa fa-fw fa-check"></span>'
                    : '<span class="fa fa-fw fa-times"></span>';
            })
            ->editColumn('manager', function ($area) {
                return $area->manager
                    ? '<span class="badge" data-toggle="tooltip" data-placement="top"
                             title="' . $area->manager->name . '">' .
                            $area->manager->account_number .
                      '</span>'
                    : '';
            })
            ->addColumn('resellers', function ($area) {
                return '
                    <a href="#" class="btn btn-xs btn-primary">
                        <span class="fa fa-shopping-cart fa-fw"></span>
                        Verkaufsstellen <span class="badge">
                            ' . $area->resellerCount() . '
                        </span>
                    </a>';
            })
            ->addColumn('ticket_types', function ($area) {
                $route = route('admin::areas.tickettypes.index', ['areaId' => $area->id]);

                return '
                    <a href="' . $route . '" class="btn btn-xs btn-primary">
                        <span class="fa fa-ticket fa-fw"></span>
                        Angelkarten <span class="badge">
                            ' . $area->ticketTypes->count() . '
                        </span>
                    </a>';
            })
            ->addColumn('gallery', function ($area) {
                $route = route('admin::areas.gallery.list', ['areaId' => $area->id]);

                return '
                    <a href="' . $route . '" class="btn btn-xs btn-primary">
                        <span class="fa fa-picture-o fa-fw"></span>
                        Bilder <span class="badge">
                            ' . ($area->gallery ? $area->gallery->pictures()->count() : 0) . '
                        </span>
                    </a>';
            })
            ->make(true);
    }

    public function allManagerAreas(Request $request)
    {
        $manager = $this->getManager();
        $request->query->set('manager', $manager->id);

        return with(new ApiAreaController(new Area()))->index($request);
    }

    private function getFishList()
    {
        return Fish::listsTranslations('name')->orderBy('name')->pluck('name', 'id');
    }
}
