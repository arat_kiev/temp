<?php

namespace App\Admin\Controllers\Inspect;

use App\Models\Area\Area;
use Datatables;
use Illuminate\Http\Request;

class AreaController extends BaseInspectController
{
    /**
     * @var Area
     */
    private $areaRepository;

    /**
     * TicketController constructor.
     *
     * @param Area $areaRepository
     */
    public function __construct(Area $areaRepository)
    {
        parent::__construct();

        $this->middleware(function () {
            $this->areaRepository = $this->getInspector()->inspectedAreas()->select(['areas.*']);
        });
    }

    public function getIndex(Request $request)
    {
        $ticketCategory = $request->query->getInt('ticketCategory', 0);
        $areaRepository = clone($this->areaRepository);

        $categoryList = collect(['0' => 'Alle']);

        $areaRepository->each(function ($area) use ($categoryList) {
            $area->ticketTypes->each(function ($ticketType) use ($categoryList) {
                $categoryList[$ticketType->category->id] = $ticketType->category->name;
            });
        });

        return view(
            'admin.inspect.area.index',
            compact('ticketCategory', 'categoryList')
        );
    }

    public function anyData(Request $request)
    {
        $ticketCategory = $request->query->getInt('ticketCategory', 0);
        $areas = $this->areaRepository;

        if ($ticketCategory) {
            $areas = $areas->hasTicketCategories([$ticketCategory]);
        }

        return Datatables::of($areas)
            ->editColumn('public', function ($area) {
                if ($area->public) {
                    return '<span class="fa fa-fw fa-check"></span>';
                } else {
                    return '<span class="fa fa-fw fa-times" ></span >';
                }
            })
            ->editColumn('manager', function ($area) {
                return $area->manager
                    ? '<span class="badge" data-toggle="tooltip" data-placement="top">' .
                            $area->manager->name .
                       '</span>'
                    : '';
            })
            ->addColumn('resellers', function ($area) {
                return '<button class="btn btn-xs btn-primary">
                            <span class="fa fa-shopping-cart fa-fw"></span>
                            Verkaufsstellen <span class="badge">' . $area->resellers->count() . '</span>
                        </button>';
            })
            ->addColumn('ticket_types', function ($area) {

                return '<button class="btn btn-xs btn-primary">
                            <span class="fa fa-ticket fa-fw"></span>
                            Angelkarten <span class="badge">' . $area->ticketTypes->count() . '</span>
                        </button>';
            })
            ->make(true);
    }
}
