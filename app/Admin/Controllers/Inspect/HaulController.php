<?php

namespace App\Admin\Controllers\Inspect;

use App\Managers\Export\HaulExportManager;
use App\Managers\TicketManager;
use App\Models\Area\Area;
use App\Models\Haul;
use App\Models\Ticket\Ticket;
use Datatables;
use Illuminate\Http\Request;

class HaulController extends BaseInspectController
{
    public function getIndex(Request $request)
    {
        $ticket = $request->query->getInt('ticket') ?: null;

        return view('admin.inspect.haul.index', compact('ticket'));
    }

    public function anyData(Request $request)
    {
        $ticket = $request->query->get('ticket');
        $hauls = $this->getInspectorHauls($ticket);

        return Datatables::of($hauls)
            ->addColumn('fish_name', function ($haul) {
                return $haul->fish ? $haul->fish->name : 'Leermeldung';
            })
            ->addColumn('fish_size', function ($haul) {
                return $haul->fish
                    ? $haul->size_value . ' ' . $haul->size_type
                    : '';
            })
            ->addColumn('user_name', function ($haul) {
                return $haul->user->full_name;
            })
            ->editColumn('ticket_number', function (Haul $haul) {
                return $haul->ticket_number ?:
                    ($haul->ticket ? (new TicketManager())->getIdentifier($haul->ticket) : null);
            })
            ->addColumn('area_name', function ($haul) {
                return $haul->area ? $haul->area->name : $haul->area_name;
            })
            ->editColumn('catch_date', function ($haul) {
                return $haul->catch_time
                    ? $haul->catch_date->format('d.m.Y') . ' ' . $haul->catch_time
                    : $haul->catch_date->format('d.m.Y');
            })
            ->make(true);
    }

    public function getExport($format = 'xls', Request $request, HaulExportManager $exportManager)
    {
        $hauls = $this->getInspectorHauls($request->query->getInt('ticket', null));
        $file = $exportManager->run($hauls, $format, []);

        return response()->download($file);
    }

    private function getInspectorHauls($ticket = null)
    {
        $hauls = Haul::with(['user', 'area'])
            ->select('hauls.*')
            ->join('tickets', 'tickets.id', '=', 'hauls.ticket_id')
            ->join('ticket_types', 'tickets.ticket_type_id', '=', 'ticket_types.id')
            ->join('areas', 'ticket_types.area_id', '=', 'areas.id')
            ->join('area_inspectors', 'area_inspectors.area_id', '=', 'areas.id')
            ->join('users', 'users.id', '=', 'area_inspectors.user_id')
            ->where('users.id', '=', $this->getInspector()->id);

        if ($ticket && $ticket > 0) {
            $hauls->whereHas('ticket', function ($q) use ($ticket) {
                $q->whereId($ticket);
            });
        }

        return $hauls;
    }
}
