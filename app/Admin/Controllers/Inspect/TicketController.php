<?php

namespace App\Admin\Controllers\Inspect;

use App\Admin\Forms\TicketInspectionForm;
use App\Admin\Requests\TicketInspectionRequest;
use App\Managers\TicketManager;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketInspection;
use Datatables;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Html;

class TicketController extends BaseInspectController
{
    use FormBuilderTrait;

    /**
     * @var Ticket
     */
    private $ticketRepository;

    /**
     * TicketController constructor.
     *
     * @param Ticket $ticketRepository
     */
    public function __construct(Ticket $ticketRepository)
    {
        parent::__construct();

        $this->middleware(function ($request, $next) use ($ticketRepository) {
            $inspector = $this->getInspector();

            $this->ticketRepository = $ticketRepository
                ->with(['user', 'type', 'type.area', 'price', 'resellerTicket'])
                ->select('tickets.*')
                ->join('ticket_types', 'tickets.ticket_type_id', '=', 'ticket_types.id')
                ->join('ticket_categories', 'ticket_types.ticket_category_id', '=', 'ticket_categories.id')
                ->join('areas', 'ticket_types.area_id', '=', 'areas.id')
                ->join('area_inspectors', 'area_inspectors.area_id', '=', 'areas.id')
                ->join('users', 'users.id', '=', 'area_inspectors.user_id')
                ->leftJoin('reseller_tickets', 'tickets.id', '=', 'reseller_tickets.ticket_id')
                ->where('users.id', '=', $inspector->id);

            return $next($request);
        });
    }

    public function getIndex(Request $request)
    {
        $area = $request->query->getInt('area', 0);
        $ticketType = $request->query->getInt('ticketType', 0);
        $from = $request->query->get('from', Carbon::now()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $ticketRepository = clone($this->ticketRepository);

        $countTickets = $this->getInspectorTickets($area, $ticketType, $from, $till)
            ->count();

        $areaList = collect(['0' => 'Alle']);
        $typeList = collect(['0' => 'Alle']);

        $ticketRepository->each(function ($ticket) use ($areaList, $typeList) {
            $area = $ticket->type->area;
            $category = $ticket->type->category;
            $areaList[$area->id] = $area->name;
            $typeList[$category->id] = $category->name;
        });

        return view(
            'admin.inspect.ticket.index',
            compact('countTickets', 'area', 'areaList', 'ticketType', 'typeList', 'from', 'till')
        );
    }
    
    public function getEdit($ticketId)
    {
        $ticket = Ticket::findOrFail($ticketId);
        $ticketInspection = new TicketInspection();
        $inspectedAreas = $this->getInspector()->inspectedAreas()->select('id')->get()->pluck('id')->toArray();

        if (!in_array($ticket->type->area->id, $inspectedAreas)) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->form(TicketInspectionForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::inspect:tickets.inspect.update', ['ticketId' => $ticket->id]),
            'model'     => $ticketInspection,
        ]);

        return view('admin.inspect.ticket.review', compact('ticket', 'form'));
    }

    public function postUpdate(TicketInspectionRequest $request, $ticketId)
    {
        $ticket = Ticket::findOrFail($ticketId);
        $ticketInspection = new TicketInspection();
        $inspectedAreas = $this->getInspector()->inspectedAreas()->select('id')->get()->pluck('id')->toArray();

        if (!in_array($ticket->type->area->id, $inspectedAreas)) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->form(TicketInspectionForm::class, ['model' => $ticketInspection]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->inspect($request, $ticket, $ticketInspection);

        flash()->success('Angelkarte '.(new TicketManager())->getIdentifier($ticket).' wurde kontrolliert');

        return redirect()->route('admin::inspect:tickets.index');
    }
    
    private function inspect(TicketInspectionRequest $request, Ticket $ticket, TicketInspection $inspection)
    {
        $inspection->ticket()->associate($ticket);
        $inspection->inspector()->associate($this->getInspector());

        $inspection->status = $request->request->get('status', 'OK');
        $inspection->comment = strip_tags($request->request->get('comment'));
        $inspection->location = $request->request->get('latitude') && $request->request->get('longitude')
            ? [
                'latitude'  => (float) $request->request->get('latitude') ?: null,
                'longitude' => (float) $request->request->get('longitude') ?: null,
                'accuracy'  => $request->request->getInt('accuracy') ?: null,
              ]
            : [];
        $inspection->save();
    }

    public function anyData(Request $request)
    {
        $area = $request->query->getInt('area', 0);
        $ticketType = $request->query->getInt('ticketType', 0);
        $from = $request->query->get('from', Carbon::now()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));

        $tickets = $this->getInspectorTickets($area, $ticketType, $from, $till);

        return Datatables::of($tickets)
            ->editColumn('type.area.name', function (Ticket $ticket) {
                return str_limit($ticket->type->area->name, 30);
            })
            ->editColumn('user.full_name', function (Ticket $ticket) {
                if ($ticket->user) {
                    return $ticket->user->first_name . ' ' . $ticket->user->last_name;
                } else {
                    return $ticket->resellerTicket->first_name . ' ' . $ticket->resellerTicket->last_name;
                }
            })
            ->addColumn('reseller', function (Ticket $ticket) {
                return $ticket->user ? 'online' : str_limit($ticket->resellerTicket->reseller->name, 30);
            })
            ->addColumn('actions', function (Ticket $ticket) {
                $catchBtn = $ticket->hauls()->count()
                    ? ' <a   type="button"
                             href="'.route('admin::inspect:hauls.index', ['ticket' => $ticket->id]).'"
                             class="btn btn-md btn-success btn-datatables">
                            <span class="fa fa-shopping-basket fa-fw"></span> Fänge
                            <span class="badge">' . $ticket->hauls()->count() . '</span>
                        </a>'
                    : ' <button class="btn btn-md btn-warning btn-datatables">
                            <span class="fa fa-shopping-basket fa-fw"></span> Fänge
                            <span class="badge">' . $ticket->hauls()->count() . '</span>
                        </button>';

                $controlClass = $ticket->inspections()->where('inspector_id', $this->getInspector()->id)->count()
                    ? 'btn-success'
                    : 'btn-danger';

                return '
                     <a   type="button"
                         href="' . route('admin::inspect:tickets.inspect.edit', ['ticket' => $ticket->id]) . '"
                         class="btn btn-md btn-datatables ' . $controlClass . '">
                        <span class="fa fa-check fa-fw"></span> Kontrolle
                    </a>
                    ' . $catchBtn . '
                    <button type="button" class="btn btn-md btn-primary btn-datatables"
                        data-target="#preview-modal" data-toggle="modal"
                        data-ticket-id="'.$ticket->id.'">Angelkarte
                        <span class="fa fa-eye fa-fw"></span>
                    </button>
                    

                ';
            })
            ->editColumn('created_at', function (Ticket $ticket) {
                return $ticket->created_at ? $ticket->created_at->format('d.m.Y H:i') : null;
            })
            ->blacklist(['actions', 'user.full_name', 'type.area.name', 'reseller'])
            ->make(true);
    }

    public function anyInspectionData(Request $request, int $ticketId)
    {
        $ticket = Ticket::findOrFail($ticketId);
        $inspectedAreas = $this->getInspector()->inspectedAreas()->select('id')->get()->pluck('id')->toArray();

        if (!in_array($ticket->type->area->id, $inspectedAreas)) {
            throw new AccessDeniedHttpException();
        }

        $ticketInspections = TicketInspection::where('ticket_id', $ticketId)
            ->orderBy('created_at', 'desc');

        return Datatables::of($ticketInspections)
            ->editColumn('status', function (TicketInspection $inspection) {
                return $inspection->status == 'OK'
                    ? '<span class="fa fa-fw fa-check"></span>'
                    : '<span class="fa fa-fw fa-times"></span>';
            })
            ->addColumn('inspector', function (TicketInspection $inspection) {
                return auth()->user()->hasRole('superadmin')
                    ? Html::linkRoute(
                        'admin::super:users.edit',
                        "{$inspection->inspector->full_name} ({$inspection->inspector->id})",
                        ['userId' => $inspection->inspector->id]
                    )
                    : "{$inspection->inspector->full_name} ({$inspection->inspector->id})";
            })
            ->editColumn('created_at', function (TicketInspection $inspection) {
                return $inspection->created_at->format('d.m.Y H:i');
            })
            ->make(true);
    }

    private function getInspectorTickets($area = 0, $ticketType = 0, $from = null, $till = null)
    {
        $query = $this->ticketRepository;

        if ($from && $till) {
            try {
                $from = Carbon::createFromFormat('Y-m-d', $from)->startOfDay();
                $till = Carbon::createFromFormat('Y-m-d', $till)->endOfDay();

                $query->whereDate('tickets.valid_from', '>=', $from);
                $query->whereDate('tickets.valid_to', '<=', $till);
            } catch (\InvalidArgumentException $e) {
            }
        }

        if ($area === 0) {
            $query->whereNotNull('areas.id');
        } else {
            $query->where('areas.id', '=', $area);
        }

        if ($ticketType === 0) {
            $query->whereNotNull('ticket_categories.id');
        } else {
            $query->where('ticket_categories.id', '=', $ticketType);
        }

        return $query;
    }
}
