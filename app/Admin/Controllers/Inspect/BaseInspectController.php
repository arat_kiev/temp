<?php

namespace App\Admin\Controllers\Inspect;

use App;
use App\Admin\Controllers\AdminController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;

class BaseInspectController extends AdminController
{
    private $allowedRoles = ['superadmin', 'ticket_inspector'];

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (!Auth::user() || !Auth::user()->hasAnyRole($this->allowedRoles) || !$this->getInspector() || is_null($this->getInspector())) {
                throw new AccessDeniedHttpException();
            }

            return $next($request);
        });
    }
}
