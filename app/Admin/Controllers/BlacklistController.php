<?php

namespace App\Admin\Controllers;

use DB;
use Datatables;
use Illuminate\Http\Request;
use App\Blacklist\Blacklist;
use App\Blacklist\BlacklistModel;
use App\Models\User;
use App\Models\Location\CountryTranslation;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class BlacklistController extends AdminController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (!$this->getManager()) {
                throw new AccessDeniedHttpException();
            }
            return $next($request);
        });
    }

    /**
     * Get list of blocked users
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.blacklist.index');
    }

    /**
     * Get user's data by email or fisher id and exclude already blocked ones
     *
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserBy(Request $request, User $user)
    {
        $search = trim($request->input('search'));
        $searchBy = '';

        switch ($search) {
            case (bool) preg_match('#^\w{3}-\d{3}-\w{3}$#', $search):
                $searchBy = 'fisher_id';
                break;
            case filter_var($search, FILTER_VALIDATE_EMAIL):
                $searchBy = 'email';
                break;
            default:
                return response()->json();
        }

        $user = $user->selectRaw("users.id, CONCAT($searchBy, ' (', users.first_name, ' ', users.last_name, ')') AS text")
            ->leftJoin('blacklist AS b', function($join)
            {
               $join->on('users.first_name', '=', 'b.first_name')
                    ->on('users.last_name', '=', 'b.last_name')
                    ->on('users.birthday', '=', 'b.birthday');
            })
            ->whereNull('b.id')
            ->where($searchBy, $search)
            ->get();

        return response()->json(['results' => $user]);
    }

    /**
     * Get countries
     *
     * @param Request $request
     * @param CountryTranslation $country
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountry(Request $request, CountryTranslation $country)
    {
        $search = $request->input('search');
        $countries = $country->selectRaw('country_id AS id, name AS text')
                             ->where('locale', '=',  \App::getLocale())
                             ->where('name', 'like', $search . '%')
                             ->get();
        return response()->json(['results' => $countries]);
    }

    /**
     * Remove user from blacklist by record id
     *
     * @param $record_id
     * @param Blacklist $blacklist
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeUserFromBlackList($record_id, Blacklist $blacklist)
    {
        $blacklist->RemoveFromBlacklist($record_id);
            return response()->json();
    }

    /**
     * Add OR update users by user's id / data in blacklist
     *
     * @param Request $request
     * @param Blacklist $blacklist
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addToBlackList(Request $request, Blacklist $blacklist)
    {
        $user = $request->all();
        // new user
        if(empty($user['id'])) {
            // add missed fields
            $user['manager_id'] = $this->getManager()->id;
                // user by by email or fisher_id
                if(!empty($user['user_id'])) {
                    $user_data = User::select(['first_name', 'last_name', 'birthday', 'street', 'post_code', 'city', 'country_id'])
                                ->where('id', $user['user_id'])
                                ->first()
                                ->toArray();
                    $user = array_merge($user, $user_data);
                }
            // validate
            if ($blacklist->isUserOnBlacklist($user, false)) {
                return response()->json(['error' => '<b>Achtung:</b> Bereits auf Sperrliste'], 400);
            }
        }

        $blacklist->InsertIntoBlacklist($user);
            return response()->json();
    }

    /**
     * Data for datatable
     *
     * @return mixed
     */
    public function anyData()
    {
        $blacklist = BlacklistModel::with(['country'])->where('manager_id', $this->getManager()->id);

        return Datatables::of($blacklist)
            ->addColumn('country', function ($blacklist) {
                return $blacklist->country
                    ? $blacklist->country->translate(app()->getLocale(), true)->name
                    : '';
            })
            ->editColumn('birthday', function ($blacklist) {
                return Carbon::parse($blacklist->birthday)->format('d.m.Y');
            })
            ->addColumn('actions', function () {
                return '
                    <button type="button"
                        class="btn btn-xs btn-primary edit-user"
                        data-target="#add-to-blacklist-by-user-data-modal"
                        data-toggle="modal">
                        <span class="fa fa-edit fa-fw"></span>
                    </button>
                    <button type="button"
                            class="btn btn-xs btn-danger"
                            data-target="#remove-from-blacklist-modal"
                            data-toggle="modal">
                        <span class="fa fa-trash-o fa-fw"></span>
                    </button>
                ';
            })
            ->make(true);
    }
}