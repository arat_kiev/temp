<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\ResellerForm;
use App\Admin\Requests\StoreResellerRequest;
use App\Models\Contact\Reseller;
use App\Models\Ticket\TicketPrice;
use App\Models\Product\Product;
use App\Api1\Controllers\ProductController as ApiProductController;
use Datatables;
use Html;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Carbon\Carbon;
use App;
use DB;

class ResellerController extends AdminController
{
    use FormBuilderTrait;

    private $ticketPriceWith = [];

    public function __construct()
    {
        parent::__construct();

        $this->ticketPriceWith = [
            'ticketType'    => function ($query) {
                $query->addSelect('id', 'area_id')->with([
                    'area'           => function ($query) {
                        $query->addSelect('id', 'name')
                            ->without('rule', 'gallery', 'ticketTypes', 'resellers', 'locations');
                    },
                ]);
            },
        ];
    }

    /**
     * Initial List view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $ticketPriceRows = TicketPrice::with($this->ticketPriceWith)
            ->select('id', 'value', 'ticket_type_id')
            ->where('valid_to', '>=', Carbon::now())
            ->whereHas('translations', function ($query) {
                $query->where('locale', App::getLocale());
            })
            ->whereHas('ticketType', function ($query) {
                $query->with('area')
                    ->whereHas('translations', function ($query) {
                        $query->where('locale', App::getLocale());
                    })
                    ->whereHas('area', function ($query) {
                        $query->where('manager_id', '=', $this->getManager()->id);
                    });
            })
            ->get();

        $ticketPrices = $this->prepareJsTreeData($ticketPriceRows);

        return view('admin.reseller.index', compact('ticketPrices'));
    }

    public function getEdit($resellerId)
    {
        $reseller = Reseller::findOrFail($resellerId);

        $form = $this->form(ResellerForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::resellers.update', ['resellerId' => $reseller->id]),
            'model'     => $reseller,
        ]);

        return view('admin.reseller.edit', compact('form', 'reseller'));
    }

    public function postUpdate(StoreResellerRequest $request, $resellerId)
    {
        $reseller = Reseller::findOrFail($resellerId);

        $form = $this->form(ResellerForm::class, ['model' => $reseller]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $reseller);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::resellers.edit', ['resellerId' => $reseller->id]);
    }

    private function update(StoreResellerRequest $request, Reseller $reseller)
    {
        $reseller->fill($request->all());
        $reseller->country()->associate($request->request->get('country'));
        $reseller->save();
    }

    /**
     * Get Reseller Tickets' Prices
     *
     * @param $resellerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function ticketPricesList($resellerId)
    {
        $reseller = Reseller::findOrFail($resellerId);
        $ticketPrices = $reseller->ticketPrices()
            ->select('ticket_prices.id', 'ticket_prices.ticket_type_id')
            ->where('ticket_prices.valid_to', '>=', Carbon::now())
            ->pluck('id')
            ->toArray();

        return response()->json($ticketPrices);
    }

    /**
     * Update Reseller Tickets' Prices
     *
     * @param $resellerId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTicketPrices($resellerId, Request $request)
    {
        $reseller = Reseller::findOrFail($resellerId);

        $priceIds = $request->request->get('ticketPriceIds', []);

        $reseller->ticketPrices()->sync($priceIds);

        return response()->json();
    }

    public function anyData()
    {
        $resellers = $this->getManager()->resellers()
            ->with('invoices', 'activeTickets')
            ->select(['resellers.*']);

        return Datatables::of($resellers)
            ->editColumn('name', function ($row) {
                return Html::linkRoute(
                    'admin::resellers.edit',
                    str_limit($row['name'], 100), ['resellerId' => $row['id']]
                );
            })
            ->editColumn('links', function($reseller) {
                return '
                    <a type="button"
                            href="' . route('admin::invoices.index', ['reseller' => $reseller->id]) . '"
                            class="btn btn-xs btn-warning"
                            target="_blank">
                        <span class="fa fa-file-text-o fa-fw"></span>
                        Abrechnungen <span class="badge">' . $reseller->invoices->count() . '</span>
                    </a>
                    <a type="button"
                            href="' . route('admin::sales.index', ['reseller' => $reseller->id]) . '"
                            class="btn btn-xs btn-primary"
                            target="_blank">
                        <span class="fa fa-ticket fa-fw"></span>
                        Angelkarten <span class="badge">' . $reseller->activeTickets->count() . '</span>
                    </a>
                    <button type="button"
                            class="btn btn-xs btn-primary"
                            data-action="' . route('admin::resellers.products.index', ['resellerId' => $reseller->id]) . '"
                            data-target="#product-modal"
                            data-toggle="modal">
                        <span class="fa fa-cart-arrow-down fa-fw"></span>
                        Produkte <span class="badge">
                            ' . ($reseller->products ? $reseller->products->count() : 0) . '
                        </span>
                    </button>
                ';
            })
            ->editColumn('updated_at', function($reseller) {
                return $reseller->updated_at->diffForHumans();
            })
            ->addColumn('actions', function ($reseller) {
                return '
                    <a class="btn btn-xs btn-primary"
                       data-target="#modal-edit"
                       data-toggle="modal"
                       data-record="'.$reseller->id.'">
                        <i class="glyphicon glyphicon-edit"></i> Zuweisen
                    </a>
                ';
            })
            ->blacklist(['admin_count'])
            ->make(true);
    }

    public function getProducts(Request $request, $resellerId)
    {
        $reseller = Reseller::findOrFail($resellerId);

        $productIds = $reseller->products()->pluck('products.id')->toArray();
        $request->query->set('ids', $productIds);
        $request->query->set('_perPage', count($productIds));

        return with(new ApiProductController(new Product()))->index($request);
    }

    public function postProducts(Request $request, $resellerId)
    {
        $reseller = Reseller::findOrFail($resellerId);

        $products = $request->request->get('products', []);
        $reseller->products()->sync($products);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->back();
    }

    private function prepareJsTreeData($ticketPrices)
    {
        $ticketTypesData = $this->fetchJsTreeTicketTypes($ticketPrices);
        $data = $this->fetchJsTreeAreas($ticketTypesData);

        return $data;
    }

    private function fetchJsTreeTicketTypes(Collection $ticketPrices)
    {
        $ticketTypeIcon = 'glyphicon glyphicon-menu-hamburger';
        $ticketPriceIcon = 'glyphicon glyphicon-euro';
        $data = [];

        foreach ($ticketPrices as $ticketPrice) {
            $typeId = $ticketPrice->ticketType->id;
            $ticketPriceName = number_format($ticketPrice->value / 100.0, 2, ',', '.')
                               . ' <span class="glyphicon glyphicon-chevron-right"></span> '
                               . $ticketPrice->name;

            if (!isset($data[$typeId])) {
                $data[$typeId] = [
                    'id'        => 'type' . $typeId,
                    'text'      => $ticketPrice->ticketType->name,
                    'icon'      => $ticketTypeIcon,
                    'areaId'    => $ticketPrice->ticketType->area->id,
                    'areaName'  => $ticketPrice->ticketType->area->name,
                ];
            }

            $data[$typeId]['children'][] = [
                'id' => $ticketPrice->id,
                'text' => $ticketPriceName,
                'icon' => $ticketPriceIcon
            ];
        }

        return array_values($data);
    }

    private function fetchJsTreeAreas(array $ticketTypes)
    {
        $data = [];

        foreach ($ticketTypes as $ticketType) {
            $areaId = $ticketType['areaId'];

            if (!isset($data[$areaId])) {
                $data[$areaId] = [
                    'id'    => 'area' . $areaId,
                    'text'  => $ticketType['areaName'],
                ];
            }

            $data[$areaId]['children'][] = $ticketType;
        }

        return array_values($data);
    }
}
