<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\PictureForm;
use App\Admin\Requests\StorePictureRequest;
use App\Api1\Requests\ImageUploadRequest;
use App\Models\Area\Area;
use App\Models\Gallery;
use App\Models\Picture;
use App\Models\User;
use Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class GalleryController extends AdminController
{
    use FormBuilderTrait;

    public function getList($areaId)
    {
        $area = Area::findOrFail($areaId);
        $gallery = $area->gallery;

        return view('admin.gallery.list', compact('gallery', 'area'));
    }

    public function getEdit($areaId, $pictureId)
    {
        $area = Area::findOrFail($areaId);

        if (!$area->gallery || !($picture = $area->gallery->pictures()->find(['id' => $pictureId])->first())) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->form(PictureForm::class, [
            'method' => 'POST',
            'url' => route('admin::areas.gallery.save', ['areaId' => $areaId, 'pictureId' => $pictureId]),
            'model' => $picture,
        ]);

        return view('admin.gallery.edit', compact('area', 'picture', 'form'));
    }

    public function postSave(StorePictureRequest $request, $areaId, $pictureId)
    {
        $area = Area::findOrFail($areaId);

        if (!($gallery = $area->gallery) || !($picture = $gallery->pictures()->find(['id' => $pictureId])->first())) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->form(PictureForm::class, ['model' => $picture]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        if ($request->get('priority') == 5) {
            $area->gallery->pictures()->wherePivot('priority', '=', 5)->each(function (Picture $picture) use ($gallery) {
                $gallery->pictures()->updateExistingPivot($picture->id, ['priority' => 4], false);
            });
        }

        $gallery->pictures()->updateExistingPivot($picture->id, ['priority' => $request->get('priority')], false);
        $picture->update($request->only(['name', 'description']));

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::areas.gallery.list', ['areaId' => $areaId]);
    }

    public function getDelete($areaId, $pictureId)
    {
        /** @var Area $area */
        $area = Area::findOrFail($areaId);

        if ($area->manager->id != $this->getManager()->id) {
            throw new AccessDeniedHttpException();
        }

        if (!($gallery = $area->gallery) || !($picture = $gallery->pictures()->find(['id' => $pictureId])->first())) {
            throw new AccessDeniedHttpException();
        }

        $picture->delete();

        flash()->success('Bild wurde gelöscht.');

        return redirect()->route('admin::areas.gallery.list', ['areaId' => $areaId]);
    }

    public function postUpload(ImageUploadRequest $request, $areaId)
    {
        /** @var User $user */
        $user = Auth::user();
        /** @var Area $area */
        $area = Area::findOrFail($areaId);

        if ($area->manager->id != $this->getManager()->id) {
            throw new AccessDeniedHttpException();
        }

        $picture = new Picture([
            'file' => $request->file('file'),
        ]);
        $picture->author()->associate($user);

        $gallery = $area->gallery ?: Gallery::create(['name' => $area->name]);
        $gallery->pictures()->save($picture);
        $area->gallery()->associate($gallery);
        $area->save();

        return response()->json();
    }
}
