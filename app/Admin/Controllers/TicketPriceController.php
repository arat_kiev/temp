<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\TicketPriceForm;
use App\Admin\Requests\AssignOrganizationsRequest;
use App\Admin\Requests\StoreTicketPriceRequest;
use App\Api1\Controllers\OrganizationController as ApiOrganizationController;
use App\Events\TicketPriceCreated;
use App\Models\Area\Area;
use App\Models\Organization\Organization;
use App\Models\Ticket\TicketPrice;
use App\Models\Ticket\TicketType;
use App\Traits\ModelQuery;
use Event;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use DB;

class TicketPriceController extends AdminController
{
    use FormBuilderTrait,
        ModelQuery;

    public function getIndex($areaId, $ticketTypeId)
    {
        $manager = $this->getManager();

        $area = Area::findOrFail($areaId);

        if ($manager->id != $area->manager_id && $manager->id != $area->manager->parent_id) {
            throw new AccessDeniedHttpException();
        }

        $ticketType = $this->findOrAccessDenied($area->ticketTypes(), $ticketTypeId);

        return view('admin.ticketprices.index', compact('area', 'ticketType'));
    }

    public function getOrganizations(Request $request, $areaId, $ticketTypeId, $ticketPriceId)
    {
        $manager = $this->getManager();

        $area = $this->findOrAccessDenied($manager->areas(), $areaId);
        $ticketType = $this->findOrAccessDenied($area->ticketTypes(), $ticketTypeId);
        $ticketPrice = $this->findOrAccessDenied($ticketType->prices(), $ticketPriceId);

        $organizationIds = $ticketPrice->organizations()->pluck('organizations.id')->toArray();
        $request->query->set('organization_id', $organizationIds);
        $request->query->set('_perPage', count($organizationIds));

        return with(new ApiOrganizationController(new Organization()))->index($request);
    }

    public function getAllOrganizations(Request $request)
    {
        return with(new ApiOrganizationController(new Organization()))->index($request);
    }
    
    public function postOrganizations(AssignOrganizationsRequest $request, $areaId, $ticketTypeId, $ticketPriceId)
    {
        $manager = $this->getManager();

        $area = $this->findOrAccessDenied($manager->areas(), $areaId);
        $ticketType = $this->findOrAccessDenied($area->ticketTypes(), $ticketTypeId);
        $ticketPrice = $this->findOrAccessDenied($ticketType->prices(), $ticketPriceId);

        $organizations = $request->request->get('organizations', []);
        $ticketPrice->organizations()->sync($organizations);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::ticketprices.index', [
            'areaId'        => $area->id,
            'ticketTypeId'  => $ticketType->id
        ]);
    }

    public function getCreate($areaId, $ticketTypeId)
    {
        $manager = $this->getManager();

        $area = $this->findOrAccessDenied($manager->areas(), $areaId);
        $ticketType = $this->findOrAccessDenied($area->ticketTypes(), $ticketTypeId);

        $ticketPrice = new TicketPrice();
        $ticketPrice->ticketType()->associate($ticketTypeId);

        $form = $form = $this->form(TicketPriceForm::class, [
            'method' => 'POST',
            'url' => route('admin::ticketprices.store', ['areaId' => $areaId, 'ticketTypeId' => $ticketTypeId]),
            'model' => $ticketPrice,
        ]);

        return view('admin.ticketprices.edit', compact('area', 'ticketType', 'ticketPrice', 'form'));
    }

    public function postStore(StoreTicketPriceRequest $request, $areaId, $ticketTypeId)
    {
        $manager = $this->getManager();

        $area = $this->findOrAccessDenied($manager->areas(), $areaId);
        $ticketType = $this->findOrAccessDenied($area->ticketTypes(), $ticketTypeId);
        $ticketPrice = new TicketPrice();

        $form = $this->form(TicketPriceForm::class, ['model' => $ticketPrice]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $value = $request->request->get('value');
        $value = str_replace(',', '.', $value);
        $value = ((int)$value) * 100;

        DB::transaction(function () use ($request, $ticketPrice, $ticketType, $value) {
            $this->update($request, $ticketPrice, $ticketType, $value);
        });

        flash()->success('Daten erfolgreich gespeichert');

        Event::fire(new TicketPriceCreated($ticketPrice));

        return $ticketPrice->visible_from->isFuture() || Auth::user()->hasRole('superadmin')
            ? redirect()->route('admin::ticketprices.edit', [
                'areaId'        => $area->id,
                'ticketTypeId'  => $ticketType->id,
                'ticketPriceId' => $ticketPrice->id,
            ])
            : redirect()->route('admin::ticketprices.index', [
                'areaId'        => $area->id,
                'ticketTypeId'  => $ticketType->id,
            ]);

    }
    
    public function getEdit($areaId, $ticketTypeId, $ticketPriceId)
    {
        $manager = $this->getManager();

        $area = Area::findOrFail($areaId);

        if ($manager->id != $area->manager_id && $manager->id != $area->manager->parent_id) {
            throw new AccessDeniedHttpException();
        }

        $ticketType = $this->findOrAccessDenied($area->ticketTypes(), $ticketTypeId);
        $ticketPrice = $this->findOrAccessDenied($ticketType->prices(), $ticketPriceId);

        $form = $this->form(TicketPriceForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::ticketprices.update', [
                'areaId'        => $areaId,
                'ticketTypeId'  => $ticketTypeId,
                'ticketPriceId' => $ticketPriceId,
            ]),
            'model'     => $ticketPrice,
        ]);

        return view('admin.ticketprices.edit', compact('form', 'area', 'ticketType', 'ticketPrice'));
    }

    public function postUpdate(StoreTicketPriceRequest $request, $areaId, $ticketTypeId, $ticketPriceId)
    {
        $manager = $this->getManager();

        $area = $this->findOrAccessDenied($manager->areas(), $areaId);
        $ticketType = $this->findOrAccessDenied($area->ticketTypes(), $ticketTypeId);
        $ticketPrice = $this->findOrAccessDenied($ticketType->prices(), $ticketPriceId);

        $value = $request->request->get('value');
        $value = str_replace(',', '.', $value);
        $value = ((int)$value) * 100;
        $warning = null;

        $form = $this->form(TicketPriceForm::class, ['model' => $ticketPrice]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        if (!($ticketPrice->visible_from->isFuture()) && $ticketPrice->value !== $value) {
            $value = $ticketPrice->value;
            $warning = 'Innerhalb des "Sichtbar ab" bzw. der Saison kann der Preis in € nicht geändert werden.';
        }
        if ($ticketPrice->tickets() && $ticketPrice->value !== $value) {
            $value = $ticketPrice->value;
            $warning = 'Es wurden bereits Angelkarten verkauft. Preis in € wurde nicht geändert. Bitte wenden Sie sich an die Administratoren.';
        }

        $this->update($request, $ticketPrice, $ticketType, $value);

        if ($warning) {
            flash()->warning($warning);
        } else {
            flash()->success('Daten erfolgreich gespeichert.');
        }

        return redirect()->route('admin::ticketprices.edit', [
                'areaId'        => $area->id,
                'ticketTypeId'  => $ticketType->id,
                'ticketPriceId' => $ticketPrice->id,
            ]);
    }

    private function update(StoreTicketPriceRequest $request, TicketPrice $ticketPrice, TicketType $ticketType, $value)
    {
        if (!$ticketPrice->ticketType) {
            $ticketPrice->ticketType()->associate($ticketType);
        }

        $ticketPrice->name = $request->request->get('name');
        $ticketPrice->type = $request->request->get('type');
        $ticketPrice->value = $value;
        $ticketPrice->group_count = $request->request->get('group_count', 0);
        $ticketPrice->show_vat = $request->request->getBoolean('show_vat');
        $ticketPrice->age = $request->request->get('age');
        $ticketPrice->visible_from = Carbon::createFromFormat('d.m.Y', $request->request->get('visible_from'));
        $ticketPrice->valid_from = Carbon::createFromFormat('d.m.Y', $request->request->get('valid_from'));
        $ticketPrice->valid_to = Carbon::createFromFormat('d.m.Y', $request->request->get('valid_to'));
        $ticketPrice->save();

        $ticketPrice->weekdays()->delete();

        foreach ($request->request->get('weekdays', []) as $weekday) {
            $ticketPrice->weekdays()->create([
                'weekday' => $weekday,
            ]);
        }

        $ticketPrice->lockPeriods()->delete();

        foreach ($request->request->get('lock_periods', []) as $lock_period) {
            $ticketPrice->lockPeriods()->create([
                'lock_from' => Carbon::createFromFormat('d.m.Y', $lock_period['lock_from_string']),
                'lock_till' => Carbon::createFromFormat('d.m.Y', $lock_period['lock_till_string']),
            ]);
        }
    }
}
