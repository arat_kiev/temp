<?php

namespace App\Admin\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $loginView = 'admin.auth.login';

    protected $linkRequestView = 'admin.auth.reset';

    protected $redirectTo = '/';

    protected $loginPath = 'login';

    protected $subDomain = 'admin';

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->middleware(function ($request, $next) {
            $this->subDomain = $subDomain = Route::input('pos', config('app.sub_domains.admin'));

            View::share('portal', $subDomain);
            View::share('resetLink',  $subDomain == config('app.sub_domains.admin')
                ? route('admin::reset')
                : route('pos::reset', ['pos' => $subDomain]));

            return $next($request);
        });
    }

    public function showLoginForm(Request $request)
    {
        return view($this->loginView);
    }

    public function redirectTo()
    {
        app('session')->forget('url.intended');
        return route('pos::home', ['pos' => $this->subDomain]);
    }
}