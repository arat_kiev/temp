<?php

namespace App\Admin\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\PasswordReset;
use App\Models\User;
use App\Events\PasswordReset as PasswordResetEvent;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;


    protected $redirectTo = '/';

    protected $linkRequestView = 'admin.auth.reset';

    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware(function ($request, $next) {
            $this->subDomain = $subDomain = Route::input('pos', 'admin');

            View::share('portal', $subDomain);
            View::share('resetLink',  $subDomain == 'admin'
                ? route('admin::reset')
                : route('pos::reset', ['pos' => $subDomain]));

            return $next($request);
        });
    }

    public function getPasswordReset(Request $request)
    {
        if (Auth::user()) {
            return redirect()->to($this->redirectPath);
        }

        return $this->showResetForm($request);
    }

    public function postPasswordReset(Request $request)
    {
        $this->validateResetEmail($request);

        $email = trim($request->request->get('email'));
        $user = User::where('email', '=', $email)->first();
        $client = Client::where('name', 'bissanzeiger')->firstOrFail();
        $token = \Uuid::generate(4);

        if (PasswordReset::where('emails', '=', $email)->count() >= 3) {
            flash()->error('Zu viele Anfragen');
            return redirect()->back()->withInput();
        }

        PasswordReset::create([
            'emails'    => $email,
            'token'     => $token,
        ]);

        if ($user) {
            Event::fire(new PasswordResetEvent($user, $token, $client));
        }

        flash()->success('Der Link zum Zurücksetzen Ihres Passwortes wurde an Ihre E-Mail-Adresse gesendet.');

        return redirect()->route('pos::login', ['pos' => $this->subDomain]);
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view($this->linkRequestView)->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    private function validateResetEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);
    }
}