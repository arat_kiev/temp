<?php

namespace App\Admin\Controllers;

use App\Models\Contact\Manager;
use App\Models\Haul;
use App\Models\Ticket\Ticket;
use Auth;
use Cache;
use Carbon\Carbon;
use Carbon\CarbonInterval;

class DashboardController extends AdminController
{
    public function getIndex()
    {
        $user = Auth::user();

        if ($this->getInspector() && !$user->hasRole(['superadmin'])) {
            return redirect()->route('admin::inspect:tickets.index');
        } elseif ($user->hasRole(['organization']) && !$user->hasRole(['superadmin'])) {
            return redirect()->route('admin::organization.index');
        } elseif ($user->rentals()->count() && !$user->hasRole(['superadmin'])) {
            return redirect()->route('admin::rental.index');
        }

        $areasCount = $haulsCount = $salesCount = 0;

        if ($manager = $this->getManager()) {
            $from = Carbon::now()->startOfYear();
            $till = Carbon::now()->endOfDay();

            $areasCount = Cache::remember('dash_count_areas_' . $manager->id, 15, function () use ($manager) {
                return $manager->areas()->count();
            });

            $haulsCount = Cache::remember('dash_count_hauls_' . $manager->id, 15, function () use ($manager, $from) {
                return $manager->hauls()->whereDate('catch_date', '>', $from)->count();
            });

            $salesCount = Cache::remember('dash_count_sales_' . $manager->id, 15, function () use ($manager, $from, $till) {
                return $this->getManagerTickets($manager, $from, $till)->count();
            });
        }

        return view('admin.dashboard.index', compact('areasCount', 'haulsCount', 'salesCount'));
    }

    public function getTicketsPerArea()
    {
        if (!$manager = $this->getManager()) {
            return response()->json([], 400);
        }

        $from = Carbon::now()->subDays(7)->startOfDay();
        $till = Carbon::now()->endOfDay();

        // Sales per area
        $cacheKey = 'dash_area_tickets_' . md5(json_encode([$manager, $from, $till]));
        $areaCounts = Cache::remember($cacheKey, 15, function () use ($manager, $from, $till) {
            return $this->getManagerTickets($manager, $from, $till)
                ->select(\DB::raw('DATE(tickets.created_at) as date, COUNT(tickets.id) as quantity'))
                ->addSelect(['areas.id as area_id', 'areas.name as area_name'])
                ->groupBy(['area_id', 'date'])
                ->orderBy('area_id')
                ->orderBy('date')
                ->get();
        });

        $areaIds = $areaCounts->pluck('area_name', 'area_id')->unique();

        $areaTickets = [0 => [], 1=> [], 2 => [], 3 => [], 4 => [], 5=> [], 6 => []];
        $this->getDayRanges($from, $till)->each(function ($day, $di) use (&$areaTickets, $areaCounts, $areaIds) {
            $areaTickets[$di]['day'] = $day;

            $areaIds->each(function ($areaName, $areaId) use (&$areaTickets, $areaCounts, $day, $di) {
                $entry = $areaCounts->where('date', $day)->where('area_id', $areaId)->first();

                $areaTickets[$di][$areaId] = $entry ? $entry->quantity : 0;
            });
        });

        // Total sales
        $cacheKey = 'dash_tickets_' . md5(json_encode([$manager, $from, $till]));
        $totalCounts = Cache::remember($cacheKey, 15, function () use ($manager, $from, $till) {
            return $this->getManagerTickets($manager, $from, $till)
                ->select(\DB::raw('DATE(tickets.created_at) as day, COUNT(tickets.id) as quantity'))
                ->groupBy('day')
                ->orderBy('day')
                ->get();
        });

        $totalTickets = [0 => [], 1=> [], 2 => [], 3 => [], 4 => [], 5=> [], 6 => []];
        $this->getDayRanges($from, $till)->each(function ($day, $di) use (&$totalTickets) {
            $totalTickets[$di] = [
                'day'       => $day,
                'quantity'  => 0,
            ];
        });

        $totalTickets = $totalCounts->reduce(function ($carry, $item) use ($from) {
            $date = Carbon::parse($item->day);
            $carry[$from->startOfDay()->diffInDays($date)]['quantity'] = $item->quantity;
            return $carry;
        }, $totalTickets);

        return response()->json([
            'total'     => $totalTickets,
            'perArea'   => $areaTickets,
            'areas'     => $areaIds->map(function ($item, $key) {
                return ['id' => $key, 'name' => $item];
            })->values(),
        ]);
    }

    public function getTicketsPerMonth()
    {
        if (!$manager = $this->getManager()) {
            return response()->json([], 400);
        }

        $from = Carbon::now()->startOfYear();
        $till = Carbon::now()->endOfDay();

        $cacheKey = 'dash_month_tickets_' . md5(json_encode([$manager, $from, $till]));
        $monthCounts = Cache::remember($cacheKey, 15, function () use ($manager, $from, $till) {
            return $this->getManagerTickets($manager, $from, $till)
                ->select(\DB::raw("CONCAT(YEAR(tickets.created_at), '-', LPAD(MONTH(tickets.created_at), 2, '0')) as month, COUNT(tickets.id) as quantity"))
                ->groupBy('month')
                ->orderBy('month')
                ->get();
        });

        $monthRanges = $this->getMonthRanges($from, $till);
        $result = $monthRanges->map(function ($month) use ($monthCounts) {
            $monthCount = $monthCounts->where('month', $month)->first();
            return [
                'month' => $month,
                'quantity' => $monthCount ? $monthCount->quantity : 0,
            ];
        });

        return response()->json($result);
    }

    public function getCatchesPerFishType()
    {
        if (!$manager = $this->getManager()) {
            return response()->json([], 400);
        }
        $from = Carbon::now()->startOfYear();
        $till = Carbon::now()->endOfDay();

        $cacheKey = 'dash_fish_catches_' . md5(json_encode([$manager, $from, $till]));
        $catchesCount = Cache::remember($cacheKey, 1440, function () use ($manager, $from, $till) {
            return \DB::table('hauls')
                ->join('areas', 'areas.id', '=', 'hauls.area_id')
                ->join('managers', 'managers.id', '=', 'areas.manager_id')
                ->join('fish_translations', 'fish_translations.fish_id', '=', 'hauls.fish_id')
                ->select(\DB::raw("fish_translations.name as label, COUNT(hauls.id) as value"))
                ->where(function ($query) use ($manager) {
                    $query->where('managers.id', '=', $manager->id)
                        ->orWhere('managers.parent_id', '=', $manager->id);
                })
                ->whereBetween('catch_date', [$from, $till])
                ->groupBy('hauls.fish_id')
                ->orderBy('value', 'desc')
                ->get();
        });

        return response()->json($catchesCount);
    }

    /**
     * @param Manager $manager
     * @param Carbon|null $from
     * @param Carbon|null $till
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function getManagerTickets(Manager $manager, Carbon $from = null, Carbon $till = null)
    {
        $query = Ticket::withoutStorno()
            ->select('tickets.*')
            ->join('ticket_types', 'tickets.ticket_type_id', '=', 'ticket_types.id')
            ->join('areas', 'ticket_types.area_id', '=', 'areas.id')
            ->join('managers', 'areas.manager_id', '=', 'managers.id')
            ->where(function ($query) use ($manager) {
                $query->where('managers.id', '=', $manager->id)
                    ->orWhere('managers.parent_id', '=', $manager->id);
            });

        $from = $from ?: Carbon::now()->startOfYear();
        $till = $till ?: Carbon::now()->endOfDay();
        $query->whereBetween('tickets.created_at', [$from, $till]);

        return $query;
    }

    /**
     * Get all days (full format) between $start and $end
     *
     * @param Carbon $start
     * @param Carbon $end
     * @return \Illuminate\Support\Collection
     */
    private function getDayRanges(Carbon $start, Carbon $end)
    {
        $interval = CarbonInterval::day();
        $dateRange = new \DatePeriod($start, $interval, $end);

        $days = collect();
        foreach ($dateRange as $date) {
            $days->push($date->format('Y-m-d'));
        }

        return $days;
    }

    /**
     * Get all months (with year) between $start and $end
     *
     * @param Carbon $start
     * @param Carbon $end
     * @return \Illuminate\Support\Collection
     */
    private function getMonthRanges(Carbon $start, Carbon $end)
    {
        $interval = CarbonInterval::month();
        $dateRange = new \DatePeriod($start, $interval, $end);

        $months = collect();
        foreach ($dateRange as $date) {
            $months->push($date->format('Y-m'));
        }

        return $months;
    }
}
