<?php

namespace App\Admin\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Api1\Controllers\UserController as ApiUserController;

class UserController extends AdminController
{
    private $defaultSelect = ['id', 'email', 'name', 'first_name', 'last_name'];
    private $defaultLimit = 15;

    public function search(Request $request)
    {
        return with(new ApiUserController(new User()))->index($request);
    }

    public function all(Request $request)
    {
        return $this->fetchUsers($request)->toArray();
    }

    private function fetchUsers(Request $request)
    {
        $users = User::select($this->defaultSelect);

        if ($userName = $request->query->get('user', '')) {
            $users->where('fisher_id', "$userName")
                ->orWhere('email', "$userName");
        }

        if ($request->query->getBoolean('isNotManagerInspector', false)) {
            $users->doesNotBelongsToManagerInspector($this->getManager());
        }

        if ($request->query->getBoolean('isManagerInspector', false)) {
            $users->belongsToManagerInspector($this->getManager());
        }

        return $users->limit($this->defaultLimit)->get();
    }
}
