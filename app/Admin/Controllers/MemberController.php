<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\MemberForm;
use App\Admin\Requests\StoreMemberRequest;
use App\Admin\Requests\StoreCSVUploadRequest;
use App\Events\MemberChanged;
use App\Jobs\ImportOrganizationMembers;
use App\Models\Organization\Organization;
use App\Models\Organization\Member;
use App\Models\User;
use Carbon\Carbon;
use Datatables;
use Event;
use Exception;
use Illuminate\Support\Collection;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Models\Organization\CSVUpload;
use App\Jobs\DistributeCSVUploadContentIntoCSVRows;
use Excel;
use Illuminate\Http\Request;
use App\Models\Organization\CsvData;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class MemberController extends AdminController
{
    use FormBuilderTrait;

    public function getIndex()
    {
        $runningImports = $this->getOrganization()->imports()->inProgress()->get();

        return view('admin.member.index', compact('runningImports'));
    }

    public function getEdit($memberId)
    {
        $member = $this->getOrganization()->members()->findOrFail($memberId);

        $form = $this->form(MemberForm::class, [
            'method' => 'POST',
            'url' => route('admin::members.update', ['memberId' => $memberId]),
            'model' => $member,
        ]);

        return view('admin.member.edit', compact('form', 'member'));
    }

    public function postUpdate(StoreMemberRequest $request, $memberId)
    {
        $organization = $this->getOrganization();
        $member = $organization->members()->findOrFail($memberId);

        $form = $this->form(MemberForm::class, ['model' => $member]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $organization, $member);

        flash()->success('Daten erfolgreich gespeichert');

        if ($request->get('action') == 'save') {
            return redirect()->route('admin::members.edit', ['memberId' => $member->id]);
        } else {
            return redirect()->route('admin::members.index');
        }
    }

    public function getCreate()
    {
        $member = new Member();

        $form = $form = $this->form(MemberForm::class, [
            'method' => 'POST',
            'url' => route('admin::members.store'),
            'model' => $member,
        ]);

        return view('admin.member.edit', compact('form', 'member'));
    }

    public function postStore(StoreMemberRequest $request)
    {
        $organization = $this->getOrganization();

        $member = new Member();

        $form = $this->form(MemberForm::class, ['model' => $member]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $organization, $member);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::members.edit', ['memberId' => $member->id]);
    }

    public function getDelete($memberId)
    {
        $organization = $this->getOrganization();

        $member = $organization->members()->findOrFail($memberId);

        try {
            $member->delete();

            flash()->success('Mitglied wurde entfernt.');
            return redirect()->route('admin::members.index');
        } catch (Exception $e) {
            flash()->error('Mitglied konnte nicht entfernt werden.');
            return redirect()->back();
        }
    }

    private function update(StoreMemberRequest $request, Organization $organization, Member $member)
    {
        if (!$member->organization) {
            $member->organization()->associate($organization);
        }

        $since = $request->request->get('since');
        $birthday = $request->request->get('birthday');
        $leave_date = $request->request->get('leave_date');

        // Standard values
        $member->first_name = $request->request->get('first_name');
        $member->last_name = $request->request->get('last_name');
        $member->function = $request->request->get('function');
        $member->member_id = $request->request->get('member_id');
        $member->fisher_id = $request->request->get('fisher_id');
        $member->since = $since ? Carbon::createFromFormat('d.m.Y', $since) : null;
        $member->email = $request->request->get('email');
        $member->gender = $request->request->get('gender');
        $member->birthday = $birthday ? Carbon::createFromFormat('d.m.Y', $birthday) : null;
        $member->street = $request->request->get('street');
        $member->phone = $request->request->get('phone');
        $member->leave_date = $leave_date ? Carbon::createFromFormat('d.m.Y', $leave_date) : null;
        $member->comment = $request->request->get('comment');

        $member->save();

        // Relations
        Event::fire(new MemberChanged($member));

        if ($request->request->getInt('country')) {
            $member->country()->associate($request->request->getInt('country'));
        } else {
            $member->country()->dissociate();
        }

        $member->save();
    }

    public function anyData()
    {
        $organization = $this->getOrganization();

        $members = $organization ? $organization->members()->with('user') : collect();

        return Datatables::of($members)
            ->editColumn('since', function ($member) {
                return $member->since ? $member->since->format('d.m.Y') : '';
            })
            ->addColumn('connected', function ($member) {
                if ($member->user) {
                    return '<span class="fa fa-check fa-fw"></span>';
                }

                return '<span class="fa fa-times fa-fw"></span>';
            })
            ->editColumn('fisher_id', function ($member) {
                return $member->user ? $member->user->fisher_id : $member->fisher_id;
            })
            ->addColumn('member_edit', function ($member) {
                $editUrl = route('admin::members.edit', ['memberId' => $member->id]);
                $removeUrl = route('admin::members.delete', ['memberId' => $member->id]);

                return '
                    <a href="' . $editUrl . '" type="button" class="btn btn-xs btn-primary">
                        <span class="fa fa-edit"></span>
                        Bearbeiten
                    </a>
                    <div class="btn-group">
                        <button type="button"
                                class="btn btn-default btn-xs dropdown-toggle"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                            <span class="fa fa-remove fa-fw"></span>
                            Löschen
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="' . $removeUrl . '" class="locations-btn-remove bg-danger">
                                    <span class="fa fa-exclamation fa-fw"></span>
                                    Ja, wirklich löschen
                                </a>
                            </li>
                        </ul>
                    </div>
                ';
            })
            ->make(true);
    }

    public function parseImport(StoreCSVUploadRequest $request)
    {
        if (!$organization = $this->getOrganization()) {
            throw new AccessDeniedHttpException('Not an organization admin');
        }

        $path = $request->file('csvFile')->getRealPath();

        if ($request->has('hasHeaders')) {
            $data = Excel::load($path, function ($reader) {
            })->get()->toArray();
        } else {
            $data = array_map('str_getcsv', file($path));
        }

        $csvCount = count($data);

        if ($csvCount > 0) {
            if ($request->has('hasHeaders')) {
                $csvHeaderFields = [];
                foreach ($data[0] as $key => $value) {
                    $csvHeaderFields[] = $key;
                }
            }
            $csvData = array_slice($data, 0, 5);
            $csvDataFile = $organization->imports()->create([
                'csv_filename' => $request->file('csvFile')->getClientOriginalName(),
                'csv_header' => $request->has('hasHeaders'),
                'csv_data' => json_encode($data)
            ]);

            $html = view('admin.member.csv-uploads.map-columns',
                compact('csvHeaderFields', 'csvData', 'csvDataFile', 'csvCount')
            )->render();

            return response()->json(['html' => $html]);
        }

        return response()->json(['html' => null]);
    }

    public function processImport(Request $request)
    {
        if (!$organization = $this->getOrganization()) {
            throw new AccessDeniedHttpException('Not an organization admin');
        }

        dispatch(new ImportOrganizationMembers($organization, $request->csvDataFileId, $request->fields));

        return redirect()->route('admin::members.index');
    }

}
