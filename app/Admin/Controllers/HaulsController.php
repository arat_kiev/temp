<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\HaulForm;
use App\Admin\Requests\HaulRequest;
use App\Admin\Requests\MultiHaulRequest;
use App\Api1\Transformers\TicketTransformer;
use App\Exceptions\ApplicationException;
use App\Managers\Export\HaulExportManager;
use App\Managers\TicketManager;
use App\Models\Haul;
use App\Models\Meta\FishingMethod;
use App\Models\Picture;
use App\Models\Ticket\Checkin;
use App\Models\Ticket\Ticket;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Datatables;
use DB;

class HaulsController extends AdminController
{
    use FormBuilderTrait;

    private $managerId;

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (!$this->getManager()) {
                throw new AccessDeniedHttpException();
            }

            $this->managerId = $this->getManager()->id;

            return $next($request);
        });
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        return view('admin.hauls.index', $this->indexData($request));
    }

    /**
     * @param Request $request
     * @return Collection
     */
    private function indexData(Request $request)
    {
        $data = $this->fetchIndexRequestData($request);
        $count = $this->getManagerHauls($data)->count();

        $areaList = collect([
            '0'     => '-- Alle --',
        ]);

        $fishList = collect([
            '-1'    => '-- Alle --',
            '0'     => '-- Keine --',
        ]);

        $techniqueList = collect([
            '-1'    => '-- Alle --',
            '0'     => '-- Keine --',
        ]);

        $fishingMethodsList = collect([
            '-1'    => '-- Alle --',
            '0'     => '-- Keine --',
        ]);

        $this->getManager()->areas()->each(function ($area) use ($areaList, $fishList, $techniqueList, $fishingMethodsList) {
            $areaList[$area->id] = $area->name;

            $area->fishes()->each(function ($fish) use ($fishList) {
                $fishList[$fish->id] = $fish->name;
            });

            $area->techniques()->each(function ($technique) use ($techniqueList) {
                $techniqueList[$technique->id] = $technique->name;
            });

            FishingMethod::all()->each(function ($fishingMethod) use ($fishingMethodsList) {
                $fishingMethodsList[$fishingMethod->id] = $fishingMethod->translate(app()->getLocale(), true)->name;
            });
        });

        $this->getManager()->additionalAreas()->each(function ($area) use ($areaList, $fishList, $techniqueList, $fishingMethodsList) {
            $areaList[$area->id] = $area->name;

            $area->fishes()->each(function ($fish) use ($fishList) {
                $fishList[$fish->id] = $fish->name;
            });

            $area->techniques()->each(function ($technique) use ($techniqueList) {
                $techniqueList[$technique->id] = $technique->name;
            });

            FishingMethod::all()->each(function ($fishingMethod) use ($fishingMethodsList) {
                $fishingMethodsList[$fishingMethod->id] = $fishingMethod->translate(app()->getLocale(), true)->name;
            });
        });

        return $data->merge(compact('count', 'areaList', 'fishList', 'techniqueList', 'fishingMethodsList' )) ;
    }

    /**
     * @param string $format
     * @param Request $request
     * @param HaulExportManager $exportManager
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getExport($format = 'xls', Request $request, HaulExportManager $exportManager)
    {
        $data = $this->fetchIndexRequestData($request);
        $hauls = $this->getManagerHauls($data);
        $from = $request->query->get('from');
        $till = $request->query->get('till');
        $taken = $request->query->get('taken');
        $count = $request->query->get('count');
        $fishingMethod = $request->query->get('fishingMethod');
        $file = $exportManager->run($hauls, $format, compact('from', 'till', 'taken', 'count', 'fishingMethod'));

        return response()->download($file);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate(Request $request)
    {
        $haul = new Haul();

        $form = $this->form(HaulForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::hauls.store'),
            'model'     => $haul,
            'data'      => [
                'manager'   => $this->getManager(),
                'user'      => $haul->user,
                'ticketId'  => $request->query->get('ticket'),
                'areaId'    => $request->query->get('area'),
                'userId'    => $request->query->get('user'),
            ],
        ]);

        return view('admin.hauls.edit', compact('haul', 'form'));
    }

    /**
     * @param $haulId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($haulId)
    {
        $haul = Haul::findOrFail($haulId);

        if (!$haul->area || (
                    $this->managerId != $haul->area->manager_id
                    && $this->managerId != $haul->area->manager->parent_id)
            ) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->form(HaulForm::class, [
            'method'    => 'PUT',
            'url'       => route('admin::hauls.update', ['haulId' => $haul->id]),
            'model'     => $haul,
            'data'      => [
                'manager'   => $this->getManager(),
                'user'      => $haul->user,
            ],
        ]);

        return view('admin.hauls.edit', compact('haul', 'form'));
    }

    /**
     * Get ticket hauls
     *
     * @param $ticketId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getHauls($ticketId)
    {
        $ticket = Ticket::findOrFail($ticketId);

        $hauls = [];

        $ticket->hauls->each(function ($haul) use ($hauls) {
            $hauls[] = view('admin.hauls.haul', compact('haul'))->render();
        });

        return response()->json(compact('hauls'));
    }

    /**
     * @param HaulRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postStore(HaulRequest $request)
    {
        $haul = new Haul();

        $form = $this->form(HaulForm::class, [
            'model' => $haul,
            'data'      => [
                'manager'   => $this->getManager(),
                'user'      => $haul->user,
            ],
        ]);

        if (($ticketId = $request->request->getInt('ticket'))
            && ($user = Ticket::findOrFail($ticketId)->user)
            && $user->id != $request->request->getInt('user')) {
            throw new ApplicationException('Selected ticket does not belongs to selected user');
        }

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $haul);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::hauls.edit', ['haulId' => $haul->id]);
    }

    /**
     * Set multicatch to ticket
     *
     * @param MultiHaulRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */

    /**
     * Set empty haul
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postEmptyHaul(Request $request)
    {
        $this->validate($request, [
            'catch_date' => 'required|date',
            'count' => 'integer',
            'ticket' => 'required|integer',
            'area' => 'integer'
        ]);

        $ticket = Ticket::findOrFail($request->ticket);

        $haul = Haul::create([
            'area_id' => $request->area,
            'catch_date' => Carbon::parse($request->catch_date)->format('Y-d-m'),
            'count' => $request->count,
            'ticket_id' => $ticket->id
        ]);

        $haul->ticket()->associate($ticket);
        $haul->area()->associate($ticket->type->area);

        return response()->json(['success' => true, 'haul' => $haul]);
    }

    public function postMultiStore(MultiHaulRequest $request)
    {
        $ticket = Ticket::findOrFail($request->ticket);

        $checkin = Checkin::create([
            'ticket_id' => $ticket->id,
            'from' => Carbon::now()->format('Y-m-d H:i:s'),
            'till' => Carbon::now()->addHours($request->duration)->format('Y-m-d H:i:s')
        ]);

        $fishingMethod = FishingMethod::find($request->fishing_method_id);
        if (!is_null($fishingMethod)) {
            $checkin->fishingMethods()->save($fishingMethod);
        }

        $haul = Haul::create([
            'catch_date'    => Carbon::parse($request->catch_date)->format('Y-m-d'),
            'catch_time'    => isset($request->catch_time)
                ? Carbon::parse($request->catch_time)->format('H:i')
                : null,
            'ticket_number' => (new TicketManager())->getIdentifier($ticket),
            'user_comment'  => !empty($request->user_comment) ? $request->user_comment : '',
            'public'        => false,
            'count'         => $request->count ?? 1,
            'technique_id'  => $request->technique_id,
            'taken'         => $request->taken,
            'size_value'    => $request->size_value ?? null,
            'size_type'     => isset($request->size_type) ? $request->size_type : null,
        ]);

        $haul->ticket()->associate($ticket);
        $haul->checkin()->associate($checkin);
        $haul->area()->associate($ticket->type->area);

        if ($ticket->user) {
            $haul->user()->associate($ticket->user);
        }
        if (isset($request->fish)) {
            $haul->fish()->associate($request->fish);
        }

        $haul->save();

        $haulHTML = view('admin.hauls.haul', compact('haul'))->render();

        return response()->json(['hauls' => [$haulHTML]]);
    }

    /**
     * @param $haulId
     * @param HaulRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate($haulId, HaulRequest $request)
    {
        $haul = Haul::findOrFail($haulId);

        $form = $this->form(HaulForm::class, [
            'model' => $haul,
            'data'      => [
                'manager'   => $this->getManager(),
                'user'      => $haul->user,
                'ticketId'  => null,
            ],
        ]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $haul);
        flash()->success('Daten erfolgreich gespeichert');
        $managerAreas = $this->getManager()->areas()->pluck('id')->toArray();

        return in_array($haul->area->id, $managerAreas)
            ? redirect()->route('admin::hauls.edit', ['haulId' => $haulId])
            : redirect()->route('admin::hauls');
    }

    /**
     * @param HaulRequest $request
     * @param Haul $haul
     */
    private function update(HaulRequest $request, Haul $haul)
    {
        // Standard values
        $haul->area_name = $request->request->get('area_name');
        $haul->ticket_number = $request->request->get('ticket_number', '');
        $haul->count = $request->request->get('count', 1);
        $haul->size_value = $request->request->get('size_value');
        $haul->size_type = $request->request->get('size_type') ?: null;
        $haul->catch_date = Carbon::parse($request->request->get('catch_date'))->format('Y-m-d');
        $haul->catch_time = $request->request->get('catch_time')
            ? Carbon::parse($request->request->get('catch_time'))->format('H:i')
            : null;
        $haul->weather = $request->request->get('weather') ?: null;
        $haul->temperature = $request->request->get('temperature') ?: null;
        $haul->user_comment = $request->request->get('user_comment', '');
        $haul->public = $request->request->getBoolean('public');
        $haul->taken = $request->request->getBoolean('taken');

        // User
        if (($request->method() === 'POST') && ($user = $request->request->getInt('user'))) {
            $haul->user()->associate($user);
        }
        $haul->save();

        // Area
        if ($area = $request->request->getInt('area')) {
            $haul->area()->associate($area);
        } else {
            $haul->area()->dissociate();
        }

        // Fish
        if ($fish = $request->request->getInt('fish')) {
            $haul->fish()->associate($fish);
        } else {
            $haul->fish()->dissociate();
        }

        // Technique
        if ($technique = $request->request->getInt('technique')) {
            $haul->technique()->associate($technique);
        } else {
            $haul->technique()->dissociate();
        }

        // Ticket
        if ($ticket = $request->request->getInt('ticket')) {
            $haul->ticket()->associate($ticket);
        } else {
            $haul->ticket()->dissociate();
        }

        // Picture
        if ($request->hasFile('picture')) {
            $picture = Picture::create(['file' => $request->file('picture')]);
            if ($haul->picture) {
                $haul->picture->delete();
            }
            $haul->picture()->associate($picture);
        }

        $haul->save();
    }

    /**
     * Delete Haul
     *
     * @param $haulId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function delete($haulId)
    {
        $haul = Haul::findOrFail($haulId);

        if (is_null($this->getManager()->children)) {
            throw new AccessDeniedHttpException;
        }

        $haul->delete();

        return response('Haul with id ' . $haul->id . ' has been deleted', 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function anyData(Request $request)
    {
        $data = $this->fetchIndexRequestData($request);
        $hauls = $this->getManagerHauls($data);

        return Datatables::of($hauls)
            ->orderColumn('catch_date', 'catch_date $1, catch_time $1')
            ->addColumn('fish_name', function (Haul $haul) {
                return $haul->fish ? $haul->fish->name : '<i>Leermeldung</i>';
            })
            ->addColumn('fish_size', function (Haul $haul) {
                return $haul->fish
                    ? $haul->size_value . ' ' . $haul->size_type
                    : '';
            })
            ->addColumn('count', function (Haul $haul) {
                return $haul->count ? $haul->count : 1;
            })
            ->addColumn('taken', function ($haul) {
                return $haul->taken ?
                    '<span class="fa fa-check fa-fw"></span>Ja' :
                    '<span class="fa fa-times fa-fw"></span>Nein';
            })
            ->addColumn('fishing_method', function (Haul $haul) {
                $fishingMethods = '';
                if ($haul->checkin && !empty($haul->checkin->fishingMethods)) {
                    foreach ($haul->checkin->fishingMethods as $fishingMethod) {
                        $fishingMethods .= $fishingMethod->translate(app()->getLocale(), true)->name . '<br />';
                    }
                }
                return $fishingMethods;
            })
            ->addColumn('user_name', function (Haul $haul) {
                return $haul->user
                    ? $haul->user->full_name
                    : ($haul->ticket && $haul->ticket->resellerTicket
                        ? $haul->ticket->resellerTicket->full_name
                        : '<i>Unbekannt</i>');
            })
            ->editColumn('ticket_number', function (Haul $haul) {
                return $haul->ticket_number ?:
                    ($haul->ticket ? (new TicketManager())->getIdentifier($haul->ticket) : null);
            })
            ->addColumn('area_name', function (Haul $haul) {
                return $haul->area ? $haul->area->name : $haul->area_name;
            })
            ->editColumn('catch_date', function (Haul $haul) {
                return $haul->catch_time
                    ? $haul->catch_date->format('d.m.Y') . ' ' . $haul->catch_time
                    : $haul->catch_date->format('d.m.Y');
            })
            ->addColumn('actions', function (Haul $haul) {
                $editRoute = route('admin::hauls.edit', ['haulId' => $haul->id]);
                $removeRoute = route('admin::hauls.delete', ['haulId' => $haul->id]);

                return '
                    <a class="btn btn-xs btn-primary"
                       href="'.$editRoute.'">
                        <i class="fa fa-edit fa-fw"></i>
                    </a>
                    <button type="button"
                            data-action="' . $removeRoute . '"
                            data-method="DELETE"
                            class="btn btn-xs btn-danger"
                            data-target="#delete-modal"
                            data-toggle="modal">
                        <i class="fa fa-trash-o fa-fw"></i>
                    </button>
                ';
            })
            ->make(true);
    }

    /**
     * @param Request $request
     * @return Collection
     */
    private function fetchIndexRequestData(Request $request)
    {
        return collect([
            'ticket'    => $request->query->getInt('ticket'),
            'area'      => $request->query->getInt('area'),
            'count'     => $request->query->getInt('count'),
            'taken'     => $request->query->getInt('taken', -1),
            'fish'      => $request->query->getInt('fish', -1),
            'technique' => $request->query->getInt('technique', -1),
            'fishingMethod' => $request->query->getInt('fishingMethod', -1),
            'from'      => $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d')),
            'till'      => $request->query->get('till', Carbon::now()->format('Y-m-d')),
        ]);
    }

    /**
     * @param Collection $data
     * @return Haul|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    private function getManagerHauls(Collection $data)
    {
        $hauls = Haul::with(['user', 'area'])
            ->select('hauls.*')
            ->leftJoin('areas', 'hauls.area_id', '=', 'areas.id')
            ->join('managers', 'areas.manager_id', '=', 'managers.id')
            ->where(function ($query) {
                $query
                    ->where('managers.id', '=', $this->managerId)
                    ->orWhere('managers.parent_id', '=', $this->managerId);
            });

        if ($data->has('ticket') && ($ticket = $data->get('ticket')) > 0) {
            $hauls->where(function ($query) use ($ticket) {
                $query
                    ->whereHas('ticket', function ($q) use ($ticket) {
                        $q->whereId($ticket);
                    })
                    ->orWhere('ticket_number', 'LIKE', '%-'.sprintf('%06d', $ticket));
            });
        }

        if ($data->has('area') && ($area = $data->get('area')) > 0) {
            $hauls->where('area_id', $area);
        }

        if ($data->has('fishingMethod') && ($fishingMethod = $data->get('fishingMethod')) > 0) {
            $hauls->where(function ($query) use ($fishingMethod) {
                $query->whereHas('ticket', function ($q) use ($fishingMethod) {
                        $q->whereHas('checkins', function ($q) use ($fishingMethod) {
                            $q->whereHas('fishingMethods', function ($q) use ($fishingMethod) {
                                $q->whereId($fishingMethod);
                            });
                        });
                    });
            });
        }

        if ($data->has('fish') && ($fish = $data->get('fish')) >= 0) {
            if ($fish > 0) {
                $hauls->where('fish_id', $fish);
            } else {
                $hauls->whereNull('fish_id');
            }
        }

        if ($data->has('taken') && ($taken = $data->get('taken')) >= 0) {
            if ($taken >= 0) {
                $hauls->where('taken', $taken);
            } else {
                $hauls->whereNotNull($taken);
            }
        }

        if ($data->has('from') && $data->has('till') && ($from = $data->get('from')) && ($till = $data->get('till'))) {
            try {
                $from = Carbon::createFromFormat('Y-m-d', $from)->startOfDay();
                $till = Carbon::createFromFormat('Y-m-d', $till)->endOfDay();

                $hauls->whereBetween('hauls.catch_date', [$from, $till]);
            } catch (\InvalidArgumentException $e) {
            }
        }

        return $hauls;
    }
}
