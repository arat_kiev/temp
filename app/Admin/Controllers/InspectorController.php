<?php

namespace App\Admin\Controllers;

use App\Api1\Controllers\AreaController as ApiAreaController;
use App\Models\Area\Area;
use App\Models\User;
use Illuminate\Http\Request;
use Datatables;
use Html;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class InspectorController extends AdminController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (!$this->getManager()) {
                throw new AccessDeniedHttpException();
            }

            return $next($request);
        });
    }

    public function getIndex()
    {
        return view('admin.inspector.index');
    }

    public function getInspectorAreas(Request $request, $userId, $type = 'public')
    {
        $user = User::findOrFail($userId);
        $public = ($type == 'public');

        $managerAreaIds = $this->getManager()->areas()->pluck('areas.id')->toArray();
        $inspectorAreaIds = $user->inspectedAreas()->where('is_public', $public)->pluck('areas.id')->toArray();

        $areaIds = array_filter($managerAreaIds, function ($managerId) use ($inspectorAreaIds) {
            return in_array($managerId, $inspectorAreaIds);
        });
        $request->query->set('area_id', $areaIds);
        $request->query->set('_perPage', count($areaIds));

        return with(new ApiAreaController(new Area()))->index($request);
    }

    public function postInspectorAreas(Request $request, $userId)
    {
        $user = User::findOrFail($userId);

        $managerAreaIds = $this->getManager()->areas()->pluck('areas.id')->toArray();
        $publicAreas = array_filter($request->request->get('public-areas', []), function($area) {
            return is_numeric($area);
        });
        $privateAreas = array_filter($request->request->get('private-areas', []), function ($area) use ($publicAreas) {
            return is_numeric($area) && !in_array($area, $publicAreas);
        });
        $publicData = array_fill_keys(array_unique($publicAreas), ['is_public' => true]);
        $privateData = array_fill_keys(array_unique($privateAreas), ['is_public' => false]);

        $user->inspectedAreas()->detach($managerAreaIds);
        $user->inspectedAreas()->sync($publicData, false);
        $user->inspectedAreas()->sync($privateData, false);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::inspector.index');
    }

    public function attachToManager($userId)
    {
        $user = User::findOrFail($userId);

        if (!$user->hasRole('ticket_inspector')) {
            $user->assignRole('ticket_inspector');
        }

        $this->getManager()->inspectors()->attach($user);
        
        return response()->json([
            'notification'  => 'success',
            'message'       => 'Kontrolleur wurde zugeweissen',
        ]);
    }

    public function detachFromManager($userId)
    {
        $user = User::findOrFail($userId);
        $this->getManager()->inspectors()->detach($userId);

        if (!$user->inspectedAreas()->count() && !$user->managerInspectors()->count()) {
            $user->removeRole('ticket_inspector');
        }

        return response()->json([
            'notification'  => 'success',
            'message'       => 'Kontrolleur wurde entfernt',
        ]);
    }

    public function anyData()
    {
        $users = User::select('users.*')
            ->leftJoin('area_inspectors', 'users.id', '=', 'area_inspectors.user_id')
            ->join('manager_inspectors', function ($join) {
                $join->on('users.id', '=', 'manager_inspectors.user_id')
                    ->where('manager_inspectors.manager_id', '=', $this->getManager()->id);
            })
            ->join('user_roles', 'user_roles.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'user_roles.role_id')
            ->where('roles.name', '=', 'ticket_inspector')
            ->orWhereNotNull('area_inspectors.area_id')
            ->groupBy('users.id')
            ->get();

        return Datatables::of($users)
            ->editColumn('last_activity_at', function (User $user) {
                return $user->last_activity_at ? $user->last_activity_at->format("d.m.Y - H:i") : 'Nie';
            })
            ->addColumn('areas', function (User $user) {
                $editRoute = route('admin::inspector.areas.store', ['userId' => $user->id]);
                $publicRoute = route('admin::inspector.areas.index', ['userId' => $user->id, 'type' => 'public']);
                $privateRoute = route('admin::inspector.areas.index', ['userId' => $user->id, 'type' => 'private']);
                return '
                    <button type="button" class="btn btn-xs btn-primary"
                        data-action="' . $editRoute . '"
                        data-public-action="' . $publicRoute . '"
                        data-private-action="' . $privateRoute . '"
                        data-target="#area-modal" data-toggle="modal">
                        <span class="fa fa-map-marker fa-fw"></span>
                        Gewässer
                    </button>
                ';
            })
            ->addColumn('actions', function (User $user) {
                $removeRoute = route('admin::inspector.manager.detach', ['userId' => $user->id]);
                $inspectionRoute = route('admin::inspection.index', ['inspectorId' => $user->id]);

                return '
                    <a  href="'.$inspectionRoute.'"
                        type="button"
                        class="btn btn-xs btn-primary">
                        <i class="fa fa-eye fa-fw"></i>
                        Kontrollen
                    </a>
                    <button type="button"
                            class="btn btn-xs btn-danger"
                            data-target="#delete-modal"
                            data-toggle="modal"
                            data-action="' . $removeRoute . '">
                        <span class="fa fa-trash-o fa-fw"></span>
                        Entfernen
                    </button>
                ';
            })
            ->make(true);
    }
}
