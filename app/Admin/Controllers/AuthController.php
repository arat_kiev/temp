<?php

namespace App\Admin\Controllers;

use App\Events\PasswordReset as PasswordResetEvent;
use App\Http\Controllers\Controller;
use App\Managers\UserManager;
use App\Models\Client;
use App\Models\PasswordReset;
use App\Models\User;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Session;
use Event;
use View;
use Route;

class AuthController extends Controller
{
    use AuthenticatesUsers,
        ResetsPasswords;

    protected $loginView = 'admin.auth.login';
    protected $linkRequestView = 'admin.auth.reset';

    protected $loginPath = 'login';

    protected $redirectPath = '/';

    public function doLogout()
    {
        Session::forget('manager');
        Session::forget('inspector');

        return $this->getLogout();
    }

    public function getLogin()
    {
        if (Auth::user()) {
            return redirect()->to($this->redirectPath);
        }
        View::share('portal', Route::input('pos', 'admin'));

        return $this->getParentLogin();
    }

    public function getPasswordReset()
    {
        if (Auth::user()) {
            return redirect()->to($this->redirectPath);
        }
        View::share('portal', Route::input('pos', 'admin'));

        return $this->getEmail();
    }

    public function postPasswordReset(Request $request, UserManager $userManager)
    {
        $this->validateResetEmail($request);

        $email = trim($request->request->get('email'));
        $user = User::where('email', '=', $email)->firstOrFail();
        $client = Client::where('name', 'bissanzeiger')->firstOrFail();

        if (!$userManager->prePasswordResetCheck($user)) {
            flash()->error('Zu oft zurückgesetzt');
            return redirect()->back()->withInput();
        }

        $userManager->resetPassword($user, $client);
        flash()->success('Eine E-Mail mit Link zum Zurücksetzen wurde gesendet');

        return redirect()->back();
    }

    public function showLoginForm()
    {
        return view($this->loginView);
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view($this->linkRequestView)->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    private function validateResetEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email|exists:users,email']);
    }
}
