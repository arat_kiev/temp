<?php

namespace App\Admin\Controllers\Super\Reports;

use Html;
use Datatables;
use App\Models\Comment;
use App\Admin\Requests\Reports\CommentRequest;
use App\Admin\Controllers\Super\SuperAdminController;

class CommentController extends SuperAdminController
{
    private $commentTypes = [
        'areas'                 => 'Gewässer',
        'hauls'                 => 'Fänge',
        'techniques'            => 'Techniken',
        'fishes'                => 'Fische',
        'users'                 => 'Benutzer',
        'points_of_interest'    => 'Points Of Interest',
    ];

    public function getIndex(CommentRequest $request)
    {
        $status = $request->query->get('status', 'review');

        $statusesList = [
            'all'       => 'All',
            'review'    => 'Review',
            'accepted'  => 'Accepted',
            'rejected'  => 'Rejected',
        ];

        return view('admin.super.reports.comments.index', compact('statusesList', 'status'));
    }

    public function getEdit($commentId)
    {
        $comment = Comment::with(['user'])->findOrFail($commentId);
        $type = isset($this->commentTypes[$comment->commentable_type])
            ? $this->commentTypes[$comment->commentable_type]
            : null;

        return view('admin.super.reports.comments.edit', compact('comment', 'type'));
    }

    public function postUpdate(CommentRequest $request, $commentId)
    {
        $comment = Comment::findOrFail($commentId);
        $comment->update(['status' => $request->get('action')]);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:reports.comments.index');
    }

    public function anyData(CommentRequest $request)
    {
        $status = $request->get('status', 'review');

        $comments = Comment::with(['user']);

        if ($status !== 'all') {
            $comments->where('status', '=', $status);
        }

        return Datatables::of($comments)
            ->editColumn('id', function ($comment) {
                return Html::linkRoute('admin::super:reports.comments.edit', $comment->id, $comment->id);
            })
            ->editColumn('user.full_name', function ($comment) {
                return $comment->user ? $comment->user->full_name : '';
            })

            ->editColumn('body', function ($comment) {
                return str_limit($comment->body);
            })
            ->addColumn('type', function ($comment) {
                return isset($this->commentTypes[$comment->commentable_type])
                    ? $this->commentTypes[$comment->commentable_type]
                    : null;
            })
            ->addColumn('actions', function($comment) {
                $editRoute = route('admin::super:reports.comments.edit', ['commentId' => $comment->id]);
                $acceptBlock = in_array($comment->status, ['review', 'rejected'])
                    ? '<button value="accepted" class="btn btn-xs btn-success action"
                               data-action="'. route('admin::super:reports.comments.update', ['commentId' => $comment->id]) .'">
                            <span class="fa fa-check fa-fw"></span> Alles in Ordnung
                       </button> '
                    : '';
                $rejectBlock = in_array($comment->status, ['review', 'accepted'])
                    ? '<button value="rejected" class="btn btn-xs btn-danger action"
                               data-action="' . route('admin::super:reports.comments.update', ['commentId' => $comment->id]) .'">
                            <span class="fa fa-times fa-fw"></span> Kommentar blockieren
                       </button> '
                    : '';

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        Ansehen
                    </a> '.
                    $acceptBlock.
                    $rejectBlock;
            })
            ->make(true);
    }
}
