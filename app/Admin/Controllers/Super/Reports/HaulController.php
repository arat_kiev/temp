<?php

namespace App\Admin\Controllers\Super\Reports;

use Html;
use Datatables;
use App\Models\Haul;
use App\Admin\Requests\Reports\HaulRequest;
use App\Admin\Controllers\Super\SuperAdminController;

class HaulController extends SuperAdminController
{
    public function getIndex(HaulRequest $request)
    {
        $status = $request->query->get('status', 'review');

        $statusesList = [
            'all'       => 'All',
            'review'    => 'Review',
            'accepted'  => 'Accepted',
            'rejected'  => 'Rejected',
        ];

        return view('admin.super.reports.hauls.index', compact('statusesList', 'status'));
    }

    public function getEdit($haulId)
    {
        $haul = Haul::with(['user', 'area', 'fish', 'technique', 'picture'])
            ->findOrFail($haulId);

        return view('admin.super.reports.hauls.edit', compact('haul'));
    }

    public function postUpdate(HaulRequest $request, $haulId)
    {
        $haul = Haul::findOrFail($haulId);
        $haul->update(['status' => $request->get('action')]);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:reports.hauls.index');
    }

    public function anyData(HaulRequest $request)
    {
        $status = $request->get('status', 'review');

        $hauls = Haul::with(['user', 'area'])
            ->select('hauls.*')
            ->leftJoin('areas', 'hauls.area_id', '=', 'areas.id')
            ->leftJoin('users', 'hauls.user_id', '=', 'users.id');

        if ($status !== 'all') {
            $hauls->where('status', '=', $status);
        }

        return Datatables::of($hauls)
            ->editColumn('id', function ($haul) {
                return Html::linkRoute('admin::super:reports.hauls.edit', $haul->id, $haul->id);
            })
            ->editColumn('fish.name', function ($haul) {
                return $haul->fish ? $haul->fish->name : '';
            })
            ->editColumn('user.full_name', function ($haul) {
                return $haul->user ? $haul->user->full_name : '';
            })
            ->editColumn('area.name', function ($haul) {
                return $haul->area ? $haul->area->name : '';
            })
            ->editColumn('catch_date', function ($haul) {
                $date = new \DateTime($haul->catch_date);
                return $date->format('d.m.Y') . ' ' . $haul->catch_time;
            })
            ->addColumn('picture', function ($haul) {
                if ($haul->picture) {
                    if (ends_with($haul->picture->file, '.pdf')) {
                        return '<a href = "'. route('filestream', ['file' => $haul->picture->name]) .'" 
                                    target = "_blank"> Anhang (PDF)</a>';
                    } else {
                        $pictureLink = route('imagecache', ['template' => 'org', 'filename' => $haul->picture->file]);
                        $previewLink = route('imagecache', ['template' => 'lre', 'filename' => $haul->picture->file]);
                        return '
                            <a href = "'. $pictureLink .'"
                               data - toggle = "lightbox"
                               data - title = "Original" >
                                <img src = "'. $previewLink .'"
                                     alt = "'. $haul->picture->name .'"
                                     width = "200"
                                     class="img-responsive img-thumbnail" />
                            </a>
                        ';
                    }
                } else {
                    return 'Es gibt kein Bild';
                }
            })
            ->addColumn('actions', function($haul) {
                $editRoute = route('admin::super:reports.hauls.edit', ['haulId' => $haul->id]);

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        Ansehen
                    </a>
                ';
            })
            ->make(true);
    }
}
