<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\UserForm;
use App\Admin\Requests\StoreUserRequest;
use App\Api1\Controllers\RoleController as ApiRoleController;
use App\Api1\Controllers\UserController as ApiUserController;
use App\Managers\UserManager;
use App\Models\Authorization\Role;
use App\Models\Client;
use App\Models\Location\Country;
use App\Models\License\LicenseType;
use App\Models\PasswordReset;
use App\Models\Ticket\Ticket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\User;
use Datatables;
use Html;
use File;
use App;
use DB;

class UserController extends SuperAdminController
{
    use FormBuilderTrait;

    public function getIndex()
    {
        return view('admin.super.user.index');
    }

    public function getEdit($userId)
    {
        $user = User::where('id', $userId)->with(['tickets' => function($query)
            {
                $query->select('id', 'user_id');
            }])
        ->firstOrFail();
        $userTicketsCount = $user->tickets->count();

        $form = $this->form(UserForm::class, [
            'method' => 'POST',
            'url' => route('admin::super:users.update', ['userId' => $user->id]),
            'model' => $user,
        ]);

        return view('admin.super.user.edit', compact('form', 'user', 'userTicketsCount'));
    }

    public function postUpdate(StoreUserRequest $request, $userId)
    {
        $user = User::findOrFail($userId);

        $form = $this->form(UserForm::class, ['model' => $user]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $user);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:users.edit', ['userId' => $user->id]);
    }

    private function update(StoreUserRequest $request, User $user)
    {
        $addressBefore = [
            'post_code' => $user->post_code,
            'country_id' => $user->country_id,
        ];

        $user->fill($request->except(['birthday', 'balance']));

        $balance = $request->request->get('balance');
        $balance = str_replace(',', '.', $balance);
        $balance = ((float)$balance) * 100;

        $user->balance = $balance;
        $user->active = $request->request->getBoolean('active', false);
        $user->birthday = $request->request->get('birthday', null)
            ? Carbon::createFromFormat('d.m.Y', $request->request->get('birthday'))
            : null;

        if ($country = Country::find($request->request->get('country', 0))) {
            $user->country()->associate($country);
        } else {
            $user->country()->dissociate();
        }

        $user->save();

        $addressAfter = [
            'post_code' => $user->post_code,
            'country_id' => $user->country_id,
        ];

        if (array_diff_assoc($addressBefore, $addressAfter)) {
            $state = (new UserManager())->fetchState($user);
            $user->state()->associate($state);
            $user->save();
        }
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->delete();

        flash()->success('User erfolgreich gelöscht');

        return response()->json([
            'message'   => 'User wurde gelöscht.',
        ]);
    }

    public function getAllUsers(Request $request)
    {
        return with(new ApiUserController(new User()))->index($request);
    }

    public function getAllRoles(Request $request)
    {
        return with(new ApiRoleController(new Role()))->index($request);
    }

    public function getRoles(Request $request, $userId)
    {
        $user = User::findOrFail($userId);

        $roleIds = $user->roles()->pluck('roles.id')->toArray();
        $request->query->set('role_id', $roleIds);
        $request->query->set('_perPage', count($roleIds));

        return with(new ApiRoleController(new Role()))->index($request);
    }

    public function postRoles(Request $request, $userId)
    {
        $user = User::findOrFail($userId);

        $roles = $request->request->get('roles', []);
        $user->roles()->sync($roles);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::super:users.index');
    }

    public function postPasswordReset($userId, UserManager $userManager)
    {
        $user = User::findOrFail($userId);
        $client = Client::where('name', 'bissanzeiger')->firstOrFail();
        $userManager->resetPassword($user, $client);

        return response()->json([
            'message'   => 'E-Mail zum Passwortzurücksetzen wurde gesendet.',
            'attempts'  => ($attempts = PasswordReset::where('emails', '=', $user->email)->count()),
            'attemptsExhausted' => ($attempts >= PasswordReset::MAX_ALLOWED_ATTEMPTS),
        ]);
    }

    /**
     * Users stats and licenses types list
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function stats(Request $request)
    {
        $license = LicenseType::select(['id', 'id AS text'])->get()->toArray();

        $stats = User::selectRaw('
        COUNT(*) AS totalUsers,
        (SELECT COUNT(DISTINCT `user_id`) FROM `licenses` INNER JOIN `users` ON licenses.user_id = users.id) AS totalLicenses,
        TRIM(SUM(balance)/100)+0 AS totalBalance,
        SUM(CASE WHEN balance > 0 THEN 1 ELSE 0 END) AS usersWithBalance')
            ->where('test', '=', '0')
            ->get()
            ->toArray();

        $stats = current($stats);
        $stats['usersPerPeriod'] = $this->statsUsers($request);
        $stats['ticketsPerPeriod'] = $this->statsTickets($request);

        return response()->json(['license' => $license, 'stats' => $stats]);
    }

    /**
     * Count new users per period (active or all)
     * @param Request $request
     * @return int
     */
    public function statsUsers(Request $request)
    {
        if ($request->has('upp') && $request->has('us')) {
            $users = User::where('created_at', '>', DB::raw("DATE_SUB(NOW(), INTERVAL {$request->get('upp')})"));
            $request->get('us') != 'active' ?: $users->where('active', 1);
            return $users->count();
        }
        return 0;
    }

    /**
     * Count users with tickets bought in last X days
     * @param Request $request
     * @return int
     */
    public function statsTickets(Request $request)
    {
        if ($request->has('tpp') && $request->has('tv') && $request->get('tv') > 0) {
            $tickets = Ticket::select(DB::raw('COUNT(DISTINCT user_id) AS count'))->where('created_at', '>', DB::raw("DATE_SUB(NOW(), INTERVAL {$request->get('tv')} {$request->get('tpp')})"))->first();
            return $tickets->count;
        }
        return 0;
    }

    /**
     * Generate users export data
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createExport(Request $request)
    {
        $form = (object) $request->all();
        $clause = [];

        $form->active == '' ?: $clause[] = ['active', '=', $form->active];
        empty($form->email) ?: $clause[] = ['email', 'like', "%$form->email%"];
        empty($form->balance) ?: $clause[] = ['balance', $form->compareBalance, $form->balance * 100];
        empty($form->birthday) ?: $clause[] = ['birthday', $form->compareBirthday, Carbon::parse($form->birthday)->toDateString()];
        empty($form->activity) ?: $clause[] = ['last_activity_at', $form->compareActivity, Carbon::parse($form->activity)->toDateTimeString()];


        $users = User::select('users.id', 'users.name AS username', 'first_name', 'last_name', 'street', 'post_code', 'city', 'c.name AS country_name', 'phone', 'birthday', DB::raw('TRIM(balance/100)+0 AS balance'), DB::raw('(SELECT COUNT(*) FROM `tickets` WHERE `user_id` = users.id) as tickets'), DB::raw('IF(active = 1, "1", "0") AS active'), 'users.created_at', 'users.updated_at', 'users.last_activity_at')
            ->leftJoin('country_translations AS c', function ($q) {
                $q->on('users.country_id', '=', 'c.country_id')
                    ->where('c.locale', '=', App::getLocale());
            })
            // applying form conditions
            ->where($clause);
        // License (condition based on license type)
        empty($form->license)
            ?: $users->join('licenses', function ($q) use ($form) {
            $q->on('users.id', '=', 'licenses.user_id')
                ->whereIn('licenses.license_type_id', explode(',', $form->license));
        });
        // Ticket (condition based on tickets bought period: from - to)
        if($form->ticket_from || $form->ticket_till) {
            $users->join('tickets', function ($q) use ($form) {
                $q->on('users.id', '=', 'tickets.user_id');
                empty($form->ticket_from) ?: $q->where('tickets.created_at', '>', Carbon::parse($form->ticket_from)->toDateTimeString());
                empty($form->ticket_till) ?: $q->where('tickets.created_at', '<', Carbon::parse($form->ticket_till)->toDateTimeString());
            });
        }

        $users = $users->groupBy('users.id')
            ->get();

        if ($users->count()) {
            $filename = Carbon::now()->timestamp .'_'. 'users';
            $excel =  Excel::create($filename, function($excel) use($users) {
                $excel->sheet(Carbon::now()->toDateString(), function($sheet) use($users) {
                    $sheet->fromArray($users);
                });
            })->store('xls', false, true);

            return response()->json(['file' => $excel['file']]);
        }
        return response()->json();
    }

    /**
     * Return generated file and remove it
     * @param $filename
     */
    public function getExport($filename)
    {
        $file = 'storage/exports/'.$filename;

        $excel = Excel::load($file);
        File::delete(base_path($file));

        $excel->download();
    }

    public function anyData()
    {
        $users = User::with(['tickets' => function($query)
            {
                $query->select('id', 'user_id');
            }]);

        return Datatables::of($users)
            ->editColumn('name', function ($row) {
                return Html::linkRoute('admin::super:users.edit', $row->name, ['userId' => $row->id]);
            })
            ->editColumn('last_activity_at', function ($user) {
                return $user->last_activity_at ? $user->last_activity_at->format("d.m.Y - H:i") : 'Nie';
            })
            ->addColumn('roles', function ($user) {
                return '
                    <button type="button" class="btn btn-xs btn-primary"
                        data-action="' . route('admin::super:users.roles.index', ['userId' => $user->id]) . '"
                        data-target="#role-modal" data-toggle="modal">
                        <span class="fa fa-key fa-fw"></span>
                        Rollen
                    </button>
                ';
            })
            ->addColumn('actions', function ($user) {
                return '
                    <button type="button"
                            class="btn btn-xs btn-info"
                            data-action="' . route('admin::super:users.password.reset', ['userId' => $user->id]) . '"
                            data-method="POST"
                            data-target="#password-modal"
                            data-toggle="modal">
                        Password
                        <span class="fa fa-refresh fa-fw"></span>
                    </button>
                    <button type="button"
                            class="btn btn-xs btn-danger"
                            data-action="' . route('admin::super:users.destroy', ['userId' => $user->id]) . '"
                            data-method="DELETE"
                            data-target="#delete-modal"
                            data-toggle="modal">
                        Löschen
                        <span class="fa fa-refresh fa-fw"></span>
                    </button>
                ';
            })
            ->addColumn('history', function ($user) {
                $ticketsCount = $user->tickets->count();

                $buttonIcon = $ticketsCount ? $ticketsCount : '<span class="fa fa-ticket fa-fw"></span>';
                $buttonState = $ticketsCount ? '' : 'disabled="disabled"';

                return '
                    <button type="button" class="btn btn-xs btn-primary"
                        data-action="' . route('admin::super:users.tickets.history', ['userId' => $user->id]) . '"
                        data-target="#ticket-history-modal" data-toggle="modal" ' . $buttonState . '>
                        ' . $buttonIcon . ' 
                        Angelkartenkäufe
                    </button>
                ';
            })
            ->make(true);
    }

    /**
     * Get user's bought tickets
     * @param $user_id
     */
    public function getUserTicketHistory($user_id)
    {
        $tickets = Ticket::where('user_id', $user_id)->select('id', 'created_at', 'ticket_type_id', 'ticket_price_id')
            ->with([
                'price' => function($query)
                    {
                        $query->select('id', 'value');
                    },
                'type' => function($query)
                    {
                        $query->select('id', 'area_id');
                    },
                'type.area' => function($query)
                    {
                        $query->select('id',  'name');
                    }
        ]);

        return Datatables::of($tickets)
            ->editColumn('id', function ($tickets) {
                return Html::linkRoute('admin::super:tickets.edit', str_limit($tickets->id, 100), ['ticketId' => $tickets->id], ['target' => '_blank']);
            })
            ->editColumn('date', function ($tickets) {
                return $tickets->created_at->format("d.m.Y");
            })
            ->editColumn('type', function ($tickets) {
                return $tickets->type->name;
            })
            ->editColumn('area', function ($tickets) {
                return Html::linkRoute('admin::super:areas.edit', str_limit($tickets->type->area->name, 100), ['areaId' => $tickets->type->area->id], ['target' => '_blank']);
            })
            ->editColumn('price', function ($tickets) {
                return number_format($tickets->price->value / 100.0, 2, ",", ".").' €';
            })
            ->make(true);
    }

}
