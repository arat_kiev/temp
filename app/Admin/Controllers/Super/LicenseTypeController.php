<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\LicenseTypeForm;
use App\Admin\Requests\LicenseTypeRequest;
use App\Api1\Controllers\StateController as ApiStateController;
use App\Models\License\LicenseType;
use App\Models\Location\State;
use Illuminate\Http\Request;
use Datatables;
use Html;
use DB;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class LicenseTypeController extends SuperAdminController
{
    use FormBuilderTrait;
    
    public function getIndex(Request $request)
    {
        $state = $request->query->getInt('stateId', -1);

        $stateList = collect([
            '-1'    => '-- Alle --',
            '0'     => '-- Ohne Bundesländer --',
        ]);

        State::with('country')->get()->each(function ($state) use ($stateList) {
            $stateList[$state->id] = $state->name . ' (' . $state->country->name . ')';
        });

        return view(
            'admin.super.licensetype.index',
            compact('state', 'stateList', 'area', 'areaList')
        );
    }

    public function getCreate()
    {
        $lType = new LicenseType();

        $form = $this->form(LicenseTypeForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:licensetypes.store', ['typeId' => $lType->id]),
            'model'     => $lType,
        ]);

        return view('admin.super.licensetype.edit', compact('lType', 'form'));
    }

    public function postStore(LicenseTypeRequest $request)
    {
        $lType = new LicenseType();

        $form = $this->form(LicenseTypeForm::class, ['model' => $lType]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $lType);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:licensetypes.edit', ['typeId' => $lType->id]);
    }

    public function getEdit(int $typeId)
    {
        $lType = LicenseType::findOrFail($typeId);

        $form = $this->form(LicenseTypeForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:licensetypes.update', ['typeId' => $lType->id]),
            'model'     => $lType,
        ]);

        return view('admin.super.licensetype.edit', compact('lType', 'form'));
    }

    public function postUpdate(int $typeId, LicenseTypeRequest $request)
    {
        $lType = LicenseType::findOrFail($typeId);

        $form = $this->form(LicenseTypeForm::class, ['model' => $lType]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $lType);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:licensetypes.edit', ['typeId' => $lType->id]);
    }
    
    private function update(LicenseTypeRequest $request, LicenseType $licenseType)
    {
        DB::transaction(function () use ($request, $licenseType) {
            $licenseType->name = $request->request->get('name');

            if (!$licenseType->licenses()->count()) {
                $fields = [];
                foreach ($request->request->get('fields', []) as $field) {
                    $fields[$field['field_name']] = $field['field_type'];
                }
                $licenseType->fields = $fields;
                $licenseType->attachments = $request->request->get('attachments', []);
            }

            $licenseType->save();
        });
    }

    public function getStates(Request $request, $lTypeId)
    {
        $licenseType = LicenseType::findOrFail($lTypeId);

        $stateIds = $licenseType->states()->pluck('states.id')->toArray();
        $request->query->set('ids', $stateIds);
        $request->query->set('_perPage', count($stateIds));

        return with(new ApiStateController(new State()))->index($request);
    }

    public function postStates(Request $request, $lTypeId)
    {
        $licenseType = LicenseType::findOrFail($lTypeId);

        $states = $request->request->get('states', []);
        $licenseType->states()->sync($states);

        flash()->success('Bundesländer erfolgreich gespeichert');
        return redirect()->route('admin::super:licensetypes.index');
    }

    public function anyData(Request $request)
    {
        $state = $request->query->getInt('stateId', -1);
        $licenseTypes = $this->fetchLicenseTypes($state);

        return Datatables::of($licenseTypes)
            ->editColumn('name', function (LicenseType $lType) {
                return Html::linkRoute(
                    'admin::super:licensetypes.edit',
                    $lType->name,
                    ['typeId' => $lType->id]
                );
            })
            ->addColumn('states', function (LicenseType $lType) {
                $route = route('admin::super:licensetypes.states.index', ['typeId' => $lType->id]);

                return '
                    <button type="button"
                            class="btn btn-xs btn-primary"
                            data-action="' . $route . '"
                            data-target="#state-modal" data-toggle="modal">
                        <span class="fa fa-compass fa-fw"></span>
                        Bundesländer
                        <span class="badge">' . $lType->states()->count() . '</span>
                    </button>
                ';
            })
            ->editColumn('updated_at', function (LicenseType $lType) {
                return $lType->updated_at ? $lType->updated_at->diffForHumans() : '<i>Nie</i>';
            })
            ->make(true);
    }

    private function fetchLicenseTypes(int $stateId = -1)
    {
        $licenseTypes = (new LicenseType())->newQuery();

        if ($stateId == 0) {
            $licenseTypes->doesntHave('states');
        } elseif ($stateId > 0) {
            $licenseTypes->whereHas('states', function ($states) use ($stateId) {
                $states->whereId($stateId);
            });
        }

        return $licenseTypes;
    }
}
