<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\ProductStockForm;
use App\Admin\Requests\ProductStockRequest;
use App\Api1\Controllers\UserController as ApiUserController;
use App\Models\Location\Country;
use App\Models\Product\ProductStock;
use App\Models\Stock;
use App\Models\User;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Datatables;
use Html;
use DB;

class ProductStockController extends SuperAdminController
{
    use FormBuilderTrait;

    public function getIndex(Request $request)
    {
        return view('admin.super.productstock.index', $this->indexData($request));
    }

    public function getCreate(Request $request)
    {
        $stock = new ProductStock();

        $form = $this->form(ProductStockForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:product_stocks.store', ['stockId' => $stock->id]),
            'model'     => $stock,
            'data'      => [
                'productId' => $request->query->get('product'),
                'stockId'   => $request->query->get('stock'),
            ],
        ]);

        return view('admin.super.productstock.edit', compact('stock', 'form'));
    }

    public function getEdit(Request $request, $stockId)
    {
        $stock = ProductStock::findOrFail($stockId);

        $form = $this->form(ProductStockForm::class, [
            'method'    => 'PUT',
            'url'       => route('admin::super:product_stocks.update', ['stockId' => $stock->id]),
            'model'     => $stock,
            'data'      => [
                'productId' => $request->query->get('product'),
                'stockId'   => $request->query->get('stock'),
            ],
        ]);

        return view('admin.super.productstock.edit', compact('stock', 'form'));
    }

    public function postCreate(ProductStockRequest $request)
    {
        $stock = new ProductStock();

        $form = $this->form(ProductStockForm::class, ['model' => $stock]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $stock);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:product_stocks.edit', ['stockId' => $stock->id]);
    }

    public function postUpdate(ProductStockRequest $request, $stockId)
    {
        $stock = ProductStock::findOrFail($stockId);

        $form = $this->form(ProductStockForm::class, ['model' => $stock]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $stock);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:product_stocks.edit', ['stockId' => $stockId]);
    }

    private function update(ProductStockRequest $request, ProductStock $productStock)
    {
        DB::transaction(function () use ($request, $productStock) {
            // Product
            if (!$productStock->id && $request->request->has('product')) {
                $productStock->product()->associate($request->request->getInt('product'));
            }

            // Stock
            $productStock->stock()->associate($request->request->getInt('stock'));

            // General data
            $productStock->fill([
                'quantity'              => $request->request->getInt('quantity'),
                'unit'                  => $request->request->get('unit', ProductStock::DEFAULT_UNIT),
                'delivery_time'         => $request->request->getInt('delivery_time'),
                'notification_quantity' => $request->request->getInt('notification_quantity'),
            ]);

            $productStock->save();
        });
    }

    public function delete($stockId)
    {
        $stock = ProductStock::findOrFail($stockId);

        $stock->delete();
        flash()->success('Lagermenge wurde gelöscht');
    }

    public function anyData(Request $request)
    {
        $stock = $request->query->getInt('stock', -1);
        $product = $request->query->getInt('product', -1);

        $productStocks = $this->getProductStocks($stock, $product);

        return Datatables::of($productStocks)
            ->orderColumn('quantity', 'quantity $1')
            ->editColumn('product', function (ProductStock $productStock) {
                return Html::linkRoute(
                    'admin::super:products.edit',
                    str_limit($productStock->product->name, 50),
                    ['productId' => $productStock->product->id]
                );
            })
            ->editColumn('stock', function (ProductStock $productStock) {
                return Html::linkRoute(
                    'admin::super:stocks.edit',
                    str_limit($productStock->stock->name, 50) . ', ' . $productStock->stock->country->name,
                    ['stockId' => $productStock->stock->id]
                );
            })
            ->editColumn('quantity', function (ProductStock $productStock) {
                $rule = $productStock->quantity <= $productStock->notification_quantity
                    ? 'text-danger'
                    : 'text-success';

                return "<span class='$rule'>$productStock->quantity $productStock->human_readable_unit</span>";
            })
            ->addColumn('notify_users', function (ProductStock $productStock) {
                return '
                    <button type="button"
                            class="btn btn-xs btn-primary"
                            data-action="'
                            . route('admin::super:product_stocks.notifiable.store', ['stockId' => $productStock->id])
                            . '"
                            data-target="#users-modal"
                            data-toggle="modal">
                        <span class="fa fa-users fa-fw"></span> Benutzer benachrichtigen
                        <span class="badge">'.$productStock->notificatableUsers()->count().'</span>
                    </button>
                ';
            })
            ->addColumn('actions', function (ProductStock $productStock) {
                $editRoute = route('admin::super:product_stocks.edit', ['stockId' => $productStock->id]);
                $removeRoute = route('admin::super:product_stocks.delete', ['stockId' => $productStock->id]);

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        <i class="fa fa-edit fa-fw"></i>
                        Bearbeiten
                    </a>
                    <button type="button"
                            data-action="' . $removeRoute . '"
                            data-method="DELETE"
                            class="btn btn-xs btn-danger"
                            data-target="#delete-modal"
                            data-toggle="modal">
                        <span class="fa fa-trash-o fa-fw"></span>
                        Löschen
                    </button>
                ';
            })
            ->blacklist(['actions'])
            ->make(true);
    }

    public function getNotifiables(Request $request, $stockId)
    {
        $productStock = ProductStock::findOrFail($stockId);

        $notifiableIds = $productStock->notificatableUsers()->pluck('users.id')->toArray();
        $request->query->set('user_id', $notifiableIds);
        $request->query->set('_perPage', count($notifiableIds));

        return with(new ApiUserController(new User()))->index($request);
    }

    public function postNotifiables(Request $request, $stockId)
    {
        $productStock = ProductStock::findOrFail($stockId);

        $users = $request->request->get('users', []);
        $productStock->notificatableUsers()->sync($users);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:product_stocks.index');
    }

    private function indexData(Request $request) : array
    {
        $stock = $request->query->getInt('stock', -1);
        $product = $request->query->getInt('product', -1);

        $productStocks = $this->getProductStocks($stock, $product);
        $count = $productStocks->count();

        $stockList = collect([
            '-1'    => 'Alle',
        ]);

        $stocks = Stock::select(['id', 'name'])
            ->has('products')
            ->each(function ($stock) use ($stockList) {
                $stockList[$stock->id] = $stock->name;
            });

        $stockList->merge($stocks);

        return compact('count', 'stock', 'stockList');
    }

    private function getProductStocks(int $stockId = -1, int $productId = -1)
    {
        $query = (new ProductStock())->newQuery();

        if ($stockId > 0) {
            $query->whereHas('stock', function ($q) use ($stockId) {
                $q->whereId($stockId);
            });
        }

        if ($productId > 0) {
            $query->whereHas('product', function ($q) use ($productId) {
                $q->whereId($productId);
            });
        }

        return $query;
    }
}
