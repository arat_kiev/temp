<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\HaulForm;
use App\Admin\Requests\HaulRequest;
use App\Managers\Export\HaulExportManager;
use App\Models\Haul;
use App\Models\Picture;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Datatables;

class HaulsController extends SuperAdminController
{
    use FormBuilderTrait;

    public function getIndex(Request $request)
    {
        $ticket = $request->query->getInt('ticket', null);

        return view('admin.super.hauls.index', compact('ticket'));
    }

    public function getExport($format = 'xls', Request $request, HaulExportManager $exportManager)
    {
        $hauls = $this->getHauls($request->query->getInt('ticket'));
        $file = $exportManager->run($hauls, $format, []);

        return response()->download($file);
    }

    public function getCreate(Request $request)
    {
        $haul = new Haul();

        $form = $this->form(HaulForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:hauls.store'),
            'model'     => $haul,
            'data'      => [
                'manager'   => null,
                'user'      => $haul->user,
                'ticketId'  => $request->query->get('ticket'),
                'areaId'    => $request->query->get('area'),
                'userId'    => $request->query->get('user'),
            ],
        ]);

        return view('admin.super.hauls.edit', compact('haul', 'form'));
    }

    public function getEdit($haulId)
    {
        $haul = Haul::findOrFail($haulId);

        $form = $this->form(HaulForm::class, [
            'method'    => 'PUT',
            'url'       => route('admin::super:hauls.update', ['haulId' => $haul->id]),
            'model'     => $haul,
            'data'      => [
                'manager'   => null,
                'user'      => $haul->user,
            ],
        ]);

        return view('admin.super.hauls.edit', compact('haul', 'form'));
    }

    public function postStore(HaulRequest $request)
    {
        $haul = new Haul();

        $form = $this->form(HaulForm::class, [
            'model' => $haul,
            'data'      => [
                'manager'   => null,
                'user'      => $haul->user,
            ],
        ]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $haul);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:hauls.edit', ['haulId' => $haul->id]);
    }

    public function postUpdate($haulId, HaulRequest $request)
    {
        $haul = Haul::findOrFail($haulId);

        $form = $this->form(HaulForm::class, [
            'model' => $haul,
            'data'      => [
                'manager'   => null,
                'user'      => $haul->user,
                'ticketId'  => null,
            ],
        ]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $haul);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:hauls.edit', ['haulId' => $haulId]);
    }

    private function update(HaulRequest $request, Haul $haul)
    {
        // Standard values
        $haul->area_name = $request->request->get('area_name');
        $haul->ticket_number = $request->request->get('ticket_number', '');
        $haul->size_value = $request->request->get('size_value');
        $haul->size_type = $request->request->get('size_type') ?: null;
        $haul->catch_date = Carbon::parse($request->request->get('catch_date'))->format('Y-m-d');
        $haul->catch_time = $request->request->get('catch_time')
            ? Carbon::parse($request->request->get('catch_time'))->format('H:i')
            : null;
        $haul->weather = $request->request->get('weather') ?: null;
        $haul->temperature = $request->request->get('temperature') ?: null;
        $haul->user_comment = $request->request->get('user_comment', '');
        $haul->public = $request->request->getBoolean('public');
        $haul->taken = $request->request->getBoolean('taken');

        // User
        if (($request->method() === 'POST') && ($user = $request->request->getInt('user'))) {
            $haul->user()->associate($user);
        }
        $haul->save();

        // Area
        if ($area = $request->request->getInt('area')) {
            $haul->area()->associate($area);
        } else {
            $haul->area()->dissociate();
        }

        // Fish
        if ($fish = $request->request->getInt('fish')) {
            $haul->fish()->associate($fish);
        } else {
            $haul->fish()->dissociate();
        }

        // Technique
        if ($technique = $request->request->getInt('technique')) {
            $haul->technique()->associate($technique);
        } else {
            $haul->technique()->dissociate();
        }

        // Ticket
        if ($ticket = $request->request->getInt('ticket')) {
            $haul->ticket()->associate($ticket);
        } else {
            $haul->ticket()->dissociate();
        }

        // Picture
        if ($request->hasFile('picture')) {
            $picture = Picture::create(['file' => $request->file('picture')]);
            if ($haul->picture) {
                $haul->picture->delete();
            }
            $haul->picture()->associate($picture);
        }

        $haul->save();
    }

    /**
     * Delete Haul
     *
     * @param $haulId
     * @throws AccessDeniedHttpException
     */
    public function delete($haulId)
    {
        $haul = Haul::findOrFail($haulId);
        $haul->delete();
    }

    public function anyData(Request $request)
    {
        $ticket = $request->query->get('ticket');
        $hauls = $this->getHauls($ticket);

        return Datatables::of($hauls)
            ->addColumn('fish_name', function (Haul $haul) {
                return $haul->fish ? $haul->fish->name : '<i>Leermeldung</i>';
            })
            ->addColumn('fish_size', function (Haul $haul) {
                return $haul->fish
                    ? $haul->size_value . ' ' . $haul->size_type
                    : '';
            })
            ->addColumn('user_name', function (Haul $haul) {
                return $haul->user ? $haul->user->full_name : '<i>Unbekannt</i>';
            })
            ->addColumn('fishing_methods', function (Haul $haul) {
                $fishingMethods = '';
                if ($haul->checkin && !empty($haul->checkin->fishingMethods)) {
                    foreach ($haul->checkin->fishingMethods as $fishingMethod) {
                        $fishingMethods .= $fishingMethod->translate(app()->getLocale(), true)->name . '<br />';
                    }
                }
                return $fishingMethods;
            })
            ->addColumn('area_name', function (Haul $haul) {
                return $haul->area ? $haul->area->name : $haul->area_name;
            })
            ->editColumn('catch_date', function (Haul $haul) {
                return $haul->catch_time
                    ? $haul->catch_date->format('d.m.Y') . ' ' . $haul->catch_time
                    : $haul->catch_date->format('d.m.Y');
            })
            ->addColumn('actions', function (Haul $haul) {
                $editRoute = route('admin::super:hauls.edit', ['haulId' => $haul->id]);
                $removeRoute = route('admin::super:hauls.delete', ['haulId' => $haul->id]);

                return '
                    <a class="btn btn-xs btn-primary"
                       href="'.$editRoute.'">
                        <i class="fa fa-edit fa-fw"></i>
                    </a>
                    <button type="button"
                            data-action="' . $removeRoute . '"
                            data-method="DELETE"
                            class="btn btn-xs btn-danger"
                            data-target="#delete-modal"
                            data-toggle="modal">
                        <i class="fa fa-trash-o fa-fw"></i>
                    </button>
                ';
            })
            ->make(true);
    }

    private function getHauls($ticket = null)
    {
        $hauls = Haul::with(['user', 'area'])
            ->select('hauls.*')
            ->leftJoin('areas', 'hauls.area_id', '=', 'areas.id')
            ->orderBy('catch_time', 'desc')
            ->orderBy('catch_date', 'desc');

        if ($ticket && $ticket > 0) {
            $hauls->whereHas('ticket', function ($q) use ($ticket) {
                $q->whereId($ticket);
            });
        }

        return $hauls;
    }
}
