<?php

namespace App\Admin\Controllers\Super;

use App\Models\Ticket\TicketPrice;

class TicketPriceController extends SuperAdminController
{
    public function delete($priceId)
    {
        $ticketPrice = TicketPrice::findOrFail($priceId);
        
        if ($ticketPrice->tickets()->count()) {
            return response('Es gibt schon verkaufte Angelkarten mit diesem Preis', 409);
        }
        $ticketPrice->delete();

        return response('Success', 200);
    }
}
