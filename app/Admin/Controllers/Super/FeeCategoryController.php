<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\FeeCategoryForm;
use App\Admin\Requests\FeeCategoryRequest;
use App\Models\Product\Fee;
use App\Models\Product\FeeCategory;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Datatables;
use DB;
use Html;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class FeeCategoryController extends SuperAdminController
{
    use FormBuilderTrait;

    public function getIndex(Request $request)
    {
        return view('admin.super.feecategory.index', $this->indexData($request));
    }

    public function getCreate()
    {
        $feeCategory = new FeeCategory();

        $form = $form = $this->form(FeeCategoryForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:fee_categories.store', ['categoryId' => $feeCategory->id]),
            'model'     => $feeCategory,
        ]);

        return view('admin.super.feecategory.edit', compact('feeCategory', 'form'));
    }

    public function getEdit($categoryId)
    {
        $feeCategory = FeeCategory::findOrFail($categoryId);

        $form = $this->form(FeeCategoryForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:fee_categories.update', ['categoryId' => $feeCategory->id]),
            'model'     => $feeCategory,
        ]);

        return view('admin.super.feecategory.edit', compact('feeCategory', 'form'));
    }

    /**
     * Get fee categories data
     *
     * @param $categoryId
     * @return mixed
     */
    public function show($categoryId)
    {
        return FeeCategory::findOrFail($categoryId)->toArray();
    }

    public function postCreate(FeeCategoryRequest $request)
    {
        $category = new FeeCategory();

        $form = $this->form(FeeCategoryForm::class, ['model' => $category]);

        if ($errorMsg = $this->checkFormValidity($form)) {
            flash()->error($errorMsg);
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $category);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:fee_categories.edit', ['categoryId' => $category->id]);
    }

    public function postUpdate(FeeCategoryRequest $request, $categoryId)
    {
        $category = FeeCategory::findOrFail($categoryId);

        $form = $this->form(FeeCategoryForm::class, ['model' => $category]);

        if ($errorMsg = $this->checkFormValidity($form)) {
            flash()->error($errorMsg);
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $category);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::super:fee_categories.edit', ['categoryId' => $categoryId]);
    }

    private function checkFormValidity(Form $form)
    {
        switch (true) {
            case !$form->isValid():
                return 'Es ist ein Fehler aufgetreten';
            default:
                return null;
        }
    }

    private function update(FeeCategoryRequest $request, FeeCategory $category)
    {
        DB::transaction(function () use ($request, $category) {
            // Translations
            $this->updateTranslations($request, $category);

            $category->save();
        });
    }

    private function updateTranslations(FeeCategoryRequest $request, FeeCategory $category)
    {
        $newData = $request->request->get('data', []);
        $newLocales = array_map(function ($item) {
            return $item['locale'];
        }, $newData);
        $category->translations()->whereNotIn('locale', $newLocales)->delete();

        foreach ($newData as $newItem) {
            $locale = $newItem['locale'];
            $category->translateOrNew($locale)->name = $newItem['name'];
        }
    }

    public function delete($categoryId)
    {
        $category = FeeCategory::findOrFail($categoryId);

        if ($category->fees()->count()) {
            throw new AccessDeniedHttpException('Kategorie könnte nicht gelöscht werden');
        }

        $category->delete();
        flash()->success('Kategorie wurde gelöscht');
    }

    public function anyData(Request $request)
    {
        $categories = $this->getFeeCategories($request->query->getInt('fee', 0))
            ->with(['fees']);

        return Datatables::of($categories)
            ->editColumn('name', function ($category) {
                return Html::linkRoute(
                    'admin::super:fee_categories.edit',
                    str_limit($category->name, 50),
                    ['categoryId' => $category->id]
                );
            })
            ->addColumn('actions', function ($category) {
                $editRoute = route('admin::super:fee_categories.edit', ['categoryId' => $category->id]);
                $removeRoute = route('admin::super:fee_categories.delete', ['categoryId' => $category->id]);
                $feesExists = (bool) $category->fees()->count();

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        <i class="fa fa-edit fa-fw"></i>
                        Bearbeiten
                    </a>
                    <button type="button"
                            data-action="' . $removeRoute . '"
                            data-method="DELETE"
                            class="btn btn-xs btn-danger'.($feesExists ? ' disabled ' : '').'"
                            data-target="#delete-modal"
                            data-toggle="modal"
                            '.($feesExists ? ' disabled ' : '').'>
                        Löschen<span class="fa fa-trash-o fa-fw"></span>
                    </button>
                ';
            })
            ->blacklist(['actions'])
            ->make(true);
    }

    private function indexData(Request $request) : array
    {
        $fee = $request->query->getInt('fee', 0);
        $categories = $this->getFeeCategories($fee);

        $count = $categories->count();

        $feeList = collect([
            '0' => 'Alle',
        ]);

        $fees = Fee::each(function ($fee) use ($feeList) {
                $feeList[$fee->id] = $fee->name;
            });

        $feeList->merge($fees);

        return compact('count', 'fee', 'feeList');
    }

    /**
     * @param int     $feeId
     * @return mixed
     */
    private function getFeeCategories(int $feeId = 0)
    {
        $categories = (new FeeCategory())->newQuery();

        if ($feeId > 0) {
            $categories->whereHas('fees', function ($query) use ($feeId) {
                $query->whereId($feeId);
            });
        }

        return $categories;
    }
}
