<?php

namespace App\Admin\Controllers\Super\Content;

use App\Admin\Controllers\Super\SuperAdminController;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Admin\Requests\LandingPageRequest;
use Illuminate\Http\Request;
use App\Models\Picture;
use App\Models\LandingPages\LandingPage;
use App\Admin\Forms\LandingPageForm;
use App\Models\Area\Area;
use App\Api1\Controllers\AreaController as ApiAreaController;
use Datatables;
use DB;
use Auth;

class LandingPageController extends SuperAdminController
{
    use FormBuilderTrait;

    /** @var Area */
    private $areaRepository;

    /**
     * @param Area $areaRepository
     */
    public function __construct(Area $areaRepository)
    {
        parent::__construct();
        $this->areaRepository = $areaRepository;
    }

    /**
     * Get index page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.super.content.landingpage.index');
    }

    /**
     * Get create page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $lp = new LandingPage();
        $form = $this->form(LandingPageForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.lp.store'),
            'model'     => $lp,
        ]);

        return view('admin.super.content.landingpage.edit', compact('form', 'lp'));
    }

    /**
     * Get edit page
     *
     * @param $lpId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($lpId)
    {
        $lp = LandingPage::findOrFail($lpId);

        $form = $this->form(LandingPageForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.lp.update', ['lpId' => $lp->id]),
            'model'     => $lp,
        ]);

        return view('admin.super.content.landingpage.edit', compact('form', 'lp'));
    }

    /**
     * Form request to create
     *
     * @param LandingPageRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postCreate(LandingPageRequest $request)
    {
        $lp = new LandingPage();
        $form = $this->form(LandingPageForm::class, ['model' => $lp]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $lp = $this->create($request, $lp);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.lp.edit', ['lpId' => $lp->id]);
    }

    /**
     * Form request to update
     *
     * @param LandingPageRequest $request
     * @param                    $lpId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postUpdate(LandingPageRequest $request, $lpId)
    {
        $lp = LandingPage::findOrFail($lpId);

        $form = $this->form(LandingPageForm::class, ['model' => $lp]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $this->update($request, $lp);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.lp.edit', ['lpId' => $lp->id]);
    }

    /**
     * Create new landing page
     *
     * @param LandingPageRequest $request
     * @param LandingPage        $lp
     * @return LandingPage
     * @throws \Exception
     */
    private function create(LandingPageRequest $request, LandingPage $lp)
    {
        $lp->fill($request->all());
        $lp->save();

        // Areas
        if ($request->request->get('search', [])) {
            // Find areas by search params
            $areaIds = $this->parseAreaRequest($request->request->get('search', []))->pluck('id')->toArray();
            if (count($areaIds) < 3) {
                throw new \Exception('ERROR: areas muss mindestens 3 Elemente haben.');
            }
            $lp->areas()->sync($areaIds);
        } else {
            // Areas: direct input
            $lp->areas()->sync($request->request->get('areas', []));
        }

        // Translations
        foreach ($request->request->get('data', []) as $newItem) {
            $locale = $newItem['locale'];
            $lp->translateOrNew($locale)->slug = $newItem['slug'];
            $lp->translateOrNew($locale)->title = trim($newItem['title']);
            $lp->translateOrNew($locale)->headline = trim($newItem['headline']);
            $lp->translateOrNew($locale)->punchline = trim($newItem['punchline']);
            $lp->translateOrNew($locale)->description = trim($newItem['description']);
        }

        // Picture
        if ($request->file('picture', null) != null) {
            $picture = new Picture();
            $picture->fill([
                'file'      => $request->file('picture'),
                'name'      => $request->file('picture')->getClientOriginalName(),
            ]);
            $picture->author()->associate(Auth::user());
            $picture->save();
            $lp->picture()->associate($picture);
        }

        $lp->save();

        return $lp;
    }

    /**
     * Update existing landing page
     *
     * @param LandingPageRequest $request
     * @param LandingPage        $lp
     * @throws \Exception
     */
    private function update(LandingPageRequest $request, LandingPage $lp)
    {
        $lp->fill($request->all());

        // Areas
        if ($request->request->get('search', [])) {
            // Find areas by search params
            $areaIds = $this->parseAreaRequest($request->request->get('search', []))->pluck('id')->toArray();
            if (count($areaIds) < 3) {
                throw new \Exception('ERROR: areas muss mindestens 3 Elemente haben.');
            }
            $lp->areas()->sync($areaIds);
        } else {
            // Areas: direct input
            $lp->areas()->sync($request->request->get('areas', []));
        }

        $existingData = $lp->translations()->get();
        $newData = $request->request->get('data', []);

        // Delete translations which did not come from request
        foreach ($existingData as $existingItem) {
            $toDelete = true;
            foreach ($newData as $newItem) {
                if ($newItem['locale'] == $existingItem['locale'] && $newItem['slug'] == $existingItem['slug']) {
                    $toDelete = false;
                }
            }
            if ($toDelete) {
                $existingItem->delete();
            }
        }

        // Translations
        foreach ($newData as $newItem) {
            $locale = $newItem['locale'];
            $lp->translateOrNew($locale)->slug = $newItem['slug'];
            $lp->translateOrNew($locale)->title = trim($newItem['title']);
            $lp->translateOrNew($locale)->headline = trim($newItem['headline']);
            $lp->translateOrNew($locale)->punchline = trim($newItem['punchline']);
            $lp->translateOrNew($locale)->description = trim($newItem['description']);
        }

        // Picture
        if ($request->file('picture', null) != null && $request->get('pic_changed', 0) == 1) {
            $picture = new Picture();
            $picture->fill([
                'file'      => $request->file('picture'),
                'name'      => $request->file('picture')->getClientOriginalName(),
            ]);
            $picture->author()->associate(Auth::user());
            $picture->save();
            $lp->picture()->associate($picture);
        }
        $lp->save();
    }

    /**
     * Delete landing page
     *
     * @param $lpId
     */
    public function delete($lpId)
    {
        $lp = LandingPage::findOrFail($lpId);
        $lp->delete();
    }

    /**
     * Get areas for specific landing page
     *
     * @param Request $request
     * @param         $lpId
     * @return mixed
     */
    public function getAreas(Request $request, $lpId)
    {
        $lp = LandingPage::findOrFail($lpId);

        $areaIds = $lp->areas()->pluck('areas.id')->toArray();
        $request->query->set('area_id', $areaIds);
        $request->query->set('_perPage', count($areaIds));

        return with(new ApiAreaController(new Area()))->index($request);
    }

    /**
     * List of all areas
     *
     * @param Request $request
     * @return mixed
     */
    public function getAllAreas(Request $request)
    {
        return with(new ApiAreaController(new Area()))->index($request);
    }

    /**
     * Get landing page data
     *
     * @param $lpId
     * @return mixed
     */
    public function show($lpId)
    {
        return LandingPage::findOrFail($lpId)->toArray();
    }

    /**
     * Search landing page
     *
     * @param LandingPageRequest $request
     * @return mixed
     */
    public function anyData(LandingPageRequest $request)
    {
        $areas = LandingPage::select([
            'landingpages.id',
            'landingpages.type',
        ])
            ->join('landingpage_translations', 'landingpages.id', '=', 'landingpage_translations.lp_id')
            //->where('landingpage_translations.locale', '=', App::getLocale());
            ->groupBy('landingpage_translations.lp_id');

        return Datatables::of($areas)
            ->filter(function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('landingpage_translations.slug', 'like', '%'.$request->search['value'].'%');
                }
            })
            ->editColumn('slug', function($area) {
                return $area->slug ?: ($area->translations ? $area->translations[0]->slug : null);
            })
            ->editColumn('title', function($area) {
                return $area->title ?: ($area->translations ? $area->translations[0]->title : null);
            })
            ->addColumn('actions', function($area) {
                $editRoute = route('admin::super:content.lp.edit', ['areaId' => $area->id]);
                $removeRoute = route('admin::super:content.lp.delete', ['areaId' => $area->id]);

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        Bearbeiten
                    </a>
                    <button type="button" data-action="' . $removeRoute . '" data-method="DELETE"
                        class="btn btn-xs btn-danger" data-target="#areas-modal" data-toggle="modal">
                        Löschen
                    </button>
                ';
            })
            ->make(true);
    }

    /**
     * Parse the area requested params (sort, filter, search, ...) and build query
     * 
     * @param array $search
     * @return \Illuminate\Database\Query\Builder;
     */
    private function parseAreaRequest(array $search)
    {
        $areaQuery = $this->areaRepository
            ->addSelect(['areas.*', 'areas.id as id'])
            ->orderBy('id', 'asc');

        // Filter by manager
        if (isset($search['manager']) && $search['manager']) {
            $areaQuery->hasManager($search['manager']);
        }

        // Filter by area type
        if (isset($search['area_types'])) {
            $areaQuery->hasAreaType($search['area_types']);
        }

        // if has fishes search parameter
        if (isset($search['fishes'])) {
            $areaQuery->hasFishes($search['fishes']);
        }

        // if has techniques search parameter
        if (isset($search['techniques'])) {
            $areaQuery->hasTechniques($search['techniques']);
        }

        // if has tickets available
        if (isset($search['is_ticket'])) {
            $areaQuery->hasTickets($search['is_ticket']);
        }

        if (isset($search['ticket_categories'])) {
            $areaQuery->hasTicketCategories($search['ticket_categories']);
        }

        // if is lease area
        if (isset($search['lease'])) {
            $areaQuery->isLease($search['lease']);
        }

        // if is not public
        $areaQuery->isPublic();

        // if has region/state/country search parameter
        if (isset($search['city'])) {
            $areaQuery->hasCity($search['city']);
        } elseif (isset($search['region'])) {
            $areaQuery->hasRegion($search['region']);
        } elseif (isset($search['state'])) {
            $areaQuery->hasState($search['state']);
        } elseif (isset($search['country'])) {
            $areaQuery->hasCountry($search['country']);
        }

        // if has member_only search parameter
        if (isset($search['member_only'])) {
            $areaQuery->hasMember($search['member_only']);
        }

        // if has boat search parameter
        if (isset($search['boat'])) {
            $areaQuery->hasBoat($search['boat']);
        }

        // if has phone_ticket search parameter
        if (isset($search['phone_ticket'])) {
            $areaQuery->hasPhoneTicket($search['phone_ticket']);
        }

        // if has nightfishing search parameter
        if (isset($search['nightfishing'])) {
            $areaQuery->hasNight($search['nightfishing']);
        }

        return $areaQuery;
    }
}
