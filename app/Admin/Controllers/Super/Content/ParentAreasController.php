<?php

namespace App\Admin\Controllers\Super\Content;

use App\Admin\Controllers\Super\SuperAdminController;
use App\Models\Area\AreaParent;
use App\Admin\Forms\ParentAreasForm;
use Illuminate\Http\Request;
use App\Admin\Requests\Content\ParentAreasRequest;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Datatables;
use Html;
use DB;
use App;

class ParentAreasController extends SuperAdminController
{
    use FormBuilderTrait;

    /**
     * Get index page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.super.content.parentareas.index');
    }

    /**
     * Get create page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $area = new AreaParent();
        $form = $this->form(ParentAreasForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.parentareas.store'),
            'model'     => $area,
        ]);

        return view('admin.super.content.parentareas.edit', compact('form', 'area'));
    }

    /**
     * Get edit page
     *
     * @param $parentAreaId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($parentAreaId)
    {
        $area = AreaParent::findOrFail($parentAreaId);

        $form = $this->form(ParentAreasForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.parentareas.update', ['areaId' => $area->id]),
            'model'     => $area,
        ]);

        return view('admin.super.content.parentareas.edit', compact('form', 'area'));
    }

    /**
     * Form request to create
     *
     * @param ParentAreasRequest    $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postCreate(ParentAreasRequest $request)
    {
        $area = new AreaParent();
        $form = $this->form(ParentAreasForm::class, ['model' => $area]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $areaId = $this->create($request, $area);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.parentareas.edit', ['areaId' => $areaId]);
    }

    /**
     * Form request to update
     *
     * @param ParentAreasRequest    $request
     * @param                       $parentAreaId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postUpdate(ParentAreasRequest $request, $parentAreaId)
    {
        $area = AreaParent::findOrFail($parentAreaId);

        $form = $this->form(ParentAreasForm::class, ['model' => $area]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $this->update($request, $area);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.parentareas.edit', ['areaId' => $area->id]);
    }

    /**
     * Create new item
     *
     * @param   ParentAreasRequest  $request
     * @param   AreaParent          $area
     * @return  int
     * @throws  \Exception
     * @internal param LandingPage $lp
     */
    private function create(ParentAreasRequest $request, AreaParent $area)
    {
        $area->fill($request->all());

        // Area type
        $type = $request->request->get('type', null) ?: null;
        $area->type()->associate($type);

        foreach ($request->request->get('data', []) as $newItem) {
            $locale = $newItem['locale'];
            $area->translateOrNew($locale)->name = $newItem['name'];
            $area->translateOrNew($locale)->description = trim($newItem['description']);
        }

        $area->save();

        return $area->id;
    }

    /**
     * Update existing item
     *
     * @param ParentAreasRequest    $request
     * @param AreaParent            $area
     * @throws \Exception
     */
    private function update(ParentAreasRequest $request, AreaParent $area)
    {
        $area->fill($request->all());

        // Area type
        $type = $request->request->get('type', null) ?: null;
        $area->type()->associate($type);

        $existingData = $area->translations()->get();
        $newData = $request->request->get('data', []);

        // Delete translations which did not come from request
        foreach ($existingData as $existingItem) {
            $toDelete = true;
            foreach ($newData as $newItem) {
                if ($newItem['locale'] == $existingItem['locale'] && $newItem['name'] == $existingItem['name']) {
                    $toDelete = false;
                }
            }
            if ($toDelete) {
                $existingItem->delete();
            }
        }

        foreach ($newData as $newItem) {
            $locale = $newItem['locale'];
            $area->translateOrNew($locale)->name = $newItem['name'];
            $area->translateOrNew($locale)->description = trim($newItem['description']);
        }

        $area->save();
    }

    /**
     * Get data
     *
     * @param $parentAreaId
     * @return mixed
     */
    public function show($parentAreaId)
    {
        return AreaParent::findOrFail($parentAreaId)->toArray();
    }

    /**
     * Delete city page
     *
     * @param $parentAreaId
     */
    public function delete($parentAreaId)
    {
        $area = AreaParent::findOrFail($parentAreaId);
        $area->delete();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function anyData(Request $request)
    {
        $areas = AreaParent::select([
            'parent_areas.*',
        ])
            ->join('parent_area_translations', 'parent_areas.id', '=', 'parent_area_translations.parent_id')
            //->where('parent_area_translations.locale', '=', App::getLocale());
            ->groupBy('parent_area_translations.parent_id');

        return Datatables::of($areas)
            ->filter(function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('parent_area_translations.name', 'like', '%'.$request->search['value'].'%');
                }
            })
            ->editColumn('name', function($area) {
                $editRoute = route('admin::super:content.parentareas.edit', ['areaId' => $area->id]);
                return '<a href="' . $editRoute . '">' . $area->name . '</a>';
            })
            ->addColumn('type', function ($area) {
                return $area->type ? $area->type->name : null;
            })
            ->addColumn('areas', function ($area) {
                //$route = route('admin::areas.index', ['parentId' => $area->id]);

                return '
                    <button class="btn btn-xs btn-primary">
                        <span class="fa fa-globe fa-fw"></span>
                        Gewässer <span class="badge">' .
                       ($area->areas ? $area->areas->count() : 0) .
                       '</span>
                    </button>';
            })
            ->addColumn('actions', function($area) {
                $editRoute = route('admin::super:content.parentareas.edit', ['areaId' => $area->id]);
                $removeRoute = route('admin::super:content.parentareas.delete', ['areaId' => $area->id]);

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        Bearbeiten
                    </a>
                    <button type="button" data-action="' . $removeRoute . '" data-method="DELETE"
                        class="btn btn-xs btn-danger" data-target="#areas-modal" data-toggle="modal">
                        Löschen
                    </button>
                ';
            })
            ->make(true);
    }
}
