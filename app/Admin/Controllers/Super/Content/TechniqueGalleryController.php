<?php

namespace App\Admin\Controllers\Super\Content;

use App\Admin\Controllers\Super\SuperAdminController;
use App\Admin\Forms\PictureForm;
use App\Admin\Requests\StorePictureRequest;
use App\Api1\Requests\ImageUploadRequest;
use App\Models\Gallery;
use App\Models\Meta\Technique;
use App\Models\Picture;
use Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class TechniqueGalleryController extends SuperAdminController
{
    use FormBuilderTrait;

    public function getList($techId)
    {
        $technique = Technique::findOrFail($techId);
        $gallery = $technique->gallery;

        return view('admin.super.content.technique.gallery.list', compact('gallery', 'technique'));
    }

    public function getEdit($techId, $pictureId)
    {
        $technique = Technique::findOrFail($techId);

        if (!$technique->gallery
            || !($picture = $technique->gallery->pictures()->find(['id' => $pictureId])->first())) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->form(PictureForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.techniques.gallery.save', [
                'techId'    => $techId,
                'pictureId' => $pictureId,
            ]),
            'model'     => $picture,
        ]);

        return view('admin.super.content.technique.gallery.edit', compact('technique', 'picture', 'form'));
    }

    public function postSave(StorePictureRequest $request, $techId, $pictureId)
    {
        $technique = Technique::findOrFail($techId);

        if (!($gallery = $technique->gallery)
            || !($picture = $gallery->pictures()->find(['id' => $pictureId])->first())) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->form(PictureForm::class, ['model' => $picture]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        if ($request->get('priority') == 5) {
            $technique->gallery->pictures()
                ->wherePivot('priority', '=', 5)
                ->each(function (Picture $picture) use ($gallery) {
                    $gallery->pictures()->updateExistingPivot($picture->id, ['priority' => 4], false);
                });
        }

        $gallery->pictures()->updateExistingPivot($picture->id, ['priority' => $request->get('priority')], false);
        $picture->update($request->only(['name', 'description']));

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.techniques.gallery.list', ['techId' => $techId]);
    }

    public function getDelete($techId, $pictureId)
    {
        /** @var Technique $technique */
        $technique = Technique::findOrFail($techId);

        if (!($gallery = $technique->gallery)
            || !($picture = $gallery->pictures()->find(['id' => $pictureId])->first())) {
            throw new AccessDeniedHttpException();
        }

        $picture->delete();

        flash()->success('Bild wurde gelöscht.');

        return redirect()->route('admin::super:content.techniques.gallery.list', ['techId' => $techId]);
    }

    public function getDetach($techId, $pictureId)
    {
        /** @var Technique $technique */
        $technique = Technique::findOrFail($techId);

        if (!($gallery = $technique->gallery) || !$gallery->pictures()->find(['id' => $pictureId])->first()) {
            throw new AccessDeniedHttpException();
        }

        $gallery->pictures()->detach($pictureId);

        flash()->success('Bild wurde von Gallery entfernt.');

        return redirect()->route('admin::super:content.techniques.gallery.list', ['techId' => $techId]);
    }

    public function postUpload(ImageUploadRequest $request, $techId)
    {
        /** @var Technique $technique */
        $technique = Technique::findOrFail($techId);

        $picture = new Picture([
            'file'  => $request->file('file'),
            'name'  => $request->file('file')->getClientOriginalName(),
        ]);
        $picture->author()->associate(Auth::user());

        $gallery = $technique->gallery ?: Gallery::create(['name' => $technique->name]);
        $gallery->pictures()->save($picture);
        $technique->gallery()->associate($gallery);
        $technique->save();

        return response()->json();
    }
}
