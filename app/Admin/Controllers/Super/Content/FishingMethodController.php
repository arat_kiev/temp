<?php

namespace App\Admin\Controllers\Super\Content;

use App\Admin\Controllers\Super\SuperAdminController;
use App\Models\Meta\FishingMethod;
use App\Admin\Forms\FishingMethodForm;
use App\Admin\Requests\Content\FishingMethodRequest;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Datatables;

class FishingMethodController extends SuperAdminController
{
    use FormBuilderTrait;

    /**
     * @var FishingMethod
     */
    private $fishingMethodRepository;

    public function __construct(FishingMethod $fishingMethodRepository)
    {
        parent::__construct();

        $this->fishingMethodRepository = $fishingMethodRepository;
    }

    /**
     * Get index page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.super.content.fishing_methods.index');
    }

    /**
     * Get FishingMethod form from AJAX
     *
     * @param FishingMethodRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getDataForm(FishingMethodRequest $request)
    {
        if ($request->fishing_method_id && $request->fishing_method_id !== 0) {
            $fishingMethod = $this->fishingMethodRepository->findOrFail($request->fishing_method_id);
        } else {
            $fishingMethod = null;
        }

        $html = view('admin.super.content.fishing_methods.forms.data', ['fishingMethod' => $fishingMethod, 'rowId' => $request->rowId])->render();

        return response()->json(['data' => $html]);
    }

    /**
     * Get FishingMethod create page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $fishingMethod = new $this->fishingMethodRepository();
        $form = $this->form(FishingMethodForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.fishing-methods.store'),
            'model'     => $fishingMethod,
        ]);

        return view('admin.super.content.fishing_methods.edit', compact('form', 'fishingMethod'));
    }

    /**
     * Get FishingMethod edit page
     *
     * @param $fishingMethodId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($fishingMethodId)
    {
        $fishingMethod = $this->fishingMethodRepository->findOrFail($fishingMethodId);

        $form = $this->form(FishingMethodForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.fishing-methods.update', ['fishingMethod' => $fishingMethod]),
            'model'     => $fishingMethod,
        ]);

        return view('admin.super.content.fishing_methods.edit', compact('form', 'fishingMethod'));
    }

    /**
     * Create new fishing Method
     *
     * @param FishingMethodRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(FishingMethodRequest $request)
    {
        $this->fishingMethodRepository = new $this->fishingMethodRepository();

        foreach ($request->request->get('data', []) as $newItem) {
            $locale = $newItem['locale'];
            $this->fishingMethodRepository->translateOrNew($locale)->name = $newItem['name'];
            $this->fishingMethodRepository->translateOrNew($locale)->description = trim($newItem['description']);
        }

        $this->fishingMethodRepository->save();

        return redirect()->route('admin::super:content.fishing-methods.index');
    }

    /**
     * Update FishingMethod
     *
     * @param FishingMethodRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(FishingMethodRequest $request)
    {
        $this->fishingMethodRepository = $this->fishingMethodRepository->findOrFail($request->fishingMethodId);
        $this->fishingMethodRepository->deleteTranslations();

        foreach ($request->request->get('data', []) as $newItem) {
            $locale = $newItem['locale'];
            $this->fishingMethodRepository->translateOrNew($locale)->name = $newItem['name'];
            $this->fishingMethodRepository->translateOrNew($locale)->description = trim($newItem['description']);
        }

        $this->fishingMethodRepository->save();

        return redirect()->route('admin::super:content.fishing-methods.index');
    }


    /**
     * Delete FishingMethod
     *
     * @param $fishingMethodId
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($fishingMethodId)
    {
        $fishingMethodRepository = $this->fishingMethodRepository->findOrFail($fishingMethodId);
        $fishingMethodRepository->delete();

        return redirect()->route('admin::super:content.fishing-methods.index');
   }

    /**
     * Restore FishingMethod
     *
     * @param $fishingMethodId
     * @throws \Exception
     */
    public function restore($fishingMethodId)
    {
        $fishingMethodRepository = $this->fishingMethodRepository->findOrFail($fishingMethodId);
        $fishingMethodRepository->restore();
    }


    /**
     * Destroy FishingMethod
     *
     * @param $fishingMethodId
     * @throws \Exception
     */
    public function destroy($fishingMethodId)
    {
        $this->fishingMethodRepository = $this->fishingMethodRepository->findOrFail($fishingMethodId);
        $this->fishingMethodRepository->deleteTranslations();
        $this->fishingMethodRepository->destroy($fishingMethodId);
    }

    /**
     * Get index Datatables data
     *
     * @param FishingMethodRequest $request
     * @return mixed
     */
    public function anyData(FishingMethodRequest $request)
    {
        $this->fishingMethodRepository = $this->fishingMethodRepository->select([
            'fishing_methods.id',
        ])
            ->join('fishing_methods_translations', 'fishing_methods.id', '=', 'fishing_methods_translations.fishing_method_id')
            ->groupBy('fishing_methods_translations.fishing_method_id');

        return Datatables::of($this->fishingMethodRepository)
            ->filter(function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('fishing_methods_translations.name', 'like', '%'.$request->search['value'].'%');
                }
            })
            ->editColumn('name', function($fishingMethodRepository) {
                $editRoute = route('admin::super:content.fishing-methods.edit', ['fishingMethodId' => $fishingMethodRepository->id]);
                return '<a href="' . $editRoute . '">' . $fishingMethodRepository->name . '</a>';
            })
            ->addColumn('actions', function($fishingMethodRepository) {
                $editRoute = route('admin::super:content.fishing-methods.edit', ['fishingMethodId' => $fishingMethodRepository->id]);
                $removeRoute = route('admin::super:content.fishing-methods.delete', ['fishingMethodId' => $fishingMethodRepository->id]);

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        Bearbeiten
                    </a>
                    <button type="button" data-action="' . $removeRoute . '" data-method="DELETE"
                        class="btn btn-xs btn-danger" data-target="#fishing-method-modal" data-toggle="modal">
                        Löschen
                    </button>
                ';
            })
            ->make(true);
    }
}
