<?php

namespace App\Admin\Controllers\Super\Content;

use App\Admin\Controllers\Super\SuperAdminController;
use App\Models\PointOfInterest\PointOfInterest;
use App\Models\PointOfInterest\PointOfInterestOpeningHour;
use App\Models\Picture;
use App\Admin\Forms\PointOfInterestForm;
use App\Admin\Requests\Content\PointOfInterestRequest;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Illuminate\Http\Request;
use App;
use Datatables;
use DB;
use Auth;

class PoiController extends SuperAdminController
{
    use FormBuilderTrait;


    private $poiRepository;

    public function __construct(PointOfInterest $poiRepository)
    {
        parent::__construct();

        $this->poiRepository = $poiRepository->includeDraft();
    }

    /**
     * Get index page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.super.content.poi.index');
    }

    /**
     * Get create page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $poi = new PointOfInterest();
        $weekDays = $this->getWeekDays();
        
        $form = $this->form(PointOfInterestForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.poi.store'),
            'model'     => $poi,
        ]);

        return view('admin.super.content.poi.edit', compact('form', 'poi', 'weekDays'));
    }

    /**
     * Get edit page
     *
     * @param $poiId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($poiId)
    {
        $poi = $this->poiRepository->findOrFail($poiId);
        $weekDays = $this->getWeekDays();

        $form = $this->form(PointOfInterestForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.poi.update', ['poiId' => $poi->id]),
            'model'     => $poi,
        ]);

        return view('admin.super.content.poi.edit', compact('form', 'poi', 'weekDays'));
    }

    /**
     * Form request to create
     *
     * @param PointOfInterestRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postCreate(PointOfInterestRequest $request)
    {
        $poi = new PointOfInterest();
        $form = $this->form(PointOfInterestForm::class, ['model' => $poi]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $poiId = $this->create($request, $poi);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.poi.edit', ['poiId' => $poiId]);
    }

    /**
     * Form request to update
     *
     * @param PointOfInterestRequest    $request
     * @param                           $poiId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postUpdate(PointOfInterestRequest $request, $poiId)
    {
        $poi = $this->poiRepository->findOrFail($poiId);

        $form = $this->form(PointOfInterestForm::class, ['model' => $poi]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $this->update($request, $poi);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.poi.edit', ['poiId' => $poi->id]);
    }

    /**
     * Create new item
     *
     * @param   PointOfInterestRequest  $request
     * @param   PointOfInterest         $poi
     * @return  int
     * @throws  \Exception
     * @internal param LandingPage $lp
     */
    private function create(PointOfInterestRequest $request, PointOfInterest $poi)
    {
        $poi->fill($request->all());
        $poi->save();

        // Country
        $country = $request->request->get('country', null) ?: null;
        $poi->country()->associate($country);

        // Areas
        $poi->areas()->sync($request->request->get('areas', []));

        // Categories
        $poi->categories()->sync($request->request->get('categories', []));

        // Opening Hours
        // $poi->opening_hours()->sync($request->request->get('opening_hours', []));
        $newHours = [];
        foreach ($request->request->get('opening_hours', []) as $hoursItem) {
            $newHours[] = new PointOfInterestOpeningHour([
                'day'           => $hoursItem['day'],
                'opens'         => $hoursItem['opens'],
                'closes'        => $hoursItem['closes'],
                'time_open'     => date("Hi", strtotime($hoursItem['opens'])),
                'time_closed'   => date("Hi", strtotime($hoursItem['closes'])),
            ]);
        }
        $poi->opening_hours()->saveMany($newHours);

        // Picture
        if ($request->hasFile('picture')) {
            $picture = new Picture();
            $picture->fill([
                'file'  => $request->file('picture'),
                'name'  => $request->file('picture')->getClientOriginalName(),
            ]);
            $picture->author()->associate(Auth::user());
            $picture->save();
            $poi->picture()->associate($picture);
        }

        // Translations
        foreach ($request->request->get('data', []) as $newItem) {
            $locale = $newItem['locale'];
            $poi->translateOrNew($locale)->description = trim($newItem['description']);
        }

        $poi->save();

        return $poi->id;
    }

    /**
     * Update existing fish
     *
     * @param PointOfInterestRequest    $request
     * @param PointOfInterest           $poi
     * @throws \Exception
     */
    private function update(PointOfInterestRequest $request, PointOfInterest $poi)
    {
        $poi->fill($request->all());
        $poi->save();

        // Country
        $country = $request->get('country', null) ?: null;
        $poi->country()->associate($country);

        // Areas
        $poi->areas()->sync($request->get('areas', []));

        // Categories
        $poi->categories()->sync($request->get('categories', []));

        // Opening Hours
        $poi->opening_hours()->delete();
        $newHours = [];
        foreach ($request->get('opening_hours', []) as $hoursItem) {
            $newHours[] = new PointOfInterestOpeningHour([
                'day'           => $hoursItem['day'],
                'opens'         => $hoursItem['opens'],
                'closes'        => $hoursItem['closes'],
                'time_open'     => date("Hi", strtotime($hoursItem['opens'])),
                'time_closed'   => date("Hi", strtotime($hoursItem['closes'])),
            ]);
        }
        $poi->opening_hours()->saveMany($newHours);

        // Picture
        if ($request->hasFile('picture')) {
            $oldPictureId = $poi->picture ? $poi->picture->id : null;
            $picture = new Picture();
            $picture->fill([
                'file'  => $request->file('picture'),
                'name'  => $request->file('picture')->getClientOriginalName(),
            ]);
            $picture->author()->associate(Auth::user());
            $picture->save();
            $poi->picture()->associate($picture);
            $poi->save();
            if ($oldPictureId) Picture::find($oldPictureId)->delete();
        }

        $newData = $request->get('data', []);
        $newLocales = array_map(function($data) {
            return $data['locale'];
        }, $newData);
        $existingData = $poi->translations()->get();

        // Delete translations which did not come from request
        foreach ($existingData as $existingItem) {
            if (!in_array($existingItem['locale'], $newLocales)) {
                $existingItem->delete();
            }
        }

        // Translations
        foreach ($newData as $newItem) {
            $locale = $newItem['locale'];
            $poi->translateOrNew($locale)->description = trim($newItem['description']);
        }

        $poi->save();
    }

    /**
     * Delete item
     *
     * @param $poiId
     */
    public function delete($poiId)
    {
        $poi = $this->poiRepository->findOrFail($poiId);
        $poi->delete();
    }

    /**
     * publish or unpublish
     * @param $managerId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function publish($managerId, Request $request)
    {
        $manager = $this->poiRepository->findOrFail($managerId);
        $manager->draft($request->get('drafted'));
            return response()->json();
    }

    /**
     * Search rows
     *
     * @param PointOfInterestRequest $request
     * @return mixed
     */
    public function anyData(PointOfInterestRequest $request)
    {
        $pois = PointOfInterest::with(['translations', 'country']);

        return Datatables::of($pois)
            ->filter(function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('points_of_interest.name', 'like', '%'.$request->search['value'].'%');
                }
            })
            ->editColumn('name', function($poi) {
                $editRoute = route('admin::super:content.poi.edit', ['poiId' => $poi->id]);
                return '<a href="' . $editRoute . '">' . $poi->name . '</a>';
            })
            ->editColumn('country', function($poi) {
                if (!$poi->country) {
                    return null;
                }
                $editRoute = route('admin::super:content.countries.edit', ['countryId' => $poi->country_id]);
                return '<a href="' . $editRoute . '">' . $poi->country->name . '</a>';
            })
            ->addColumn('actions', function($poi) {
                $editRoute = route('admin::super:content.poi.edit', ['poiId' => $poi->id]);
                $removeRoute = route('admin::super:content.poi.delete', ['poiId' => $poi->id]);

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        Bearbeiten
                    </a>
                    <button type="button" data-action="' . $removeRoute . '" data-method="DELETE"
                        class="btn btn-xs btn-danger" data-target="#poi-modal" data-toggle="modal">
                        Löschen
                    </button>
                ';
            })
            ->make(true);
    }

    private function getWeekDays()
    {
        return [
            'MON'   => 'Montag',
            'TUE'   => 'Dienstag',
            'WED'   => 'Mittwoch',
            'THU'   => 'Donnerstag',
            'FRI'   => 'Freitag',
            'SAT'   => 'Samstag',
            'SUN'   => 'Sonntag',
        ];
    }
}
