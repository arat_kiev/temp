<?php

namespace App\Admin\Controllers\Super\Content;

use App\Admin\Controllers\Super\SuperAdminController;
use App\Admin\Forms\LegalDocumentForm;
use App\Admin\Requests\Content\LegalDocumentRequest;
use App\Models\Meta\LegalDocument;
use App\Models\Picture;
use Auth;
use Datatables;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class LegalDocumentController extends SuperAdminController
{
    use FormBuilderTrait;

    private $documentRepository;

    public function __construct(LegalDocument $documentRepository)
    {
        parent::__construct();

        $this->documentRepository = $documentRepository;
    }

    public function getIndex(Request $request)
    {
        $count = $this->documentRepository->count();

        return view('admin.super.content.legaldocs.index', compact('count'));
    }

    public function getCreate()
    {
        $document = $this->documentRepository->newInstance();
        $form = $this->form(LegalDocumentForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.legaldocs.store'),
            'model'     => $document,
        ]);

        return view('admin.super.content.legaldocs.edit', compact('form', 'document'));
    }

    public function getEdit($docId)
    {
        $document = $this->documentRepository->findOrFail($docId);
        $form = $this->form(LegalDocumentForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.legaldocs.update', ['docId' => $document]),
            'model'     => $document,
        ]);

        return view('admin.super.content.legaldocs.edit', compact('form', 'document'));
    }

    public function postCreate(LegalDocumentRequest $request)
    {
        $document = $this->documentRepository->newInstance();
        $form = $this->form(LegalDocumentForm::class, ['model' => $document]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $document);

        return redirect()->route('admin::super:content.legaldocs.edit', ['docId' => $document]);
    }

    public function postUpdate(LegalDocumentRequest $request, $docId)
    {
        $document = $this->documentRepository->findOrFail($docId);
        $form = $this->form(LegalDocumentForm::class, ['model' => $document]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $document);

        return redirect()->route('admin::super:content.legaldocs.edit', ['docId' => $document]);
    }

    public function delete($docId)
    {
        $document = $this->documentRepository->findOrFail($docId);
        $document->delete();

        return response()->json();
    }

    private function update(LegalDocumentRequest $request, $document)
    {
        $translations = collect($request->request->get('translations'))
            ->zip($request->file('translations'))
            ->transform(function ($item) {
                return array_collapse($item);
            });

        $document->internal_name = $request->request->get('internal_name');
        $this->updateTranslations($document, $translations);
        $this->updateTranslationFiles($document, $translations);
        $document->save();
    }

    private function updateTranslationFiles($document, $translations)
    {
        foreach ($translations as $translation) {
            if (!array_has($translation, 'file')) {
                continue;
            }

            $docFile = new Picture([
                'file' => $translation['file'],
            ]);
            $docFile->author()->associate(Auth::user());
            $docFile->save();

            $document->translate($translation['locale'])->file()->associate($docFile);
        }
    }

    private function updateTranslations($document, $translations)
    {
        $preparedTranslations = collect($translations)->keyBy('locale');

        $existingLocales = $document->translations()->pluck('locale');
        $newLocales = $preparedTranslations->keys();
        $obsoleteLocales = $existingLocales->diff($newLocales);

        if ($obsoleteLocales->isNotEmpty()) {
            $document->deleteTranslations($obsoleteLocales);
        }

        $document->fill($preparedTranslations->toArray());
    }

    public function anyData()
    {
        $legalDocs = $this->documentRepository->withCount('managers');

        return Datatables::of($legalDocs)
            ->editColumn('created_at', function ($doc) {
                return $doc->created_at->format('d.m.Y - H:i');
            })
            ->editColumn('name', function ($doc) {
                $editRoute = route('admin::super:content.legaldocs.edit', ['docId' => $doc]);

                return "
                    <a href='{$editRoute}'>
                        {$doc->internal_name}
                    </a>              
                ";
            })
            ->addColumn('managers', function ($doc) {
                $managerRoute = route('admin::super:content.legaldocs.managers.index', ['docId' => $doc]);

                return "
                    <a href='{$managerRoute}' class='btn btn-xs btn-primary'>
                        <span class='fa fa-shopping-cart fa-fw'></span>
                        Bewirtschafter <span class='badge'>{$doc->managers_count}</span>
                    </a>
                ";
            })
            ->addColumn('actions', function ($doc) {
                $editRoute = route('admin::super:content.legaldocs.edit', ['docId' => $doc]);
                $removeRoute = route('admin::super:content.legaldocs.delete', ['docId' => $doc]);

                return "
                    <a href='{$editRoute}' class='btn btn-xs btn-primary'>
                        Bearbeiten
                    </a>
                    <button type='button' data-action='{$removeRoute}' data-method='delete'
                        class='btn btn-xs btn-danger' data-target='#legaldocs-modal' data-toggle='modal'>
                        Löschen
                    </button>
                ";
            })
            ->make(true);
    }
}