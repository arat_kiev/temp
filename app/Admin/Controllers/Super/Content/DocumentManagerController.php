<?php

namespace App\Admin\Controllers\Super\Content;

use App\Admin\Controllers\Super\SuperAdminController;
use App\Models\Contact\Manager;
use App\Models\Meta\LegalDocument;
use Carbon\Carbon;
use Datatables;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class DocumentManagerController extends SuperAdminController
{
    use FormBuilderTrait;

    private $documentRepository;

    private $managerRepository;

    const FILTER_STATUS_LIST = [
        'ALL' => 'ALL',
        'PENDING' => 'PENDING',
        'CONFIRMED' => 'CONFIRMED',
    ];

    public function __construct(LegalDocument $documentRepository, Manager $managerRepository)
    {
        parent::__construct();

        $this->documentRepository = $documentRepository;
        $this->managerRepository = $managerRepository;
    }

    public function getIndex(Request $request, $docId)
    {
        $document = $this->documentRepository->findOrFail($docId);

        $filter['status']['select'] = $request->query->get('status', 'PENDING');
        $filter['status']['values'] = self::FILTER_STATUS_LIST;

        return view('admin.super.content.legaldocs.managers.index', compact('document', 'filter'));
    }

    public function postAdd(Request $request, $docId)
    {
        $document = $this->documentRepository->findOrFail($docId);

        $newManagers = $request->request->get('managers', []);
        $document->managers()->attach($newManagers);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::super:content.legaldocs.managers.index', compact('docId'));
    }

    public function delete(Request $request, $docId, $managerId)
    {
        $document = $this->documentRepository->findOrFail($docId);
        $document->managers()->detach($managerId);

        return response()->json();
    }

    public function anyData(Request $request, $docId)
    {
        $managers = $this->documentRepository->findOrFail($docId)->managers();

        $filterStatus = $request->query->get('status', 'PENDING');

        if ($filterStatus !== 'ALL') {
            if ($filterStatus === 'PENDING') {
                $managers->whereNull('signed_at');
            } else {
                $managers->whereNotNull('signed_at');
            }
        }

        return Datatables::of($managers)
            ->editColumn('created_at', function ($manager) {
                return $manager->created_at->format('d.m.Y - H:i');
            })
            ->addColumn('signed_at', function ($manager) {
                return $manager->signed_at ?
                    Carbon::parse($manager->signed_at)->format('d.m.Y - H:i') : '---';
            })
            ->addColumn('signed_by', function ($manager) {
                return $manager->signed_by ?: '---';
            })
            ->addColumn('actions', function ($manager) use ($docId) {
                $removeRoute = route('admin::super:content.legaldocs.managers.delete', [
                    'docId' => $docId,
                    'managerId' => $manager,
                ]);

                return "
                    <button type='button' data-action='{$removeRoute}' data-method='delete'
                        class='btn btn-xs btn-danger' data-target='#manager-docs-modal' data-toggle='modal'>
                        Entfernen
                    </button>
                ";
            })
            ->make(true);
    }
}