<?php

namespace App\Admin\Controllers\Super\Content;

use App\Admin\Controllers\Super\SuperAdminController;
use App\Models\Location\Region;
use App\Models\Location\City;
use App\Admin\Forms\CityForm;
use Illuminate\Http\Request;
use App\Admin\Requests\Content\CityRequest;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App;
use Datatables;
use DB;
use Auth;

class CityController extends SuperAdminController
{
    use FormBuilderTrait;

    /**
     * Get index page
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $region = $request->query->getInt('regionId', 0);
        $count = $this->getRegionCities($region)->count();

        $regionList = collect([
            '0' => 'Alle',
        ]);

        $regions = Region::all()->each(function($region) use ($regionList) {
            $regionList[$region->id] = $region->name;
        });

        $regionList->merge($regions);

        return view('admin.super.content.city.index', compact('regionList', 'count', 'region'));
    }

    /**
     * Get create page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $city = new City();
        $form = $this->form(CityForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.cities.store'),
            'model'     => $city,
        ]);

        return view('admin.super.content.city.edit', compact('form', 'city'));
    }

    /**
     * Get edit page
     *
     * @param $cityId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($cityId)
    {
        $city = City::findOrFail($cityId);

        $form = $this->form(CityForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.cities.update', ['cityId' => $city->id]),
            'model'     => $city,
        ]);

        return view('admin.super.content.city.edit', compact('form', 'city'));
    }

    /**
     * Form request to create
     *
     * @param CityRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postCreate(CityRequest $request)
    {
        $city = new City();
        $form = $this->form(CityForm::class, ['model' => $city]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $cityId = $this->create($request, $city);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.cities.edit', ['cityId' => $cityId]);
    }

    /**
     * Form request to update
     *
     * @param CityRequest   $request
     * @param               $cityId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postUpdate(CityRequest $request, $cityId)
    {
        $city = City::findOrFail($cityId);

        $form = $this->form(CityForm::class, ['model' => $city]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $this->update($request, $city);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.cities.edit', ['cityId' => $city->id]);
    }

    /**
     * Create new city
     *
     * @param   CityRequest $request
     * @param   City        $city
     * @return  int
     * @throws  \Exception
     * @internal param LandingPage $lp
     */
    private function create(CityRequest $request, City $city)
    {
        $city->fill($request->all());

        // Region
        $region = $request->request->get('region', null) ?: null;
        $city->region()->associate($region);

        foreach ($request->request->get('data', []) as $newItem) {
            $locale = $newItem['locale'];
            $city->translateOrNew($locale)->name = $newItem['name'];
            $city->translateOrNew($locale)->description = trim($newItem['description']);
        }

        $city->save();

        return $city->id;
    }

    /**
     * Update existing city
     *
     * @param CityRequest   $request
     * @param City          $city
     * @throws \Exception
     */
    private function update(CityRequest $request, City $city)
    {
        $city->fill($request->all());

        // Region
        $region = $request->request->get('region', null) ?: null;
        $city->region()->associate($region);

        $existingData = $city->translations()->get();
        $newData = $request->request->get('data', []);

        // Delete translations which did not come from request
        foreach ($existingData as $existingItem) {
            $toDelete = true;
            foreach ($newData as $newItem) {
                if ($newItem['locale'] == $existingItem['locale'] && $newItem['name'] == $existingItem['name']) {
                    $toDelete = false;
                }
            }
            if ($toDelete) {
                $existingItem->delete();
            }
        }

        foreach ($newData as $newItem) {
            $locale = $newItem['locale'];
            $city->translateOrNew($locale)->name = $newItem['name'];
            $city->translateOrNew($locale)->description = trim($newItem['description']);
        }

        $city->save();
    }

    /**
     * Delete city page
     *
     * @param $cityId
     */
    public function delete($cityId)
    {
        $city = City::findOrFail($cityId);
        $city->delete();
    }

    /**
     * Get city data
     *
     * @param $cityId
     * @return mixed
     */
    public function show($cityId)
    {
        return City::findOrFail($cityId)->toArray();
    }

    /**
     * Search city
     *
     * @param CityRequest $request
     * @return mixed
     */
    public function anyData(CityRequest $request)
    {
        $region = $request->query->getInt('regionId', 0);
        $cities = $this->getRegionCities($region)
            ->join('city_translations', 'cities.id', '=', 'city_translations.city_id')
            // ->where('city_translations.locale', '=', App::getLocale());
            ->groupBy('city_translations.city_id');

        return Datatables::of($cities)
            ->filter(function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('city_translations.name', 'like', '%'.$request->search['value'].'%');
                }
            })
            ->editColumn('name', function($city) {
                $editRoute = route('admin::super:content.cities.edit', ['cityId' => $city->id]);
                return '<a href="' . $editRoute . '">' . $city->name . '</a>';
            })
            ->editColumn('region', function($city) {
                $editRoute = route('admin::super:content.regions.edit', ['regionId' => $city->region_id]);
                return '<a href="' . $editRoute . '">' . $city->region->name . '</a>';
            })
            ->addColumn('areas', function ($city) {
                //$route = route('admin::areas.index', ['cityId' => $city->id]);

                return '
                    <button class="btn btn-xs btn-primary">
                        <span class="fa fa-globe fa-fw"></span>
                        Gewässer <span class="badge">' .
                       ($city->areas ? $city->areas->count() : 0) .
                       '</span>
                    </button>';
            })
            ->addColumn('actions', function($city) {
                $editRoute = route('admin::super:content.cities.edit', ['cityId' => $city->id]);
                $removeRoute = route('admin::super:content.cities.delete', ['cityId' => $city->id]);

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        Bearbeiten
                    </a>
                    <button type="button" data-action="' . $removeRoute . '" data-method="DELETE"
                        class="btn btn-xs btn-danger" data-target="#cities-modal" data-toggle="modal">
                        Löschen
                    </button>
                ';
            })
            ->make(true);
    }

    /**
     * Return query of cities
     *
     * @param $region
     * @return mixed
     */
    private function getRegionCities($region)
    {
        $query = City::with(['translations', 'region'])
            ->select('cities.*');

        if ($region > 0) {
            $query->where('cities.region_id', '=', $region);
        }

        return $query;
    }
}
