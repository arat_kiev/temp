<?php

namespace App\Admin\Controllers\Super\Content;

use App\Admin\Controllers\Super\SuperAdminController;
use App\Models\Location\Country;
use App\Admin\Forms\CountryForm;
use App\Admin\Requests\Content\CountryRequest;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App;
use Datatables;
use DB;

class CountryController extends SuperAdminController
{
    use FormBuilderTrait;

    /**
     * Get index page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.super.content.country.index');
    }

    /**
     * Get create page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $country = new Country();
        $form = $this->form(CountryForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.countries.store'),
            'model'     => $country,
        ]);

        return view('admin.super.content.country.edit', compact('form', 'country'));
    }

    /**
     * Get edit page
     *
     * @param $countryId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($countryId)
    {
        $country = Country::findOrFail($countryId);

        $form = $this->form(CountryForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.countries.update', ['countryId' => $country->id]),
            'model'     => $country,
        ]);

        return view('admin.super.content.country.edit', compact('form', 'country'));
    }

    /**
     * Form request to create
     *
     * @param CountryRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postCreate(CountryRequest $request)
    {
        $country = new Country();
        $form = $this->form(CountryForm::class, ['model' => $country]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $countryId = $this->create($request, $country);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.countries.edit', ['countryId' => $countryId]);
    }

    /**
     * Form request to update
     *
     * @param CountryRequest    $request
     * @param                   $countryId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postUpdate(CountryRequest $request, $countryId)
    {
        $country = Country::findOrFail($countryId);

        $form = $this->form(CountryForm::class, ['model' => $country]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $this->update($request, $country);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.countries.edit', ['countryId' => $country->id]);
    }

    /**
     * Create new country
     *
     * @param   CountryRequest  $request
     * @param   Country         $country
     * @return  int
     * @throws  \Exception
     * @internal param LandingPage $lp
     */
    private function create(CountryRequest $request, Country $country)
    {
        $country->fill($request->all());

        foreach ($request->request->get('data', []) as $newItem) {
            $locale = $newItem['locale'];
            $country->translateOrNew($locale)->name = $newItem['name'];
            $country->translateOrNew($locale)->description = trim($newItem['description']);
        }

        $country->save();

        return $country->id;
    }

    /**
     * Update existing country
     *
     * @param CountryRequest    $request
     * @param Country           $country
     * @throws \Exception
     */
    private function update(CountryRequest $request, Country $country)
    {
        $country->fill($request->all());

        $existingData = $country->translations()->get();
        $newData = $request->request->get('data', []);

        // Delete translations which did not come from request
        foreach ($existingData as $existingItem) {
            $toDelete = true;
            foreach ($newData as $newItem) {
                if ($newItem['locale'] == $existingItem['locale'] && $newItem['name'] == $existingItem['name']) {
                    $toDelete = false;
                }
            }
            if ($toDelete) {
                $existingItem->delete();
            }
        }

        foreach ($newData as $newItem) {
            $locale = $newItem['locale'];
            $country->translateOrNew($locale)->name = $newItem['name'];
            $country->translateOrNew($locale)->description = trim($newItem['description']);
        }

        $country->save();
    }

    /**
     * Delete country page
     *
     * @param $countryId
     */
    public function delete($countryId)
    {
        $country = Country::findOrFail($countryId);
        $country->delete();
    }

    /**
     * Get country data
     *
     * @param $countryId
     * @return mixed
     */
    public function show($countryId)
    {
        return Country::findOrFail($countryId)->toArray();
    }

    /**
     * Search country
     *
     * @param CountryRequest $request
     * @return mixed
     */
    public function anyData(CountryRequest $request)
    {
        $countries = Country::select([
            'countries.id',
        ])
            ->join('country_translations', 'countries.id', '=', 'country_translations.country_id')
            // ->where('country_translations.locale', '=', App::getLocale());
            ->groupBy('country_translations.country_id');

        return Datatables::of($countries)
            ->filter(function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('country_translations.name', 'like', '%'.$request->search['value'].'%');
                }
            })
            ->editColumn('name', function($country) {
                $editRoute = route('admin::super:content.countries.edit', ['countryId' => $country->id]);
                return '<a href="' . $editRoute . '">' . $country->name . '</a>';
            })
            ->addColumn('states', function ($country) {
                $route = route('admin::super:content.states.index', ['countryId' => $country->id]);

                return '
                    <a href="' . $route . '" class="btn btn-xs btn-primary">
                        <span class="fa fa-globe fa-fw"></span>
                        Bundesländer <span class="badge">' .
                       ($country->states ? $country->states->count() : 0) .
                       '</span>
                    </a>';
            })
            ->addColumn('actions', function($country) {
                $editRoute = route('admin::super:content.countries.edit', ['countryId' => $country->id]);
                $removeRoute = route('admin::super:content.countries.delete', ['countryId' => $country->id]);

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        Bearbeiten
                    </a>
                    <button type="button" data-action="' . $removeRoute . '" data-method="DELETE"
                        class="btn btn-xs btn-danger" data-target="#countries-modal" data-toggle="modal">
                        Löschen
                    </button>
                ';
            })
            ->make(true);
    }
}
