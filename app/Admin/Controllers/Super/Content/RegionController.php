<?php

namespace App\Admin\Controllers\Super\Content;

use App\Admin\Controllers\Super\SuperAdminController;
use App\Models\Location\State;
use App\Models\Location\Region;
use App\Admin\Forms\RegionForm;
use Illuminate\Http\Request;
use App\Admin\Requests\Content\RegionRequest;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App;
use Datatables;
use DB;
use Auth;

class RegionController extends SuperAdminController
{
    use FormBuilderTrait;

    /**
     * Get index page
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $state = $request->query->getInt('stateId', 0);
        $count = $this->getStateRegions($state)->count();

        $stateList = collect([
            '0' => 'Alle',
        ]);

        $countries = State::all()->each(function($state) use ($stateList) {
            $stateList[$state->id] = $state->name;
        });

        $stateList->merge($countries);

        return view('admin.super.content.region.index', compact('stateList', 'count', 'state'));
    }

    /**
     * Get create page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $region = new Region();
        $form = $this->form(RegionForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.regions.store'),
            'model'     => $region,
        ]);

        return view('admin.super.content.region.edit', compact('form', 'region'));
    }

    /**
     * Get edit page
     *
     * @param $regionId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($regionId)
    {
        $region = Region::findOrFail($regionId);

        $form = $this->form(RegionForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.regions.update', ['regionId' => $region->id]),
            'model'     => $region,
        ]);

        return view('admin.super.content.region.edit', compact('form', 'region'));
    }

    /**
     * Form request to create
     *
     * @param RegionRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postCreate(RegionRequest $request)
    {
        $region = new Region();
        $form = $this->form(RegionForm::class, ['model' => $region]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $regionId = $this->create($request, $region);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.regions.edit', ['regionId' => $regionId]);
    }

    /**
     * Form request to update
     *
     * @param RegionRequest $request
     * @param               $regionId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postUpdate(RegionRequest $request, $regionId)
    {
        $region = Region::findOrFail($regionId);

        $form = $this->form(RegionForm::class, ['model' => $region]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $this->update($request, $region);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.regions.edit', ['regionId' => $region->id]);
    }

    /**
     * Create new region
     *
     * @param   RegionRequest $request
     * @param   Region        $region
     * @return  int
     * @throws  \Exception
     * @internal param LandingPage $lp
     */
    private function create(RegionRequest $request, Region $region)
    {
        $region->fill($request->all());

        // State
        $state = $request->request->get('state', null) ?: null;
        $region->state()->associate($state);

        foreach ($request->request->get('data', []) as $newItem) {
            $locale = $newItem['locale'];
            $region->translateOrNew($locale)->name = $newItem['name'];
            $region->translateOrNew($locale)->description = trim($newItem['description']);
        }

        $region->save();

        return $region->id;
    }

    /**
     * Update existing region
     *
     * @param RegionRequest   $request
     * @param Region          $region
     * @throws \Exception
     */
    private function update(RegionRequest $request, Region $region)
    {
        $region->fill($request->all());

        // State
        $state = $request->request->get('state', null) ?: null;
        $region->state()->associate($state);

        $existingData = $region->translations()->get();
        $newData = $request->request->get('data', []);

        // Delete translations which did not come from request
        foreach ($existingData as $existingItem) {
            $toDelete = true;
            foreach ($newData as $newItem) {
                if ($newItem['locale'] == $existingItem['locale'] && $newItem['name'] == $existingItem['name']) {
                    $toDelete = false;
                }
            }
            if ($toDelete) {
                $existingItem->delete();
            }
        }

        foreach ($newData as $newItem) {
            $locale = $newItem['locale'];
            $region->translateOrNew($locale)->name = $newItem['name'];
            $region->translateOrNew($locale)->description = trim($newItem['description']);
        }

        $region->save();
    }

    /**
     * Delete region page
     *
     * @param $regionId
     */
    public function delete($regionId)
    {
        $region = Region::findOrFail($regionId);
        $region->delete();
    }

    /**
     * Get region data
     *
     * @param $regionId
     * @return mixed
     */
    public function show($regionId)
    {
        return Region::findOrFail($regionId)->toArray();
    }

    /**
     * Search region
     *
     * @param RegionRequest $request
     * @return mixed
     */
    public function anyData(RegionRequest $request)
    {
        $state = $request->query->getInt('stateId', 0);
        $regions = $this->getStateRegions($state)
            ->join('region_translations', 'regions.id', '=', 'region_translations.region_id')
            // ->where('region_translations.locale', '=', App::getLocale());
            ->groupBy('region_translations.region_id');

        return Datatables::of($regions)
            ->filter(function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('region_translations.name', 'like', '%'.$request->search['value'].'%');
                }
            })
            ->editColumn('name', function($region) {
                $editRoute = route('admin::super:content.regions.edit', ['regionId' => $region->id]);
                return '<a href="' . $editRoute . '">' . $region->name . '</a>';
            })
            ->editColumn('state', function($region) {
                $editRoute = route('admin::super:content.states.edit', ['stateId' => $region->state_id]);
                return '<a href="' . $editRoute . '">' . $region->state->name . '</a>';
            })
            ->addColumn('cities', function ($region) {
                $route = route('admin::super:content.cities.index', ['regionId' => $region->id]);

                return '
                    <a href="' . $route . '" class="btn btn-xs btn-primary">
                        <span class="fa fa-globe fa-fw"></span>
                        Städte <span class="badge">' .
                       ($region->cities ? $region->cities->count() : 0) .
                       '</span>
                    </a>';
            })
            ->addColumn('actions', function($region) {
                $editRoute = route('admin::super:content.regions.edit', ['regionId' => $region->id]);
                $removeRoute = route('admin::super:content.regions.delete', ['regionId' => $region->id]);

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        Bearbeiten
                    </a>
                    <button type="button" data-action="' . $removeRoute . '" data-method="DELETE"
                        class="btn btn-xs btn-danger" data-target="#regions-modal" data-toggle="modal">
                        Löschen
                    </button>
                ';
            })
            ->make(true);
    }

    /**
     * Return query of regions
     *
     * @param $state
     * @return mixed
     */
    private function getStateRegions($state)
    {
        $query = Region::with(['translations', 'state'])
            ->select('regions.*');

        if ($state > 0) {
            $query->where('regions.state_id', '=', $state);
        }

        return $query;
    }
}
