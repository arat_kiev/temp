<?php

namespace App\Admin\Controllers\Super\Content;

use App\Admin\Controllers\Super\SuperAdminController;
use App\Models\Area\Rule;
use App\Models\Location\Country;
use App\Models\Location\State;
use App\Admin\Forms\StateForm;
use App\Api1\Controllers\StateController as ApiStateController;
use App\Models\Picture;
use Illuminate\Http\Request;
use App\Admin\Requests\Content\StateRequest;
use Illuminate\Http\UploadedFile;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Datatables;
use DB;
use Slugify;

class StateController extends SuperAdminController
{
    use FormBuilderTrait;

    /**
     * Get index page
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $country = $request->query->getInt('countryId', 0);
        $count = $this->getCountryStates($country)->count();

        $countryList = collect([
            '0' => 'Alle',
        ]);

        $countries = Country::all()->each(function($country) use ($countryList) {
            $countryList[$country->id] = $country->name;
        });

        $countryList->merge($countries);

        return view('admin.super.content.state.index', compact('countryList', 'count', 'country'));
    }

    /**
     * Get create page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $state = new State();
        $form = $this->form(StateForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.states.store'),
            'model'     => $state,
        ]);

        return view('admin.super.content.state.edit', compact('form', 'state'));
    }

    /**
     * Get edit page
     *
     * @param $stateId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($stateId)
    {
        $state = State::findOrFail($stateId);

        $form = $this->form(StateForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.states.update', ['stateId' => $state->id]),
            'model'     => $state,
        ]);

        return view('admin.super.content.state.edit', compact('form', 'state'));
    }

    /**
     * Form request to create
     *
     * @param StateRequest  $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postCreate(StateRequest $request)
    {
        $state = new State();
        $form = $this->form(StateForm::class, ['model' => $state]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $stateId = $this->create($request, $state);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.states.edit', ['stateId' => $stateId]);
    }

    /**
     * Form request to update
     *
     * @param StateRequest  $request
     * @param               $stateId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postUpdate(StateRequest $request, $stateId)
    {
        $state = State::findOrFail($stateId);

        $form = $this->form(StateForm::class, ['model' => $state]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $this->update($request, $state);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.states.edit', ['stateId' => $state->id]);
    }

    /**
     * Create new state
     *
     * @param   StateRequest  $request
     * @param   State         $state
     * @return  int
     * @throws  \Exception
     * @internal param LandingPage $lp
     */
    private function create(StateRequest $request, State $state)
    {
        $state->fill($request->all());

        // Country
        $country = $request->request->get('country', null) ?: null;
        $state->country()->associate($country);

        // Rules
        $this->saveStateRules($state, $request);

        foreach ($request->request->get('data', []) as $newItem) {
            $locale = $newItem['locale'];
            $state->translateOrNew($locale)->name = $newItem['name'];
            $state->translateOrNew($locale)->description = trim($newItem['description']);
        }

        $state->save();

        return $state->id;
    }

    /**
     * Update existing state
     *
     * @param StateRequest    $request
     * @param State           $state
     * @throws \Exception
     */
    private function update(StateRequest $request, State $state)
    {
        $state->fill($request->all());

        // Country
        $country = $request->request->get('country', null) ?: null;
        $state->country()->associate($country);

        // Rules
        $this->saveStateRules($state, $request);

        $existingData = $state->translations()->get();
        $newData = $request->request->get('data', []);

        // Delete translations which did not come from request
        foreach ($existingData as $existingItem) {
            $toDelete = true;
            foreach ($newData as $newItem) {
                if ($newItem['locale'] == $existingItem['locale'] && $newItem['name'] == $existingItem['name']) {
                    $toDelete = false;
                }
            }
            if ($toDelete) {
                $existingItem->delete();
            }
        }

        foreach ($newData as $newItem) {
            $locale = $newItem['locale'];
            $state->translateOrNew($locale)->name = $newItem['name'];
            $state->translateOrNew($locale)->description = trim($newItem['description']);
        }

        $state->save();
    }

    private function saveStateRules(State $state, StateRequest $request)
    {
        $rule = $state->rule ?: Rule::create([]);

        $rule->text = $request->request->get('rule_text');

        foreach ($request->request->get('rule_files_delete', []) as $fileId) {
            $rule->files()->detach($fileId);
        }

        // rule files
        $this->processRuleFiles(
            $state,
            $rule,
            $request->file('rule_files', []),
            $request->get('rule_files_name', [])
        );

        if ($rule->isEmpty()) {
            $rule->delete();
            $state->rule()->dissociate();
        } else {
            $rule->save();
            $state->rule()->associate($rule);
        }
    }

    /**
     * Process rule files
     *
     * @param State $state
     * @param Rule $rule
     * @param UploadedFile[]|null $files
     * @param string[]|null $fileNames
     */
    private function processRuleFiles(State $state, Rule $rule, array $files, $fileNames)
    {
        foreach ($files as $file) {
            if ($file != null) {
                $currentNewName = $fileNames[$file->getClientOriginalName()];

                $name = !empty($currentNewName) ? $currentNewName : $state->name.'-bestimmungen';
                $slug = Slugify::slugify($name, '-');

                $rule->files()->save(new Picture(['file' => $file, 'name' => $slug]), ['public' => true]);
            }
        }
    }

    /**
     * Delete state page
     *
     * @param $stateId
     */
    public function delete($stateId)
    {
        $state = State::findOrFail($stateId);
        $state->delete();
    }

    /**
     * Get state data
     *
     * @param $stateId
     * @return mixed
     */
    public function show($stateId)
    {
        return State::findOrFail($stateId)->toArray();
    }

    public function getAllStates(Request $request)
    {
        $request->query->set('detailed', true);
        return with(new ApiStateController(new State()))->index($request);
    }

    /**
     * Search state
     *
     * @param StateRequest $request
     * @return mixed
     */
    public function anyData(StateRequest $request)
    {
        $country = $request->query->getInt('countryId', 0);
        $states = $this->getCountryStates($country)
            ->join('state_translations', 'states.id', '=', 'state_translations.state_id')
            // ->where('state_translations.locale', '=', App::getLocale());
            ->groupBy('state_translations.state_id');

        return Datatables::of($states)
            ->filter(function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('state_translations.name', 'like', '%'.$request->search['value'].'%');
                }
            })
            ->editColumn('name', function($state) {
                $editRoute = route('admin::super:content.states.edit', ['stateId' => $state->id]);
                return '<a href="' . $editRoute . '">' . $state->name . '</a>';
            })
            ->editColumn('country', function($state) {
                $editRoute = route('admin::super:content.countries.edit', ['countryId' => $state->country_id]);
                return '<a href="' . $editRoute . '">' . $state->country->name . '</a>';
            })
            ->addColumn('regions', function ($state) {
                $route = route('admin::super:content.regions.index', ['stateId' => $state->id]);

                return '
                    <a href="' . $route . '" class="btn btn-xs btn-primary">
                        <span class="fa fa-globe fa-fw"></span>
                        Bezirke <span class="badge">' .
                       ($state->regions ? $state->regions->count() : 0) .
                       '</span>
                    </a>';
            })
            ->addColumn('fishes', function ($state) {
                $route = route('admin::super:content.states.fishes.index', ['stateId' => $state->id]);

                return '
                    <a href="' . $route . '" class="btn btn-xs btn-primary">
                        <span class="fa fa-picture-o fa-fw"></span>
                        Fische <span class="badge">' .
                       ($state->fishes ? $state->fishes->count() : 0) .
                       '</span>
                    </a>';
            })
            ->addColumn('actions', function($state) {
                $editRoute = route('admin::super:content.states.edit', ['stateId' => $state->id]);
                $removeRoute = route('admin::super:content.states.delete', ['stateId' => $state->id]);

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        Bearbeiten
                    </a>
                    <button type="button" data-action="' . $removeRoute . '" data-method="DELETE"
                        class="btn btn-xs btn-danger" data-target="#states-modal" data-toggle="modal">
                        Löschen
                    </button>
                ';
            })
            ->make(true);
    }

    /**
     * Return query of states
     *
     * @param $country
     * @return mixed
     */
    private function getCountryStates($country)
    {
        $query = State::with(['translations', 'country'])
            ->select('states.*');

        if ($country > 0) {
            $query->where('states.country_id', '=', $country);
        }

        return $query;
    }
}
