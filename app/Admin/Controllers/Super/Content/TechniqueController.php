<?php

namespace App\Admin\Controllers\Super\Content;

use App\Admin\Controllers\Super\SuperAdminController;
use App\Models\Meta\Technique;
use App\Models\Picture;
use App\Admin\Forms\TechniqueForm;
use App\Admin\Requests\Content\TechniqueRequest;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Illuminate\Http\Request;
use App;
use Auth;
use Datatables;
use DB;

class TechniqueController extends SuperAdminController
{
    use FormBuilderTrait;

    private $techniqueRepository;

    public function __construct(Technique $techniqueRepository)
    {
        parent::__construct();

        $this->techniqueRepository = $techniqueRepository->includeDraft();
    }

    /**
     * Get index page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.super.content.technique.index');
    }

    /**
     * Get create page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $technique = new Technique();
        $form = $this->form(TechniqueForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.techniques.store'),
            'model'     => $technique,
        ]);

        return view('admin.super.content.technique.edit', compact('form', 'technique'));
    }

    /**
     * Get edit page
     *
     * @param $techniqueId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($techniqueId)
    {
        $technique = $this->techniqueRepository->findOrFail($techniqueId);

        $form = $this->form(TechniqueForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.techniques.update', ['techniqueId' => $technique->id]),
            'model'     => $technique,
        ]);

        return view('admin.super.content.technique.edit', compact('form', 'technique'));
    }

    /**
     * Form request to create
     *
     * @param TechniqueRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postCreate(TechniqueRequest $request)
    {
        $technique = new Technique();
        $form = $this->form(TechniqueForm::class, ['model' => $technique]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $techniqueId = $this->create($request, $technique);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.techniques.edit', ['techniqueId' => $techniqueId]);
    }

    /**
     * Form request to update
     *
     * @param TechniqueRequest  $request
     * @param                   $techniqueId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postUpdate(TechniqueRequest $request, $techniqueId)
    {
        $technique = $this->techniqueRepository->findOrFail($techniqueId);

        $form = $this->form(TechniqueForm::class, ['model' => $technique]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $this->update($request, $technique);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.techniques.edit', ['techniqueId' => $technique->id]);
    }

    /**
     * Create new technique
     *
     * @param   TechniqueRequest $request
     * @param   Technique        $technique
     * @return  int
     * @throws  \Exception
     * @internal param LandingPage $lp
     */
    private function create(TechniqueRequest $request, Technique $technique)
    {
        $technique->fill($request->all());

        foreach ($request->request->get('data', []) as $newItem) {
            $locale = $newItem['locale'];
            $technique->translateOrNew($locale)->name = $newItem['name'];
            $technique->translateOrNew($locale)->description = trim($newItem['description']);
        }

        // Picture
        if ($request->hasFile('picture')) {
            $picture = new Picture();
            $picture->fill([
                'file'  => $request->file('picture'),
                'name'  => $request->file('picture')->getClientOriginalName(),
            ]);
            $picture->author()->associate(Auth::user());
            $picture->save();
            $technique->picture()->associate($picture);
        }

        $technique->save();

        return $technique->id;
    }

    /**
     * Update existing technique
     *
     * @param TechniqueRequest  $request
     * @param Technique         $technique
     * @throws \Exception
     */
    private function update(TechniqueRequest $request, Technique $technique)
    {
        $technique->fill($request->all());

        $existingData = $technique->translations()->get();
        $newData = $request->request->get('data', []);

        // Delete translations which did not come from request
        foreach ($existingData as $existingItem) {
            $toDelete = true;
            foreach ($newData as $newItem) {
                if ($newItem['locale'] == $existingItem['locale'] && $newItem['name'] == $existingItem['name']) {
                    $toDelete = false;
                }
            }
            if ($toDelete) {
                $existingItem->delete();
            }
        }

        foreach ($newData as $newItem) {
            $locale = $newItem['locale'];
            $technique->translateOrNew($locale)->name = $newItem['name'];
            $technique->translateOrNew($locale)->description = trim($newItem['description']);
        }

        // Picture
        if ($request->hasFile('picture')) {
            $oldPictureId = $technique->picture ? $technique->picture->id : null;
            $picture = new Picture();
            $picture->fill([
                'file'  => $request->file('picture'),
                'name'  => $request->file('picture')->getClientOriginalName(),
            ]);
            $picture->author()->associate(Auth::user());
            $picture->save();
            $technique->picture()->associate($picture);
            $technique->save();
            if ($oldPictureId) {
                Picture::find($oldPictureId)->delete();
            }
        }

        $technique->save();
    }

    /**
     * Delete technique page
     *
     * @param $techniqueId
     */
    public function delete($techniqueId)
    {
        $technique = $this->techniqueRepository->findOrFail($techniqueId);
        $technique->delete();
    }

    /**
     * publish or unpublish
     * @param $techniqueId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function publish($techniqueId, Request $request)
    {
        $technique = $this->techniqueRepository->findOrFail($techniqueId);
        $technique->draft($request->get('drafted'));
            return response()->json();
    }

    /**
     * Get technique data
     *
     * @param $techniqueId
     * @return mixed
     */
    public function show($techniqueId)
    {
        return Technique::findOrFail($techniqueId)->toArray();
    }

    /**
     * Search technique
     *
     * @param TechniqueRequest $request
     * @return mixed
     */
    public function anyData(TechniqueRequest $request)
    {
        $techniques = Technique::select(['techniques.id', 'gallery_id'])
            ->join('technique_translations', 'techniques.id', '=', 'technique_translations.technique_id')
            // ->where('technique_translations.locale', '=', App::getLocale());
            ->groupBy('technique_translations.technique_id');

        return Datatables::of($techniques)
            ->filter(function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('technique_translations.name', 'like', '%'.$request->search['value'].'%');
                }
            })
            ->editColumn('name', function($technique) {
                $editRoute = route('admin::super:content.techniques.edit', ['techniqueId' => $technique->id]);
                return '<a href="' . $editRoute . '">' . $technique->name . '</a>';
            })
            ->addColumn('gallery', function ($technique) {
                $route = route('admin::super:content.techniques.gallery.list', ['techId' => $technique->id]);

                return '
                    <a href="' . $route . '" class="btn btn-xs btn-primary">
                        <span class="fa fa-picture-o fa-fw"></span>
                        Bilder <span class="badge">' .
                       ($technique->gallery ? $technique->gallery->pictures()->count() : 0) .
                       '</span>
                    </a>';
            })
            ->addColumn('actions', function($technique) {
                $editRoute = route('admin::super:content.techniques.edit', ['techniqueId' => $technique->id]);
                $removeRoute = route('admin::super:content.techniques.delete', ['techniqueId' => $technique->id]);

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        Bearbeiten
                    </a>
                    <button type="button" data-action="' . $removeRoute . '" data-method="DELETE"
                        class="btn btn-xs btn-danger" data-target="#techniques-modal" data-toggle="modal">
                        Löschen
                    </button>
                ';
            })
            ->make(true);
    }
}
