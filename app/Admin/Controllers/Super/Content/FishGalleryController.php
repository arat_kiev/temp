<?php

namespace App\Admin\Controllers\Super\Content;

use App\Admin\Controllers\Super\SuperAdminController;
use App\Admin\Forms\PictureForm;
use App\Admin\Requests\StorePictureRequest;
use App\Api1\Requests\ImageUploadRequest;
use App\Models\Gallery;
use App\Models\Meta\Fish;
use App\Models\Picture;
use Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class FishGalleryController extends SuperAdminController
{
    use FormBuilderTrait;

    public function getList($fishId)
    {
        $fish = Fish::findOrFail($fishId);
        $gallery = $fish->gallery;

        return view('admin.super.content.fish.gallery.list', compact('gallery', 'fish'));
    }

    public function getEdit($fishId, $pictureId)
    {
        $fish = Fish::findOrFail($fishId);

        if (!$fish->gallery || !($picture = $fish->gallery->pictures()->find(['id' => $pictureId])->first())) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->form(PictureForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.fishes.gallery.save', [
                'fishId'    => $fishId,
                'pictureId' => $pictureId,
            ]),
            'model'     => $picture,
        ]);

        return view('admin.super.content.fish.gallery.edit', compact('fish', 'picture', 'form'));
    }

    public function postSave(StorePictureRequest $request, $fishId, $pictureId)
    {
        $fish = Fish::findOrFail($fishId);

        if (!($gallery = $fish->gallery) || !($picture = $gallery->pictures()->find(['id' => $pictureId])->first())) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->form(PictureForm::class, ['model' => $picture]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        if ($request->get('priority') == 5) {
            $fish->gallery->pictures()
                ->wherePivot('priority', '=', 5)
                ->each(function (Picture $picture) use ($gallery) {
                    $gallery->pictures()->updateExistingPivot($picture->id, ['priority' => 4], false);
                });
        }

        $gallery->pictures()->updateExistingPivot($picture->id, ['priority' => $request->get('priority')], false);
        $picture->update($request->only(['name', 'description']));

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.fishes.gallery.list', ['fishId' => $fishId]);
    }

    public function getDelete($fishId, $pictureId)
    {
        /** @var Fish $fish */
        $fish = Fish::findOrFail($fishId);

        if (!($gallery = $fish->gallery) || !($picture = $gallery->pictures()->find(['id' => $pictureId])->first())) {
            throw new AccessDeniedHttpException();
        }

        $picture->delete();

        flash()->success('Bild wurde gelöscht.');

        return redirect()->route('admin::super:content.fishes.gallery.list', ['fishId' => $fishId]);
    }

    public function postUpload(ImageUploadRequest $request, $fishId)
    {
        /** @var Fish $fish */
        $fish = Fish::findOrFail($fishId);

        $picture = new Picture([
            'file'  => $request->file('file'),
            'name'  => $request->file('file')->getClientOriginalName(),
        ]);
        $picture->author()->associate(Auth::user());

        $gallery = $fish->gallery ?: Gallery::create(['name' => $fish->name]);
        $gallery->pictures()->save($picture);
        $fish->gallery()->associate($gallery);
        $fish->save();

        return response()->json();
    }
}
