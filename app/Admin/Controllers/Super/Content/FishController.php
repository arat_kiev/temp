<?php

namespace App\Admin\Controllers\Super\Content;

use App\Admin\Controllers\Super\SuperAdminController;
use App\Models\Meta\Fish;
use App\Models\Picture;
use App\Admin\Forms\FishForm;
use App\Admin\Requests\Content\FishRequest;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Illuminate\Http\Request;
use App;
use Datatables;
use DB;
use Auth;

class FishController extends SuperAdminController
{
    use FormBuilderTrait;

    private $fishRepository;

    public function __construct(Fish $fishRepository)
    {
        parent::__construct();

        $this->fishRepository = $fishRepository->includeDraft();
    }

    /**
     * Get index page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.super.content.fish.index');
    }

    /**
     * Get create page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $fish = new Fish();
        $form = $this->form(FishForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.fishes.store'),
            'model'     => $fish,
        ]);

        return view('admin.super.content.fish.edit', compact('form', 'fish'));
    }

    /**
     * Get edit page
     *
     * @param $fishId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($fishId)
    {
        $fish = $this->fishRepository->findOrFail($fishId);

        $form = $this->form(FishForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.fishes.update', ['fishId' => $fish->id]),
            'model'     => $fish,
        ]);

        return view('admin.super.content.fish.edit', compact('form', 'fish'));
    }

    /**
     * Form request to create
     *
     * @param FishRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postCreate(FishRequest $request)
    {
        $fish = new Fish();
        $form = $this->form(FishForm::class, ['model' => $fish]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $fishId = $this->create($request, $fish);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.fishes.edit', ['fishId' => $fishId]);
    }

    /**
     * Form request to update
     *
     * @param FishRequest   $request
     * @param               $fishId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postUpdate(FishRequest $request, $fishId)
    {
        $fish = $this->fishRepository->findOrFail($fishId);

        $form = $this->form(FishForm::class, ['model' => $fish]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $fish);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.fishes.edit', ['fishId' => $fish->id]);
    }

    /**
     * Create new fish
     *
     * @param   FishRequest     $request
     * @param   Fish            $fish
     * @return  int
     */
    private function create(FishRequest $request, Fish $fish)
    {
        $fish->fill($request->all());

        // Category
        $category = $request->request->get('category', null) ?: null;
        $fish->category()->associate($category);

        foreach ($request->request->get('data', []) as $newItem) {
            $locale = $newItem['locale'];
            $fish->translateOrNew($locale)->name = $newItem['name'];
            $fish->translateOrNew($locale)->description = trim($newItem['description']);
        }

        // Picture
        if ($request->hasFile('picture')) {
            $picture = new Picture();
            $picture->fill([
                'file'  => $request->file('picture'),
                'name'  => $request->file('picture')->getClientOriginalName(),
            ]);
            $picture->author()->associate(Auth::user());
            $picture->save();
            $fish->picture()->associate($picture);
        }

        $fish->save();

        return $fish->id;
    }

    /**
     * Update existing fish
     *
     * @param FishRequest $request
     * @param Fish        $fish
     */
    private function update(FishRequest $request, Fish $fish)
    {
        $fish->fill($request->all());

        // Category
        $category = $request->request->get('category', null) ?: null;
        $fish->category()->associate($category);

        // Picture
        if ($request->hasFile('picture')) {
            $oldPictureId = $fish->picture ? $fish->picture->id : null;
            $picture = new Picture();
            $picture->fill([
                'file'  => $request->file('picture'),
                'name'  => $request->file('picture')->getClientOriginalName(),
            ]);
            $picture->author()->associate(Auth::user());
            $picture->save();
            $fish->picture()->associate($picture);
            $fish->save();
            if ($oldPictureId) {
                Picture::find($oldPictureId)->delete();
            }
        }

        $existingData = $fish->translations()->get();
        $newData = $request->request->get('data', []);

        // Delete translations which did not come from request
        foreach ($existingData as $existingItem) {
            $toDelete = true;
            foreach ($newData as $newItem) {
                if ($newItem['locale'] == $existingItem['locale'] && $newItem['name'] == $existingItem['name']) {
                    $toDelete = false;
                }
            }
            if ($toDelete) {
                $existingItem->delete();
            }
        }

        foreach ($newData as $newItem) {
            $locale = $newItem['locale'];
            $fish->translateOrNew($locale)->name = $newItem['name'];
            $fish->translateOrNew($locale)->description = trim($newItem['description']);
        }

        $fish->save();
    }

    /**
     * Delete fish page
     *
     * @param $fishId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($fishId)
    {
        $fish = $this->fishRepository->findOrFail($fishId);
        if ($fish->hauls->count()) {
//            return response()->json('Fisch kann nicht entfernt werden, da er mit den Fänge bezieht', 400);
            return response('Fisch kann nicht entfernt werden, da er mit den Fänge bezieht', 400);
        }
        $fish->delete();
    }

    /**
     * publish or unpublish
     * @param $fishId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function publish($fishId, Request $request)
    {
        $fish = $this->fishRepository->findOrFail($fishId);
        $fish->draft($request->get('drafted'));
            return response()->json();
    }

    /**
     * Get fish data
     *
     * @param $fishId
     * @return mixed
     */
    public function show($fishId)
    {
        return Fish::findOrFail($fishId)->toArray();
    }

    /**
     * Search fish
     *
     * @param FishRequest $request
     * @return mixed
     */
    public function anyData(FishRequest $request)
    {
        $fishes = Fish::select([
            'fishes.id',
            'latin',
            'gallery_id',
        ])
            ->join('fish_translations', 'fishes.id', '=', 'fish_translations.fish_id')
            // ->where('fish_translations.locale', '=', App::getLocale());
            ->groupBy('fish_translations.fish_id');

        return Datatables::of($fishes)
            ->filter(function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('fish_translations.name', 'like', '%'.$request->search['value'].'%');
                }
            })
            ->editColumn('name', function($fish) {
                $editRoute = route('admin::super:content.fishes.edit', ['fishId' => $fish->id]);
                return '<a href="' . $editRoute . '">' . $fish->name . '</a>';
            })
            ->addColumn('gallery', function ($fish) {
                $route = route('admin::super:content.fishes.gallery.list', ['fishId' => $fish->id]);

                return '
                    <a href="' . $route . '" class="btn btn-xs btn-primary">
                        <span class="fa fa-picture-o fa-fw"></span>
                        Bilder <span class="badge">' .
                            ($fish->gallery ? $fish->gallery->pictures()->count() : 0) .
                        '</span>
                    </a>';
            })
            ->addColumn('actions', function($fish) {
                $editRoute = route('admin::super:content.fishes.edit', ['fishId' => $fish->id]);
                $removeRoute = route('admin::super:content.fishes.delete', ['fishId' => $fish->id]);

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        Bearbeiten
                    </a>
                    <button type="button" data-action="' . $removeRoute . '" data-method="DELETE"
                        class="btn btn-xs btn-danger" data-target="#fishes-modal" data-toggle="modal">
                        Löschen
                    </button>
                ';
            })
            ->make(true);
    }
}
