<?php

namespace App\Admin\Controllers\Super\Content;

use App\Admin\Controllers\Super\SuperAdminController;
use App\Models\Location\State;
use App\Models\Meta\StateFish;
use App\Admin\Forms\StateFishForm;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Admin\Requests\Content\StateFishRequest;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App;
use DB;
use Datatables;
use Carbon\Carbon;

class StateFishController extends SuperAdminController
{
    use FormBuilderTrait;

    /**
     * Get index page
     *
     * @param $stateId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex($stateId)
    {
        $state = State::findOrFail($stateId);
        $stateFishes = StateFish::where('state_id', '=', $stateId);

        $count = $stateFishes->count();
        $stateName = $state->name;

        return view('admin.super.content.state.fish.index', compact('count', 'stateName', 'stateId'));
    }

    /**
     * Get create page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate($stateId)
    {
        $state = State::findOrFail($stateId);
        $stateName = $state->name;

        $stateFish = new StateFish();
        $form = $this->form(StateFishForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.states.fishes.store', ['stateId' => $stateId]),
            'model'     => $stateFish,
        ]);

        return view('admin.super.content.state.fish.edit', compact('form', 'stateFish', 'stateName', 'stateId'));
    }

    /**
     * Get edit page
     *
     * @param $stateId
     * @param $fishId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws ModelNotFoundException
     */
    public function getEdit($stateId, $fishId)
    {
        $state = State::findOrFail($stateId);
        $stateName = $state->name;

        $stateFish = StateFish::where('state_id', '=', $stateId)
            ->where('fish_id', '=', $fishId)
            ->first();

        if (!$stateFish || !$stateFish->count()) {
            throw (new ModelNotFoundException())->setModel(StateFish::class);
        }

        $form = $this->form(StateFishForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:content.states.fishes.update', [
                'stateId'   => $stateId,
                'fishId'    => $fishId
            ]),
            'model'     => $stateFish,
        ]);

        return view('admin.super.content.state.fish.edit', compact('form', 'stateFish', 'stateName', 'stateId'));
    }

    /**
     * Form request to create
     *
     * @param StateFishRequest $request
     * @param                  $stateId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postCreate(StateFishRequest $request, $stateId)
    {
        // Check if input doesn't exists already
        $count = StateFish::where('state_id', '=', $stateId)
            ->where('fish_id', '=', $request->get('fish', null))
            ->count();

        if ($count) {
            return redirect()->back()->withErrors('Entry already exists')->withInput();
        }

        $stateFish = new StateFish();
        $form = $this->form(StateFishForm::class, ['model' => $stateFish]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $this->update($request, $stateFish, $stateId);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.states.fishes.edit', [
            'stateId'   => $stateId,
            'fishId'    => $stateFish->fish_id,
        ]);
    }

    /**
     * Form request to update
     *
     * @param StateFishRequest  $request
     * @param                   $stateId
     * @param                   $fishId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postUpdate(StateFishRequest $request, $stateId, $fishId)
    {
        $stateFish = StateFish::where('state_id', '=', $stateId)
            ->where('fish_id', '=', $fishId)
            ->first();

        if (!$stateFish || !$stateFish->count()) {
            throw (new ModelNotFoundException())->setModel(StateFish::class);
        }

        $form = $this->form(StateFishForm::class, ['model' => $stateFish]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::beginTransaction();
        try {
            $this->update($request, $stateFish, $stateId);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:content.states.fishes.edit', [
            'stateId'   => $stateId,
            'fishId'    => $stateFish->fish_id,
        ]);
    }

    /**
     * Add data to state fish
     *
     * @param StateFishRequest $request
     * @param StateFish        $stateFish
     * @param                  $stateId
     * @internal param StateFish $state
     */
    private function update(StateFishRequest $request, StateFish $stateFish, $stateId)
    {
        $requestData = [];
        foreach ($request->all() as $key => $item) {
            $requestData[$key] = $item ?: null;
        }
        $requestData['closed_from'] = $requestData['closed_from']
            ? Carbon::parse($requestData['closed_from'])->format('Y-m-d')
            : null;
        $requestData['closed_till'] = $requestData['closed_till']
            ? Carbon::parse($requestData['closed_till'])->format('Y-m-d')
            : null;;
        $stateFish->fill($requestData);

        $stateFish->fish()->associate($request->get('fish', null));
        $stateFish->state()->associate($stateId);
        $stateFish->save();
    }

    /**
     * Delete state fish
     *
     * @param $stateId
     * @param $fishId
     */
    public function delete($stateId, $fishId)
    {
        $stateFish = StateFish::where('state_id', '=', $stateId)
            ->where('fish_id', '=', $fishId);
        $stateFish->delete();
    }

    /**
     * Search country
     *
     * @param Request $request
     * @return mixed
     */
    public function anyData(Request $request, $stateId)
    {
        $fishes = StateFish::where('state_id', '=', $stateId)
            ->select(['state_fishes.*'])
            ->join('fishes', 'fishes.id', '=', 'state_fishes.fish_id')
            ->join('fish_translations', 'fishes.id', '=', 'fish_translations.fish_id')
            ->with(['fish', 'state']);

        return Datatables::of($fishes)
            ->filter(function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('fish_translations.name', 'like', '%'.$request->search['value'].'%');
                }
            })
            ->editColumn('fish', function($fish) {
                $editRoute = route('admin::super:content.fishes.edit', ['fishId' => $fish->fish_id]);
                return '<a href="' . $editRoute . '">' . $fish->fish->name . '</a>';
            })
            ->editColumn('closed_from', function($fish) {
                return $fish->closed_from
                    ? $fish->closed_from->format('d.m.Y')
                    : null;
            })
            ->editColumn('closed_till', function($fish) {
                return $fish->closed_till
                    ? $fish->closed_till->format('d.m.Y')
                    : null;
            })
            ->addColumn('actions', function($fish) {
                $editRoute = route('admin::super:content.states.fishes.edit', [
                    'stateId'   => $fish->state_id,
                    'fishId'    => $fish->fish_id,
                ]);
                $removeRoute = route('admin::super:content.states.fishes.delete', [
                    'stateId'   => $fish->state_id,
                    'fishId'    => $fish->fish_id,
                ]);

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        Bearbeiten
                    </a>
                    <button type="button" data-action="' . $removeRoute . '" data-method="DELETE"
                        class="btn btn-xs btn-danger" data-target="#fishes-modal" data-toggle="modal">
                        Löschen
                    </button>
                ';
            })
            ->make(true);
    }
}
