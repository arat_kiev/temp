<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\ResellerForm;
use App\Admin\Requests\AssignAdminsRequest;
use App\Admin\Requests\AssignManagersRequest;
use App\Admin\Requests\StoreResellerRequest;
use App\Api1\Controllers\ManagerController as ApiManagerController;
use App\Api1\Controllers\ProductController as ApiProductController;
use App\Models\Contact\Manager;
use App\Models\Contact\Reseller;
use App\Models\Product\Product;
use App\Models\User;
use Datatables;
use Html;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Api1\Controllers\UserController as ApiUserController;

class ResellerController extends SuperAdminController
{
    use FormBuilderTrait;

    public function getIndex()
    {
        return view('admin.super.reseller.index');
    }

    public function getEdit($resellerId)
    {
        $reseller = Reseller::findOrFail($resellerId);

        $form = $this->form(ResellerForm::class, [
            'method' => 'POST',
            'url' => route('admin::super:resellers.update', ['resellerId' => $reseller->id]),
            'model' => $reseller,
        ]);

        return view('admin.super.reseller.edit', compact('form', 'reseller'));
    }

    public function getCreate()
    {
        $reseller = new Reseller();

        $form = $this->form(ResellerForm::class, [
            'method' => 'POST',
            'url' => route('admin::super:resellers.store'),
            'model' => $reseller,
        ]);

        return view('admin.super.reseller.edit', compact('form', 'reseller'));
    }

    public function postUpdate(StoreResellerRequest $request, $resellerId)
    {
        $reseller = Reseller::findOrFail($resellerId);

        $form = $this->form(ResellerForm::class, ['model' => $reseller]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $reseller);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:resellers.edit', ['resellerId' => $reseller->id]);
    }

    public function postStore(StoreResellerRequest $request)
    {
        $reseller = new Reseller();

        $form = $this->form(ResellerForm::class, ['model' => $reseller]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $reseller);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:resellers.edit', ['resellerId' => $reseller->id]);
    }

    private function update(StoreResellerRequest $request, Reseller $reseller)
    {
        $reseller->fill($request->all());
        $reseller->country()->associate($request->request->get('country'));
        $reseller->save();
    }

    public function getAdmins(Request $request, $resellerId)
    {
        $reseller = Reseller::findOrFail($resellerId);

        $adminIds = $reseller->admins()->pluck('users.id')->toArray();
        $request->query->set('user_id', $adminIds);
        $request->query->set('_perPage', count($adminIds));

        return with(new ApiUserController(new User()))->index($request);
    }

    public function postAdmins(AssignAdminsRequest $request, $resellerId)
    {
        $reseller = Reseller::findOrFail($resellerId);

        $admins = $request->request->get('admins', []);
        $reseller->admins()->sync($admins);

        flash()->success('Admins erfolgreich gespeichert');
        return redirect()->route('admin::super:resellers.index');
    }

    public function getManagers(Request $request, $resellerId)
    {
        $reseller = Reseller::findOrFail($resellerId);

        $managerIds = $reseller->managers()->pluck('managers.id')->toArray();
        $request->query->set('manager_id', $managerIds);
        $request->query->set('_perPage', count($managerIds));

        return with(new ApiManagerController(new Manager()))->index($request);
    }

    public function postManagers(AssignManagersRequest $request, $resellerId)
    {
        $reseller = Reseller::findOrFail($resellerId);

        $managers = $request->request->get('managers', []);
        $reseller->managers()->sync($managers);

        flash()->success('Bewirtschafter erfolgreich gespeichert');
        return redirect()->route('admin::super:resellers.index');
    }

    public function getProducts(Request $request, $resellerId)
    {
        $reseller = Reseller::findOrFail($resellerId);

        $productIds = $reseller->products()->pluck('products.id')->toArray();
        $request->query->set('ids', $productIds);
        $request->query->set('_perPage', count($productIds));

        return with(new ApiProductController(new Product()))->index($request);
    }

    public function postProducts(Request $request, $resellerId)
    {
        $reseller = Reseller::findOrFail($resellerId);

        $products = $request->request->get('products', []);
        $reseller->products()->sync($products);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->back();
    }

    public function anyData()
    {
        $resellers = Reseller::select(['resellers.*'])
            ->with(['managers', 'admins'])
            ->groupBy('resellers.id');

        return Datatables::of($resellers)
            ->editColumn('name', function ($reseller) {
                return Html::linkRoute(
                    'admin::super:resellers.edit',
                    str_limit($reseller->name, 100),
                    ['managerId' => $reseller->id]
                );
            })
            ->addColumn('admins', function ($reseller) {
                $adminsRoute = route('admin::super:resellers.admins.index', ['resellerId' => $reseller->id]);
                return '
                    <button type="button" data-action="'.$adminsRoute.'" class="btn btn-xs btn-primary"
                        data-target="#admin-modal" data-toggle="modal">
                        <span class="fa fa-ticket fa-fw"></span>
                        Admins <span class="badge">'.$reseller->admins->count().'</span>
                    </button>
                ';
            })
            ->addColumn('managers', function ($reseller) {
                $managersRoute = route('admin::super:resellers.managers.index', ['resellerId' => $reseller->id]);
                return '
                    <button type="button" data-action="'.$managersRoute.'" class="btn btn-xs btn-primary"
                        data-target="#manager-modal" data-toggle="modal">
                        <span class="fa fa-briefcase fa-fw"></span>
                        Bewirtschafter <span class="badge">'.$reseller->managers->count().'</span>
                    </button>
                ';
            })
            ->addColumn('products', function ($reseller) {
                $productRoute = route('admin::super:resellers.products.index', ['resellerId' => $reseller->id]);

                return '
                    <button type="button"
                            class="btn btn-xs btn-primary"
                            data-action="' . $productRoute . '"
                            data-target="#product-modal"
                            data-toggle="modal">
                        <span class="fa fa-cart-arrow-down fa-fw"></span>
                        Produkte <span class="badge">
                            ' . ($reseller->products ? $reseller->products->count() : 0) . '
                        </span>
                    </button>';
            })
            ->editColumn('updated_at', function ($reseller) {
                return $reseller->updated_at->diffForHumans();
            })
            ->blacklist(['admins', 'managers', 'products'])
            ->make(true);
    }
}
