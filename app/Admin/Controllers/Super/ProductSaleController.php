<?php

namespace App\Admin\Controllers\Super;

use App\Events\ProductSaleStatusChanged;
use App\Managers\Export\ProductSaleExportManager;
use App\Managers\ProductSaleManager;
use App\Models\Contact\Manager;
use App\Models\Contact\Reseller;
use App\Models\Product\ProductSale;
use App\Models\Product\ProductStock;
use App\Models\Ticket\Ticket;
use Carbon\Carbon;
use Datatables;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Event;

class ProductSaleController extends SuperAdminController
{
    private $isStorno = false;

    public function getIndex(Request $request)
    {
        return view('admin.super.productsale.index', $this->indexData($request));
    }

    public function getStornoIndex(Request $request)
    {
        $this->isStorno = true;

        return view('admin.super.productsale.storno.index', $this->indexData($request));
    }

    private function indexData(Request $request)
    {
        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $reseller = $request->query->getInt('reseller', -1);
        $invoiced = $request->query->getInt('invoiced', -1);
        $status = $request->query->get('status', -1 );

        $query = $this->getProductSales($from, $till, $reseller, $status)
            ->when($invoiced === 0, function ($query) {
                return $query->whereNull('hf_invoice_id');
            })
            ->when($invoiced === 1, function ($query) {
                return $query->whereNotNull('hf_invoice_id');
            })
            ->when($status === 'RECEIVED', function ($query) {
                return $query->whereRaw('status = 1');
            })
            ->when($status === 'SHIPPING', function ($query) {
                return $query->whereRaw('status = 2');
            })
            ->when($status === 'SHIPPED', function ($query) {
                return $query->whereRaw('status = 3');
            });

        $count = $query->count();
        $total = $query->sum('product_prices.value');
        $total = number_format($total / 100.0, 2, ',', '.') . ' €';
        $commissionTotal = $query->sum('commission_value');
        $commissionTotal = number_format($commissionTotal / 100.0, 2, ',', '.') . ' €';

        $resellerList = collect([
            '-1'    => 'Alle',
            '0'     => 'Online',
        ]);

        Reseller::orderBy('name')->each(function ($reseller) use ($resellerList) {
            $resellerList->put($reseller->id, $reseller->name);
        });

        return compact('total', 'commissionTotal', 'count', 'from', 'till', 'reseller', 'status' ,'resellerList', 'invoiced');
    }

    public function anyData(Request $request, ProductSaleManager $productSaleManager)
    {
        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $reseller = $request->query->getInt('reseller', -1);
        $invoiced = $request->query->getInt('invoiced', -1);
        $status = $request->query->get('status', -1 );

        $sales = $this->getProductSales($from, $till, $reseller)
            ->when($invoiced === 0, function ($query) {
                return $query->whereNull('hf_invoice_id');
            })
            ->when($invoiced === 1, function ($query) {
                return $query->whereNotNull('hf_invoice_id');
            })
            ->when($status === 'RECEIVED', function ($query) {
                return $query->whereRaw('status = 1');
            })
            ->when($status === 'SHIPPING', function ($query) {
                return $query->whereRaw('status = 2');
            })
            ->when($status === 'SHIPPED', function ($query) {
                return $query->whereRaw('status = 3');
            });

        return Datatables::of($sales)
            ->addColumn('sale_number', function ($sale) use ($productSaleManager) {
                return $productSaleManager->getIdentifier($sale);
            })
            ->editColumn('name', function ($sale) {
                return $sale->product->name;
            })
            ->editColumn('created_at', function ($sale) {
                return $sale->created_at->format("d.m.Y - H:i");
            })
            ->editColumn('price.value', function ($sale) {
                return $sale->price->value . ' €';
            })
            ->addColumn('total_price', function ($sale) {
                return $sale->total_price . ' €';
            })
            ->editColumn('user.full_name', function ($sale) {
                $name = $sale->user
                    ? $sale->user->full_name
                    : $sale->first_name . ' ' . $sale->last_name;
                $email = $sale->user
                    ? $sale->user->email
                    : $sale->email;

                if ($email) {
                    $name .= '<a href="mailto:'.$email.'">
                                <span class="fa fa-envelope fa-fw"></span>
                              </a>';
                }

                return $name;
            })
            ->addColumn('reseller', function ($sale) {
                return $sale->reseller
                    ? str_limit($sale->reseller->name, 30)
                    : ($sale->user ? 'online' : 'System');
            })
            ->addColumn('status', function ($sale) {
                return '
                    <div class="form-group col-sm-9">
                        <select class="form-control statuses" style="width:100%" data-id="'.$sale->id.'">
                            <option value="RECEIVED" '.($sale->status == "RECEIVED" ? "selected" : "").'>
                                Eingegangen
                            </option>
                            <option value="SHIPPING" '.($sale->status == "SHIPPING" ? "selected" : "").'>
                                In Bearbeitung
                            </option>
                            <option value="SHIPPED" '.($sale->status == "SHIPPED" ? "selected" : "").'>
                                Versendet
                            </option>
                        </select>
                    </div>
                    <button class="btn btn-default status-change disabled" data-id="'.$sale->id.'" disabled>
                        Ändern
                    </button>
                ';
            })
            ->addColumn('actions', function ($sale) {
                $removeRoute = route('admin::super:sales.product.storno', ['saleId' => $sale->id]);
                $previewRoute = route('preview.product', ['product' => $sale->id, 'format' => 'pdf']);

                return '
                    <button type="button"
                            class="btn btn-xs btn-danger"
                            data-target="#delete-modal"
                            data-toggle="modal"
                            data-action="' . $removeRoute . '">
                        Löschen<span class="fa fa-trash-o fa-fw"></span>
                    </button>
             
               <a href="' . $previewRoute . '"
                       type="button"
                       class="btn btn-xs btn-warning"
                       target="_blank">
                       PDF
                        <span class="fa fa-file-archive-o fa-fw"></span>
                    </a>
                ';
            })
            ->addColumn('invoiced', function ($sale) {
                return $sale->hf_invoice_id ?
                    '<span class="fa fa-check fa-fw"></span>' :
                    '<span class="fa fa-times fa-fw"></span>';
            })
            ->blacklist(['user.full_name', 'reseller', 'actions', 'stocks', 'status'])
            ->make(true);
    }

    public function anyStornoData(Request $request, ProductSaleManager $productSaleManager)
    {
        $this->isStorno = true;

        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $reseller = $request->query->getInt('reseller', -1);

        $sales = $this->getProductSales($from, $till, $reseller);

        return Datatables::of($sales)
            ->addColumn('sale_number', function ($sale) use ($productSaleManager) {
                return $productSaleManager->getIdentifier($sale);
            })
            ->editColumn('created_at', function ($sale) {
                return $sale->created_at->format("d.m.Y - H:i");
            })
            ->editColumn('price.value', function ($sale) {
                return $sale->price->value . ' €';
            })
            ->addColumn('total_price', function ($sale) {
                return $sale->total_price . ' €';
            })
            ->editColumn('user.full_name', function ($sale) {
                $name = $sale->user
                    ? $sale->user->full_name
                    : $sale->first_name . ' ' . $sale->last_name;
                $email = $sale->user
                    ? $sale->user->email
                    : $sale->email;

                if ($email) {
                    $name .= '<a href="mailto:'.$email.'">
                                <span class="fa fa-envelope fa-fw"></span>
                              </a>';
                }

                return $name;
            })
            ->addColumn('reseller', function ($sale) {
                return $sale->reseller
                    ? str_limit($sale->reseller->name, 30)
                    : ($sale->user ? 'online' : 'System');
            })
            ->editColumn('storno_date', function ($sale) {
                return $sale->created_at->format("d.m.Y - H:i");
            })
            ->addColumn('attachment', function ($sale) {
                $previewRoute = route('imagecache', ['template' => 'download', 'filename' => $sale->product->attachment]);
                $downloadBtn = '
                    <a href="' . $previewRoute . '"
                       type="button"
                       class="btn btn-xs btn-warning"
                       target="_blank">
                        <span class="fa fa-file-archive-o fa-fw"></span>
                    </a>
                ';

                return $sale->product->attachment ? $downloadBtn : '';
            })
            ->blacklist(['user.full_name', 'reseller'])
            ->make(true);
    }

    private function getProductSales($from = null, $till = null, $reseller = -1)
    {
        $query = ProductSale::with(['product', 'user', 'reseller', 'price', 'country'])
            ->join('product_prices', 'product_prices.id', '=', 'product_sales.price_id')
            ->select('product_sales.*');

        if ($this->isStorno) {
            // show storned ticket
            $query->whereNotNull('storno_id');
            $query->withTrashed();
        } else {
            // show active ticket
            $query->whereNull('storno_id');
            $query->where('is_deleted', false);
        }

        if ($from && $till) {
            try {
                $from = Carbon::createFromFormat('Y-m-d', $from)->startOfDay();
                $till = Carbon::createFromFormat('Y-m-d', $till)->endOfDay();

                $query->whereBetween('product_sales.created_at', [$from, $till]);
            } catch (\InvalidArgumentException $e) {
            }
        }

        if ($reseller >= 0) {
            if ($reseller === 0) {
                $query->has('user');
            } else {
                $query->where('reseller_id', '=', $reseller);
            }
        }

        return $query;
    }

    public function getExport(ProductSaleExportManager $exportManager, Request $request, $format = 'xls')
    {
        if ($format != 'xls') {
            throw new \Exception('Invalid format parameter');
        }

        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $reseller = $request->query->getInt('reseller', -1);
        $invoiced = $request->query->getInt('invoiced', -1);

        $productSales = $this->getProductSales($from, $till, $reseller)
            ->when($invoiced === 0, function ($query) {
                return $query->whereNull('hf_invoice_id');
            })
            ->when($invoiced === 1, function ($query) {
                return $query->whereNotNull('hf_invoice_id');
            })
            ->orderBy('product_sales.created_at');

        $file = $exportManager->run($productSales, $format, compact('from', 'till', 'reseller'));

        return response()->download($file);
    }

    public function postStornoProductSale($saleId, Request $request, ProductSaleManager $saleManager) {
        /** @var ProductSale $sale */
        if (!$sale = ProductSale::find($saleId)) {
            return response()->json([
                'notification' => 'danger',
                'message' => 'Produkte Verkäufe wurde nicht gefunden',
            ]);
        }

        try {
            $saleManager->createStornoSale($sale, $request->get('reason', ''));

            return response()->json([
                'notification' => 'success',
                'message' => 'Produkte Verkäufe wurde gelöscht',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'notification' => 'danger',
                'message' => 'Produkte Verkäufe konnte nicht gelöscht werden',
            ]);
        }
    }

    public function postUpdateStatus($saleId, Request $request)
    {
        /** @var ProductSale $sale */
        if ($sale = ProductSale::find($saleId)) {
            $status = $request->request->get('status', 'RECEIVED');
            if (!in_array($status, ProductSale::ALLOWED_STATUSES)) {
                return response()->json([
                    'notification' => 'danger',
                    'message' => 'Bestellstatus nicht erlaubt',
                ]);
            } elseif ($status === 'SHIPPING' && $sale->product->productStocks()->count()) {
                try {
                    (new ProductSaleManager())->shipProduct($request, $sale);
                } catch (HttpResponseException $e) {
                    return $e->getResponse();
                } catch (\Exception $e) {
                    $stock = ProductStock::where('stock_id', $request->request->getInt('stock'))->first();
                    $stock->quantity += $sale->quantity;
                    $stock->save();

                    $responseData = ['notification' => 'danger', 'message' => 'Es ist ein Fehler aufgetreten.'];
                    return response()->json($responseData);
                }
            }
            $sale->status = $status;
            $sale->save();

            // Fire email to product purchaser email
            Event::fire(new ProductSaleStatusChanged($sale, $status));
            $result = ['notification' => 'success', 'message' => 'Status wurde geändert'];
        } else {
            $result = ['notification' => 'danger', 'message' => 'Bestellung wurde nicht gefunden'];
        }

        return response()->json($result);
    }
}
