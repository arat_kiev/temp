<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\ProductCategoryForm;
use App\Admin\Requests\ProductCategoryRequest;
use App\Models\Picture;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Datatables;
use DB;
use Html;
use Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ProductCategoryController extends SuperAdminController
{
    use FormBuilderTrait;

    public function getIndex(Request $request)
    {
        return view('admin.super.productcategory.index', $this->indexData($request));
    }

    public function getCreate()
    {
        $productCategory = new ProductCategory();

        $form = $form = $this->form(ProductCategoryForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:product_categories.store', ['categoryId' => $productCategory->id]),
            'model'     => $productCategory,
        ]);

        return view('admin.super.productcategory.edit', compact('productCategory', 'form'));
    }

    public function getEdit($categoryId)
    {
        $productCategory = ProductCategory::findOrFail($categoryId);

        $form = $this->form(ProductCategoryForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:product_categories.update', ['categoryId' => $productCategory->id]),
            'model'     => $productCategory,
        ]);

        return view('admin.super.productcategory.edit', compact('productCategory', 'form'));
    }

    /**
     * Get product categories data
     *
     * @param $categoryId
     * @return mixed
     */
    public function show($categoryId)
    {
        return ProductCategory::findOrFail($categoryId)->toArray();
    }

    public function postCreate(ProductCategoryRequest $request)
    {
        $category = new ProductCategory();

        $form = $this->form(ProductCategoryForm::class, ['model' => $category]);

        if ($errorMsg = $this->checkFormValidity($form)) {
            flash()->error($errorMsg);
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $category);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:product_categories.edit', ['categoryId' => $category->id]);
    }

    public function postUpdate(ProductCategoryRequest $request, $categoryId)
    {
        $category = ProductCategory::findOrFail($categoryId);

        $form = $this->form(ProductCategoryForm::class, ['model' => $category]);

        if ($errorMsg = $this->checkFormValidity($form)) {
            flash()->error($errorMsg);
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $category);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::super:product_categories.edit', ['categoryId' => $categoryId]);
    }

    private function checkFormValidity(Form $form)
    {
        switch (true) {
            case !$form->isValid():
                return 'Es ist ein Fehler aufgetreten';
            default:
                return null;
        }
    }

    private function update(ProductCategoryRequest $request, ProductCategory $category)
    {
        DB::transaction(function () use ($request, $category) {
            // Translations
            $this->updateTranslations($request, $category);

            // Picture
            if ($request->hasFile('picture')) {
                $picture = new Picture();
                $picture->fill([
                    'file'  => $request->file('picture'),
                    'name'  => $request->file('picture')->getClientOriginalName(),
                ]);
                $picture->author()->associate(Auth::user());
                $picture->save();
                $category->picture()->associate($picture);
            }

            $category->save();
        });
    }

    private function updateTranslations(ProductCategoryRequest $request, ProductCategory $category)
    {
        $newData = $request->request->get('data', []);
        $newLocales = array_map(function ($item) {
            return $item['locale'];
        }, $newData);
        $category->translations()->whereNotIn('locale', $newLocales)->delete();

        foreach ($newData as $newItem) {
            $locale = $newItem['locale'];
            $category->translateOrNew($locale)->name = $newItem['name'];
            $category->translateOrNew($locale)->short_description = trim($newItem['short_description']);
            $category->translateOrNew($locale)->long_description = trim($newItem['long_description']);
        }
    }

    public function delete($categoryId)
    {
        $category = ProductCategory::findOrFail($categoryId);

        if ($category->products()->count()) {
            throw new AccessDeniedHttpException('Kategorie könnte nicht gelöscht werden');
        }

        $category->delete();
        flash()->success('Kategorie wurde gelöscht');
    }

    public function anyData(Request $request)
    {
        $categories = $this->getProductCategories($request->query->getInt('product', 0))
            ->with(['products']);

        return Datatables::of($categories)
            ->editColumn('name', function ($category) {
                return Html::linkRoute(
                    'admin::super:product_categories.edit',
                    str_limit($category->name, 50),
                    ['categoryId' => $category->id]
                );
            })
            ->addColumn('actions', function ($category) {
                $editRoute = route('admin::super:product_categories.edit', ['categoryId' => $category->id]);
                $removeRoute = route('admin::super:product_categories.delete', ['categoryId' => $category->id]);
                $productsExists = (bool) $category->products()->count();

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        <i class="fa fa-edit fa-fw"></i>
                        Bearbeiten
                    </a>
                    <button type="button"
                            data-action="' . $removeRoute . '"
                            data-method="DELETE"
                            class="btn btn-xs btn-danger'.($productsExists ? ' disabled ' : '').'"
                            data-target="#delete-modal"
                            data-toggle="modal"
                            '.($productsExists ? ' disabled ' : '').'>
                      Löschen<span class="fa fa-trash-o fa-fw"></span>
                    </button>
                ';
            })
            ->blacklist(['actions'])
            ->make(true);
    }

    private function indexData(Request $request) : array
    {
        $product = $request->query->getInt('product', 0);
        $categories = $this->getProductCategories($product);

        $count = $categories->count();

        $productList = collect([
            '0' => 'Alle',
        ]);

        $products = Product::without(['gallery'])
            ->each(function ($product) use ($productList) {
                $productList[$product->id] = $product->name;
            });

        $productList->merge($products);

        return compact('count', 'product', 'productList');
    }

    /**
     * @param int     $productId
     * @return mixed
     */
    private function getProductCategories(int $productId = 0)
    {
        $categories = (new ProductCategory())->newQuery();

        if ($productId > 0) {
            $categories->whereHas('products', function ($query) use ($productId) {
                $query->whereId($productId);
            });
        }

        return $categories;
    }
}
