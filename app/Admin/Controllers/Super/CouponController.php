<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Requests\UpdateCouponCodeRequest;
use App\Models\Promo\Coupon;
use App\Models\Promo\Promotion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Datatables;
use Html;

class CouponController extends SuperAdminController
{
    public function getIndex(Request $request)
    {
        return view('admin.super.coupon.index', $this->indexData($request));
    }

    private function indexData(Request $request) : array
    {
        $promoId = $request->query->getInt('promo', 0);
        $promoType = $request->query->get('promo_type', '');
        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->endOfYear()->format('Y-m-d'));
        $coupons = $this->fetchCoupons($from, $till, $promoType, $promoId);

        $count = $coupons->count();

        $promoTypeList = collect(array_merge(['' => 'Alle', 'NO' => 'Ohne Promotion'], Promotion::AVAILABLE_TYPES));

        return compact('count', 'promoType', 'promoTypeList', 'from', 'till');
    }

    public function show($couponId)
    {
        return response()->json(Coupon::findOrFail($couponId)->toArray());
    }
    
    public function updateCode(UpdateCouponCodeRequest $request, $couponId)
    {
        $coupon = Coupon::findOrFail($couponId);
        $coupon->code = strtoupper($request->request->get('code'));
        $coupon->save();

        return response()->json(['success' => true]);
    }

    public function anyData(Request $request)
    {
        $promoId = $request->query->getInt('promo', 0);
        $promoType = $request->query->get('promo_type', '');
        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->endOfYear()->format('Y-m-d'));
        $coupons = $this->fetchCoupons($from, $till, $promoType, $promoId);

        return Datatables::of($coupons)
            ->orderColumn('bonus', 'usage_bonus $1')
            ->filterColumn('bonus', function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('usage_bonus', 'LIKE', $request->search['value'].'%');
                }
            })
            ->editColumn('code', function (Coupon $coupon) {
                return '
                    <p class="preview-text" data-id="'.$coupon->id.'">
                        '.$coupon->code.'
                        <button data-id="'.$coupon->id.'"
                                class="btn btn-xs btn-default pull-right code-edit"
                                role="button">
                            Bearbeiten<i class="fa fa-edit fa-fw"></i>
                        </button>
                    </p>
                    <form class="aui" data-id="'.$coupon->id.'">
                        <input type="text" name="code" class="code form-control" value="'.$coupon->code.'">
                        <div class="save-options">
                            <button type="submit" class="aui-button submit">
                                <span class="aui-icon aui-icon-small aui-iconfont-success">Save</span>
                            </button>
                            <button type="button" class="aui-button cancel">
                                <span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Cancel</span>
                            </button>
                        </div>
                    </form>';
            })
            ->editColumn('promotion', function (Coupon $coupon) {
                return $coupon->promotion
                    ? Html::linkRoute(
                        'admin::super:promotions.edit',
                        $coupon->promotion->name,
                        ['promoId' => $coupon->promotion->id]
                    )
                    : '<span class="text-danger">Keine</span>';
            })
            ->editColumn('is_public', function (Coupon $coupon) {
                return $coupon->is_public
                    ? '<span class="fa fa-check fa-fw text-success"></span>'
                    : '<span class="fa fa-times fa-fw text-danger"></span>';
            })
            ->addColumn('usages', function (Coupon $coupon) {
                return '
                    <button type="button" class="btn btn-xs btn-primary">
                        <span class="fa fa-exchange fa-fw"></span>
                        Benützt <span class="badge">'.$coupon->usedBy()->count().'</span>
                    </button>
                ';
            })
            ->addColumn('bonus', function (Coupon $coupon) {
                $promo = $coupon->promotion;
                return $promo && $promo->type === 'INVITE'
                    ? 'Eingeladend - ' . $coupon->usage_bonus . ' € (Einladend - ' . $promo->owner_bonus . ' €)'
                    : $coupon->usage_bonus.' €';
            })
            ->addColumn('actions', function (Coupon $coupon) {
                $removeRoute = route('admin::super:coupons.delete', ['couponId' => $coupon->id]);
                $hasUsages = (boolean) $coupon->usedBy()->count();

                return '
                    <button type="button"
                            data-action="' . $removeRoute . '"
                            data-method="DELETE"
                            class="btn btn-xs btn-danger '.($hasUsages ? 'disabled' : '').'"
                            data-target="#delete-modal"
                            data-toggle="modal"
                            '.($hasUsages ? 'disabled' : '').'>
                        Löschen<span class="fa fa-trash-o fa-fw"></span>
                    </button>
                ';
            })
            ->blacklist(['actions', 'usages'])
            ->make(true);
    }

    public function delete($couponId)
    {
        $coupon = Coupon::findOrFail($couponId);
        $hasUsages = (boolean) $coupon->usedBy()->count();

        if ($hasUsages) {
            return response('Gutschein kann nicht entfernt werden, da sie schon verwendet worden ist', 400);
        }
        $coupon->delete();

        return response()->json(['success' => true]);
    }

    private function fetchCoupons($from = null, $till = null, string $promoType = '', int $promoId = 0)
    {
        $coupons = (new Coupon)->newQuery();

        if ($from && $till) {
            try {
                $from = Carbon::createFromFormat('Y-m-d', $from)->startOfDay();
                $till = Carbon::createFromFormat('Y-m-d', $till)->endOfDay();

                $coupons->where(function ($query) use ($from, $till) {
                    $query
                        ->whereHas('promotion', function ($promotion) use ($from, $till) {
                            $promotion->whereDate('valid_from', '<=', $till);
                            $promotion->where(function ($q) use ($from) {
                                $q->whereDate('valid_till', '>=', $from)
                                    ->orWhereNull('valid_till');
                            });
                        })
                        ->doesntHave('promotion', 'OR');
                });
            } catch (\InvalidArgumentException $e) {
            }
        }

        if ($promoType) {
            if ($promoType === 'NO') {
                $coupons->doesntHave('promotion');
            } else {
                $coupons->whereHas('promotion', function ($promotion) use ($promoType) {
                    $promotion->where('type', $promoType);
                });
            }
        }

        if ($promoId) {
            $coupons->whereHas('promotion', function ($promotion) use ($promoId) {
                $promotion->whereId($promoId);
            });
        }

        return $coupons;
    }
}
