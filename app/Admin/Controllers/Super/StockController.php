<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\StockForm;
use App\Admin\Requests\StockRequest;
use App\Models\Location\Country;
use App\Models\Stock;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Datatables;
use Html;
use DB;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class StockController extends SuperAdminController
{
    use FormBuilderTrait;

    public function getIndex(Request $request)
    {
        return view('admin.super.stock.index', $this->indexData($request));
    }

    public function getCreate()
    {
        $stock = new Stock();

        $form = $this->form(StockForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:stocks.store', ['stockId' => $stock->id]),
            'model'     => $stock,
        ]);

        return view('admin.super.stock.edit', compact('stock', 'form'));
    }

    public function getEdit($stockId)
    {
        $stock = Stock::findOrFail($stockId);

        $form = $this->form(StockForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:stocks.update', ['stockId' => $stock->id]),
            'model'     => $stock,
        ]);

        return view('admin.super.stock.edit', compact('stock', 'form'));
    }

    /**
     * Get stock data
     *
     * @param $stockId
     * @return mixed
     */
    public function show($stockId)
    {
        return Stock::findOrFail($stockId)->toArray();
    }

    public function postCreate(StockRequest $request)
    {
        $stock = new Stock();

        $form = $this->form(StockForm::class, ['model' => $stock]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $stock);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:stocks.edit', ['stockId' => $stock->id]);
    }

    public function postUpdate(StockRequest $request, $stockId)
    {
        $stock = Stock::findOrFail($stockId);

        $form = $this->form(StockForm::class, ['model' => $stock]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $stock);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:stocks.edit', ['stockId' => $stockId]);
    }

    private function update(StockRequest $request, Stock $stock)
    {
        DB::transaction(function () use ($request, $stock) {
            // Stock country
            $stock->country()->associate($request->request->getInt('country'));

            // General data
            $stock->fill([
                'name'      => $request->request->get('name'),
                'city'      => $request->request->get('city'),
                'street'    => $request->request->get('street'),
                'building'  => $request->request->get('building'),
                'rest_info' => $request->request->get('rest_info'),
                'emails'    => $request->request->get('emails', []),
                'phones'    => $request->request->get('phones', []),
            ]);

            $stock->save();
        });
    }

    public function delete($stockId)
    {
        $stock = Stock::findOrFail($stockId);

        if ($stock->products()->count()) {
            throw new AccessDeniedHttpException(
                'Lager konnte nicht gelöscht werden, da es noch Produkte enthält'
            );
        }

        $stock->delete();
        flash()->success('Lager wurde gelöscht');
    }

    public function anyData(Request $request)
    {
        $country = $request->query->getInt('country', -1);

        $stocks = $this->getStocks($country)->with(['country']);

        return Datatables::of($stocks)
            ->orderColumn('location', 'city $1')
            ->orderColumn('address', 'street $1')
            ->filterColumn('location', function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('city', 'like', $request->search['value'].'%');
                }
            })
            ->filterColumn('address', function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('street', 'like', $request->search['value'].'%');
                }
            })
            ->editColumn('name', function (Stock $stock) {
                return Html::linkRoute(
                    'admin::super:stocks.edit',
                    str_limit($stock->name, 50),
                    ['stockId' => $stock->id]
                );
            })
            ->addColumn('location', function (Stock $stock) {
                return $stock->city . ', ' . $stock->country->name;
            })
            ->addColumn('address', function (Stock $stock) {
                return $stock->address;
            })
            ->addColumn('products', function (Stock $stock) {
                return '
                    <a   type="button"
                         href="'.route('admin::super:product_stocks.index', ['stock' => $stock->id]).'"
                         class="btn btn-xs btn-primary">
                        <span>Produkte</span>
                        <span class="badge">' . $stock->products()->count() . '</span>
                    </a>
                    <a   type="button"
                         href="' . route('admin::super:product_stocks.create', ['stock'    => $stock->id]) . '"
                         class="btn btn-xs btn-success pull-right"
                         target="_blank">Wareneingang
                        <span class="fa fa-plus-circle fa-fw"></span>
                    </a>
                ';
            })
            ->addColumn('actions', function (Stock $stock) {
                $editRoute = route('admin::super:stocks.edit', ['stockId' => $stock->id]);
                $removeRoute = route('admin::super:stocks.delete', ['stockId' => $stock->id]);
                $productsExists = (bool) $stock->products()->count();

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        <i class="fa fa-edit fa-fw"></i>
                        Bearbeiten
                    </a>
                    <button type="button"
                            data-action="' . $removeRoute . '"
                            data-method="DELETE"
                            class="btn btn-xs btn-danger'.($productsExists ? ' disabled' : '').'"
                            data-target="#delete-modal"
                            data-toggle="modal"'.($productsExists ? ' disabled' : '').'>
                        <span class="fa fa-trash-o fa-fw"></span>
                        Löschen
                    </button>
                ';
            })
            ->blacklist(['actions'])
            ->make(true);
    }

    private function indexData(Request $request) : array
    {
        $country = $request->query->getInt('country', -1);
        $stocks = $this->getStocks($country);

        $count = $stocks->count();

        $countryList = collect([
            '-1'    => 'Alle',
        ]);

        $countries = Country::select(['id'])
            ->has('stocks')
            ->each(function ($country) use ($countryList) {
                $countryList[$country->id] = $country->name;
            });

        $countryList->merge($countries);

        return compact('count', 'country', 'countryList');
    }

    private function getStocks(int $countryId = -1)
    {
        $stocks = (new Stock())->newQuery();

        if ($countryId > 0) {
            $stocks->whereHas('country', function ($query) use ($countryId) {
                $query->whereId($countryId);
            });
        }

        return $stocks;
    }
}
