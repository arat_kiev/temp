<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\RentalForm;
use App\Admin\Requests\AssignAdminsRequest;
use App\Admin\Requests\StoreRentalRequest;
use App\Api1\Controllers\UserController as ApiUserController;
use App\Models\Contact\Rental;
use App\Models\User;
use Datatables;
use Html;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class RentalController extends SuperAdminController
{
    use FormBuilderTrait;

    /** @var Rental */
    private $rentalRepository;

    public function __construct(Rental $rentalRepository)
    {
        parent::__construct();

        $this->rentalRepository = $rentalRepository;
    }

    public function getIndex()
    {
        return view('admin.super.rental.index');
    }

    public function getEdit($rentalId)
    {
        $rental = $this->rentalRepository->findOrFail($rentalId);

        $form = $this->form(RentalForm::class, [
            'method' => 'POST',
            'url' => route('admin::super:rentals.update', ['rentalId' => $rental->id]),
            'model' => $rental,
            'super' => true,
        ]);

        return view('admin.super.rental.edit', compact('form', 'rental'));
    }

    public function getCreate()
    {
        $rental = new Rental();

        $form = $this->form(RentalForm::class, [
            'method' => 'POST',
            'url' => route('admin::super:rentals.store'),
            'model' => $rental,
            'super' => true,
        ]);

        return view('admin.super.rental.edit', compact('form', 'rental'));
    }

    public function postUpdate(StoreRentalRequest $request, $rentalId)
    {
        $rental = $this->rentalRepository->findOrFail($rentalId);

        $form = $this->form(RentalForm::class, ['model' => $rental, 'super' => true]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $rental);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:rentals.edit', ['rentalId' => $rental->id]);
    }

    private function update(StoreRentalRequest $request, Rental $rental)
    {
        $rental->fill($request->all());

        $rental->country()->associate($request->request->get('country'));

        $rental->save();
    }

    public function postStore(StoreRentalRequest $request)
    {
        $rental = new Rental();

        $form = $this->form(RentalForm::class, ['model' => $rental, 'super' => true]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $rental);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:rentals.edit', ['rentalId' => $rental->id]);
    }

    /**
     * Delete action
     *
     * @param $rentalId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function delete($rentalId)
    {
        $rental = $this->rentalRepository->findOrFail($rentalId);
        $rental->delete();
        return response()->json();
    }

    public function getAdmins(Request $request, $rentalId)
    {
        $rental = $this->rentalRepository->findOrFail($rentalId);

        $adminIds = $rental->admins()->pluck('users.id')->toArray();
        $request->query->set('user_id', $adminIds);
        $request->query->set('_perPage', count($adminIds));

        return with(new ApiUserController(new User()))->index($request);
    }

    public function postAdmins(AssignAdminsRequest $request, $rentalId)
    {
        $rental = $this->rentalRepository->findOrFail($rentalId);

        $admins = $request->request->get('admins', []);
        $rental->admins()->sync($admins);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::super:rentals.index');
    }

    public function anyData()
    {
        $rentals = $this->rentalRepository->select([
            'rentals.*',
            \DB::raw('count(rental_admins.user_id) as admin_count')
        ])
            ->leftJoin('rental_admins', 'rentals.id', '=', 'rental_admins.rental_id')
            ->groupBy('rentals.id');

        return Datatables::of($rentals)
            ->editColumn('name', function ($rental) {
                return Html::linkRoute('admin::super:rentals.edit', str_limit($rental->name, 100), ['rentalId' => $rental->id]);
            })
            ->editColumn('admin_count', function ($rental) {
                return '
                    <button type="button" data-action="' . route('admin::super:rentals.admins.index', ['rentalId' => $rental->id]) . '" class="btn btn-xs btn-primary"
                        data-target="#admin-modal" data-toggle="modal">
                        <span class="fa fa-ticket fa-fw"></span>
                        Admins <span class="badge">' . $rental->admin_count . '</span>
                    </button>
                ';
            })
            ->editColumn('updated_at', function ($rental) {
                return $rental->updated_at->diffForHumans();
            })
            ->blacklist(['admin_count'])
            ->make(true);
    }
}
