<?php

namespace App\Admin\Controllers\Super;

use App\Models\Ticket\TicketType;
use App\Api1\Controllers\TicketTypeController as ApiTicketTypeController;
use Illuminate\Http\Request;

class TicketTypeController extends SuperAdminController
{
    public function allTicketTypes(Request $request)
    {
        return with(new ApiTicketTypeController(new TicketType()))->index($request);
    }

    public function delete($priceId)
    {
        $ticketType = TicketType::findOrFail($priceId);

        if ($ticketType->tickets()->count()) {
            return response('Es gibt schon verkaufte Angelkarten mit diesem Typ', 409);
        }
        if ($ticketType->prices()->count()) {
            return response('Es gibt Preise mit diesem Typ', 409);
        }
        $ticketType->delete();

        return response('Success', 200);
    }
}
