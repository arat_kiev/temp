<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\LicenseForm;
use App\Admin\Requests\LicenseRequest;
use App\Admin\Forms\LicenseCreateForm;
use App\Events\LicenseChanged;
use App\Exceptions\ApplicationException;
use App\Models\Client;
use App\Traits\FileTrait;
use Carbon\Carbon;
use Event;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Models\License\License;
use App\Models\License\LicenseType;
use App\Models\Picture;
use Datatables;
use Html;

class LicenseController extends SuperAdminController
{
    use FormBuilderTrait,
        FileTrait;

    public function getIndex(Request $request, FormBuilder $formBuilder)
    {
        $statuses = License::select('status')->distinct()->get();

        $status = $request->query->get('status', 'PENDING');

        $statusesList = [
            'ALL' => 'ALL',
            'PENDING' => 'PENDING'
        ];

        foreach ($statuses as $statusValue) {
            $statusesList[$statusValue->status] = $statusValue->status;
        }

        $form = $formBuilder->create(LicenseCreateForm::class);

        return view('admin.super.license.index', compact('statusesList', 'status', 'form'));
    }

    public function getEdit($licenseId)
    {
        $license = License::findOrFail($licenseId);

        $form = $this->form(LicenseForm::class, [
            'method' => 'POST',
            'url' => route('admin::super:licenses.update', ['licenseId' => $license->id]),
            'model' => $license,
        ]);

        return view('admin.super.license.edit', compact('form', 'license'));
    }

    public function postUpdate(LicenseRequest $request, $licenseId)
    {
        $license = License::findOrFail($licenseId);

        if ($license->type->id !== $request->request->getInt('license_type')) {
            $licenseType = LicenseType::findOrFail($request->request->getInt('license_type'));
            if ($request->request->get('action', 'save') !== 'reject') {
                $this->validateCustomFields($request, $licenseType, $license);
            }
            $license->type()->associate($licenseType);
            $license->attachments()->detach();
            $this->saveAttachments($license, $licenseType, $request);
        } else {
            $licenseType = $license->type;
            if ($request->request->get('action', 'save') !== 'reject') {
                $this->validateCustomFields($request, $licenseType, $license);
            }
            // if there is files in request and they are required by attachments
            if ($request->allFiles() && $this->checkIsAllUplodedFilesAreAttachments($licenseType, $request)) {
                $license->attachments()->detach();
                $this->saveAttachments($license, $licenseType, $request);
            }
        }

        $license->valid_from = !empty($request->get('valid_from'))
            ? Carbon::createFromFormat('d.m.Y', $request->get('valid_from'))
            : null;

        $license->valid_to = !empty($request->get('valid_to'))
            ? Carbon::createFromFormat('d.m.Y', $request->get('valid_to'))
            : null;

        $license->status_text = $request->get('status_text', '');

        $customFields = $request->except(['valid_from', 'valid_to', 'status_text', 'action', 'license_type', '_token']);

        $storeFields = [];

        foreach ($customFields as $field => $value) {
            $fieldIndex = str_replace('_', ' ', $field);
            if (isset($licenseType->fields[$fieldIndex])) {
                if (in_array($licenseType->fields[$fieldIndex], ['date', 'from', 'till'])) {
                    $storeFields[$fieldIndex] = Carbon::createFromFormat('d.m.Y', $value)->format('Y-m-d');
                } else {
                    $storeFields[$fieldIndex] = $value;
                }
            }
        }

        $license->fields = $storeFields;

        switch ($request->get('action', 'save')) {
            case 'save':
                $license->save();
                flash()->success('Daten erfolgreich gespeichert');
                break;

            case 'accept':
                $license->status = 'ACCEPTED';
                $license->save();
                flash()->success('Lizenz wurde bestätigt');
                Event::fire(new LicenseChanged($license, Client::where('name', '=', 'bissanzeiger')->first()));
                break;

            case 'reject':
                $license->status = 'REJECTED';
                $license->save();
                flash()->success('Lizenz wurde abgelehnt');
                Event::fire(new LicenseChanged($license, Client::where('name', '=', 'bissanzeiger')->first()));
                break;
        }

        return redirect()->route('admin::super:licenses.index');
    }

    public function getDelete($licenseId)
    {
        $license = License::findOrFail($licenseId);
        $licenseName = $license->type->name;
        $licenseUser = $license->user->full_name;
        $license->delete();

        flash()->success("Lizenz {$licenseName} (#{$licenseId}) von {$licenseUser} wurde gelöscht");
        return redirect()->back();
    }

    /**
     * Create Licence
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $license = new License;
        $license->user_id = $request->user_id;
        $license->license_type_id = $request->license_type_id;
        $license->status = 'ACCEPTED';

        $licenseType = $this->getLicenseTypeAttributes($request->license_type_id)->getData();

        // Save fields
        if ($licenseType->fields) {
            $fields = [];
            foreach ($licenseType->fields as $field => $type) {
                $fieldName = str_replace(' ', '_', trim($field));
                if ($request->has($fieldName)) {
                    $fieldValue = $request->get($fieldName);
                    switch ($field) {
                        case 'from':
                            $license->valid_from = $fieldValue;
                            break;
                        case 'till':
                            $license->valid_to = $fieldValue;
                            break;
                        default:
                            $fields[$field] = $fieldValue;
                            break;
                    }
                }
            }
            $license->fields = $fields;
        }
        $license->save();

        // Save and attach files
        $this->saveAttachments($license, $licenseType, $request);

        return response()->json();
    }

    private function validateCustomFields(Request $request, LicenseType $licenseType, License $license)
    {
        $requiredFields = array_merge(array_keys($licenseType->fields), $licenseType->attachments);
        $requestParams = array_map(function ($param) {
            return str_replace('_', ' ', trim($param));
        }, array_keys($request->all()));
        $allowedFileTypes = ['image/jpeg', 'image/png', 'application/pdf'];

        $this->validateUploadedFiles($request);

        foreach ($requiredFields as $field) {
            $requestName = str_replace(' ', '_', $field);
            if (!in_array($field, $requestParams) && !$this->checkIsFieldCanBeSkipped($field, $licenseType, $license)) {
                throw new ApplicationException('Missed custom parameters');
            } elseif (!$request->get($requestName)
                      && !$request->hasFile($requestName)
                      && !$this->checkIsFieldCanBeSkipped($field, $licenseType, $license)) {
                throw new ApplicationException('Empty custom parameter - '.$field);
            } elseif ($request->hasFile($requestName)
                      && !in_array($request->file($requestName)->getMimeType(), $allowedFileTypes)) {
                throw new ApplicationException('Only .pdf, .png and .jpg extensions allowed as file extension');
            }
        }
    }

    private function checkIsFieldCanBeSkipped(string $requiredField, LicenseType $licenseType, License $license) : bool
    {
        if (!in_array($requiredField, $licenseType->attachments)) {
            // Cannot be skipped if it's not an attachment
            return false;
        } elseif ($license->type->id === $licenseType->id) {
            // Can be skipped if type wasn't changed and required attachment exists
            return (bool) $licenseType->attachments ? $license->attachments->isNotEmpty() : true;
        } else {
            // Can be skipped if type was changed and attachment not required for this type
            return !in_array($requiredField, $licenseType->attachments);
        }
    }

    private function checkIsAllUplodedFilesAreAttachments(LicenseType $licenseType, Request $request)
    {
        $requestFiles = array_map(function ($item) {
            return str_replace('_', ' ', $item);
        }, array_keys($request->allFiles()));

        return !array_diff($licenseType->attachments, $requestFiles);
    }

    private function saveAttachments(License $license, LicenseType $licenseType, Request $request)
    {
        if ($licenseType->attachments) {
            foreach ($licenseType->attachments as $key => $attach) {
                $attach = strtr(trim($attach), [' ' => '_', '.' => '_']);
                if ($request->hasFile($attach)) {
                    $license->attachments()->attach(Picture::create([
                        'name' => $licenseType->attachments[$key],
                        'file' => $request->file($attach)
                    ]));
                }
            }
        }
    }

    /**
     * Get license type attributes
     * @param $licenseTypeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLicenseTypeAttributes($licenseTypeId)
    {
        $licenseType = LicenseType::where('id', $licenseTypeId)->select('fields', 'attachments')->firstOrFail();

        return response()->json($licenseType);
    }

    public function anyData(Request $request)
    {
        $licenseStatus = $request->query->get('status', 'PENDING');

        $licenses = License::with('user', 'type')
            ->select('licenses.*');

        if ($licenseStatus != 'ALL') {
            $licenses->where('licenses.status', $licenseStatus);
        }

        return Datatables::of($licenses)
            ->filter(function ($query) {
                $search = request('search.value');

                $query->leftJoin('users', 'users.id', '=', 'licenses.user_id');
                $query->leftJoin('license_types', 'license_types.id', '=', 'licenses.license_type_id');
                $query->where(function ($query) use ($search) {
                    $query->where('licenses.id', 'like', "%{$search}%")
                        ->orWhere('users.first_name', 'like', "%{$search}%")
                        ->orWhere('users.last_name', 'like', "%{$search}%")
                        ->orWhere('license_types.name', 'like', "%{$search}%");
                });

                if (request()->has('id')) {
                    $query->where('licenses.id', '=', request('id'));
                }
            })
            ->editColumn('id', function (License $license) {
                return Html::linkRoute(
                    'admin::super:licenses.edit',
                    $license->id,
                    ['licenseId' => $license->id]
                );
            })
            ->addColumn('first_name', function (License $license) {
                return $license->user ? $license->user->first_name : null;
            })
            ->addColumn('last_name', function (License $license) {
                return $license->user ? $license->user->last_name : null;
            })
            ->addColumn('type_name', function (License $license) {
                return Html::linkRoute(
                    'admin::super:licensetypes.edit',
                    str_limit($license->type->name, 100),
                    ['licenseId' => $license->type->id]
                );
            })
            ->editColumn('updated_at', function (License $license) {
                return $license->updated_at ? $license->updated_at->diffForHumans() : null;
            })
            ->addColumn('actions', function (License $license) {
                $editRoute = route('admin::super:licenses.edit', ['licenseId' => $license->id]);
                $delRoute = route('admin::super:licenses.delete', ['licenseId' => $license->id]);
                return '
                    <div class="btn-group">
                        <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                            <span class="fa fa-edit fa-fw"></span> Bearbeiten
                        </a>
                        <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                             <li><a href="' . $delRoute .'" class="btn-danger">
                                 <span class="fa fa-trash-o fa-fw"></span> Löschen
                             </a></li>
                        </ul>
                    </div>
                ';
            })
            ->make(true);
    }
}
