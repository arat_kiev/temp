<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\FeeForm;
use App\Admin\Requests\FeeRequest;
use App\Api1\Controllers\FeeController as ApiFeeController;
use App\Models\Location\Country;
use App\Models\Product\Fee;
use App\Models\Product\FeeCategory;
use App\Models\Product\ProductPrice;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Datatables;
use Html;
use DB;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class FeeController extends SuperAdminController
{
    use FormBuilderTrait;

    public function getIndex(Request $request)
    {
        return view('admin.super.fee.index', $this->indexData($request));
    }

    public function getCreate()
    {
        $fee = new Fee();

        $form = $this->form(FeeForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:fees.store', ['feeId' => $fee->id]),
            'model'     => $fee,
        ]);

        return view('admin.super.fee.edit', compact('fee', 'form'));
    }

    public function getEdit($feeId)
    {
        $fee = Fee::findOrFail($feeId);

        $form = $this->form(FeeForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:fees.update', ['feeId' => $fee->id]),
            'model'     => $fee,
        ]);

        return view('admin.super.fee.edit', compact('fee', 'form'));
    }

    /**
     * Get fee data
     *
     * @param $feeId
     * @return mixed
     */
    public function show($feeId)
    {
        return Fee::findOrFail($feeId)->toArray();
    }

    public function postCreate(FeeRequest $request)
    {
        $fee = new Fee();

        $form = $this->form(FeeForm::class, ['model' => $fee]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $fee);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:fees.edit', ['feeId' => $fee->id]);
    }

    public function postUpdate(FeeRequest $request, $feeId)
    {
        $fee = Fee::findOrFail($feeId);

        $form = $this->form(FeeForm::class, ['model' => $fee]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $fee);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::super:fees.edit', ['feeId' => $feeId]);
    }

    private function update(FeeRequest $request, Fee $fee)
    {
        DB::transaction(function () use ($request, $fee) {
            // Fee category
            $fee->category()->associate($request->request->getInt('category'));

            // Fee country
            $fee->country()->associate($request->request->getInt('country'));

            // General data
            $fee->type = $request->request->get('type', Fee::DEFAULT_TYPE);
            $fee->value = $request->request->get('value', 0);

            $this->updateTranslations($request, $fee);

            $fee->save();
        });
    }

    private function updateTranslations(FeeRequest $request, Fee $fee)
    {
        $newData = $request->request->get('data', []);
        $newLocales = array_map(function ($item) {
            return $item['locale'];
        }, $newData);
        $fee->translations()->whereNotIn('locale', $newLocales)->delete();

        foreach ($newData as $newItem) {
            $locale = $newItem['locale'];
            $fee->translateOrNew($locale)->name = $newItem['name'];
        }
    }

    public function delete($feeId)
    {
        $fee = Fee::findOrFail($feeId);

        if ($fee->prices()->count()) {
            throw new AccessDeniedHttpException('Gebühr kann nicht gelöscht werden');
        }

        $fee->delete();
        flash()->success('Gebühr wurde gelöscht');
    }

    public function getAllFees(Request $request)
    {
        if ($request->query->has('search') && is_array($request->query->get('search'))) {
            $request->query->set('search', $request->query->get('search')['value']);
        }

        return with(new ApiFeeController(new Fee()))->index($request);
    }

    public function anyData(Request $request)
    {
        $category = $request->query->getInt('category', -1);
        $country = $request->query->getInt('country', -1);

        $fees = $this->getFees($category, $country)
            ->select(['fees.id', 'country_id', 'fees.category_id', 'value', 'type'])
            ->leftJoin('fee_translations', 'fee_translations.fee_id', '=', 'fees.id')
            // ->where('fee_translations.locale', '=', \App::getLocale())
            ->with(['country', 'category'])
            ->groupBy('fees.id');

        return Datatables::of($fees)
            ->filterColumn('name', function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('fee_translations.name', 'like', '%'.$request->search['value'].'%');
                }
            })
            ->editColumn('name', function ($fee) {
                return Html::linkRoute(
                    'admin::super:fees.edit',
                    str_limit($fee->name, 50),
                    ['feeId' => $fee->id]
                );
            })
            ->editColumn('value', function ($fee) {
                $mark = $fee->type === 'relative' ? ' %' : ' €';
                return $fee->value . $mark;
            })
            ->editColumn('type', function ($fee) {
                return Fee::AVAILABLE_TYPES[$fee->type] ?? Fee::AVAILABLE_TYPES[Fee::DEFAULT_TYPE];
            })
            ->editColumn('category', function ($fee) {
                return $fee->category ? $fee->category->name : '';
            })
            ->editColumn('country', function ($fee) {
                return $fee->country ? $fee->country->name : '';
            })
            ->addColumn('actions', function ($fee) {
                $editRoute = route('admin::super:fees.edit', ['feeId' => $fee->id]);
                $removeRoute = route('admin::super:fees.delete', ['feeId' => $fee->id]);
                $pricesExists = (bool) $fee->prices()->count();

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        <i class="fa fa-edit fa-fw"></i>
                        Bearbeiten
                    </a>
                    <button type="button"
                            data-action="' . $removeRoute . '"
                            data-method="DELETE"
                            class="btn btn-xs btn-danger'.($pricesExists ? ' disabled' : '').'"
                            data-target="#delete-modal"
                            data-toggle="modal"'.($pricesExists ? ' disabled' : '').'>
                            Löschen
                        <span class="fa fa-trash-o fa-fw"></span>
                    </button>
                ';
            })
            ->blacklist(['actions'])
            ->make(true);
    }

    private function indexData(Request $request) : array
    {
        $category = $request->query->getInt('category', -1);
        $country = $request->query->getInt('country', -1);
        $fees = $this->getFees($category, $country);

        $count = $fees->count();

        $categoryList = collect([
            '-1'    => 'Alle',
        ]);
        $countryList = collect([
            '-1'    => 'Alle',
        ]);

        $categories = FeeCategory::select(['id'])
            ->has('fees')
            ->each(function ($category) use ($categoryList) {
                $categoryList[$category->id] = $category->name;
            });

        $countries = Country::select(['id'])
            ->has('fees')
            ->each(function ($country) use ($countryList) {
                $countryList[$country->id] = $country->name;
            });

        $categoryList->merge($categories);
        $countryList->merge($countries);

        return compact('count', 'category', 'categoryList', 'country', 'countryList');
    }

    private function getFees(int $category = -1, int $country = -1)
    {
        $fees = (new Fee)->newQuery();

        if ($category > 0) {
            $fees->whereHas('category', function ($query) use ($category) {
                $query->where('id', $category);
            });
        }

        if ($country > 0) {
            $fees->whereHas('country', function ($query) use ($country) {
                $query->where('id', $country);
            });
        }

        return $fees;
    }
}
