<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Controllers\AdminController;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Models\Area\Area;
use App\Models\Meta\Fish;
use App\Models\Meta\Technique;
use App\Models\Contact\Manager;
use App\Models\PointOfInterest\PointOfInterest;
use Illuminate\Http\Request;
use Datatables;

class DraftController extends SuperAdminController
{

    use FormBuilderTrait;

    private $draftableModels;

    public function __construct()
    {
        $this->draftableModels = [
            'managers' => [Manager::class, 'admin::super:managers.edit'],
            'pointofinterest' => [PointOfInterest::class, 'admin::super:content.poi.edit'],
            'area' => [Area::class, 'admin::super:areas.edit'],
            'fish' => [Fish::class, 'admin::super:content.fishes.edit'],
            'techniques' => [Technique::class, 'admin::super:content.techniques.edit']
        ];
    }

    /**
     * Display a listing of the resource.
     * @param FormBuilder $formBuilder
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(FormBuilder $formBuilder)
    {
        $types = [['id' => '', 'text' => '']];
        foreach($this->draftableModels as $name => $model) {
            $model = current($model);
            if($model::getDraft()->exists()) {
                $types[] = ['id' => $name, 'text' => class_basename($model)];
            }
        }

        $types = json_encode($types);
            return view('admin.super.draft.index', compact('types'));
    }

    /**
     * Filling DataTable
     * @return mixed
     */
    public function anyData(Request $request)
    {
        $model = strtolower(current($request->route()->parameters('type')));
            if(isset($this->draftableModels[$model])) {
                $data = current($this->draftableModels[$model])::getDraft()->withTranslation();

                return Datatables::of($data)
                    ->editColumn('name', function($data) {
                        return $data->name;
                    })
                    ->editColumn('type', function() use($model) {
                        return class_basename(current($this->draftableModels[$model]));
                    })
                    ->addColumn('action', function ($data) use($model) {
                        return '<a class="btn btn-info btn-xs btn-primary" href="'.route(last($this->draftableModels[$model]), [$model.'Id' => $data->id]).'"><i class="glyphicon glyphicon-edit"></i> Bearbeiten</a>';
                    })
                    ->make(true);
            }
        return false;
    }

}