<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Controllers\AdminController;
use App\Admin\Forms\AreaForm;
use App\Admin\Requests\AssignClientsRequest;
use App\Admin\Requests\StoreAdditionalManagerRequest;
use App\Admin\Requests\StoreAreaRequest;
use App\Api1\Transformers\ClientTransformer;
use App\Api1\Controllers\AreaController as ApiAreaController;
use App\Events\AreaChanged;
use App\Events\AreaCreated;
use App\Models\Area\Area;
use App\Models\Area\Rule;
use App\Models\Client;
use App\Models\Contact\Manager;
use App\Models\Meta\Fish;
use App\Models\Picture;
use App\Models\Ticket\TicketType;
use Auth;
use Carbon\Carbon;
use Datatables;
use Event;
use Exception;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Slugify;

class AreaController extends SuperAdminController
{
    use FormBuilderTrait;

    private $areaRepository;

    public function __construct(Area $areaRepository)
    {
        parent::__construct();

        $this->areaRepository = $areaRepository->includeDraft();
    }

    public function getIndex(Request $request)
    {
        return view('admin.super.area.index', $this->indexData($request, false));
    }

    public function getAffiliated(Request $request)
    {
        return view('admin.super.area.index', $this->indexData($request, true));
    }

    private function indexData(Request $request, bool $children = false)
    {
        $manager = $request->query->getInt('manager', 0);
        $client = $request->query->getInt('client', -1);
        
        $managerList = collect([
            '0'     => 'Alle',
        ]);
        $clientList = collect([
            '-1'    => 'Beliebiger',
            '0'     => 'Ohne Clients',
        ]);

        $managers = Manager::orderBy('name', 'asc')->get()->each(function ($manager) use ($managerList) {
            $managerList[$manager->id] = $manager->name . ' (' . $manager->id . ')';
        });
        $clients = Client::all()->each(function ($client) use ($clientList) {
            $clientList[$client->id] = $client->name;
        });

        $managerList->merge($managers);
        $clientList->merge($clients);

        return compact('children', 'manager', 'managerList', 'client', 'clientList');
    }

    public function getCreate()
    {
        $area = new Area();

        $fishes = $this->getFishList();

        $form = $form = $this->form(AreaForm::class, [
            'method' => 'POST',
            'url'    => route('admin::super:areas.store', ['areaId' => $area->id]),
            'model'  => $area,
        ]);

        return view('admin.super.area.edit', compact('area', 'form', 'fishes'));
    }

    public function postStore(StoreAreaRequest $request)
    {
        $area = new Area();

        $form = $this->form(AreaForm::class, ['model' => $area]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');

            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $area);

        // send admin email about new area
        Event::fire(new AreaCreated($area, Auth::user()));

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:areas.edit', ['areaId' => $area->id]);
    }

    public function getEdit($areaId)
    {
        $area = $this->areaRepository->findOrFail($areaId);

        $fishes = $this->getFishList();

        $form = $this->form(AreaForm::class, [
            'method' => 'POST',
            'url'    => route('admin::super:areas.update', ['areaId' => $area->id]),
            'model'  => $area,
        ]);

        return view('admin.super.area.edit', compact('area', 'form', 'fishes'));
    }

    public function postUpdate(StoreAreaRequest $request, $areaId)
    {
        $area = $this->areaRepository->findOrFail($areaId);

        $form = $this->form(AreaForm::class, ['model' => $area]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');

            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $area);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:areas.edit', ['areaId' => $areaId]);
    }

    public function getAllClients()
    {
        /** @var Builder $clientQuery */
        $clientQuery = Client::where('filter_areas', '=', 1);

        $paginator = $clientQuery->paginate();
        $clients = $paginator->items();

        $data = fractal()
            ->collection($clients, new ClientTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    public function allAreas(Request $request)
    {
        return with(new ApiAreaController(new Area()))->index($request);
    }

    public function getClients($areaId)
    {
        $area =  $this->areaRepository->findOrFail($areaId);

        $clientIds = $area->clients()->pluck('clients.id')->toArray();

        /** @var Builder $clientQuery */
        $clientQuery = Client::where('filter_areas', '=', 1)->whereIn('id', $clientIds);

        $paginator = $clientQuery->paginate();
        $clients = $paginator->items();

        $data = fractal()
            ->collection($clients, new ClientTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();

        return response()->json($data);
    }

    public function postClients(AssignClientsRequest $request, $areaId)
    {
        $area = Area::findOrFail($areaId);

        $clients = $request->request->get('clients', []);
        $area->clients()->sync($clients);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::super:areas.index');
    }

    public function createAdditionalManagers(StoreAdditionalManagerRequest $request, $area_id)
    {
        $percentage = $request->get('percentage') * 100;

        $this->areaRepository->find($area_id)->additionalManagers()->attach($request->get('manager_id'), [
            'public' => $request->get('public'),
            'percentage' => (int)$percentage,
        ]);

            return $this->getAdditionalManagers($area_id);
    }

    public function deleteAdditionalManagers($area_id, $manager_id)
    {
        $this->areaRepository->find($area_id)->additionalManagers()->detach($manager_id);

            return $this->getAdditionalManagers($area_id);
    }

    public function editAdditionalManagers(StoreAdditionalManagerRequest $request, $area_id, $manager_id)
    {
        $percentage = $request->get('percentage') * 100;
        Area::find($area_id)->additionalManagers()->updateExistingPivot($manager_id, [
            'public' => $request->get('public'),
            'percentage' => (int)$percentage,
            'manager_id' => $request->get('manager_id')
        ]);

            return $this->getAdditionalManagers($area_id);
    }

    /**
     * Retrieve data for datatable
     * @param $area_id
     * @return mixed
     */
    public function getAdditionalManagers($area_id)
    {
        $amanagers = Manager::query()->additionalManagersToArea($area_id)->select('id', 'name', 'public', 'percentage');
        $manager = Area::value('manager_id');

        return Datatables::of($amanagers)
            ->editColumn('manager', function ($amanagers) use ($manager) {
                return '<select class="am-list form-control" data-am-id="' . $amanagers->id . '"></select>';
            })
            ->editColumn('public', function ($amanagers) {
                return '<input type="checkbox" class="am-public" ' . ($amanagers->public ? 'checked' : '') . '>';
            })
            ->editColumn('percentage', function ($amanagers) {
                $percentage = $amanagers->percentage / 100;
                    return '<input type="text" 
                                   data-old-value="' . $percentage . '" 
                                   class="am-percentage form-control" 
                                   value="' . $percentage . '">';
            })
            ->addColumn('actions', function ($amanagers) use ($area_id) {
                $editRoute = route('admin::super:edit.areas.additional.managers', ['areaId' => $area_id, 'managerId' => $amanagers->id]);
                $removeRoute = route('admin::super:delete.areas.additional.managers', ['areaId' => $area_id, 'managerId' => $amanagers->id]);
                    return '
                        <button type="button"
                                data-action="' . $editRoute . '"
                                data-method="POST"
                                class="btn btn-xs btn-primary am-edit">
                            Aktualisieren<span class="fa fa-edit fa-fw"></span>
                        </button>
                        <button type="button"
                                data-action="' . $removeRoute . '"
                                data-method="DELETE"
                                class="btn btn-xs btn-danger am-delete">
                            Löschen<span class="fa fa-trash-o fa-fw"></span>
                        </button>';
            })
            ->make(true);
    }

    private function update(StoreAreaRequest $request, Area $area)
    {
        // Standard values
        $area->public = $request->request->getBoolean('public');
        $area->lease = $request->request->getBoolean('lease');
        $area->name = $request->request->get('name');
        $area->description = $request->request->get('description');
        $area->season_begin = $request->request->get('season_begin');
        $area->season_end = $request->request->get('season_end');
        $area->borders = $request->request->get('borders');
        $area->ticket_info = $request->request->get('ticket_info');
        $area->member_only = $request->request->getBoolean('member_only');
        $area->boat = $request->request->getBoolean('boat');
        $area->phone_ticket = $request->request->getBoolean('phone_ticket');
        $area->nightfishing = $request->request->getBoolean('nightfishing');
        $area->rods_max = $request->request->get('rods_max');
        $area->polyfield = $request->request->get('polyfield');
        $area->manager_id = $request->request->get('manager');
        $area->save();

        // Custom tags
        $customTags = $request->request->get('custom_tags', []);

        // Relations
        $area->type()->associate($request->request->getInt('type'));
        $area->techniques()->sync($request->request->get('techniques', []));

        // Fishes and restrictions
        $area->fishes()->detach();
        foreach ($request->request->get('fishes', []) as $fish) {
            $area->fishes()->attach($fish['id'], [
                'min_length' => $fish['min_length'] ?: null,
                'max_length' => $fish['max_length'] ?: null,
                'closed_from' => $fish['closed_from'] ? Carbon::createFromFormat('d.m.Y', $fish['closed_from']) : null,
                'closed_till' => $fish['closed_till'] ? Carbon::createFromFormat('d.m.Y', $fish['closed_till']) : null,
            ]);
        }

        // Geolocations
        $area->locations()->delete();
        foreach ($request->request->get('locations', []) as $locString) {
            $location = $area->locations()->create([]);
            $locString = str_replace(' ', '', $locString);
            list($location->latitude, $location->longitude) = explode(',', $locString);
            $location->save();
        }

        // Rules
        $rule = $area->rule ?: Rule::create([]);

        $rule->text = $request->request->get('rule_text');

        foreach ($request->request->get('rule_files_delete', []) as $fileId) {
            $rule->files()->detach($fileId);
        }

        // non-public rule files
        $this->processRuleFiles(
            $area,
            $rule,
            $request->file('rule_files', []),
            $request->get('rule_files_name', []),
            false
        );

        // public rule files
        $this->processRuleFiles(
            $area,
            $rule,
            $request->file('rule_files_public', []),
            $request->get('rule_public_files_name', []),
            true
        );

        if ($rule->isEmpty()) {
            $rule->delete();
            $area->rule()->dissociate();
        } else {
            $rule->save();
            $area->rule()->associate($rule);
        }

        $area->save();

        $primCity = $cityId = $request->request->getInt('cities');
        Event::fire(new AreaChanged($area, $primCity, $customTags));

        /*if ($request->request->has('cities')) {
            ;
            $area->cities()->updateExistingPivot($cityId, ['primary' => true]);
        }*/
    }

    /**
     * Process rule files
     *
     * @param $area Area
     * @param $rule Rule
     * @param $files UploadedFile[]|null
     * @param $fileNames string[]|null
     * @param $public boolean
     */
    private function processRuleFiles($area, $rule, $files, $fileNames, $public)
    {
        foreach ($files as $file) {
            if ($file != null) {
                $currentNewName = $fileNames[$file->getClientOriginalName()];

                $name = !empty($currentNewName) ? $currentNewName : $area->name.'-bestimmungen';
                $slug = Slugify::slugify($name, '-');

                $rule->files()->save(new Picture(['file' => $file, 'name' => $slug]), ['public' => $public]);
            }
        }
    }

    public function delete($areaId)
    {
        /** @var Area $area */
        $area = $this->areaRepository->findOrFail($areaId);

        try {
            foreach ($area->ticketTypes as $ticketType) {
                /** @var TicketType $ticketType */
                $ticketType->prices()->delete();
                $ticketType->delete();
            }

            $area->delete();
            return response('Gewässer wurde gelöscht.');
        } catch (Exception $e) {
            return response('Gewässer kann nicht gelöscht werden.', 400);
        }
    }

    /**
     * publish or unpublish
     * @param $areaId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function publish($areaId, Request $request)
    {
        $area = $this->areaRepository->findOrFail($areaId);
        $area->draft($request->get('drafted'));
            return response()->json();
    }

    public function anyData(Request $request)
    {
        $manager = $request->query->getInt('manager', 0);
        $client = $request->query->getInt('client', -1);

        $areas = $this->fetchAreas($manager, $client)
            ->join('managers', 'managers.id', '=', 'areas.manager_id')
            ->leftJoin('client_areas', 'client_areas.area_id', '=', 'areas.id')
            ->leftJoin('clients', 'clients.id', '=', 'client_areas.client_id')
            ->groupBy('areas.id');

        return Datatables::of($areas)
            ->filterColumn('manager', function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('managers.name', 'LIKE', "%".$request->search['value']."%");
                }
            })
            ->filterColumn('clients', function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('clients.name', 'LIKE', "%".$request->search['value']."%");
                }
            })
            ->editColumn('name', '
                {{ Html::linkRoute(\'admin::super:areas.edit\', str_limit($name, 50), [\'areaId\' => $id]) }}
            ')
            ->editColumn('public', '
                @if($public)
                    <span class="fa fa-fw fa-check"></span>
                @else
                    <span class="fa fa-fw fa-times"></span>
                @endif
            ')
            ->editColumn('manager', function ($area) {
                if ($area->manager) {
                    return '
                        <span class="badge" data-toggle="tooltip" data-placement="top" title="' . $area->manager->name . '">' .
                        $area->manager->account_number .
                        '</span>
                    ';
                } else {
                    return '';
                }
            })
            ->addColumn('resellers', function ($area) {
                return '
                    <a href="#" class="btn btn-xs btn-primary">
                        <span class="fa fa-shopping-cart fa-fw"></span>
                        Verkaufsstellen <span class="badge">
                            ' . $area->resellers->count() . '
                        </span>
                    </a>';
            })
            ->addColumn('additional_managers', function ($area) {
                return '
                    <button type="button" class="btn btn-xs btn-primary" 
                        data-action="' . route('admin::super:get.areas.additional.managers', ['areaId' => $area->id]) . '" 
                        data-target="#am-modal" data-toggle="modal" data-area-id="' . $area->id . '">
                        <span class="fa fa-briefcase fa-fw"></span>
                        Bewirtschafter <span class="badge"> 
                            ' . $area->additionalManagers->count() . '
                        </span>
                    </button>';
            })
            ->addColumn('ticket_types', function ($area) {
                $route = route('admin::areas.tickettypes.index', ['areaId' => $area->id]);

                return '
                    <a href="' . $route . '" class="btn btn-xs btn-primary">
                        <span class="fa fa-ticket fa-fw"></span>
                        Angelkarten <span class="badge">
                            ' . $area->ticketTypes->count() . '
                        </span>
                    </a>';
            })
            ->addColumn('gallery', function ($area) {
                $route = route('admin::areas.gallery.list', ['areaId' => $area->id]);

                return '
                    <a href="' . $route . '" class="btn btn-xs btn-primary">
                        <span class="fa fa-picture-o fa-fw"></span>
                        Bilder <span class="badge">
                            ' . ($area->gallery ? $area->gallery->pictures()->count() : 0) . '
                        </span>
                    </a>';
            })
            ->addColumn('clients', function ($area) {
                $route = route('admin::super:areas.clients.index', ['areaId' => $area->id]);

                return '
                    <button type="button" data-action="' . $route . '" class="btn btn-xs btn-warning" data-target="#client-modal" data-toggle="modal">
                        <span class="fa fa-sitemap fa-fw"></span>
                        Clients <span class="badge"> 
                            ' . $area->clients->count() . '
                        </span>
                    </button>';
            })
            ->make(true);
    }

    private function fetchAreas(int $manager = 0, int $client = -1)
    {
        $areas = Area::with('resellers', 'manager', 'gallery', 'gallery.pictures', 'clients', 'ticketTypes', 'additionalManagers')
            ->select(['areas.id as id', 'areas.name as name', 'public', 'manager_id', 'gallery_id']);

        if ($manager > 0) {
            $areas->whereHas('manager', function ($query) use ($manager) {
                $query->where('id', $manager);
            });
        }

        if ($client == 0) {
            $areas->doesntHave('clients');
        } elseif ($client > 0) {
            $areas->whereHas('clients', function ($query) use ($client) {
                $query->where('id', $client);
            });
        }

        return $areas;
    }

    private function getFishList()
    {
        return Fish::listsTranslations('name')->orderBy('name')->pluck('name', 'id');
    }
}
