<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\PromotionForm;
use App\Admin\Requests\PromotionRequest;
use App\Api1\Controllers\ProductController as ApiProductController;
use App\Api1\Controllers\TicketTypeController as ApiTicketTypeController;
use App\Exceptions\ApplicationException;
use App\Models\Promo\Promotion;
use App\Models\Product\Product;
use App\Models\Ticket\TicketType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\ViewErrorBag;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Datatables;
use DB;
use CouponManager;

class PromotionController extends SuperAdminController
{
    use FormBuilderTrait;

    public function getIndex(Request $request)
    {
        return view('admin.super.promotion.index', $this->indexData($request));
    }

    public function getCreate()
    {
        $promotion = new Promotion();

        $form = $this->form(PromotionForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:promotions.store', ['promoId' => $promotion->id]),
            'model'     => $promotion,
        ]);

        return view('admin.super.promotion.edit', compact('promotion', 'form'));
    }

    public function getEdit($promoId)
    {
        $promotion = Promotion::findOrFail($promoId);

        $form = $this->form(PromotionForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:promotions.update', ['promoId' => $promotion->id]),
            'model'     => $promotion,
        ]);

        return view('admin.super.promotion.edit', compact('promotion', 'form'));
    }

    public function postCreate(PromotionRequest $request)
    {
        $promo = new Promotion();

        $form = $this->form(PromotionForm::class, ['model' => $promo]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $promo);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:promotions.edit', ['promoId' => $promo->id]);
    }

    public function postUpdate($promoId, PromotionRequest $request)
    {
        $promo = Promotion::findOrFail($promoId);

        $form = $this->form(PromotionForm::class, ['model' => $promo]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $promo);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:promotions.edit', ['promoId' => $promoId]);
    }
    
    private function update(PromotionRequest $request, Promotion $promotion)
    {
        $this->validateAmounts($request, ['usage_bonus', 'owner_bonus'], 0.5);

        DB::transaction(function () use ($request, $promotion) {
            $promotion->type = $type = $request->request->get('type');
            $promotion->valid_from = $request->request->get('valid_from')
                ? Carbon::createFromFormat('d.m.Y H:i', $request->request->get('valid_from'))
                : null;
            $promotion->valid_till = $request->request->get('valid_till')
                ? Carbon::createFromFormat('d.m.Y H:i', $request->request->get('valid_till'))
                : null;
            $promotion->is_active = $request->request->getBoolean('is_active');

            $promotion->usage_bonus = $request->request->get('usage_bonus');
            $promotion->owner_bonus = $type === 'INVITE'
                ? $request->request->get('owner_bonus')
                : '';

            $this->updateTranslations($request, $promotion);

            $promotion->save();

            if (($type === 'SALE' || $type === 'SIGNUP') && !$promotion->coupons()->count()) {
                $promotion->coupons()->create([
                    'code'          => CouponManager::generateCode(),
                    'usage_bonus'   => $promotion->usage_bonus,
                    'is_public'     => true,
                ]);
            } else {
                $promotion->coupons()->update([
                    'usage_bonus'   => $promotion->usage_bonus_origin,
                ]);
            }
        });
    }

    private function validateAmounts(Request $request, array $validateParams = [], float $min = 0)
    {
        $errors = new MessageBag();

        foreach ($validateParams as $param) {
            $value = $request->request->get($param);

            if (!$value) {
                continue;
            }

            $floatedValue = is_string($value)
                ? (float) str_replace(',', '.', $value)
                : (float) $value;

            if ($floatedValue < $min) {
                $errors->add('default', "$param muss grosser als $min sein.");
            }
        }

        if ($errors->has('default')) {
            $errorBag = new ViewErrorBag();
            $errorBag->put('default', $errors);
            $request->session()->put('errors', $errorBag);
            throw new ApplicationException();
        }
    }

    private function updateTranslations(PromotionRequest $request, Promotion $promotion)
    {
        $newData = $request->request->get('data', []);
        $newLocales = array_map(function ($item) {
            return $item['locale'];
        }, $newData);
        $promotion->translations()->whereNotIn('locale', $newLocales)->delete();

        foreach ($newData as $newItem) {
            $locale = $newItem['locale'];
            $promotion->translateOrNew($locale)->name = $newItem['name'];
            $promotion->translateOrNew($locale)->description = trim($newItem['description']);
        }
    }

    public function show($promoId)
    {
        return response()->json(Promotion::findOrFail($promoId)->toArray());
    }

    public function getProducts(Request $request, $promoId)
    {
        $promo = Promotion::findOrFail($promoId);

        $productIds = $promo->products()->pluck('products.id')->toArray();
        $request->query->set('ids', $productIds);
        $request->query->set('_perPage', count($productIds));

        return with(new ApiProductController(new Product()))->index($request);
    }

    public function postProducts(Request $request, $promoId)
    {
        $promo = Promotion::findOrFail($promoId);

        $products = $request->request->get('products', []);
        $promo->products()->sync($products);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->back();
    }

    public function getTicketTypes(Request $request, $promoId)
    {
        $promo = Promotion::findOrFail($promoId);

        $ticketTypesIds = $promo->ticketTypes()->pluck('id')->toArray();
        $request->query->set('ticket_type_id', $ticketTypesIds);
        $request->query->set('_perPage', count($ticketTypesIds));

        return with(new ApiTicketTypeController(new TicketType()))->index($request);
    }

    public function postTicketTypes(Request $request, $promoId)
    {
        $promo = Promotion::findOrFail($promoId);

        $ticketTypes = $request->request->get('ticketTypes', []);
        $promo->ticketTypes()->sync($ticketTypes);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->back();
    }

    public function anyData(Request $request)
    {
        $type = $request->query->get('type', '');
        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->endOfYear()->format('Y-m-d'));
        $promos = $this->fetchPromotions($type, $from, $till);

        return Datatables::of($promos)
            ->orderColumn('status', 'is_active $1')
            ->orderColumn('bonus', 'usage_bonus $1')
            ->filterColumn('bonus', function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('usage_bonus', 'LIKE', $request->search['value'].'%');
                }
            })
            ->addColumn('status', function ($promo) {
                return $promo->deleted_at
                    ? '<button type="button" class="btn btn-xs btn-danger">
                            <span class="fa fa-trash-o fa-fw"></span> Gelöscht
                        </button>'
                    : ($promo->is_active
                        ? '<span class="fa fa-check fa-fw text-success"></span>'
                        : '<span class="fa fa-times fa-fw text-danger"></span>');
            })
            ->editColumn('name', function ($promo) {
                return str_limit($promo->name, 30);
            })
            ->editColumn('type', function ($promo) {
                return Promotion::AVAILABLE_TYPES[$promo->type] ?? '';
            })
            ->editColumn('valid_from', function ($promo) {
                $isActive = $promo->valid_from->lte(Carbon::now());

                return "<span class='" . ($isActive ? 'text-success' : 'text-danger') . "'>
                            {$promo->valid_from->format('d.m.Y H:i')}
                        </span>";
            })
            ->editColumn('valid_till', function ($promo) {
                $isActive = !$promo->valid_till || $promo->valid_till->gt(Carbon::now());

                return $promo->valid_till
                    ? "<span class='". ($isActive ? 'text-success' : 'text-danger') ."'>
                            {$promo->valid_till->format('d.m.Y H:i')}
                        </span>"
                    : "<span class='text-success'>Unbegrenzt</span>";
            })
            ->addColumn('bonus', function ($promo) {
                return $promo->type === 'INVITE'
                    ? 'Eingeladend - ' . $promo->usage_bonus . ' € (Einladend - ' . $promo->owner_bonus . ' €)'
                    : $promo->usage_bonus.' €';
            })
            ->addColumn('coupons', function ($promo) {
                $route = route('admin::super:coupons.index', ['promo' => $promo->id]);
                return '
                    <a href="' . $route . '" class="btn btn-xs btn-primary">
                        Gutscheine <span class="badge">' . $promo->coupons()->count() . '</span>
                    </a>';
            })
            ->addColumn('targets', function ($promo) {
                if ($promo->type === 'SALE') {
                    $productRoute = route('admin::super:promotions.products.index', ['promoId' => $promo->id]);
                    $ticketTypeRoute = route('admin::super:promotions.tickettypes.index', ['promoId' => $promo->id]);
                    return '
                        <button type="button"
                                data-action="'.$ticketTypeRoute.'"
                                class="btn btn-xs btn-primary"
                                data-target="#ticket-types-modal"
                                data-toggle="modal">
                            <span class="fa fa-ticket fa-fw"></span>
                            Angelkarten <span class="badge">'.$promo->ticketTypes()->count().'</span>
                        </button>
                        <button type="button"
                                data-action="'.$productRoute.'"
                                class="btn btn-xs btn-primary"
                                data-target="#products-modal"
                                data-toggle="modal">
                            <span class="fa fa-cart-arrow-down fa-fw"></span>
                            Produkte <span class="badge">'.$promo->products()->count().'</span>
                        </button>
                    ';
                } else {
                    return '';
                }
            })
            ->addColumn('actions', function ($promo) {
                $editRoute = route('admin::super:promotions.edit', ['promoId' => $promo->id]);
                $removeRoute = route('admin::super:promotions.delete', ['promoId' => $promo->id]);
                $hasUsages = false;

                $promo->coupons()->each(function ($coupon) use (&$hasUsages) {
                    $hasUsages = $coupon->usedBy()->count() ? true : $hasUsages;
                });

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        Bearbeiten<span class="fa fa-edit fa-fw"></span>
                    </a>
                    <button type="button"
                            data-action="' . $removeRoute . '"
                            data-method="DELETE"
                            class="btn btn-xs btn-danger '.($hasUsages ? 'disabled' : '').'"
                            data-target="#delete-modal"
                            data-toggle="modal"
                            '.($hasUsages ? 'disabled' : '').'>
                        Löschen<span class="fa fa-trash-o fa-fw"></span>
                    </button>
                ';
            })
            ->blacklist(['actions', 'status', 'targets', 'coupons'])
            ->make(true);
    }

    public function delete($promoId)
    {
        $promo = Promotion::findOrFail($promoId);
        $hasUsages = false;

        $promo->coupons()->each(function ($coupon) use (&$hasUsages) {
            $hasUsages = $coupon->usedBy()->count() ? true : $hasUsages;
        });

        if ($hasUsages) {
            return response('Promotion kann nicht entfernt werden, da sie schon verwendet worden ist', 400);
        }
        $promo->delete();

        return response()->json(['success' => true]);
    }

    private function indexData(Request $request) : array
    {
        $type = $request->query->get('type', '');
        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->endOfYear()->format('Y-m-d'));
        $promos = $this->fetchPromotions($type, $from, $till);

        $count = $promos->count();

        $typeList = collect(array_merge(['' => 'Alle'], Promotion::AVAILABLE_TYPES));

        return compact('count', 'type', 'typeList', 'from', 'till');
    }

    private function fetchPromotions(string $type, $from = null, $till = null)
    {
        $promos = (new Promotion)->newQuery();

        if ($type) {
            $promos->where('type', $type);
        }

        if ($from && $till) {
            try {
                $from = Carbon::createFromFormat('Y-m-d', $from)->startOfDay();
                $till = Carbon::createFromFormat('Y-m-d', $till)->endOfDay();

                $promos->whereDate('valid_from', '<=', $till);
                $promos->where(function ($q) use ($from) {
                    $q->whereDate('valid_till', '>=', $from)
                        ->orWhereNull('valid_till');
                });
            } catch (\InvalidArgumentException $e) {
            }
        }

        return $promos;
    }
}
