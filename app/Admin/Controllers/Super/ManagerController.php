<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\ManagerForm;
use App\Admin\Forms\RKSVForm;
use App\Admin\Requests\AssignAdminsRequest;
use App\Admin\Requests\CreateRKSVRequest;
use App\Admin\Requests\StoreManagerRequest;
use App\Api1\Controllers\ManagerController as ApiManagerController;
use App\Api1\Controllers\UserController as ApiUserController;
use App\CashRegister\Offisy\Client as OffisyClient;
use App\CashRegister\Offisy\Exceptions\RequestFailedException;
use App\Models\Area\Rule;
use App\Models\Contact\Manager;
use App\Models\Picture;
use App\Models\User;
use Carbon\Carbon;
use Datatables;
use Html;
use Http\Client\Exception as HttpException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Slugify;

class ManagerController extends SuperAdminController
{
    use FormBuilderTrait;

    private $managerRepository;

    public function __construct(Manager $managerRepository)
    {
        parent::__construct();

        $this->managerRepository = $managerRepository->includeDraft();
    }

    public function getIndex()
    {
        $rksvForm = $this->form(RKSVForm::class, [
            'method' => 'POST',
        ]);

        return view('admin.super.manager.index', compact('rksvForm'));
    }

    public function getEdit($managerId)
    {
        $manager = $this->managerRepository->findOrFail($managerId);

        $form = $this->form(ManagerForm::class, [
            'method' => 'POST',
            'url' => route('admin::super:managers.update', ['managerId' => $manager->id]),
            'model' => $manager,
            'super' => true,
        ]);

        return view('admin.super.manager.edit', compact('form', 'manager'));
    }

    public function getCreate()
    {
        $manager = new Manager();

        $form = $this->form(ManagerForm::class, [
            'method' => 'POST',
            'url' => route('admin::super:managers.store'),
            'model' => $manager,
            'super' => true,
        ]);

        return view('admin.super.manager.edit', compact('form', 'manager'));
    }

    public function postUpdate(StoreManagerRequest $request, $managerId)
    {
        $manager = $this->managerRepository->findOrFail($managerId);

        $form = $this->form(ManagerForm::class, ['model' => $manager, 'super' => true]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $manager);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:managers.edit', ['managerId' => $manager->id]);
    }

    private function update(StoreManagerRequest $request, Manager $manager)
    {
        $manager->fill($request->except('rule_text', 'rule_files_delete', 'rule_files', 'rule_files_public'));
        $manager->commission_included = $request->request->getBoolean('commission_included');

        // Emails to be notified on storno email event
        $stornoEmails = $request->request->get('storno_emails', []);
        $manager->stornoNotifications()->delete();

        foreach ($stornoEmails as $email) {
            $manager->stornoNotifications()->create([
                'email' => $email,
            ]);
        }

        // Country
        $manager->country()->associate($request->request->get('country'));

        // Logo
        if ($request->hasFile('logo')) {
            $logo = Picture::create(['file' => $request->file('logo')]);
            if ($manager->logo) {
                $manager->logo->delete();
            }
            $manager->logo()->associate($logo);
        }

        // Rules
        $rule = $manager->rule ?: Rule::create([]);

        $rule->text = $request->request->get('rule_text');

        foreach ($request->request->get('rule_files_delete', []) as $fileId) {
            $rule->files()->detach($fileId);
        }

        // non-public rule files
        $this->processRuleFiles(
            $manager,
            $rule,
            $request->file('rule_files', []),
            $request->get('rule_files_name', []),
            false
        );

        // public rule files
        $this->processRuleFiles(
            $manager,
            $rule,
            $request->file('rule_files_public', []),
            $request->get('rule_public_files_name', []),
            true
        );

        if ($rule->isEmpty()) {
            $rule->delete();
            $manager->rule()->dissociate();
        } else {
            $rule->save();
            $manager->rule()->associate($rule);
        }

        $manager->save();
    }

    /**
     * Process rule files
     *
     * @param $manager Manager
     * @param $rule Rule
     * @param $files UploadedFile[]|null
     * @param $fileNames string[]|null
     * @param $public boolean
     */
    private function processRuleFiles($manager, $rule, $files, $fileNames, $public)
    {
        foreach ($files as $file) {
            if ($file != null) {
                $currentNewName = $fileNames[$file->getClientOriginalName()];

                $name = !empty($currentNewName) ? $currentNewName : $manager->name . '-bestimmungen';
                $slug = Slugify::slugify($name, '-');

                $rule->files()->save(new Picture(['file' => $file, 'name' => $slug]), ['public' => $public]);
            }
        }
    }

    public function postStore(StoreManagerRequest $request)
    {
        $manager = new Manager();

        $form = $this->form(ManagerForm::class, ['model' => $manager, 'super' => true]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $manager);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:managers.edit', ['managerId' => $manager->id]);
    }

    /**
     * Delete action
     * @param $managerId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function delete($managerId)
    {
        $manager = $this->managerRepository->findOrFail($managerId);
        $manager->delete();
        return response()->json();
    }

    /**
     * publish or unpublish
     * @param $managerId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function publish($managerId, Request $request)
    {
        $manager = $this->managerRepository->findOrFail($managerId);
        $manager->draft($request->get('drafted'));
        return response()->json();
    }

    public function getManagerData($managerId)
    {
        $manager = Manager::findOrFail($managerId);

        return with(new ApiManagerController(new Manager()))->show($managerId);
    }

    public function getUserData($userId)
    {
        $user = User::findOrFail($userId);

        return with(new ApiUserController(new User()))->show($userId);
    }

    public function getAdmins(Request $request, $managerId)
    {
        $manager = Manager::findOrFail($managerId);

        $adminIds = $manager->admins()->pluck('users.id')->toArray();
        $request->query->set('user_id', $adminIds);
        $request->query->set('_perPage', count($adminIds));

        return with(new ApiUserController(new User()))->index($request);
    }

    public function getLastInvoice($managerId)
    {
        $manager = Manager::findOrFail($managerId);

        $lastInvoice = $manager ? $manager->invoices()->orderBy('created_at', 'desc')->first() : null;

        $from = $lastInvoice ? $lastInvoice->date_till->copy()->addDay() : Carbon::parse('first day of this month');
        $till = Carbon::today();

        return response()->json([
            'from' => $from->toIso8601String(),
            'till' => $till->toIso8601String(),
        ]);
    }

    public function postAdmins(AssignAdminsRequest $request, $managerId)
    {
        $manager = Manager::findOrFail($managerId);

        $admins = $request->request->get('admins', []);
        $manager->admins()->sync($admins);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::super:managers.index');
    }

    public function createRKSV(CreateRKSVRequest $request, $managerId)
    {
        $manager = Manager::findOrFail($managerId);

        if ($manager->offisy_id) {
            flash()->error('Bewirtschafter hat bereits RKSV-Anbindung');
            return redirect()->back();
        }

        try {
            $client = new OffisyClient();

            $company_id = $client->createCompany([
                'name' => $request->get('company_name'),
                'street' => $request->get('company_street'),
                'zip' => $request->get('company_plz'),
                'city' => $request->get('company_city'),
                'country' => strtoupper($request->get('company_country')),
            ])->id();

            $manager->offisy_id = $company_id;

            $client->createUserForCompany([
                'first_name' => $request->get('user_first_name'),
                'last_name' => $request->get('user_last_name'),
                'email' => $request->get('user_email'),
                'company_id' => $company_id,
            ]);

            $manager->save();
            flash()->success('Offisy: Anbindung erstellt. Benutzer erhält in Kürze eine Aktivierungs-Email.');
        } catch (RequestFailedException $e) {
            flash()->error('Offisy: Anbindung fehlgeschlagen. Kontaktieren Sie bitte Mike.');
        } catch (HttpException $e) {
            if ($e->getCode() == 409) {
                flash()->error('Offisy: Company oder User existieren bereits.');
            } else {
                flash()->error('Offisy: Verbindungfehlgeschlagen');
            }
        }

        return redirect()->back();
    }

    public function activateRKSV($managerId)
    {
        $manager = Manager::findOrFail($managerId);
        $manager->rksv_enabled = true;
        $manager->save();

        flash()->success('RKSV wurde aktiviert');
        return redirect()->back();
    }

    public function deactivateRKSV($managerId)
    {
        $manager = Manager::findOrFail($managerId);
        $manager->rksv_enabled = false;
        $manager->save();

        flash()->warning('RKSV wurde deaktiviert');
        return redirect()->back();
    }

    /**
     * Retrieve managers list
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdditionalManagersList()
    {
        $managers = \DB::table('managers')
            ->select('id', 'name AS text')
            ->get();

        return response()->json($managers);
    }

    public function anyData()
    {
        $managers = Manager::select([
            'managers.*',
            \DB::raw('count(manager_admins.user_id) as admin_count')
        ])
            ->leftJoin('manager_admins', 'managers.id', '=', 'manager_admins.manager_id')
            ->groupBy('managers.id');

        return Datatables::of($managers)
            ->editColumn('name', function ($row) {
                return Html::linkRoute('admin::super:managers.edit', str_limit($row['name'], 100), ['managerId' => $row['id']]);
            })
            ->editColumn('admin_count', function ($manager) {
                return '
                    <button type="button" data-action="' . route('admin::super:managers.admins.index', ['managerId' => $manager->id]) . '" class="btn btn-xs btn-primary"
                        data-target="#admin-modal" data-toggle="modal">
                        <span class="fa fa-ticket fa-fw"></span>
                        Admins <span class="badge">' . $manager->admin_count . '</span>
                    </button>
                ';
            })
            ->addColumn('rksv', function ($manager) {
                $buttonClass = $manager->rksv_enabled ? 'btn-success' :
                    ($manager->offisy_id ? 'btn-warning' : 'btn-default');

                $buttonText = $manager->rksv_enabled ? 'Aktiv' :
                    ($manager->offisy_id ? 'Inaktiv' : 'Deaktiviert');

                $buttonIcon = $manager->rksv_enabled ? 'fa-toggle-on' :
                    ($manager->offisy_id ? 'fa-toggle-off' : 'fa-ban');

                $buttonTarget = $manager->offisy_id ? '#activation-modal' : '#rksv-modal';

                $buttonStatus = $manager->rksv_enabled ? 'active' : 'inactive';

                return "
                    <button type='button' class='btn btn-xs {$buttonClass}' 
                        data-manager='{$manager->id}' data-status='{$buttonStatus}'
                        data-target='{$buttonTarget}' data-toggle='modal'>
                        <span class='fa {$buttonIcon} fa-fw'></span> {$buttonText}
                    </button>
                ";
            })
            ->editColumn('updated_at', function ($manager) {
                return $manager->updated_at->diffForHumans();
            })
            ->addColumn('commissions', function ($manager) {
                return '
                    <a class="btn btn-xs btn-primary"
                       data-target="#tickets-modal"
                       data-toggle="modal"
                       data-record="'.$manager->id.'">
                        <i class="fa fa-ticket fa-fw"></i> Angelkarten
                    </a>
                    <a class="btn btn-xs btn-warning"
                       data-target="#products-modal"
                       data-toggle="modal"
                       data-record="'.$manager->id.'">
                        <i class="fa fa-cart-arrow-down fa-fw"></i> Produkte
                    </a>
                ';
            })
            ->addColumn('links', function ($manager) {
                return '
                    <div class="btn-group">
                        <a type="button"
                            href="' . route('admin::super:invoices.create', ['manager' => $manager->id]) . '"
                            class="btn btn-xs btn-primary"
                            target="_blank">
                            <span class="fa fa-plus-circle fa-fw"></span>
                            Abrechnung anlegen
                        </a>
                        <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu">
                            <li>
                                <a type="button"
                                    href="' . route('admin::super:invoices.index', ['manager' => $manager->id]) . '">
                                    <span class="fa fa-list-alt fa-fw"></span>
                                    Abrechnungen anzeigen
                                </a>
                            </li>
                        </ul>
                    </div>
                ';
            })
            ->blacklist(['admin_count', 'rksv'])
            ->make(true);
    }
}
