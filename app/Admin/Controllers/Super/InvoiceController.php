<?php

namespace App\Admin\Controllers\Super;

use App\Managers\Export\InvoiceExportManager;
use App\Managers\InvoiceManager;
use App\Managers\ProductSaleManager;
use App\Managers\TicketManager;
use App\Models\Contact\Manager;
use App\Models\Product\ProductSale;
use App\Models\Ticket\Invoice;
use App\Models\Ticket\Ticket;
use App\Admin\Forms\SuperInvoiceForm;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Http\Request;
use App\Admin\Requests\SuperInvoiceRequest;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Carbon\Carbon;
use Datatables;
use DB;

class InvoiceController extends SuperAdminController
{
    use FormBuilderTrait;

    private $isStorno = false;
    private $productSales = [];

    private $ticketSelect = [
        "id",
        "manager_ticket_id",
        "user_id",
        "created_at",
        "valid_from",
        "valid_to",
        "client_id",
        "ticket_type_id",
        "ticket_price_id",
        "commission_value",
        "'ticket' AS 'data_type'"
    ];
    private $productSaleSelect = [
        "id",
        "null AS manager_ticket_id",
        "user_id",
        "created_at",
        "null AS valid_from",
        "null AS valid_to",
        "null AS ticket_type_id",
        "null AS ticket_price_id",
        "client_id",
        "commission_value",
        "'product' AS 'data_type'"
    ];

    public function getIndex(Request $request)
    {
        $manager = $request->query->getInt('manager', -1);
        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));

        $managerList = collect([
            '-1' => 'Alle',
        ]);

        $managers = Manager::orderBy('name')->each(function ($manager) use ($managerList) {
            $managerList[$manager->id] = $manager->name;
        });

        $managerList->merge($managers);

        return view('admin.super.invoice.index', compact('manager', 'managerList', 'from', 'till'));
    }

    public function getCreate(Request $request)
    {
        $invoice = new Invoice();
        $manager = $request->query->getInt('manager', -1) > 0
            ? Manager::findOrFail($request->query->get('manager'))
            : null;

        $form = $this->form(SuperInvoiceForm::class, [
            'method' => 'POST',
            'url'    => route('admin::super:invoices.store'),
            'model'  => $invoice,
            'data'   => ['manager' => $manager],
        ]);

        return view('admin.super.invoice.create', compact('form', 'invoice'));
    }

    public function getShow($invoiceId)
    {
        $invoice = Invoice::findOrFail($invoiceId);
        $manager = $invoice->manager;

        $from = Carbon::parse($invoice->date_from)->format('d.m.Y');
        $till = Carbon::parse($invoice->date_till)->format('d.m.Y');
        $createdAt = Carbon::parse($invoice->created_at)->format('d.m.Y H:i');

        return view('admin.super.invoice.show', compact('invoice', 'manager', 'from', 'till', 'createdAt'));
    }

    public function getExport(Request $request, $invoiceId, $format = 'xls', InvoiceExportManager $exportManager)
    {
        if ($format != 'xls') {
            throw new \Exception('Invalid format parameter');
        }

        $from = $request->query->get('from');
        $till = $request->query->get('till');
        $manager = $request->query->getInt('manager');

        $invoice = Invoice::findOrFail($invoiceId);
        $tickets = Ticket::selectRaw(implode(', ', $this->ticketSelect))
            ->with(['type.area.manager', 'price', 'user.country', 'resellerTicket.country', 'hauls'])
            ->where('hf_invoice_id', '=', $invoice->id);
        $productSale = ProductSale::selectRaw(implode(',', $this->productSaleSelect))
            ->with('user')
            ->where('hf_invoice_id', '=', $invoice->id);

        $results = $tickets->unionAll($productSale)->orderBy('id');

        $invoiceManager = new InvoiceManager();

        $invoiceNumber = $invoiceManager->getInvoiceNumber($invoice);

        $file = $exportManager->run($results, $format, compact('from', 'till', 'manager', 'invoiceNumber'));

        return response()->download($file);
    }

    public function postCreate(SuperInvoiceRequest $request)
    {
        $invoice = new Invoice();
        $manager = $request->query->getInt('manager', -1) > 0
            ? Manager::findOrFail($request->query->get('manager'))
            : null;

        $form = $this->form(
            SuperInvoiceForm::class,
            ['model' => $invoice, 'data' => ['manager' => $manager]]
        );

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        try {
            DB::beginTransaction();
            $this->create($request);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            flash()->error($e->getMessage());
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:invoices.show', ['invoiceId' => $invoice->id]);
    }

    private function create(SuperInvoiceRequest $request)
    {
        $dateFrom = Carbon::parse($request->request->get('date_from') ?: 'first day of this month')->toDateString();
        $dateTill = Carbon::parse($request->request->get('date_till') ?: 'last day of this month')->toDateString();

        $tickets = Ticket::whereNull('tickets.hf_invoice_id')
            ->whereNull('tickets.storno_id')
            ->whereDate('tickets.created_at', '>=', $dateFrom)
            ->whereDate('tickets.created_at', '<=', $dateTill)
            ->join('ticket_types', 'tickets.ticket_type_id', '=', 'ticket_types.id')
            ->join('areas', 'ticket_types.area_id', '=', 'areas.id')
            ->join('managers', 'areas.manager_id', '=', 'managers.id')
            ->where('managers.id', '=', $request->get('manager'))
            ->select('tickets.*');

        $productSales = ProductSale::whereNull('product_sales.hf_invoice_id')
            ->whereNull('product_sales.storno_id')
            ->whereDate('product_sales.created_at', '>=', $dateFrom)
            ->whereDate('product_sales.created_at', '<=', $dateTill)
            ->join('products', 'product_sales.product_id', '=', 'products.id')
            ->join('has_products', function ($join) {
                $join->on('has_products.product_id', '=', 'products.id');
                $join->where('has_products.related_type', '=', 'managers');
            })
            ->join('managers', 'has_products.related_id', '=', 'managers.id')
            ->where('managers.id', '=', $request->get('manager'))
            ->select('product_sales.*');

        if (!$tickets->count() && !$productSales->count()) {
            throw new \Exception('Es gibt keine Angelkarten oder Produkte für die Abrechnung');
        }

        $this->createTicketsInvoice($request, $tickets, true);
        $this->createTicketsInvoice($request, $tickets, false);

        $this->createProductsInvoice($request, $productSales, true);
        $this->createProductsInvoice($request, $productSales, false);
    }

    private function createInvoice(SuperInvoiceRequest $request, $isOnline)
    {
        $invoice = new Invoice();
        $dateFrom = Carbon::parse($request->request->get('date_from') ?: 'first day of this month')->toDateString();
        $dateTill = Carbon::parse($request->request->get('date_till') ?: 'last day of this month')->toDateString();

        $invoice->fill([
            'date_from' => $dateFrom,
            'date_till' => $dateTill,
            'online' => $isOnline,
        ]);
        $invoice->manager()->associate($request->request->getInt('manager'));
        $invoice->save();

        return $invoice;
    }

    private function createTicketsInvoice(SuperInvoiceRequest $request, EloquentBuilder $tickets, $isOnline = true)
    {
        $filteredTickets = clone $tickets;

        if ($isOnline) {
            $filteredTickets->doesntHave('resellerTicket');
        } else {
            $filteredTickets->has('resellerTicket');
        }
        
        if ($filteredTickets->count()) {
            $invoice = $this->createInvoice($request, $isOnline);

            foreach ($filteredTickets->get() as $ticket) {
                $ticket->hfInvoice()->associate($invoice->id);
                $ticket->save();
            }
        }
    }

    private function createProductsInvoice(SuperInvoiceRequest $request, EloquentBuilder $sales, $isOnline = true)
    {
        $filteredSales = clone $sales;

        if ($isOnline) {
            $filteredSales->doesntHave('reseller');
        } else {
            $filteredSales->has('reseller');
        }

        if ($filteredSales->count()) {
            $invoice = $this->createInvoice($request, $isOnline);

            foreach ($filteredSales->get() as $sale) {
                $sale->hfInvoice()->associate($invoice->id);
                $sale->save();
            }
        }
    }

    public function anyData(Request $request, InvoiceManager $invoiceManager)
    {
        $manager = $request->query->getInt('manager', -1);
        $from = $request->query->get('from', Carbon::now()->firstOfYear()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));

        $invoices = $this->getManagerInvoices($manager, $from, $till);

        return Datatables::of($invoices)
            ->addColumn('invoice_number', function ($invoice) use ($invoiceManager) {
                return $invoiceManager->getInvoiceNumber($invoice);
            })
            ->addColumn('tickets_count', function ($invoice) {
                return ($invoice->hfTickets->count() + $invoice->hfProductSales->count()) ?: '<span class="fa fa-times fa-fw"></span>';
            })
            ->addColumn('manager', function ($invoice) {
                return str_limit($invoice->manager->name, 30);
            })
            ->editColumn('online', function ($invoice) {
                return $invoice->online ?
                    '<span class="fa fa-check fa-fw"></span>' :
                    '<span class="fa fa-times fa-fw"></span>';
            })
            ->editColumn('date_from', function ($invoice) {
                return $invoice->date_from->format('d.m.Y');
            })
            ->editColumn('date_till', function ($invoice) {
                return $invoice->date_till->format('d.m.Y');
            })
            ->editColumn('created_at', function ($invoice) {
                return $invoice->created_at->format('d.m.Y H:i');
            })
            ->addColumn('actions', function ($invoice) {
                $showRoute = route('admin::super:invoices.show', ['invoiceId' => $invoice->id]);
                $removeRoute = route('admin::super:invoices.storno', ['invoiceId' => $invoice->id]);

                return '
                    <a href="'.route('preview.invoice', ['invoice' => $invoice->id, 'format' => 'pdf']).'"
                       type="button"
                       class="btn btn-xs btn-warning"
                       target="_blank">
                        <span class="fa fa-file-pdf-o fa-fw"></span>
                        PDF-Rechnung
                    </a>
                     <a class="btn btn-xs btn-success" href="' . route('admin::super:invoices.export', [
                        'invoice' => $invoice->id,
                        'format' => 'xls',
                        'from' => $invoice->date_from->format('Y-m-d'),
                        'till' => $invoice->date_till->format('Y-m-d'),
                        'manager' => $invoice->manager_id,
                    ]) . '">
                        <span class="fa fa-file-excel-o fa-fw"></span>XLS-Datei
                    </a>
                    
                    <a href="' . $showRoute . '" class="btn btn-xs btn-default">
                        <span class="fa fa-ticket fa-fw"></span>
                        Detail
                    </a>

                      
                    <button type="button"
                            class="btn btn-xs btn-danger"
                            data-target="#delete-modal"
                            data-toggle="modal"
                            data-action="' . $removeRoute . '">
                        <span class="fa fa-trash-o fa-fw"></span>
                        Storno
                    </button>
                ';
            })
            ->make(true);
    }

    private function getManagerInvoices($managerId = -1, $from = null, $till = null)
    {
        $query = Invoice::select('invoices.*')
            ->join('managers', 'managers.id', '=', 'invoices.manager_id')
            ->whereNull('invoices.reseller_id');

        if ($managerId > 0) {
            $query->where(function($query) use ($managerId) {
                $query->where('managers.id', '=', $managerId)
                    ->orWhere('managers.parent_id', '=', $managerId);
            });
        }

        if ($from && $till) {
            try {
                $from = Carbon::createFromFormat('Y-m-d', $from)->startOfDay();
                $till = Carbon::createFromFormat('Y-m-d', $till)->endOfDay();

                $query->whereBetween('invoices.created_at', [$from, $till]);
            } catch (\InvalidArgumentException $e) {
            }
        }

        if ($this->isStorno) {
            // show storned ticket
            $query->whereNotNull('invoices.storno_id');
            $query->withTrashed();
        } else {
            // show active ticket
            $query->whereNull('invoices.storno_id');
            $query->where('invoices.is_deleted', 0);
        }

        return $query;
    }

    public function invoiceData($invoiceId, TicketManager $ticketManager, ProductSaleManager $productSaleManager)
    {
        $invoice = Invoice::findOrFail($invoiceId);
        $tickets = Ticket::selectRaw(implode(', ', $this->ticketSelect))
            ->with(['type.area.manager', 'price', 'user.country', 'resellerTicket.country', 'hauls'])
            ->where('hf_invoice_id', '=', $invoice->id);
        $productSale = ProductSale::selectRaw(implode(',', $this->productSaleSelect))
            ->with('user')
            ->where('hf_invoice_id', '=', $invoice->id);

        $results = $tickets->unionAll($productSale);

        return Datatables::of($results)
            ->addColumn('manager_ticket_id', function ($ticket) use ($ticketManager) {
                if ($ticket->data_type === 'product') {
                    return null;
                }
                return $ticketManager->getManagerIdentifier($ticket);
            })
            ->addColumn('ticket_number', function($ticket) use ($ticketManager, $productSaleManager) {
                if ($ticket->data_type === 'product') {
                    $productSale = $this->fetchProductSale($ticket->id);
                    return $productSaleManager->getIdentifier($productSale);
                }
                return $ticketManager->getIdentifier($ticket);
            })
            ->addColumn('user_full_name', function ($ticket) {
                if ($ticket->data_type === 'product') {
                    $product = $this->fetchProductSale($ticket->id);
                    list($name, $email) = $this->fetchProductSaleUserData($product);
                } else {
                    list($name, $email) = $this->fetchTicketUserData($ticket);
                }

                if (!empty($email)) {
                    $name .= ' <a href="mailto:'.$email.'">
                                    <span class="fa fa-envelope fa-fw"></span>
                               </a>';
                }

                return $name;
            })
            ->addColumn('area_name', function ($ticket) {
                if ($ticket->data_type === 'product') {
                    $productSale = $this->fetchProductSale($ticket->id);
                    $areas = array_filter($productSale->product->areas()->pluck('name')->toArray(), 'strlen');
                    return str_limit(implode($areas, ', '), 45);
                }
                return str_limit($ticket->type->area->name, 30);
            })
            ->addColumn('type_name', function ($ticket) {
                if ($ticket->data_type === 'product') {
                    return 'Produkte';
                }
                return str_limit($ticket->type->name, 30);
            })
            ->addColumn('valid_from', function ($ticket) use ($ticketManager) {
                if ($ticket->data_type === 'product') {
                    $productSale = $this->fetchProductSale($ticket->id);
                    return $productSale->product->featured_from
                        ? $productSale->product->featured_from->format('d.m.Y')
                        : '';
                }
                $validFrom = $ticketManager->validFrom($ticket->price, $ticket->valid_from);

                return $ticket->price->ticketType->category->type === 'HOUR'
                    ? $validFrom->format('d.m.Y - H:i')
                    : $validFrom->format('d.m.Y');
            })
            ->editColumn('created_at', function ($ticket) {
                return $ticket->created_at->format("d.m.Y - H:i");
            })
            ->addColumn('price_compact', function ($ticket) {
                if ($ticket->data_type === 'product') {
                    $productSale = $this->fetchProductSale($ticket->id);
                    return $productSale->price->value . ' €';
                }
                return $ticket->price->name.' ('.number_format($ticket->price->value / 100.0, 2, ",", ".").' €)';
            })
            ->addColumn('price_brutto', function ($ticket) {
                if ($ticket->data_type === 'product') {
                    $productSale = $this->fetchProductSale($ticket->id);
                    return $productSale->price->value . ' €';
                }
                return number_format($ticket->price->value / 100.0, 2, ",", ".").' €';
            })
            ->addColumn('user', function ($ticket) {
                return $ticket->data_type === 'product'
                    ? $this->fetchProductSale($ticket->id)->user->toArray()
                    : ($ticket->user
                        ? $ticket->user->toArray()
                        : []);
            })
            ->addColumn('price', function ($ticket) {
                return $ticket->data_type === 'product'
                    ? $this->fetchProductSale($ticket->id)->price->toArray()
                    : ($ticket->price
                        ? $ticket->price->toArray()
                        : null);
            })
            ->addColumn('commission', function ($ticket) {
                return $ticket->commission_value . ' €';
            })
            ->addColumn('actions', function ($ticket) {
                $removeRoute = $ticket->data_type === 'product'
                    ? route('admin::sales.product.storno', ['saleId' => $ticket->id])
                    : route('admin::sales.ticket.storno', ['ticketId' => $ticket->id]);
                return '
                    <button type="button"
                            class="btn btn-xs btn-danger"
                            data-target="#delete-modal"
                            data-toggle="modal"
                            data-action="' . $removeRoute . '">
                        <span class="fa fa-trash-o fa-fw"></span>
                    </button>
                ';
            })
            ->blacklist(['manager_ticket_id', 'user_full_name', 'type_area_name', 'valid_from', 'reseller'])
            ->make(true);
    }

    public function postStornoInvoice($invoiceId, Request $request) {
        /** @var Invoice $invoice */
        if ($invoice = Invoice::find($invoiceId)) {
            try {
                DB::beginTransaction();
                Invoice::$stornoReason = $request->get('reason', '');
                $invoice->delete();

                // create storned invoice
                $stornoInvoice = $invoice->replicate(['is_deleted', 'deleted_at', 'deleted_by']);
                $stornoInvoice->storno_id = $invoice->id;
                $stornoInvoice->save();
                DB::commit();
                $result = ['notification' => 'success', 'message' => 'Abrechnung wurde gelöscht'];
            } catch (\Exception $e) {
                DB::rollBack();
                $result = ['notification' => 'danger', 'message' => $e->getMessage()];
            }
        } else {
            $result = ['notification' => 'danger', 'message' => 'Abrechnung wurde nicht gefunden'];
        }

        return response()->json($result);
    }

    private function fetchProductSale($productSaleId)
    {
        if (isset($this->productSales[$productSaleId])) {
            return $this->productSales[$productSaleId];
        }

        $productSale = $this->productSales[$productSaleId] = ProductSale::select([
            'id', 'first_name', 'last_name', 'email', 'user_id', 'reseller_id', 'price_id', 'product_id'
        ])
            ->with(['user', 'reseller', 'price', 'product.areas', 'user.country'])
            ->find($productSaleId);

        return $productSale;
    }

    private function fetchTicketUserData(Ticket $ticket) : array
    {
        $name = $ticket->user
            ? $ticket->user->first_name.' '.$ticket->user->last_name
            : ($ticket->resellerTicket
                ? $ticket->resellerTicket->first_name . ' ' . $ticket->resellerTicket->last_name
                : '');
        $email = $ticket->user
            ? $ticket->user->email
            : ($ticket->resellerTicket
                ? $ticket->resellerTicket->email
                : '');

        return [$name, $email];
    }

    private function fetchProductSaleUserData(ProductSale $productSale) : array
    {
        $name = $productSale->user
            ? $productSale->user->first_name . ' ' . $productSale->user->last_name
            : ($productSale->reseller
                ? $productSale->reseller->name
                : $productSale->first_name . ' ' . $productSale->last_name);
        $email = $productSale->user
            ? $productSale->user->email
            : ($productSale->reseller
                ? $productSale->reseller->email
                : $productSale->email);

        return [$name, $email];
    }
}
