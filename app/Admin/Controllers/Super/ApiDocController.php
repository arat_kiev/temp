<?php

namespace App\Admin\Controllers\Super;

use App\Models\User;
use Carbon\Carbon;
use DOMDocument;
use File;
use Artisan;
use JWTAuth;
use Mpociot\Documentarian\Documentarian;

class ApiDocController extends SuperAdminController
{
    public function getIndex()
    {
        $fileExists = File::exists(storage_path('app/docs/index.html'));
        $lastModified = null;

        if ($fileExists) {
            $fileTimestamp = File::lastModified(storage_path('app/docs/index.html'));
            $lastModified = Carbon::createFromTimestamp($fileTimestamp)->diffForHumans();
        }

        return view('admin.super.apidoc.index', compact('fileExists', 'lastModified'));
    }

    public function refresh()
    {
        $outputPath = storage_path('app/docs');
        $documentarian = new Documentarian();

        if (!is_dir($outputPath) || !is_dir($outputPath . '/source')) {
            $documentarian->create($outputPath);
        }

        $this->generate($outputPath);

        $this->alterFiles();

        return response()->json(['Status' => 1]);
    }

    private function generate($outputPath)
    {
        $user = User::whereHas('roles', function($q) {
            $q->where('name', 'superadmin');
        })->first();

        $token = $user ? JWTAuth::fromUser($user) : '';

        Artisan::call('api:generate', [
            '--routePrefix' => 'v1*',
            '--output'      => $outputPath,
            '--header'      => 'Authorization: Bearer ' . $token,
            '--force'       => true,
        ]);
    }

    private function alterFiles()
    {
        // Edit content of `index.html`
        $apiDocContent = File::get(storage_path('app/docs/index.html'));

        $matches = $this->fetchRelatedDataFromAttributes($apiDocContent);
        $newApiDocContent = $this->replaceHardCodedLinksInApiDoc($matches, $apiDocContent);

        File::put(storage_path('app/docs/index.html'), $newApiDocContent);
    }

    private function fetchRelatedDataFromAttributes($content)
    {
        try {
            $dom = new DOMDocument;
            $dom->loadHTML($content);
        } catch (\Exception $e) {
            throw new \Exception('HTML-Datei konnte nicht gelesen werden. Fehlermeldung: ' . $e->getMessage());
        }

        return $this->findMatchesInAttributes($dom);
    }

    private function findMatchesInAttributes(DOMDocument $dom)
    {
        $matches = [];

        foreach($dom->getElementsByTagName('link') as $element) {
            if ($element->getAttribute('href') && $element->getAttribute('href') != "#") {
                $matches[] = $element->getAttribute('href');
            }
        }

        foreach($dom->getElementsByTagName('a') as $element) {
            if ($element->getAttribute('href') && $element->getAttribute('href') != "#") {
                $matches[] = $element->getAttribute('href');
            }
        }

        foreach($dom->getElementsByTagName('script') as $element) {
            if ($element->getAttribute('src') && $element->getAttribute('src') != "#") {
                $matches[] = $element->getAttribute('src');
            }
        }

        foreach($dom->getElementsByTagName('img') as $element) {
            if ($element->getAttribute('src') && $element->getAttribute('src') != "#") {
                $matches[] = $element->getAttribute('src');
            }
        }

        return $matches;
    }

    private function replaceHardCodedLinksInApiDoc(array $matches, $content)
    {
        $replace = [];

        foreach ($matches as $key => $match) {
            if (strpos($match, 'logo.png') && File::exists(public_path('img/sales/logo-pos.png'))) {
                $replace[$key] = config('app.url') . '/img/sales/logo-pos.png';
            } elseif (!starts_with($match, 'http')) {
                $replace[$key] = config('app.url') . '/docs/' . basename($match);
            } elseif (ends_with($match, 'collection.json')) {
                $replace[$key] = route('apidoc.postman');
            } else {
                $replace[$key] = $match;
            }
        }

        return str_replace($matches, $replace, $content);
    }
}
