<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\ProductPriceForm;
use App\Admin\Requests\ProductPriceRequest;
use App\Api1\Controllers\FeeController as ApiFeeController;
use App\Models\Commission;
use App\Models\Product\Fee;
use App\Models\Product\Product;
use App\Models\Product\ProductPrice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Datatables;
use Html;
use DB;
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ProductPriceController extends SuperAdminController
{
    use FormBuilderTrait;

    public function getIndex(Request $request)
    {
        return view('admin.super.productprice.index', $this->indexData($request));
    }

    public function getCreate(Request $request)
    {
        $productPrice = new ProductPrice();

        $form = $form = $this->form(ProductPriceForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:product_prices.store', ['priceId' => $productPrice->id]),
            'model'     => $productPrice,
        ], [
            'product'   => $request->query->get('product', null),
        ]);

        return view('admin.super.productprice.edit', compact('productPrice', 'form'));
    }

    public function getEdit($priceId)
    {
        $productPrice = ProductPrice::findOrFail($priceId);

        $form = $this->form(ProductPriceForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:product_prices.update', ['priceId' => $productPrice->id]),
            'model'     => $productPrice,
        ], [
            'mode'      => 'edit',
        ]);

        return view('admin.super.productprice.edit', compact('productPrice', 'form'));
    }

    public function postCreate(ProductPriceRequest $request)
    {
        $price = new ProductPrice();

        $form = $this->form(ProductPriceForm::class, ['model' => $price]);

        if ($errorMsg = $this->checkFormValidity($form, $price, $request)) {
            flash()->error($errorMsg);
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        DB::transaction(function () use ($request, $price) {
            $this->update($request, $price);
            $this->addDefaultCommissions($price);
        });

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:product_prices.edit', ['priceId' => $price->id]);
    }

    private function addDefaultCommissions(ProductPrice $productPrice)
    {
        $this->addDefaultResellerCommission($productPrice);
        $this->addDefaultOnlineCommission($productPrice);
    }

    private function addDefaultOnlineCommission(ProductPrice $productPrice)
    {
        $commissionPrice = ProductPrice::with('commissions')
            ->whereHas('commissions', function ($query) {
                $query->where('is_online', true)
                    ->whereNull('valid_till');
            })
            ->whereHas('product.managers', function ($query) use ($productPrice) {
                $managerIds = $productPrice->product->managers->pluck('id')->toArray();
                if ($managerIds) {
                    $query->whereIn('id', $managerIds);
                } else {
                    $query->whereNotNull('id');
                }
            })
            ->orderBy('created_at', 'desc')
            ->first();

        $commission = $commissionPrice
            ? $commissionPrice->commissions()
                ->where('is_online', true)
                ->whereNull('valid_till')
                ->orderBy('created_at', 'desc')
                ->first()
            : (object) Commission::DEFAULT_COMMISSIONS['online'];

        $productPrice->commissions()->create([
            'commission_type'   => $commission->commission_type,
            'commission_value'  => $commission->commission_value,
            'min_value'         => $commission->min_value,
            'is_online'         => true,
            'valid_from'        => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

    private function addDefaultResellerCommission(ProductPrice $productPrice)
    {
        $productPrice->load('product');
        $commissionPrice = ProductPrice::with('commissions')
            ->whereHas('commissions', function ($query) {
                $query->where('is_online', false)
                    ->whereNull('valid_till');
            })
            ->whereHas('product.managers', function ($query) use ($productPrice) {
                $managerIds = $productPrice->product->managers->pluck('id')->toArray();
                if ($managerIds) {
                    $query->whereIn('id', $managerIds);
                } else {
                    $query->whereNotNull('id');
                }
            })
            ->orderBy('created_at', 'desc')
            ->first();

        $commission = $commissionPrice
            ? $commissionPrice->commissions()
                ->where('is_online', false)
                ->whereNull('valid_till')
                ->orderBy('created_at', 'desc')
                ->first()
            : (object) Commission::DEFAULT_COMMISSIONS['reseller'];

        $productPrice->commissions()->create([
            'commission_type'   => $commission->commission_type,
            'commission_value'  => $commission->commission_value,
            'min_value'         => $commission->min_value,
            'is_online'         => false,
            'valid_from'        => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

    public function postUpdate(ProductPriceRequest $request, $priceId)
    {
        $price = ProductPrice::findOrFail($priceId);

        $form = $this->form(ProductPriceForm::class, ['model' => $price], ['mode' => 'edit']);

        if ($errorMsg = $this->checkFormValidity($form, $price, $request)) {
            flash()->error($errorMsg);
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $price);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::super:product_prices.edit', ['priceId' => $priceId]);
    }

    private function checkFormValidity(Form $form, ProductPrice $price, ProductPriceRequest $request)
    {
        switch (true) {
            case !$form->isValid():
                return 'Es ist ein Fehler aufgetreten';
            case $price->sales()->count():
                return 'Es gibt schon verkaufte Produkte mit diesem Preis';
            case $this->isDatesIntersected($request, $price):
                return 'Es gibt schon Preis für gewählte Produkte in diesem Zeitraum';
            default:
                return null;
        }
    }

    private function isDatesIntersected(ProductPriceRequest $request, ProductPrice $productPrice)
    {
        $product = Product::findOrFail($request->request->getInt('product'));
        $rFrom = Carbon::createFromFormat('d.m.Y', $request->request->get('valid_from'))->startOfDay();
        $rTill = Carbon::createFromFormat('d.m.Y', $request->request->get('valid_till'))->endOfDay();

        return (bool) $product->prices()
            ->where('id', '<>', $productPrice->id)
            ->where('valid_from', '<=', $rTill)
            ->where('valid_till', '>=', $rFrom)
            ->count();
    }

    private function update(ProductPriceRequest $request, ProductPrice $productPrice)
    {
        DB::transaction(function () use ($request, $productPrice) {
            if (!$productPrice->product) {
                $productPrice->product()->associate($request->request->getInt('product'));
            }

            $productPrice->value = $request->request->get('value');
            $productPrice->vat = $request->request->get('vat');
            $productPrice->visible_from = $request->request->get('visible_from', null)
                ? Carbon::createFromFormat('d.m.Y', $request->request->get('visible_from'))->startOfDay()
                : null;
            $productPrice->valid_from = $request->request->get('valid_from', null)
                ? Carbon::createFromFormat('d.m.Y', $request->request->get('valid_from'))->startOfDay()
                : null;
            $productPrice->valid_till = $request->request->get('valid_till', null)
                ? Carbon::createFromFormat('d.m.Y', $request->request->get('valid_till'))->endOfDay()
                : null;
            $productPrice->save();
        });
    }

    public function delete($priceId)
    {
        $price = ProductPrice::findOrFail($priceId);

        if ($price->sales()->count()) {
            throw new AccessDeniedHttpException('Preis könnte nicht gelöscht werden');
        }

        $price->delete();
        flash()->success('Preis wurde gelöscht');
    }

    public function getFees(Request $request, $priceId)
    {
        $price = ProductPrice::findOrFail($priceId);

        $feeIds = $price->fees()->pluck('fees.id')->toArray();
        $request->query->set('fee_ids', $feeIds);
        $request->query->set('_perPage', count($feeIds));

        return with(new ApiFeeController(new Fee()))->index($request);
    }

    public function postFees(Request $request, $priceId)
    {
        $price = ProductPrice::findOrFail($priceId);

        if ($price->sales()->count()) {
            flash()->error('Preis könnte nicht bearbeiten werden. Es gibt schon einen Verkauf mit diesem Preis.');
            return redirect()->back();
        }

        $fees = $request->request->get('fees', []);
        $price->fees()->sync($fees);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::super:product_prices.index');
    }

    public function anyData(Request $request)
    {
        $prices = $this->getProductPrices($request->query->getInt('product', -1))
            ->select(['id', 'value', 'valid_from', 'valid_till', 'product_id'])
            ->with(['product']);

        return Datatables::of($prices)
            ->editColumn('product_name', function ($price) {
                return Html::linkRoute(
                    'admin::super:products.edit',
                    str_limit($price->product->name, 50),
                    ['productId' => $price->product->id]
                );
            })
            ->editColumn('valid_from', function ($price) {
                return $price->valid_from ? $price->valid_from->format('d.m.Y') : '';
            })
            ->editColumn('valid_till', function ($price) {
                return $price->valid_till ? $price->valid_till->format('d.m.Y') : '';
            })
            ->editColumn('value', function ($price) {
                return $price->value . ' €';
            })
            ->addColumn('fees', function ($price) {
                $salesExists = (boolean) $price->sales()->count();
                $feesRoute = route('admin::super:product_prices.fees.index', ['priceId' => $price->id]);
                return '
                    <button type="button"
                            class="btn btn-xs btn-primary"
                            data-action="' . $feesRoute . '"
                            data-target="#fee-modal"
                            data-toggle="modal"
                            '.($salesExists ? 'disabled="disabled"' : '').'>
                        <span class="fa fa-suitcase fa-fw"></span>
                        Gebühren <span class="badge">
                            ' . ($price->fees ? $price->fees->count() : 0) . '
                        </span>
                    </button>
                ';
            })
            ->addColumn('actions', function ($price) {
                $editRoute = route('admin::super:product_prices.edit', ['priceId' => $price->id]);
                $removeRoute = route('admin::super:product_prices.delete', ['priceId' => $price->id]);
                $saleExists = (bool) $price->sales()->count();

                return '
                    <a href="' . $editRoute . '" class="btn btn-xs btn-primary">
                        <i class="fa fa-edit fa-fw"></i>
                    </a>
                    <button type="button"
                            data-action="' . $removeRoute . '"
                            data-method="DELETE"
                            class="btn btn-xs btn-danger'.($saleExists ? ' disabled ' : '').'"
                            data-target="#delete-modal"
                            data-toggle="modal"
                            '.($saleExists ? ' disabled ' : '').'>
                        Löschen<span class="fa fa-trash-o fa-fw"></span>
                    </button>
                ';
            })
            ->removeColumn('product')
            ->blacklist(['actions'])
            ->make(true);
    }

    private function indexData(Request $request) : array
    {
        $product = $request->query->getInt('product', 0);
        $prices = $this->getProductPrices($product);

        $count = $prices->count();

        $productList = collect([
            '0' => 'Alle',
        ]);

        $products = Product::without(['gallery'])
            ->each(function ($product) use ($productList) {
                $productList[$product->id] = $product->name;
            });

        $productList->merge($products);

        return compact('count', 'product', 'productList');
    }

    /**
     * @param int     $productId
     * @return mixed
     */
    private function getProductPrices(int $productId = 0)
    {
        $prices = (new ProductPrice())->newQuery();

        if ($productId > 0) {
            $prices->where('product_id', $productId);
        }

        return $prices;
    }
}
