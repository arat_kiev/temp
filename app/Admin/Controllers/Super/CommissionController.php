<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Requests\CommissionRequest;
use App\Models\Area\Area;
use App\Models\Contact\Manager;
use App\Models\Product\Product;
use App\Models\Product\ProductPrice;
use App\Models\Ticket\TicketPrice;
use App\Models\Ticket\TicketType;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use DB;

class CommissionController extends SuperAdminController
{
    private $commissionTreeMode = 'tickets';

    private $commissionSelect = ['commission_type', 'commission_value', 'min_value'];

    public function treeTicketsCommissions($managerId)
    {
        $manager = Manager::with(['commissions', 'areas'])->findOrFail($managerId);

        return response()->json($this->buildCommissionTree($manager));
    }

    public function treeProductsCommissions($managerId)
    {
        $manager = Manager::with(['commissions', 'areas'])->findOrFail($managerId);
        $this->commissionTreeMode = 'products';

        return response()->json($this->buildCommissionTree($manager));
    }

    private function buildCommissionTree(Model $model) : array
    {
        $children = [];

        foreach ($this->fetchCommissionTreeChildren($model) as $child) {
            array_push($children, $this->buildCommissionTree($child));
        }

        return [
            'id'            => $model->id,
            'object'        => $model->getTable(),
            'name'          => $this->fetchCommissionName($model),
            'commissions'   => [
                'online'        => $model->commissions()
                    ->where('is_online', true)
                    ->whereDate('valid_from', '<=', Carbon::now())
                    ->where(function ($q) {
                        $q->whereDate('valid_till', '>=', Carbon::now())
                            ->orWhereNull('valid_till');
                    })
                    ->select($this->commissionSelect)
                    ->orderBy('created_at', 'desc')
                    ->first(),
                'reseller'      => $model->commissions()
                    ->where('is_online', false)
                    ->whereDate('valid_from', '<=', Carbon::now())
                    ->where(function ($q) {
                        $q->whereDate('valid_till', '>=', Carbon::now())
                            ->orWhereNull('valid_till');
                    })
                    ->select($this->commissionSelect)
                    ->orderBy('created_at', 'desc')
                    ->first(),
            ],
            'children'      => $children,
        ];
    }

    private function fetchCommissionTreeChildren(Model $object) : Collection
    {
        switch ($object->getTable()) {
            case 'managers':
                return $this->commissionTreeMode === 'tickets'
                    ? $object->areas
                    : ($this->commissionTreeMode === 'products' ? $object->products : collect());
            case 'areas':
                return $object->ticketTypes;
            case 'ticket_types':
            case 'products':
                return $object->prices;
            default:
                return collect();
        }
    }

    private function fetchCommissionName(Model $object): string
    {
        if ($object instanceof ProductPrice) {
            return 'Preis für ' . $object->product->name
                . ' ' . number_format($object->value_origin / 100, 2, ',', '.') . ' €';
        } elseif ($object instanceof TicketPrice) {
            return $object->name_with_year
                . ' ' . number_format($object->value / 100, 2, ',', '.') . ' €';
        }

        return $object->name ?? '';
    }

    public function updateTicketsCommissions($managerId, CommissionRequest $request)
    {
        $manager = Manager::with([
                'commissions',
                'areas.commissions',
                'areas.ticketTypes.commissions',
                'areas.ticketTypes.prices.commissions',
            ])
            ->findOrFail($managerId);

        DB::transaction(function () use ($request, $manager) {
            $this->updateCommission($manager, $request->request->get('managers', []));
            $manager->areas()->each(function ($area) use ($request) {
                $this->updateCommission($area, $request->request->get('areas', []));
                $area->ticketTypes()->each(function ($ticketType) use ($request) {
                    $this->updateCommission($ticketType, $request->request->get('ticket_types', []));
                    $ticketType->prices()->each(function ($price) use ($request) {
                        $this->updateCommission($price, $request->request->get('ticket_prices', []));
                    });
                });
            });
        });

        flash()->success('Daten erfolgreich gespeichert');

        return response()->json(['success' => true]);
    }

    public function updateProductsCommissions($managerId, CommissionRequest $request)
    {
        $manager = Manager::with([
                'commissions',
                'products.commissions',
                'products.prices.commissions',
            ])
            ->findOrFail($managerId);

        DB::transaction(function () use ($request, $manager) {
            $this->updateCommission($manager, $request->request->get('managers', []));
            $manager->products()->each(function ($product) use ($request) {
                $this->updateCommission($product, $request->request->get('products', []));
                $product->prices()->each(function ($price) use ($request) {
                    $this->updateCommission($price, $request->request->get('product_prices', []));
                });
            });
        });

        flash()->success('Daten erfolgreich gespeichert');

        return response()->json(['success' => true]);
    }

    private function updateCommission(Model $model, array $data = [])
    {
        $model->commissions()
            ->whereNull('valid_till')
            ->update([
                'valid_till'    => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

        foreach ($data as $item) {
            if ($model->id == $item['id'] && isset($item['commissions']['online'])) {
                $model->commissions()->create([
                    'commission_type'   => $item['commissions']['online']['commission_type'],
                    'commission_value'  => (float) $item['commissions']['online']['commission_value'],
                    'min_value'         => (float) $item['commissions']['online']['min_value'],
                    'is_online'         => true,
                    'valid_from'        => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
            }
            if ($model->id == $item['id'] && isset($item['commissions']['reseller'])) {
                $model->commissions()->create([
                    'commission_type'   => $item['commissions']['reseller']['commission_type'],
                    'commission_value'  => (float) $item['commissions']['reseller']['commission_value'],
                    'min_value'         => (float) $item['commissions']['reseller']['min_value'],
                    'is_online'         => false,
                    'valid_from'        => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
            }
        }
    }
}
