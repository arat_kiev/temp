<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\ProductForm;
use App\Admin\Requests\ProductRequest;
use App\Api1\Controllers\AreaController as ApiAreaController;
use App\Api1\Controllers\ManagerController as ApiManagerController;
use App\Api1\Controllers\RentalController as ApiRentalController;
use App\Api1\Controllers\ProductController as ApiProductController;
use App\Models\Area\Area;
use App\Models\Contact\Manager;
use App\Models\Contact\Rental;
use App\Models\Product\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Datatables;
use Html;
use DB;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class ProductController extends SuperAdminController
{
    use FormBuilderTrait;

    public function getIndex(Request $request)
    {
        return view('admin.super.product.index', $this->indexData($request));
    }

    public function getCreate()
    {
        $product = new Product();

        $form = $this->form(ProductForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:products.store', ['productId' => $product->id]),
            'model'     => $product,
        ]);

        return view('admin.super.product.edit', compact('product', 'form'));
    }

    public function getEdit($productId)
    {
        $product = Product::findOrFail($productId);

        $form = $this->form(ProductForm::class, [
            'method'    => 'POST',
            'url'       => route('admin::super:products.update', ['productId' => $product->id]),
            'model'     => $product,
        ]);

        return view('admin.super.product.edit', compact('product', 'form'));
    }

    /**
     * Get product data
     *
     * @param $productId
     * @return mixed
     */
    public function show($productId)
    {
        return Product::findOrFail($productId)->toArray();
    }

    public function postCreate(ProductRequest $request)
    {
        $product = new Product();

        $form = $this->form(ProductForm::class, ['model' => $product]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $product);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:products.edit', ['productId' => $product->id]);
    }

    public function postUpdate(ProductRequest $request, $productId)
    {
        $product = Product::findOrFail($productId);

        $form = $this->form(ProductForm::class, ['model' => $product]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $product);
        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:products.edit', ['productId' => $productId]);
    }

    private function update(ProductRequest $request, Product $product)
    {
        DB::transaction(function () use ($request, $product) {
            // Product category
            $product->category()->associate($request->request->getInt('category'));

            // General data
            $product->featured_from = $request->request->get('featured_from', null)
                ? Carbon::createFromFormat('d.m.Y', $request->request->get('featured_from'))->startOfDay()
                : null;
            $product->featured_till = $request->request->get('featured_till', null)
                ? Carbon::createFromFormat('d.m.Y', $request->request->get('featured_till'))->endOfDay()
                : null;
            $product->account_number = empty($request->request->get('account_number'))
                ? null
                : $request->request->get('account_number');
            $product->template = $request->request->get('template', null);

            $product->max_quantity = $request->request->get('max_quantity');

            $this->updateTranslations($request, $product);

            $product->save();
        });
    }

    private function updateTranslations(ProductRequest $request, Product $product)
    {
        $newData = $request->request->get('data', []);
        $newLocales = array_map(function ($item) {
            return $item['locale'];
        }, $newData);
        $product->translations()->whereNotIn('locale', $newLocales)->delete();

        foreach ($newData as $newItem) {
            $locale = $newItem['locale'];
            $product->translateOrNew($locale)->name = $newItem['name'];
            $product->translateOrNew($locale)->short_description = trim($newItem['short_description']);
            $product->translateOrNew($locale)->long_description = trim($newItem['long_description']);
        }
    }

    public function getAreas(Request $request, $productId)
    {
        $product = Product::findOrFail($productId);

        $areaIds = $product->areas()->pluck('areas.id')->toArray();
        $request->query->set('area_id', $areaIds);
        $request->query->set('_perPage', count($areaIds));

        return with(new ApiAreaController(new Area()))->index($request);
    }

    public function postAreas(Request $request, $productId)
    {
        $product = Product::findOrFail($productId);

        $areas = $request->request->get('areas', []);
        $product->areas()->sync($areas);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::super:products.index');
    }

    public function getManagers(Request $request, $productId)
    {
        $product = Product::findOrFail($productId);

        $managerIds = $product->managers()->pluck('managers.id')->toArray();
        $request->query->set('manager_id', $managerIds);
        $request->query->set('_perPage', count($managerIds));

        return with(new ApiManagerController(new Manager()))->index($request);
    }

    public function postManagers(Request $request, $productId)
    {
        $product = Product::findOrFail($productId);

        $managers = $request->request->get('managers', []);
        $product->managers()->sync($managers);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::super:products.index');
    }

    public function getRentals(Request $request, $productId)
    {
        $product = Product::findOrFail($productId);

        $rentalIds = $product->rentals()->pluck('rentals.id')->toArray();
        $request->query->set('rental_id', $rentalIds);
        $request->query->set('_perPage', count($rentalIds));

        return with(new ApiRentalController(new Rental()))->index($request);
    }

    public function postRentals(Request $request, $productId)
    {
        $product = Product::findOrFail($productId);

        $rentals = $request->request->get('rentals', []);
        $product->rentals()->sync($rentals);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::super:products.index');
    }

    public function allProducts(Request $request)
    {
        return with(new ApiProductController(new Product()))->index($request);
    }

    public function uploadAttachment($productId, Request $request)
    {
        $product = Product::findOrFail($productId);

        $product->attachment = $request->file('file');
        $product->save();

        return response()->json(['status' => 1]);
    }

    public function deleteAttachment($productId)
    {
        $product = Product::findOrFail($productId);

        $product->attachment = null;
        $product->save();

        flash()->success('Anhang erfolgreich gelöscht');

        return redirect()->route('admin::super:products.index');
    }

    public function anyData(Request $request)
    {
        $products = $this->getProducts($request->query->getInt('area', -1), $request->query->getInt('manager', -1))
            ->select(['products.id', 'featured_from', 'featured_till', 'gallery_id', 'attachment'])
            ->leftJoin('product_translations', 'product_translations.product_id', '=', 'products.id')
            // ->where('product_translations.locale', '=', \App::getLocale())
            ->with(['category', 'gallery', 'prices', 'areas'])
            ->groupBy('products.id');

        return Datatables::of($products)
            ->filterColumn('name', function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    $instance->where('product_translations.name', 'like', '%'.$request->search['value'].'%');
                }
            })
            ->editColumn('name', function (Product $product) {
                return Html::linkRoute(
                    'admin::super:products.edit',
                    str_limit($product->name, 50),
                    ['productId' => $product->id]
                );
            })
            ->editColumn('featured_from', function (Product $product) {
                return $product->featured_from ? $product->featured_from->format('d.m.Y') : '';
            })
            ->editColumn('featured_till', function (Product $product) {
                return $product->featured_till ? $product->featured_till->format('d.m.Y') : '';
            })
            ->addColumn('price', function (Product $product) {
                $price = $product->prices()
                    ->where('valid_from', '<', Carbon::now())
                    ->where('valid_till', '>', Carbon::now())
                    ->first();
                $routeUrl = $price
                    ? route('admin::super:product_prices.edit', ['priceId' => $price->id])
                    : route('admin::super:product_prices.index', ['product' => $product->id]);

                $pricesRoute = '<a href="'. $routeUrl .'"
                                   class="btn btn-xs btn-default pull-right"
                                   role="button">
                                    <i class="fa fa-edit fa-fw"></i>
                                </a>';

                return $price ? "$price->value € $pricesRoute" : $pricesRoute;
            })
            ->addColumn('gallery', function (Product $product) {
                $route = route('admin::super:products.gallery.list', ['productId' => $product->id]);

                return '
                    <a href="' . $route . '" class="btn btn-xs btn-primary">
                        <span class="fa fa-picture-o fa-fw"></span>
                        Bilder <span class="badge">
                            ' . ($product->gallery ? $product->gallery->pictures()->count() : 0) . '
                        </span>
                    </a>';
            })
            ->addColumn('areas', function (Product $product) {
                $areasRoute = route('admin::super:products.areas.index', ['productId' => $product->id]);
                return '
                    <button type="button" class="btn btn-xs btn-primary"
                            data-action="' . $areasRoute . '"
                            data-target="#area-modal" data-toggle="modal">
                        <span class="fa fa-map-marker fa-fw"></span>
                        Gewässer <span class="badge">
                            ' . ($product->areas ? $product->areas->count() : 0) . '
                        </span>
                    </button>
                ';
            })
            ->addColumn('managers', function (Product $product) {
                $managersRoute = route('admin::super:products.managers.index', ['productId' => $product->id]);
                return '
                    <button type="button" class="btn btn-xs btn-primary"
                            data-action="' . $managersRoute . '"
                            data-target="#manager-modal" data-toggle="modal">
                        <span class="fa fa-map-marker fa-fw"></span>
                        Bewirtschafter <span class="badge">
                            ' . ($product->managers ? $product->managers->count() : 0) . '
                        </span>
                    </button>
                ';
            })
            ->addColumn('rentals', function (Product $product) {
                $rentalsRoute = route('admin::super:products.rentals.index', ['productId' => $product->id]);
                return '
                    <button type="button" class="btn btn-xs btn-primary"
                            data-action="' . $rentalsRoute . '"
                            data-target="#rental-modal" data-toggle="modal">
                        <span class="fa fa-map-marker fa-fw"></span>
                        Bootsverleihe <span class="badge">
                            ' . ($product->rentals()->count()) . '
                        </span>
                    </button>
                ';
            })
            ->addColumn('stocks', function (Product $product) {
                $stockBtn = $product->productStocks()->count()
                    ? ' <a   type="button"
                             href="'.route('admin::super:product_stocks.index', ['product' => $product->id]).'"
                             class="btn btn-xs btn-primary">
                            <i class="fa fa-university fa-fw"></i> Lager
                            <span class="badge">' . $product->productStocks()->count() . '</span>
                        </a>'
                    : ' <button class="btn btn-xs btn-default">
                            <i class="fa fa-power-off fa-fw"></i> Online
                        </button>';
                return $stockBtn . '
                    <a   type="button"
                         href="' . route('admin::super:product_stocks.create', ['product'    => $product->id]) . '"
                         class="btn btn-xs btn-success pull-right"
                         target="_blank">Wareneingang
                        <span class="fa fa-plus-circle fa-fw"></span>
                    </a>
                ';
            })
            ->addColumn('attachment', function (Product $product) {
                $previewRoute = route('imagecache', ['template' => 'download', 'filename' => $product->attachment]);
                $uploadRoute = route('admin::super:products.attachment.upload', ['productId' => $product->id]);
                $deleteRoute = route('admin::super:products.attachment.delete', ['productId' => $product->id]);

                $downloadBtn = '
                    <div class="pull-left">
                        <a href="' . $previewRoute . '"
                           type="button"
                           class="btn btn-xs btn-warning"
                           target="_blank">
                            <span class="fa fa-file-archive-o fa-fw"></span>
                        </a>
                    </div>
                ';
                $deleteBtn = '
                    <form action="' . $deleteRoute . '"
                          method="post"
                          class="pull-left delete-attachment"
                          data-id="' . $product->id . '"
                          style="margin-left:2px">
                        ' . method_field('delete') . '
                        <button type="submit" class="btn btn-xs btn-danger">
                            Löschen<span class="fa fa-trash-o fa-fw"></span>
                        </button>
                    </form>
                ';
                $uploadBtn = '
                    <div class="pull-right">
                        <a href="#"
                           class="btn btn-xs btn-success"
                           data-toggle="modal"
                           data-target="#upload-modal"
                           data-action="'. $uploadRoute .'">
                            <span class="fa fa-upload fa-fw"></span> Hochladen
                        </a>
                    </div>
                ';

                return $product->attachment
                    ? $downloadBtn . $deleteBtn . $uploadBtn
                    : $uploadBtn;
            })
            ->removeColumn('category', 'prices', 'short_description', 'long_description')
            ->blacklist(['price_value', 'gallery'])
            ->make(true);
    }

    private function indexData(Request $request) : array
    {
        $area = $request->query->getInt('area', -1);
        $manager = $request->query->getInt('manager', -1);
        $products = $this->getProducts($area, $manager);

        $count = $products->count();

        $areaList = collect([
            '-1'    => '---',
            '0'     => 'Alle mit Gewässer',
        ]);
        $managerList = collect([
            '-1'    => '---',
            '0'     => 'Alle mit Bewirtschafter',
        ]);

        $areas = Area::select(['id', 'name'])
            ->without(['rule', 'gallery.pictures', 'ticketTypes', 'resellers', 'locations'])
            ->has('products')
            ->each(function ($area) use ($areaList) {
                $areaList[$area->id] = $area->name;
            });

        $managers = Manager::select(['id', 'name'])
            ->without(['logo', 'rule', 'admins'])
            ->has('products')
            ->each(function ($manager) use ($managerList) {
                $managerList[$manager->id] = $manager->name;
            });

        $areaList->merge($areas);
        $managerList->merge($managers);

        return compact('count', 'area', 'areaList', 'manager', 'managerList');
    }

    private function getProducts(int $area = -1, int $manager = -1)
    {
        $products = (new Product)->newQuery();

        if ($area > 0) {
            $products->whereHas('areas', function ($query) use ($area) {
                $query->where('id', $area);
            });
        } elseif ($area === 0) {
            $products->has('areas');
        }

        if ($manager > 0) {
            $products->whereHas('managers', function ($query) use ($manager) {
                $query->where('id', $manager);
            });
        } elseif ($manager === 0) {
            $products->has('managers');
        }

        return $products;
    }
}
