<?php

namespace App\Admin\Controllers\Super;

use App;
use App\Admin\Controllers\AdminController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;

class SuperAdminController extends AdminController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (!Auth::user() || !Auth::user()->hasRole(['superadmin'])) {
                if (!App::runningInConsole()) {
                    throw new AccessDeniedHttpException();
                }
            }

            return $next($request);
        });
    }
}
