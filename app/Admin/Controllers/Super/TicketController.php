<?php

namespace App\Admin\Controllers\Super;

use App\Admin\Forms\TicketForm;
use App\Admin\Requests\TicketRequest;
use App\Events\TicketStorned;
use App\Managers\Export\TicketExportManager;
use App\Models\Contact\Reseller;
use App\Managers\TicketManager;
use App\Models\Ticket\TicketPrice;
use App\Models\Ticket\Checkin;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Http\Request;
use App\Models\Ticket\Ticket;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Carbon\Carbon;
use Datatables;
use Event;

class TicketController extends SuperAdminController
{
    use FormBuilderTrait;

    public function getIndex(Request $request)
    {
        $from = $request->query->get('from', Carbon::now()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $reseller = $request->query->getInt('resellerId', -1);
        $invoiced = $request->query->getInt('invoiced', -1);

        // prepare base query
        $query = $this->fetchTickets($from, $till, $reseller)
            ->join('ticket_prices', 'tickets.ticket_price_id', '=', 'ticket_prices.id')
            ->when($invoiced === 0, function (QueryBuilder $query) {
                return $query->whereNull('hf_invoice_id');
            })
            ->when($invoiced === 1, function (QueryBuilder $query) {
                return $query->whereNotNull('hf_invoice_id');
            });

        // ticket count
        $countTickets = $query->count();

        // ticket sum
        $sumTickets = $query->sum('value');
        $sumTickets = number_format($sumTickets / 100.0, 2, ",", ".") . ' €';

        // commission sum
        $commissionTotal = $query->sum('tickets.commission_value');
        $commissionTotal = number_format($commissionTotal / 100.0, 2, ",", ".") . ' €';

        // filter options
        $resellerList = collect([
            '-1' => 'Alle',
            Reseller::OFFLINE_TICKETS_PARAM => 'Alle Verkaufsstellen',
            Reseller::ONLINE_TICKETS_PARAM => 'Online',
        ]);

        Reseller::orderBy('name')->each(function ($reseller) use ($resellerList) {
            $resellerList->put($reseller->id, $reseller->name);
        });

        return view(
            'admin.super.ticket.index',
            compact('countTickets', 'sumTickets', 'commissionTotal', 'from', 'till', 'reseller', 'resellerList', 'invoiced')
        );

    }

    public function getEdit($ticketId)
    {
        $ticket = Ticket::findOrFail($ticketId);

        $form = $this->form(TicketForm::class, [
            'method' => 'POST',
            'url' => route('admin::super:tickets.update', ['ticketId' => $ticketId]),
            'model' => $ticket,
        ]);

        return view('admin.super.ticket.edit', compact('form', 'ticket'));
    }

    public function postUpdate(TicketRequest $request, $ticketId)
    {
        $ticket = Ticket::findOrFail($ticketId);

        $form = $this->form(TicketForm::class, ['model' => $ticket]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $checked = $this->preUpdateCheck($request);
        if ($checked !== true) {
            return $checked;
        }
        $this->update($request, $ticket);

        flash()->success('Daten erfolgreich gespeichert');

        return redirect()->route('admin::super:tickets.edit', ['ticketId' => $ticketId]);
    }

    private function preUpdateCheck(TicketRequest $request)
    {
        $ticketPrice = TicketPrice::findOrFail($request->request->get('price'));

        if ($ticketPrice->ticket_type_id != $request->request->get('type')) {
            return redirect()->back()
                ->withErrors(['type' => 'Falscher Tickettyp für den ausgewählten Ticketpreis'])
                ->withInput();
        }

        $requestDateFrom = Carbon::createFromFormat('d.m.Y H:i', $request->request->get('valid_from'));
        $requestDateTill = Carbon::createFromFormat('d.m.Y H:i', $request->request->get('valid_to'));

        if ($requestDateFrom < $ticketPrice->valid_from || $requestDateTill > $ticketPrice->valid_to) {
            return redirect()->back()
                ->withErrors([
                    'valid_from' => 'Gültigkeit außerhalb der Saison',
                    'valid_to' => 'Gültigkeit außerhalb der Saison',
                ])
                ->withInput();
        }

        return true;
    }

    private function update(TicketRequest $request, Ticket $ticket)
    {
        $ticket->price()->associate($request->request->get('price'));
        $ticket->type()->associate($request->request->get('type'));
        $ticket->client()->associate($request->request->get('client'));
        $ticket->checkins()->delete();
        $newCheckins = [];
        foreach ($request->get('checkins', []) as $checkin) {
            $newCheckins[] = new Checkin([
                'ticket_id' => $checkin['ticket_id'],
                'from' => $checkin['from'],
                'till' => $checkin['till'],
            ]);
        }
        $ticket->checkins()->saveMany($newCheckins);

        $ticket->fill([
            'valid_from' => Carbon::createFromFormat('d.m.Y H:i', $request->request->get('valid_from')),
            'valid_to' => Carbon::createFromFormat('d.m.Y H:i', $request->request->get('valid_to')),
        ]);
        $ticket->save();
    }

    public function softDelete($ticketId, Request $request, TicketManager $ticketManager)
    {
        if (!$ticket = Ticket::find($ticketId)) {
            return response()->json([
                'notification' => 'danger',
                'message' => 'Ticket wurde nicht gefunden'
            ]);
        }

        try {
            $ticketManager->createStornoTicket($ticket, $request->get('reason', ''));
            Event::fire(new TicketStorned($ticket));

            return response()->json([
                'notification' => 'success',
                'message' => 'Ticket wurde gelöscht'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'notification' => 'danger',
                'message' => 'Ticket konnte nicht gelöscht werden'
            ]);
        }
    }

    public function anyData(TicketRequest $request)
    {
        $from = $request->query->get('from', Carbon::now()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $reseller = $request->query->getInt('resellerId', -1);
        $invoiced = $request->query->getInt('invoiced', -1);

        $tickets = $this->fetchTickets($from, $till, $reseller)
            ->leftJoin('users', 'users.id', '=', 'tickets.user_id')
            ->when($invoiced === 0, function (QueryBuilder $query) {
                return $query->whereNull('hf_invoice_id');
            })
            ->when($invoiced === 1, function (QueryBuilder $query) {
                return $query->whereNotNull('hf_invoice_id');
            });

        return Datatables::of($tickets)
            ->filterColumn('user_full_name', function ($instance) use ($request) {
                if ($request->has('search') && $request->search['value']) {
                    if (is_numeric($request->search['value'])) {
                        $instance->where('users.id', 'LIKE', $request->search['value']."%");
                    } elseif (filter_var($request->search['value'], FILTER_VALIDATE_EMAIL)) {
                        $instance->where('users.email', 'LIKE', "%".$request->search['value']."%");
                    } elseif (preg_match(User::FISHER_ID_REGEX, $request->search['value'])) {
                        $instance->where('users.fisher_id', $request->search['value']);
                    } else {
                        $tokens = explode(' ', preg_replace('/\s+/', ' ', $request->search['value']));

                        if (count($tokens) === 1) {
                            $instance->where('users.first_name', 'LIKE', "%{$tokens[0]}%");
                            $instance->orWhere('users.last_name', 'LIKE', "%{$tokens[0]}%");
                            $instance->orWhere('reseller_tickets.first_name', 'LIKE', "%{$tokens[0]}%");
                            $instance->orWhere('reseller_tickets.last_name', 'LIKE', "%{$tokens[0]}%");
                        }

                        if (count($tokens) >= 2) {
                            $instance->orWhere(function ($query) use ($tokens) {
                                $query->where('users.first_name', 'LIKE', "%{$tokens[0]}%");
                                $query->where('users.last_name', 'LIKE', "%{$tokens[1]}%");
                            });

                            $instance->orWhere(function ($query) use ($tokens) {
                                $query->where('users.first_name', 'LIKE', "%{$tokens[1]}%");
                                $query->where('users.last_name', 'LIKE', "%{$tokens[0]}%");
                            });

                            $instance->orWhere(function ($query) use ($tokens) {
                                $query->where('reseller_tickets.first_name', 'LIKE', "%{$tokens[0]}%");
                                $query->where('reseller_tickets.last_name', 'LIKE', "%{$tokens[1]}%");
                            });

                            $instance->orWhere(function ($query) use ($tokens) {
                                $query->where('reseller_tickets.first_name', 'LIKE', "%{$tokens[1]}%");
                                $query->where('reseller_tickets.last_name', 'LIKE', "%{$tokens[0]}%");
                            });
                        }
                    }
                }
            })
            ->editColumn('price.name', function ($ticket) {
                return $ticket->type->name . ' (' . number_format($ticket->price->value / 100.0, 2, ",", ".") . ' €)';
            })
            ->editColumn('type.area.name', function ($ticket) {
                return str_limit($ticket->type->area->name, 30);
            })
            ->editColumn('user_full_name', function ($ticket) {
                return $ticket->user ?
                    $ticket->user->first_name . ' ' . $ticket->user->last_name :
                    $ticket->resellerTicket->first_name . ' ' . $ticket->resellerTicket->last_name;
            })
            ->addColumn('invoiced', function ($ticket) {
                return $ticket->hf_invoice_id ?
                    '<span class="fa fa-check fa-fw"></span>' :
                    '<span class="fa fa-times fa-fw"></span>';
            })
            ->addColumn('hauls_count', function ($ticket) {
                $haulClass = $ticket->hauls->count()
                    ? 'btn-success'
                    : ($ticket->type->is_catchlog_required
                        ? 'btn-danger'
                        : 'btn-warning');
                return '
                    <a   type="button"
                         href="'.route('admin::super:hauls.index', ['ticket' => $ticket->id]).'"
                         class="btn btn-xs btn-datatables '.$haulClass.' ">
                        <span>Fänge</span>
                        <span class="badge">' . $ticket->hauls->count() . '</span>
                    </a>
                    <a      type="button"
                            href="' . route('admin::super:hauls.create', [
                        'ticket' => $ticket->id,
                        'area' => $ticket->type->area->id,
                        'user' => $ticket->user ? $ticket->user->id : null,
                    ]) . '"
                            class="btn btn-xs btn-primary btn-datatables"
                            target="_blank">
                        <span class="fa fa-plus-circle fa-fw"></span>
                        Eintragen
                    </a>';
            })
            ->addColumn('reseller', function ($ticket) {
                return ($ticket->user && $ticket->client->name !== 'pos')
                    ? 'online'
                    : str_limit($ticket->resellerTicket->reseller->name, 30);
            })
            ->addColumn('commission_value', function ($ticket) {
                return $ticket->commission_value . ' €';
            })
            ->addColumn('actions', function ($ticket) {
                $previewRoute = route('preview.ticket', ['ticket' => $ticket->id, 'format' => 'pdf']);
                $editRoute = route('admin::super:tickets.edit', ['ticketId' => $ticket->id]);
                $removeRoute = route('admin::super:tickets.delete', ['ticketId' => $ticket->id]);

                return '
                    <button type="button"
                            class="btn btn-xs btn-default btn-datatables"
                            data-target="#preview-modal"
                            data-toggle="modal"
                            data-ticket-id="' . $ticket->id . '">
                        <span class="fa fa-eye fa-fw"></span>
                        Vorschau
                    </button>
                    <a href="' . $previewRoute . '"
                       type="button"
                       class="btn btn-xs btn-warning btn-datatables"
                       target="_blank">
                        <span class="fa fa-file-pdf-o fa-fw"></span>
                        PDF
                    </a>
                    <a href="' . $editRoute . '" type="button" class="btn btn-xs btn-primary btn-datatables">
                        <span class="fa fa-pencil-square-o fa-fw"></span>
                        Bearbeiten
                    </a>
                    <button type="button"
                            class="btn btn-xs btn-danger btn-datatables"
                            data-target="#delete-modal"
                            data-toggle="modal"
                            data-action="' . $removeRoute . '">
                        Stornieren<span class="fa fa-trash-o fa-fw"></span>
                    </button>
                ';
            })
            ->blacklist(['actions', 'user.full_name', 'type.area.name', 'reseller'])
            ->make(true);
    }

    public function getExport(TicketRequest $request, $format = 'xls', TicketExportManager $exportManager)
    {
        if ($format != 'xls') {
            throw new \Exception('Invalid format parameter');
        }

        $from = $request->query->get('from', Carbon::now()->format('Y-m-d'));
        $till = $request->query->get('till', Carbon::now()->format('Y-m-d'));
        $reseller = $request->query->getInt('reseller', -1);
        $invoiced = $request->query->getInt('invoiced', -1);

        $tickets = $this->fetchTickets($from, $till, $reseller)
            ->when($invoiced === 0, function (QueryBuilder $query) {
                return $query->whereNull('hf_invoice_id');
            })
            ->when($invoiced === 1, function (QueryBuilder $query) {
                return $query->whereNotNull('hf_invoice_id');
            })
            ->orderBy('tickets.created_at');

        $file = $exportManager->run($tickets, $format, compact('from', 'till', 'reseller'));

        return response()->download($file);
    }

    private function fetchTickets($from = null, $till = null, $reseller = -1)
    {
        $query = Ticket::with(['user', 'type', 'type.area', 'price', 'resellerTicket'])
            ->withoutStorno()
            ->leftJoin('reseller_tickets', 'tickets.id', '=', 'reseller_tickets.ticket_id')
            ->select('tickets.*');

        $from = !is_null($from)
            ? Carbon::createFromFormat('Y-m-d', $from)->startOfDay()
            : Carbon::today()->startOfDay();
        $till = !is_null($till)
            ? Carbon::createFromFormat('Y-m-d', $till)->endOfDay()
            : Carbon::today()->endOfDay();

        if ($reseller >= 0) {
            switch ($reseller) {
                case Reseller::ONLINE_TICKETS_PARAM:
                    $query->whereNotNull('tickets.user_id');
                    break;
                case Reseller::OFFLINE_TICKETS_PARAM:
                    $query->whereNull('tickets.user_id');
                    break;
                default:
                    $query->where('reseller_tickets.reseller_id', '=', $reseller);
            }
        }

        $query->whereBetween('tickets.created_at', [$from, $till]);

        return $query;
    }
}
