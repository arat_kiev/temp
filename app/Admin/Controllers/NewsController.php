<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\NewsForm;
use App\Admin\Requests\StoreNewsRequest;
use App\Api1\Requests\ImageUploadRequest;
use App\Models\Gallery;
use App\Models\Organization\News;
use App\Models\Organization\Organization;
use App\Models\Picture;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Datatables;
use Html;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class NewsController extends AdminController
{
    use FormBuilderTrait;

    public function getIndex()
    {
        return view('admin.news.index');
    }

    public function getCreate()
    {
        $news = new News();

        $form = $this->form(NewsForm::class, [
            'method' => 'POST',
            'url' => route('admin::news.store'),
            'model' => $news,
        ]);

        return view('admin.news.edit', compact('form', 'news'));
    }

    public function postStore(StoreNewsRequest $request)
    {
        $organization = $this->getOrganization();

        $news = new News();

        $form = $this->form(NewsForm::class, ['model' => $news]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $organization, $news);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::news.edit', ['newsId' => $news->id]);
    }

    public function getEdit($newsId)
    {
        $news = News::withTrashed()->findOrFail($newsId);

        $form = $this->form(NewsForm::class, [
            'method' => 'POST',
            'url' => route('admin::news.update', ['newsId' => $news->id]),
            'model' => $news,
        ]);

        return view('admin.news.edit', compact('form', 'news'));
    }

    public function postUpdate(StoreNewsRequest $request, $newsId)
    {
        $organization = $this->getOrganization();

        $news = News::withTrashed()->findOrFail($newsId);

        $form = $this->form(NewsForm::class, ['model' => $news]);

        if (!$form->isValid()) {
            flash()->error('Es ist ein Fehler aufgetreten');
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $this->update($request, $organization, $news);

        flash()->success('Daten erfolgreich gespeichert');
        return redirect()->route('admin::news.edit', ['newsId' => $news->id]);
    }

    public function getDelete($newsId)
    {
        $news = News::withTrashed()->findOrFail($newsId);

        if ($news->organization->id !== $this->getOrganization()->id) {
            throw new AccessDeniedHttpException();
        }

        $news->gallery()->delete();
        $news->forceDelete();

        flash()->success('Beitrag gelöscht');
        return redirect()->route('admin::news.index');
    }

    public function getDeletePicture($newsId, $pictureId)
    {
        $news = News::withTrashed()->findOrFail($newsId);
        $picture = Picture::findOrFail($pictureId);

        if ($news->organization->id !== $this->getOrganization()->id) {
            throw new AccessDeniedHttpException();
        }

        $picture->delete();

        flash()->success('Bild gelöscht');
        return redirect()->route('admin::news.edit', ['newsId' => $news->id]);
    }

    private function update(StoreNewsRequest $request, Organization $organization, News $news)
    {
        if (!$news->organization) {
            $news->organization()->associate($organization);
        }

        if (!$news->gallery) {
            $news->gallery()->associate(Gallery::create());
        }

        $user = Auth::user();

        $published_on = Carbon::now();
        ;
        if (!empty($request->request->get('published_on'))) {
            $published_on = Carbon::createFromFormat('d.m.Y H:i', $request->request->get('published_on'));
        }

        $unpublished_on = null;
        if (!empty($request->request->get('unpublished_on'))) {
            $unpublished_on = Carbon::createFromFormat('d.m.Y H:i', $request->request->get('unpublished_on'));
        }

        $news->author()->associate($user);
        $news->title = $request->request->get('title');
        $news->content = clean($request->request->get('content'));
        $news->public = $request->request->getBoolean('public');
        $news->published_on = $published_on;
        $news->deleted_at = $unpublished_on;
        $news->save();
    }

    public function postUpload(ImageUploadRequest $request, $newsId)
    {
        /** @var User $user */
        $user = Auth::user();

        $news = News::withTrashed()->findOrFail($newsId);

        if ($news->organization->id !== $this->getOrganization()->id) {
            throw new AccessDeniedHttpException();
        }

        $picture = new Picture([
            'file' => $request->file('file'),
        ]);
        $picture->author()->associate($user);


        $news->gallery->pictures()->save($picture);

        return response()->json();
    }

    public function getPrimary($newsId, $pictureId)
    {
        if (!$news = $this->getOrganization()->news()->withTrashed()->find($newsId)) {
            throw new AccessDeniedHttpException();
        }

        foreach ($news->gallery->pictures as $picture) {
            $news->gallery->pictures()->updateExistingPivot($picture->id, [
                'priority' => $picture->id == $pictureId ? 5 : 0
            ]);
        }

        flash()->success('Titelbild gesetzt');
        return redirect()->route('admin::news.edit', ['newsId' => $news->id]);
    }

    public function anyData()
    {
        $organization = $this->getOrganization();

        $news = $organization ? $organization->news()->withTrashed() : collect();

        return Datatables::of($news)
            ->editColumn('title', function ($entry) {
                return Html::linkRoute('admin::news.edit', $entry->title, ['newsId' => $entry->id]);
            })
            ->editColumn('public', function ($entry) {
                $icon = $entry->public ? 'fa-check' : 'fa-times';
                return '<span class="fa ' . $icon . ' fa-fw"></span>';
            })
            ->addColumn('status', function ($entry) {
                return $entry->getStatus();
            })
            ->addColumn('author_name', function ($entry) {
                return $entry->author->fullName !== 'Unbekannt' ? $entry->author->fullName: $entry->author->name;
            })
            ->editColumn('published_on', function ($entry) {
                return $entry->published_on->format("d.m.Y - H:i");
            })
            ->editColumn('created_at', function ($entry) {
                return $entry->created_at->format("d.m.Y - H:i");
            })
            ->addColumn('actions', function ($entry) {
                $removeUrl = route('admin::news.delete', ['newsId' => $entry->id]);
                return '
                    <div class="btn-group">
                        <button type="button"
                                class="btn btn-default btn-xs dropdown-toggle"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                            <span class="fa fa-remove fa-fw"></span>
                            Löschen
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="' . $removeUrl . '" class="locations-btn-remove bg-danger">
                                    <span class="fa fa-exclamation fa-fw"></span>
                                    Ja, wirklich löschen
                                </a>
                            </li>
                        </ul>
                    </div>
                ';
            })
            ->make(true);
    }
}
