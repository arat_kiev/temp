<?php

namespace App\Admin\Controllers;

use App\Api1\Controllers\OrganizationController as ApiOrganizationController;
use App\Api1\Controllers\InspectorController as ApiInspectorController;
use App\Api1\Controllers\ManagerController as ApiManagerController;
use App\Api1\Controllers\RentalController as ApiRentalController;
use App\Models\Contact\Rental;
use App\Models\Organization\Organization;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Http\Controllers\Controller;
use App\Models\Contact\Manager;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use View;

class AdminController extends Controller
{
    /** @var Manager */
    private $manager = null;

    /** @var Organization */
    private $organization = null;

    /** @var User */
    private $inspector = null;

    /** @var Rental */
    private $rental = null;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user() || (!Auth::user()->hasAnyRole([
                'superadmin', 'manager', 'ticket_inspector', 'organization'
            ]) && !Auth::user()->rentals()->count())) {
                throw new AccessDeniedHttpException();
            }

            $viewShares = [];

            if ($this->getManager()) {
                $viewShares['current_manager'] = $this->getManager();
            }

            if ($this->getOrganization()) {
                $viewShares['current_organization'] = $this->getOrganization();
            }

            if ($this->getInspector()) {
                $viewShares['inspector'] = $this->getInspector();
            }

            if ($this->getRental()) {
                $viewShares['current_rental'] = $this->getRental();
            }

            if ($viewShares) {
                View::share($viewShares);
            }

            return $next($request);
        });
    }

    /**
     * Get the admin users manager
     *
     * @return Manager
     */
    protected function getManager()
    {
        if ($this->manager) {
            return $this->manager;
        }

        if ($managerId = session('manager')) {
            $this->manager = Manager::find($managerId);
        } elseif (Auth::user()) {
            $this->manager = Auth::user()->managers()->first();
        } else {
            $this->manager = null;
        }

        return $this->manager;
    }

    public function getAllManagers(Request $request)
    {
        if ($request->query->has('search') && is_array($request->query->get('search'))) {
            $request->query->set('search', $request->query->get('search')['value']);
        }

        return with(new ApiManagerController(new Manager()))->index($request);
    }

    public function setManager($managerId)
    {
        if (!Auth::user()->hasRole('superadmin')) {
            throw new AccessDeniedHttpException('No superadmin');
        }

        session(['manager' => $managerId]);

        return response()->json();
    }

    /**
     * Get the admin users organization
     *
     * @return Organization
     */
    protected function getOrganization()
    {
        if ($this->organization) {
            return $this->organization;
        }

        if ($organizationId = session('organization')) {
            $this->organization = Organization::find($organizationId);
        } elseif (Auth::user()) {
            $this->organization = Auth::user()->organizations()->first();
        } else {
            $this->organization = null;
        }

        return $this->organization;
    }

    public function getAllOrganizations(Request $request)
    {
        if ($request->query->has('search') && is_array($request->query->get('search'))) {
            $request->query->set('search', $request->query->get('search')['value']);
        }

        return with(new ApiOrganizationController(new Organization()))->index($request);
    }

    public function setOrganization($organizationId)
    {
        if (!Auth::user()->hasRole('superadmin')) {
            throw new AccessDeniedHttpException('No superadmin');
        }

        session(['organization' => $organizationId]);

        return response()->json();
    }

    /**
     * Get the admin areas inspector
     *
     * @return User
     */
    protected function getInspector()
    {
        if ($this->inspector) {
            return $this->inspector;
        }

        if (Auth::user()->hasRole('ticket_inspector') || Auth::user()->inspectedAreas()->first()) {
            $this->inspector = Auth::user();
        } elseif ($inspectorId = session('inspector')) {
            $this->inspector = User::find($inspectorId);
        } else {
            $this->inspector = null;
        }

        return $this->inspector;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getAllInspectors(Request $request)
    {
        if ($request->query->has('search') && is_array($request->query->get('search'))) {
            $request->query->set('search', $request->query->get('search')['value']);
        }

        return with(new ApiInspectorController(new User()))->index($request);
    }

    public function setInspector($inspectorId)
    {
        if (!Auth::user()->hasRole('superadmin')) {
            throw new AccessDeniedHttpException('No superadmin');
        }

        session(['inspector' => $inspectorId]);

        return response()->json(['Status' => 1]);
    }

    /**
     * Get the rental admin
     *
     * @return Rental
     */
    protected function getRental()
    {
        if ($this->rental) {
            return $this->rental;
        }

        if (Auth::user()->rentals()->count()) {
            $this->rental = Auth::user()->rentals()->first();
        } elseif ($rentalId = session('rental')) {
            $this->rental = Rental::find($rentalId);
        } else {
            $this->rental = null;
        }

        return $this->rental;
    }

    public function getAllRentals(Request $request)
    {
        if ($request->query->has('search') && is_array($request->query->get('search'))) {
            $request->query->set('search', $request->query->get('search')['value']);
        }

        return with(new ApiRentalController(new Rental))->index($request);
    }

    public function setRental($rentalId)
    {
        if (!Auth::user()->hasRole('superadmin')) {
            throw new AccessDeniedHttpException('No superadmin');
        }

        session(['rental' => $rentalId]);

        return response()->json(['Status' => 1]);
    }
}
