<?php

namespace App\Models\Promo;

use App\Models\BaseModel;

class UserCoupon extends BaseModel
{
    protected $table = 'user_coupons';
}
