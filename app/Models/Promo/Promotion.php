<?php

namespace App\Models\Promo;

use App\Models\BaseModel;
use App\Models\Product\Product;
use App\Models\Ticket\TicketType;
use App\Models\User;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends BaseModel
{
    use SoftDeletes,
        Translatable;

    protected $table = 'promotions';

    protected $fillable = [
        'type',
        'valid_from',
        'valid_till',
        'usage_bonus',
        'owner_bonus',
        'is_active',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'valid_from', 'valid_till'];

    protected $casts = [
        'usage_bonus'   => 'integer',
        'owner_bonus'   => 'integer',
        'is_active'     => 'boolean',
    ];

    protected $with = ['translations'];

    protected $translationForeignKey = 'promo_id';

    protected $translatedAttributes = ['name', 'description'];

    const AVAILABLE_TYPES = [
        'INVITE'    => 'Einladung',
        'SIGNUP'    => 'Registrierung',
        'SALE'      => 'Verkäufe',
    ];

    /**
     * Register model event handlers
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function (Promotion $promo) {
            $promo->created_by = auth()->check() ? auth()->user()->id : null;
            $promo->updated_by = auth()->check() ? auth()->user()->id : null;
        });

        static::updating(function (Promotion $promo) {
            $promo->updated_by = auth()->check() ? auth()->user()->id : null;
        });

        static::deleting(function (Promotion $promo) {
            $promo->deleted_by = auth()->check() ? auth()->user()->id : null;
        });
    }

    /**
     * Get the value attribute
     *
     * @param  string  $value
     * @return integer
     */
    public function getUsageBonusOriginAttribute($value)
    {
        return $this->attributes['usage_bonus'] ?? null;
    }

    /**
     * Get the value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getUsageBonusAttribute($value) : string
    {
        return number_format($value / 100.0, 2, ',', '.');
    }

    /**
     * Set the value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function setUsageBonusAttribute(string $value)
    {
        $this->attributes['usage_bonus'] = ((float) str_replace(',', '.', $value)) * 100;
    }

    /**
     * Get the value attribute
     *
     * @param  string  $value
     * @return integer
     */
    public function getOwnerBonusOriginAttribute($value)
    {
        return $this->attributes['owner_bonus'] ?? null;
    }

    /**
     * Get the value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getOwnerBonusAttribute($value) : string
    {
        return number_format($value / 100.0, 2, ',', '.');
    }

    /**
     * Set the value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function setOwnerBonusAttribute(string $value)
    {
        $this->attributes['owner_bonus'] = $value ? ((float) str_replace(',', '.', $value)) * 100 : null;
    }

    /**
     * Get the promotion coupons
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function coupons()
    {
        return $this->hasMany(Coupon::class, 'promo_id');
    }

    /**
     * Get the invoice created_by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * Get the invoice created_by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    /**
     * Get the invoice created_by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function deletedBy()
    {
        return $this->belongsTo(User::class, 'deleted_by');
    }

    /**
     * Get ticket types related to promotion
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function ticketTypes()
    {
        return $this->morphedByMany(TicketType::class, 'related', 'promotion_targets', 'promo_id')->withTimestamps();
    }

    /**
     * Get products related to promotion
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function products()
    {
        return $this->morphedByMany(Product::class, 'related', 'promotion_targets', 'promo_id')->withTimestamps();
    }
}
