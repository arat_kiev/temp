<?php

namespace App\Models\Promo;

use App\Models\BaseModel;

class PromotionTranslation extends BaseModel
{
    protected $table = 'promotion_translations';

    protected $fillable = ['name', 'description'];
}
