<?php

namespace App\Models\Promo;

use App\Models\BaseModel;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends BaseModel
{
    use SoftDeletes;

    protected $table = 'coupons';

    protected $fillable = [
        'code',
        'usage_bonus',
        'is_public',
        'created_by',
        'created_for',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'is_public'     => 'boolean',
    ];

    /**
     * Register model event handlers
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function (Coupon $coupon) {
            $coupon->created_by = auth()->check() ? auth()->user()->id : null;
        });

        static::deleting(function (Coupon $coupon) {
            $coupon->deleted_by = auth()->check() ? auth()->user()->id : null;
        });
    }

    /**
     * Get the value attribute
     *
     * @param  string  $value
     * @return integer
     */
    public function getUsageBonusOriginAttribute($value) : int
    {
        return $this->attributes['usage_bonus'];
    }

    /**
     * Get the value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getUsageBonusAttribute($value) : string
    {
        return number_format($value / 100.0, 2, ',', '.');
    }

    /**
     * Set the value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function setUsageBonusAttribute(string $value)
    {
        $this->attributes['usage_bonus'] = ((float) str_replace(',', '.', $value)) * 100;
    }

    /**
     * Get the coupon promotion
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function promotion()
    {
        return $this->belongsTo(Promotion::class, 'promo_id');
    }

    /**
     * Get the coupon created_by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * Get the coupon created_for user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdFor()
    {
        return $this->belongsTo(User::class, 'created_for');
    }

    /**
     * Get the coupon deleted_by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deletedBy()
    {
        return $this->belongsTo(User::class, 'deleted_by');
    }

    /**
     * Get the users who used the coupon
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usedBy()
    {
        return $this->belongsToMany(User::class, 'user_coupons')->withTimestamps();
    }
}
