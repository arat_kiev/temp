<?php

namespace App\Models\Promo;

use App\Models\BaseModel;

class PromotionTarget extends BaseModel
{
    protected $table = 'promotion_targets';
}
