<?php

namespace App\Models;

class Comment extends BaseModel
{
    protected $table = 'comments';

    protected $fillable = ['body', 'status'];

    protected $dates = ['created_at', 'updated_at'];

    /**
     * Get the polymorpic relation (Area, Fish, ...)
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * Get the comments owner
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Get reports
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function report()
    {
        return $this->hasMany(CommentFlags::class)->orderBy('id', 'desc')->first();
    }

    /**
     * Get the comment pictures
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function pictures()
    {
        return $this->belongsToMany(Picture::class, 'comment_pictures');
    }
}
