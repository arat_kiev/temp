<?php

namespace App\Models;

class Gallery extends BaseModel
{
    const PRIO_FRONT = 10;
    const PRIO_TOP   = 4;
    const PRIO_HIGH  = 3;
    const PRIO_MED   = 2;
    const PRIO_LOW   = 1;

    protected $table = 'galleries';

    protected $fillable = ['name', 'description'];

    /**
     * Get the galleries images
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function pictures()
    {
        return $this->belongsToMany(Picture::class, 'gallery_pictures')->withPivot('priority');
    }
}
