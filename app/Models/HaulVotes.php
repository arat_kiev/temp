<?php

namespace App\Models;

class HaulVotes extends BaseModel
{
    protected $table = 'haul_votes';

    protected $fillable = ['haul_id', 'user_id', 'vote'];

    protected $dates = ['created_at', 'updated_at'];

    public static function findVote($haulId, $userId)
    {
        return self::where('haul_id', '=', $haulId)->where('user_id', '=', $userId)->first();
    }

    /**
     * Get the hauls user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Get the hauls haul
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function haul()
    {
        return $this->belongsTo(Haul::class);
    }
}
