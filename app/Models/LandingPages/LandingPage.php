<?php

namespace App\Models\LandingPages;

use App\Models\Area\Area;
use App\Models\BaseModel;
use App\Models\Picture;
use Dimsav\Translatable\Translatable;

class LandingPage extends BaseModel
{
    use Translatable;

    protected $table = 'landingpages';

    protected $fillable = ['type'];

    protected $translatedAttributes = ['slug', 'title', 'headline', 'punchline', 'description'];

    protected $translationForeignKey = 'lp_id';

    protected $with = ['translations', 'picture'];

    protected $slugAttribute = 'name';

    private static $availableTypes = [
        'techniques'                => 'Angeltechniken',
        'fishes'                    => 'Fische',
        'regions'                   => 'Regionen',
        'countries'                 => 'Länder',
        'states'                    => 'Zustände',
        'techniques-fishes-regions' => 'Angeltechniken-Fische-Regionen',
    ];

    public static function getAvailableTypes($detailed = false)
    {
        return $detailed ? self::$availableTypes : array_keys(self::$availableTypes);
    }

    /**
     * Get all areas where this fish can be caught
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function areas()
    {
        return $this->belongsToMany(Area::class, 'landingpages_areas', 'lp_id', 'area_id');
    }

    /**
     * Get the fishes picture
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function picture()
    {
        return $this->belongsTo(Picture::class, 'image_id', 'id');
    }
}