<?php

namespace App\Models\LandingPages;

use App\Models\BaseModel;

class LandingPageTranslation extends BaseModel
{
    protected $table = 'landingpage_translations';

    public $timestamps = false;

    protected $fillable = ['slug', 'title', 'headline', 'punchline', 'description'];
}
