<?php

namespace App\Models;

use App\Models\Contact\Manager;

class Testimonial extends BaseModel
{
    protected $table = 'testimonials';

    protected $fillable = ['person_name', 'person_description', 'text', 'position'];

    /**
     * Get the testimonials manager if available
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo(Manager::class);
    }

    /**
     * Get the testiminals picture
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function picture()
    {
        return $this->belongsTo(Picture::class);
    }
}
