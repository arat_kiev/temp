<?php

namespace App\Models\PointOfInterest;

use Illuminate\Database\Eloquent\Model;

class PointOfInterestOpeningHour extends Model
{
    protected $table = 'point_of_interest_opening_hours';

    protected $fillable = ['day', 'opens', 'closes', 'time_open', 'time_closed'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
