<?php

namespace App\Models\PointOfInterest;

use Illuminate\Database\Eloquent\Model;

class PointOfInterestArea extends Model
{
    protected $table = 'point_of_interest_areas';
}
