<?php

namespace App\Models\PointOfInterest;

use Illuminate\Database\Eloquent\Model;

class PointOfInterestCategoryTranslation extends Model
{
    public $timestamps = false;

    protected $table = 'point_of_interest_category_translations';
}
