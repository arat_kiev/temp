<?php

namespace App\Models\PointOfInterest;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class PointOfInterestCategory extends Model
{
    use Translatable;

    protected $translatedAttributes = ['translation'];

    public $translationForeignKey = 'poi_category_id';

    protected $table = 'point_of_interest_categories';


    public function poi()
    {
        return $this->belongsToMany(PointOfInterest::class, 'point_of_interest_pivot_categories', 'poi_category_id', 'poi_id');
    }
}
