<?php

namespace App\Models\PointOfInterest;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Model;
use App\Models\Location\Geolocation;
use App\Models\Location\Country;
use App\Models\Area\Area;
use App\Models\Picture;
use App\Traits\Draftable;
use Dimsav\Translatable\Translatable;

class PointOfInterest extends Model
{
    use Translatable,
        Draftable;

    protected $translatedAttributes = ['description'];

    public $translationForeignKey = 'poi_id';

    protected $table = 'points_of_interest';

    protected $fillable = ['name', 'street', 'post_code', 'city', 'email', 'phone', 'website',
        'featured_from', 'featured_till', 'picture_id', 'country_id'];

    protected $with = ['translations', 'country'];

    public function picture()
    {
        return $this->belongsTo(Picture::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function opening_hours()
    {
        return $this->hasMany(PointOfInterestOpeningHour::class, 'poi_id');
    }

    public function areas()
    {
        return $this->belongsToMany(Area::class, 'point_of_interest_areas', 'poi_id', 'area_id');
    }

    public function categories()
    {
        return $this->belongsToMany(
            PointOfInterestCategory::class,
            'point_of_interest_pivot_categories',
            'poi_id',
            'poi_category_id'
        );
    }

    public function locations()
    {
        return $this->morphMany(Geolocation::class, 'locatable');
    }

    /**
     * Get the comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
