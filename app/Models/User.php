<?php

namespace App\Models;

use App\Exceptions\NotEnoughBalanceException;
use App\Exceptions\ProfileNotCompleteException;
use App\Models\Area\Area;
use App\Models\Contact\Manager;
use App\Models\Contact\ManagerInspector;
use App\Models\Contact\Rental;
use App\Models\Inspection\InspectionTour;
use App\Models\Inspection\Observation;
use App\Models\Location\State;
use App\Models\Organization\Organization;
use App\Models\Organization\Member;
use App\Models\Contact\Reseller;
use App\Models\License\License;
use App\Models\Location\Country;
use App\Models\Product\ProductSale;
use App\Models\Promo\Coupon;
use App\Models\Promo\Promotion;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\ResellerTicket;
use App\Models\Meta\Fish;
use App\Models\Meta\Technique;
use App\Models\Ticket\TicketInspection;
use Config;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Spatie\Permission\Traits\HasRoles;
use Uuid;

class User extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable,
        Authorizable,
        CanResetPassword,
        HasRoles,
        SoftDeletes;

    const MAX_ENTRIES_VISITS = 12;

    const FISHER_ID_REGEX = '/^[A-Z]{3}-[0-9]{3}-[A-Z]{3}$/';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'first_name', 'last_name',
        'street', 'post_code', 'city', 'birthday', 'phone',
        'google_id', 'facebook_id', 'active', 'country_id', 'fisher_id', 'newsletter', 'activation_code'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'birthday', 'last_activity_at', 'deleted_at'];

    /**
     * The attributes that should be casted to native types
     *
     * @var array
     */
    protected $casts = [
        'visited_areas' => 'array',
    ];

    /**
     * Get the users country of origin
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state()
    {
        return $this->belongsTo(State::class);
    }

    /**
     * Get the users country of origin
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Get the users favorite areas
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function favorites()
    {
        return $this->belongsToMany(Area::class, 'area_favorites');
    }

    /**
     * Get the users hauls
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hauls()
    {
        return $this->hasMany(Haul::class);
    }

    /**
     * Get the users tickets
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    /**
     * Get the ticket inspections
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ticketInspections()
    {
        return $this->hasMany(TicketInspection::class, 'inspector_id');
    }

    /**
     * Get the users product sales
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productSales()
    {
        return $this->hasMany(ProductSale::class);
    }

    /**
     * Get the users licenses
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function licenses()
    {
        return $this->hasMany(License::class);
    }

    /**
     * Get the users rental admin positions
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rentals()
    {
        return $this->belongsToMany(Rental::class, 'rental_admins');
    }

    /**
     * Get the users reseller admin positions
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function resellers()
    {
        return $this->belongsToMany(Reseller::class, 'reseller_admins');
    }

    /**
     * Get the users manager admin positions
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function managers()
    {
        return $this->belongsToMany(Manager::class, 'manager_admins');
    }

    /**
     * Get the users organization admin positions
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function organizations()
    {
        return $this->belongsToMany(Organization::class, 'organization_admins');
    }

    /**
     * Get areas inspected by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function inspectedAreas()
    {
        return $this->belongsToMany(Area::class, 'area_inspectors')->withPivot('is_public');
    }

    /**
     * Get the users memberships in organizations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function memberships()
    {
        return $this->hasMany(Member::class);
    }

    /**
     * Get the users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function signupClient()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * Get user's follows
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function follows()
    {
        return $this->belongsToMany(self::class, 'user_follows_user', 'user_id', 'follower_id')->orderBy('name');
    }

    /**
     * Get user's followers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followed()
    {
        return $this->belongsToMany(self::class, 'user_follows_user', 'follower_id', 'user_id')->orderBy('name');
    }

    /**
     * Get user's favorite fish
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function fishes()
    {
        return $this->belongsToMany(Fish::class, 'user_fishes');
    }

    /**
     * Get user's favorite techniques
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function techniques()
    {
        return $this->belongsToMany(Technique::class, 'user_techniques');
    }

    /**
     * Get the comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * Get the coupons that user used
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function usedCoupons()
    {
        return $this->belongsToMany(Coupon::class, 'user_coupons')->withTimestamps();
    }

    /**
     * Get the coupons that user created
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function createdCoupons()
    {
        return $this->hasMany(Coupon::class, 'created_by');
    }

    /**
     * Get the promotions that user created
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function createdPromotions()
    {
        return $this->hasMany(Promotion::class, 'created_by');
    }

    /**
     * Get user's avatar image
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function avatarPicture()
    {
        return $this->belongsTo(Picture::class);
    }

    /**
     * Get user's hero image
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function heroPicture()
    {
        return $this->belongsTo(Picture::class);
    }

    /**
     * Get managers' inspectors
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function managerInspectors()
    {
        return $this->hasMany(ManagerInspector::class);
    }

    /**
     * Get the last visited areas
     *
     * @return Area[]|null
     */
    public function areas_visited()
    {
        return Area::whereIn('id', array_values($this->visited_areas))->get();
    }

    /**
     * Get inspections tours
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inspectionTours()
    {
        return $this->hasMany(InspectionTour::class, 'inspector_id', 'id');
    }

    public function observations()
    {
        return $this->hasManyThrough(Observation::class, InspectionTour::class, 'inspector_id', 'tour_id', 'id');
    }

    /**
     * Add area to last visited areas
     *
     * @param Area $area
     */
    public function addVisitedArea(Area $area)
    {
        if (!in_array((int)$area->id, $this->visited_areas, true)) {
            array_push($this->visited_areas, (int)$area->id);

            if (count($this->visited_areas) > self::MAX_ENTRIES_VISITS) {
                array_shift($this->visited_areas);
            }
        }

        $this->save();
    }

    /**
     * Add credit to users balance
     *
     * @param $amount
     */
    public function addCredit($amount)
    {
        $this->balance += $amount;
        $this->save();
    }

    public function removeCredit($amount)
    {
        $this->balance -= $amount;
        $this->save();
    }

    /**
     * Remove credit from users balance
     *
     * @param $amount
     * @throws NotEnoughBalanceException
     */
    public function useCredit($amount)
    {
        if ($this->hasEnoughCredit($amount)) {
            $this->removeCredit($amount);
        } else {
            throw new NotEnoughBalanceException($this->balance - $amount);
        }
    }

    /**
     * Check if user has balance more than or equal the amount
     *
     * @param $amount
     * @return bool
     */
    public function hasEnoughCredit($amount)
    {
        return $this->balance >= $amount;
    }

    /**
     * Check if user is a member of organization
     *
     * @param Organization $organization
     * @return boolean
     */
    public function isMemberOf(Organization $organization)
    {
        return $this->memberships()->pluck('organization_id')->contains($organization->id);
    }

    /**
     * Get the full name of the user
     *
     * @param $value
     * @return string
     * @throws ProfileNotCompleteException
     */
    public function getFullNameAttribute($value)
    {
        if (empty($this->first_name) || empty($this->last_name)) {
            return null;
        }

        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Mutator: automatically hash supplied password
     *
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /**
     * Check if profile is completed
     *
     * @param $value
     * @return bool
     */
    public function getCompleteAttribute($value)
    {
        return $this->active && $this->country != null &&
            !empty($this->first_name) && !empty($this->last_name) &&
            !empty($this->street) && !empty($this->post_code) &&
            !empty($this->city) && !empty($this->birthday);
    }

    /**
     * Generates unique fisher_id
     * @return string
     */
    public static function uniqueFisherId()
    {
        do {
            $fisher_id = str_random_formatted(9, 3, 'alternate');
        } while (User::where('fisher_id', $fisher_id)->exists() ||
        ResellerTicket::where('fisher_id', $fisher_id)->exists() ||
        Member::where('fisher_id', $fisher_id)->exists());

        return $fisher_id;
    }

    /**
     * Get only `inspectors` which belongs to certain manager
     *
     * @param $query
     * @param Manager $manager
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBelongsToManagerInspector($query, Manager $manager)
    {
        return $query->whereHas('managerInspectors', function ($query) use ($manager) {
            return $query->where('manager_id', '=', $manager->id);
        });
    }

    /**
     * Get only `inspectors` which does not belongs to certain manager
     *
     * @param $query
     * @param Manager $manager
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDoesNotBelongsToManagerInspector($query, Manager $manager)
    {
        return $query->whereDoesntHave('managerInspectors', function ($query) use ($manager) {
            return $query->where('manager_id', '=', $manager->id);
        });
    }

    /**
     * Register model event handlers
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function (User $user) {
            $user->signupClient()->associate(Config::get('app.client'));
            $user->activation_code = Uuid::generate(4);
            $user->fisher_id = $user->fisher_id ?: self::uniqueFisherId();
        });

        static::updating(function (User $user) {
            if (!$user->getOriginal('active') && $user->active) {
                $user->activation_code = null;
            }
        });
    }
}
