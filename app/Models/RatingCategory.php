<?php

namespace App\Models;

use Illuminate\Support\Facades\App;
use Dimsav\Translatable\Translatable;

class RatingCategory extends BaseModel
{
    use Translatable;

    protected $table = 'rating_categories';

    protected $translatedAttributes = ['name', 'description'];

    protected $fillable = ['name', 'description'];

    protected $with = ['translations'];
}
