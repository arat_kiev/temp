<?php

namespace App\Models;

use App\Models\Location\Country;
use App\Models\Product\Product;
use App\Models\Product\ProductStock;

class Stock extends BaseModel
{
    protected $table = 'stocks';

    protected $fillable = ['name', 'city', 'street', 'building', 'rest_info', 'emails', 'phones'];
    
    protected $dates = ['created_at', 'updated_at'];
    
    protected $casts = [
        'emails'    => 'array',
        'phones'    => 'array',
    ];

    protected $with = ['country'];

    /**
     * Get the stock country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Get the stock products
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_stocks')->using(ProductStock::class);
    }

    public function getAddressAttribute()
    {
        return $this->rest_info
            ? $this->street . ' ' . $this->building . ', ' . $this->rest_info
            : $this->street . ' ' . $this->building;
    }
}
