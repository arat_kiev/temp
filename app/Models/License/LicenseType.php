<?php

namespace App\Models\License;

use App\Models\BaseModel;
use App\Models\Location\State;
use App\Models\Picture;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\JoinClause;

class LicenseType extends BaseModel
{
    use SoftDeletes;

    protected $table = 'license_types';

    protected $fillable = ['name', 'fields', 'attachments'];

    protected $perPage = 90;

    protected $casts = [
        'fields'        => 'array',
        'attachments'   => 'array',
    ];
    
    const ALLOWED_FIELD_TYPES = [
        'Datum' => 'date',
        'Reihe' => 'string',
        'Seit'  => 'from',
        'Bis'   => 'till',
    ];

    /**
     * Get all licenses with this license type
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function licenses()
    {
        return $this->hasMany(License::class);
    }

    /**
     * Get the license types example pictures
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function examples()
    {
        return $this->belongsToMany(Picture::class, 'license_examples');
    }

    /**
     * Get the states where license type could be available
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function states()
    {
        return $this->belongsToMany(State::class, 'license_type_states')->withTimestamps();
    }

    /**
     * Get only license types in specified state
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $state
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasState($query, $state)
    {
        return $query
            ->join('license_type_states', 'license_types.id', '=', 'license_type_states.license_type_id')
            ->join('states', function (JoinClause $join) use ($state) {
                $join->on('states.id', '=', 'license_type_states.state_id')
                    ->where('states.id', '=', $state);
            })
            ->groupBy('license_types.id');
    }
}
