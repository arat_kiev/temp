<?php

namespace App\Models\License;

use App\Models\BaseModel;
use App\Models\Picture;
use App\Models\User;
use Carbon\Carbon;

class License extends BaseModel
{
    protected $table = 'licenses';

    protected $fillable = ['fields'];

    protected $dates = ['valid_from', 'valid_to', 'created_at', 'updated_at'];

    protected $casts = [
        'fields' => 'array',
    ];

    /**
     * Get the licenses type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(LicenseType::class, 'license_type_id');
    }

    /**
     * Get the licenses user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Get the licenses attachments
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attachments()
    {
        return $this->belongsToMany(Picture::class, 'license_attachments');
    }

    /**
     * Scope a query to only include valid licenses
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param bool $valid
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeValid($query, $valid = true)
    {
        $now = Carbon::now()->toDateString();

        if ($valid) {
            return $query->whereRaw('status = ?', ['ACCEPTED'])
                ->whereRaw('? BETWEEN COALESCE(valid_from, ?) AND COALESCE(valid_to, ?)', [$now, $now, $now]);
        } else {
            return $query->whereRaw('status != ?', ['ACCEPTED'])
                ->orWhereRaw('? NOT BETWEEN COALESCE(valid_from, ?) AND COALESCE(valid_to, ?)', [$now, $now, $now]);
        }
    }
}
