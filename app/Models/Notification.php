<?php

namespace App\Models;

class Notification extends BaseModel
{
    protected $table = 'notifications';

    protected $fillable = ['type', 'user_id', 'subject_id', 'object_id', 'object_type'];

    protected $dates = ['created_at', 'read_at'];

    /**
     * Get owner of notification
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function belongsToUser()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Get sender of notification
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function fromUser()
    {
        return $this->hasOne(User::class, 'id', 'subject_id');
    }

    /**
     * Notifications filter; 1 - old, 0 - new
     * @param $query \Illuminate\Database\Eloquent\Builder
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRead($query, $status = false)
    {
        return $status ? $query->whereNotNull('read_at') : $query->whereNull('read_at');
    }

    /**
     * Get notifications with specific type
     * @param $query \Illuminate\Database\Eloquent\Builder
     * @param array $types
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasTypes($query, array $types)
    {
        return $query->whereIn('notifications.type', $types);
    }

    /**
     * Update the creation and update timestamps.
     *
     * @return void
     */
    protected function updateTimestamps()
    {
        $time = $this->freshTimestamp();

        if (! $this->exists && ! $this->isDirty(static::CREATED_AT)) {
            $this->setCreatedAt($time);
        }
    }
}
