<?php

namespace App\Models;

use App\Models\Contact\Manager;

class StornoNotification extends BaseModel
{
    protected $table = 'storno_notifications';

    protected $fillable = ['email'];

    protected $dates = ['created_at', 'updated_at'];

    public function manager()
    {
        return $this->belongsTo(Manager::class);
    }

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = trim($value);
    }
}
