<?php

namespace App\Models\Location;

use App\Models\BaseModel;

class Geolocation extends BaseModel
{
    const MIN_LAT = -1.5707963267949;
    const MAX_LAT =  1.5707963267949;
    const MIN_LON = -3.1415926535898;
    const MAX_LON =  3.1415926535898;

    const R = 6371.01;

    protected $table = 'geolocations';

    public $timestamps = false;

    protected $fillable = ['longitude', 'latitude'];

    protected $casts = [
        'longitude' => 'double',
        'latitude' => 'double',
    ];

    /**
     * Get the polymorpic relation (City, Area, ...)
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function locatable()
    {
        return $this->morphTo();
    }

    /**
     * Get the location as Lon/Lat string
     *
     * @param $value
     * @return string
     */
    public function getLonLatStringAttribute($value)
    {
        $lat = str_replace(',', '.', strval($this->latitude));
        $lon = str_replace(',', '.', strval($this->longitude));

        return "$lon,$lat";
    }

    /**
     * Get the location as Lat/Lon string
     *
     * @param $value
     * @return string
     */
    public function getLatLonStringAttribute($value)
    {
        $lat = str_replace(',', '.', strval($this->latitude));
        $lon = str_replace(',', '.', strval($this->longitude));

        return "$lat,$lon";
    }

    /**
     * Get the location as Lon/Lat string
     *
     * @param $value
     * @return string
     */
    public function setLonLatStringAttribute($value)
    {
        list($this->longitude, $this->latitude) = explode(',', $value);
    }

    /**
     * Get the location as Lat/Lon string
     *
     * @param $value
     * @return string
     */
    public function setLatLonStringAttribute($value)
    {
        list($this->latitude, $this->longitude) = explode(',', $value);
    }

    /**
     * Get the bounding box for the given distance
     *
     * @param $distance
     * @return array|float[]
     */
    public function getBoundingBox($distance)
    {
        $radDist = $distance / self::R;

        $minLat = deg2rad($this->latitude) - $radDist;
        $maxLat = deg2rad($this->latitude) + $radDist;

        if ($minLat > self::MIN_LAT && $maxLat < self::MAX_LAT) {
            $deltaLon = asin(sin($radDist) / cos(deg2rad($this->latitude)));

            $minLon = deg2rad($this->longitude) - $deltaLon;
            if ($minLon < self::MIN_LON) {
                $minLon += 2 * pi();
            }

            $maxLon = deg2rad($this->longitude) + $deltaLon;
            if ($maxLon > self::MAX_LON) {
                $maxLon -= 2 * pi();
            }
        } else {
            $minLat = max($minLat, self::MIN_LAT);
            $maxLat = min($maxLat, self::MAX_LAT);
            $minLon = self::MIN_LON;
            $maxLon = self::MAX_LON;
        }

        return [
            $minLat * 180 / M_PI, $minLon * 180 / M_PI,
            $maxLat * 180 / M_PI, $maxLon * 180 / M_PI,
        ];
    }
}
