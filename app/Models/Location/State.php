<?php

namespace App\Models\Location;

use App\Models\Area\Rule;
use App\Models\BaseModel;
use App\Models\License\LicenseType;
use App\Models\Meta\Fish;
use App\Traits\Sluggable;
use Dimsav\Translatable\Translatable;

class State extends BaseModel
{
    use Translatable,
        Sluggable;

    protected $table = 'states';

    public $timestamps = false;

    protected $fillable = ['name', 'description'];

    protected $translatedAttributes = ['name', 'description'];

    protected $with = ['translations', 'country', 'rule'];

    protected $slugAttribute = 'name';

    /**
     * Get the states country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Get the states regions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function regions()
    {
        return $this->hasMany(Region::class);
    }

    /**
     * Get the states fishes/restrictions
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function fishes()
    {
        return $this->belongsToMany(Fish::class, 'state_fishes')
            ->withPivot('closed_from', 'closed_till', 'min_length', 'max_length');
    }

    /**
     * Get the state rules
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rule()
    {
        return $this->belongsTo(Rule::class);
    }

    /**
     * Get the license types related to state
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function licenseTypes()
    {
        return $this->belongsToMany(LicenseType::class, 'license_type_states')->withTimestamps();
    }

    /**
     * Get the affiliates
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function affiliates()
    {
        return $this->morphToMany(Affiliate::class, 'related', 'affiliables');
    }
}
