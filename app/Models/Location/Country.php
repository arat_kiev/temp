<?php

namespace App\Models\Location;

use App\Models\BaseModel;
use App\Models\Product\Fee;
use App\Models\Stock;
use App\Models\Affiliate;
use App\Traits\Sluggable;
use Dimsav\Translatable\Translatable;

class Country extends BaseModel
{
    use Translatable,
        Sluggable;

    protected $table = 'countries';

    public $timestamps = false;

    protected $fillable = ['area_code', 'vat', 'country_code', 'name', 'description'];

    protected $translatedAttributes = ['name', 'description'];

    protected $with = ['translations'];

    protected $slugAttribute = 'name';

    /**
     * Get the countries states
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function states()
    {
        return $this->hasMany(State::class);
    }

    /**
     * Get the countries fees
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fees()
    {
        return $this->hasMany(Fee::class);
    }

    /**
     * Get the countries stocks
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }

    /**
     * Get the affiliates
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function affiliates()
    {
        return $this->morphToMany(Affiliate::class, 'related', 'affiliables');
    }
}
