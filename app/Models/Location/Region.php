<?php

namespace App\Models\Location;

use App\Models\BaseModel;
use App\Traits\Sluggable;
use Dimsav\Translatable\Translatable;

class Region extends BaseModel
{
    use Translatable,
        Sluggable;

    protected $table = 'regions';

    public $timestamps = false;

    protected $fillable = ['name', 'description'];

    protected $translatedAttributes = ['name', 'description'];

    protected $with = ['translations', 'state'];

    protected $slugAttribute = 'name';

    /**
     * Get the regions state
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state()
    {
        return $this->belongsTo(State::class);
    }

    /**
     * Get the regions cities
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
