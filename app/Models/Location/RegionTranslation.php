<?php

namespace App\Models\Location;

use App\Models\BaseModel;

class RegionTranslation extends BaseModel
{
    public $timestamps = false;

    protected $fillable = ['name', 'description'];
}
