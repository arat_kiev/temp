<?php

namespace App\Models\Location;

use App\Models\Area\Area;
use App\Models\BaseModel;
use App\Traits\Sluggable;
use Dimsav\Translatable\Translatable;

class City extends BaseModel
{
    use Translatable,
        Sluggable;

    protected $table = 'cities';

    public $timestamps = false;

    protected $fillable = ['name', 'description', 'post_code'];

    protected $translatedAttributes = ['name', 'description'];

    protected $with = ['translations', 'region'];

    protected $slugAttribute = 'name';

    /**
     * Get the cities region
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    /**
     * Get the cities geolocation
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function location()
    {
        return $this->morphOne(Geolocation::class, 'locatable');
    }

    /**
     * Get the cities nearby areas
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function areas()
    {
        return $this->belongsToMany(Area::class, 'area_cities');
    }

    /**
     * Get only cities within the bounding box
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInBoundingBox($query, $north, $east, $south, $west)
    {
        return $query->select('cities.*')
            ->leftJoin('geolocations', function ($join) use ($north, $east, $south, $west) {
                $join->on('cities.id', '=', 'geolocations.locatable_id')
                    ->where('locatable_type', '=', 'cities');
            })->whereNotNull('locatable_id')
                ->whereBetween('longitude', [number_format($west, 8, '.', ''), number_format($east, 8, '.', '')])
                ->whereBetween('latitude', [number_format($south, 8, '.', ''), number_format($north, 8, '.', '')])
                ->distinct();
    }
}
