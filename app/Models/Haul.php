<?php

namespace App\Models;

use App\Models\Area\Area;
use App\Models\Meta\Fish;
use App\Models\Meta\Technique;
use App\Models\Ticket\Checkin;
use App\Models\Ticket\Ticket;

class Haul extends BaseModel
{
    protected $table = 'hauls';

    protected $fillable = [
        'area_id',
        'area_name',
        'ticket_id',
        'fish_id',
        'technique_id',
        'picture_id',
        'status',
        'public',
        'taken',
        'count',
        'size_value',
        'size_type',
        'weather',
        'temperature',
        'user_comment',
        'catch_date',
        'catch_time',
        'ticket_number',
    ];

    protected $dates = ['created_at', 'updated_at', 'catch_date'];

    protected $with = ['user', 'area', 'fish', 'ticket', 'picture'];

    const AVAILABLE_SIZE_TYPES = [
        ''      => '',
        'KG'    => 'KG',
        'CM'    => 'CM',
    ];

    const AVAILABLE_WEATHER = [
        '-'         => '',
        'Sonnig'    => 'sunny',
        'Wolkig'    => 'cloudy',
        'Regnet'    => 'rain',
        'Schnee'    => 'snow',
    ];

    /**
     * Register model event handlers
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function (Haul $haul) {
            if ($haul->public && $haul->picture && $haul->technique && $haul->technique->gallery) {
                $haul->technique->gallery->pictures()->attach($haul->picture);
            }
        });

        static::updating(function (Haul $haul) {
            if ($haul->picture && $haul->technique && ($gallery = $haul->technique->gallery) && $gallery->pictures) {
                $gallery->pictures()->detach($haul->picture);
                if ($haul->public) {
                    $gallery->pictures()->attach($haul->picture);
                }
            }
        });

        static::deleting(function (Haul $haul) {
            $haul->picture()->delete();
        });
    }

    /**
     * Get the hauls user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Get the hauls area
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    /**
     * Get the hauls fish
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fish()
    {
        return $this->belongsTo(Fish::class);
    }

    /**
     * Get checkin
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function checkin()
    {
        return $this->belongsTo(Checkin::class);
    }

    /**
     * Get the hauls technique
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function technique()
    {
        return $this->belongsTo(Technique::class);
    }

    /**
     * Get the hauls ticket (if available)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

    /**
     * Get the hauls picture
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function picture()
    {
        return $this->belongsTo(Picture::class);
    }

    public function getCatchTimeAttribute($value)
    {
        return substr($value, 0, 5);
    }

    /**
     * Get total count of all votes per haul
     *
     * @return mixed
     */
    public function votes()
    {
        return $this->hasMany(HaulVotes::class)->sum('vote');
    }

    /**
     * Get total count of all votes per haul
     *
     * @return mixed
     */
    public function likes()
    {
        return $this->hasMany(HaulVotes::class)->where('vote', '1')->count();
    }

    /**
     * Get total count of all votes per haul
     *
     * @return mixed
     */
    public function dislikes()
    {
        return $this->hasMany(HaulVotes::class)->where('vote', '-1')->count();
    }

    /**
     * Get the hauls reports
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function report()
    {
        return $this->hasMany(HaulFlags::class)->orderBy('id', 'desc')->first();
    }

    /**
     * Get the comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * Get only hauls with/without fish_id
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param boolean $with
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasFish($query, $with)
    {
        return $with ? $query->whereNotNull('fish_id') : $query->whereNull('fish_id');
    }

    /**
     * Get only hauls with certain fishes
     *
     * @param $query
     * @param $fishes
     * @return mixed
     */
    public function scopeHasFishes($query, $fishes)
    {
        return $query->whereIn('hauls.fish_id', $fishes);
    }
    /**
     * Get only `hauls` which belongs to certain cities
     * @param $query
     * @param $cities
     */
    public function scopeBelongsToCities($query, $cities)
    {
        $query->whereHas('area.cities', function ($query) use ($cities) {
            $query->whereIn('id', $cities);
        });
    }

    /**
     * Get only `hauls` which belongs to certain regions
     * @param $query
     * @param $regions
     */
    public function scopeBelongsToRegions($query, $regions)
    {
        $query->whereHas('area.cities.region', function ($query) use ($regions) {
            $query->whereIn('id', $regions);
        });
    }

    /**
     * Get only `hauls` which belongs to certain states
     * @param $query
     * @param $states
     */
    public function scopeBelongsToStates($query, $states)
    {
        $query->whereHas('area.cities.region.state', function ($query) use ($states) {
            $query->whereIn('id', $states);
        });
    }

    /**
     * Get only `hauls` which belongs to certain countries
     * @param $query
     * @param $countries
     */
    public function scopeBelongsToCountries($query, $countries)
    {
        $query->whereHas('area.cities.region.state.country', function ($query) use ($countries) {
            $query->whereIn('id', $countries);
        });
    }

    /**
     * Virtual attribute - Weight
     * @return object
     */
    public function getWeightAttribute()
    {
        $type = 'KG';
        $weight = ['type' => null, 'value' => null];
        if ($this->size_type == $type) {
            $weight['type'] = $this->size_type;
            $weight['value'] = $this->size_value;
        }
        return (object)$weight;
    }

    /**
     * Virtual attribute - Length
     * @return object
     */
    public function getLengthAttribute()
    {
        $type = 'CM';
        $length = ['type' => null, 'value' => null];
        if ($this->size_type == $type) {
            $length['type'] = $this->size_type;
            $length['value'] = $this->size_value;
        }
        return (object)$length;
    }

    /**
     * Replace `dot` with `comma` in `size_value` attribute
     * @param $size_value
     * @return string
     */
    public function getSizeValueAttribute($size_value)
    {
        return $this->attributes['size_value'] = number_format((float)$size_value, 2, '.', '');
    }

    /**
     * Get only hauls for specific area
     *
     * @param $query
     * @param $areaId
     * @return mixed
     */
    public function scopeHasArea($query, $areaId)
    {
        return $query->where('area_id', '=', $areaId);
    }

    /**
     * Get only hauls with/without picture_id
     *
     * @param $query
     * @param $with
     * @return mixed
     */
    public function scopeHasPicture($query, $with)
    {
        return $with ? $query->whereNotNull('picture_id') : $query->whereNull('picture_id');
    }

    /**
     * Get only hauls with certain techniques
     *
     * @param $query
     * @param $techniques
     * @return mixed
     */
    public function scopeHasTechnique($query, $techniques)
    {
        return $query->whereIn('hauls.technique_id', $techniques);
    }

    /**
     * Get only hauls with status taken (for statistics)
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param bool                                  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsTaken($query, $value = true)
    {
        return $query->where('taken', '=', $value);
    }

    /**
     * Get only hauls which are empty reports (Leermeldung)
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param bool $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsEmptyReport($query, $value = true)
    {
        return $value ? $query->whereNull('fish_id') : $query->whereNotNull('fish_id');
    }
}
