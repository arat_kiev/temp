<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;
use Dimsav\Translatable\Translatable;

class CheckinUnit extends BaseModel
{
    use Translatable;

    protected $table = 'checkin_units';

    protected $fillable = ['type', 'name'];

    protected $translatedAttributes = ['name'];

    protected $with = ['translations'];
}
