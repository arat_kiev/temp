<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class LockPeriod extends BaseModel
{
    protected $table = 'ticket_lock_periods';

    protected $fillable = ['lock_from', 'lock_till'];

    protected $dates = ['created_at', 'updated_at', 'lock_from', 'lock_till'];

    public function getLockFromStringAttribute()
    {
        return $this->lock_from->format('d.m.Y');
    }

    public function getLockTillStringAttribute()
    {
        return $this->lock_till->format('d.m.Y');
    }
}
