<?php

namespace App\Models\Ticket;

use Illuminate\Database\Eloquent\Model;

class TicketDay extends Model
{
    protected $table = 'ticket_days';

    protected $fillable = ['weekday'];

    public $timestamps = false;

    /**
     * Get the owning ticket type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(TicketType::class, 'ticket_type_id');
    }
}
