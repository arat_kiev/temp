<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TicketPriceTranslation extends BaseModel
{
    protected $table = 'ticket_price_translations';

    public $timestamps = false;

    protected $fillable = ['name'];
}
