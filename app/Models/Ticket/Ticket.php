<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;
use App\Models\Client;
use App\Models\Commission;
use App\Models\Haul;
use App\Models\User;
use App\Models\Contact\Manager;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends BaseModel
{
    use SoftDeletes;

    protected $table = 'tickets';

    protected $fillable = [
        'valid_from', 'valid_to', 'manager_ticket_id', 'is_deleted',
        'storno_id', 'storno_reason', 'group_data', 'commission_value'
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'valid_from', 'valid_to'];

    protected $casts = [
        'offisy_tags' => 'array',
        'group_data' => 'array',
    ];

    const IDENT_FORMAT = 'BA-%d-%06d';

    const IDENT_REGEX = '/^BA-(\d{1,2})-(\d{1,6})$/';

    const IDENT_FORMAT_MANAGER = '%s-%d-%06d';

    /**
     *
     */
    public static function boot() {
        parent::boot();

        // create a event to happen on deleting
        static::deleting(function ($table)  {
            $table->is_deleted = 1;
            $table->storno_reason = self::$stornoReason;
            $table->save();
        });
    }

    public static $stornoReason = '';

    /**
     * Get the tickets type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(TicketType::class, 'ticket_type_id');
    }

    /**
     * Get the tickets price
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function price()
    {
        return $this->belongsTo(TicketPrice::class, 'ticket_price_id');
    }

    /**
     * Get the tickets owner
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Get the tickets client (bissanzeiger, bundesforste, ...)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * Get the ticket invoice
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    /**
     * Get the ticket superadmin invoice
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */
    public function hfInvoice()
    {
        return $this->belongsTo(Invoice::class, 'hf_invoice_id');
    }

    /**
     * Get the corresponding reseller ticket
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function resellerTicket()
    {
        return $this->hasOne(ResellerTicket::class);
    }

    /**
     * Get the tickets checkins
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function checkins()
    {
        return $this->hasMany(Checkin::class);
    }

    /**
     * Get the tickets hauls
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hauls()
    {
        return $this->hasMany(Haul::class);
    }

    /**
     * Get the ticket inspections
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inspections()
    {
        return $this->hasMany(TicketInspection::class);
    }

    /**
     * Get the tickets active status
     *
     * @param $value
     * @return bool
     */
    public function getActiveAttribute($value)
    {
        $now = Carbon::now();

        return $now->between($this->valid_from, $this->valid_to);
    }

    /**
     * Get the commission value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getCommissionValueOriginAttribute($value)
    {
        return $this->attributes['commission_value'];
    }

    /**
     * Get the commission value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getCommissionValueAttribute($value)
    {
        return number_format($value / 100.0, 2, ',', '.');
    }

    /**
     * @return mixed
     */
    public function getTotalPriceOriginAttribute()
    {
        // HINT:
        //    `tickets.commission_value` must be always included to `select`,
        //    otherwise it could be overwritten with `reseller_tickets.commission_value`
        return $this->price->value + $this->commission_value_origin;
    }

    /**
     * @return string
     */
    public function getTotalPriceAttribute()
    {
        return number_format($this->total_price_origin / 100.0, 2, ',', '.');
    }

    /**
     * Filter query by active status
     *
     * @param $query
     * @param bool $active
     * @return mixed
     */
    public function scopeActive($query, $active = true)
    {
        $now = Carbon::now()->toDateString();

        if ($active) {
            return $query->whereRaw('? BETWEEN valid_from AND valid_to', [$now]);
        } else {
            return $query->whereRaw('? NOT BETWEEN valid_from AND valid_to', [$now]);
        }
    }

    /**
     * Filter query by required catch log
     *
     * @param      $query
     * @param bool $required
     * @return mixed
     */
    public function scopeRequiredCatchlog($query, bool $required = true)
    {
        return $query->whereHas('type', function ($type) use ($required) {
            return $type->where('is_catchlog_required', $required);
        });
    }

    /**
     * Filter out storno tickets
     *
     * @param $query
     * @return mixed
     */
    public function scopeWithoutStorno($query)
    {
        return $query->whereNull('storno_id');
    }

    /**
     * Scope for inspection tickets
     *
     * @param $query
     * @param null $validFrom
     * @param null $validTo
     * @return mixed
     */
    public function scopeInspectionTickets($query, $validFrom = null, $validTo = null)
    {
        if (!is_null($validFrom)) {
            $query->where('valid_from', '>=', $validFrom);
        }

        if (!is_null($validTo)) {
            $query->where('valid_to', '<=', $validTo);
        }

        return $query->orderBy('id', 'desc');
    }
}
