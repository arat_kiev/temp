<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TicketCategoryTranslation extends BaseModel
{
    protected $table = 'ticket_category_translations';

    public $timestamps = false;

    protected $fillable = ['name'];
}
