<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;
use App\Models\Inspection\InspectionTour;
use App\Models\Inspection\TicketInspectionHaul;
use App\Models\User;

class TicketInspection extends BaseModel
{
    protected $table = 'ticket_inspections';

    protected $fillable = ['status', 'comment', 'location', 'ticket_id', 'tour_id', 'gallery_id'];

    protected $dates = ['created_at', 'updated_at'];

    protected $casts = [
        'location'  => 'array',
    ];

    const AVAILABLE_STATUSES = [
        'In Ordnung' => 'OK',
        'Nicht OK'   => 'NOT_OK',
    ];

    /**
     * Get inspected ticket
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

    /**
     * Get inspector
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inspector()
    {
        return $this->belongsTo(User::class, 'inspector_id');
    }

    /**
     * Get inspection tour
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inspectionTour()
    {
        return $this->belongsTo(InspectionTour::class, 'tour_id', 'id');
    }

    /**
     * Get ticket inspection hauls
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hauls()
    {
        return $this->hasMany(TicketInspectionHaul::class, 'inspection_id', 'id');
    }

    /**
     * @return null
     */
    public function getHumanReadableStatusAttribute()
    {
        return $this->status
            ? array_flip(self::AVAILABLE_STATUSES)[$this->status]
            : null;
    }

    /**
     * @param $value
     * @return array|mixed|null
     */
    public function getLocationAttribute($value)
    {
        $arrayValue = $value ? json_decode($value, true) : null;

        return $arrayValue && isset($arrayValue['latitude']) && isset($arrayValue['longitude'])
            ? $arrayValue
            : [];
    }
}
