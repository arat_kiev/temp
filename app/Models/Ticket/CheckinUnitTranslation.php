<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class CheckinUnitTranslation extends BaseModel
{
    protected $table = 'checkin_unit_translations';

    public $timestamps = false;

    protected $fillable = ['name'];
}
