<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class QuotaUnitTranslation extends BaseModel
{
    protected $table = 'quota_unit_translations';

    public $timestamps = false;

    protected $fillable = ['name'];
}
