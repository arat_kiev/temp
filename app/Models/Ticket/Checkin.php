<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;
use App\Models\Meta\FishingMethod;
use App\Models\Haul;

class Checkin extends BaseModel
{
    protected $table = 'checkins';

    protected $fillable = ['ticket_id', 'from', 'till'];

    protected $dates = ['created_at', 'updated_at', 'from', 'till'];

    /**
     * Get the checkins corresponding ticket
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo+
     */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

    /**
     * Get checkin hauls
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hauls()
    {
        return $this->hasMany(Haul::class);
    }

    /**
     * Get checkin fishing methods
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function fishingMethods()
    {
        return $this->belongsToMany(FishingMethod::class);
    }

    /**
     * Get the checkins duration in seconds
     *
     * @return int
     */
    public function getDurationInSecondsAttribute()
    {
        return $this->from->diffInSeconds($this->till);
    }

    /**
     * Get the checkins duration in minutes
     *
     * @return int
     */

    public function getDurationInMinutesAttribute()
    {
        return $this->from->diffInMinutes($this->till);
    }

    /**
     * Get the checkins duration in hours
     *
     * @return int
     */
    public function getDurationInHoursAttribute()
    {
        return $this->from->diffInHours($this->till);
    }
}
