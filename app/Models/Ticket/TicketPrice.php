<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;
use App\Models\Commission;
use App\Models\Contact\Reseller;
use App\Models\Organization\Organization;
use Carbon\Carbon;
use Dimsav\Translatable\Translatable;
use Gloudemans\Shoppingcart\Contracts\Buyable;

class TicketPrice extends BaseModel implements Buyable
{
    use Translatable;

    protected $table = 'ticket_prices';

    protected $fillable = ['type', 'name', 'value', 'age', 'group_count',
        'show_vat', 'valid_from', 'valid_to', 'visible_from'];

    protected $translatedAttributes = ['name'];

    protected $with = ['translations'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'valid_from', 'valid_to', 'visible_from'];

    /**
     * Get the prices parent ticket type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticketType()
    {
        return $this->belongsTo(TicketType::class);
    }

    /**
     * Get the tickets with current ticket price
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    /**
     * Get the prices allowed week days
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function weekdays()
    {
        return $this->hasMany(TicketDay::class);
    }

    /**
     * Get the prices lock periods
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lockPeriods()
    {
        return $this->hasMany(LockPeriod::class);
    }

    /**
     * Get the prices associated resellers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function resellers()
    {
        return $this->belongsToMany(Reseller::class, 'reseller_ticket_prices')->withPivot(['commission_type', 'commission_value']);
    }

    /**
     * Get the prices associated organizations (only for member prices)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function organizations()
    {
        return $this->belongsToMany(Organization::class, 'ticket_price_organizations')->withTimestamps();
    }

    /**
     * Get the commissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function commissions()
    {
        return $this->morphMany(Commission::class, 'related');
    }

    /**
     * @param bool $isOnline
     * @return mixed
     */
    public function commissionsWithFallback($isOnline = true)
    {
        return $this->commissions()
            ->where('is_online', $isOnline)
            ->where(function ($query) {
                $query->whereDate('valid_till', '>=', Carbon::now())
                    ->orWhereNull('valid_till');
            })
            ->orderBy('created_at')
            ->first()
            ?: $this->ticketType->commissionsWithFallback($isOnline);
    }

    public function getNameWithYearAttribute()
    {
        return $this->name . ' (' . $this->getYearSuffix() . ')';
    }

    private function getYearSuffix()
    {
        $startYear = (string)$this->valid_from->year;
        $endYear = (string)$this->valid_to->year;

        if ($startYear === $endYear) {
            return $startYear;
        } else {
            return substr($startYear, 2, 2) . '/' . substr($endYear, 2, 2);
        }
    }

    /**
     * Get only prices which are active (not member only)
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param bool $active
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query, $active = true)
    {
        $now = Carbon::now()->toDateString();

        if ($active) {
            return $query->whereRaw('? BETWEEN visible_from AND valid_to', [$now]);
        } else {
            return $query->whereRaw('? NOT BETWEEN visible_from AND valid_to', [$now]);
        }
    }

    /**
     * Select described name to be displayed in Form Builder
     *
     * @return $this
     */
    public function scopeSelectDescribedName()
    {
        return $this
            ->listsTranslations('name')
            ->selectRaw(
                "CONCAT(
                    '#',
                    ticket_prices.id,
                    ' ',
                    ticket_price_translations.name,
                    ' (',
                    ticket_prices.type,
                    ') - von ',
                    ticket_prices.valid_from,
                    ' bis ',
                    ticket_prices.valid_to
                ) as described_name"
            )
            ->addSelect('ticket_prices.id')
            ->groupBy('ticket_prices.id');
    }

    /**
     * Get the identifier of the Buyable item.
     *
     * @return int|string
     */
    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    /**
     * Get the description or title of the Buyable item.
     *
     * @return string
     */
    public function getBuyableDescription($options = null)
    {
        return "{$this->ticketType->area->name} - {$this->ticketType->name} ({$this->name})";
    }

    /**
     * Get the price of the Buyable item.
     *
     * @return float
     */
    public function getBuyablePrice($options = null)
    {
        return $this->value / 100.0;
    }

    /**
     * Get the type of the Buyable item.
     *
     * @return string
     */
    public function getBuyableType($options = null)
    {
        return 'ticket';
    }
}
