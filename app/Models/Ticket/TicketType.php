<?php

namespace App\Models\Ticket;

use App\Models\Area\Area;
use App\Models\Area\Rule;
use App\Models\BaseModel;
use App\Models\Commission;
use App\Models\License\LicenseType;
use App\Models\Promo\Promotion;
use App\Models\Promo\PromotionTarget;
use App\Models\User;
use App\Scopes\TenantScope;
use Carbon\Carbon;
use Dimsav\Translatable\Translatable;

class TicketType extends BaseModel
{
    use Translatable;

    protected $table = 'ticket_types';

    protected $fillable = [
        'name',
        'duration',
        'online_only',
        'group',
        'fish_per_day',
        'fishing_days',
        'day_starts_at_sunrise',
        'day_starts_at_sunset',
        'day_starts_at_time',
        'day_ends_at_sunrise',
        'day_ends_at_sunset',
        'day_ends_at_time',
        'checkin_max',
        'quota_max',
        'is_catchlog_required',
        'account_number',
    ];

    protected $translatedAttributes = ['name'];

    protected $with = ['translations', 'rule', 'prices'];

    /**
     * Get the ticket types area
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function area()
    {
        return $this->belongsTo(Area::class)->withoutGlobalScope(TenantScope::class);
    }

    /**
     * Get additional ticket type areas
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function additionalAreas()
    {
        return $this->belongsToMany(Area::class, 'ticket_type_additional_areas', 'ticket_type_id', 'area_id');
    }

    /**
     * Get the ticket types rules
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rule()
    {
        return $this->belongsTo(Rule::class);
    }

    /**
     * @return mixed
     */
    public function getRuleWithFallbackAttribute($value)
    {
        return $this->rule ?: ($this->area->rule ?: $this->area->manager->rule);
    }

    /**
     * Get the ticket types category (hour, day, week, ...)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(TicketCategory::class, 'ticket_category_id');
    }

    /**
     * Get the ticket types prices
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->hasMany(TicketPrice::class);
    }

    /**
     * Get the ticket types tickets
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    /**
     * Get the ticket types checkin unit
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function checkinUnit()
    {
        return $this->belongsTo(CheckinUnit::class, 'checkin_id');
    }

    /**
     * Get the ticket types quota unit
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function quotaUnit()
    {
        return $this->belongsTo(QuotaUnit::class, 'quota_id');
    }

    /**
     * Get the ticket types required license types
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function requiredLicenseTypes()
    {
        return $this->belongsToMany(LicenseType::class, 'required_license_types');
    }

    /**
     * Get the ticket types optional license types
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function optionalLicenseTypes()
    {
        return $this->belongsToMany(LicenseType::class, 'optional_license_types');
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function productBasedNotifications()
    {
        return $this->belongsToMany(User::class, 'ticket_type_notifications');
    }

    /**
     * Get promotions related to
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function promotions()
    {
        return $this->morphToMany(Promotion::class, 'related', 'promotion_targets')->withTimestamps();
    }

    /**
     * Get the commissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function commissions()
    {
        return $this->morphMany(Commission::class, 'related');
    }

    /**
     * @param bool $isOnline
     * @return mixed
     */
    public function commissionsWithFallback($isOnline = true)
    {
        return $this->commissions()
            ->where('is_online', $isOnline)
            ->where(function ($query) {
                $query->whereDate('valid_till', '>=', Carbon::now())
                    ->orWhereNull('valid_till');
            })
            ->orderBy('created_at')
            ->first()
            ?: $this->area->commissionsWithFallback($isOnline);
    }

    /**
     * Get only ticket types which have active prices
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasActivePrices($query)
    {
        $now = Carbon::now()->toDateString();

        $query->whereHas('prices', function ($query) use ($now) {
            $query->whereRaw('? BETWEEN visible_from AND valid_to', [$now]);
        });

        return $query;
    }

    /**
     * Order ticket types by prices
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSortByPrice($query, $dir = 'asc')
    {
        return $query
            ->select('ticket_types.*')
            ->join('ticket_prices', 'ticket_prices.ticket_type_id', '=', 'ticket_types.id')
            ->groupBy('ticket_types.id')
            ->orderBy(\DB::raw((($dir == 'asc') ? 'MIN' : 'MAX') . '(ticket_prices.value)'), $dir);
    }

    /**
     * Select described name to be displayed in Form Builder
     * 
     * @return $this
     */
    public function scopeSelectDescribedName()
    {
        return $this
            ->listsTranslations('name')
            ->join('areas', 'areas.id', '=', 'ticket_types.area_id')
            ->join('ticket_prices', 'ticket_prices.ticket_type_id', '=', 'ticket_types.id')
            ->leftJoin(
                'ticket_price_translations',
                'ticket_price_translations.ticket_price_id',
                '=',
                'ticket_prices.id'
            )
            ->where('ticket_price_translations.locale', $this->locale())
            ->selectRaw(
                "CONCAT(
                    '#',
                    ticket_types.id,
                    ' ',
                    ticket_type_translations.name,
                    ' - ',
                    ticket_price_translations.name,
                    ' für ',
                    areas.name
                ) as described_name"
            )
            ->addSelect('ticket_types.id')
            ->groupBy('ticket_types.id');
    }
}
