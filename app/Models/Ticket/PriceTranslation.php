<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class PriceTranslation extends BaseModel
{
    public $timestamps = false;

    protected $fillable = ['name'];
}
