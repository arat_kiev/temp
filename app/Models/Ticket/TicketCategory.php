<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;
use Dimsav\Translatable\Translatable;

class TicketCategory extends BaseModel
{
    use Translatable;

    protected $table = 'ticket_categories';

    protected $fillable = ['type', 'name'];

    protected $translatedAttributes = ['name'];

    protected $with = ['translations'];
}
