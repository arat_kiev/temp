<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class ManagerLastId extends BaseModel
{
    protected $table = 'manager_last_ids';

    protected $fillable = ['year', 'last_id'];

    public $timestamps = false;

    /**
     * Increment last id and return it
     *
     * @return integer
     */
    public function next()
    {
        return ++$this->last_id;
    }
}
