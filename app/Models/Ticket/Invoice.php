<?php

namespace App\Models\Ticket;

use App\Models\Contact\Manager;
use App\Models\Product\ProductSale;
use App\Models\User;
use Auth;
use App\Models\BaseModel;
use App\Models\Contact\Reseller;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends BaseModel
{
    use SoftDeletes;

    protected $table = 'invoices';

    protected $fillable = [
        'date_from',
        'date_till',
        'online',
        'is_deleted',
        'storno_id',
        'storno_reason',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'date_from', 'date_till'];

    protected $casts = [
        'is_deleted'    => 'boolean',
        'online' => 'boolean'
    ];

    const IDENT_FORMAT = 'INV-%d-%09d';

    public static function boot() {
        parent::boot();

        // create a event to happen on updating
        static::updating(function(Invoice $invoice) {
            $invoice->updated_by = Auth::user() ? Auth::user()->id : null;
        });

        // create a event to happen on deleting
        static::deleting(function(Invoice $invoice) {
            $invoice->deleted_by = Auth::user() ? Auth::user()->id : null;
            $invoice->is_deleted = true;
            $invoice->storno_reason = self::$stornoReason;

            $invoice->tickets()->update(['invoice_id' => null]);
            $invoice->hfTickets()->update(['hf_invoice_id' => null]);
            $invoice->productSales()->update(['invoice_id' => null]);
            $invoice->hfProductSales()->update(['hf_invoice_id' => null]);
            $invoice->save();
        });

        // create a event to happen on creating
        static::creating(function(Invoice $invoice) {
            $invoice->created_by = Auth::user() ? Auth::user()->id : null;
            $invoice->updated_by = Auth::user() ? Auth::user()->id : null;
            $invoice->invoice_id = static::getIdForYear(Carbon::now()->year);
        });
    }

    private static function getIdForYear($year)
    {
        $current = DB::table('invoices_last_ids')->where('year', $year)->first();

        if ($current === null) {
            $nextId = 1;
            DB::table('invoices_last_ids')
                ->insert(['year' => $year, 'last_id' => $nextId]);
        } else {
            $nextId = $current->last_id + 1;
            DB::table('invoices_last_ids')->where('year', $year)
                ->update(['last_id' => $nextId]);
        }

        return $nextId;
    }

    public static $stornoReason = '';

    /**
     * Get the invoice tickets
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    /**
     * Get the superadmin invoice tickets
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function hfTickets()
    {
        return $this->hasMany(Ticket::class, 'hf_invoice_id');
    }

    /**
     * Get the invoice product sales
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function productSales()
    {
        return $this->hasMany(ProductSale::class);
    }

    /**
     * Get the superadmin invoice product sales
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function hfProductSales()
    {
        return $this->hasMany(ProductSale::class, 'hf_invoice_id');
    }

    /**
     * Get the invoice reseller
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function reseller()
    {
        return $this->belongsTo(Reseller::class);
    }

    /**
     * Get the invoice manager
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function manager()
    {
        return $this->belongsTo(Manager::class);
    }

    /**
     * Get the invoice created_by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * Get the invoice created_by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    /**
     * Get the invoice created_by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function deletedBy()
    {
        return $this->belongsTo(User::class, 'deleted_by');
    }
}