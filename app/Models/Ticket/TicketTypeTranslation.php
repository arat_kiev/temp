<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;

class TicketTypeTranslation extends BaseModel
{
    protected $table = 'ticket_type_translations';

    public $timestamps = false;

    protected $fillable = ['name'];
}
