<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;
use App\Models\User;
use App\Models\Contact\Reseller;
use App\Models\Location\Country;

class ResellerTicket extends BaseModel
{
    protected $table = 'reseller_tickets';

    protected $primaryKey = 'ticket_id';

    protected $fillable = [
        'first_name',
        'last_name',
        'street',
        'post_code',
        'city',
        'country_id',
        'birthday',
        'phone',
        'email',
        'licenses',
        'authorization_id',
        'fisher_id',
        'issuing_authority',
    ];

    protected $with = ['baseTicket'];

    protected $dates = ['birthday', 'cleared_at'];

    public $timestamps = false;

    protected $casts = [
        'licenses' => 'array',
    ];

    /**
     * Get the reseller tickets shop
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reseller()
    {
        return $this->belongsTo(Reseller::class, 'reseller_id', 'id', 'reseller_tickets');
    }

    /**
     * Get the reseller tickets base ticket
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function baseTicket()
    {
        return $this->belongsTo(Ticket::class, 'ticket_id');
    }

    /**
     * Get the reseller tickets user country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    /**
     * Get the buyers full name
     *
     * @param $value
     * @return string
     */
    public function getFullNameAttribute($value)
    {
        return $this->first_name.' '.$this->last_name;
    }

    /**
     * Register model event handlers
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function (ResellerTicket $ticket) {
            $ticket->fisher_id = $ticket->fisher_id ?: User::uniqueFisherId();
        });
    }
}
