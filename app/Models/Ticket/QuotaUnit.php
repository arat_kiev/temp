<?php

namespace App\Models\Ticket;

use App\Models\BaseModel;
use Dimsav\Translatable\Translatable;

class QuotaUnit extends BaseModel
{
    use Translatable;

    protected $table = 'quota_units';

    protected $fillable = ['type', 'name'];

    protected $translatedAttributes = ['name'];

    protected $with = ['translations'];
}
