<?php

namespace App\Models\Product;

use App\Models\BaseModel;
use App\Models\User;

class StockNotificatable extends BaseModel
{
    protected $table = 'stock_notificatables';

    protected $dates = ['created_at', 'updated_at'];

    /**
     * Get the product stock data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productStock()
    {
        return $this->belongsTo(ProductStock::class);
    }

    /**
     * Get the user to be notified
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }
}
