<?php

namespace App\Models\Product;

use App\Models\Location\Country;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    use Translatable;

    protected $table = 'fees';

    protected $fillable = ['type', 'value', 'name'];

    protected $translatedAttributes = ['name'];

    protected $with = ['translations'];

    protected $dates = ['created_at', 'updated_at'];

    const AVAILABLE_TYPES = [
        'relative'  => 'Relativ (in %)',
        'absolute'  => 'Absolut (in €)',
    ];

    const DEFAULT_TYPE = 'relative';
    
    /**
     * Get the value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getValueOriginAttribute($value)
    {
        return $this->attributes['value'];
    }

    /**
     * Get the value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getValueAttribute($value)
    {
        return number_format($value / 100.0, 2, ',', '.');
    }

    /**
     * Get the value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getValueWithMarkAttribute($value)
    {
        $value = number_format($this->attributes['value'] / 100.0, 2, ',', '.');
        // Do not forgot to include `type` to select for proper result
        $type = $this->attributes['type'] ?? self::DEFAULT_TYPE;

        return $type === 'relative' ? $value.' %' : $value.' €';
    }

    public function getNameWithCountryAttribute()
    {
        // HINT: Do not forget include `country_id` to select
        return $this->attributes['name'] . ' (' . $this->country->name . ')';
    }

    /**
     * Set the value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function setValueAttribute($value)
    {
        $this->attributes['value'] = ((float) str_replace(',', '.', $value)) * 100;
    }

    /**
     * Get the fee country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Get the fee category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(FeeCategory::class);
    }

    /**
     * Get the fee product prices
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function prices()
    {
        return $this->belongsToMany(ProductPrice::class, 'product_fees');
    }
}
