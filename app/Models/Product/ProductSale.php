<?php

namespace App\Models\Product;

use App\Managers\ProductSaleManager;
use App\Models\BaseModel;
use App\Models\Client;
use App\Models\Contact\Reseller;
use App\Models\Location\Country;
use App\Models\Ticket\Invoice;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSale extends BaseModel
{
    use SoftDeletes;

    protected $table = 'product_sales';

    protected $fillable = [
        'status',
        'email',
        'first_name',
        'last_name',
        'street',
        'post_code',
        'city',
        'storno_id',
        'storno_reason',
        'offisy_id',
        'offisy_code',
        'offisy_tags',
        'commission_value',
        'quantity',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'offisy_tags'   => 'array',
        'is_deleted'    => 'boolean',
    ];

    const IDENT_FORMAT = 'PR-%d-%06d';

    const IDENT_REGEX = '/^PR-(\d{1,2})-(\d{1,6})$/';

    const IDENT_FORMAT_MANAGER = '%s-%d-%06d';

    const DEFAULT_STATUS = 'RECEIVED';

    const ALLOWED_STATUSES = ['RECEIVED', 'SHIPPING', 'SHIPPED'];

    public static $stornoReason = '';

    public static function boot() {
        parent::boot();

        // create a event to happen on deleting
        static::deleting(function($table)  {
            $table->is_deleted = true;
            $table->storno_reason = self::$stornoReason;
            $table->save();
        });
    }

    /**
     * Get the product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Get the user who bought a product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Get the reseller who bought a product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reseller()
    {
        return $this->belongsTo(Reseller::class);
    }

    /**
     * Get the client related to product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * Get the price related to product sale
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function price()
    {
        return $this->belongsTo(ProductPrice::class);
    }

    /**
     * Get the sales booked timeslots
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function timeslotDates()
    {
        return $this->belongsToMany(TimeslotDate::class, 'timeslot_sales', 'product_sale_id', 'timeslot_date_id');
    }

    /**
     * Get the users fee
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function fees()
    {
        $countryId = $this->country
            ? $this->country->id
            : ($this->user && $this->user->country ? $this->user->country->id : null);
        $fees = $this->price->fees();

        return $countryId
            ? $fees->whereHas('country', function ($query) use ($countryId) {
                    $query->whereId($countryId);
                })
            : $fees;
    }

    public function getTotalPriceOriginAttribute()
    {
        $amount = $this->price->value_origin + $this->commission_value_origin;

        return $this->reseller || !$this->user
            ? $amount
            : $amount + (new ProductSaleManager())->fetchFeeValues($this->price, $this->user);
    }

    public function getTotalPriceAttribute()
    {
        return number_format($this->total_price_origin / 100.0, 2, ',', '.');
    }

    /**
     * Get the commission value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getCommissionValueOriginAttribute($value)
    {
        return $this->attributes['commission_value'];
    }

    /**
     * Get the commission value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getCommissionValueAttribute($value)
    {
        return number_format($value / 100.0, 2, ',', '.');
    }

    /**
     * Get the country product was sold
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Get the sale invoice
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    /**
     * Get the sale superadmin invoice
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */
    public function hfInvoice()
    {
        return $this->belongsTo(Invoice::class, 'hf_invoice_id');
    }
}
