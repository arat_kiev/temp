<?php

namespace App\Models\Product;

use App\Models\BaseModel;
use App\Models\Stock;
use App\Models\User;

class ProductStock extends BaseModel
{
    protected $table = 'product_stocks';

    protected $fillable = ['quantity', 'unit', 'delivery_time', 'notification_quantity'];

    protected $dates = ['created_at', 'updated_at'];

    protected $casts = [
        'id'                    => 'integer',
        'quantity'              => 'integer',
        'delivery_time'         => 'integer',
        'notification_quantity' => 'integer',
    ];

    const AVAILABLE_UNITS = [
        'piece' => 'Stück',
    ];

    const DEFAULT_UNIT = 'piece';

    /**
     * Get the product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Get the stock
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stock()
    {
        return $this->belongsTo(Stock::class);
    }

    /**
     * Get the users to be notified
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function notificatableUsers()
    {
        return $this->belongsToMany(User::class, 'stock_notificatables');
    }

    /**
     * Get delivery time in days
     *
     * @return int
     */
    public function getDeliveryTimeInDaysAttribute()
    {
        return $this->delivery_time ? (int) ceil($this->delivery_time / 24) : null;
    }

    /**
     * Get human-readable unit attribute
     *
     * @return int
     */
    public function getHumanReadableUnitAttribute()
    {
        $units = self::AVAILABLE_UNITS;

        return $this->unit && isset($units[$this->unit])
            ? $units[$this->unit]
            : $units[self::DEFAULT_UNIT];
    }
}
