<?php

namespace App\Models\Product;

use App\Models\BaseModel;
use App\Models\Picture;
use Dimsav\Translatable\Translatable;
use App\Traits\Sluggable;

class ProductCategory extends BaseModel
{
    use Translatable,
        Sluggable;

    protected $table = 'product_categories';

    protected $translatedAttributes = ['name', 'short_description', 'long_description'];

    protected $with = ['translations'];

    protected $translationForeignKey = 'category_id';

    protected $fillable = [];

    protected $slugAttribute = 'name';

    protected $dates = ['created_at', 'updated_at'];

    /**
     * Get the product category picture
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function picture()
    {
        return $this->belongsTo(Picture::class);
    }

    /**
     * Get the products from specified product category
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }
}
