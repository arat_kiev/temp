<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class ProductTranslation extends BaseModel
{
    public $timestamps = false;

    protected $fillable = ['name', 'short_description', 'long_description'];
}
