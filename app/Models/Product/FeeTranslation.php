<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class FeeTranslation extends BaseModel
{
    public $timestamps = false;

    protected $fillable = ['name'];
}
