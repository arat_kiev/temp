<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class ProductCategoryTranslation extends BaseModel
{
    public $timestamps = false;

    protected $fillable = ['name', 'short_description', 'long_description'];
}
