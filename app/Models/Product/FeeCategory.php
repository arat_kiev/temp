<?php

namespace App\Models\Product;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class FeeCategory extends Model
{
    use Translatable;

    protected $table = 'fee_categories';

    protected $fillable = ['name'];

    protected $translatedAttributes = ['name'];

    protected $with = ['translations'];

    protected $translationForeignKey = 'category_id';

    protected $dates = ['created_at', 'updated_at'];

    /**
     * Get the fees
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fees()
    {
        return $this->hasMany(Fee::class, 'category_id');
    }
}
