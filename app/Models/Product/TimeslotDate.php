<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class TimeslotDate extends BaseModel
{
    protected $table = 'timeslot_dates';

    protected $fillable = ['date', 'pool'];

    protected $casts = [
        'date' => 'date',
    ];

    /**
     * Get the date entries parent timeslot.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function timeslot()
    {
        return $this->belongsTo(Timeslot::class);
    }

    /**
     * Get the sales for this timeslot date
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function productSales()
    {
        return $this->belongsToMany(ProductSale::class, 'timeslot_sales', 'timeslot_date_id', 'product_sale_id');
    }

    public function reservations()
    {
        return $this->hasMany(RentalReservation::class);
    }

    public function getAvailableAttribute()
    {
        return $this->pool - $this->productSales()->count() - $this->reservations()->count();
    }

    public function hasReservationsAvailable()
    {
        return $this->available > 0;
    }
}