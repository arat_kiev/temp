<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class TimeDiscount extends BaseModel
{
    protected $table = 'time_discounts';

    protected $fillable = ['count', 'discount', 'inclusive'];

    /**
     * Get the parent product price
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function price()
    {
        return $this->belongsTo(ProductPrice::class, 'product_price_id');
    }

    /**
     * Get the formatted discount
     *
     * @return string
     */
    public function formattedDiscountAttribute()
    {
        return number_format($this->discount / 100, 2, ',', '.');
    }
}