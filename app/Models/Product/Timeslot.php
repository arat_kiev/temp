<?php

namespace App\Models\Product;

use App\Models\BaseModel;
use Carbon\Carbon;

class Timeslot extends BaseModel
{
    protected $table = 'product_timeslots';

    protected $fillable = ['from', 'till', 'default_pool'];

    /**
     * Get the parent product price.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productPrice()
    {
        return $this->belongsTo(ProductPrice::class);
    }

    /**
     * Get all dates of this timeslot
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dates()
    {
        return $this->hasMany(TimeslotDate::class, 'timeslot_id');
    }

    /**
     * Accessor for attribute from. Transforms string to carbon/datetime
     *
     * @param $value
     * @return Carbon
     */
    public function getFromAttribute($value)
    {
        return new Carbon($value);
    }

    /**
     * Mutator for attribute from. Transforms carbon/datetime/string to string
     *
     * @param $value
     */
    public function setFromAttribute($value)
    {
        $this->attributes['from'] = with(new Carbon($value))->format('H:i');
    }

    /**
     * Accessor for attribute till. Transforms string to carbon/datetime
     *
     * @param $value
     * @return Carbon
     */
    public function getTillAttribute($value)
    {
        return new Carbon($value);
    }

    /**
     * Mutator for attribute till. Transforms carbon/datetime/string to string
     *
     * @param $value
     */
    public function setTillAttribute($value)
    {
        $this->attributes['till'] = with(new Carbon($value))->format('H:i');
    }
}