<?php

namespace App\Models\Product;

use App\Managers\ProductSaleManager;
use App\Models\BaseModel;
use App\Models\Commission;
use Auth;
use Carbon\Carbon;
use Gloudemans\Shoppingcart\Contracts\Buyable;

class ProductPrice extends BaseModel implements Buyable
{
    protected $table = 'product_prices';

    protected $fillable = ['visible_from', 'valid_from', 'valid_till', 'value', 'vat'];

    protected $dates = ['visible_from', 'valid_from', 'valid_till', 'created_at', 'updated_at'];

    protected $with = ['timeslots.dates'];

    /**
     * Get the value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getValueOriginAttribute($value)
    {
        return $this->attributes['value'];
    }

    /**
     * Get the value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getValueAttribute($value)
    {
        return number_format($value / 100.0, 2, ',', '.');
    }

    /**
     * Set the value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function setValueAttribute($value)
    {
        $this->attributes['value'] = ((float) str_replace(',', '.', $value)) * 100;
    }

    /**
     * Get the product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Get the product sales
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sales()
    {
        return $this->hasMany(ProductSale::class, 'price_id');
    }

    /**
     * Get the product prices fees
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function fees()
    {
        return $this->belongsToMany(Fee::class, 'product_fees');
    }

    /**
     * Get the commissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function commissions()
    {
        return $this->morphMany(Commission::class, 'related');
    }

    /**
     * Get the time discounts
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function timeDiscounts()
    {
        return $this->hasMany(TimeDiscount::class);
    }

    /**
     * Get the timeslots
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function timeslots()
    {
        return $this->hasMany(Timeslot::class)->orderBy('from');
    }

    /**
     * Get the timeslot dates
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function timeslotDates()
    {
        return $this->hasManyThrough(TimeslotDate::class, Timeslot::class)->orderBy('date');
    }

    /**
     * @param bool $isOnline
     * @return mixed
     */
    public function commissionsWithFallback($isOnline = true)
    {
        return $this->commissions()
            ->where('is_online', $isOnline)
            ->where(function ($query) {
                $query->whereDate('valid_till', '>=', Carbon::now())
                    ->orWhereNull('valid_till');
            })
            ->orderBy('created_at')
            ->first()
            ?: $this->product->commissionsWithFallback($isOnline);
    }

    /**
     * Get the identifier of the Buyable item.
     *
     * @return int|string
     */
    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    /**
     * Get the description or title of the Buyable item.
     *
     * @return string
     */
    public function getBuyableDescription($options = null)
    {
        return $this->product->name;
    }

    /**
     * Get the price of the Buyable item.
     *
     * @return float
     */
    public function getBuyablePrice($options = null)
    {
        $price = $this->value_origin;

        if (!empty($options['timeslot_dates'])) {
            $dm = app('App\Managers\DiscountManager');
            $price = $dm->discountedPrice($this, count($options['timeslot_dates']));
        }

        $priceWithFees = $price + (new ProductSaleManager())->fetchFeeValues($this, Auth::user());

        return $priceWithFees / 100.0;
    }

    /**
     * Get the type of the Buyable item.
     *
     * @return string
     */
    public function getBuyableType($options = null)
    {
        return 'product';
    }
}
