<?php

namespace App\Models\Product;

use App\Models\Area\Area;
use App\Models\BaseModel;
use App\Models\Commission;
use App\Models\Contact\Manager;
use App\Models\Contact\Rental;
use App\Models\Contact\Reseller;
use App\Models\Gallery;
use App\Models\Picture;
use App\Models\Stock;
use App\Models\Promo\Promotion;
use App\Models\Promo\PromotionTarget;
use App\Traits\Draftable;
use App\Traits\Sluggable;
use Carbon\Carbon;
use ZipManager;
use Dimsav\Translatable\Translatable;

class Product extends BaseModel
{
    use Translatable,
        Sluggable,
        Draftable;

    protected $table = 'products';

    protected $translatedAttributes = ['name', 'short_description', 'long_description'];

    protected $with = ['translations', 'gallery'];

    protected $fillable = [
        'template', 'featured_from', 'featured_till',
        'account_number', 'attachment', 'max_quantity',
        'addon_only', 'hidden_info',
    ];

    protected $dates = ['featured_from', 'featured_till', 'created_at', 'updated_at'];

    protected $slugAttribute = 'name';

    protected $casts = [
        'id'            => 'integer',
        'category_id'   => 'integer',
        'gallery_id'    => 'integer',
        'drafted_by'    => 'integer',
        'template'      => 'array',
    ];

    /**
     * Get the product category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }

    /**
     * Get the product gallery
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    /**
     * Get the product prices
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->hasMany(ProductPrice::class);
    }

    /**
     * Get the product sales
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sales()
    {
        return $this->hasMany(ProductSale::class);
    }

    /**
     * Get all of the areas that are assigned this product
     */
    public function areas()
    {
        return $this->morphedByMany(Area::class, 'related', 'has_products');
    }

    /**
     * Get all of the managers that are assigned this product
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function managers()
    {
        return $this->morphedByMany(Manager::class, 'related', 'has_products');
    }

    /**
     * Get all of the rentals that are assigned this product
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function rentals()
    {
        return $this->morphedByMany(Rental::class, 'related', 'has_products');
    }

    /**
     * Get promotions related to
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function promotions()
    {
        return $this->morphToMany(Promotion::class, 'related', 'promotion_targets')->withTimestamps();
    }

    /**
     * Get the product stocks data
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productStocks()
    {
        return $this->hasMany(ProductStock::class);
    }

    /**
     * Get all of the resellers that are assigned this product
     */
    public function resellers()
    {
        return $this->morphedByMany(Reseller::class, 'related', 'has_products');
    }

    /**
     * Get the commissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function commissions()
    {
        return $this->morphMany(Commission::class, 'related');
    }

    /**
     * @param bool $isOnline
     * @return mixed
     */
    public function commissionsWithFallback($isOnline = true)
    {
        return $this->commissions()
            ->where('is_online', $isOnline)
            ->where(function ($query) {
                $query->whereDate('valid_till', '>=', Carbon::now())
                    ->orWhereNull('valid_till');
            })
            ->orderBy('created_at')
            ->first()
            ?: null;
    }

    /**
     * Move uploaded file and save path as string
     *
     * @param array $files
     * @throws \Exception
     */
    public function setAttachmentAttribute(array $files = null)
    {
        $fileName = null;

        if ($files) {
            $fileName = ZipManager::storageName();
            $filesAdded = ZipManager::zipFiles($files, $fileName);

            if (!$filesAdded) {
                $this->attributes['attachment'] = null;
                $this->save();
                throw new \Exception("Files wasn't added to zip attachment");
            }
        }

        $this->attributes['attachment'] = $fileName;
    }

    /**
     * Generate file path
     *
     * @param $file
     * @return string
     */
    public function getAttachmentAttribute($file)
    {
        return $file
            ? implode(DIRECTORY_SEPARATOR, [ZipManager::storagePath($file), $file])
            : $file;
    }

    /**
     * Get only attachment name
     *
     * @return mixed
     */
    public function getAttachmentNameAttribute()
    {
        return !empty($this->attachment) ? $this->attachment : $this->attributes['attachment'];
    }

    /**
     * Get files absolute path on filesystem
     *
     * @return string
     */
    public function getAttachmentFullPathAttribute()
    {
        return $this->attachment
            ? ZipManager::getFileFullPath($this->attachment)
            : '';
    }

    /**
     * Get the price attribute
     *
     * @return ProductPrice
     */
    public function getPriceAttribute()
    {
        return $this->prices()
            ->where('visible_from', '<=', Carbon::now())
            ->where('valid_till', '>=', Carbon::now())
            ->first();
    }

    /**
     * Get attribute to know if product is in stock
     *
     * @return bool
     */
    public function getInStockAttribute()
    {
        return $this->productStocks
            ? (bool) $this->productStocks
                ->filter(function ($stock) {
                    return $stock->quantity > 0;
                })
            : true;
    }

    /**
     * Get only ticket types which have active prices
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasActivePrice($query)
    {
        $now = Carbon::now()->toDateString();

        $query->whereHas('prices', function ($query) use ($now) {
            $query->whereRaw('? BETWEEN visible_from AND valid_till', [$now]);
        });

        return $query;
    }

    /**
     * Get only products with specified categories
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array                                 $categories
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasCategories($query, array $categories)
    {
        return $query
            ->whereHas('category', function ($subQuery) use ($categories) {
                $subQuery->whereIn('id', $categories);
            });
    }

    /**
     * Get only products with specified prices
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array                                 $prices
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasPriceValues($query, array $prices)
    {
        return $query
            ->whereHas('prices', function ($subQuery) use ($prices) {
                $subQuery->whereIn('value', $prices);
            })
            ->groupBy('products.id');
    }
}
