<?php

namespace App\Models\Product;

use App\Models\BaseModel;

class FeeCategoryTranslation extends BaseModel
{
    public $timestamps = false;

    protected $fillable = ['name'];
}
