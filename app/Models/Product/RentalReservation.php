<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class RentalReservation extends Model
{
    protected $fillable = ['notice'];

    public function timeslotDate()
    {
        return $this->belongsTo(TimeslotDate::class);
    }
}
