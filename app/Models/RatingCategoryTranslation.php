<?php

namespace App\Models;

class RatingCategoryTranslation extends BaseModel
{
    protected $table = 'rating_category_translations';

    public $timestamps = false;

    protected $fillable = ['name', 'description'];

    public function ratingCategory()
    {
        return $this->belongsTo(RatingCategory::class);
    }
}
