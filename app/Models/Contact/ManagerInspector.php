<?php

namespace App\Models\Contact;

use App\Models\BaseModel;
use App\Models\User;

class ManagerInspector  extends BaseModel
{
    protected $table = 'manager_inspectors';

    public function manager()
    {
        return $this->belongsTo(Manager::class);
    }

    public function inspector()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }
}
