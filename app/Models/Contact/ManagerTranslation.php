<?php

namespace App\Models\Contact;

use App\Models\BaseModel;

class ManagerTranslation extends BaseModel
{
    public $timestamps = false;

    protected $fillable = ['ticket_info'];

    /**
     * XSS Protection
     *
     * @param $value
     * @return string
     */
    public function getTicketInfoAttribute($value)
    {
        return clean($value);
    }
}
