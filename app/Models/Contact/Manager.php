<?php

namespace App\Models\Contact;

use App\Models\Area\Area;
use App\Models\Affiliate;
use App\Models\BaseModel;
use App\Models\Commission;
use App\Models\Haul;
use App\Models\Location\Country;
use App\Models\Meta\LegalDocument;
use App\Models\Organization\Organization;
use App\Models\Picture;
use App\Models\Area\Rule;
use App\Models\StornoNotification;
use App\Models\Product\Product;
use App\Models\Ticket\Invoice;
use App\Models\Ticket\ManagerLastId;
use App\Models\User;
use App\Traits\Sluggable;
use App\Traits\Draftable;
use Carbon\Carbon;
use DB;
use Dimsav\Translatable\Translatable;

class Manager extends BaseModel
{
    use Sluggable,
        Translatable,
        Draftable;

    protected $table = 'managers';

    protected $fillable = ['name', 'street', 'area_code', 'city', 'person', 'phone', 'email',
        'website', 'uid', 'ticket_info', 'account_number', 'short_code', 'templates', 'commission_included', 'config'];

    protected $casts = [
        'templates' => 'array',
        'config' => 'array',
    ];

    protected $translatedAttributes = ['ticket_info'];

    protected $with = ['translations', 'logo', 'rule', 'admins', 'signature'];

    protected $slugAttribute = 'name';

    private static $defaultPdfTemplate = [
        "pdf" => [
            "ticket" => "bissanzeiger.pdf.ticket"
        ]
    ];

    /**
     * Get the managers country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Get the managers areas
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function areas()
    {
        return $this->hasMany(Area::class);
    }

    /**
     * Get additional areas
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function additionalAreas()
    {
        return $this->belongsToMany(Area::class, 'area_additional_managers')->withPivot(['public', 'percentage']);
    }

    /**
     * Get the affiliates
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function affiliates()
    {
        return $this->morphToMany(Affiliate::class, 'related', 'affiliables');
    }

    /**
     * Get the managers organizations (member prices)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function organizations()
    {
        return $this->hasMany(Organization::class);
    }

    /**
     * Get the managers logo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function logo()
    {
        return $this->belongsTo(Picture::class);
    }

    /**
     * Get the managers signature image
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function signature()
    {
        return $this->belongsTo(Picture::class);
    }

    /**
     * Get the managers hauls
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function hauls()
    {
        return $this->hasManyThrough(Haul::class, Area::class);
    }

    /**
     * Get the managers rules
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rule()
    {
        return $this->belongsTo(Rule::class);
    }

    /**
     * Get the managers parent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Manager::class);
    }

    /**
     * Get the managers child manager
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Manager::class, 'parent_id');
    }

    /**
     * Get the managers child areas
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function childrenAreas()
    {
        return $this->hasManyThrough(Area::class, Manager::class, 'parent_id', 'manager_id');
    }

    /**
     * Get the managers admin users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function admins()
    {
        return $this->belongsToMany(User::class, 'manager_admins');
    }

    public function inspectors()
    {
        return $this->belongsToMany(User::class, 'manager_inspectors')->withTimestamps();
    }

    /**
     * Get the managers active resellers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function resellers()
    {
        return $this->belongsToMany(Reseller::class, 'manager_resellers')
            ->withPivot('account_number');
    }

    public function legalDocuments()
    {
        return $this->morphToMany(LegalDocument::class, 'confirmable', 'legal_document_confirmations')
            ->withPivot('signed_by', 'signed_at')
            ->withTimestamps();
    }

    /**
     * Get the manager invoices
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function stornoNotifications()
    {
        return $this->hasMany(StornoNotification::class);
    }

    /**
     * Get all of the products for the manager
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function products()
    {
        return $this->morphToMany(Product::class, 'related', 'has_products');
    }

    /**
     * Get the commissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function commissions()
    {
        return $this->morphMany(Commission::class, 'related');
    }

    /**
     * @param bool $isOnline
     * @return mixed
     */
    public function commissionsWithFallback($isOnline = true)
    {
        return $this->commissions()
            ->where('is_online', $isOnline)
            ->where(function ($query) {
                $query->whereDate('valid_till', '>=', Carbon::now())
                    ->orWhereNull('valid_till');
            })
            ->orderBy('created_at')
            ->first()
            ?: null;
    }

    /**
     * Get the managers last ticket id per year
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lastId()
    {
        return $this->hasMany(ManagerLastId::class);
    }

    /**
     * Get the next ticket id for this manager
     *
     * @param $year integer
     * @return integer
     */
    public function getNextTicketId($year)
    {
        return DB::transaction(function () use ($year) {
            $last_id = $this->lastId()->firstOrNew(['year' => $year]);
            $next_id = $last_id->next();
            $last_id->save();

            return $next_id;
        });
    }

    /**
     * Get managers as additional ones to the area
     * @param $query
     * @param $area_id
     * @return mixed
     */
    public function scopeAdditionalManagersToArea($query, $area_id)
    {
        return $query->join('area_additional_managers AS am', function($join) use($area_id)
        {
            $join->on('am.manager_id', '=', 'managers.id');
            $join->on('am.area_id','=', DB::raw($area_id));
        });
    }

    /**
     * Register model event handlers
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function (Manager $manager) {
            $manager->templates = $manager->templates ?: self::$defaultPdfTemplate;
        });
    }
}
