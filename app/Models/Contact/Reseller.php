<?php

namespace App\Models\Contact;

use App\Models\Area\Area;
use App\Models\BaseModel;
use App\Models\Location\Country;
use App\Models\Product\Product;
use App\Models\Product\ProductSale;
use App\Models\Ticket\Invoice;
use App\Models\Ticket\ResellerTicket;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketPrice;
use App\Models\User;

class Reseller extends BaseModel
{
    const ONLINE_TICKETS_PARAM = 0;
    const OFFLINE_TICKETS_PARAM = 999999;

    protected $table = 'resellers';

    protected $fillable = ['name', 'street', 'area_code', 'city', 'person', 'phone', 'email', 'website', 'uid'];

    /**
     * Get the resellers country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Get resellers areas
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function areas()
    {
        return $this->belongsToMany(Area::class, 'reseller_areas')->withPivot(['commission_type', 'commission_value']);
    }

    /**
     * Get the resellers tickets
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany(ResellerTicket::class);
    }

    /**
     * Get all of the products for the reseller
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function products()
    {
        return $this->morphToMany(Product::class, 'related', 'has_products');
    }

    /**
     * Get the resellers product sales
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productSales()
    {
        return $this->hasMany(ProductSale::class);
    }


    /**
     * Get the resellers active tickets
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activeTickets()
    {
        return $this->hasManyThrough(Ticket::class, ResellerTicket::class, 'reseller_id', 'id')
            ? $this->hasManyThrough(Ticket::class, ResellerTicket::class, 'reseller_id', 'id')
                ->whereNull('tickets.storno_id')
                ->where('tickets.is_deleted', 0)
            : null;
    }

    /**
     * Get the resellers invoices
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    /**
     * Get the resellers admins
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function admins()
    {
        return $this->belongsToMany(User::class, 'reseller_admins');
    }

    /**
     * Get the managers for which the reseller can sell tickets
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function managers()
    {
        return $this->belongsToMany(Manager::class, 'manager_resellers')
            ->withPivot('account_number');
    }

    /**
     * Get the managers for which the reseller can sell tickets
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ticketPrices()
    {
        return $this->belongsToMany(TicketPrice::class, 'reseller_ticket_prices')
            ->withPivot(['commission_type', 'commission_value']);
    }
}
