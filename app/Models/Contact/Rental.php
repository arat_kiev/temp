<?php

namespace App\Models\Contact;

use App\Models\Area\Area;
use App\Models\BaseModel;
use App\Models\Location\Country;
use App\Models\Product\Product;
use App\Models\Ticket\Invoice;
use App\Models\Ticket\ResellerTicket;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketPrice;
use App\Models\User;

class Rental extends BaseModel
{
    protected $table = 'rentals';

    protected $fillable = ['name', 'street', 'area_code', 'city', 'person', 'phone', 'email', 'website'];

    /**
     * Get the rentals country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Get all of the products for the rental
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function products()
    {
        return $this->morphToMany(Product::class, 'related', 'has_products');
    }

    /**
     * Get the rentals admin users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function admins()
    {
        return $this->belongsToMany(User::class, 'rental_admins');
    }
}
