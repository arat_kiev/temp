<?php

namespace App\Models\Authorization;

use Dimsav\Translatable\Translatable;
use Spatie\Permission\Models\Permission as BasePermission;

class Permission extends BasePermission
{
    use Translatable;

    protected $table = 'permissions';

    protected $fillable = ['name', 'description'];

    protected $translatedAttributes = ['description'];

    protected $with = ['translations'];
}
