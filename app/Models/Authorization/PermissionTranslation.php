<?php

namespace App\Models\Authorization;

use App\Models\BaseModel;

class PermissionTranslation extends BaseModel
{
    protected $table = 'permission_translations';

    public $timestamps = false;

    protected $fillable = ['description'];
}
