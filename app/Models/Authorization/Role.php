<?php

namespace App\Models\Authorization;

use Dimsav\Translatable\Translatable;
use Spatie\Permission\Models\Role as BaseRole;

class Role extends BaseRole
{
    use Translatable;

    protected $table = 'roles';

    protected $fillable = ['name', 'description'];

    protected $translatedAttributes = ['description'];

    protected $with = ['translations'];
}
