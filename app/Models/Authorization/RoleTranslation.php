<?php

namespace App\Models\Authorization;

use App\Models\BaseModel;

class RoleTranslation extends BaseModel
{
    protected $table = 'role_translations';

    public $timestamps = false;

    protected $fillable = ['description'];
}
