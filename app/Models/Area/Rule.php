<?php

namespace App\Models\Area;

use App\Models\BaseModel;
use App\Models\Picture;

class Rule extends BaseModel
{
    protected $table = 'rules';

    protected $fillable = ['text'];

    /**
     * Get the rules files
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function files()
    {
        return $this->belongsToMany(Picture::class, 'rule_files')->withPivot('public');
    }

    /**
     * Check if rule is empty
     *
     * @return bool
     */
    public function isEmpty()
    {
        return empty(strip_tags($this->text)) && $this->files()->count() == 0;
    }
}
