<?php

namespace App\Models\Area;

use App\Models\BaseModel;

class AreaTypeTranslation extends BaseModel
{
    protected $table = 'area_type_translations';

    public $timestamps = false;

    protected $fillable = ['name'];
}
