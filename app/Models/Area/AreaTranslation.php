<?php

namespace App\Models\Area;

use App\Models\BaseModel;

class AreaTranslation extends BaseModel
{
    public $timestamps = false;

    protected $fillable = ['description', 'borders', 'ticket_info'];

    /**
     * XSS Protection
     *
     * @param $value
     * @return string
     */
    public function getDescriptionAttribute($value)
    {
        return clean($value);
    }

    /**
     * XSS Protection
     *
     * @param $value
     * @return string
     */
    public function getBordersAttribute($value)
    {
        return clean($value);
    }

    /**
     * XSS Protection
     *
     * @param $value
     * @return string
     */
    public function getTicketInfoAttribute($value)
    {
        return clean($value);
    }
}
