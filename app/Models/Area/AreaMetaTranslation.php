<?php

namespace App\Models\Area;

use App\Models\BaseModel;

class AreaMetaTranslation extends BaseModel
{
    protected $table = 'meta_type_translations';

    public $timestamps = false;
}
