<?php

namespace App\Models\Area;

use App\Models\BaseModel;
use App\Models\User;
use App\Models\RatingCategory;

class AreaRating extends BaseModel
{

    protected $table = 'area_ratings';

    protected $fillable = ['area_id', 'user_id', 'rating_category_id', 'value'];

    /**
     * Get related areas
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id', 'id');
    }

    /**
     * Get related areas
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Get related areas
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(RatingCategory::class, 'rating_category_id', 'id');
    }
}
