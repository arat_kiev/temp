<?php

namespace App\Models\Area;

use App\Models\BaseModel;

class AreaTag extends BaseModel
{
    const FEATHER = 1;
    const LIGHT   = 2;
    const MIDDLE  = 3;
    const HEAVY   = 4;
    const SUPER   = 5;

    protected $table = 'area_tags';

    public $timestamps = false;

    protected $fillable = ['tag', 'weight', 'custom'];

    /**
     * @var array
     */
    protected $mappingProperties = [
        'tag' => [
            'type' => 'string',
            'analyzer' => 'standard'
        ]
    ];

    /**
     * Set index name for Elasticsearch
     *
     * @return string
     */
    function getIndexName()
    {
        return 'area_tags';
    }

    /**
     * Get Area from AreaTag
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function area()
    {
        return $this->belongsTo(Area::class);
    }
}
