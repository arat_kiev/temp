<?php

namespace App\Models\Area;

use App\Models\BaseModel;
use Dimsav\Translatable\Translatable;

class AreaMeta extends BaseModel
{
    use Translatable;

    protected $table = 'meta_types';

    protected $translatedAttributes = ['name'];

    public $translationForeignKey = 'meta_type_id';

    /**
     * Get all areas with this area meta
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function areas()
    {
        return $this->belongsToMany(Area::class, 'area_meta', 'meta_type_id', 'area_id')->withPivot('value');
    }
}
