<?php

namespace App\Models\Area;

use App\Exceptions\AreaHasNoLocations;
use App\Exceptions\AreaHasNoLocationsException;
use App\Models\Affiliate;
use App\Models\BaseModel;
use App\Models\Client;
use App\Models\Comment;
use App\Models\Commission;
use App\Models\Gallery;
use App\Models\Haul;
use App\Models\LandingPages\LandingPage;
use App\Models\Location\City;
use App\Models\Location\Geolocation;
use App\Models\Contact\Manager;
use App\Models\Location\State;
use App\Models\Meta\Fish;
use App\Models\Meta\Technique;
use App\Models\Picture;
use App\Models\Contact\Reseller;
use App\Models\Product\Product;
use App\Models\Ticket\Ticket;
use App\Models\Ticket\TicketType;
use App\Models\Ticket\TicketPrice;
use App\Models\User;
use App\Models\PointOfInterest\PointOfInterest;
use App\Traits\Sluggable;
use App\Traits\Tenantable;
use App\Traits\Draftable;
use Carbon\Carbon;
use DB;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;

class Area extends BaseModel
{
    use Sluggable,
        Tenantable,
        Translatable,
        Draftable;

    protected $table = 'areas';

    protected $fillable = [
        'approved',
        'public',
        'lease',
        'name',
        'description',
        'borders',
        'ticket_info',
        'account_number',
        'polyfield',
        'season_begin',
        'season_end',
    ];

    protected $translatedAttributes = ['description', 'borders', 'ticket_info'];

    protected $with = ['translations', 'rule', 'gallery.pictures', 'ticketTypes', 'resellers', 'locations'];

    protected $slugAttribute = 'name';

    protected $dates = [
        'created_at',
        'updated_at',
        'season_begin',
        'season_end',
    ];

    /**
     * Get the clients (tenants)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clients()
    {
        return $this->belongsToMany(Client::class, 'client_areas');
    }

    /**
     * Get the areas type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(AreaType::class);
    }

    /**
     * Get additional ticket types
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function additionalTicketTypes()
    {
        return $this->belongsToMany(TicketType::class, 'ticket_type_additional_areas', 'area_id', 'ticket_type_id');
    }

    /**
     * Get the target fishes
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function fishes()
    {
        return $this->belongsToMany(Fish::class, 'area_fishes')
            ->withPivot('closed_from', 'closed_till', 'min_length', 'max_length');
    }

    /**
     * Get the permitted techniques
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function techniques()
    {
        return $this->belongsToMany(Technique::class, 'area_techniques');
    }

    /**
     * Get the areas manager
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo(Manager::class);
    }

    /**
     * Get additional managers
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function additionalManagers()
    {
        return $this->belongsToMany(Manager::class, 'area_additional_managers')->withPivot(['public', 'percentage']);
    }

    /**
     * Get the areas rules
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rule()
    {
        return $this->belongsTo(Rule::class);
    }

    /**
     * Get the areas resellers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function resellers()
    {
        return $this->belongsToMany(Reseller::class, 'reseller_areas')->withPivot(['commission_type', 'commission_value']);
    }

    /**
     * Get the areas reseller count
     *
     * @return mixed
     */
    public function resellerCount()
    {
        $sub = Reseller::select('resellers.*')
            ->join('reseller_ticket_prices', 'resellers.id', '=', 'reseller_ticket_prices.reseller_id')
            ->join('ticket_prices', 'ticket_prices.id', '=', 'reseller_ticket_prices.ticket_price_id')
            ->join('ticket_types', 'ticket_types.id', '=', 'ticket_prices.ticket_type_id')
            ->where('ticket_types.area_id', '=', $this->id)
            ->groupBy('resellers.id');

        return DB::table(DB::raw("({$sub->toSql()}) as sub"))
            ->mergeBindings($sub->getQuery())
            ->count();
    }

    /**
     * Get the areas gallery
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    /**
     * Get the areas locations
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function locations()
    {
        return $this->morphMany(Geolocation::class, 'locatable');
    }

    /**
     * Get the comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * Get the affiliates
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function affiliates()
    {
        return $this->morphToMany(Affiliate::class, 'related', 'affiliables');
    }

    /**
     * Get the commissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function commissions()
    {
        return $this->morphMany(Commission::class, 'related');
    }

    /**
     * @param bool $isOnline
     * @return mixed
     */
    public function commissionsWithFallback($isOnline = true)
    {
        return $this->commissions()
            ->where('is_online', $isOnline)
            ->where(function ($query) {
                $query->whereDate('valid_till', '>=', Carbon::now())
                    ->orWhereNull('valid_till');
            })
            ->orderBy('created_at')
            ->first()
            ?: $this->manager->commissionsWithFallback($isOnline);
    }

    /**
     * Get all of the products for the area
     * 
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function products()
    {
        return $this->morphToMany(Product::class, 'related', 'has_products');
    }

    /**
     * Get the cities nearby areas
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cities()
    {
        return $this->belongsToMany(City::class, 'area_cities')->orderBy('area_cities.primary', 'desc');
    }

    /**
     * Get the states nearby areas
     *
     * @return \Illuminate\Support\Collection<State>|State[]
     */
    public function getStatesAttribute()
    {
        return $this->cities->map(function ($city) {
            return $city->region->state;
        })->unique('id');
    }

    /**
     * Get the areas ticket types
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ticketTypes()
    {
        return $this->hasMany(TicketType::class);
    }

    /**
     * Get the areas ticket prices available for now
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function ticketActivePrices()
    {
        $now = Carbon::now()->toDateString();

        return $this->hasManyThrough(TicketPrice::class, TicketType::class)
            ->whereRaw('? BETWEEN visible_from AND valid_to', [$now]);
    }

    public function additionalTicketActivePrices()
    {
        $now = Carbon::now()->toDateString();

        return TicketPrice::query()
            ->join('ticket_types', 'ticket_types.id', '=', 'ticket_prices.ticket_type_id')
            ->join('ticket_type_additional_areas', 'ticket_type_additional_areas.ticket_type_id', '=', 'ticket_types.id')
            ->where('ticket_type_additional_areas.area_id', '=', $this->id)
            ->whereRaw('? BETWEEN visible_from AND valid_to', [$now]);
    }

    /**
     * Get all sold tickets for this area
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function tickets()
    {
        return $this->hasManyThrough(Ticket::class, TicketType::class);
    }

    /**
     * Get the areas tags
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tags()
    {
        return $this->hasMany(AreaTag::class);
    }

    /**
     * Get the areas hauls
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hauls()
    {
        return $this->hasMany(Haul::class);
    }

    /**
     * Get the areas public hauls
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function publicHauls()
    {
        return $this->hasMany(Haul::class)->where('public', '=', 1);
    }

    /**
     * Get points of interest
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function poi()
    {
        return $this->belongsToMany(PointOfInterest::class, 'point_of_interest_areas', 'area_id', 'poi_id');
    }

    /**
     * Get the areas meta
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function meta()
    {
        return $this->belongsToMany(AreaMeta::class, 'area_meta', 'area_id', 'meta_type_id')->withPivot('value');
    }

    /**
     * Get the areas landing pages
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function partOf()
    {
        return $this->belongsToMany(LandingPage::class, 'landingpages_areas', 'area_id', 'lp_id');
    }

    /**
     * Get the areas parent
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parent()
    {
        return $this->hasOne(AreaParent::class, 'id', 'parent_area_id');
    }

    public function ratings()
    {
        return $this->hasMany(AreaRating::class);
    }

    /**
     * Get ticket inspectors per areas
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function inspectors()
    {
        return $this->belongsToMany(User::class, 'area_inspectors')->withPivot('is_public');
    }

    /**
     * Get the first gallery picture as area image
     *
     * @return Picture|null
     */
    public function getPictureAttribute($value)
    {
        return $this->gallery ? $this->gallery->pictures->sortByDesc('pivot_priority')->first() : null;
    }

    /**
     * Get the areas hero picture
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function heroImage()
    {
        return $this->belongsTo(Picture::class, 'hero_picture_id');
    }

    /**
     * @param $value
     */
    public function setSeasonBeginAttribute($value)
    {
        if (empty($value)) {
            $this->attributes['season_begin'] = null;
        } else {
            list ($day, $month) = explode('.', $value);
            $this->attributes['season_begin'] = Carbon::createFromDate(1, $month, $day);
        }
    }

    /**
     * @param $value
     */
    public function setSeasonEndAttribute($value)
    {
        if (empty($value)) {
            $this->attributes['season_end'] = null;
        } else {
            list ($day, $month) = explode('.', $value);
            $this->attributes['season_end'] = Carbon::createFromDate(1, $month, $day);   
        }
    }

    /**
     * XSS Protection
     *
     * @param $value
     * @return mixed
     */
    public function getNameAttribute($value)
    {
        return strip_tags($value);
    }

    /**
     * Gets all areas in in "radius" distance of the geolocation
     *
     * @param $distance float Distance in kilometer
     * @return bool|Builder
     * @throws AreaHasNoLocationsException
     */
    public function getNearbyAreasQuery($distance)
    {
        if (!$location = $this->locations()->first()) {
            throw new AreaHasNoLocationsException();
        }

        list ($south, $west, $north, $east) = $location->getBoundingBox($distance);

        return Area::with('picture')
            ->select(['areas.*', 'areas.id as id'])
            ->where('areas.id', '<>', $this->id)
            ->inBoundingBox($north, $east, $south, $west)
            ->inRandomOrder();
    }

    public function scopeUserHasLicenses($query, User $user)
    {
        $userLicenseIds = $user->licenses()->valid()->pluck('license_type_id');

        $query
            ->where(function ($query) use ($user) {
                $query->whereIn('areas.id', function ($query) use ($user) {
                    $query->select('areas.id')
                        ->distinct()
                        ->from('areas')
                        ->join('ticket_types', 'ticket_types.area_id', '=', 'areas.id')
                        ->join('required_license_types', 'required_license_types.ticket_type_id', '=', 'ticket_types.id')
                        ->join('license_types', 'license_types.id', '=', 'required_license_types.license_type_id')
                        ->leftJoin('licenses', function ($join) use ($user) {
                            $join->on('licenses.license_type_id', '=', 'license_types.id')
                                ->where('licenses.user_id', '=', $user->id);
                        })
                        ->groupBy('ticket_types.id')
                        ->havingRaw('SUM(CASE WHEN licenses.id IS NULL THEN 1 END) IS NULL');
                })
                    ->orWhereIn('areas.id', function ($query) {
                        $query->select('areas.id')
                            ->distinct()
                            ->from('areas')
                            ->join('ticket_types', 'ticket_types.area_id', '=', 'areas.id')
                            ->leftJoin('required_license_types', 'required_license_types.ticket_type_id', '=', 'ticket_types.id')
                            ->whereNull('required_license_types.license_type_id');
                    });
            })
            ->whereIn('areas.id', function ($query) use ($userLicenseIds) {
                $query->select('areas.id')
                    ->distinct()
                    ->from('areas')
                    ->join('ticket_types', 'ticket_types.area_id', '=', 'areas.id')
                    ->leftJoin('optional_license_types', 'optional_license_types.ticket_type_id', '=', 'ticket_types.id')
                    ->leftJoin('license_types', 'license_types.id', '=', 'optional_license_types.license_type_id')
                    ->whereIn('license_types.id', $userLicenseIds)
                    ->orWhereNull('license_types.id');
            });

        return $query;
    }

    /**
     * Get only areas which have active prices
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasActivePrices($query)
    {
        $now = Carbon::now()->toDateString();

        $query->whereHas('ticketTypes', function ($query) use ($now) {
            $query->whereHas('prices', function ($query) use ($now) {
                $query->whereRaw('? BETWEEN visible_from AND valid_to', [$now]);
            });
        });

        return $query;
    }

    /**
     * Get only areas having at least one match
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $tags
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasTags($query, $tags)
    {
        $tags = array_unique($tags);

        if (count($tags) > 0) {
            return $query
                ->selectRaw('COUNT(area_tags.tag) * SUM(area_tags.weight) AS relevance')
                ->join('area_tags', 'areas.id', '=', 'area_tags.area_id')
                ->whereIn('area_tags.tag', $tags)
                ->groupBy('areas.id')
                ->havingRaw('SUM(area_tags.weight) > ?', [2])
                ->orderBy('relevance', 'desc')
                ->orderBy('approved', 'desc');
        }

        return $query;
    }

    /**
     * Get only areas having these area types
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $types
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasTypes($query, array $types)
    {
        return $query->whereIn('areas.type_id', $types);
    }

    /**
     * Get only areas with rods number >= rods_max value
     *
     * @param $query
     * @param $rods
     * @return mixed
     */
    public function scopeHasRods($query, $rods)
    {
        return $query->where('areas.rods_max', '>=', $rods);
    }

    /**
     * Get only areas with set "member_only" flag
     *
     * @param $query
     * @param $member
     * @return mixed
     */
    public function scopeHasMember($query, $member)
    {
        return $query->where('areas.member_only', '=', $member);
    }

    /**
     * Get only areas with set "boat" flag
     *
     * @param $query
     * @param $boat
     * @return mixed
     */
    public function scopeHasBoat($query, $boat)
    {
        return $query->where('areas.boat', '=', $boat);
    }

    /**
     * Get only areas with set "phone_ticket" flag
     *
     * @param $query
     * @param $phone_ticket
     * @return mixed
     */
    public function scopeHasPhoneTicket($query, $phone_ticket)
    {
        return $query->where('areas.phone_ticket', '=', $phone_ticket);
    }

    /**
     * Get only areas with set "nightfishing" flag
     *
     * @param $query
     * @param $night
     * @return mixed
     */
    public function scopeHasNight($query, $night)
    {
        return $query->where('areas.nightfishing', '=', $night);
    }

    /**
     * Get only areas having these fishes
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $fishes
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasFishes($query, array $fishes)
    {
        return $query
            ->join('area_fishes', function (JoinClause $join) use ($fishes) {
                $join->on('areas.id', '=', 'area_fishes.area_id')
                    ->whereIn('area_fishes.fish_id', $fishes);
            })
            ->groupBy('areas.id');
    }

    /**
     * Get only areas having these techniques
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $techniques
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasTechniques($query, array $techniques)
    {
        return $query
            ->join('area_techniques', function (JoinClause $join) use ($techniques) {
                $join->on('areas.id', '=', 'area_techniques.area_id')
                    ->whereIn('area_techniques.technique_id', $techniques);
            })
            ->groupBy('areas.id');
    }

    /**
     * Get only areas in specified city
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $city
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasCity($query, $city)
    {
        return $query
            ->join('area_cities', 'areas.id', '=', 'area_cities.area_id')
            ->join('cities', function (JoinClause $join) use ($city) {
                $join->on('cities.id', '=', 'area_cities.city_id')
                    ->where('cities.id', '=', $city);
            })
            ->groupBy('areas.id');
    }

    /**
     * Get only areas in specified region
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $region
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasRegion($query, $region)
    {
        return $query
            ->join('area_cities', 'areas.id', '=', 'area_cities.area_id')
            ->join('cities', 'cities.id', '=', 'area_cities.city_id')
            ->join('regions', function (JoinClause $join) use ($region) {
                $join->on('regions.id', '=', 'cities.region_id')
                    ->where('regions.id', '=', $region);
            })
            ->groupBy('areas.id');
    }

    /**
     * Get only areas in specified state
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $state
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasState($query, $state)
    {
        return $query
            ->join('area_cities', 'areas.id', '=', 'area_cities.area_id')
            ->join('cities', 'cities.id', '=', 'area_cities.city_id')
            ->join('regions', 'regions.id', '=', 'cities.region_id')
            ->join('states', function (JoinClause $join) use ($state) {
                $join->on('states.id', '=', 'regions.state_id')
                    ->where('states.id', '=', $state);
            })
            ->groupBy('areas.id');
    }

    /**
     * Get only areas in specified country
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $country
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasCountry($query, $country)
    {
        return $query
            ->join('area_cities', 'areas.id', '=', 'area_cities.area_id')
            ->join('cities', 'cities.id', '=', 'area_cities.city_id')
            ->join('regions', 'regions.id', '=', 'cities.region_id')
            ->join('states', 'states.id', '=', 'regions.state_id')
            ->join('countries', function (JoinClause $join) use ($country) {
                $join->on('countries.id', '=', 'states.country_id')
                    ->where('countries.id', '=', $country);
            })
            ->groupBy('areas.id');
    }

    /**
     * Get only areas from specified manager
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $manager
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasManager($query, $manager)
    {
        /** @var Manager $manager */
        $manager = Manager::with('children')->findOrFail($manager);

        $manager_ids = $manager->children->count() ? $manager->children()->pluck('id') : [];
        $manager_ids[] = $manager->id;

        return $query->whereIn('manager_id', $manager_ids);
    }

    /**
     * Get only areas with specific area type
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $areaType
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasAreaType($query, $areaType)
    {
        return $query->where('type_id', '=', $areaType);
    }

    /**
     * Get only areas with status approved (selling tickets)
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasTickets($query, $value = true)
    {
        return $query->where('approved', '=', $value);
    }

    /**
     * Get only areas with specified ticket categories
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasTicketCategories($query, array $categories)
    {
        return $query
            ->join('ticket_types', function (JoinClause $join) use ($categories) {
                $join->on('areas.id', '=', 'ticket_types.area_id')
                    ->whereIn('ticket_types.ticket_category_id', $categories);
            })->groupBy('areas.id');
    }

    /**
     * Get only areas with status lease (for sale)
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsLease($query, $value = true)
    {
        return $query->where('lease', '=', $value);
    }

    /**
     * Get only areas which are public (not member only)
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsPublic($query, $value = true)
    {
        return $query->where('public', '=', $value);
    }

    /**
     * Get only areas within the bounding box
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInBoundingBox($query, $north, $east, $south, $west)
    {
        return $query->leftJoin('geolocations', function ($join) {
            $join->on('areas.id', '=', 'geolocations.locatable_id')
                ->where('locatable_type', '=', 'areas');
        })->whereNotNull('locatable_id')
            ->whereBetween('longitude', [$west, $east])
            ->whereBetween('latitude', [$south, $north])
            ->distinct();
    }

    /**
     * Get only areas within the radius sorted by distance
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInRadius($query, $lng, $lat, $radius)
    {
        return $query
            ->addSelect(DB::raw('MIN(`geoloc`.`distance`) AS distance'))
            ->leftJoin(DB::raw('(
            SELECT 
                ROUND(ST_Distance_Sphere(
                    Point(geolocations.longitude, geolocations.latitude),
                    Point(?, ?)
                ) / 1000, 2) AS distance,
                locatable_id AS area_id
            FROM
                geolocations
            WHERE
                locatable_type = \'areas\' AND
                locatable_id IS NOT NULL
            HAVING distance < ?
        ) geoloc'), function($join) {
            $join->on('areas.id', '=', 'geoloc.area_id');
        })->whereNotNull('geoloc.area_id')
            ->addBinding($lng, 'join')
            ->addBinding($lat, 'join')
            ->addBinding($radius, 'join')
            ->orderBy('distance')
            ->groupBy('areas.id');
    }

    /**
     * Get areas where user can buy member tickets
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsMember($query, User $user)
    {
        $orgIds = $user->memberships()->pluck('organization_id')->toArray();

        if (empty($orgIds)) {
            $orgIds[] = -1;
        }

        return $query->addSelect(['areas.*', 'areas.id as id'])
            ->join('ticket_types', 'areas.id', '=', 'ticket_types.area_id')
            ->join('ticket_prices', 'ticket_types.id', '=', 'ticket_prices.ticket_type_id')
            ->join('ticket_price_organizations', function (JoinClause $join) use ($orgIds) {
                $join->on('ticket_prices.id', '=', 'ticket_price_organizations.ticket_price_id')
                    ->whereIn('organization_id', $orgIds);
            })->groupBy('areas.id');
    }

    /**
     * Get landing pages to which area related
     * 
     * @param $query
     * @param $lpIds
     * @return mixed
     */
    public function scopePartOf($query, $lpIds)
    {
        return $query
            ->join('landingpages_areas', function (JoinClause $join) use ($lpIds) {
                $join->on('areas.id', '=', 'landingpages_areas.area_id')
                    ->whereIn('landingpages_areas.lp_id', $lpIds);
            })->groupBy('areas.id');
    }
}
