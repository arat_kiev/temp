<?php

namespace App\Models\Area;

use App\Models\BaseModel;
use App\Traits\Sluggable;
use Dimsav\Translatable\Translatable;

class AreaType extends BaseModel
{
    use Translatable,
        Sluggable;

    protected $table = 'area_types';

    protected $fillable = ['name'];

    protected $translatedAttributes = ['name'];

    protected $with = ['translations'];

    protected $slugAttribute = 'name';

    /**
     * Get all areas with this area type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function areas()
    {
        return $this->hasMany(Area::class);
    }

    /**
     * Get all parent areas with this area type
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parentAreas()
    {
        return $this->hasOne(AreaParent::class, 'type_id');
    }
}
