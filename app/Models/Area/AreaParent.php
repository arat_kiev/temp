<?php

namespace App\Models\Area;

use App\Models\BaseModel;
use App\Traits\Sluggable;
use Dimsav\Translatable\Translatable;

class AreaParent extends BaseModel
{
    use Translatable,
        Sluggable;

    protected $table = 'parent_areas';

    protected $fillable = ['name', 'description'];

    protected $translatedAttributes = ['name', 'description'];

    protected $with = ['translations'];

    public $translationForeignKey = 'parent_id';

    public $useTranslationFallback = true;

    public $timestamps = false;

    protected $slugAttribute = 'name';

    /**
     * Get related areas
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function areas()
    {
        return $this->hasMany(Area::class, 'parent_area_id', 'id');
    }

    /**
     * Get related types
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(AreaType::class);
    }
}
