<?php

namespace App\Models\Area;

use App\Models\BaseModel;

class AreaParentTranslation extends BaseModel
{
    protected $table = 'parent_area_translations';

    public $timestamps = false;

    protected $fillable = ['name', 'description'];
}
