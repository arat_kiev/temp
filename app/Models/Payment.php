<?php

namespace App\Models;

use Hash;

class Payment extends BaseModel
{
    protected $table = 'payments';

    protected $fillable = ['uuid', 'amount', 'success_url', 'failure_url'];

    /**
     * Get the payments user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Get a secure hash (app_key + uuid)
     *
     * @return string
     */
    public function getSecureHashAttribute()
    {
        return Hash::make(config('app.key').$this->uuid);
    }

    /**
     * Check a given hash for correctness
     *
     * @param $hash
     * @return bool
     */
    public function checkSecureHash($hash)
    {
        return Hash::check(config('app.key').$this->uuid, $hash);
    }
}
