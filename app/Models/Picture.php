<?php

namespace App\Models;

use Config;
use Symfony\Component\HttpFoundation\File\File;

class Picture extends BaseModel
{
    protected $table = 'pictures';

    protected $fillable = ['name', 'description', 'file'];

    protected $with = 'author';

    /**
     * Get galleries which include this image
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function galleries()
    {
        return $this->belongsToMany(Gallery::class, 'gallery_pictures');
    }

    /**
     * Get the pictures author (only user-supplied images)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Set the file path directly without a file
     *
     * @param $path
     */
    public function setFilePathAttribute($path)
    {
        $this->attributes['file'] = $path;
    }

    /**
     * Move uploaded file and save path as string
     *
     * @param File $file
     */
    public function setFileAttribute(File $file)
    {
        $fileName = $this->storageName($file);
        $filePath = implode(DIRECTORY_SEPARATOR, [
            Config::get('filesystems.disks.image_uploads.root'),
            $this->storagePath($fileName)
        ]);

        $file->move($filePath, $fileName);

        $this->attributes['file'] = $fileName;
    }

    /**
     * Generate file path
     *
     * @param $file
     * @return string
     */
    public function getFileAttribute($file)
    {
        return implode(DIRECTORY_SEPARATOR, [$this->storagePath($file), $file]);
    }

    /**
     * Get only filename
     *
     * @return mixed
     */
    public function getFileNameAttribute()
    {
        return !empty($this->name) ? $this->name : $this->attributes['file'];
    }

    public function getFilePathWithNameAttribute()
    {
        if (!empty($this->name)) {
            $tokens = explode('.', $this->attributes['file']);

            return $tokens[0] . '/' . $this->name . '.' . $tokens[1];
        }

        return $this->attributes['file'];
    }

    /**
     * Get files absolute path on filesystem
     *
     * @return string
     */
    public function getFileWithFullPathAttribute()
    {
        return implode(DIRECTORY_SEPARATOR, [
            Config::get('filesystems.disks.image_uploads.root'),
            $this->file,
        ]);
    }

    /**
     * Get random filename for uploaded image
     *
     * @param File $file
     * @return string
     */
    protected function storageName(File $file)
    {
        return sha1(uniqid(mt_rand(), true)) . '.' . $file->guessExtension();
    }

    /**
     * Get the absolute path for the specified filename
     *
     * @param string $fileName
     * @return string
     */
    protected function storagePath($fileName)
    {
        return implode(DIRECTORY_SEPARATOR, [
            substr($fileName, 0, 2),
            substr($fileName, 2, 2),
        ]);
    }

    /**
     * Get only public files
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublic($query)
    {
        return $query->where('public', '=', true);
    }
}
