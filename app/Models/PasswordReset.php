<?php

namespace App\Models;

class PasswordReset extends BaseModel
{
    protected $table = 'password_resets';

    protected $fillable = ['emails', 'token'];
    
    const MAX_ALLOWED_ATTEMPTS = 3;
}
