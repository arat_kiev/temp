<?php

namespace App\Models;

class CommentFlags extends BaseModel
{
    protected $table = 'comment_flags';

    protected $fillable = ['description'];

    protected $dates = ['created_at', 'updated_at'];

    public static function findReport($commentId, $userId)
    {
        return self::where('comment_id', '=', $commentId)->where('user_id', '=', $userId)->first();
    }

    /**
     * Get user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Get comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function comment()
    {
        return $this->belongsTo(Comment::class);
    }
}
