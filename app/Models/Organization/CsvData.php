<?php

namespace App\Models\Organization;

use Illuminate\Database\Eloquent\Model;

class CsvData extends Model
{
    protected $table = 'csv_data';

    protected $fillable = ['csv_filename', 'csv_header', 'csv_data', 'progress'];

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function scopeInProgress($query)
    {
        return $query->whereNotNull('progress')
            ->whereNull('progress')
            ->orWhere('progress', '<', 100);
    }
}
