<?php

namespace App\Models\Organization;

use App\Models\BaseModel;
use App\Models\Contact\Manager;
use App\Models\Location\Country;
use App\Models\Picture;
use App\Models\Ticket\TicketPrice;
use App\Models\User;
use App\Traits\Sluggable;

class Organization extends BaseModel
{
    use Sluggable;

    protected $table = 'organizations';

    protected $fillable = ['name', 'street', 'area_code', 'city', 'person', 'phone', 'email', 'website'];

    protected $with = ['logo'];

    protected $slugAttribute = 'name';

    /**
     * Get the organizations country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Get the organizations manager (if applicable)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo(Manager::class);
    }

    /**
     * Get the organizations logo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function logo()
    {
        return $this->belongsTo(Picture::class);
    }

    /**
     * Get the organizations members
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function members()
    {
        return $this->hasMany(Member::class);
    }

    /**
     * Get the organizations ticket prices with member status
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ticketPrices()
    {
        return $this->belongsToMany(TicketPrice::class, 'ticket_price_organizations')->withTimestamps();
    }

    /**
     * Get the organizations admins
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function admins()
    {
        return $this->belongsToMany(User::class, 'organization_admins');
    }

    /**
     * Get the organizations news posts
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function news()
    {
        return $this->hasMany(News::class);
    }

    /**
     * Get the organizations csv imports
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function imports()
    {
        return $this->hasMany(CsvData::class);
    }
}
