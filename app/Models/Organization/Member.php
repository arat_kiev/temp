<?php

namespace App\Models\Organization;

use App\Models\BaseModel;
use App\Models\Location\Country;
use App\Models\Picture;
use App\Models\User;
use App\Traits\Sluggable;

class Member extends BaseModel
{
    protected $table = 'organization_members';

    protected $fillable = [
        'member_id', 'fisher_id', 'first_name', 'last_name',
        'email', 'phone', 'since', 'function', 'gender', 'comment',
        'street', 'postcode', 'city', 'leave_date', 'birthday', 'organization_id'
    ];

    protected $dates = [
        'created_at', 'updated_at', 'since', 'leave_date', 'birthday',
    ];

    /**
     * Get the members country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Get the members organization
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * Get the members user, if has matched
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Register model event handlers
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function (Member $member) {
            $member->fisher_id = User::uniqueFisherId();
        });
    }
}
