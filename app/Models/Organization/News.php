<?php

namespace App\Models\Organization;

use App\Models\BaseModel;
use App\Models\Gallery;
use App\Models\Picture;
use App\Models\User;
use App\Traits\Sluggable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends BaseModel
{
    const WAITING = 0;
    const ACTIVE  = 1;
    const DELETED = 2;

    use Sluggable,
        SoftDeletes;

    protected $table = 'organization_news';

    protected $fillable = ['title', 'content', 'published_on'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'published_on'];

    /**
     * Get the news organization
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * Get the news author
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Get the news gallery
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    /**
     * Get the primary picture
     *
     * @return Picture|null
     */
    public function getPictureAttribute()
    {
        return $this->gallery ? $this->gallery->pictures()->orderBy('pivot_priority', 'desc')->first() : null;
    }

    /**
     * Get only public news entries
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param boolean $show
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublic($query, $show = true)
    {
        return $query->where('public', $show);
    }

    /**
     * Get only active news entries
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param Carbon $datetime
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query, Carbon $datetime)
    {
        return $query->where('published_on', '<', $datetime);
    }

    /**
     * Get the news publish status
     *
     * @return int
     */
    public function getStatus()
    {
        $now = Carbon::now();

        if ($this->published_on->gt($now)) {
            return self::WAITING;
        } else {
            if ($this->deleted_at && $this->deleted_at->lt($now)) {
                return self::DELETED;
            } else {
                return self::ACTIVE;
            }
        }
    }
}
