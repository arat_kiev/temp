<?php

namespace App\Models\Inspection;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class InspectionTour extends Model
{
    protected $fillable = ['inspector_id', 'updated_at'];

    /**
     * Get Inspector info
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function inspector()
    {
        return $this->hasOne(User::class, 'id', 'inspector_id');
    }

    /**
     * Get  inspection tour observations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function observations()
    {
        return $this->hasMany(Observation::class, 'tour_id', 'id');
    }
}
