<?php

namespace App\Models\Inspection;

use App\Models\Gallery;
use Illuminate\Database\Eloquent\Model;

class Observation extends Model
{
    protected $fillable = ['tour_id', 'observation_category_id', 'gallery_id', 'comment', 'location', 'updated_at'];

    /**
     * Get observation tour
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tour()
    {
        return $this->belongsTo(InspectionTour::class, 'id', 'tour_id');
    }

    /**
     * Get observation category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(ObservationCategory::class, 'observation_category_id', 'id');
    }

    /**
     * Get observation gallery
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gallery()
    {
        return $this->belongsTo(Gallery::class, 'gallery_id', 'id');
    }
}
