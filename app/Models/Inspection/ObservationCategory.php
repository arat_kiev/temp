<?php

namespace App\Models\Inspection;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class ObservationCategory extends Model
{
    use Translatable;

    protected $fillable = ['type'];

    protected $translatedAttributes = ['observation_category_id'];

    protected $with = ['translations'];

    public $timestamps = false;
}
