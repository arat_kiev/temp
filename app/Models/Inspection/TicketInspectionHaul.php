<?php

namespace App\Models\Inspection;

use App\Models\Meta\Fish;
use App\Models\Ticket\TicketInspection;
use Illuminate\Database\Eloquent\Model;

class TicketInspectionHaul extends Model
{
    /**
     * Get inspection
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inspection()
    {
        return $this->belongsTo(TicketInspection::class, 'id', 'inspection_id');
    }

    /**
     * Get haul fish
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function fish()
    {
        return $this->hasOne(Fish::class);
    }
}
