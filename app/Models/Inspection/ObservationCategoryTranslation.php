<?php

namespace App\Models\Inspection;

use Illuminate\Database\Eloquent\Model;

class ObservationCategoryTranslation extends Model
{
    protected $fillable = ['observation_category_id', 'locale', 'name'];

    public $timestamps = false;
}
