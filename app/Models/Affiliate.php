<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\Area\Area;
use App\Models\Contact\Manager;
use App\Models\Location\Country;
use App\Models\Location\State;

class Affiliate extends BaseModel
{
    protected $table = 'affiliates';

    protected $fillable = [
        'name',
        'link',
        'image_link',
        'price',
        'description',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function areas()
    {
        return $this->morphedByMany(Area::class, 'related', 'affiliables');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function managers()
    {
        return $this->morphedByMany(Manager::class, 'related', 'affiliables');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function countries()
    {
        return $this->morphedByMany(Country::class, 'related', 'affiliables');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function states()
    {
        return $this->morphedByMany(State::class, 'related', 'affiliables');
    }

}
