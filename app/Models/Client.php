<?php

namespace App\Models;

use App\Models\Area\Area;

class Client extends BaseModel
{
    protected $table = 'clients';

    protected $fillable = ['name', 'locale', 'api_key', 'templates'];

    protected $casts = [
        'templates' => 'array',
    ];

    /**
     * Get all areas for client
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function areas()
    {
        return $this->belongsToMany(Area::class, 'client_areas');
    }

    /**
     * Get the clients watermark image
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function watermark()
    {
        return $this->belongsTo(Picture::class);
    }
}
