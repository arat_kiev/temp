<?php

namespace App\Models;

class Commission extends BaseModel
{
    protected $table = 'commissions';

    protected $fillable = ['commission_type', 'commission_value', 'min_value', 'is_online', 'valid_from', 'valid_till'];

    protected $dates = ['created_at', 'updated_at', 'valid_from', 'valid_till'];

    protected $casts = [
        'commission_value'  => 'integer',
        'min_value'         => 'integer',
        'is_online'         => 'boolean',
    ];

    const AVAILABLE_TYPES = ['RELATIVE', 'ABSOLUTE'];

    const DEFAULT_COMMISSIONS = [
        'online'    => [
            'commission_type'           => 'RELATIVE',
            'commission_value'          => 0,
            'commission_value_origin'   => 0,
            'min_value'                 => 0,
            'min_value_origin'          => 0, // 250 cents = 2.50 euro
        ],
        'reseller'  => [
            'commission_type'           => 'RELATIVE',
            'commission_value'          => 0,
            'commission_value_origin'   => 0,
            'min_value'                 => 0,
            'min_value_origin'          => 0, // 250 cents = 2.50 euro
        ],
    ];

    /**
     * Get the min value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getMinValueOriginAttribute($value)
    {
        return $this->attributes['min_value'];
    }

    /**
     * Get the min value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getMinValueAttribute($value)
    {
        return number_format($value / 100.0, 2, ',', '.');
    }

    /**
     * Get the commission value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getCommissionValueOriginAttribute($value)
    {
        return $this->attributes['commission_value'];
    }

    /**
     * Get the commission value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function getCommissionValueAttribute($value)
    {
        return number_format($value / 100.0, 2, ',', '.');
    }

    /**
     * Set the min value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function setMinValueAttribute($value)
    {
        $this->attributes['min_value'] = ((float) str_replace(',', '.', $value)) * 100;
    }

    /**
     * Set the min value attribute
     *
     * @param  string  $value
     * @return string
     */
    public function setCommissionValueAttribute($value)
    {
        $this->attributes['commission_value'] = ((float)str_replace(',', '.', $value)) * 100;

    }

    public function commissionable()
    {
        return $this->morphTo();
    }
}
