<?php

namespace App\Models\Meta;

use App\Models\BaseModel;
use App\Models\Picture;

class LegalDocumentTranslation extends BaseModel
{
    public $timestamps = false;

    protected $fillable = ['name', 'content'];

    public function file()
    {
        return $this->belongsTo(Picture::class, 'file_id');
    }
}