<?php

namespace App\Models\Meta;

use App\Models\Area\Area;
use App\Models\BaseModel;
use App\Models\Comment;
use App\Models\Gallery;
use App\Models\Picture;
use App\Models\Haul;
use App\Traits\Sluggable;
use App\Traits\Draftable;
use Carbon\Carbon;
use Dimsav\Translatable\Translatable;

class Fish extends BaseModel
{
    use Translatable,
        Sluggable,
        Draftable;

    protected $table = 'fishes';

    protected $fillable = ['latin', 'name', 'description', 'picture_id', 'min_size', 'max_size',  'min_weight', 'max_weight'];

    protected $translatedAttributes = ['name', 'description'];

    protected $casts = [
        'min_size'  => 'double',
        'max_size'  => 'double',
    ];

    protected $with = ['translations', 'picture', 'gallery'];

    protected $slugAttribute = 'name';

    /**
     * Get all areas where this fish can be caught
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function areas()
    {
        return $this->belongsToMany(Area::class, 'area_fishes')
            ->withPivot('closed_from', 'closed_till', 'min_length', 'max_length');
    }

    /**
     * Get the fishes picture
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function picture()
    {
        return $this->belongsTo(Picture::class);
    }

    /**
     * Get the fishes gallery (user supplied catch pics)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }

    /**
     * Get the catch pics directly
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function pictures()
    {
        return $this->hasManyThrough(Picture::class, Gallery::class);
    }

    /**
     * Get the fish category info
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(FishCategory::class);
    }

    /**
     * Get the fish category info
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function hauls()
    {
        return $this->hasMany(Haul::class);
    }

    /**
     * Get the comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function setMinSizeAttribute($value)
    {
        $this->attributes['min_size'] = $value ?: null;
    }

    public function setMaxSizeAttribute($value)
    {
        $this->attributes['max_size'] = $value ?: null;
    }

    public function getClosedFromAttribute()
    {
        if ($this->pivot && $this->pivot->closed_from) {
            return Carbon::parse($this->pivot->closed_from);
        } elseif (false) {
            // Check state info
        }

        return null;
    }

    public function getClosedTillAttribute()
    {
        if ($this->pivot && $this->pivot->closed_till) {
            return Carbon::parse($this->pivot->closed_till);
        } elseif (false) {
            // Check state info
        }

        return null;
    }
}
