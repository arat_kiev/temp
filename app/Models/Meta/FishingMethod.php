<?php

namespace App\Models\Meta;

use App\Models\Ticket\Checkin;
use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class FishingMethod extends Model
{
    use Translatable, SoftDeletes;

    protected $table = 'fishing_methods';

    protected $translatedAttributes = ['name', 'description'];

    protected $with = ['translations'];

    protected $translationForeignKey = 'fishing_method_id';

    /**
     * Get checkins
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function checkins()
    {
        return $this->belongsToMany(Checkin::class);
    }

}
