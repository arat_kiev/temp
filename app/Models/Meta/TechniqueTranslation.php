<?php

namespace App\Models\Meta;

use App\Models\BaseModel;

class TechniqueTranslation extends BaseModel
{
    public $timestamps = false;

    protected $fillable = ['name', 'description'];
}
