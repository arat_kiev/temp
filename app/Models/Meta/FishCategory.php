<?php

namespace App\Models\Meta;

use App\Models\BaseModel;
use App\Traits\Sluggable;
use Dimsav\Translatable\Translatable;

class FishCategory extends BaseModel
{
    use Translatable,
        Sluggable;

    protected $table = 'fish_categories';

    protected $fillable = ['name', 'description'];

    protected $translatedAttributes = ['name', 'description'];

    protected $with = ['translations'];

    protected $slugAttribute = 'name';

    /**
     * Get all fishes related to this category
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fishes()
    {
        return $this->hasMany(Fish::class, 'category_id');
    }
}
