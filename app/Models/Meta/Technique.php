<?php

namespace App\Models\Meta;

use App\Models\Area\Area;
use App\Models\Gallery;
use App\Models\Picture;
use App\Models\BaseModel;
use App\Models\Comment;
use App\Traits\Sluggable;
use App\Traits\Draftable;
use Dimsav\Translatable\Translatable;

class Technique extends BaseModel
{
    use Translatable,
        Sluggable,
        Draftable;

    protected $table = 'techniques';

    protected $fillable = ['name', 'description'];

    protected $translatedAttributes = ['name', 'description'];

    protected $with = ['translations'];

    protected $slugAttribute = 'name';

    /**
     * Register model event handlers
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function (Technique $technique) {
            $gallery = Gallery::create([
                'name' => $technique->hasTranslation('de_DE')
                    ? $technique->translate('de_DE')->name
                    : 'Technik #' . $technique->id,
            ]);
            $technique->gallery()->associate($gallery);
        });

        static::deleting(function (Technique $technique) {
            $technique->gallery()->delete();
            $technique->picture()->delete();
        });
    }

    /**
     * Get all areas where this fish can be caught
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function areas()
    {
        return $this->belongsToMany(Area::class, 'area_techniques');
    }

    /**
     * Get the comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * Get the techniques picture
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function picture()
    {
        return $this->belongsTo(Picture::class);
    }

    /**
     * Get the techniques gallery
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }
}
