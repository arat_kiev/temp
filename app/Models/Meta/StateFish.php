<?php

namespace App\Models\Meta;

use App\Models\BaseModel;
use App\Models\Location\State;

class StateFish extends BaseModel
{
    protected $table = 'state_fishes';

    protected $fillable = ['closed_from', 'closed_till', 'min_length', 'max_length'];

    /**
     * Change primary key
     *
     * @var integer $primaryKey
     */
    protected $primaryKey = ['state_id', 'fish_id'];

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool    $incrementing
     */
    public $incrementing = false;

    protected $with = ['fish', 'state'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'closed_from',
        'closed_till',
    ];

    public $timestamps = false;

    /**
     * Get the fish
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fish()
    {
        return $this->belongsTo(Fish::class);
    }

    /**
     * Get the state
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state()
    {
        return $this->belongsTo(State::class);
    }

    // Overwrite default save method to use with primary key for two column
    public function save(array $options = array())
    {
        if(!is_array($this->getKeyName())) {
            return parent::save($options);
        }

        // Fire Event for others to hook
        if($this->fireModelEvent('saving') === false) {
            return false;
        }

        // Prepare query for inserting or updating
        $query = $this->newQueryWithoutScopes();

        if ($this->exists) {
            $this->performCreateSave($query);
        } else {
            $this->performUpdateSave($query);
        }

        // Fires an event
        $this->fireModelEvent('saved', false);

        // Sync
        $this->original = $this->attributes;

        // Touches all relations
        if (array_get($options, 'touch', true)) {
            $this->touchOwners();
        }

        return true;
    }

    private function performCreateSave($query)
    {
        if (count($this->getDirty()) > 0) {
            // Fire Event for others to hook
            if ($this->fireModelEvent('updating') === false) {
                return false;
            }

            // Touch the timestamps
            if ($this->timestamps) {
                $this->updateTimestamps();
            }

            // Convert primary key into an array if it's a single value
            $primary = (count($this->getKeyName()) > 1) ? $this->getKeyName() : [$this->getKeyName()];

            // Fetch the primary key(s) values before any changes
            $unique = array_intersect_key($this->original, array_flip($primary));

            // Fetch the primary key(s) values after any changes
            $unique = !empty($unique) ? $unique : array_intersect_key($this->getAttributes(), array_flip($primary));

            // Fetch the element of the array if the array contains only a single element
            $unique = (count($unique) <> 1) ? $unique : reset($unique);

            // Apply SQL logic
            $query->where($unique);

            // Update the records
            $query->update($this->getDirty());

            // Fire an event for hooking into
            $this->fireModelEvent('updated', false);
        }
    }

    private function performUpdateSave($query)
    {
        // Fire an event for hooking into
        if ($this->fireModelEvent('creating') === false) {
            return false;
        }

        // Touch the timestamps
        if ($this->timestamps) {
            $this->updateTimestamps();
        }

        // Retrieve the attributes
        $attributes = $this->attributes;

        if ($this->incrementing && !is_array($this->getKeyName())) {
            $this->insertAndSetId($query, $attributes);
        } else {
            $query->insert($attributes);
        }

        // Set exists to true in case someone tries to update it during an event
        $this->exists = true;

        // Fire an event for hooking into
        $this->fireModelEvent('created', false);
    }
}
