<?php

namespace App\Models\Meta;

use App\Models\BaseModel;
use App\Models\Contact\Manager;
use Dimsav\Translatable\Translatable;

class LegalDocument extends BaseModel
{
    use Translatable {
        getLocales as traitGetLocales;
    }

    protected $fillable = ['internal_name', 'name', 'content'];

    protected $translatedAttributes = ['name', 'content', 'file'];

    protected $with = ['translations.file', 'managers'];

    public static function boot()
    {
        parent::boot();

        static::creating(function (LegalDocument $doc) {
            $doc->created_by = auth()->check() ? auth()->user()->id : null;
            $doc->updated_by = auth()->check() ? auth()->user()->id : null;
        });

        static::updating(function (LegalDocument $doc) {
            $doc->updated_by = auth()->check() ? auth()->user()->id : null;
        });

        static::deleting(function (LegalDocument $doc) {
            $doc->deleted_by = auth()->check() ? auth()->user()->id : null;
        });
    }

    public function file()
    {
        return $this->translate()->file();
    }

    public function managers()
    {
        return $this->morphedByMany(Manager::class, 'confirmable', 'legal_document_confirmations')
            ->withPivot('signed_by', 'signed_at')
            ->withTimestamps();
    }

    public function getLocales()
    {
        return array_filter($this->traitGetLocales(), function ($locale) {
            return strlen($locale) === 5;
        });
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUnsigned($query)
    {
        return $query->whereNull('signed_at');
    }
}