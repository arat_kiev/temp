<?php

namespace App\Models\Meta;

use App\Models\BaseModel;

class FishingMethodTranslation extends BaseModel
{

    protected $table = 'fishing_methods_translations';

    public $timestamps = false;

    protected $fillable = ['name', 'description'];
}
