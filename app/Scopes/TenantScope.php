<?php

namespace App\Scopes;

use App\Models\Client;
use Config;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

/**
 * Scope for multi tenant queries
 */
class TenantScope implements Scope
{
    /**
     * Apply the scope
     *
     * @param Builder $builder
     * @param Model $model
     */
    public function apply(Builder $builder, Model $model)
    {
        /** @var Client $client */
        $client = Config::get('app.client', Client::where('name', 'system')->first());

        if ($client->filter_areas) {
            $builder->whereHas('clients', function (Builder $query) use ($client) {
                $query->where('clients.id', $client->id);
            });
        }
    }
}
