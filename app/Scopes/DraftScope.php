<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

/**
 * Scope for filtering draft items
 */
class DraftScope implements Scope
{

    /**
     * Apply the scope
     * @param Builder $builder
     * @param Model $model
     * @return $this
     */
    public function apply(Builder $builder, Model $model)
    {
        return $builder->where($model->getTable().'.draft', 0);
    }

}