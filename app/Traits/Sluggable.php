<?php

namespace App\Traits;

use App;
use Dimsav\Translatable\Translatable;
use Slugify;

trait Sluggable
{
    /**
     * Get slug with supplied locale
     *
     * @param string $locale
     * @return string
     */
    public function getSlug($locale = '')
    {
        $translatable = array_has(class_uses(self::class), Translatable::class) &&
            isset($this->translatedAttributes) && in_array($this->slugAttribute, $this->translatedAttributes);

        $locale = $locale ?: App::getLocale();
        $locale = $translatable ? $locale : '';

        if ($locale === '') {
            return Slugify::slugify($this->{$this->slugAttribute});
        } else {
            return Slugify::slugify($this->{$this->slugAttribute.':'.$locale});
        }
    }

    /**
     * Get slug with default locale as attribute
     *
     * @return string
     */
    public function getSlugAttribute()
    {
        return $this->getSlug();
    }
}
