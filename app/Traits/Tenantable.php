<?php

namespace App\Traits;

use App\Scopes\TenantScope;

trait Tenantable
{
    public static function bootTenantable()
    {
        static::addGlobalScope(new TenantScope);
    }
}
