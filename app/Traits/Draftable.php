<?php

namespace App\Traits;

use App\Scopes\DraftScope;
use Auth;

trait Draftable
{

    /**
     * Get only drafted items
     *
     * @param $query
     * @return mixed
     */
    public function scopeGetDraft($query)
    {
        return $query->includeDraft()->where('draft', true);
    }

    /**
     * Disable draft filter
     *
     * @param $query
     * @return mixed
     */
    public function scopeIncludeDraft($query)
    {
        return $query->withoutGlobalScope(DraftScope::class);
    }

    /**
     * Remove from draft
     *
     * @param boolean $draft
     */
    public function draft($draft = true)
    {
        $this->draft = $draft;
        $this->save();
    }

    /**
     * Save item as draft
     *
     * @param array $attributes
     * @return $this
     */
    public function saveAsDraft(array $attributes = [])
    {
        $this->fill($attributes);
        $this->drafted_by = Auth::id();
        $this->draft = true;

        $this->save();

        return $this;
    }

    /**
     * Add draft filter
     */
    public static function bootDraftable()
    {
        static::addGlobalScope(new DraftScope);
    }

}