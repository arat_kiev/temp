<?php

namespace App\Traits;

use App\Exceptions\ApplicationException;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\ViewErrorBag;

trait FileTrait
{
    /**
     * Check if files has been uploaded successfully
     * 
     * @param Request $request
     * @throws ApplicationException
     * @return void
     */
    protected function validateUploadedFiles(Request $request)
    {
        $errors = new MessageBag();

        foreach ($request->allFiles() as $keyName => $file) {
            if ($file->getError()) {
                $errors->add('files', $file->getErrorMessage());
            }
        }

        if ($errors->has('files')) {
            $errorBag = new ViewErrorBag();
            $errorBag->put('default', $errors);
            $request->session()->put('errors', $errorBag);
            throw new ApplicationException();
        }
    }
}
