<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Relations\Relation;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

trait ModelQuery
{
    protected function findOrAccessDenied(Relation $entity, $id)
    {
        try {
            return $entity->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new AccessDeniedHttpException($e->getMessage());
        }
    }
}
