<?php

return [
    'db_fields' => [
        'member_id', 'fisher_id', 'first_name', 'last_name', 'email', 'birthday', 'street', 'post_code', 'city',
        'phone', 'leave_date', 'since', 'function', 'gender', 'comment'
    ]
];