<?php

return [

    /*
     * Snappy HTML2PDF
     */
    'pdf' => [
        'enabled' => true,
        'binary' => base_path(env('WKHTML_PDF', 'vendor/bin/wkhtmltopdf-amd64')),
        'timeout' => false,
        'options' => [],
    ],

    /*
     * Snappy HTML2Image
     */
    'image' => [
        'enabled' => true,
        'binary' => base_path(env('WKHTML_IMG', 'vendor/bin/wkhtmltoimage-amd64')),
        'timeout' => false,
        'options' => [],
    ],


];
