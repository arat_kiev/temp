<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'eu-west-1'),
    ],

    'stripe' => [
        'model'  => App\Models\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'provider' => League\OAuth2\Client\Provider\Facebook::class,
        'client_id' => env('FACEBOOK_ID'),
        'client_secret' => env('FACEBOOK_SECRET'),
        'redirect' => 'https://api.biss-2016.dev',  //TODO change
    ],

    'google' => [
        'provider' => League\OAuth2\Client\Provider\Google::class,
        'client_id' => env('GOOGLE_ID'),
        'client_secret' => env('GOOGLE_SECRET'),
        'redirect' => 'https://api.biss-2016.dev',  //TODO change
    ],

    'heidelpay' => [
        'url' => env('HP_URL'),
        'sender' => env('HP_SENDER'),
        'login' => env('HP_LOGIN'),
        'password' => env('HP_PWD'),
        'mode' => env('HP_MODE'),
        'channel' => env('HP_CHANNEL'),
    ],

    'mapbox' => [
        'access_token' => 'pk.eyJ1IjoiYmlzc2FuemVpZ2VyIiwiYSI6IlotYWtmTlEifQ.k3lZ5gT08-UZ6aVjEUzLfA',
        'base_path' => 'https://api.mapbox.com/v4/bissanzeiger.map-gsuv3bcx'
    ],

    'analytics' => [
        'key' => env('GTM_KEY', 'GTM-5S84TQ5'),
    ],

    'offisy' => [
        'uri' => [
            'auth' => 'https://auth.offisy.at/token',
            'api' => 'https://api.offisy.at/',
        ],
        'iss' => 'https://www.hejfish.com',
        'sub' => 'mailto:info@hejfish.com',
        'aud' => 'https://auth.offisy.at',
        'pem' => config_path('secure/private.pem'),
        'ttl' => 60,
    ],

    'gmaps' => [
       'api_token' => env('GOOGLE_MAP_JS_API', '')
    ],

];
