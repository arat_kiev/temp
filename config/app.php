<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => 'https://'.env('DOMAIN', 'localhost'),

    'domain' => env('DOMAIN', 'localhost'),

    'sub_domains' => [
        'admin' => env('ADMIN_SUBDOMAIN', 'admin'),
        'api' => env('API_SUBDOMAIN', 'api'),
        'pos' => env('POS_SUBDOMAIN', 'pos'),
        'stats' => env('STATS_SUBDOMAIN', 'stats'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => env('TIMEZONE', 'Europe/Vienna'),

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'de_DE',

    /*
    |--------------------------------------------------------------------------
    | Available locales
    |--------------------------------------------------------------------------
    */

    'locales' => ['de_DE', 'en_GB'],

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'de_DE',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY', 'SomeRandomStringWith32Characters'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => env('APP_LOG', 'daily'),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Third-Party Service Providers...
         */
        Barryvdh\Debugbar\ServiceProvider::class,
        Barryvdh\Snappy\ServiceProvider::class,
        Barryvdh\Cors\ServiceProvider::class,
        Spatie\Fractal\FractalServiceProvider::class,
        Spatie\Permission\PermissionServiceProvider::class,
        App\Providers\SlugifyServiceProvider::class,
        Tymon\JWTAuth\Providers\JWTAuthServiceProvider::class,
        Dimsav\Translatable\TranslatableServiceProvider::class,
        Intervention\Image\ImageServiceProvider::class,
        Kozz\Laravel\Providers\Guzzle::class,
        Laravel\Tinker\TinkerServiceProvider::class,
        SimpleSoftwareIO\QrCode\QrCodeServiceProvider::class,
        Collective\Html\HtmlServiceProvider::class,
        Lavary\Menu\ServiceProvider::class,
        Kris\LaravelFormBuilder\FormBuilderServiceProvider::class,
        Laracasts\Flash\FlashServiceProvider::class,
        Mews\Purifier\PurifierServiceProvider::class,
        Yajra\Datatables\DatatablesServiceProvider::class,
        Arcanedev\LogViewer\LogViewerServiceProvider::class,
        Fideloper\Proxy\TrustedProxyServiceProvider::class,
        Mpociot\ApiDoc\ApiDocGeneratorServiceProvider::class,
        Propaganistas\LaravelCacheKeywords\CacheKeywordsServiceProvider::class,
        Dusterio\AwsWorker\Integrations\LaravelServiceProvider::class,
        Gloudemans\Shoppingcart\ShoppingcartServiceProvider::class,
        Bepsvpt\SecureHeaders\SecureHeadersServiceProvider::class,
        Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class,
        Maatwebsite\Excel\ExcelServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,
        App\Providers\MailCssInlineServiceProvider::class,
        App\Providers\ModelRelationMorphMapper::class,
        App\Providers\ValidationServiceProvider::class,
        App\Providers\ComposerServiceProvider::class,
        App\Cache\ResponseCacheServiceProvider::class,
        App\Blacklist\BlacklistServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App'       => Illuminate\Support\Facades\App::class,
        'Artisan'   => Illuminate\Support\Facades\Artisan::class,
        'Auth'      => Illuminate\Support\Facades\Auth::class,
        'Blade'     => Illuminate\Support\Facades\Blade::class,
        'Bus'       => Illuminate\Support\Facades\Bus::class,
        'Cache'     => Illuminate\Support\Facades\Cache::class,
        'Config'    => Illuminate\Support\Facades\Config::class,
        'Cookie'    => Illuminate\Support\Facades\Cookie::class,
        'Crypt'     => Illuminate\Support\Facades\Crypt::class,
        'DB'        => Illuminate\Support\Facades\DB::class,
        'Eloquent'  => Illuminate\Database\Eloquent\Model::class,
        'Event'     => Illuminate\Support\Facades\Event::class,
        'File'      => Illuminate\Support\Facades\File::class,
        'Gate'      => Illuminate\Support\Facades\Gate::class,
        'Hash'      => Illuminate\Support\Facades\Hash::class,
        'Input'     => Illuminate\Support\Facades\Input::class,
        'Inspiring' => Illuminate\Foundation\Inspiring::class,
        'Lang'      => Illuminate\Support\Facades\Lang::class,
        'Log'       => Illuminate\Support\Facades\Log::class,
        'Mail'      => Illuminate\Support\Facades\Mail::class,
        'Password'  => Illuminate\Support\Facades\Password::class,
        'Queue'     => Illuminate\Support\Facades\Queue::class,
        'Redirect'  => Illuminate\Support\Facades\Redirect::class,
        'LRedis'     => Illuminate\Support\Facades\Redis::class,
        'Request'   => Illuminate\Support\Facades\Request::class,
        'Response'  => Illuminate\Support\Facades\Response::class,
        'Route'     => Illuminate\Support\Facades\Route::class,
        'Schema'    => Illuminate\Support\Facades\Schema::class,
        'Session'   => Illuminate\Support\Facades\Session::class,
        'Storage'   => Illuminate\Support\Facades\Storage::class,
        'URL'       => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View'      => Illuminate\Support\Facades\View::class,
        'ResponseCache' => App\Cache\ResponseCacheFacade::class,

        /*
         * Third-Party Class Aliases
         */
        'SnappyPDF' => Barryvdh\Snappy\Facades\SnappyPdf::class,
        'SnappyImage' => Barryvdh\Snappy\Facades\SnappyImage::class,
        'Debugbar' => Barryvdh\Debugbar\Facade::class,
        'Fractal' => Spatie\Fractal\FractalFacade::class,
        'Slugify' => Cocur\Slugify\Bridge\Laravel\SlugifyFacade::class,
        'JWTAuth' => Tymon\JWTAuth\Facades\JWTAuth::class,
        'Image' => Intervention\Image\Facades\Image::class,
        'Guzzle' => Kozz\Laravel\Facades\Guzzle::class,
        'Uuid' => Webpatser\Uuid\Uuid::class,
        'QrCode' => SimpleSoftwareIO\QrCode\Facades\QrCode::class,
        'Html' => Collective\Html\HtmlFacade::class,
        'Form' => Collective\Html\FormFacade::class,
        'Menu' => Lavary\Menu\Facade::class,
        'FormBuilder' => Kris\LaravelFormBuilder\Facades\FormBuilder::class,
        'Flash' => Laracasts\Flash\Flash::class,
        'Purifier' => Mews\Purifier\Facades\Purifier::class,
        'Datatables' => Yajra\Datatables\Facades\Datatables::class,
        'Cart' => Gloudemans\Shoppingcart\Facades\Cart::class,
        'Excel' => Maatwebsite\Excel\Facades\Excel::class,

        // Custom Aliases
        'ZipManager'    => \App\Facades\ZipFacade::class,
        'CouponManager' => \App\Facades\CouponFacade::class,
        'CommissionManager' => \App\Facades\CommissionFacade::class,
    ],

];
