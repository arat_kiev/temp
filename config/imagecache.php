<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Name of route
    |--------------------------------------------------------------------------
    |
    | Enter the routes name to enable dynamic imagecache manipulation.
    | This handle will define the first part of the URI:
    | 
    | {route}/{template}/{filename}
    | 
    | Examples: "images", "img/cache"
    |
    */
   
    'route' => 'image',

    /*
    |--------------------------------------------------------------------------
    | Storage paths
    |--------------------------------------------------------------------------
    |
    | The following paths will be searched for the image filename, submited 
    | by URI. 
    | 
    | Define as many directories as you like.
    |
    */
    
    'paths' => [
        storage_path('uploads/images'),
        storage_path('uploads/attachments'),
        public_path('img'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Manipulation templates
    |--------------------------------------------------------------------------
    |
    | Here you may specify your own manipulation filter templates.
    | The keys of this array will define which templates 
    | are available in the URI:
    |
    | {route}/{template}/{filename}
    |
    | The values of this array will define which filter class
    | will be applied, by its fully qualified name.
    |
    */
   
    'templates' => [
        // Default
        'tsq' => App\Image\Templates\TinySquare::class,
        'ssq' => App\Image\Templates\SmallSquare::class,
        'msq' => App\Image\Templates\MediumSquare::class,
        'lsq' => App\Image\Templates\LargeSquare::class,

        'sre' => App\Image\Templates\SmallRect::class,
        'mre' => App\Image\Templates\MediumRect::class,
        'lre' => App\Image\Templates\LargeRect::class,

        'org' => App\Image\Templates\Original::class,

        // Logo
        'sLogo' => App\Image\Templates\Logo\SmallLogo::class,
        'mLogo' => App\Image\Templates\Logo\MediumLogo::class,
        'lLogo' => App\Image\Templates\Logo\LargeLogo::class,

        // Hero
        'ssqHero' => App\Image\Templates\Hero\SmallSquare::class,
        'msqHero' => App\Image\Templates\Hero\MediumSquare::class,
        'lsqHero' => App\Image\Templates\Hero\LargeSquare::class,

        'sreHero' => App\Image\Templates\Hero\SmallRect::class,
        'mreHero' => App\Image\Templates\Hero\MediumRect::class,
        'lreHero' => App\Image\Templates\Hero\LargeRect::class,

        // Avatar
        'ssqAvatar' => App\Image\Templates\Avatar\SmallSquare::class,
        'sreAvatar' => App\Image\Templates\Avatar\SmallRect::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Image Cache Lifetime
    |--------------------------------------------------------------------------
    |
    | Lifetime in minutes of the images handled by the imagecache route.
    |
    */
   
    'lifetime' => 43200,

];
